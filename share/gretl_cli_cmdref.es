headings 10
Tests 24
add
adf
bds
bkw
chow
coeffsum
coint
cusum
difftest
johansen
kpss
leverage
levinlin
meantest
modtest
normtest
omit
panspec
qlrtest
reset
restrict
runs
vartest
vif
Graphs 14
boxplot
gnuplot
graphpg
gridplot
gpbuild
hfplot
kdplot
panplot
plot
qqplot
rmplot
scatters
textplot
tsplots
Statistics 14
anova
corr
corrgm
fractint
freq
hurst
mahal
pca
pergm
pvalue
spearman
summary
xcorrgm
xtab
Dataset 18
append
data
dataset
delete
genr
info
join
labels
markers
nulldata
open
rename
setinfo
setmiss
setobs
smpl
store
varlist
Estimation 34
ar
ar1
arch
arima
arma
biprobit
dpanel
duration
equation
estimate
garch
gmm
heckit
hsk
intreg
lad
logistic
logit
midasreg
mle
mpols
negbin
nls
ols
panel
poisson
probit
quantreg
system
tobit
tsls
var
vecm
wls
Programming 21
break
catch
clear
continue
elif
else
end
endif
endloop
flush
foreign
funcerr
function
if
include
loop
makepkg
mpi
run
set
setopt
Transformations 10
diff
discrete
dummify
lags
ldiff
logs
orthdev
sdiff
square
stdize
Utilities 6
eval
help
modeltab
pkg
quit
shell
Printing 6
eqnprint
modprint
outfile
print
printf
tabprint
Prediction 1
fcast

# add Tests

Argumento:  listavariables 
Opciones:   --lm (Hace un contraste de ML; solo con MCO)
            --quiet (Presenta solo los resultados básicos del contraste)
            --silent (No presenta nada)
            --vcv (Presenta la matriz de covarianzas del modelo ampliado)
            --both (Solo para estimación VI; mira abajo)
Ejemplos:   add 5 7 9
            add xx yy zz --quiet

Debes solicitar esta instrucción después de ejecutar una instrucción de
estimación. Realiza un contraste conjunto (cuyos resultados puedes obtener
con los accesores "$test" y "$pvalue") sobre la adición de las variables
indicadas en el argumento, al último modelo estimado.

Por defecto, se estima una versión "ampliada" del modelo original, que
resulta al añadirle a este las variables del argumento listavariables, como
regresores. En este caso, el contraste es de tipo Wald sobre el modelo
ampliado, pasando a ser este el "modelo vigente" en lugar del original.
Debes tener esto en cuenta, por ejemplo, para usar $uhat porque este permite
recuperar los errores del que sea el modelo vigente en cada momento, o para
hacer contrastes posteriores.

Alternativamente, si indicas la opción --lm (que solo está disponible para
aquellos modelos estimados mediante MCO), se realiza un contraste de
Multiplicadores de Lagrange. Para eso, se ejecuta una regresión auxiliar en
la que el error de estimación del último modelo se toma como variable
dependiente; y las variables independientes son las de ese último modelo
más las de listavariables. Bajo la hipótesis nula de que las variables
añadidas no tienen una capacidad explicativa adicional, el estadístico
formado multiplicando el tamaño de la muestra por el R-cuadrado de esta
regresión, tiene la distribución de una variable chi-cuadrado con tantos
grados de libertad como el número de regresores añadidos. En este caso, el
modelo original no se substituye por el modelo de la regresión auxiliar.

La opción --both es específica del método de estimación de Mínimos
Cuadrados en 2 Etapas. Indica que las nuevas variables deben añadirse tanto
a la lista de los regresores como a la lista de los instrumentos, puesto que
cuando no se indica nada, se añaden por defecto solo a la de regresores.

Menú gráfico: Ventana de modelo: Contrastes/Añadir variables

# adf Tests

Argumentos: orden listavariables 
Opciones:   --nc (Sin constante)
            --c (Con constante)
            --ct (Con constante más tendencia)
            --ctt (Con constante, más tendencia cuadrática)
            --seasonals (Incluye variables ficticias estacionales)
            --gls (Detrae la media o la tendencia usando MCG)
            --verbose (Muestra los resultados de la regresión)
            --quiet (No presenta los resultados)
            --difference (Usa las primeras diferencias de la variable)
            --test-down[=criterio] (Orden de retardos automático)
            --perron-qu (Mira abajo)
Ejemplos:   adf 0 y
            adf 2 y --nc --c --ct
            adf 12 y --c --test-down
            Ver también jgm-1996.inp

Las opciones que se muestran abajo y la discusión que sigue, se
corresponden sobre todo con el uso de la instrucción adf con datos de
típicas series temporales. Para utilizar esta instrucción con datos de
panel, mira más abajo la sección titulada "Datos de panel".

Esta instrucción calcula un conjunto de contrastes de Dickey-Fuller sobre
cada una de las variables del argumento, siendo la hipótesis nula la
existencia de una raíz unitaria. (Ahora bien, cuando escoges la opción
--difference, se calcula la primera diferencia de la(s) variable(s) antes de
hacer el contraste, y la discusión de abajo debes entenderla como referida
a la(s) variable(s) transformada(s).)

Por defecto, se muestran dos variantes del contraste: una basada en una
regresión que utiliza una constante, y otra que utiliza una constante más
una tendencia lineal. Puedes controlar las variantes que se presentan
especificando uno o más de los indicadores de opción: --nc, --c, --ct, o
--ctt.

Puedes usar la opción --gls con las opciones --c y --ct (con constante, y
con constante más tendencia). El efecto de esta opción es que la serie que
se quiere contrastar, se detrae de la media o de la tendencia usando el
procedimiento de Mínimos Cuadrados Generalizados propuesto Elliott,
Rothenberg y Stock (1996), que proporciona un contraste de mayor potencia
que la aproximación estándar de Dickey-Fuller. Esta opción no es
compatible con --nc, --ctt ni --seasonals.

En todos los casos, la variable dependiente en la regresión del contraste,
es la primera diferencia de la serie especificada (y), y la variable
independiente clave es el primer retardo de y. La regresión se forma de
modo que el coeficiente de la variable y retardada, es igual a la raíz en
cuestión, α, menos 1. Por ejemplo, el modelo con constante puede
escribirse como

  (1 - L)y(t) = b0 + (a-1)y(t-1) + e(t)

Bajo la hipótesis nula de existencia de una raíz unitaria, el coeficiente
de la variable y retardada es igual a cero. Bajo la hipótesis alternativa
de que y es estacionaria, este coeficiente es negativo. Entonces el
contraste es propiamente de una cola.

Selección del orden de retardos

La versión más simple del contraste de Dickey-Fuller asume que la
perturbación aleatoria de la regresión que se utiliza en el contraste no
presenta autocorrelación. En la práctica, esto no es probable que suceda
por lo que la especificación de la regresión a menudo se amplía
incluyendo uno o más retardos de la variable dependiente, proporcionando un
contraste de Dickey-Fuller aumentado (ADF). El argumento orden controla el
número de esos retardos (k), eventualmente dependiendo del tamaño de la
muestra (T).

  Para usar un valor fijo de k, especificado por el usuario: indica un valor
  no negativo para orden.

  Para usar un valor de k dependiente de T: indica orden igual a -1. Así el
  orden se establece según lo aconsejado por Schwert (1989): concretamente
  se toma la parte entera de calcular 12(T/100)^0.25.

Sin embargo, en general no se sabe cuantos retardos serán necesarios para
poder "blanquear" el residuo de la regresión de Dickey-Fuller. Por
consiguiente, es habitual especificar el máximo valor de k, y dejar que los
datos 'decidan' el número concreto de retardos que se van a incluir. Esto
se puede hacer por medio de la opción --test-down. Y también puedes
establecer el criterio con el que se determine un valor óptimo para k,
utilizando el parámetro para esta opción que deberá ser uno de entre AIC
(por defecto), BIC o tstat.

Cuando pides que se compruebe hacia atrás mediante AIC o BIC, el orden de
retardo final para la ecuación ADF es el que optimiza el criterio de
información que elijas (de Akaike o Bayesiano de Schwarz). El procedimiento
exacto dependerá de si indicas o no la opción --gls. Cuando se especifica
GLS (MCG), los criterios AIC y BIC son las versiones "modificadas" descritas
en Ng y Perron (2001); en otro caso, son las versiones estándar. En caso de
MCG, dispones de un refinamiento. Cuando indicas la opción adicional
--perron-qu, la selección del orden de retardo se realiza mediante el
método revisado que recomendaron Perron y Qu (2007). En este caso, los
datos se detraen primero mediante OLS (MCO) de la media o de la tendencia;
GLS (MCG) se aplica una vez que ya se haya determinado el orden de retardo.

Cuando pides que se pruebe hacia atrás mediante el método del estadístico
t, el procedimiento es como se indica a continuación:

1. Se estima la regresión de Dickey-Fuller utilizando k retardos de la
   variable dependiente.

2. ¿Es significativo el último retardo? Si lo es, se ejecuta el contraste
   con un orden de retardos k. Si no lo es, se hace que k = k - 1, y se
   vuelve al paso 1 con un retardo menos. El proceso se repite hasta que sea
   significativo el último retardo de una regresión, o hasta que k sea 0
   (se haría el contraste con un orden de retardos igual a 0).

En el contexto del paso 2 de arriba, "significativo" quiere decir que el
estadístico t del último retardo tiene un valor p asintótico de dos colas
igual o menor que 0.10, frente a la distribución Normal.

En resumen, si admitimos los diferentes argumentos de Perron, Ng, Qu y
Schwert indicados arriba, la instrucción preferible para comprobar una
serie y es probable que sea:

	adf -1 y --c --gls --test-down --perron-qu

(O sustituyendo --ct en lugar de --c si la serie parece tener una
tendencia.) El orden de retardo para el contraste será entonces determinado
probándolo hacia atrás, mediante los cambios en AIC a partir del máximo
de Schwert, con el refinamiento de Perron-Qu.

Los valores P para los contrastes de Dickey-Fuller están basados en
estimaciones de tipo superficie de respuesta. Cuando no se aplica MCG (GLS),
se toman de MacKinnon (1996). De lo contrario, se toman de Cottrell (2015)
o, cuando se prueba hacia atrás, de Sephton (2021). Los valores P son
específicos para el tamaño de la muestra, excepto que estén etiquetados
como asintóticos.

Datos de Panel

Cuando se utiliza la instrucción adf con datos de panel para hacer un
contraste de raíz unitaria de panel, las opciones aplicables y los
resultados que se muestran son algo diferentes.

Primero, mientras que puedes indicar una lista de variables para probar en
el caso de series temporales típicas, con datos de panel solo puedes
contrastar una variable por cada instrucción. Segundo, las opciones que
manejan la inclusión de términos determinísticos pasan a ser mutuamente
excluyentes: debes escoger una entre sin constante, con constante, y con
constante más tendencia; por defecto es con constante. Además, la opción
--seasonals no está disponible. Tercero, la opción --verbose aquí tiene
un significado diferente: produce un breve informe del contraste para cada
serie temporal individual (siendo este por defecto una presentación solo
del resultado global).

Se calcula el contraste global (Hipótesis nula: La serie en cuestión tiene
una raíz unitaria para todas las unidades del panel) de una o las dos
formas siguientes: utilizando el método de Im, Pesaran y Shin (Journal of
Econometrics, 2003) o la de Choi (Journal of International Money and
Finance, 2001) El contraste de Choi requiere que estén disponibles las
probabilidades asociadas (valores P) para los contrastes individuales; si
este no es el caso (dependiendo de las opciones escogidas), se omite. El
estadístico concreto proporcionado para el contraste de Im, Pesaran y Shin
varía del modo siguiente: si el orden de retardo para el contraste no es
cero, se muestra su estadístico W; por otro lado, si las longitudes de las
series de tiempo difieren de un individuo a otro, se muestra su estadístico
Z; en otro caso, se muestra su estadístico t-barra. Consulta también la
instrucción "levinlin".

Menú gráfico: /Variable/Contrastes de raíz unitaria/Contraste aumentado de Dickey-Fuller

# anova Statistics

Argumentos: respuesta tratamiento [ control ] 
Opción:     --quiet (No presenta los resultados)

Análisis de la Varianza: El argumento respuesta deberá ser una serie que
mida algún efecto de interés, y tratamiento deberá ser una variable
discreta que codifique dos o más tipos de tratamiento (o no tratamiento).
Para un ANOVA de dos factores, la variable control (que también será
discreta) deberá codificar los valores de alguna variable de control.

Excepto cuando indicas la opción --quiet, esta instrucción presenta una
tabla mostrando las sumas de cuadrados y los cuadrados de la media junto con
un contraste F. Puedes recuperar el estadístico del contraste F y su
probabilidad asociada, utilizando los accesores "$test" y "$pvalue",
respectivamente.

La hipótesis nula del contraste F es que la respuesta media es invariante
con respecto al tipo de tratamiento o, en otras palabras, que el tratamiento
no tiene efecto. Hablando estrictamente, el contraste solo es válido cuando
la varianza de la respuesta es la misma para todos los tipos de tratamiento.

Ten en cuenta que los resultados que muestra esta instrucción son de hecho
un subconjunto de la información ofrecida por el siguiente procedimiento,
que puedes preparar fácilmente en el GRETL. (1) Genera un conjunto de
variables ficticias que codifiquen todos los tipos de tratamiento excepto
uno. Para un ANOVA de dos factores, genera además un conjunto de variables
ficticias que codifiquen todos los bloques de "control" excepto uno. (2) Haz
la regresión de respuesta sobre una constante y las variables ficticias
utilizando "ols". Con un único factor, se presenta la tabla ANOVA mediante
la opción --anova en esa función ols. En caso de dos factores, el
contraste F relevante lo encuentras utilizando la instrucción "omit" luego
de la regresión. Por ejemplo, (asumiendo que respuesta es y, que xt
codifica el tratamiento, y que xb codifica los bloques de "control"):

	# Un factor
	list Fict_xt = dummify(xt)
	ols y 0 Fict_xt --anova
	# Dos factores
	list Fict_xb = dummify(xb)
	ols y 0 Fict_xt Fict_xb
	# Contraste de significación conjunta de Fict_xt
	omit Fict_xt --quiet

Menú gráfico: /Modelo/Otros modelos lineales/ANOVA

# append Dataset

Argumento:  nombrearchivo 
Opciones:   --time-series (Mira abajo)
            --fixed-sample (Mira abajo)
            --update-overlap (Mira abajo)
            --quiet (Presenta menos detalles de confirmación; mira abajo)
            Mira abajo para opciones adicionales especiales

Abre un archivo de datos y agrega el contenido al conjunto vigente de datos,
si los nuevos datos son compatibles. El programa intentará detectar el
formato del archivo de datos (propio, texto plano, CSV, Gnumeric, Excel,
etc.). Por favor, ten en cuenta que dispones de la instrucción "join" que
ofrece un control mucho mayor para hacer coincidir los datos adicionales con
la base de datos vigente. Observa también que añadir datos a un conjunto
de datos ya existente es potencialmente bastante complicado; en ese sentido,
consulta más abajo la sección titulada "Datos de panel".

Los datos añadidos pueden tener el formato de observaciones adicionales
sobre series ya presentes en el conjunto de datos, y/o el formato de nuevas
series. En caso de añadir series, la compatibilidad requiere (a) que el
número de observaciones de los nuevos datos sea igual al número de datos
actuales, o (b) que los nuevos datos conlleven clara información de las
observaciones de modo que GRETL pueda deducir como colocar los valores.

Un caso que no se admite es aquel en el que los nuevos datos comienzan antes
y acaban después que los datos originales. Para añadir series en esa
situación, puedes utilizar la opción --fixed-sample; esto tiene como
efecto que se suprime la adición de observaciones, por lo que así, la
operación se restringe únicamente a añadir series nuevas.

Cuando se selecciona un archivo de datos para agregar, puede haber un área
de solapamiento con el conjunto de datos existente; es decir, una o más
series pueden tener una o más observaciones en común entre los dos
orígenes. Cuando indicas la opción --update-overlap, la instrucción
append substituye cualquier observación solapada con los valores del
archivo de datos escogido; en otro caso, los valores que en ese momento ya
están en su sitio no se ven afectados.

Las opciones especiales adicionales --sheet, --coloffset, --rowoffset y
--fixed-cols funcionan del mismo modo que con "open"; consulta esa
instrucción para obtener más explicaciones.

Por defecto, se presenta alguna información sobre el conjunto de datos
agregado. La opción --quiet reduce esa informacióna a un simple mensaje
confirmatorio que solo indica la ruta hasta el archivo. Si quieres que la
operación se complete de modo silencioso, indica la instrucción set
verbose off antes de agregar los datos, en combinación con la opción
--quiet.

Datos de panel

Cuando los datos se añaden a un conjunto de datos de panel, el resultado
tan solo va a ser correcto si coinciden de forma apropiada tanto las
"unidades" o "individuos", como los períodos de tiempo.

La instrucción append debería manejar correctamente dos situaciones
relativamente simples. Sirva n para denotar el número de unidades
atemporales y T para denotar el número de períodos de tiempo del panel
vigente; y sirva m para denotar el número de observaciones de los nuevos
datos. Si m = n, los nuevos datos se consideran invariantes en el tiempo, y
se copian repetidos para cada período de tiempo. Por otro lado, si m = T
los datos se tratan como invariantes entre las unidades atemporales, y se
copian repetidos para cada unidad atemporal. Si el panel es "cuadrado", y T
= n aparece una ambigüedad. Por defecto, en este caso se tratan los nuevos
datos como invariantes en el tiempo, pero puedes forzar a que GRETL los
trate como series temporales (invariantes entre las unidades) con la opción
--time-series.

Cuando se reconoce tanto al conjunto de datos vigente como a los datos que
se van a añadir como datos de panel, aparecen dos situaciones. (1) La
longitud de las series de tiempo, T, es diferente entre los dos. Entonces se
presenta un fallo. (2) T coincide. En ese caso se hace un supuesto muy
simple, en particular que las unidades coinciden, empezando por la primera
unidad en ambos conjuntos de datos. Si ese supuesto no es correcto, deberás
utilizar la instrucción "join" en lugar de append.

Menú gráfico: /Archivo/Añadir datos

# ar Estimation

Argumentos: retardos ; depvar indepvars 
Opciones:   --vcv (Presenta la matriz de covarianzas)
            --quiet (No presenta las estimaciones de los parámetros)
Ejemplo:    ar 1 3 4 ; y 0 x1 x2 x3

Calcula las estimaciones de los parámetros utilizando el procedimiento
iterativo generalizado de Cochrane-Orcutt; consulta la Sección 9.5 de
Ramanathan (2002). Las iteraciones acaban cuando la sucesión de sumas de
errores cuadrados no difiere de un término al siguiente en más del 0.005
por ciento, o después de 20 iteraciones.

Con "retardos" tienes que indicar una lista de retardos del término de
perturbación, acabada en un punto y coma. En el ejemplo de arriba, el
término de perturbación se especifica como

  u(t) = rho(1)*u(t-1) + rho(3)*u(t-3) + rho(4)*u(t-4)

Menú gráfico: /Modelo/Series temporales univariantes/Errores AR (MCG)

# ar1 Estimation

Argumentos: depvar indepvars 
Opciones:   --hilu (Utiliza el procedimiento de Hildreth-Lu)
            --pwe (Utiliza el estimador de Prais-Winsten)
            --vcv (Presenta la matriz de covarianzas)
            --no-corc (No afina los resultados con Cochrane-Orcutt)
            --loose (Utiliza un criterio de convergencia menos preciso)
            --quiet (No presenta nada)
Ejemplos:   ar1 1 0 2 4 6 7
            ar1 y 0 xlista --pwe
            ar1 y 0 xlista --hilu --no-corc

Calcula estimaciones MCG que sean viables para un modelo en el que el
término de perturbación se asume que sigue un proceso autorregresivo de
primer orden.

El método utilizado por defecto es el procedimiento iterativo de
Cochrane-Orcutt; por ejemplo, consulta la sección 9.4 de Ramanathan (2002).
El criterio para lograr la convergencia es que las estimaciones sucesivas
del coeficiente de autocorrelación, no difieran en más de 1e-6 o, cuando
indicas la opción --loose, en más de 0.001. Si esto no se alcanza antes de
que se hagan las 100 iteraciones, se muestra un fallo.

Cuando indicas la opción --pwe, se utiliza el estimador de Prais-Winsten.
Esto implica una iteración similar a la de Cochrane-Orcutt; la diferencia
está en que mientras que el método de Cochrane-Orcutt descarta la primera
observación, el método de Prais-Winsten hace uso de ella. Para obtener
más detalles consulta, por ejemplo, el capítulo 13 de Greene (2000).

Cuando indicas la opción --hilu, se utiliza el procedimiento de búsqueda
de Hildreth-Lu. En ese caso, se afinan los resultados utilizando el método
de Cochrane-Orcutt, excepto que especifiques la opción --no-corc. Esta
opción --no-corc se ignora para estimadores diferentes al del método de
Hildreth-Lu.

Menú gráfico: /Modelo/Series temporales univariantes/Errores AR (MCG)

# arch Estimation

Argumentos: orden depvar indepvars 
Opción:     --quiet (No presenta nada)
Ejemplo:    arch 4 y 0 x1 x2 x3

En este momento, esta instrucción se mantiene por compatibilidad con
versiones anteriores, pero sales ganando si utilizas el estimador máximo
verosímil que ofrece la instrucción "garch". Si quieres estimar un modelo
ARCH sencillo, puedes usar el GARCH haciendo que su primer parámetro sea 0.

Estima la especificación indicada del modelo permitiendo ARCH
(Heterocedasticidad Condicional Autorregresiva). Primero, se estima el
modelo mediante MCO, y luego se ejecuta una regresión auxiliar, en la que
se regresa el error cuadrado de la primera sobre sus propios valores
retardados. El paso final es la estimación por mínimos cuadrados
ponderados, utilizando como ponderaciones las inversas de las varianzas de
los errores ajustados con la regresión auxiliar. (Si la varianza que se
predice para alguna observación de la regresión auxiliar, no es positiva,
entonces se utiliza en su lugar el error cuadrado correspondiente).

Los valores alpha presentados debajo de los coeficientes son los parámetros
estimados del proceso ARCH con la regresión auxiliar.

Consulta también "garch" y "modtest" (opción --arch).

# arima Estimation

Argumentos: p d q [ ; P D Q ] ; depvar [ indepvars ] 
Opciones:   --verbose (Presenta los detalles de las iteraciones)
            --quiet (No presenta los resultados)
            --vcv (Presenta la matriz de covarianzas)
            --hessian (Mira abajo)
            --opg (Mira abajo)
            --nc (Sin constante)
            --conditional (Utiliza Máxima Verosimilitud condicional)
            --x-12-arima (Utiliza ARIMA X-12 o X-13 en la estimación)
            --lbfgs (Utiliza el maximizador L-BFGS-B)
            --y-diff-only (ARIMAX especial; mira abajo)
            --lagselect (Mira abajo)
Ejemplos:   arima 1 0 2 ; y
            arima 2 0 2 ; y 0 x1 x2 --verbose
            arima 0 1 1 ; 0 1 1 ; y --nc
            Ver también armaloop.inp, auto_arima.inp, bjg.inp

Advertencia: arma es un alias aceptable para esta instrucción.

Cuando no indicas la lista indepvars, se estima un modelo univariante ARIMA
(Autorregresivo, Integrado, de Medias móviles). Los valores p, d y q
representan el orden autorregresivo (AR), el orden de diferenciación y el
orden de medias móviles (MA), respectivamente. Puedes indicar estos valores
en formato numérico, o como nombres de variables escalares ya existentes.
Por ejemplo, un valor de 1 para d significa que, antes de estimar los
parámetros del ARMA, debe tomarse la primera diferencia de la variable
dependiente.

Si quieres incluir en el modelo solo retardos AR o MA específicos (en
contraposición a todos los retardos hasta un orden indicado) puedes
substituir por p y/o q bien (a) el nombre de una matriz definida previamente
que contiene un conjunto de valores enteros, o bien (b) una expresión tal
como {1,4}; es decir, un conjunto de retardos separados con comas y puestos
entre llaves.

Los valores enteros P, D y Q (opcionales) representan el orden AR
estacional, el orden de diferenciación estacional y el orden MA estacional,
respectivamente. Estos órdenes solo los puedes aplicar cuando los datos
tienen una frecuencia mayor que 1 (por ejemplo, con datos trimestrales o
mensuales); y puedes indicarlas en formato numérico o como variables
escalares.

En el caso univariante, por defecto se incluye en el modelo una ordenada en
el origen, pero puedes eliminar esto por medio de la opción --nc. Cuando
añades indepvars, el modelo se convierte en un ARMAX; en este caso, debes
incluir la constante explícitamente si quieres tener la ordenada en el
origen (como en el segundo ejemplo de arriba).

Dispones de una forma alternativa de sintaxis para esta instrucción: si no
quieres aplicar diferencias (ni estacionales ni no estacionales), puedes
omitir los dos campos d y D a la vez, mejor que introducir explícitamente
0. Además, arma es un alias o sinónimo de arima y así, por ejemplo, la
siguiente instrucción es un modo válido de especificar un modelo ARMA(2,
1):

	arma 2 1 ; y

Por defecto, se utiliza la funcionalidad ARMA "propia" de GRETL, con la
estimación Máximo Verosímil (MV) exacta; pero dispones de la opción de
hacer la estimación mediante MV condicional. (Si el programa ARIMA X-12
está instalado en el ordenador, tienes la posibilidad de utilizarlo en vez
del código propio. Ten en cuenta que, de igual modo, el más reciente X13
puede funcionar como un recambio automático.) Para otros detalles
relacionados con estas opciones, consulta El manual de gretl (Capítulo 31).

Cuando se utiliza código propio de MV exacta, las desviaciones típicas
estimadas se basan por defecto en una aproximación numérica a la (inversa
negativa de la) matriz Hessiana, con un último recurso al Producto Externo
del vector Gradiente (PEG) si el cálculo de la matriz Hessiana numérica
pudiera fallar. Puedes utilizar dos indicadores de opción (mutuamente
excluyentes) para forzar esta cuestión: mientras que la opción --opg
fuerza la utilización del método PEG, sin intentar calcular la matriz
Hessiana, la opción --hessian inhabilita el último recurso a PEG. Ten en
cuenta que un fallo en el cálculo de la matriz Hessiana numérica,
generalmente es un indicador de que un modelo está mal especificado.

La opción --lbfgs es específica de la estimación que utiliza código ARMA
propio y Máxima Verosimilitud exacta; y solicita que se utilice el
algoritmo de "memoria limitada" L-BFGS-B en lugar del maximizador BFGS
habitual. Esto puede ser de ayuda en algunos casos en los que la
convergencia es difícil de lograr.

La opción --y-diff-only es específica de la estimación de modelos ARIMAX
(modelos con orden de integración no nulo, en los que se incluyen
regresores exógenos) y se aplica solo cuando se utiliza la Máxima
Verosimilitud exacta propia de GRETL. Para esos modelos, el comportamiento
por defecto consiste en calcular las primeras diferencias tanto de la
variable dependiente como de los regresores; pero cuando indicas esta
opción, solo se calcula para la variable dependiente, quedando los
regresores en niveles.

El valor del AIC de Akaike indicado en conexión con modelos ARIMA, se
calcula de acuerdo con la definición que utiliza el ARIMA X-12,
concretamente

  AIC = -2L + 2k

donde L es el logaritmo de la verosimilitud y k es el número total de
parámetros estimados. Observa que el ARIMA X-12 no produce criterios de
información tales como AIC cuando la estimación es por Máxima
Verosimilitud condicional.

Las raíces AR y MA mostradas en conexión con la estimación ARMA se basan
en la siguiente representación de un proceso ARMA(p, q):

      	(1 - a_1*L - a_2*L^2 - ... - a_p*L^p)Y =
        c + (1 + b_1*L + b_2*L^2 + ... + b_q*L^q) e_t

Por lo tanto, las raíces AR son las soluciones a

       1 - a_1*z - a_2*z^2 - ... - a_p*L^p = 0

y la estabilidad requiere que estas raíces se encuentren fuera del círculo
de radio unitario.

La cantidad "Frecuencia" presentada en conexión con las raíces AR y MA, es
el valor lambda que soluciona z = r * exp(i*2*pi*lambda) donde z es la raíz
en cuestión, y r es su módulo.

Lag selection

Cuando se indica la opción --lagselect, esta instrucción no proporciona
estimaciones concretas, sino que en su lugar produce una tabla que presenta
criterios de información y el logaritmo de la verosimilitud para ciertas
especificaciones ARMA o ARIMA. Los órdenes de retardo p y q se consideran
como máximos; y cuando se indica una especificación estacional, P y Q
también se consideran como máximos. En todo caso, el orden mínimo se
considera que es 0, y los resultados se muestran para todas las
especificaciones desde la mínima hasta la máxima. Los grados de
diferenciación en la instrucción, d y/o D, se respetan pero no se tratan
como objeto de investigación. Puedes obtener una matriz que contenga los
resultados mediante el accesor "$test".

Menú gráfico: /Modelo/Series temporales univariantes/ARIMA

# arma Estimation

Consulta "arima"; arma es un alias.

# bds Tests

Argumentos: orden x 
Opciones:   --corr1=rho (Mira abajo)
            --sdcrit=multiple (Mira abajo)
            --boot=N (Mira abajo)
            --matrix=m (Utiliza una entrada matricial)
            --quiet (Suprime la presentación de resultados)
Ejemplos:   bds 5 x
            bds 3 --matrix=m
            bds 4 --sdcrit=2.0

Lleva a cabo el contraste BDS (Brock, Dechert, Scheinkman y LeBaron, 1996)
de no linealidad para la serie x. En el contexto econométrico, esto se
aplica habitualmente para comprobar si los residuos de una regresión
incumplen la condición IID (distribución idéntica e independiente). El
contraste se basa en un conjunto de integrales de correlación, preparadas
para detectar la no linealidad de dimensión progresivamente mayor; y se
establece el número de esas integrales con el argumento orden. Estas deben
ser por lo menos 2; con la primera integral se establece una referencia de
partida, pero sin que permita un contraste. El contraste BDS es de tipo
"portmanteau": adecuado para detectar toda clase de desviaciones respecto a
la linealidad, pero no esclarecedor del modo exacto en el que se incumple la
condición.

En lugar de indicar x como serie, puedes utilizar la opción --matrix para
especificar una matriz como entrada, la cual debe tener forma de vector
(columna o fila).

Criterio de proximidad

Las integrales de correlación están basadas en una medida de "proximidad"
entre los puntos de datos, de forma que se consideran próximos a dos de
esos puntos si están situados uno del otro a menos de ε. Dado que el
contraste necesita que se especifique ε, por defecto, GRETL sigue la
recomendación de Kanzler (1999): ε se escoge de modo que la integral de
correlación de primer orden esté en torno a 0.7. Una alternativa habitual
(que necesita menos cálculos) consiste en especificar ε como un múltiplo
de la desviación típica de la serie de interés. La opción --sdcrit
permite este último método; así, en el tercer ejemplo indicado más
arriba, ε se determina que sea igual a dos veces la desviación típica de
x. La opción --corr1 implica la utilización del método de Kanzler pero
permite otra correlación objetivo diferente de 0.7. Deberías tener claro
que estas dos opciones se excluyen mutuamente.

Muestreo repetido

Los estadísticos del contraste BDS tienen distribución asintótica de tipo
N(0,1), pero el contraste rechaza demasiado la hipótesis nula de modo muy
notable con muestras de tamaño entre pequeño y moderado. Por ese motivo,
los valores P se obtienen por defecto mediante muestreo repetido
(bootstrapping) cuando x tiene una longitud menor que 600 (y con referencia
a la distribución Normal, en caso contrario). Si quieres utilizar el
muestreo repetido con muestras más largas, puedes forzar esta cuestión
indicando un valor no nulo para la opción --boot. Por el contrario, si no
quieres que se haga el muestreo repetido con las muestras más pequeñas,
indica un valor de cero para --boot.

Cuando se hace el muestreo repetido, el número de iteraciones por defecto
es de 1999; pero puedes especificar un número diferente indicando un valor
mayor que 1 con la opción --boot.

Matriz accesoria matrix

Cuando se completa con éxito la ejecución de esta instrucción, "$result"
proporciona los resultados del contraste en forma de una matriz con dos
filas y orden - 1 columnas. La primera fila contiene los estadísticos de
contraste y la segunda los valores P, de cada uno de los contrastes por
dimensión, bajo la hipótesis nula de que x es lineal/IID.

# biprobit Estimation

Argumentos: depvar1 depvar2 indepvars1 [ ; indepvars2 ] 
Opciones:   --vcv (Presenta la matriz de covarianzas)
            --robust (Desviaciones típicas robustas)
            --cluster=clustvar (Consulta "logit" para aclaración)
            --opg (Mira abajo)
            --save-xbeta (Mira abajo)
            --verbose (Presenta información adicional)
Ejemplos:   biprobit y1 y2 0 x1 x2
            biprobit y1 y2 0 x11 x12 ; 0 x21 x22
            Ver también biprobit.inp

Estima un modelo probit bivariante utilizando el método de Newton-Raphson
para maximizar la verosimilitud.

La lista de argumentos comienza con las dos variables (binarias)
dependientes, seguidas de una lista de regresores. Cuando indicas una
segunda lista (separada por un punto y coma) se entiende como un grupo de
regresores específicos de la segunda ecuación, siendo indepvars1
específica de la primera ecuación; en otro caso, indepvars1 se considera
que representa un conjunto de regresores común.

Por defecto, las desviaciones típicas se calculan utilizando la matriz
Hessiana analítica al converger. Pero si indicas la opción --opg, la
matriz de covarianzas se basa en el Producto Externo del vector Gradiente
(PEG o OPG); o si indicas la opción --robust, se calculan las desviaciones
típicas cuasi máximo verosímiles (QML), utilizando un "emparedado" entre
la inversa de la matriz Hessiana y el PEG.

Observa que la estimación de rho, la correlación de los términos de error
entre las dos ecuaciones, se incluye en el vector de coeficientes; es el
último elemento de los accesores coeff, stderr y vcv.

Luego de una estimación correcta, el accesor $uhat permite recuperar una
matriz con 2 columnas que contiene los errores generalizados de las dos
ecuaciones; es decir, los valores esperados de las perturbaciones
condicionadas a los resultados observados y a las variables covariantes. Por
defecto, $yhat permite recuperar una matriz con 4 columnas que contiene las
probabilidades estimadas de los 4 posibles resultados conjuntos para (y_1,
y_2), en el orden (1,1), (1,0), (0,1), (0,0). Alternativamente, cuando
indicas la opción --save-xbeta, entonces $yhat tiene 2 columnas y contiene
los valores de las funciones índice de las ecuaciones respectivas.

El resultado incluye un contraste de la hipótesis nula de que las
perturbaciones de las dos ecuaciones no están correlacionadas. Este es un
test de razón de verosimilitudes excepto que se solicite el estimador cuasi
máximo verosímil (QML) de la varianza, en cuyo caso se usa el test de
Wald.

# bkw Tests

Opción:     --quiet (No presenta nada)
Ejemplos:   longley.inp

Debe ir después de la estimación de un modelo que contenga al menos dos
variables explicativas. Calcula y presenta información de diagnóstico en
relación a la multicolinealidad, en concreto la Tabla BKW que está basada
en el trabajo de Belsley, Kuh y Welsch (1980). Esta tabla presenta un
sofisticado análisis del grado y de las causas de la multicolinealidad,
mediante el examen de los autovalores de la inversa de la matriz de
correlaciones. Para tener una explicación en detalle del enfoque BKW en
relación a GRETL, y con diversos ejemplos, consulta Adkins, Waters y Hill
(2015).

Después de utilizar esta instrucción, puedes usar el accesor "$result"
para recuperar la tabla BKW en forma de matriz. Consulta también la
instrucción "vif" para obtener un enfoque más sencillo del diagnóstico de
la multicolinealidad.

Hay también una función denominada "bkw" que ofrece una mayor
flexibilidad.

Menú gráfico: Ventana de modelo: Análisis/Colinealidad

# boxplot Graphs

Argumento:  listavariables 
Opciones:   --notches (Muestra el intervalo del 90 por ciento para la mediana)
            --factorized (Mira abajo)
            --panel (Mira abajo)
            --matrix=nombre (Representa las columnas de la matriz indicada)
            --output=nombrearchivo (Envía el resultado al archivo especificado)

Estos gráficos muestran la distribución de una variable. Una caja central
encierra el 50 por ciento central de los datos; i.e. está deslindada por el
primer y el tercer cuartiles. Un "bigote" se extiende desde cada límite de
la caja con un rango igual a 1.5 veces el rango intercuartil. Las
observaciones que están fuera de ese rango se consideran valores atípicos
y se representan mediante puntos. Se dibuja una línea a lo ancho de la caja
en la mediana. El signo "+" se utiliza para indicar la media. Si escoges la
opción de mostrar un intervalo de confianza para la mediana, este se
calcula mediante el método bootstrap y se muestra con formato de líneas
con rayas horizontales por arriba y/o abajo de la mediana.

La opción --factorized te permite examinar la distribución de la variable
elegida condicionada al valor de algún factor discreto. Por ejemplo, si un
conjunto de datos contiene una variable con los salarios y una variable
ficticia con el género, puedes escoger la de los salarios como objetivo y
la del género como el factor, para ver así los gráficos de cajas de
salarios de hombres y mujeres, uno al lado del otro, como en

	boxplot salario genero --factorized

Ten en cuenta que en este caso debes especificar exactamente solo dos
variables, con el factor indicado en segundo lugar.

Cuando tienes un conjunto vigente de datos de panel y especificas solo una
variable, la opción --panel produce una serie de gráficos de cajas (uno al
lado del otro) en la que cada uno se corresponde con un grupo o "unidad" del
panel.

Generalmente se requiere el argumento listavariables que se refiere a una o
más series del conjunto vigente de datos (indicadas bien por el nombre o
bien por el número ID). Pero si, mediante la opción --matrix, indicas una
matriz ya definida, este argumento se convierte en opcional pues, por
defecto, se dibuja un gráfico para cada columna de la matriz especificada.

Los gráficos de cajas en GRETL se generan utilizando la instrucción
gnuplot, y resulta posible especificar con mayor detalle el gráfico
añadiendo instrucciones adicionales de Gnuplot, puestas entre llaves. Para
obtener más detalles, consulta la ayuda para la instrucción "gnuplot".

En modo interactivo, el resultado se muestra inmediatamente. En modo de
procesamiento por lotes, el proceder por defecto consiste en escribir un
archivo de instrucciones de Gnuplot en el directorio de trabajo del usuario,
con un nombre con el patrón gpttmpN.plt, comenzando con N = 01. Puedes
generar los gráficos más tarde utilizando el gnuplot (o bien wgnuplot bajo
MS Windows). Puedes modificar este comportamiento mediante el uso de la
opción --output=nombrearchivo. Si quieres obtener más detalles, consulta
la instrucción "gnuplot".

Menú gráfico: /Ver/Gráficos/Gráficos de caja

# break Programming

Salida de un bucle. Puedes utilizar esta instrucción solo dentro de un
bucle; eso provoca que la ejecución de instrucciones salga del bucle actual
(del más interior, si hay varios anidados). Consulta también "loop",
"continue".

# catch Programming

Sintaxis:   catch command

Esta no es una instrucción por si misma, pero puedes utilizarla como
prefijo en la mayoría de las instrucciones habituales: su efecto es el de
prevenir que acabe un guion de instrucciones si ocurre un fallo al ejecutar
una de ellas. Si aparece un fallo, esto se registra con un código de fallo
interno al que puedes acceder con "$error" (un valor de 0 indica éxito).
Inmediatamente después de utilizar catch deberías verificar siempre cual
es el valor de $error, y realizar una acción adecuada si falló una de las
instrucciones.

No puedes utilizar la palabra clave catch antes de if, elif o endif.
Además, no debe utilizarse en peticiones a funciones definidas por el
usuario, pues se pretende utilizarla solo con las instrucciones de GRETL y
con las peticiones a los operadores o funciones "internos". Más aún, no
puedes usar catch combinada con la asignación mediante "flecha atrás" de
modelos o gráficos, a iconos de sesión (consulta El manual de gretl
(Capítulo 3)).

# chow Tests

Variantes:  chow obs
            chow dummyvar --dummy
Opciones:   --dummy (Utiliza una variable ficticia ya existente)
            --quiet (No presenta las estimaciones del modelo ampliado)
            --limit-to=lista (Limita el contraste a un subconjunto de regresores)
Ejemplos:   chow 25
            chow 1988:1
            chow mujer --dummy

Debe ir a continuación de una regresión MCO (OLS). Si indicas un número
de observación o una fecha, proporciona un contraste respecto a la
hipótesis nula de que no existe cambio estructural en el punto de corte
indicado. El procedimiento consiste en crear una variable ficticia que toma
el valor 1 desde el punto de corte especificado por obs hasta el final de la
muestra, y 0 en otro caso, así como generar términos de interacción entre
esa ficticia y los regresores originales. Si indicas una ficticia, se
contrasta esa hipótesis nula de homogeneidad estructural respecto a esa
variable ficticia, y también se añaden términos de interacción. En cada
caso se ejecuta una regresión ampliada incluyendo los términos
adicionales.

Por defecto, se calcula un estadístico F, considerando la regresión
ampliada como el modelo sin restricciones y el modelo original como el
restringido. Pero si el modelo original utilizó un estimador robusto para
la matriz de covarianzas, el estadístico de contraste es uno de Wald con
distribución chi-cuadrado; con su valor basado en un estimador robusto de
la matriz de covarianzas de la regresión ampliada.

Puedes utilizar la opción --limit-to para limitar el conjunto de términos
de interacción con la variable ficticia de corte, a un subconjunto de los
regresores originales. El argumento para esta opción debe ser una lista ya
definida en la que todos sus elementos estén entre los regresores
originales, y no debe incluir la constante.

Menú gráfico: Ventana de modelo: Contrastes/Contraste de Chow

# clear Programming

Opciones:   --dataset (Elimina solo el conjunto de datos)
            --functions (Elimina las funciones (unicamente))
            --all (Elimina todo)

Por defecto, esta instrucción quita de la memoria el conjunto de datos
vigente (si hay alguno), además de todas las variables guardadas
(escalares, matrices, etc.). Ten en cuenta que también tienes este efecto
al abrir un nuevo conjunto de datos, o al utilizar la instrucción
"nulldata" para crear un conjunto de datos vacío; por eso normalmente no
necesitas hace uso explícito de "clear".

Cuando indicas la opción --dataset, entonces solo se elimina el conjunto de
datos (más cualquier lista de series definida); otros objetos guardados
como matrices, escalares o 'bundles', se van a conservar.

Cuando indicas la opción --functions, entonces se elimina de la memoria
cualquier función definida por el usuario y cualquier función definida en
los paquetes que tengas cargados. El conjunto de datos y otras variables no
se ven afectados.

Cuando indicas la opción --all, entonces la eliminación es completa: el
conjunto de datos, las variables guardadas de todo tipo, además de las
funciones definidas por el usuario y en paquetes.

# coeffsum Tests

Argumento:  listavariables 
Opción:     --quiet (No presenta nada)
Ejemplos:   coeffsum xt xt_1 xr_2
            Ver también restrict.inp

Debe ir después de una regresión. Calcula la suma de los coeficientes de
las variables del argumento listavariables. Presenta esta suma junto con su
desviación típica y la probabilidad asociada al estadístico para
contrastar la hipótesis nula de que la suma es cero.

Ten en cuenta la diferencia entre esto y la instrucción "omit", pues esta
última te permite contrastar la hipótesis nula de que los coeficientes de
un subconjunto especificado de variables independientes son todos nulos.

La opción --quiet te puede ser de utilidad si únicamente deseas acceder a
los valores de "$test" y de "$pvalue" que se registran después de terminar
la estimación con éxito.

Menú gráfico: Ventana de modelo: Contrastes/Suma de los coeficientes

# coint Tests

Argumentos: orden depvar indepvars 
Opciones:   --nc (Sin constante)
            --ct (Con constante y tendencia)
            --ctt (Con constante más tendencia cuadrática)
            --seasonals (Con variables ficticias estacionales)
            --skip-df (Sin contrastes DF sobre las variables individuales)
            --test-down[=criterio] (Orden de retardos automático)
            --verbose (Presenta detalles adicionales de las regresiones)
            --silent (No presenta nada)
Ejemplos:   coint 4 y x1 x2
            coint 0 y x1 x2 --ct --skip-df

Contraste de cointegración de Engle-Granger (1987). El proceso por defecto
consiste en: (1) realizar los contrastes de Dickey-Fuller respecto a la
hipótesis nula de que cada una de las variables enumeradas tiene una raíz
unitaria; (2) estimar la regresión de cointegración; y (3) hacer un
contraste DF respecto a los errores que comete la regresión de
cointegración. Cuando se indica la opción --skip-df, se omite el paso (1).

Si el orden especificado de retardos es positivo, todos los contrastes de
Dickey-Fuller usan ese orden pero con este requisito: cuando se indica la
opción --test-down, el valor indicado se toma como un máximo, y el orden
concreto de retardos que se utilizará en cada caso se obtiene probando
hacia abajo. Consulta la instrucción "adf" para obtener más detalles sobre
este procedimiento.

Por defecto, la regresión de cointegración contiene una constante pero, si
quieres eliminar la constante, añade la opción --nc. Si quieres ampliar la
lista de términos determinísticos en la regresión de cointegración con
tendencia lineal (o cuadrática), añade la opción --ct (o --ctt). Estos
indicadores de opción son mutuamente excluyentes. También tienes la
posibilidad de añadir variables ficticias estacionales (en caso de utilizar
datos trimestrales o mensuales).

Los valores P (probabilidades asociadas) de este contraste se basan en
MacKinnon (1996). El código relevante se incluye con el amable permiso del
propio autor.

Para obtener los contrastes de cointegración de Søren Johansen, consulta
"johansen".

Menú gráfico: /Modelo/Series temporales multivariantes

# continue Programming

Puedes usar esta instrucción solo dentro de un bucle; su efecto consiste en
saltarse los enunciados posteriores que haya dentro de la iteración vigente
del bucle (más interno) vigente. Consulta también "loop", "break".

# corr Statistics

Variantes:  corr [ listavariables ]
            corr --matrix=nombrematriz
Opciones:   --uniform (Garantiza una muestra uniforme)
            --spearman (Rho de Spearman)
            --kendall (Tau de Kendall)
            --verbose (Presenta jerarquías)
            --plot=modo-o-nombrearchivo (Mira abajo)
            --triangle (Representa solo la mitad inferior, mira abajo)
            --quiet (No presenta nada)
Ejemplos:   corr y x1 x2 x3
            corr ylista --uniform
            corr x y --spearman
            corr --matrix=X --plot=display

Por defecto, presenta los coeficientes de correlación (correlación
producto-momento de Pearson) por pares de las variables de listavariables, o
de todas las variables del conjunto de datos si no indicas listavariables.
El comportamiento típico de esta instrucción consiste en utilizar todas
las observaciones disponibles para calcular cada coeficiente por parejas de
variables, pero cuando indicas la opción --uniform, la muestra se limita
(si es necesario) de modo que se utiliza el mismo conjunto de observaciones
para todos los coeficientes. Esta opción es adecuada solo cuando hay un
número diferente de valores ausentes en las variables utilizadas.

Las opciones --spearman y --kendall (que son mutuamente excluyentes)
permiten calcular, respectivamente, el coeficiente rho de correlación por
rangos de Spearman y el coeficiente tau de correlación por rangos de
Kendall en lugar del coeficiente de Pearson (por defecto). Cuando indicas
alguna de estas opciones, listavariables debe contener solo dos variables.

Cuando se calcula una correlación por rangos, puedes utilizar la opción
--verbose para presentar los datos originales y su jerarquía (en otro caso,
esta alternativa se ignora).

Si listavariables contiene más de dos series y GRETL no está en modo de
procesamiento por lotes, se muestra un gráfico de "mapa de calor" de la
matriz de correlaciones. Puedes ajustar esto mediante la opción --plot, en
la que los parámetros que se admiten son: none (para no mostrar el
gráfico), display (para presentar el gráfico incluso cuando se esté en
modo de procesamiento por lotes), o un nombre de archivo. El efecto de
indicar un nombre de archivo es como el descrito para la opción --output de
la instrucción "gnuplot". Cuando activas la representación del gráfico,
puedes utilizar la opción --triangle para mostrar solo el mapa de calor del
triángulo inferior de la matriz.

Cuando indicas una forma alternativa, utilizando una matriz ya definida en
lugar de una lista de series, las opciones --spearman y --kendall no están
disponibles (pero consulta la función "npcorr").

Puedes usar el accesor "$result" para obtener las correlaciones en forma de
matriz. Ten en cuenta que cuando esta es la matriz que te interese (y no
solo los coeficientes de las parejas), entonces en caso de que haya valores
ausentes se aconseja utilizar la opción --uniform. A no ser que utilices
una muestra común única, no se garantiza que la matriz de correlaciones
sea semidefinida positiva, como debiera ser por construcción.

Menú gráfico: /Ver/Matriz de correlación
Otro acceso:  Ventana principal: Menú emergente (tras selección múltiple)

# corrgm Statistics

Argumentos: y [ orden ] 
Opciones:   --bartlett (Utiliza las desviaciones típicas de Bartlett)
            --plot=modo-o-nombrearchivo (Mira abajo)
            --silent (No presenta nada)
            --acf-only (Omite las autocorrelaciones parciales)
Ejemplos:   corrgm x 12
            corrgm GDP 12 --acf-only

Muestra y/o representa los valores de la función de autocorrelación (FAC)
de la serie y, que puede especificarse por su nombre o por su número. Los
valores se definen como rho(u_t, u_t-s) donde u_t es la t-ésima
observación de la variable u, y s denota el número de retardos.

A no ser que indiques la opción --acf-only, también se presentan los
coeficientes de la función de autocorrelación parcial (FACP, calculados
utilizando el algoritmo de Durbin-Levinson), y que están libres de los
efectos de los retardos intermedios.

Se utilizan asteriscos para indicar la significación estadística de las
autocorrelaciones individuales. Por defecto, esto se evalúa utilizando una
desviación típica igual al cociente entre 1 y la raíz cuadrada del
tamaño de la muestra; pero cuando indicas la opción --bartlett, entonces
se utilizan las desviaciones típicas de Bartlett para la FAC. Si resulta
aplicable, esta opción también determina la banda de confianza que se
dibuja en el gráfico de la FAC. Además, se muestra el estadístico Q de
Ljung-Box, que contrasta la hipótesis nula de que la serie es "ruido
blanco" hasta el nivel de retardo indicado.

Si especificas un valor para orden, la longitud del correlograma se limita
hasta ese número de retardos como máximo; en otro caso, la longitud se
determina automáticamente como una función de la frecuencia de los datos y
del número de observaciones.

Representación gráfica

Por defecto, si GRETL no está en modo de procesamiento por lotes, se genera
un gráfico del correlograma. Esto puedes ajustarlo mediante la opción
--plot en la que los parámetros que se admiten son: none (para no mostrar
el gráfico), display (para presentar un gráfico incluso en modo de
procesamiento por lotes); o un nombre de archivo. El efecto de indicar un
nombre de archivo es como el descrito para la opción --output de la
instrucción "gnuplot".

Accesores

Cuando se complete con éxito esta instrucción, puedes usar los accesores
"$test" y "$pvalue" para recuperar el estadístico Q y su Probabilidad
Asociada (valor P), evaluados para el retardo de orden mayor. Ten en cuenta
que si únicamente quieres este contraste, puedes utilizar en su lugar la
función "ljungbox".

Menú gráfico: /Variable/Correlograma
Otro acceso:  Ventana principal: Menú emergente (selección única)

# cusum Tests

Opciones:   --squares (Realiza el contraste CUSUMSQ)
            --quiet (Solo presenta el contraste de Harvey-Collier)
            --plot=Modo-o-nombrearchivo (Mira abajo)

Debe ir después de la estimación de un modelo mediante MCO. Te permite
realizar el contraste CUSUM de estabilidad de los parámetros (o el
contraste CUSUMSQ si indicas la opción --squares). Vas a obtener una serie
con los errores de predicción adelantados un paso, ejecutando una serie de
regresiones. En la primera regresión se utilizan las primeras k
observaciones y te permite generar la predicción de la variable dependiente
en la observación k + 1; en la segunda se utilizan las primeras k + 1
observaciones y se genera una predicción para la observación k + 2, y así
sucesivamente (donde k es el número de parámetros del modelo original).

Se presenta la suma acumulada de los errores de predicción escalados (o los
cuadrados de estos errores). La hipótesis nula de estabilidad de los
parámetros se rechaza con un nivel de significación del 5 por ciento
cuando la suma acumulada se separa de la banda de confianza del 95 por
ciento.

En el caso del contraste CUSUM, también se presenta el estadístico t de
Harvey-Collier para contrastar la hipótesis nula de estabilidad de los
parámetros. Consulta el libro Econometric Analysis de Greene para obtener
más detalles. Para el contraste CUSUMSQ, se calcula la banda de confianza
del 95 por ciento utilizando el algoritmo indicado en Edgerton y Wells
(1994).

Por defecto, cuando GRETL no está en modo de procesamiento por lotes, se
muestra un gráfico con la serie acumulada y el intervalo de confianza.
Puedes ajustar esto por medio de la opción --plot. Los parámetros
admisibles para esta opción son none (para omitir el gráfico); display
(para visualizar un gráfico incluso en modo de procesamiento por lotes); o
el nombre de un archivo. El efecto de proporcionar el nombre de un archivo
es como el descrito para la opción --output de la instrucción "gnuplot".

Menú gráfico: Ventana de modelo: Contrastes/Contraste CUSUM(SQ)

# data Dataset

Argumento:  listavariables 
Opciones:   --compact=método (Especifica el método para compactar)
            --quiet (No muestra los resultados excepto en caso de fallo)
            --name=identificador (Renombra series importadas)
            --odbc (Importa de una base de datos ODBC)
            --no-align (Específico para ODBC, mira abajo)

Lee las variables de listavariables de un archivo de base de datos (propio
de GRETL, RATS 4.0 o PcGive) que debe abrirse previamente utilizando la
instrucción "open". Puedes usar la instrucción data para importar series
de DB.NOMICS o de una base de datos ODBC; para obtener detalles sobre estas
variantes consulta gretl + DB.NOMICS o El manual de gretl (Capítulo 42),
respectivamente.

Puedes establecer la frecuencia de los datos y el rango de la muestra
mediante las instrucciones "setobs" y "smpl", antes de utilizar esta
instrucción. Este es un ejemplo:

	open fedstl.bin
	setobs 12 2000:01
	smpl ; 2019:12
	data unrate cpiaucsl

Las instrucciones de arriba abren una base de datos (que se ofrece con
GRETL) llamada fedstl.bin, determinan que los datos son mensuales, que
empiezan en enero de 2000, que la muestra finaliza en diciembre de 2019, y
que se importan las series denominadas unrate (tasa de desempleo) y cpiaucsl
(IPC de todos).

Si no especificas setobs y smpl de este modo, la frecuencia de los datos y
el rango de la muestra se establecen utilizando la primera variable que se
lee de la base de datos.

Si las series que se van a leer son de frecuencia mayor que el conjunto de
datos de trabajo, puedes especificar un método para compactar como aquí
debajo:

	data LHUR PUNEW --compact=average

Los cinco métodos que permiten compactar de los que dispones son estos:
"average" (toma la media de las observaciones de alta frecuencia), "last"
(utiliza la última observación), "first", "sum" y "spread", pero si no
especificas ningún método, por defecto se utiliza la media. El método
"spread" es especial pues con él no se pierde ninguna información, sino
que más bien esta se expande entre varias series, una por cada subperíodo.
Así con ella cuando añades, por ejemplo, una serie mensual a un conjunto
de datos trimestrales, se generan 3 series (una por cada mes del trimestre)
cuyos nombres contienen los sufijos m01, m02 y m03.

Cuando las series que se leen son de frecuencia menor que la del conjunto de
datos de trabajo, los valores de los datos añadidos sencillamente se
repiten según se necesite; pero ten en cuenta que puedes utilizar la
función "tdisagg" para solicitar que se haga una distribución o una
interpolación ("desagregación temporal ").

En el caso de bases de datos propias (únicamente) de GRETL, puedes utilizar
los caracteres "genéricos", * y ? en listavariables para importar series
que coincidan con el patrón indicado. Por ejemplo, la siguiente expresión
va a importar todas las series de la base de datos cuyos nombres comiencen
por cpi:

	data cpi*

Puedes usar la opción --name para determinar un nombre distinto del nombre
original en la base de datos, para la seire importada. El parámetro debe
ser un identificador válido de GRETL. Esta opción se restringe al caso en
el que especificas una única serie a importar.

La opción --no-align se aplica solo para importar series mediante ODBC. Por
defecto, se necesita que la solicitud ODBC devuelva información que indique
a GRETL en qué filas del conjunto de datos situar los datos que se reciben
(o que el número de valores que se reciben coincida, al menos, bien con la
extensión del conjunto de datos o bien con la extensión del rango de la
muestra vigente). Determinando la opción --no-align se relaja este
requisito: si no se cumplen estas condiciones mencionadas, los valores que
se reciben se colocan simplemente de forma consecutiva, comenzando en la
primera fila del conjunto de datos. Si el número de esos valores es menor
que el de filas en el conjunto de datos, las filas del final se rellenan con
NAs; si el número es mayor que el de filas, se descartan los valores extra.
Para obtener más información sobre cómo importar con ODBC, consulta El
manual de gretl (Capítulo 42).

Menú gráfico: /Archivo/Bases de datos

# dataset Dataset

Argumentos: clave parámetros 
Opción:     --panel-time (Mira abajo 'addobs')
Ejemplos:   dataset addobs 24
            dataset addobs 2 --panel-time
            dataset insobs 10
            dataset compact 1
            dataset compact 4 last
            dataset expand
            dataset transpose
            dataset sortby x1
            dataset resample 500
            dataset renumber x 4
            dataset pad-daily 7
            dataset unpad-daily
            dataset clear

Realiza diversas operaciones en el conjunto de datos como un todo,
dependiendo de la clave indicada, que debe ser: addobs, insobs, clear,
compact, expand, transpose, sortby, dsortby, resample, renumber, pad-daily o
unpad-daily. Advertencia: Con la excepción de la opción clear, estas
acciones no están disponibles mientras tengas una submuestra del conjunto
de datos, escogida por selección de los casos según algún criterio
booleano.

addobs: Debe estar seguido de un entero positivo, digamos n. Añade las n
observaciones adicionales al final del conjunto de datos de trabajo. Esto
está pensado principalmente con el propósito de hacer predicciones. Los
valores de la mayoría de las variables a lo largo del rango añadido se van
a estipular como ausentes, pero ciertas variables determinísticas se
reconocen y su contenido se extiende al rango añadido; en concreto, las
variables con tendencia lineal simple y las variables ficticias periódicas.
Si el conjunto de datos tiene la estructura de un panel, la acción
predeterminada consiste en añadir n unidades de sección cruzada al panel;
pero si indicas la opción --panel-time, el efecto consiste en añadir n
observaciones a las series temporales para cada unidad.

insobs: Debe estar seguido de un entero positivo (no mayor que el número
vigente de observaciones) que especifica la posición en la que se inserta
una única observación. Todos los datos posteriores se desplazan un lugar y
el conjunto de datos se amplía en una observación. Excepto a la constante,
se le dan valores ausentes a todas las variables en la nueva observación.
Esta acción no está disponible para conjuntos de datos de panel.

clear: No necesita ningún parámetro. Vacía todos los datos vigentes,
devolviendo el GRETL a su estado "vacío" inicial.

compact: Esta acción solo está disponible para datos de series temporales,
y compacta todas las series del conjunto de datos a una frecuencia menor.
Requiere un parámetro, un entero positivo que represente la nueva
frecuencia de los datos. En general, debe ser menor que la frecuencia
vigente (por ejemplo, indicar un valor de 4 cuando la frecuencia vigente es
12, indica que se van a compactar los datos de mensuales a trimestrales). La
única excepción es una nueva frecuencia de 52 (semanal) cuando los datos
vigentes sean diarios (frecuencia 5, 6 o 7). También puedes indicar un
segundo parámetro, en concreto uno de entre sum, first, last o spread.
Estos permiten especificar qué se va a compactar utilizando,
respectivamente: la suma de los valores de frecuencia mayor, el valor de
inicio-de-período, el valor de fin-de-período, o expandiendo los valores
de frecuencia mayor entre varias series (una por cada subperíodo), pues por
defecto se hace usando la media.

En caso de querer compactar (unicamente) de frecuencia diaria a semanal,
tienes disponibles las dos opciones especiales --repday y --weekstart. La
primera de ellas te permite seleccionar un "día representativo" de la
semana que sirva como valor semanal. El parámetro de esta opción deberá
consistir en un número entero entre 0 (Domingo) y 6 (Sábado), incluidos.
Por ejemplo, indicando --repday=3 estarás eligiendo el valor de los
Miércoles como representativo del valor semanal. Si no indicas la opción
--repday, se necesita saber en que día de la semana pretendes comenzar,
para alinear los datos correctamente. Con datos con frecuencia de 5 o de 6
días, siempre se toma el Lunes, pero con datos de 7 días tienes la
posibilidad de elegir entre las opciones --weekstart=0 (Domingo) y
--weekstart=1 (Lunes), en la que se toma el Lunes como la predeterminada.

expand: Esta acción solo está disponible para datos de series temporales
anuales o trimestrales, pues los datos anuales se pueden expandir a
trimestrales o mensuales, y los datos trimestrales a mensuales. Todas las
series del conjunto de datos se rellenan con la nueva frecuencia repitiendo
los valores existentes. Si la base de datos original es anual, la expansión
por defecto es la trimestral, pero la función expand puede estar seguida de
12 para solicitar que sea la mensual. Consulta la función "tdisagg" para
obtener medios más sofisticados de convertir los datos a una frecuencia
mayor.

transpose: No necesita ningún parámetro adicional. Traspone el conjunto
vigente de datos, es decir, cada observación (fila) del conjunto vigente de
datos se va a tratar como una variable (columna), y cada variable como una
observación. Esta acción te puede ser útil si los datos se leyeron de
algún origen externo en el que las filas de la tabla de datos representan
variables.

sortby: Se requiere el nombre de una única serie o lista. Cuando indicas
una serie, las observaciones de todas las variables del conjunto de datos se
vuelven a ordenar según los valores ascendentes de la serie especificada.
Cuando indicas una lista, la reordenación se hace jerárquicamente: si hay
observaciones empatadas al reordenarse según la primera variable clave,
entonces la segunda clave se utiliza para romper el empate, y así
sucesivamente hasta que se rompa el empate o se agoten las claves. Ten en
cuenta que esta acción está disponible solo para datos sin fecha.

dsortby: Funciona como sortby excepto que la reordenación se hace según
los valores descendientes de la serie clave.

resample: Construye un nuevo conjunto de datos mediante muestreo aleatorio
(con substitución) de las filas del conjunto vigente de datos, y requiere
que indiques como argumento el número concreto de filas que quieres
incluir. Este puede ser menor, igual o mayor que el número de observaciones
de los datos originales. Puedes recuperar el conjunto original de datos
mediante la instrucción smpl full.

renumber: Requiere el nombre de una serie ya existente seguida de un número
entero entre 1 y el número de series del conjunto de datos menos 1. Mueve
la serie especificada a la posición indicada del conjunto de datos,
volviendo a numerar las demás series conforme a esto. (La posición 0 se
ocupa con la constante, que no puede moverse.)

pad-daily: Válido solo cuando el conjunto vigente de datos contiene datos
con fechas diarias con un calendario incompleto. Tiene como efecto llenar
los datos en un calendario completo insertando filas en blanco (es decir,
filas que no contienen nada excepto NAs). Esta opción requiere un número
entero como parámetro, concretamente el número de días por semana (5, 6 o
7), y que debe ser mayor o igual que la frecuencia vigente de los datos.
Cuando se completa con éxito, el calendario de datos va a estar "completo"
en relación a este valor. Por ejemplo, si días-por-semana es igual a 5,
entonces se representan todos los días laborables, haya o no algún dato
disponible para esos días.

unpad-daily: Válido solo cuando el conjunto vigente de datos contiene datos
con fechas diarias, en cuyo caso esto realiza la operación inversa a
pad-daily. Es decir, se elimina cualquier fila que no contenga NAs, mientras
que se conserva la propiedad de series temporales del conjunto de datos
junto con las fechas de las observaciones individuales.

Menú gráfico: /Datos

# delete Dataset

Variantes:  delete listavariables
            delete nombrevar
            delete --type=tipo
            delete nombrepaquete
Opciones:   --db (Elimina series de la base de datos)
            --force (Mira abajo)

Esta instrucción es un destructor. Deberías utilizarla con precaución
pues no se pide confirmación.

En la primera variante de arriba, listavariables es una lista de series,
indicada por su nombre o número ID. Ten en cuenta que cuando eliminas
series, se vuelve a numerar cualquier serie cuyo número ID sea mayor que
los de las series de la lista que se elimina. Si indicas la opción --db,
las series de la lista no se eliminan con esta instrucción del conjunto
vigente de datos, pero sí de la base de datos de GRETL (suponiendo que se
abrió una de ellas y que el usuario tiene permisos para escribir en el
archivo en cuestión). Consulta también la instrucción "open".

En la segunda variante, puedes indicar el nombre de un escalar, de una
matriz, de una cadena de texto o de un bundle, para que se elimine. La
opción --db no puede aplicarse en este caso. Ten en cuenta que no debes
mezclar series y variables de diferentes tipos en una misma llamada a
delete.

En la tercera variante, la opción --type debes acompañarla con alguno de
los siguientes nombres de tipos: matrix, bundle, string, list, scalar o
array; y su efecto consiste en eliminar todas las variables del tipo
indicado. En este caso no debes indicar ningún argumento que no sea la
opción.

Puedes usar la cuarta variante para descargar un paquete de funciones. En
este caso, debes proporcionar el sufijo .gfn como en

	delete somepkg.gfn

Ten en cuenta que esto no elimina el archivo de paquete; únicamente
descarga el paquete de la memoria.

Eliminar variables en un bucle

En general, no se permite eliminar variables en el contexto de un bucle,
puesto que esto puede suponer un riesgo para la integridad del código del
propio bucle. Sin embargo, si tienes total confianza en que la eliminación
de una determinada variable va a ser inocua, puedes anular esta prohibición
añadiendo la opción --force a la instrucción delete.

Menú gráfico: Ventana principal: Menú emergente (selección única)

# diff Transformations

Argumento:  listavariables 
Ejemplos:   penngrow.inp, sw_ch12.inp, sw_ch14.inp

Con esta instrucción obtienes la primera diferencia de cada variable de
listavariables, y el resultado se guarda en una nueva variable con el
prefijo d_. Así "diff x y" genera las nuevas variables

	d_x = x(t) - x(t-1)
	d_y = y(t) - y(t-1)

Menú gráfico: /Añadir/Primeras diferencias de las variables seleccionadas

# difftest Tests

Argumentos: serie1 serie2 
Opciones:   --sign (Contraste de los signos, por defecto)
            --rank-sum (Contraste de la suma de rangos de Wilcoxon)
            --signed-rank (Contraste de los rangos con signo de Wilcoxon)
            --verbose (Presenta resultados adicionales)
            --quiet (Suprime la presentación de resultados)
Ejemplos:   ooballot.inp

Lleva a cabo un contraste no paramétrico sobre la diferencia entre dos
poblaciones o grupos, en la que el contraste concreto depende de la opción
seleccionada.

Con la opción --sign, se realiza el contraste de los signos. Este contraste
se basa en el hecho de que, cuando se extraen dos muestras, x e y, de forma
aleatoria de una misma distribución, la probabilidad de que x_i > y_i, para
cada observación i, deberá ser igual a 0.5. El estadístico de contraste
es w, es decir, el número de observaciones para las que se cumple que x_i >
y_i. Bajo la hipótesis nula, este estadístico sigue una distribución de
probabilidad Binomial con parámetros (n, 0.5), donde n indica el número de
observaciones.

Con la opción --rank-sum, se realiza el contraste de la suma de rangos de
Wilcoxon. Este contraste se desarrolla determinando el rango en jerarquía
de las observaciones de ambas muestras juntas, desde la de menor valor hasta
la de mayor, y luego calculando la suma de los rangos de las observaciones
de una cualquiera de las dos muestras. No es necesario que las dos muestras
tengan el mismo tamaño y, si son diferentes, se utiliza la muestra más
pequeña para calcular la suma de los rangos. Bajo la hipótesis nula de que
las muestras proceden de poblaciones con la misma mediana, la distribución
de probabilidad de la suma de rangos puede calcularse para cualquier tamaño
de muestra que se indique; y para muestras razonablemente largas, existe una
estrecha aproximación Normal.

Con la opción --signed-rank, se realiza el contraste de los rangos con
signo de Wilcoxon, que está ideada para pares de datos ligados como, por
ejemplo, los pares de valores de una misma variable en una muestra de
individuos, antes y después de algún tratamiento. El contraste se
desarrolla calculando las diferencias entre las observaciones emparejadas
x_i - y_i, y determinando el rango de estas diferencias según su valor
absoluto, además de asignándole a cada par, un rango con un signo que
coincide con el signo de la diferencia. A continuación se calcula la suma
de los rangos con signo positivo (W_+). De igual modo que en el contraste de
la suma de rangos, bajo la hipótesis nula de que la diferencia de las
medianas es cero, este estadístico sigue una distribución de probabilidad
bien definida, que converge a la Normal para muestras de tamaño razonable.

Para los contrastes de Wilcoxon, cuando indicas la opción --verbose,
entonces se presenta la ordenación. (Esta opción no tiene efecto cuando se
selecciona el contraste de los signos.)

Al completarse con éxito, vas a tener disponibles los accesores "$test" y
"$pvalue". Si únicamente quieres obtener estos valores, puedes añadir la
opción --quiet a la instrucción.

# discrete Transformations

Argumento:  listavariables 
Opción:     --reverse (Marca las variables como continuas)
Ejemplos:   ooballot.inp, oprobit.inp

Marca cada variable de listavariables como discreta pues, por defecto, todas
las variables se tratan como continuas. Al hacer que una variable sea
discreta, eso afecta al modo en el que se maneja esa variable en los
gráficos de frecuencia, y también te permite escoger la variable para la
instrucción "dummify".

Cuando especificas la opción --reverse, la operación se invierte; es
decir, las variables contenidas en listavariables se marcan como continuas.

Menú gráfico: /Variable/Editar atributos

# dpanel Estimation

Argumento:  p ; depvar indepvars [ ; instrumentos ] 
Opciones:   --quiet (No muestra el modelo estimado)
            --vcv (Presenta la matriz de covarianzas)
            --two-step (Realiza la estimación MGM (GMM) en 2 etapas)
            --system (Añade ecuaciones en niveles)
            --collapse (Mira abajo)
            --time-dummies (Añade variables ficticias temporales)
            --dpdstyle (Imita el paquete DPD para Ox)
            --asymptotic (Desviaciones típicas asintóticas sin corregir)
            --keep-extra (Mira abajo)
Ejemplos:   dpanel 2 ; y x1 x2
            dpanel 2 ; y x1 x2 --system
            dpanel {2 3} ; y x1 x2 ; x1
            dpanel 1 ; y x1 x2 ; x1 GMM(x2,2,3)
            Ver también bbond98.inp

Realiza la estimación de modelos dinámicos con datos de panel (es decir,
modelos de panel que incluyen uno o más retardos de la variable
dependiente) utilizando bien el método GMM-DIF o bien GMM-SYS.

El parámetro p representa el orden de autorregresión para la variable
dependiente. En el caso más sencillo, este parámetro es un valor escalar,
pero también puedes indicar una matriz definida previamente para este
argumento, para especificar con ello un conjunto de retardos (posiblemente
no consecutivos) a utilizar.

Debes indicar la variable dependiente y los regresores con sus valores en
niveles, pues ya se van a diferenciar automáticamente (dado que este
estimador utiliza la diferenciación para eliminar los efectos
individuales).

El último campo (opcional) de la instrucción es para especificar los
instrumentos. Si no indicas ningún instrumento, se asume que todas las
variables independientes son estrictamente exógenas. Si especificas
cualquier instrumento, debes incluir en la lista cualquier variable
independiente estrictamente exógena. Para los regresores predeterminados
puedes utilizar la función GMM para incluir un rango específico de
retardos con el estilo diagonal por bloques, como se ilustra en el tercer
ejemplo de arriba. El primer argumento de GMM es el nombre de la variable en
cuestión, el segundo es el retardo mínimo que se utiliza como instrumento,
y el tercero es el retardo máximo. Puedes utilizar la misma sintaxis con la
función GMMlevel para especificar instrumentos de tipo GMM para las
ecuaciones en niveles.

Puedes usar la opción --collapse para limitar la proliferación de
instrumentos de "estilo GMM", lo que podría llegar a ser un problema con
este estimador. Su efecto consiste en reducir ese tipo de instrumentos, de
uno por cada retardo y por observación, a uno por cada retardo.

Por defecto, se presentan los resultados de la estimación en 1 etapa (con
las desviaciones típicas robustas) pero tienes la opción de escoger la
estimación en 2 etapas. En ambos casos, se presentan los contrastes de
autocorrelación de orden 1 y 2 , así como los contrastes de Sargan y/o de
Hansen de sobreidentificación, y el estadístico del contraste de Wald para
la significación conjunta de los regresores. Ten en cuenta que en este
modelo en diferencias, la autocorrelación de primer orden no es una amenaza
para la validez del modelo, pero la autocorrelación de segundo orden
infringe los supuestos estadísticos vigentes.

En el caso de la estimación en 2 etapas, las desviaciones típicas se
calculan por defecto utilizando la corrección de muestra finita sugerida
por Windmeijer (2005). Generalmente se considera que las desviaciones
típicas asintóticas estándar asociadas al estimador del método en 2
etapas, son una guía poco fiable para la inferencia, pero si por alguna
razón quieres verlas, puedes utilizar la opción --asymptotic para
desactivar la corrección de Windmeijer.

Si indicas la opción --time-dummies, se añade un conjunto de variables
ficticias temporales a los regresores especificados. El número de estas
variables ficticias es una menos que el número máximo de períodos usados
en la estimación, para evitar que haya multicolinealidad perfecta con la
constante. Las variables ficticias se introducen en forma de diferencias
excepto que se indique la opción --dpdstyle, en cuyo caso se introducen en
niveles.

De igual modo que con otras instrucciones para hacer la estimación,
dispones de un 'bundle' "$model" después de realizarla. En caso de dpanel,
puedes usar la opción --keep-extra para guardar información que quieras
añadir a ese 'bundle', por ejemplo las matrices de ponderaciones y de
instrumentos GMM.

Para obtener otros detalles y ejemplos, consulta El manual de gretl
(Capítulo 24).

Menú gráfico: /Modelo/Panel/Modelo de panel dinámico

# dummify Transformations

Argumento:  listavariables 
Opciones:   --drop-first (Excluye de la codificación al valor más bajo)
            --drop-last (Excluye de la codificación al valor más alto)

Para cualquier variable adecuada de listavariables, genera un conjunto de
variables ficticias que codifican los distintos valores de esa variable. Las
variables adecuadas son aquellas que se marcan explícitamente como
discretas o aquellas que tienen un número claramente pequeño de valores,
de los que todos ellos estén "claramente redondeadados" (múltiplos de
0.25).

Por defecto, se añade una variable ficticia por cada valor diferente de la
variable en cuestión. Por ejemplo, si una variable discreta x tiene 5
valores diferentes, se añaden 5 variables ficticias al conjunto de datos,
con los nombres Dx_1, Dx_2, etcétera. La primera variable ficticia va a
tener el valor 1 en las observaciones donde x toma su valor más pequeño y
0 en otro caso; la siguiente variable ficticia va a tener el valor 1 en las
observaciones donde x toma su segundo valor más pequeño, etcétera. Si
añades uno de los indicadores de opción --drop-first o --drop-last,
entonces se omite del proceso de codificación bien el valor más bajo o
bien el valor más alto de cada variable, respectivamente (lo que puede
serte útil para evitar la "trampa de las variables ficticias").

También puedes insertar esta instrucción en el contexto de la
especificación de una regresión. Por ejemplo, la siguiente línea
especifica un modelo donde y se regresa sobre el conjunto de variables
ficticias que se codifican para x. (No puedes aplicar los indicadores de
opción a "dummify" en este contexto.)

	ols y dummify(x)

Otro acceso:  Ventana principal: Menú emergente (selección única)

# duration Estimation

Argumentos: depvar indepvars [ ; censuravar ] 
Opciones:   --exponential (Utiliza la distribución exponencial)
            --loglogistic (Utiliza la distribución log-logística)
            --lognormal (Utiliza la distribución log-normal)
            --medians (Los valores ajustados son las medianas)
            --robust (Desviaciones típicas robustas: CMV (QML))
            --cluster=clustervar (Consulta "logit" para explicación)
            --vcv (Presenta la matriz de covarianzas)
            --verbose (Presenta los detalles de las iteraciones)
            --quiet (No presenta nada)
Ejemplos:   duration y 0 x1 x2
            duration y 0 x1 x2 ; cens
            Ver también weibull.inp

Estima un modelo de duración en el que la variable dependiente (que debe
ser positiva) representa la duración de algún estado de un asunto; por
ejemplo, la duración del período de desempleo para una sección cruzada de
encuestados. Por defecto, se utiliza la distribución de Weibull pero
también están disponibles las distribuciones exponencial, log-logística y
log-normal.

Si algunas de las medidas de duración están censuradas por la derecha
(e.g. el período del desempleo de un individuo aún no acabó dentro del
período de observación), entonces debes indicar en el argumento posterior
censuravar, una serie en la que los valores no nulos indiquen los casos
censurados por la derecha.

Por defecto, los valores ajustados que obtienes mediante el accesor $yhat
son las medias condicionadas de las duraciones, pero cuando indicas la
opción --medians entonces $yhat te proporciona las medianas condicionadas
en su lugar.

Consulta El manual de gretl (Capítulo 38) para obtener más detalles.

Menú gráfico: /Modelo/Variable dependiente limitada/Datos de duración

# elif Programming

Consulta "if".

# else Programming

Consulta "if". Ten en cuenta que la instrucción "else" necesita una línea
para ella misma, antes de la siguiente instrucción condicional. Puedes
añadirle un comentario, como en

	else # Correcto, hace algo distinto

Pero no puedes añadirle una instrucción, como en

	else x = 5 # Incorrecto!

# end Programming

Termina un bloque de instrucciones de algún tipo. Por ejemplo, "end system"
termina un sistema de ecuaciones ("system").

# endif Programming

Consulta "if".

# endloop Programming

Marca el final de un bucle de instrucciones. Consulta "loop".

# eqnprint Printing

Opciones:   --complete (Genera un documento completo)
            --output=nombrearchivo (Envía el resultado al archivo especificado)

Debe ir después de la estimación de un modelo y presenta el modelo
estimado en formato de una ecuación LaTeX. Si especificas el nombre de un
archivo utilizando la opción --output, el resultado se dirige a ese
archivo; en otro caso, se dirige a un archivo con un nombre con el estilo
equation_N.tex, donde N es el número de modelos estimados hasta ese momento
en la sesión vigente. Consulta también "tabprint".

El archivo resultante va a escribirse en el directorio de trabajo
("workdir") establecido en ese momento, excepto que la cadena nombrearchivo
contenga una especificación completa de la ruta.

Cuando especificas la opción --complete, el archivo LaTeX es un documento
completo (listo para procesar); en otro caso, debes incluirlo en un
documento.

Menú gráfico: Ventana de modelo: LaTeX

# equation Estimation

Argumentos: depvar indepvars 
Ejemplo:    equation y x1 x2 x3 const

Te permite especificar una de las ecuaciones de un sistema de ellas
(consulta "system"). La sintaxis para especificar una ecuación de un
sistema SUR es la misma que para, e.g., "ols". Pero para una de las
ecuaciones de un sistema a estimar con Mínimos Cuadrados en 3 etapas,
puedes: (a) indicar una especificación de una ecuación como se estima con
MCO y proporcionar una lista normal de instrumentos utilizando la palabra
clave "instr" (de nuevo, consulta "system"), o (b) utilizar la misma
sintaxis de ecuaciones que para "tsls".

# estimate Estimation

Argumentos: [ nombresistema ] [ estimador ] 
Opciones:   --iterate (Itera hasta la convergencia)
            --no-df-corr (Sin corrección de los grados de libertad)
            --geomean (Mira abajo)
            --quiet (No presenta los resultados)
            --verbose (Presenta los detalles de las iteraciones)
Ejemplos:   estimate "Klein Model 1" method=fiml
            estimate Sys1 method=sur
            estimate Sys1 method=sur --iterate

Solicita la estimación de un sistema de ecuaciones que debes definir
previamente usando la instrucción "system". Debes indicar primero el nombre
del sistema, puesto entre comillas si el nombre contiene espacios. El
estimador debe ser uno de los siguientes: "ols", "tsls", "sur", "3sls",
"fiml" o "liml"; y debes de ponerle antes la cadena de texto method=. Estos
argumentos son opcionales si el sistema en cuestión ya se estimó, y ocupa
el lugar del "último modelo"; en ese caso, el estimador que se toma por
defecto será el utilizado previamente.

Si el sistema en cuestión tuvo aplicadas un conjunto de restricciones
(consulta la instrucción "restrict"), la estimación estará sujeta a las
restricciones especificadas.

Si el método de estimación es "sur" o "3sls", y especificas la opción
--iterate, se va a calcular el estimador iterativamente. En caso de SUR, si
el procedimiento converge, los resultados son las estimaciones máximo
verosímiles. La iteración de Mínimos Cuadrados en 3 Etapas (3sls), sin
embargo, en general no converge a los resultados de la máxima verosimilitud
con información completa (fiml). La opción --iterate se ignora para otros
métodos de estimación.

Si eliges los estimadores de ecuación a ecuación "ols" o "tsls", por
defecto se aplica una corrección de los grados de libertad cuando se
calculan las desviaciones típicas, pero puedes eliminar esto utilizando la
opción --no-df-corr. Esta opción no tiene efecto con los otros
estimadores, y así no se aplica la corrección de los grados de libertad en
ningún caso.

Por defecto, la fórmula utilizada para calcular los elementos de la matriz
de covarianzas de las ecuaciones cruzadas es

  sigma(i,j) = u(i)' * u(j) / T

Cuando indicas la opción --geomean, se aplica una corrección de los grados
de libertad con lo que la fórmula en ese caso es

  sigma(i,j) = u(i)' * u(j) / sqrt((T - ki) * (T - kj))

donde las ks denotan el número de parámetros independientes en cada
ecuación.

Cuando indicas la opción --verbose y especificas un método iterativo, se
presentan detalles de las iteraciones.

# eval Utilities

Argumento:  expresión 
Ejemplos:   eval x
            eval inv(X'X)
            eval sqrt($pi)

Esta instrucción hace que GRETL funcione como una sofisticada calculadora.
El programa evalúa expresión y presenta su valor. El argumento puede ser
el nombre de una variable, o algo más complicado. En cualquier caso, debe
ser una expresión que puedas poner correctamente como el lado derecho de un
enunciado de asignación (igualdad).

En el uso interactivo (por ejemplo con la consola de GRETL), un signo igual
funciona como una abreviatura de eval, como en

	=sqrt(x)

(con o sin espacio después de "="). Pero esta variante no se admite en el
modo de edición de guiones puesto que podría esconder fácilmente fallos
de codificación.

En la mayoría de los contextos, puedes usar la instrucción "print" en
lugar de eval para obtener el mismo efecto. Consulta también "printf" para
aquellos casos en los que quieras combinar resultados de texto y numéricos.

# fcast Prediction

Variantes:  fcast [obsinicio obsfin] [nombrev]
            fcast [obsinicio obsfin] pasosadelante [nombrev] --recursive
Opciones:   --dynamic (Genera la predicción dinámica)
            --static (Genera la predicción estática)
            --out-of-sample (Genera la predicción postmuestral)
            --no-stats (No presenta las estadísticas de predicción)
            --stats-only (Presenta solo las estadísticas de predicción)
            --quiet (No presenta nada)
            --recursive (Mira abajo)
            --all-probs (Mira abajo)
            --plot=nombrearchivo (Mira abajo)
Ejemplos:   fcast 1997:1 2001:4 f1
            fcast fit2
            fcast 2004:1 2008:3 4 rfcast --recursive
            Ver también gdp_midas.inp

Debe ir después de una instrucción de estimación. Las predicciones se
generan para cierto rango de observaciones que será, bien el definido
cuando indicas obsinicio y obsfin (de ser posible), bien el definido por las
observaciones que van a continuación del rango sobre el que se estimó el
modelo cuando indicas la opción --out-of-sample, o bien, en otro caso, el
rango de la muestra definido en ese momento. Cuando solicitas una
predicción 'out-of-sample' pero no hay disponibles observaciones
relevantes, se muestra un fallo. Dependiendo de la naturaleza del modelo,
también pueden generarse las desviaciones típicas (mira abajo). También
mira abajo para indagar sobre el efecto especial de la opción --recursive.

Si el último modelo estimado tiene una única ecuación, entonces el
argumento nombrev (opcional) tiene el siguiente efecto: no se presentan los
valores de la predicción, sino que se guardan en el conjunto de datos con
el nombre indicado. Si el último modelo es un sistema de ecuaciones,
nombrev tiene un efecto distinto ya que, en concreto, escoge una variable
endógena en particular para hacer la predicción (pues por defecto se
generan las predicciones para todas las variables endógenas). En caso de un
sistema o si no indicas nombrev, puedes recuperar los valores de predicción
utilizando el accesor "$fcast" y, si están disponibles, las desviaciones
típicas mediante "$fcse".

Predicciones estáticas y dinámicas

La elección entre una predicción estática o dinámica se aplica
únicamente en caso de modelos dinámicos, con una perturbación con un
proceso autorregresivo y/o que incluyan uno o más valores retardados de la
variable dependiente como regresores. Las predicciones estáticas son un
paso adelantadas (basadas en los valores obtenidos en el período previo),
mientras que las predicciones dinámicas emplean la regla de la cadena de
predicción. Por ejemplo, si una predicción para y en 2008 requiere como
entrada un valor de y en 2007, una predicción estática es imposible sin
datos actualizados para 2007, pero una predicción dinámica para 2008 es
posible si puedes substituir una predicción previa para y en 2007.

Por defecto se proporciona: (a) una predicción estática para alguna
porción del rango de predicción que cae dentro del rango de la muestra
sobre el que se estima el modelo, y (b) una predicción dinámica (si es
relevante) fuera de la muestra. La opción --dynamic solicita una
predicción dinámica a partir de la fecha lo más temprana posible, y la
opción --static solicita una predicción estática incluso fuera de la
muestra.

Predicciones recursivas

La opción --recursive está actualmente disponible solo para modelos de una
sola ecuación, estimados mediante MCO. Cuando indicas esta opción las
predicciones son recursivas; es decir, cada predicción se genera a partir
de una estimación del modelo indicado, utilizando los datos a partir de un
punto de inicio fijado (en concreto, el inicio del rango de la muestra para
la estimación original) hasta la fecha de predicción menos k, el número
de pasos adelantados que debes indicar en el argumento pasosadelante. Las
predicciones siempre son dinámicas si eso es pertinente. Ten en cuenta que
debes indicar el argumento pasosadelante únicamente junto con la opción
--recursive.

Modelos ordenados y modelo multinomial

Cuando la estimación es mediante logit o probit ordenados, o logit
multinomial, podrías estar interesado en las probabilidades estimadas de
cada uno de los resultados discretos, mejor que únicamente en el resultado
"más probable" para cada observación. Esto se consigue mediante la opción
--all-probs: el efecto de fcast es en ese caso una matriz con una columna
por cada resultado posible. Puedes usar el argumento vname para nombrar esa
matriz, en cuyo caso no se presenta nada. Si no indicas vname, entonces la
matriz se puede recuperar mediante "$fcast". La opción --plot no es
compatible con la opción --all-probs.

Gráficos de predicciones

La opción --plot solicita que se produzca un archivo gráfico, que contiene
una representación gráfica de la predicción. En el caso de un sistema,
esta opción solo está disponible cuando se usa el argumento nombrev para
seleccionar una única variable para hacer la predición. El sufijo del
argumento nombrearchivo de esta opción, controla el formato del gráfico:
.eps para EPS, .pdf para PDF, .png para PNG, y .plt para un archivo de
instrucciones de Gnuplot. Puedes utilizar el nombre ficticio display en
substitución del nombre de archivo para forzar la representación del
gráfico en una ventana. Por ejemplo,

	fcast --plot=fc.pdf

va a generar un gráfico con formato PDF. Se respetan los nombres de rutas
que no ofrezcan dudas; en otro caso, los archivos se escriben en el
directorio de trabajo de GRETL.

Desviaciones típicas

La naturaleza de las desviaciones típicas de las predicciones (si están
disponibles) depende de la naturaleza del modelo y de la predicción. En
modelos lineales estáticos, las desviaciones típicas se calculan
utilizando el método bosquejado por Davidson y MacKinnon (2004); ellos
incorporan tanto la incertidumbre debida al proceso de la perturbación como
la incertidumbre en los parámetros (resumida en la matriz de covarianzas de
los estimadores de los parámetros). En modelos dinámicos, las desviaciones
típicas de las predicciones se calculan únicamente en caso de una
predicción dinámica, y no incorporan la incertidumbre en los parámetros.
Para modelos no lineales, las desviaciones típicas de las predicciones no
están disponibles actualmente.

Menú gráfico: Ventana de modelo: Análisis/Predicciones

# flush Programming

Esta sencilla instrucción (sin argumentos, sin opciones) está ideada para
ser usada en guiones que llevan algo de tiempo, y que deben ejecutarse con
la Interfaz Gráfica de Usuario (GUI) de GRETL (el programa de líneas de
instrucción lo ignora), para darle al usuario un indicio visual de que las
cosas se están moviendo y GRETL no está "parado".

Generalmente, si lanzas un guion en la Interfaz Gráfica de Usuario (GUI),
no se muestra el resultado hasta que se complete su ejecución, pero el
efecto de invocar flush es como se indica a continuación:

  En la primera llamada, GRETL abre una ventana, muestra los resultados
  hasta el presente y añade el mensaje "Procesando...".

  Tras invocaciones posteriores, se actualiza el texto que se muestra en la
  ventana de resultados, y se añade un nuevo mensaje "Procesando".

Cuando se completa la ejecución del guion, cualquier resultado que quede
pendiente se descarga automáticamente en la ventana de texto.

Ten en cuenta que no tiene sentido que utilices flush en guiones que tarden
menos de (digamos) 5 segundos en ejecutarse. También ten en cuenta que no
deberías utilizar esta instrucción en un lugar del guion donde no hay
resultados posteriores que presentar, ya que el mensaje "Procesando" será
entonces engañoso para el usuario.

El siguiente código ilustra el uso que se pretende con flush:

       set echo off
       scalar n = 10
       loop i=1..n
           # Hacer una operación que lleve algo de tiempo
           loop 100 --quiet
               a = mnormal(200,200)
               b = inv(a)
           endloop
           # Presentar algunos resultados
           printf "Iteración %2d hecha\n", i
           if i < n
               flush
           endif
       endloop

# foreign Programming

Sintaxis:   foreign language=ling
Opciones:   --send-data[=lista] (Carga previamente los datos; mira abajo)
            --quiet (Elimina los resultados del programa externo)

Esta instrucción abre un modo especial en el que se admiten instrucciones
que van a ejecutarse con otro programa. Puedes salir de este modo con end
foreign y, en ese punto, se ejecutan las instrucciones acumuladas.

Actualmente los programas "externos" a los que se les da apoyo de este modo
son GNU R (language=R), Python, Julia, GNU Octave (language=Octave), Ox de
Jurgen Doornik y Stata. Los nombres de los lenguajes se reconocen en
términos que no distinguen mayúsculas y minúsculas.

Junto con R, Octave y Stata, la opción --send-data tiene como efecto el
hacer accesibles los datos del espacio de trabajo del GRETL dentro del
programa señalado. Por defecto, se envía el conjunto completo de datos,
pero puedes limitar los datos que se van a enviar indicando el nombre de una
lista de series definida previamente. Por ejemplo:

	list Rlist = x1 x2 x3
	foreign language=R --send-data=Rlist

Consulta El manual de gretl (Capítulo 44) para obtener más detalles y
ejemplos.

# fractint Statistics

Argumentos: serie [ orden ] 
Opciones:   --gph (Hace el contraste de Geweke y Porter-Hudak)
            --all (Hace ambos contrastes)
            --quiet (No presenta los resultados)

Comprueba la integración fraccional ("memoria larga") de las series
especificadas contrastando la hipótesis nula de que el orden de
integración de la serie es cero. Por defecto, se utiliza el Estimador Local
Whittle (Robinson, 1995), pero cuando indicas la opción --gph, se realiza
el contraste GPH (Geweke y Porter-Hudak, 1983) en su lugar. Cuando decidas
indicar la opción --all, entonces se van a presentar los resultados de
ambos contrastes.

Para obtener más detalles sobre este tipo de contraste, consulta Phillips y
Shimotsu (2004).

Cuando no indicas el argumento orden (opcional), el orden para el(los)
contraste(s) se establece automáticamente como el número menor entre T/2 y
T^0.6.

Los órdenes estimados de integración fraccional y sus desviaciones
típicas correspondientes están disponibles a través del accesor
"$result". Con la opción --all, encontrarás la estimación Local Whittle
en la primera fila y la estimación GPH en la segunda.

Puedes recuperar los resultados del test utilizando los accesores "$test" y
"$pvalue". Estos valores se basan en el Estimador Local Whittle excepto
cuando indicas la opción --gph.

Menú gráfico: /Variable/Contrastes de raíz unitaria/Integración fraccional

# freq Statistics

Argumento:  variable 
Opciones:   --nbins=n (Especifica el número de intervalos)
            --min=valormínimo (Especifica el mínimo, mira abajo)
            --binwidth=ancho (Especifica el ancho del intervalo, mira abajo)
            --normal (Contrasta la distribución Normal)
            --gamma (Contrasta la distribución Gamma)
            --silent (No presenta nada)
            --matrix=nombrematriz (Utiliza una columna de la matriz indicada)
            --plot=modo-o-nombrearchivo (Mira abajo)
Ejemplos:   freq x
            freq x --normal
            freq x --nbins=5
            freq x --min=0 --binwidth=0.10

Si no indicas opciones, muestra la distribución de frecuencias de la serie
variable (indicada por su nombre o número) en formato tabular, con el
número de intervalos y sus tamaños elegidos automáticamente, con o sin un
gráfico adjunto tal como se explica más abajo. Cuando se completa la
instrucción con éxito, puedes recuperar la tabla de frecuencias como una
matriz utilizando el accesor "$result".

Cuando indicas la opción --matrix, entonces variable debe ser un número
entero y se interpreta en este caso como un índice que escoge una columna
de la matriz indicada. Si la matriz en cuestión es realmente un vector
columna, puedes omitir este argumento variable.

Por defecto, la distribución de frecuencias utiliza un número de
intervalos calculado automáticamente si los datos son continuos, o no
agrupa en intervalos si los datos son discretos. Para controlar este aspecto
puedes: (a) usar la instrucción "discrete" para establecer el estatus de la
variable, o (b), si los datos son continuos, especificar o el número de
intervalos, o el valor mínimo junto con el ancho de los intervalos, como se
muestra en los dos últimos ejemplos de arriba. La opción --min establece
el límite inferior del intervalo situado más a la izquierda.

Cuando indicas la opción --normal, se calcula el estadístico chi-cuadrado
de Doornik-Hansen para contrastar la Normalidad. Cuando indicas la opción
--gamma, el contraste de Normalidad se substituye por el contraste no
paramétrico de Locke respecto a la hipótesis nula de que una variable
sigue una distribución Gamma; consulta Locke (1976), y también Shapiro y
Chen (2001). Ten en cuenta que la forma en la que se indican en GRETL los
parámetros de la distribución Gamma utilizada es (forma, escala).

Por defecto, si GRETL no está en modo de procesamiento por lotes, se
muestra un gráfico de la distribución, pero puedes ajustar esto mediante
la opción --plot. Los parámetros admisibles para esta opción son: none
(para suprimir el gráfico), display (para mostrar un gráfico incluso
cuando estés en modo de procesamiento por lotes), o un nombre de archivo.
El efecto de indicar un nombre de archivo es como se describe para la
opción --output de la instrucción "gnuplot".

La opción --silent elimina el resultado de texto habitual. Puedes utilizar
esto junto con una u otra de las opciones para contrastes de distribución;
entonces se registran el estadístico de prueba más su probabilidad
asociada, y puedes recuperarlos utilizando los accesores "$test" y
"$pvalue". También puedes usar esto junto con la opción --plot si
únicamente quieres un histograma y no te interesa mirar el texto que lo
acompaña.

Ten en cuenta que GRETL no tiene una función que se corresponda con esta
instrucción, pero resulta posible utilizar la función "aggregate" para
lograr el mismo objetivo. Además, puedes obtener la distribución de
frecuencias que se genera con la instrucción freq, en forma de matriz, por
medio del accesor "$result".

Menú gráfico: /Variable/Distribución de frecuencias

# funcerr Programming

Argumento:  [ mensaje ] 

Solo es aplicable en el contexto de una función definida por el usuario
(consulta "function"). Provoca que la ejecución de la función actual,
finalice con la señalización de una condición de fallo.

El argumento mensaje (opcional) puede tener la forma de una cadena de texto
literal, o del nombre de una variable de cadena; si está presente, se
presenta como parte del mensaje de fallo que se le muestra a quien invoca la
función.

Consulta también la función estrechamente vinculada, "errorif".

# function Programming

Argumento:  nombrefunción 

Abre un bloque de expresiones en las que se define una función. Este bloque
debe estar terminado con end function. (Como excepción está el caso en el
que desees eliminar una función definida por el usuario, pues lo puedes
conseguir mediante la sencilla linea de instrucción function foo delete
para a función chamada "foo".) Consulta El manual de gretl (Capítulo 14)
para obtener más detalles.

# garch Estimation

Argumentos: p q ; depvar [ indepvars ] 
Opciones:   --robust (Desviaciones típicas robustas)
            --verbose (Presenta los detalles de las iteraciones)
            --quiet (No presenta nada)
            --vcv (Presenta la matriz de covarianzas)
            --nc (Sin constante)
            --stdresid (Tipifica los errores)
            --fcp (Utiliza el algoritmo de Fiorentini, Calzolari y Panattoni)
            --arma-init (Parámetros iniciales de la varianza partiendo de ARMA)
Ejemplos:   garch 1 1 ; y
            garch 1 1 ; y 0 x1 x2 --robust
            Ver también garch.inp, sw_ch14.inp

Estima un modelo GARCH (GARCH, Heterocedasticidad Condicional Autorregresiva
Generalizada), bien en un modelo univariante o bien incluyendo las variables
exógenas indicadas si especificas indepvars. Los valores enteros p y q (que
puedes indicar en formato numérico o con nombres de variables escalares ya
existentes) representan los órdenes de retardo en la ecuación de varianza
condicional:

  h(t) = a(0) + sum(i=1 to q) a(i)*u(t-i)^2 + sum(j=1 to p) b(j)*h(t-j)

Así, el parámetro p representa el orden Generalizado (o "AR"), mientras
que q representa el orden normal ARCH (o "MA"). Cuando p es no nulo, q
también debe ser no nulo; en otro caso, el modelo no está identificado.
Con todo, puedes estimar un modelo ARCH normal estableciendo que q es un
valor positivo, y que p es cero. La suma de p y q no debe ser mayor que 5.
Ten en cuenta que se incluye automáticamente una constante en la ecuación
media, excepto cunado indiques la opción --nc.

Por defecto, se utiliza el propio código de GRETL para estimar los modelos
GARCH, pero también tienes la opción de usar el algoritmo de Fiorentini,
Calzolari y Panattoni (1996). El primero utiliza el maximizador BFGS
mientras que el último usa la matriz de información para maximizar la
verosimilitud, con una puesta a punto mediante la matriz Hessiana.

Con esta instrucción dispones de diversas variantes de la matriz estimada
de las covarianzas de los estimadores. Por defecto, se usa la matriz
Hessiana excepto que indiques la opción --robust, en cuyo caso se va a usar
la matriz de covarianzas CMV (QML de White). También se pueden especificar
otras posibilidades (e.g. la matriz de información o el estimador de
Bollerslev-Wooldridge) mediante la clave garch_vcv bajo la instrucción
"set".

Por defecto, las estimaciones de los parámetros de la varianza se inician
usando la varianza de la perturbación no condicionada de una estimación
inicial por MCO (para la constante) y valores positivos pequeños (para los
coeficientes que acompañan a los valores pasados tanto de las
perturbaciones cuadradas como de la varianza de la perturbación). La
opción --arma-init solicita que, para establecer los valores iniciales de
estos parámetros, se utilice un modelo inicial ARMA, explotando la
relación entre GARCH y ARMA expuesta en el capítulo 21 del libro Time
Series Analysis de Hamilton. En algunos casos, esto puede mejorar las
posibilidades de convergencia.

Puedes recuperar los errores GARCH y la varianza condicionada estimada con
$uhat y $h, respectivamente. Por ejemplo, para obtener la varianza
condicional:

	series ht = $h

Cuando indicas la opción --stdresid, se dividen los valores de $uhat por la
raíz cuadrada de h_t.

Menú gráfico: /Modelo/Series temporales univariantes/GARCH

# genr Dataset

Argumentos: nuevavariable = fórmula 

NOTA: Esta instrucción experimentó numerosos cambios y mejoras desde que
se escribió el siguiente texto de ayuda, por eso para comprender y
actualizar la información sobre esta instrucción, deberás seguir la
referencia de El manual de gretl (Capítulo 10). Por otro lado, esta ayuda
no contiene nada actualmente incorrecto, por lo que interpreta lo que sigue
como "tienes esto, y más".

Para esta instrucción y en el contexto apropiado, las expresiones series,
scalar, matrix, string, bundle y array son sinónimos.

Genera nuevas variables, habitualmente mediante transformaciones de las
variables ya existentes. Consulta también "diff", "logs", "lags", "ldiff",
"sdiff" y "square" como atajos. En el contexto de una fórmula genr, debes
hacer referencia a las variables ya existentes mediante su nombre, no con su
número ID. La fórmula debe ser una combinación bien hecha de nombres de
variables, constantes, operadores y funciones (descrito más abajo). Ten en
cuenta que puedes encontrar más detalles sobre algunos aspectos de esta
instrucción en El manual de gretl (Capítulo 10).

Una instrucción genr puede producir un resultado escalar o una serie. Por
ejemplo, la fórmula x2 = x * 2 naturalmente produce una serie cuando la
variable x es una serie, y un escalar cuando x es un escalar. Las fórmulas
x = 0 y mx = mean(x) naturalmente devuelven escalares. Bajo ciertas
circunstancias, puedes querer tener un resultado escalar ampliado a una
serie o vector; esto puedes hacerlo utilizando series como un "alias" para
la instrucción genr. Por ejemplo, series x = 0 produce una serie en la que
todos sus valores se ponen a 0. También puedes utilizar scalar como alias
de genr. No es posible forzar a un resultado en forma de vector que sea un
escalar, pero la utilización de esta palabra clave indica que el resultado
debiera de ser un escalar: si no lo es, aparece un fallo.

Cuando una fórmula produce un resultado en forma de serie, el rango sobre
el que se escribe ese resultado en la variable objetivo depende de la
configuración vigente de la muestra. Por lo tanto, puedes definir una serie
hecha a trozos utilizando la instrucción smpl junto con genr.

Se admiten los operadores aritméticos, en orden de prioridad: ^ (elevar a
la potencia); *, / y % (módulo o resto); + y -.

Los operadores booleanos disponibles son (de nuevo, en orden de prioridad):
! (negación), && (Y lógico), || (O lógico), >, <, == (igual a), >= (mayor
o igual que), <= (menor o igual que) y != (no igual). También puedes
utilizar los operadores booleanos en la construcción de variables
ficticias: por ejemplo, (x > 10) devuelve 1 en caso de que x > 10, y 0 en
otro caso.

Las constantes integradas son pi y NA. La última es el código de valor
ausente: puedes iniciar una variable con el valor ausente mediante scalar x
= NA.

La instrucción genr admite un amplio rango de funciones matemáticas y
estadísticas, incluyendo todas las habituales más varias que son
especiales de Econometría. Además, ofrece acceso a muchas variables
internas que se definen durante la ejecución de las regresiones, la
realización de contrastes de hipótesis, etcétera. Para obtener un listado
de funciones y accesores, escribe "help functions".

Además de los operadores y de las funciones indicados arriba, hay algunos
usos especiales de "genr":

  "genr time" genera una variable de tendencia temporal (1,2,3,...) llamada
  "time". Y "genr index" tiene el mismo efecto, salvo que la variable se
  llama index.

  "genr dummy" genera tantas variables ficticias como sea la periodicidad de
  los datos. En caso de tener datos trimestrales (periodicidad 4), el
  programa genera dq1 = 1 para el primer trimestre y 0 para los otros
  trimestres, dq2 = 1 para el segundo trimestre y 0 para los otros
  trimestres, etcétera. Con datos mensuales, las variables ficticias se
  nombran dm1, dm2, etcétera; con datos diarios, se nombran dd1, dd2,
  etcétera; y con otras frecuencias, los nombres son dummy_1, dummy_2, etc.

  "genr unitdum" y "genr timedum" generan conjuntos de variables ficticias
  especiales para utilizar con datos de panel, codificando las unidades de
  sección cruzada con la primera, y el período de tiempo de las
  observaciones con la segunda.

Advertencia: Con el programa en líneas de instrucción, las instrucciones
"genr" que recuperan datos relacionados con un modelo, siempre se refieren
al modelo que se estimó más recientemente. Esto también es cierto en el
programa de Interfaz Gráfica de Usuario (GUI), cuando utilizas "genr" en la
"consola de GRETL"o si introduces una fórmula usando la opción "Definir
nueva variable" bajo el menú Añadir en la ventana principal. Con la GUI,
sin embargo, tienes la opción de recuperar datos de cualquiera de los
modelos que se muestran en ese momento en una ventana (sea o no sea el
modelo estimado más recientemente). Puedes hacer esto bajo el menú
"Guardar" de la ventana del modelo correspondiente.

La variable especial obs sirve como índice para las observaciones. Por
ejemplo, series dum = (obs==15) genera una variable ficticia que tiene valor
1 para la observación 15, y el valor 0 en otro caso. También puedes usar
esta variable para escoger observaciones concretas por fecha o nombre. Por
ejemplo, series d = (obs>1986:4), series d = (obs>"2008-04-01"), o series d
= (obs=="CA"). Cuando utilizas fechas diarias o marcadores de observación
en este contexto, debes ponerlas entre comillas, pero puedes usar las fechas
trimestrales y mensuales (con dos puntos) sin comillas. Ten en cuenta que,
en caso de datos de series temporales anuales, el año no se distingue
sintácticamente de un sencillo número entero. Por lo tanto, si quieres
comparar observaciones frente a obs por año, debes usar la función obsnum
para convertir así el año en un valor índice en base 1, como se hace en
series d = (obs>obsnum(1986)).

Puedes sacar los valores escalares de una serie en el contexto de una
fórmula genr, utilizando la sintaxis varname[obs] en la que puedes indicar
el valor obs por número o fecha. Ejemplos: x[5], CPI[1996:01]. Para datos
diarios, debes usar la forma YYYY-MM-DD; e.g. ibm[1970-01-23].

Puedes modificar una observación individual de una serie mediante genr.
Para hacer esto, debes añadir un número válido de observación o de
fecha, entre corchetes, al nombre de la variable en el lado izquierdo de la
fórmula. Por ejemplo, genr x[3] = 30 o genr x[1950:04] = 303.7.

  Fórmula               Comentario
  -------                -------
  y = x1^3               x1 al cubo
  y = ln((x1+x2)/x3)     
  z = x>y                z(t) = 1 si x(t) > y(t), en otro caso 0
  y = x(-2)              x retardada 2 períodos
  y = x(+2)              x adelantada 2 períodos
  y = diff(x)            y(t) = x(t) - x(t-1)
  y = ldiff(x)           y(t) = log x(t) - log x(t-1), la tasa de crecimiento 
                         instantáneo de x
  y = sort(x)            Ordena x en orden ascendente y lo guarda en y
  y = dsort(x)           Ordena x en orden descendente
  y = int(x)             Trunca x y guarda su valor entero como y
  y = abs(x)             Guarda los valores absolutos de x
  y = sum(x)             Suma los valores de x excluyendo las entradas 
                         ausentes NA
  y = cum(x)             Acumulación: y(t) = suma desde s=1 hasta s=t de x(s) 
  aa = $ess              Establece aa igual a la Suma de Errores Cuadrados de 
                         la última regresión
  x = $coeff(sqft)       Recoge el coeficiente estimado de la variable sqft de 
                         la última regresión
  rho4 = $rho(4)         Recoge el coeficiente autorregresivo de orden 4 del 
                         último modelo (asume un modelo ar)
  cvx1x2 = $vcv(x1, x2)  Recoge la covarianza estimada de los coeficientes de 
                         las variables x1 y x2 del último modelo
  foo = uniform()        Variable pseudoaleatoria Uniforme en el rango 0-1
  bar = 3 * normal()     Variable pseudoaleatoria Normal, mu = 0, sigma = 3
  samp = ok(x)           = 1 para las observaciones donde x no está ausente

Menú gráfico: /Añadir/Definir nueva variable
Otro acceso:  Ventana principal: Menú emergente

# gmm Estimation

Opciones:   --two-step (Estimación en 2 etapas)
            --iterate (GMM iterados)
            --vcv (Presenta la matriz de covarianzas)
            --verbose (Presenta los detalles de las iteraciones)
            --quiet (No presenta nada)
            --lbfgs (Utiliza L-BFGS-B en lugar del BFGS normal)
Ejemplos:   hall_cbapm.inp

Realiza la estimación con el Método Generalizado de los Momentos (MGM o
GMM) utilizando el algoritmo BFGS (Broyden, Fletcher, Goldfarb, Shanno).
Debes especificar: (a) una o más instrucciones para actualizar las
cantidades relevantes (típicamente errores GMM), (b) uno o más conjuntos
de condiciones de ortogonalidad, (c) una matriz inicial de ponderaciones, y
(d) un listado con los parámetros a estimar, todo puesto entre las
etiquetas gmm y end gmm. Cualquier opción deberá añadirse a la línea end
gmm.

Consulta El manual de gretl (Capítulo 27) para obtener más detalles sobre
esta instrucción. Aquí simplemente lo ilustramos con un ejemplo sencillo.

	gmm e = y - X*b
	  orthog e ; W
	  weights V
	  params b
	end gmm

En el ejemplo de arriba, asumimos que tanto y como X son matrices de datos,
b es un vector de valores de los parámetros con la dimensión adecuada, W
es una matriz de instrumentos, y V es una matriz adecuada de ponderaciones.
La expresión

	orthog e ; W

indica que el vector de errores (e) es ortogonal, en principio, a cada uno
de los instrumentos que constituyen las columnas de W.

Nombres de los parámetros

Al estimar un modelo no lineal, frecuentemente resulta conveniente que
nombres los parámetros de modo conciso. Al presentar los resultados, sin
embargo, puede que desees utilizar etiquetas más informativas. Puedes
lograr esto mediante la palabra clave adicional param_names dentro del
bloque de instrucción. Para un modelo con k parámetros, el argumento que
sigue a esta palabra clave debe ser, una cadena de texto literal entre
comillas que contenga k nombres separados por espacios, el nombre de una
variable de cadena que contenga k de esos nombres, o el nombre de un array
con k cadenas de texto.

Menú gráfico: /Modelo/Variables instrumentales/GMM

# gnuplot Graphs

Argumentos: yvars xvar [ varficticia ] 
Opciones:   --with-lines[=varspec] (Utiliza líneas, no puntos)
            --with-lp[=varspec] (Utiliza líneas y puntos)
            --with-impulses[=varspec] (Utiliza barras finas verticales)
            --with-steps[=varspec] (Utiliza segmentos de líneas perpendiculares)
            --time-series (Representa frente al tiempo)
            --single-yaxis (Fuerza el uso de un único eje de ordenadas)
            --y2axis=yvar (Coloca la variable especificada en un segundo eje y)
            --ylogscale[=base] (Utiliza la escala logarítmica para el eje vertical)
            --control (Mira abajo)
            --dummy (Mira abajo)
            --fit=espajuste (Mira abajo)
            --font=espfuente (Mira abajo)
            --band=espfranja (Mira abajo)
            --matrix=nombrematriz (Representa las columnas de la matriz indicada)
            --output=nombrearchivo (Envía el resultado al archivo especificado)
            --outbuf=nombrecadena (Envía el resultado a la cadena de texto especificada)
            --input=nombrearchivo (Coge la entrada de datos desde un archivo especificado)
            --inbuf=nombrecadena (Coge la entrada de datos desde la cadena de texto especificada)
Ejemplos:   gnuplot y1 y2 x
            gnuplot x --time-series --with-lines
            gnuplot wages educ gender --dummy
            gnuplot y x --fit=quadratic
            gnuplot y1 y2 x --with-lines=y2

Las series de la lista yvars se dibujan frente a xvar. Para un gráfico de
una serie temporal puedes bien proponer time en lugar de xvar, o bien
utilizar el indicador de opción --time-series. Consulta también las
instrucciones"plot" y "panplot".

Por defecto, las posiciones de los datos se muestran con puntos, pero puedes
anular esto indicando una de las siguientes opciones: --with-lines (líneas
y puntos), --with-lp, --with-impulses o --with-steps. Cuando vas a
representar más de una variable en el eje de la y, puedes limitar el efecto
de estas opciones a un subconjunto de las variables utilizando el parámetro
varspec. Este deberá tener el formato de un listado con los nombres o
números (en ambos casos separados por comas) de las variables que se van a
representar respectivamente. Por ejemplo, en el último ejemplo de arriba se
muestra como representar y1 e y2 frente a x, de tal modo que y2 se
representa con una línea mientras y1 con puntos.

Cuando yvars contiene más de una variable, podría ser preferible utilizar
dos ejes y (a la izquierda y a la derecha). Por defecto, esto se realiza
automáticamente, mediante una técnica heurística basada en las escalas
relativas de las variables; pero puedes utilizar dos opciones (mutuamente
excluyentes) para anular lo predeterminado. Como cabría esperar, la opción
--single-yaxis evita el uso de un segundo eje, mientras que --y2axis=yvar
especifica que se represente una cierta variable (únicamente) en relación
al segundo eje.

Cuando selecciones la opción --dummy, debes indicar exactamente tres
variables: una variable y simple, una variable x y dvar, una variable
discreta. El efecto de esto consiste en representar yvar frente a xvar con
los puntos mostrados con colores diferentes dependiendo del valor de
varficticia en la observación indicada.

La opción --control es similar en el hecho de que se deben indicar
exactamente tres variables: una única variable y además de las dos
variables "explicativas", x y z. El efecto de esto es la representación de
y respecto a x, con el control de z. Esta forma de representación puede ser
útil para observar la relación entre x e y, teniendo en cuenta el efecto
que pueda tener z en ambas. Estadísticamente, esto podría ser equivalente
a una regresión de y sobre las variables x y z.

Puedes especificar que la escala del eje 'y' sea logarítmica en lugar de
lineal, utilizando la opción --ylogscale, junto con un parámetro de base.
Por ejemplo,

	gnuplot y x --ylogscale=2

representa los datos de forma que el eje vertical se expresa con potencias
de 2. Si omites la base, por defecto, se establece igual a 10.

Cogiendo datos de una matriz

En el caso básico se requieren los argumentos yvars y xvar que se refieren
a series del conjunto vigente de datos (indicados por el nombre o por el
número ID). Pero si mediante la opción --matrix, indicas una matriz ya
definida, estos argumentos se convierten en opcionales: si la matriz
especificada tiene k columnas, por defecto se tratan las primeras k - 1
columnas como las yvars y la última columna se trata como xvar. Sin
embargo, cuando indicas la opción --time-series, todas las k columnas se
representan frente al tiempo. Si quieres representar columnas escogidas de
la matriz, debes especificar yvars y xvar con el formato de números de
columna enteros positivos. Por ejemplo, si quieres un gráfico de
dispersión de la columna 2 de la matriz M frente a la columna 1, puedes
hacer:

	gnuplot 2 1 --matrix=M

Mostrar la línea del mejor ajuste

La opción --fit es solo aplicable en gráficos de dispersión de dos
variables y en gráficos de series temporales individuales. Por defecto, el
procedimiento en un gráfico de dispersión consiste en mostrar el ajuste
MCO si el coeficiente de la pendiente es significativo a un nivel del 10 por
ciento, mientras que el proceder para las series temporales es no mostrar
ninguna línea de ajuste. Puedes solicitar un comportamiento diferente
utilizando esta opción junto con alguno de los siguientes valores de los
parámetros espajuste. Ten en cuenta que si el gráfico es para una serie
temporal individual, el lugar de x lo ocupa 'time'.

  linear: Muestra el ajuste MCO lineal independientemente del nivel de
  significación estadística.

  none: No muestra ninguna línea de ajuste.

  inverse (inversa), quadratic (cuadrática), cubic (cúbica), semilog o
  linlog: Muestran una línea de ajuste basada en la regresión del tipo
  indicado. Con semilog queremos decir una regresión del logaritmo de y
  sobre x; entonces la línea ajustada representa la esperanza condicionada
  de y, obtenida por medio de la función exponencial. Con linlog se quiere
  decir una regresión de y sobre el logaritmo de x.

  loess: Muestra el ajuste de una regresión robusta localmente ponderada
  (que también se conoce a veces como "lowess").

Representando una franja

Puedes utilizar la opción --band para representar una "franja" de algún
tipo (típicamente representa un intervalo de confianza) junto con otros
datos. El modo recomendado de especificar esa franja es mediante un
‘bundle', cuyo nombre se indica como parámetro de esa opción. Un paquete
band requiere dos elementos que son necesarios: una clave center, el nombre
de una serie para el centro de la franja; y una clave width, el nombre de
una serie que represente el ancho de la franja (ambos indicados como cadenas
de texto entre comillas). Además, se admiten otros cuatro elementos
opcionales, del modo que se indica a continuación.

  En la clave factor: un escalar que indica el factor por el que se debe
  multiplicar el ancho (siendo 1 el valor por defecto).

  En la clave style : una cadena de texto para especificar cómo se
  representa la franja; y que debe ser una de entre line (línea,
  predeterminada), fill (relleno), dash (raya), bars (barra) o step
  (escalón).

  En la clave color: un color para la franja, como una cadena de texto que
  contenga el nombre de un color de Gnuplot, o como una representación de
  RGB en hexadecimal (indicada como cadena de texto o como escalar). Por
  defecto, el color se selecciona automáticamente.

  En la clave title: un título para la franja, para que aparezca en la
  clave o leyenda del gráfico. Por defecto, las franjas no tienen título.

Ten en cuenta que puedes acceder al listado de nombres de color que reconoce
Gnuplot, mediante la instrucción del propio Gnuplot "show colornames"; o
por medio de la consola de GRETL, utilizando:

	eval readfile("@gretldir/data/gnuplot/gpcolors.txt")

Aquí tienes dos ejemplos de uso, en los que se emplea la sintaxis abreviada
_() para definir un 'bundle'. El primero únicamente satisface los
requerimientos mínimos, mientras que en el segundo se practican las tres
opciones. Se está asumiendo que todas as series y, x y w están en el
conjunto de datos vigente.

	bundle b1 = _(center="x", width="w")
	gnuplot y --time-series --with-lines --band=b1
	bundle b2 = _(center="x", width="w", factor=1.96, style="fill")
	b2.color=0xcccccc
	b2.title = "Intervalo del 95%"
	gnuplot y --time-series --with-lines --band=b2

Si el gráfico va a contener dos o más de esas bandas, el indicador de
opción se debe expresar en plural, y su parámetro debe ser el nombre de un
array de paquetes (bundles), como (continuando el ejemplo anterior) en lo
que sigue:

	bundles bb = defarray(b1, b2)
	gnuplot y --time-series --with-lines --bands=bb

Cuando se representa gráficamente una matriz, en lugar de datos de series,
la única diferencia es que los elementos centro (center) y ancho (width)
del ‘bundle’ de la franja se sustituyen por un único elemento en la
clave bandmat; este debe ser el nombre entre comillas de una matriz con dos
columnas, con el centro en la columna 1 y el ancho en la columna 2.

Sintaxis heredada de franja

La sintaxis descrita más arriba fue introducida en el GRETL 2023c.
Previamente a esa edición, solo se podía especificar una franja por
gráfico y la sintaxis era un poco diferente. El enfoque antiguo (que
todavía se va a admitir hasta que se comunique lo contrario) divide los
detalles de la franja en dos opciones. Primero, la opción --band que
requiere como parámetro los nombres de dos series que indican el centro y
el ancho, separadas por una coma. El factor multiplicativo para el ancho se
puede añadir como tercer elemento separado por otra coma. Ejemplos:

	gnuplot ... --band=x,w
	gnuplot ... --band=x,w,1.96

La segunda opción, --band-style, admite uno o los dos indicadores del
estilo y del color de la franja, en ese orden y separados de nuevo por una
coma, como en estos ejemplos:

	gnuplot ... --band-style=fill
	gnuplot ... --band-style=dash,0xbbddff
	gnuplot ... --band-style=,black
	gnuplot ... --band-style=bars,blue

Barras de recesión

También puedes utilizar la opción "band" para añadir "barras de
recesión" a un gráfico. De este modo nos referimos a barras verticales que
ocupan todo el rango de la dimensión y del gráfico, y que indican la
presencia (con barra) o ausencia (sin barra) de alguna característica
cualitativa, en un gráfico de series de tiempo. Estas barras habitualmente
se utilizan para indicar períodos de recesión; pero también puedes
usarlas para señalar períodos de guerra, o cualquier cosa que pueda
codificarse con una variable ficticia 0/1.

En este contexto, la opción --band requiere un único parámetro: en la
clave dummy, un nombre entre comillas de una serie 0/1 (o el nombre entre
comillas de un vector columna adecuado, en caso de una matriz de datos). Las
barras verticales estarán "activas" para las observaciones en las que esa
serie o vector tome el valor 1 e "inactivas" cuando sea 0. Las claves de
centro (center), ancho (width), factor y estilo (style) no son pertinentes,
pero se puede usar color. Observa que solo puede usarse una de esas
especificaciones por cada gráfico. Aquí tienes un ejemplo:

	open AWM17 --quiet
	series dum = obs >= 1990:1 && obs <= 1994:2
	bundle b = _(dummy="dum", color=0xcccccc)
	gnuplot YER URX --with-lines --time-series \
	  --band=b --output=display {set key top left;}

Controlando la salida de resultados

En modo interactivo, el gráfico se muestra inmediatamente pero, en modo de
procesamiento por lotes, el procedimiento por defecto consiste en escribir
un archivo de instrucciones Gnuplot en el directorio de trabajo del usuario,
con un nombre con el patrón gpttmpN.plt, comenzando con N = 01. Puedes
generar los gráficos reales más tarde utilizando gnuplot (bajo MS Windows,
wgnuplot). Y puedes modificar este proceder utilizando la opción
--output=nombrearchivo. Esta opción controla el nombre de archivo
utilizado, y al mismo tiempo te permite especificar un formato concreto para
el resultado mediante la extensión de tres letras del nombre del archivo,
del siguiente modo: .eps da como resultado la génesis de un archivo
Encapsulated PostScript (EPS); .pdf produce un PDF; .png genera uno con
formato PNG, .emf solicita que sea EMF (Enhanced MetaFile), .fig pide que
sea un archivo Xfig, .svg que sea un SVG (Scalable Vector Graphics) y .html
es para un lienzo de HTML. Si indicas el nombre ficticio de archivo
"display", entonces el gráfico se muestra en la pantalla, como en el modo
interactivo. Y cuando indicas un nombre de archivo con cualquier extensión
diferente a las que acaban de mencionarse, se escribe un archivo de
instrucciones Gnuplot.

Un medio alternativo para dirigir la salida de resultados es con la opción
--outbuf=nombrecadena. Esto escribe las instrucciones de Gnuplot en la
cadena de texto indicada o en el "buffer". Ten en cuenta que las opciones
--output y --outbuf son mutuamente incompatibles.

Especificando una fuente

Puedes utilizar la opción --font para especificar una fuente concreta para
el gráfico. El parámetro espfuente debe tener la forma del nombre de una
fuente, seguida opcionalmente por un número que indique el tamaño en
puntos, separado del nombre por una coma o espacio, todo ello puesto entre
comillas, como en

	--font="serif,12"

Ten en cuenta que las fuentes disponibles para Gnuplot varían dependiendo
de la plataforma, y si estás escribiendo una instrucción de gráfico que
pretendes que sea portable, es mejor restringir el nombre de la fuente a las
genéricas sans o serif.

Añadiendo instrucciones Gnuplot

Dispones de una opción añadida de esta instrucción pues, a continuación
de la especificación de las variables que se van a dibujar y del indicador
de opción (si hay alguno), puedes añadir instrucciones literales de
Gnuplot para controlar la apariencia del gráfico (por ejemplo,
estableciendo el título del gráfico y/o rangos de los ejes). Estas
instrucciones deben estar puestas entre llaves, y debes terminar cada
instrucción Gnuplot con un punto y coma. Puedes utilizar una barra inversa
para continuar un conjunto de instrucciones Gnuplot a lo largo de más de
una línea. Aquí tienes un ejemplo de la sintaxis:

	{ set title 'Mi Título'; set yrange [0:1000]; }

Menú gráfico: /Ver/Gráficos
Otro acceso:  Ventana principal: Menú emergente, botón de gráficos en la barra de herramientas

# graphpg Graphs

Variantes:  graphpg add
            graphpg fontscale escala
            graphpg show
            graphpg free
            graphpg --output=nombrearchivo

La sesión "Página de gráficos" va a funcionar solo cuando tengas
instalado el sistema de composición tipográfica LaTeX, y además puedas
generar y ver un resultado PDF o PostScript.

En la ventana de iconos de la sesión, puedes arrastrar hasta 8 gráficos
sobre el icono de página de gráficos. Cuando pulses un doble clic sobre la
página de gráficos (o pulses el botón derecho y elijas "Mostrar"), se va
a componer una página que contiene los gráficos seleccionados y se va a
abrir con un visor adecuado. Desde ahí deberías poder imprimir la página.

Para vaciar la página de gráficos, pulsa el botón derecho del ratón
sobre su icono y selecciona "Vaciar".

Ten en cuenta que en sistemas diferentes a MS Windows, podrías tener que
ajustar la configuración del programa utilizado para ver archivos PDF o
PostScript. Encuéntralo bajo la pestaña "Programas" en la caja de diálogo
de las Preferencias generales de GRETL (bajo el menú Herramientas de la
ventana principal).

También es posible trabajar en la página de gráficos mediante un guion, o
utilizando la consola (en el programa de Interfaz Gráfica de Usuario, GUI).
Se le da apoyo a las siguientes instrucciones y opciones:

Para añadir un gráfico a la página de gráficos, puedes indicar la
instrucción graphpg add luego de guardar un gráfico definido, como en

	grf1 <- gnuplot Y X
	graphpg add

Para mostrar la página de gráficos: graphpg show.

Para vaciar la página de gráficos: graphpg free.

Para ajustar la escala de la fuente utilizada en la página de gráficos,
usa graphpg fontscale escala, donde escala es un múltiplo (por defecto
igual a 1.0). De este modo, para hacer que el tamaño de la fuente sea un 50
por ciento mayor que el tamaño por defecto, puedes hacer

	graphpg fontscale 1.5

Para solicitar la impresión de la página del gráfico en un archivo, usa
la opción --output= más un nombre de archivo; este nombre debería tener
la extensión ".pdf", ".ps" o ".eps". Por ejemplo:

	graphpg --output="myfile.pdf"

El archivo resultante va a escribirse en el directorio establecido en ese
momento ("workdir"), excepto que la cadena nombrearchivo contenga una
especificación completa de la ruta.

En este contexto, para el resultado se utilizan líneas de colores por
defecto; para utilizar patrones punto/raya en vez de colores, puedes añadir
la opción --monochrome.

# gridplot Graphs

Argumento:  plotspecs 
Opciones:   --fontsize=fs (Tamaño de la fuente en puntos [10])
            --width=w (Ancho del gráfico en pixels [800])
            --height=h (Altura del gráfico en pixels [600])
            --title=cadena entre comillas (Añadir un título general)
            --rows=r (Mira abajo)
            --cols=c (Mira abajo)
            --layout=lmat (Mira abajo)
            --output=destino (Mira abajo)
            --outbuf=destino alternativo (Mira abajo)
Ejemplos:   gridplot myspecs --rows=3 --output=display
            gridplot myspecs --layout=lmat --output=compuesto.pdf

Esta instrucción coge dos o más especificaciones individuales para
gráficos y las ordena en una parrilla para generar un gráfico combinado.
El único argumento requerido, plotspecs, tiene el formato de un 'array' de
cadenas de texto, cada una especificando un gráfico. Su instrucción
asociada, "gpbuild", ofrece un modo fácil de crear ese tipo de 'array'.

Especificando la parrilla

La forma de la parrilla se puede establecer por cualquiera de las tres
opciones (mutuamente incompatibles) --rows, --cols y --layout. Si no se
indica ninguna de esas opciones, el número de filas se establece como la
raíz cuadrada del número de gráficos (el tamaño del 'array' de entrada),
redondeado hacia el entero superior más próximo, de ser necesario.
Entonces, el número de columnas se establece como el número de gráficos
dividido por el número de filas, de nuevo redondeado hacia arriba, de ser
necesario. Los gráficos se colocan en la parrilla por filas, en el mismo
orden que en el 'array'. Si se indica la opción --rows, ese valor ocupa la
posición de selección automática, pero el número de columnas se
establece automáticamente como se describió antes. En cambio, si se indica
la opción --cols, el número de filas se establece de forma automática.

La opción --layout, que necesita una matriz como parámetro, ofrece una
alternativa más flexible. Esa matriz especifica la configuración de la
parrilla de este modo: los elementos 0 piden celdas vacías en la parrilla,
y los elementos enteros de 1 a n se refieren a los subgráficos en el orden
que tengan en el 'array'. Así por ejemplo,

	matrix m = {1,0,0; 2,3,0; 4,5,6}
	gridplot ... --layout=m ...

se refiere a una configuración triangular inferior de seis gráficos en una
parrilla 3 x 3. Utilizando esta opción se pueden omitir algunos
subgráficos, o incluso repetir alguno.

Opciones de salida

La opción --output puede utilizarse para indicar display (presentar el
gráfico inmediatamente) o el nombre de un archivo de salida. Como
alternativa, se puede usar la opción --outbuf para hacer una salida directa
(con el formato de un búfer de instrucciones de GNUPLOT) a la cadena de
texto mencionada. En ausencia de estas opciones, la salida es un archivo de
instrucciones de GNUPLOT nombrado de forma automática. Consulta "gnuplot"
para obtener más detalles.

# gpbuild Graphs

Argumento:  plotspecs 
Ejemplo:    gpbuild MyPlots

Esta instrucción empieza un bloque en el que cualquier instrucción o
llamada a una función que produce gráficos se trata de modo especial, con
objeto de producir un 'array' de cadenas de texto de especificación de
gráficos, para usarlas con la instrucción "gridplot": el argumento
plotspecs proporciona el nombre para ese 'array'. El bloque se acaba con la
instrucción "end gpbuild".

Dos restricciones

Dentro de un bloque gpbuild solo tienen un tratamiento especial las
instrucciones para representación gráfica; todas las otras instrucciones
se ejecutan normalmente. Únicamente hay dos restricciones que advertir.

  Las instrucciones de representación no deben incluir una especificación
  de salida en este contexto, puesto que eso estaría en conflicto con la
  redirección automática de la salida, hacia el 'array' de plotspecs. Una
  excepción a esta regla se permite para --output=display (que es bastante
  habitual como opción predeterminada en los paquetes de funciones
  relacionadas con representaciones gráficas); esta directiva se ignora de
  modo silencioso en favor del tratamiento automático.

  Los gráficos que invoquen la directiva "multiplot" de GNUPLOT no son
  adecuados para ser incluidos en un bloque gpbuild. Esto es porque gridplot
  utiliza internamente multiplot, y esas construcciones no se pueden anidar.

Alternativa manual

Se puede preparar un 'array' de especificaciones de gráficos para utilizar
con gridplot sin usar un bloque gpbuild, como en el siguiente ejemplo:

	open data4-10
	strings MyPlots = array(3)
	gnuplot ENROLL CATHOL --outbuf=MyPlots[1]
	gnuplot ENROLL INCOME --outbuf=MyPlots[2]
	gnuplot ENROLL COLLEGE --outbuf=MyPlots[3]

En esencia, lo anterior es equivalente a

	open data4-10
	gpbuild MyPlots
	   gnuplot ENROLL CATHOL
	   gnuplot ENROLL INCOME
	   gnuplot ENROLL COLLEGE
	end gpbuild

# heckit Estimation

Argumentos: depvar indepvars ; ecuaciondeseleccion 
Opciones:   --quiet (No presenta los resultados)
            --two-step (Realiza la estimación en 2 etapas)
            --vcv (Presenta la matriz de covarianzas)
            --opg (Desviaciones típicas PEG (OPG))
            --robust (Desviaciones típicas CMV (QML))
            --cluster=clustvar (Consulta "logit" para más explicaciones)
            --verbose (Presenta resultados adicionales)
Ejemplos:   heckit y 0 x1 x2 ; ys 0 x3 x4
            Ver también heckit.inp

Modelo de selección de tipo Heckman. Al especificar esta instrucción, la
lista antes del punto y coma representa las variables de la ecuación
resultante, y la segunda lista representa las variables de la ecuación de
selección. La variable dependiente de la ecuación de selección (ys en el
ejemplo de arriba) debe ser una variable binaria.

Por defecto, los parámetros se estiman por el método de máxima
verosimilitud. La matriz de covarianzas de los estimadores de los
parámetros se calcula utilizando la inversa negativa de la matriz Hessiana.
Si quieres hacer la estimación en 2 etapas, utiliza la opción --two-step.
En este caso, la matriz de covarianzas de los estimadores de los parámetros
de la ecuación resultante se ajusta de modo adecuado según Heckman (1979).

Menú gráfico: /Modelo/Variable dependiente limitada/Heckit

# help Utilities

Variantes:  help
            help functions
            help instrucción
            help función
Opción:     --func (Escoge la ayuda sobre las funciones)

Si no indicas ningún argumento, presenta la lista de instrucciones
disponibles. Si indicas el argumento simple "functions", presenta la lista
de funciones disponibles (consulta "genr").

La expresión help instrucción describe cada instrucción indicada (e.g.
help smpl). La expresión help función describe cada función indicada
(e.g. help ldet). Algunas funciones tienen los mismos nombres que las
instrucciones relacionadas (e.g. diff); en ese caso, por defecto se presenta
la ayuda para la instrucción, pero puedes obtener ayuda para la función
utilizando la opción --func.

Menú gráfico: /Ayuda

# hfplot Graphs

Argumentos: listaaltafrec [ ; listabajafrec ] 
Opciones:   --with-lines (Gráfico con líneas)
            --time-series (Pon el tiempo en el eje de abscisas)
            --output=nombrearchivo (Envía el resultado al archivo especificado)

Proporciona un medio de dibujar una serie de alta frecuencia, posiblemente
junto a una o más series observadas con la frecuencia base del conjunto de
datos. El primer argumento debe ser una "MIDAS list"; y los términos
adicionales listabajafrec (opcionales) deberán ser series habituales ("de
baja frecuencia"), después de un punto y coma.

Para obtener más detalles sobre el efecto de la opción --output, consulta
la instrucción "gnuplot".

# hsk Estimation

Argumentos: depvar indepvars 
Opciones:   --no-squares (Mira abajo)
            --vcv (Presenta la matriz de covarianzas)
            --quiet (No presenta nada)

Esta instrucción es aplicable cuando existe heterocedasticidad en forma de
una función desconocida de los regresores, que puede aproximarse por medio
de una relación cuadrática. En ese contexto, ofrece la posibilidad de
obtener desviaciones típicas consistentes y estimaciones más eficientes de
los parámetros, en comparación con MCO.

El procedimiento implica (a) la estimación MCO del modelo de interés,
seguido de (b) una regresión auxiliar para generar una estimación de la
varianza de la perturbación, y finalmente (c) mínimos cuadrados
ponderados, utilizando como ponderación la inversa de la varianza estimada.

En la regresión auxiliar de (b), se regresa el logaritmo de los errores
cuadrados de la primera estimación MCO, sobre los regresores originales y
sus cuadrados (por defecto), o solo sobre los regresores originales (si
indicas la opción --no-squares). La transformación logarítmica se realiza
para asegurar que las varianzas estimadas son todas no negativas.
Denominando u^* a los valores ajustados por esta regresión, la serie con
las ponderaciones para la estimación MCP (WLS) final se forma entonces como
1/exp(u^*).

Menú gráfico: /Modelo/Otros modelos lineales/con corrección de Heterocedasticidad

# hurst Statistics

Argumento:  serie 
Opción:     --plot=modo-o-nombrearchivo (Mira abajo)

Calcula el exponente de Hurst (una medida de persistencia o memoria larga)
para una variable de tipo serie temporal que tenga por lo menos 128
observaciones. Puedes obtener el resultado (junto con su desviación
típica) mediante el accesor "$result".

Mandelbrot (1983) discute sobre el exponente de Hurst. En términos
teóricos, este es el exponente (H) de la relación

  RS(x) = an^H

donde RS expresa el "rango que se vuelve a escalar" de la variable x en
muestras de tamaño n y a es una constante. El rango reescalado es el rango
(valor máximo menos mínimo) del valor acumulado o suma parcial de x (luego
de la substracción de su media muestral) en el período de la muestra,
dividida por la desviación típica muestral.

Como punto de referencia, si x es una variable ruido blanco (con media y
persistencia nulas) entonces el rango de su "paseo" (forma un paseo
aleatorio) acumulado y escalado por su desviación típica, tiene un
crecimiento igual a la raíz cuadrada del tamaño de la muestra,
proporcionando un exponente de Hurst esperado de 0.5. Los valores del
exponente que estén significativamente por encima de 0.5 indican
persistencia, y los menores que 0.5 indican "antipersistencia"
(autocorrelación negativa). En principio, el exponente está acotado entre
0 y 1, aunque en muestras finitas es posible obtener un exponente estimado
mayor que 1.

En GRETL, el exponente se estima utilizando submuestreo binario: se empieza
con el rango completo de datos, después con las dos mitades del rango,
después con los 4 cuartos, etcétera. Para tamaños de la muestra menores
que el rango de datos, el valor RS es la media entre las muestras
disponibles. El exponente se estima así como el coeficiente de la
pendiente, en una regresión del logaritmo de RS sobre el logaritmo del
tamaño de la muestra.

Por defecto, si GRETL no está en modo de procesamiento por lotes, se
muestra un gráfico del rango reescalado pero puedes ajustar esto mediante
la opción --plot. Los parámetros que se admiten para esta opción son none
(para suprimir el gráfico); display (para presentar un gráfico incluso en
caso de procesar por lotes); o un nombre de archivo. El efecto de indicar un
nombre de archivo es como el descrito para la opción --output de la
instrucción "gnuplot".

Menú gráfico: /Variable/Exponente de Hurst

# if Programming

Control de flujo para la ejecución de instrucciones. Se admiten 3 tipos de
construcción, como las indicadas a continuación.

	# Forma simple
	if (poner la condición)
	    instrucciones
	endif

	# Dos ramas
	if (poner la condición)
	    instrucciones 1
	else
	    instrucciones 2
	endif

	# Tres o más ramas
	if (poner la condición 1)
	    instrucciones 1
	elif (poner la condición 2)
	    instrucciones 2
	else
	    instrucciones 3
	endif

La condición ("condition") debe ser una expresión booleana; para su
sintaxis consulta "genr". Puedes incluir más de un bloque "elif". Además,
puedes anidar los bloques if ... endif.

# include Programming

Argumento:  nombrearchivo 
Opción:     --force (Fuerza a volver a leer desde el archivo)
Ejemplos:   include myfile.inp
            include sols.gfn

Ideado para utilizar en un guion de instrucciones, principalmente para
incluir definiciones de funciones. El nombre del archivo (nombrearchivo)
debería tener la extensión inp (un guion de texto plano) o gfn (un paquete
de funciones de GRETL). Las instrucciones de nombrearchivo se ejecutan y
luego el control se devuelve al guion principal.

La opción --force es específica de los archivos gfn y su efecto consiste
en forzar a GRETL a que vuelva a leer el paquete de funciones desde el
archivo, incluso aunque ya esté cargado en la memoria. (Los archivos de
texto plano inp siempre se leen y se procesan en respuesta a esta
instrucción.)

Consulta también "run".

# info Dataset

Variantes:  info
            info --to-file=nombrearchivo
            info --from-file=nombrearchivo

En su forma básica, presenta cualquier información adicional (metadatos)
guardada con el archivo de datos vigente. Alternativamente, escribe esta
información en un archivo (mediante la opción --to-file), o lee los
metadatos de un archivo especificado y los incorpora al conjunto de datos
vigente (mediante --from-file; en cuyo caso el texto debe tener un formato
UTF-8 correcto).

Menú gráfico: /Datos/Información del conjunto de datos

# intreg Estimation

Argumentos: minvar maxvar indepvars 
Opciones:   --quiet (No presenta los resultados)
            --verbose (Presenta los detalles de las iteraciones)
            --robust (Desviaciones típicas robustas)
            --opg (Mira más abajo)
            --cluster=clustvar (Consulta "logit" para más explicaciones)
Ejemplos:   intreg lo hi const x1 x2
            Ver también wtp.inp

Estima un modelo de regresión por intervalos. Este modelo surge cuando la
variable dependiente está imperfectamente observada para algunas
observaciones (posiblemente todas). En otras palabras, se asume que el
proceso generador de datos es

  y* = x b + u

pero solo observamos m <= y* <= M (el intervalo puede no tener límite por
la izquierda o por la derecha). Ten en cuenta que para algunas observaciones
m puede ser igual a M. Las variables minvar y maxvar deben contener NAs para
las observaciones sin límite por la izquierda o por la derecha,
respectivamente.

El modelo se estima mediante Máxima Verosimilitud, asumiendo la
distribución Normal del término de perturbación aleatoria.

Por defecto, las desviaciones típicas se calculan utilizando la inversa
negativa de la matriz Hessiana. Cuando especificas la opción --robust,
entonces se calculan en su lugar las desviaciones típicas CMV (QML) o de
Huber-White. En este caso, la matriz de covarianzas estimada es un
"emparedado" entre la inversa de la matriz Hessiana estimada y el producto
externo del vector gradiente. Como alternativa puedes indicar la
opción--opg, en cuyo caso las desviaciones típicas se basan únicamente en
el producto externo del vector gradiente.

Menú gráfico: /Modelo/Variable dependiente limitada/Regresión de intervalos

# johansen Tests

Argumentos: orden ylista [ ; xlista ] [ ; rxlista ] 
Opciones:   --nc (Sin constante)
            --rc (Constante restringida)
            --uc (Constante no restringida)
            --crt (Constante y tendencia restringida)
            --ct (Constante y tendencia no restringida)
            --seasonals (Incluye variables ficticias estacionales centradas)
            --asy (Guarda los valores p asintóticos)
            --quiet (Presenta solo los contrastes)
            --silent (No presenta nada)
            --verbose (Presenta detalles de las regresiones auxiliares)
Ejemplos:   johansen 2 y x
            johansen 4 y x1 x2 --verbose
            johansen 3 y x1 x2 --rc
            Ver también hamilton.inp, denmark.inp

Lleva a cabo el contraste de cointegración de Johansen entre las variables
de ylista para el orden de retardos seleccionado. Para obtener más detalles
sobre este contraste, consulta El manual de gretl (Capítulo 33) o el
capítulo 20 de Hamilton (1994). Las probabilidades asociadas (valores p) se
calculan mediante la aproximación Gamma de Doornik (Doornik, 1998). Se
muestran dos conjuntos de valores p para el contraste de la traza: valores
asintóticos directos y valores ajustados por el tamaño de la muestra. Por
defecto, el accesor "$pvalue" genera la variante ajustada, pero puedes
utilizar la opción --asy para obtener en su lugar los valores asintóticos.

La inclusión de términos determinísticos en el modelo se controla
mediante los indicadores de opción. Por defecto, si no especificas ninguna
opción, se incluye una "constante no restringida", que permite la presencia
de una ordenada en el origen no nula en las relaciones de cointegración,
así como una tendencia en los niveles de las variables endógenas. En la
literatura generada a partir del trabajo de Johansen (por ejemplo, consulta
su libro de 1995) se refiere esta situación como el "caso 3". Las 4
primeras opciones indicadas arriba, que son mutuamente excluyentes, producen
respectivamente los casos 1, 2, 4 y 5. Tanto el significado de estos casos
como el criterio para seleccionar un caso se explican en El manual de gretl
(Capítulo 33).

Las listas xlista y rxlista (opcionales) te permiten controlar las variables
exógenas especificadas, y así estas entran en el sistema bien sin
restricciones (xlista) o bien restringidas al espacio de cointegración
(rxlista). Estas listas se separan de ylista y unas de las otras mediante un
punto y coma.

La opción --seasonals, que puedes combinar con cualquiera de las otras
opciones, especifica la inclusión de un conjunto de variables ficticias
estacionales centradas. Esta opción está disponible solo para datos
trimestrales o mensuales.

La siguiente tabla se ofrece como guía para la interpretación de los
resultados del contraste que se muestran, para el caso con 3 variables. H0
denota la hipótesis nula, H1 la hipótesis alternativa, y c el número de
relaciones de cointegración.

         Rango   Contraste traza      Contraste Lmáx
                  H0     H1          H0     H1
         ---------------------------------------
          0      c = 0  c = 3       c = 0  c = 1
          1      c = 1  c = 3       c = 1  c = 2
          2      c = 2  c = 3       c = 2  c = 3
         ---------------------------------------

Consulta también la instrucción "vecm"; y la instrucción "coint" si
quieres obtener el contraste de cointegración de Engle-Granger.

Menú gráfico: /Modelo/Series temporales multivariantes

# join Dataset

Argumentos: nombrearchivo nombrevar 
Opciones:   --data=nombrecolumna (Mira abajo)
            --filter=expresión (Mira abajo)
            --ikey=claveinterna (Mira abajo)
            --okey=claveexterna (Mira abajo)
            --aggr=método (Mira abajo)
            --tkey=nombrecoluma,cadenaformato (Mira abajo)
            --verbose (Informe en marcha)

Esta instrucción incorpora una o más series desde el origen nombrearchivo
(que debe ser bien un archivo de datos con el texto delimitado, o bien un
archivo de datos "propio" de GRETL), con el nombre nombrevar. Para obtener
más detalles, consulta El manual de gretl (Capítulo 7) pues aquí damos
solo un breve resumen de las opciones disponibles. Consulta también
"append" para operaciones de anexión más simples.

Puedes utilizar la opción --data para especificar el encabezamiento de los
datos del archivo de origen, si difiere del nombre por el que los datos
debieran de conocerse en GRETL.

Puedes usar la opción --filter para especificar un criterio para filtrar
los datos de origen (es decir, para escoger un subconjunto de las
observaciones).

Puedes utilizar las opciones --ikey y --okey para especificar una
equivalencia entre las observaciones del conjunto vigente de datos y las
observaciones de la fuente de datos (por ejemplo, los individuos pueden
hacerse corresponder con el hogar al que pertenecen).

La opción --aggr se utiliza cuando la equivalencia entre las observaciones
del conjunto vigente de datos y las del origen no es de una a una.

La opción --tkey se aplica solo cuando el conjunto vigente de datos tiene
una estructura de serie temporal. Puedes usarla para especificar, bien el
nombre de una columna que contenga fechas que van a ser emparejadas con el
conjunto de datos, y/o bien el formato en el que las fechas se representan
en esa columna.

Incorporación de más de una serie al mismo tiempo

Con la instrucción "join" puedes manejar la incorporación de varias series
al mismo tiempo. Esto sucede si el argumento nombrevar: (a) consiste en una
lista de nombres separados por espacios, en lugar de un único nombre; o (b)
apunta a un 'array' de cadenas de texto, cuyos elementos deben ser los
nombres de las series que se pretende incorporar.

Sin embargo, este método tiene alguna limitación como el hecho de que la
opción --data en este caso no está disponible. Y cuando incorporas
múltiples series, estás obligado a aceptar los nombres "externos" que ya
tienen. Las demás opciones se aplican de modo uniforme a todas las series
que se incorporan mediante una instrucción concreta.

# kdplot Graphs

Argumento:  y 
Opciones:   --alt (Utiliza el kernel de Epanechnikov)
            --scale=s (Factor de ajuste del ancho de banda)
            --output=nombrearchivo (Envía el gráfico al archivo indicado)

Representa un gráfico con la estimación de la densidad del kernel para la
serie y. Por defecto, el kernel es Gaussiano pero si indicas la opción
--alt, se utiliza el kernel de Epanechnikov. Puedes ajustar el grado de
suavizado mediante la opción --scale, que tiene un valor predeterminado de
1.0 (los valores más grandes de s producen un resultado más suavizado).

La opción --output tiene como efecto el envío del resultado al archivo que
se indique en ella; utiliza la palabra "display" para forzar que el
resultado aparezca en la pantalla. Consulta la instrucción "gnuplot" para
obtener más detalles sobre esta opción.

Para obtener medios más flexibles para generar estimaciones de la densidad
del kernel, con la posibilidad de recuperar el resultado en forma de matriz,
consulta la función "kdensity".

Menú gráfico: /Variable/Gráfico de la densidad estimada

# kpss Tests

Argumentos: orden listavariables 
Opciones:   --trend (Incluye una tendencia)
            --seasonals (Incluye variables ficticias estacionales)
            --verbose (Presenta los resultados de la regresión)
            --quiet (No presenta los resultados)
            --difference (Utiliza la primera diferencia de la variable)
Ejemplos:   kpss 8 y
            kpss 4 x1 --trend

Para utilizar esta instrucción con datos de panel, consulta la sección
final de estas anotaciones.

Calcula el contraste de estacionariedad KPSS (Kwiatkowski et al, Journal of
Econometrics, 1992) para cada una de las variables indicadas (o para sus
primeras diferencias, si escoges la opción --difference). La hipótesis
nula es que la variable en cuestión es estacionaria, bien alrededor de un
nivel o, si marcas la opción --trend, alrededor de una tendencia lineal
determinística.

El argumento orden determina el tamaño de la ventana utilizada para el
suavizado de Bartlett. Cuando indicas un valor negativo, eso se toma como
señal para que se utilice una ventana automática de tamaño 4(T/100)^0.25,
donde T es el tamaño de la muestra.

Si escoges la opción --verbose, se presentan los resultados de la
regresión auxiliar junto con la varianza estimada de la componente de paseo
aleatorio de la variable.

Los puntos críticos mostrados para el estadístico de contraste se basan en
superficies de respuesta estimadas del modo establecido por Sephton
(Economics Letters, 1995), que son más fiables para muestras pequeñas que
los valores indicados en el artículo original de KPSS. Cuando el
estadístico de contraste cae entre los puntos críticos del 1 y del 10 por
ciento, se muestra una probabilidad asociada (valor p) que se obtiene
mediante interpolación lineal y no debe tomarse demasiado literalmente.
Consulta la función "kpsscrit" para ver un medio de obtener esos puntos
críticos con la ayuda del programa.

Datos de panel

Cuando se utiliza la instrucción kpss con datos de panel, para realizar un
contraste de raíz unitaria de panel, las opciones aplicables y los
resultados mostrados son algo diferentes. Mientras que en el caso habitual
de series temporales, puedes indicar una lista de variables para contrastar,
con datos de panel solo puedes contrastar una variable por cada
instrucción. Y la opción --verbose tiene un significado diferente, pues
genera una breve presentación del contraste para cada serie temporal
individual (ya que, por defecto, solo se muestra el resultado global).

Cuando es posible, se calcula el contraste global (Hipótesis nula: La serie
en cuestión es estacionaria para todas las unidades del panel) utilizando
para ello el método de Choi (Journal of International Money and Finance,
2001). Esto no siempre es sencillo pues la dificultad está en que, mientras
que el contraste de Choi se basa en las probabilidades asociadas de los
contrastes con las series individuales, no tenemos actualmente un modo de
calcular las probabilidades asociadas para el estadístico de contraste
KPSS; debemos apoyarnos en unos pocos puntos críticos.

Si el estadístico de contraste con una determinada serie, cae entre los
puntos críticos del 1 y del 10 por ciento, podemos interpolar una
probabilidad asociada. Pero si el valor del estadístico del contraste cae
por debajo del correspondiente al 10 por ciento o si excede al del 1 por
ciento, no se puede interpolar y como mucho se puede establecer un límite
sobre el contraste de Choi global. Si el valor del estadístico de contraste
individual cae por debajo del correspondiente al 10 por ciento para unas
unidades y excede al del 1 por ciento para otras, ni siquiera se puede
calcular un límite para el contraste global.

Menú gráfico: /Variable/Contrastes de raíz unitaria/Contraste KPSS

# labels Dataset

Variantes:  labels [ listavariables ]
            labels --to-file=nombrearchivo
            labels --from-file=nombrearchivo
            labels --delete
Ejemplos:   oprobit.inp

Con la primera forma, se presentan las etiquetas informativas (si existen)
de las series de listavariables, o de todas las series del conjunto de datos
cuando no especificas listavariables.

Con la opción --to-file se escriben en el archivo indicado, las etiquetas
de todas las series del conjunto de datos, una etiqueta por cada línea. Si
no hay ninguna etiqueta, se muestra un fallo; y si algunas series tienen
etiqueta y otras no, se presenta una línea en blanco para las series sin
etiqueta. El archivo resultante se va a escribir en el directorio "workdir"
vigente en ese momento, excepto que la cadena nombrearchivo contenga una
especificación completa de la ruta.

Con la opción --from-file, se lee el archivo especificado (que debe ser de
texto plano) y se asignan etiquetas a las series del conjunto de datos,
leyéndose una etiqueta por línea y usando líneas en blanco para indicar
etiquetas en blanco.

La opción --delete hace lo que cabría esperar pues elimina todas las
etiquetas de las series del conjunto de datos.

Menú gráfico: /Datos/Etiquetas de variables

# lad Estimation

Argumentos: depvar indepvars 
Opciones:   --vcv (Presenta la matriz de covarianzas)
            --no-vcv (No calcula la matriz de covarianzas)
            --quiet (No presenta nada)

Calcula una regresión que minimiza la suma de las desviaciones absolutas de
los valores ajustados respecto a los valores observados de la variable
dependiente. Las estimaciones de los coeficientes se derivan utilizando el
algoritmo del simplex de Barrodale-Roberts; y se presenta una advertencia si
la solución no es única.

Las desviaciones típicas se deducen utilizando el procedimiento 'bootstrap'
con 500 extracciones. La matriz de covarianzas de los estimadores de los
parámetros, que se presenta cuando indicas --vcv, se basa en el mismo
'bootstrap'. Puesto que esta es una operación bastante costosa, la opción
--no-vcv se proporciona para aquellos casos en los que no se necesita la
matriz de covarianzas; cuando indicas esta opción, las desviaciones
típicas no van a estar disponibles.

Ten en cuenta que este método puede resultar lento cuando la muestra es muy
larga o cuando hay muchos regresores. Por eso, en esos casos, puede ser
mejor utilizar la instrucción "quantreg". Dadas una variable dependiente y
junto con una lista X de regresores, las siguientes instrucciones son
básicamente equivalentes, con la excepción de que el método "quantreg"
utiliza el algoritmo más rápido de Frisch-Newton, y que proporciona las
desviaciones típicas analíticas en lugar de las de "bootstrapping".

	lad y const X
	quantreg 0.5 y const X

Menú gráfico: /Modelo/Estimación robusta/Mínima desviación absoluta

# lags Transformations

Argumentos: [ orden ; ] listaretardos 
Opción:     --bylag (Ordena los términos por retardo)
Ejemplos:   lags x y
            lags 12 ; x y
            lags 4 ; x1 x2 x3 --bylag
            Ver también sw_ch12.inp, sw_ch14.inp

Genera nuevas series que contienen los valores retardados de cada una de las
series de listavariables. Por defecto, el número de retardos que se crean
es igual a la periodicidad de los datos. Por ejemplo, si la periodicidad es
4 (trimestral), la instrucción "lags x" genera

	x_1 = x(t-1)
	x_2 = x(t-2)
	x_3 = x(t-3)
	x_4 = x(t-4)

Puedes controlar el número de retardos generados mediante el primer
parámetro opcional (que, si existe, debe estar seguido de un punto y coma).

La opción --bylag tiene sentido solo cuando listavariables contiene más de
una serie y el orden máximo de retardos es mayor que 1. Por defecto, se
añaden los términos retardados al conjunto de datos, por variable: primero
todos los retardos de la primera serie de la lista, después todos los
retardos de la segunda serie, etcétera. Pero cuando indicas --bylag, la
ordenación se hace por retardos: primero el retardo 1 de todas las series
de la lista, después el retardo 2 de todas as series de la lista,
etcétera.

Esta prestación también está disponible como función: consulta "lags".

Menú gráfico: /Añadir/Retardos de las variables seleccionadas

# ldiff Transformations

Argumento:  listavariables 

Se obtiene la primera diferencia del logaritmo natural de cada una de las
series de listavariables, y el resultado se guarda en una nueva serie con el
prefijo ld_. Así "ldiff x y" genera las nuevas variables

	ld_x = log(x) - log(x(-1))
	ld_y = log(y) - log(y(-1))

Menú gráfico: /Añadir/Diferencias de logaritmos de las variables seleccionadas

# leverage Tests

Opciones:   --save (Guarda las series resultantes)
            --overwrite (Conformidad para sobrescribir series ya existentes)
            --quiet (No presenta los resultados)
            --plot=Modo-o-nombrearchivo (Mira abajo)
Ejemplos:   leverage.inp

Debe ir después de una instrucción de MCO ("ols"). Calcula el
apalancamiento (h, que debe caer en el rango entre 0 y 1) para cada punto de
datos de la muestra sobre la que se estimó el modelo previo. Muestra el
error (u) para cada observación junto con su apalancamiento y una medida de
su influencia en las estimaciones, uh/(1 - h). Los "puntos de Leverage" para
los que el valor de h supera 2k/n (donde k es el número de parámetros que
se estiman y n es el tamaño de la muestra) se destacan mediante un
asterisco. Para obtener más detalles sobre los conceptos de apalancamiento
e influencia, consulta el capítulo 2 del libro de Davidson y MacKinnon
(1993).

También se calculan los valores DFFITS: estos son iguales a los Errores
tipificados (errores divididos por sus desviaciones típicas) multiplicados
por la raíz cuadrada de h(1 - h). Proporcionan una medida de la diferencia
en el ajuste de la observación i dependiendo de si esa observación está
incluida o no en la muestra de la estimación. Para más información sobre
este apartado, consulta el capítulo 12 del libro de Maddala Introduction to
Econometrics o Belsley, Kuh y Welsch (1980). Para más detalles sobre los
Errores tipificados consulta más abajo, la sección titulada Matriz
mediante accesor.

Cuando especificas la opción --save con esta instrucción, los valores de
apalancamiento, influencia y DFFITS se añaden al conjunto vigente de datos;
en este contexto, puedes utilizar la opción --quiet para eliminar la
presentación de los resultados. Los nombres por defecto de las series
guardadas son lever, influ y dffits, respectivamente. Si ya existen series
con esos nombres, lo que suceda dependerá de si indicas la opción
--overwrite, pues en ese caso se van a sobrescribir las series ya
existentes. En caso contrario, los nombres se van a ajustar para poder
garantizar la unicidad, y las nuevas series generadas serán las 3 series
con números ID más grandes del conjunto de datos.

Después de la ejecución, el accesor "$test" devuelve el criterio de
validación cruzada, que se define como la suma de las desviaciones
cuadradas de la variable dependiente con relación a sus valores de
predicción, estando la predicción para cada observación basada en una
muestra de la que se excluye esa observación. (Este es el conocido como
estimador dejar-uno-fuera). Para una discusión más amplia sobre el
criterio de validación cruzada, consulta el libro de Davidson y MacKinnon
Econometric Theory and Methods, páginas 685-686, y las referencias que
contiene.

Por defecto, si haces una llamada interactiva a esta instrucción, se
muestra un gráfico con los valores de apanlancamiento e influencia. Puedes
axustar esto mediante la opción --plot. Los parámetros que se admiten para
esta opción son none (para suprimir el gráfico), display (para mostrar un
gráfico incluso al estar en modo de guiones), o un nombre de archivo. El
efecto de indicar un nombre de archivo es como el descrito para la opción
--output de la instrucción "gnuplot".

Matriz mediante accesor

Además de la opción --save señalada antes, puedes recuperar los
resultados de esta instrucción en formato de una matriz de tres columnas
por medio del accesor "$result". Las dos primeras columnas de la mencionada
matriz contienen los valores de apalancamiento y de influencia (como con
--save), pero la tercera columna contiene los Errores tipificados, en lugar
de los valores DFFITS. Estos son errores "Tipificados externamente" o
"navajeados (jackknifed)" -- es decir, la desviación típica que está en
el divisor para la observación i utiliza la media de los cuadrados de los
errores, omitiendo esa observación. Ese tipo de error puede interpretarse
como un estadístico de prueba t para la hipótesis de que una variable
ficticia 0/1 que codifica de forma especial la observación i, tendría un
coeficiente real nulo. Para obtener más detalles sobre la discusión
adicional en torno a los Errores tipificados, consulta Chatterjee e Hadi
(1986).

Los valores DFFITS también pueden obtenerse a partir de la matriz de
$result del siguiente modo:

	R = $result
	dffits = R[,3] .* sqrt(R[,1] ./ (1-R[,1]))

O utilizando series:

	series h = $result[,1]  # Apalancamiento
	series sr = $result[,3] # Error tipificado
	series dffits = sr * sqrt(h/(1-h))

Menú gráfico: Ventana de modelo: Análisis/Observaciones influyentes

# levinlin Tests

Argumentos: orden serie 
Opciones:   --nc (Sin constante)
            --ct (Con constante y tendencia)
            --quiet (No presenta los resultados)
            --verbose (Presenta los resultados por unidad)
Ejemplos:   levinlin 0 y
            levinlin 2 y --ct
            levinlin {2,2,3,3,4,4} y

Realiza el contraste de raíz unitaria para panel descrita por Levin, Lin y
Chu (2002). La hipótesis nula es que todas las series temporales
individuales presentan una raíz unitaria, y la alternativa es que ninguna
de las series tiene una raíz unitaria. (Es decir, se asume un mismo
coeficiente común de AR(1), aunque en otros aspectos se permite que las
propiedades estadísticas de las series varíen de unos individuos a otros.)

Por defecto, las regresiones del contraste ADF incluyen una constante. Para
eliminar la constante utiliza la opción --nc y para incluirla junto con una
tendencia lineal utiliza la opción --ct. (Consulta la instrucción "adf"
para una explicación de las regresiones del ADF.)

Puedes indicar el orden de retardo con orden (no negativo) para hacer el
contraste (controlando así el número de retardos de la variable
dependiente a incluir en las regresiones del ADF) de una de estas dos
formas. Cuando indicas un valor escalar, esto se aplica a todos los
individuos del panel. La alternativa es proporcionar una matriz que contenga
un orden específico de retardos para cada individuo; esta debe ser un
vector con tantos elementos como individuos haya en el rango de la muestra
vigente. Puedes especificar esa matriz con el nombre o construirla
utilizando llaves, como se ilustró en el último ejemplo de arriba.

Cuando indicas la opción --verbose, se presentan los siguientes resultados
para cada unidad del panel: delta, el coeficiente del nivel retardado en
cada regresión ADF; s2e, la varianza estimada de las innovaciones; y s2y,
la varianza estimada a largo plazo de la serie diferenciada.

Observa que los contrastes de raíz unitaria en un panel también puedes
hacerlos utilizando las instrucciones "adf" y "kpss".

Menú gráfico: /Variable/Contrastes de raíz unitaria/Contraste Levin-Lin-Chu

# logistic Estimation

Argumentos: depvar indepvars 
Opciones:   --ymax=máximo (Especifica el máximo de la variable dependiente)
            --robust (Desviaciones típicas robustas)
            --cluster=clustvar (Consulta "logit" para una explicación)
            --vcv (Presenta la matriz de covarianzas)
            --fixed-effects (Mira abajo)
            --quiet (No presenta nada)
Ejemplos:   logistic y const x
            logistic y const x --ymax=50

Regresión logística: Lleva a cabo una regresión MCO utilizando la
transformación logística de la variable dependiente,

  log(y/(y* - y))

En caso de usar datos de panel, la especificación puede incluír los
efectos fijos individuales.

La variable dependiente debe ser estrictamente positiva. Si todos sus
valores están entre 0 y 1, por defecto se utiliza un valor de y^* (el
máximo asintótico de la variable dependiente) igual a 1; si sus valores
están entre 0 y 100, entonces y^* es 100 por defecto.

Si quieres establecer un máximo diferente, utiliza la opción --ymax. Ten
en cuenta que el valor que indiques debe ser mayor que todos los valores
observados de la variable dependiente.

Los valores ajustados y los errores de la regresión se transforman
automáticamente utilizando la inversa de la transformación logística:

  y =~ E(y* / (1 + exp(-x)))

donde x representa un valor ajustado o un error, obtenidos de la regresión
MCO que utiliza la variable dependiente logística. De este modo puedes
comparar los valores que se presentan con los de la variable dependiente
original. La aproximación es necesaria pues la transformación inversa no
es lineal, y por lo tanto la esperanza no se corresponde exactamente.

La opción --fixed-effects solo es aplicable cuando el conjunto de datos
tiene forma de panel. En ese caso, se le restan las medias de grupo de la
transformación logística de la variable dependiente, y la estimación
continúa como se hace habitualmente con efectos fijos.

Ten en cuenta que si la variable dependiente es binaria, debes utilizar en
su lugar la instrucción "logit".

Menú gráfico: /Modelo/Variable dependiente limitada/Logística
Menú gráfico: /Modelo/Panel/Logística FE

# logit Estimation

Argumentos: depvar indepvars 
Opciones:   --robust (Desviaciones típicas robustas)
            --cluster=clustvar (Desviaciones típicas agrupadas)
            --multinomial (Estima un logit multinomial)
            --vcv (Presenta la matriz de covarianzas)
            --verbose (Presenta los detalles de las iteraciones)
            --quiet (No presenta los resultados)
            --p-values (Muestra los valores p en vez de las pendientes)
            --estrella (Elige la variante pseudo-R-cuadrado)
Ejemplos:   keane.inp, oprobit.inp

Si la variable dependiente es una variable binaria (todos sus valores son 0
o 1), se obtienen estimaciones máximo verosímiles de los coeficientes de
las variables de indepvars mediante el método de Newton-Raphson. Como el
modelo es no lineal, las pendientes están condicionadas por los valores de
las variables independientes. Por defecto, se calculan las pendientes con
respecto a cada una de las variables independientes (en las medias de esas
variables), y estas pendientes substituyen los valores p habituales en el
resultado de la regresión. Puedes prescindir de este proceder indicando la
opción --p-values. El estadístico chi-cuadrado contrasta la hipótesis
nula de que todos los coeficientes son cero, excepto el de la constante.

Por defecto, las desviaciones típicas se calculan utilizando la inversa
negativa de la matriz Hessiana. Si indicas la opción --robust, entonces se
calculan en su lugar las desviaciones típicas CMV (QML) o de Huber-White.
En este caso, la matriz de covarianzas estimadas es un "emparedado" entre la
inversa de la matriz Hessiana estimada y el producto externo del vector
gradiente; consulta el capítulo 10 del libro de Davidson y MacKinnon
(2004). Pero cuando indicas la opción --cluster, entonces se generan las
desviaciones típicas "robustas por agrupación"; consulta El manual de
gretl (Capítulo 22) para obtener más detalles.

Por defecto, se va a presentar el estadístico pseudo-R-cuadrado que fue
sugerido por McFadden (1974); pero en el caso binario, si indicas la opción
--estrella se va a presentar en su lugar la variante recomendada por
Estrella (1998). Esta variante presumiblemente imita de forma más parecida
las propiedades del R^2 habitual en el contexto de la estimación de
mínimos cuadrados.

Si la variable dependiente es binaria, los coeficientes de logit van a
representar los logaritmos de los ratios de probabilidades (cocientes entre
la probabilidad de que y = 1 y la de que y = 0). En ese caso, el bundle de
$model disponible después de la estimación incluye un elemento adicional
denominado oddsratios, una matriz con cuatro columnas que contienen el
coeficiente (ratio de de probabilidades) exponenciado, más el error típico
calculado mediante el método delta, y el intervalo con el 95 por ciento de
confianza, para cada regresor. Ten en cuenta, sin embargo, que el intervalo
de confianza se calcula como el exponente del intervalo para el coeficiente
original.

Si la variable dependiente no es binaria sino discreta, entonces por defecto
se interpreta como una respuesta ordinal y se obtienen las estimaciones con
un Logit Ordenado. Sin embargo, cuando indicas la opción --multinomial, la
variable dependiente se interpreta como una respuesta sin ordenar y se
generan las estimaciones con un Logit Multinomial. (En otro caso, si la
variable escogida como dependiente no es de tipo discreto, se muestra un
fallo.) El accesor $allprobs está disponible después de la estimación,
para conseguir una matriz que contenga las probabilidades estimadas de los
posibles valores de la variable dependiente para cada observación (con las
observaciones por filas y los posibles valores por columnas).

Si quieres utilizar un Logit para el análisis de proporciones donde, para
cada observación, la variable dependiente es la proporción de casos que
tienen una determinada característica (en vez de una variable con 1 o 0
para indicar si está presente o no la característica), no debes utilizar
la instrucción "logit", sino más bien construir la variable logit, como en

	series lgt_p = log(p/(1 - p))

y utilizar esta como la variable dependiente de una regresión MCO. Consulta
el capítulo 12 de Ramanathan (2002).

Menú gráfico: /Modelo/Variable dependiente limitada/Logit

# logs Transformations

Argumento:  listavariables 

Permite obtener el logaritmo natural de cada una de las series de
listavariables y el resultado se guarda en una nueva serie con el prefijo l_
("ele" y guion bajo). Por ejemplo, "logs x y" genera las nuevas variables
l_x = ln(x) y l_y = ln(y).

Menú gráfico: /Añadir/Logaritmos de las variables seleccionadas

# loop Programming

Argumento:  control 
Opciones:   --progressive (Permite formas especiales de ciertas instrucciones)
            --verbose (Refleja las instrucciones y muestra mensajes confirmatorios)
            --decr (Mira abajo)
Ejemplos:   loop 1000
            loop i=1..10
            loop while essdiff > .00001
            loop for (r=-.99; r<=.99; r+=.01)
            loop foreach i listaX
            Ver también armaloop.inp, keane.inp

Esta instrucción abre un modo especial en el que el programa admite que las
instrucciones se ejecuten repetidas veces. Terminas el proceso de ir
introduciendo las instrucciones del bucle con "endloop" y en este punto se
ejecutan las instrucciones acumuladas.

El parámetro "control" puede tener cualquiera de las 5 formas siguientes,
tal como se muestra en los ejemplos: (a) un número entero que indica las
veces a repetir las instrucciones de un bucle; (b) un rango de valores
enteros para una variable índice; (c) la palabra "while" más una
condición booleana; (d) la palabra "for" más 3 expresiones dentro de un
paréntesis, separadas con punto y comas (que imita la orden for en el
lenguaje de programación C); o (e) la palabra "foreach" más una variable
índice y una lista.

La opción --decr es específica solo para la forma de bucle del tipo "rango
de valores enteros". Por defecto, el índice se incrementa en 1 en cada
iteración; y si el valor de inicio es menor que el valor final, el bucle no
va a funcionar. Pero cuando se proporcione un valor para --decr, el índice
se minora en 1 en cada iteración.

Consulta El manual de gretl (Capítulo 13) para obtener todos los detalles y
ejemplos. Ahí se explica el efecto de la opción --progressive (que está
diseñada para ser utilizada con simulaciones de tipo Monte Carlo). No
puedes utilizar todas las instrucciones de GRETL dentro de un bucle; por eso
las instrucciones disponibles en este contexto también se exponen ahí.

Por defecto, la ejecución de instrucciones se hace de modo más silencioso
dentro de bucles que en otros contextos. Si quieres más retroalimentación
con lo que esté sucediendo en un bucle, indica la opción --verbose.

# mahal Statistics

Argumento:  listavariables 
Opciones:   --quiet (No presenta nada)
            --save (Añade las distancias al conjunto de datos)
            --vcv (Presenta la matriz de covarianzas)

Calcula las distancias de Mahalanobis entre las series indicadas en
listavariables. La distancia de Mahalanobis es la distancia entre dos puntos
en un espacio de dimensión k, escalada por la variación estadística en
cada dimensión del espacio. Por ejemplo, si p y q son dos observaciones de
un conjunto de k variables con matriz de covarianzas C, entonces la
distancia de Mahalanobis entre las observaciones viene dada por

  sqrt((p - q)' * C-inverse * (p - q))

donde (p - q) es un vector de dimensión k. Esto se reduce a la distancia
euclidiana en caso de que la matriz de covarianzas sea una matriz identidad.

El espacio para el que se calculan las distancias está definido por las
variables seleccionadas. Para cada observación del rango vigente de la
muestra, la distancia se calcula entre la observación y el centroide de las
variables escogidas. Esta distancia es la contrapartida multidimensional de
una puntuación z estándar, y puedes utilizarla para juzgar si una
observación dada "pertenece" a un grupo de otras observaciones.

Cuando indicas la opción --vcv, se presentan tanto la matriz de covarianzas
como su inversa. Cuando indicas la opción --save, las distancias se guardan
en el conjunto de datos con el nombre mdist (o mdist1, mdist2 y así
sucesivamente, si ya existe una variable con ese nombre).

Menú gráfico: /Ver/Distancias de Mahalanobis

# makepkg Programming

Argumento:  nombrearchivo 
Opciones:   --index (Escribe el archivo índice, auxiliar)
            --translations (Escribe el archivo de cadenas de texto, auxiliar)
            --quiet (Funciona sigilosamente)

Da soporte a la creación de un paquete de funciones de GRETL mediante la
línea de instrucciones. El modo de funcionamiento de esta instrucción
depende de la extensión del nombrearchivo, que debe ser .gfn o .zip.

Modo gfn

Escribe un archivo gfn. Se asume que puede accederse a un archivo de
especificación de un paquete, que tiene el mismo nombre base que
nombrearchivo pero con la extensión .spec, junto con cualquier archivo
auxiliar al que haga referencia. También se asume que todas las funciones a
empaquetar se leyeron en la memoria.

Modo zip

Escribe un archivo comprimido zip de un paquete (un gfn más otros
elementos). En caso de encontrarse un archivo gfn con el mismo nombre base
que nombrearchivo, GRETL comprueba los archivos correspondientes inp y spec,
y si los encuentra a ambos, siendo por lo menos uno de ellos más nuevo que
el archivo gfn, entonces se vuelve a generar el gfn; en otro caso, se
utiliza el gfn existente. Cuando no se encuentra ese archivo, GRETL intenta
primero generar el gfn.

Opciones de gfn

Los indicadores de opciones admiten la escritura de archivos auxiliares,
pensados para utilizar con los "añadidos" de GRETL. El archivo índice es
un corto documento XML que contiene información básica sobre el paquete, y
que tiene su mismo nombre como base además de la extensión .xml. El
archivo de traducciones contiene las cadenas de texto del paquete (en
formato C) que podrían ser apropiadas para la traducción; para un paquete
foo este archivo se llama foo-i18n.c. Estos archivos no se generan si la
instrucción opera en modo zip, y se utiliza un archivo gfn que ya existía.

Para obtener más detalles sobre todo esto, consulta la Guía para paquetes
de funciones de GRETL.

Menú gráfico: /Archivo/Paquetes de funciones/Paquete nuevo

# markers Dataset

Variantes:  markers --to-file=nombrearchivo
            markers --from-file=nombrearchivo
            markers --to-array=nombre
            markers --from-array=nombre
            markers --from-series=nombre
            markers --delete

Las opciones --to-file y --to-array proporcionan formas de guardar las
cadenas de texto que son marcadores de observaciones del conjunto vigente de
datos, en el archivo o 'array' que indiques. Si no hay ninguna de esas
cadenas, se muestra un fallo. En el caso del archivo, las cadenas se
escriben una por línea en ese archivo, y este se guarda en el directorio
("workdir") establecido en ese momento, excepto que la cadena nombrearchivo
contenga una especificación completa de la ruta. En el caso del 'array', si
nombre es el identificador de un 'array' de cadenas de texto ya existente,
ese 'array' se va a sobrescribir; en caso contrario, se crea uno nuevo.

Con la opción --from-file, se lee el archivo especificado (que debe ser de
texto UTF-8) y se asignan las etiquetas de observación contenidas en este,
uno por cada línea, a las filas del conjunto de datos. En general, debería
haber como mínimo tantas etiquetas en el archivo como observaciones en el
conjunto de datos; pero si el conjunto de datos es de tipo panel, también
se acepta que el número de etiquetas en el archivo coincida con el número
de unidades de sección cruzada (en cuyo caso las etiquetas se repiten para
cada período de tiempo.) La opción --from-array funciona de modo similar,
realizando la lectura a partir de un 'array' de cadenas de texto
determinado.

La opción --from-series ofrece un modo adecuado de crear etiquetas de
observación, copiándolas de una serie con valores en forma de cadenas de
texto. Se presenta un fallo cuando la serie indicada no tiene valores de
cadena de texto.

La opción --delete hace lo que ya esperarías, es decir, eliminar las
cadenas de texto que etiquetan cada observación del conjunto de datos.

Menú gráfico: /Datos/Etiquetas de las observaciones

# meantest Tests

Argumentos: serie1 serie2 
Opción:     --unequal-vars (Asume que las varianzas no son iguales)

Calcula el estadístico t para contrastar la hipótesis nula de que las
medias en la población son iguales para las variables serie1 y serie2, y
muestra su probabilidad asociada (valor p).

Por defecto, el estadístico de contraste se calcula bajo el supuesto de que
las varianzas son iguales para las dos variables. Con la opción
--unequal-vars se asume que las varianzas son diferentes; y en este caso,
los grados de libertad del estadístico de contraste se aproximan conforme a
Satterthwaite (1946).

Menú gráfico: /Herramientas/Calculadora de estadísticos de contraste

# midasreg Estimation

Argumentos: depvar indepvars ; términosMIDAS 
Opciones:   --vcv (Presenta la matriz de covarianzas)
            --robust (Desviaciones típicas robustas)
            --quiet (No presenta los resultados)
            --levenberg (Mira abajo)
Ejemplos:   midasreg y 0 y(-1) ; mds(X, 1, 9, 1, theta)
            midasreg y 0 y(-1) ; mds(X, 1, 9, 0)
            midasreg y 0 y(-1) ; mdsl(XL, 2, theta)
            Ver también gdp_midas.inp

Lleva a cabo la estimación por mínimos cuadrados (bien MCNL o bien MCO,
dependiendo de la especificación) de un modelo MIDAS (Mixed Data Sampling).
Este tipo de modelos incluye una o más variables independientes que se
observan con una frecuencia mayor que a variable dependiente; para una buena
y breve introducción consulta Armesto, Engemann y Owyang (2010).

Las variables de indepvars deben tener la misma frecuencia que la variable
dependiente. Esta lista normalmente debe incluir const o 0 (ordenada en el
origen), y habitualmente incluye uno o más retardos de la variable
dependiente. Los términos de alta frecuencia se indican después de un
punto y coma; cada uno tiene el formato de unos cuantos argumentos entre
paréntesis, separados por comas, precedidos por mds o por mdsl.

mds: Esta variante generalmente requiere 5 argumentos, del modo siguiente:
el nombre de una "MIDAS list", dos enteros que indican los retardos mínimo
y máximo de alta frecuencia, un entero entre 0 y 4 (o una cadena de texto,
mira abajo) que especifica el tipo de disposición de los parámetros que se
va a usar, y el nombre de un vector que contiene los valores iniciales de
los parámetros. El ejemplo de abajo solicita los retardos del 3 al 11 de
las series de alta frecuencia representadas en la lista X, utilizando para
ello una disposición de los parámetros de tipo 1 (Almon exponencial, mira
abajo) con el vector de inicio theta.

	mds(X, 3, 11, 1, theta)

mdsl: Generalmente requiere 3 argumentos: el nombre de una lista de retardos
MIDAS, un número entero (o una cadena de texto, mira abajo) para
especificar el tipo de disposición de los parámetros y el nombre de un
vector de inicio. En este caso, los retardos máximo y mínimo están
implícitos en el argumento inicial de la lista. En el ejemplo de abajo
Xlags debe ser una lista que ya contenga todos los retardos que se
necesiten; puedes construir una lista de ese tipo utilizando la función
"hflags".

	mdsl(XLags, 1, theta)

Los tipos de disposición de parámetros que se admiten, se muestran abajo.
En el contexto de las especificaciones mds y mdsl, puedes indicarlos en
forma de los códigos numéricos, o de las cadenas de texto entre comillas
que se muestran después de los números:

0 o "umidas": MIDAS sin restricciones o U-MIDAS, en el que cada retardo
tiene su propio coeficiente.

1 o "nealmon": Almon exponencial normalizada, que requiere por lo menos un
parámetro y habitualmente utiliza dos.

2 o "beta0": Beta normalizada con un último retardo nulo, que requiere
exactamente dos parámetros.

3 o "betan": Beta normalizada con un último retardo no nulo, que requiere
exactamente tres parámetros.

4 o "almonp": Polinomio de Almon (no normalizada), que requiere por lo menos
un parámetro.

5 or "beta1": Similar a beta0, pero con el primer parámetro fijado en 1
(dejando un único parámetro libre).

Cuando la disposición de parámetros es U-MIDAS, no es necesario el vector
de inicio del último argumento. En otros casos, puedes solicitar un inicio
automático substituyendo el nombre del vector de parámetros inicial por
alguna de estas dos formas:

  La palabra clave null: esto solo es admisible cuando la disposición de
  los parámetros tiene un número fijo de términos (los casos Beta, con 2
  o 3 parámetros). También se acepta en el caso del Almon exponencial, lo
  que implica que ese es el valor por defecto de los dos parámetros.

  Un valor entero que indica el número requerido de parámetros.

El método de estimación que utiliza esta instrucción depende de la
especificación de los elementos de alta frecuencia. En el caso de U-MIDAS,
el método es MCO (OLS); de lo contario, es mínimos cuadrados no lineales
(MCNL o NLS). Cuando especificas las disposiciones de parámetros Almon
exponencial normalizada o Beta normalizada, el método MCNL por defecto es
una combinación de BFGS restringido y MCO, pero puedes indicar la opción
--levenberg para forzar que se utilice el algoritmo de Levenberg-Marquardt.

Menú gráfico: /Modelo/Series temporales univariantes/MIDAS

# mle Estimation

Argumentos: función logaritmo-verosimilitud [ derivadas ] 
Opciones:   --quiet (No muestra el modelo estimado)
            --vcv (Presenta la matriz de covarianzas)
            --hessian (Basa la matriz de covarianzas en la Hessiana)
            --robust[=hac] (Matriz de covarianzas CMV (QML) o HAC)
            --cluster=clustvar (Matriz de covarianzas robusta por agrupación)
            --verbose (Presenta los detalles de las iteraciones)
            --no-gradient-check (Mira abajo)
            --auxiliary (Mira abajo)
            --lbfgs (Utiliza L-BFGS-B en vez del BFGS habitual)
Ejemplos:   weibull.inp, biprobit_via_ghk.inp, frontier.inp, keane.inp

Realiza la estimación de Máxima Verosimilitud (MV o ML) utilizando bien el
algoritmo BFGS (Broyden, Fletcher, Goldfarb, Shanno) o bien el método de
Newton. Debes especificar la función logaritmo de verosimilitud. Y debes
expresar los parámetros de esta función, y asignarles valores iniciales
antes de la estimación. Opcionalmente, el usuario puede especificar las
derivadas de la función logaritmo de verosimilitud con respecto a cada uno
de los parámetros; si no indicas las derivadas analíticas, se calcula una
aproximación numérica.

Este texto de ayuda asume que se utiliza, por defecto, el maximizador BFGS.
Para obtener más información sobre el uso del método de Newton, por favor
consulta El manual de gretl (Capítulo 26).

Ejemplo sencillo: Supón que tenemos una serie X con valores 0 o 1, y
queremos obtener la estimación máximo verosímil de la probabilidad (p) de
que X = 1. (En este caso sencillo, se puede adelantar que la estimación MV
de p será simplemente equivalente a la proporción de Xs iguales a 1, en la
muestra.)

Primero se debe añadir el parámetro p al conjunto de datos, e indicar su
valor inicial. Por ejemplo, scalar p = 0.5.

A continuación, se configura el bloque de instrucciones de estimación EMV:

	mle loglik = X*log(p) + (1-X)*log(1-p)
	  deriv p = X/p - (1-X)/(1-p)
	end mle

La primera línea de arriba especifica la función logaritmo de
verosimilitud. Comienza con la palabra clave mle, después se especifica la
variable dependiente y se indica una expresión para el logaritmo de la
verosimilitud (usando la misma sintaxis que en la instrucción "genr"). La
siguiente línea (que es opcional) comienza con la palabra clave deriv y
proporciona la derivada de la función logaritmo de verosimilitud con
respecto al parámetro p. Si no indicas las derivadas, debes incluir una
orden utilizando la palabra clave params que identifique los parámetros
libres: estos se enumeran en una línea, separados por espacios y pueden ser
bien escalares, bien vectores, o bien cualquier combinación de los dos. Por
ejemplo, puedes cambiar lo de arriba por:

	mle loglik = X*log(p) + (1-X)*log(1-p)
	  params p
	end mle

en cuyo caso se utilizarían derivadas numéricas.

Ten en cuenta que cualquier indicador de opción debe añadirse a la línea
final del bloque EMV (MLE). Por ejemplo:

	mle loglik = X*log(p) + (1-X)*log(1-p)
	  params p
	end mle --quiet

Matriz de covarianzas y desviaciones típicas

Cuando la función del logaritmo de la verosimilitud devuelve una serie o un
vector que proporciona valores por observación, entonces las desviaciones
típicas estimadas se basan por defecto en el Producto Externo del vector
Gradiente (PEG); mientras que si indicas la opción --hessian, por el
contario se basan en la inversa negativa de la matriz Hessiana, que se
aproxima numéricamente. Cuando indicas la opción --robust, se utiliza un
estimador CMV (QML, un "emparedado" entre la inversa negativa de la matriz
Hessiana y el PEG). Si además añades el parámetro hac a esta opción, el
PEG se incrementa del modo de Newey y West para permitir autocorrelación
del gradiente. (Esto únicamente tiene sentido con datos de series de
tiempo.) Ahora bien, cuando la función del logaritmo de la verosimilitud
únicamente devuelve un valor escalar, el PEG no está disponible (por lo
tanto tampoco el estimador CMV), y las desviaciones típicas tienen que
calcularse necesariamente utilizando la matriz Hessiana numérica.

En caso de que únicamente quieras las estimaciones del parámetro primario,
puedes indicar la opción --auxiliary, que elimina el cálculo de la matriz
de covarianzas y de las desviaciones típicas. Esto va a ahorrar algunos
ciclos de CPU y uso de memoria.

Comprobando las derivadas analíticas

Si proporcionas las derivadas analíticas, por defecto GRETL ejecuta una
verificación numérica de su credibilidad. Algunas veces esto puede
producir falsos positivos, por situaciones en las que las derivadas
correctas parecen ser incorrectas y la estimación se rechaza. Para tener
esto en cuenta o para conseguir un poco de velocidad adicional, puedes
indicar la opción --no-gradient-check. Obviamente, debes hacer esto solo
cuando tengas certeza de que el vector gradiente que has especificado es
correcto.

Nombres de parámetros

Al estimar un modelo no lineal, con frecuencia es conveniente nombrar los
parámetros de forma sucinta. Ahora bien, al presentar los resultados, puede
que desees utilizar etiquetas más informativas. Esto lo puedes conseguir
mediante la palabra clave adicional param_names dentro del bloque de
instrucciones. Para un modelo con k parámetros, el argumento que sigue a
esta palabra clave debe ser una cadena de texto literal entre comillas que
contenga k nombres separados por espacios, el nombre de una variable de
cadena que contenga k de esos nombres, o el nombre de un array con k cadenas
de texto.

Para una descripción más en profundidad de la estimación "mle" consulta
El manual de gretl (Capítulo 26).

Menú gráfico: /Modelo/Máxima Verosimilitud

# modeltab Utilities

Variantes:  modeltab add
            modeltab show
            modeltab free
            modeltab --output=nombrearchivo
            modeltab --options=bundle

Permite manejar la "Tabla de modelos" de GRETL; consulta El manual de gretl
(Capítulo 3) para obtener más detalles. Las instrucciones subordinadas
tienen los siguientes efectos: "add" añade el último modelo estimado a la
tabla de modelos, cuando sea posible; "show" muestra la tabla de modelos en
una ventana; y "free" vacía la tabla.

Para solicitar que se guarde la tabla de modelos, usa la opción --output=
más un nombre de archivo. Cuando el nombre del archivo tenga el sufijo
".tex", el resultado va a estar en formato TeX; cuando el sufijo sea ".rtf",
el resultado tendrá formato RTF; y en otro caso, va a estar en texto plano.
En caso de un resultado TeX, por defecto se genera un "trozo" adecuado para
incluir en un documento; en cambio, si quieres un documento independiente,
usa la opción --complete, como por ejemplo

	modeltab --output="myfile.tex" --complete

Puedes utilizar el indicador --options= (lo que requiere indicar el nombre
de un 'bundle' de GRETL) para controlar algunos aspectos de formato en la
tabla del modelo. Se admiten las siguientes claves:

  colheads: Entero de 1 a 4, que permite escoger entre los cuatro estilos
  admitidos para el encabezamiento de columnas: numeración Arábiga,
  numeración Romana, alfabética, o la utilización de los nombres bajo los
  que se guardaron los modelos. El predeterminado es 1 (numeración
  Arábiga).

  tstats: Booleano, que permite sustituir las desviaciones típicas con los
  estadísticos t, o no (el predeterminado, 0).

  pvalues: Booleano, que permite incluir los valores P, o no (el
  predeterminado, 0).

  asterisks: Booleano, que permite mostrar asteriscos relativos al nivel de
  significación, o no (el predeterminado, 0).

  digits: Entero de 2 a 6, que permite elegir el número de dígitos
  significativos que se muestran (el predeterminado, 4).

  decplaces: Entero de 2 a 6, que permite elegir el número de posiciones
  decimales que se muestran.

Ten en cuenta que las dos últimas claves son mutuamente excluyentes.
Ofrecen modos alternativos de especificar la precisión con la que se
muestran los valores numéricos: o en términos de dígitos significativos,
o en términos de posiciones decimales. Por defecto, son 4 dígitos
significativos.

Puedes indicar un 'bundle' de opciones mediante una instrucción
independiente (como en el último de los ejemplos de más abajo), o se puede
combinar con el proceso show, o con la opción --output. Por ejemplo, el
siguiente guion elabora una simple tabla de un modelo y la presenta,
mostrando los valores P en lugar de los asteriscos relativos al nivel de
significación:

	open data9-7
	ols 1 0 2 3 4
	modeltab add
	ols 1 0 2 3
	modeltab add
	bundle myopts = _(pvalues=1, asterisks=0)
	modeltab show --options=myopts

Menú gráfico: Ventana de iconos de sesión: icono de Tabla de modelos

# modprint Printing

Argumentos: matrizcoef nombres [ estadicionales ] 
Opción:     --output=nombrearchivo (Envía el resultado al archivo especificado)

Presenta la tabla de coeficientes y estadísticos adicionales opcionales
para un modelo estimado "a mano"; es útil sobre todo para funciones
escritas por el usuario.

El argumento matrizcoef debe ser una matriz de dimensión k por 2, que
contiene k coeficientes y k desviaciones típicas asociadas. El argumento
nombres debe proporcionar por lo menos k nombres para etiquetar los
coeficientes. Puedes indicarlo con el formato: (a) de una cadena de texto
literal (puesta entre comillas) o de una variable de cadena, que contenga
los nombres separados por comas o espacios, o (b) un array ya definido de
cadenas de texto.

El argumento estadicionales (opcional) es un vector que contiene p
estadísticos adicionales que se muestran debajo de la tabla de
coeficientes. Si indicas este argumento, entonces nombres debe contener k +
p nombres, de forma que los p nombres agregados se asocien a los
estadísticos adicionales.

Si no indicas el argumento estadicionales y la matriz matrizcoef tiene
adjuntos los nombres de las filas, entonces puedes omitir el argumento
nombres.

Para colocar el resultado en un archivo, utiliza la opción --output= más
un nombre de archivo. Cuando el nombre de archivo tenga el sufijo ".tex", el
resultado va a estar en formato TeX; cuando el sufijo sea ".rtf", el
resultado tendrá formato RTF; y en otro caso, va a estar en texto plano. En
caso de un resultado TeX, por defecto se genera un "trozo" adecuado para
incluir en un documento; en cambio, si quieres un documento independiente,
usa la opción --complete.

El archivo resultante se escribe en el directorio ("workdir") establecido en
ese momento, excepto que la cadena nombrearchivo contenga una
especificación completa de la ruta.

# modtest Tests

Argumento:  [ orden ] 
Opciones:   --normality (Normalidad de las perturbaciones)
            --logs (No linealidad: logaritmos)
            --squares (No linealidad: cuadrados)
            --autocorr (Autocorrelación)
            --arch (ARCH)
            --white (Heterocedasticidad: contraste de White)
            --white-nocross (Contraste de White: solo cuadrados)
            --breusch-pagan (Heterocedasticidad: contraste de Breusch-Pagan)
            --robust (Estimación con varianzas robustas para Breusch-Pagan)
            --panel (Heterocedasticidad: por grupos)
            --comfac (Restricción de factor común: solo modelos AR1)
            --xdepend (Dependencia de sección cruzada: solo con datos de panel)
            --quiet (No presenta los detalles)
            --silent (No presenta nada)
Ejemplos:   credscore.inp

Debe seguir inmediatamente a una instrucción de estimación. La discusión
de abajo se aplica a la utilización de esta instrucción a continuación de
la estimación de un modelo de una única ecuación; consulta El manual de
gretl (Capítulo 32) para una exposición de como opera "modtest" después
de la estimación de un VAR.

Dependiendo de la opción que indiques, esta instrucción efectúa una de
estas acciones: el contraste de Normalidad de la perturbación de
Doornik-Hansen; un contraste de No Linealidad (logaritmos o cuadrados) con
Multiplicadores de Lagrange; el contraste de Heterocedasticidad de White
(con o sin productos cruzados) o el de Breusch-Pagan (Breusch y Pagan,
1979); el contraste LMF de Autocorrelación (Kiviet, 1986); un contraste de
ARCH (Heterocedasticidad Condicional Autorregresiva; consulta también la
instrucción "arch"); un contraste de la restricción de Factor Común
implícita en la estimación AR(1); o un contraste de Dependencia de
sección cruzada en modelos con datos de panel. Con la excepción de los
contrastes de Normalidad, de Factor Común y de Dependencia de sección
cruzada, la mayor parte de las opciones de estos contrastes solo están
disponibles para modelos estimados mediante MCO, pero mira más abajo para
obtener más detalles en relación con Mínimos Cuadrados en 2 Etapas.

El argumento orden (opcional) es importante solo en caso de que escojas las
opciones --autocorr o --arch. Por defecto, estos contrastes se ejecutan
utilizando un orden de retardos igual a la periodicidad de los datos, pero
puedes ajustar esto indicando un orden de retardos específico.

La opción --robust se aplica únicamente cuando seleccionas el contraste de
Breusch-Pagan; su efecto consiste en que se utiliza el estimador robusto de
la varianza propuesto por Koenker (1981), haciendo el contraste menos
sensible al supuesto de Normalidad.

La opción --panel está disponible solo cuando el modelo se estima con
datos de panel; y en este caso, se realiza un contraste de
heterocedasticidad por grupos (es decir, de varianzas de las perturbaciones
diferentes entre las unidades de sección cruzada).

La opción --comfac está disponible solo cuando el modelo se estima
mediante un método AR(1) tal como el de Hildreth-Lu. La regresión auxiliar
toma la forma de un modelo dinámico relativamente no restringido, que se
utiliza para contrastar la restricción de factor común implícita en la
especificación AR(1).

La opción --xdepend está disponible solo para modelos estimados con datos
de panel. El estadístico de contraste es el desarrollado por Pesaran
(2004). La hipótesis nula es que la perturbación se distribuye
independientemente entre las unidades atemporales o los individuos.

Por defecto, el programa presenta la regresión auxiliar en la que se basa
el estadístico de contraste, si es aplicable. Puedes eliminar esto
utilizando la opción --quiet (presentación mínima de resultados) o la
opción --silent (no presenta ningún resultado). Puedes recuperar el
estadístico de contraste y su probabilidad asociada (valor p) utilizando
los accesores "$test" y "$pvalue", respectivamente.

Cuando un modelo se estima por Mínimos Cuadrados en 2 Etapas (consulta
"tsls"), se rompe el principio de Máxima Verosimilitud y GRETL ofrece
algunos equivalentes: la opción --autocorr calcula el estadístico de
Godfrey para contrastar autocorrelación (Godfrey, 1994) mientras que la
opción --white produce el estadístico del contraste HET1 de
heterocedasticidad (Pesaran y Taylor, 1999).

Para contrastes adicionales de diagnóstico sobre los modelos, consulta
"chow", "cusum", "reset" y "qlrtest".

Menú gráfico: Ventana de modelo: Contrastes

# mpi Programming

Argumento:  Mira abajo 

La instrucción mpi comienza un bloque de expresiones (que se deben
finalizar con end mpi) para ser ejecutadas usando el cómputo en paralelo de
MPI (Interfaz de Paso de Mensajes). Consulta gretl-mpi.pdf para obtener un
informe completo de esta prestación.

# mpols Estimation

Argumentos: depvar indepvars 
Opciones:   --vcv (Presenta la matriz de covarianzas)
            --simple-print (No presenta los estadísticos auxiliares)
            --quiet (No presenta los resultados)

Calcula las estimaciones de MCO para el modelo especificado, utilizando
aritmética de punto flotante con precisión múltiple, con la ayuda de la
biblioteca Gnu Multiple Precision (GMP). Por defecto, se utilizan 256 bits
de precisión en los cálculos, pero puedes aumentar esto mediante la
variable de entorno GRETL_MP_BITS. Por ejemplo, cuando utilizas el
intérprete Bash se te podría ocurrir la siguiente instrucción para
establecer una precisión de 1024 bits antes de comenzar GRETL.

	export GRETL_MP_BITS=1024

Dispones de una opción (más bien rebuscada) para esta instrucción,
principalmente con el propósito de hacer pruebas: cuando la lista indepvars
va seguida de un punto y coma, más de una lista posterior de números, esos
números se toman como potencias de x que se añaden a la regresión, donde
x es la última variable de indepvars. Estos términos adicionales se
calculan y se guardan con precisión múltiple. En el siguiente ejemplo, se
hace la regresión de y sobre x más la segunda, tercera y cuarta potencias
de ese x:

	mpols y 0 x ; 2 3 4

Menú gráfico: /Modelo/Otros modelos lineales/MCO de alta precisión

# negbin Estimation

Argumentos: depvar indepvars [ ; exposición ] 
Opciones:   --model1 (Utiliza el modelo NegBin 1)
            --robust (Matriz de covarianzas CMV (QML))
            --cluster=clustvar (Consulta "logit" para una explicación)
            --opg (Mira abajo)
            --vcv (Presenta la matriz de covarianzas)
            --verbose (Presenta los detalles de las iteraciones)
            --quiet (No presenta los resultados)
Ejemplos:   camtriv.inp

Estima un modelo Binomial Negativo. Se toma la variable dependiente para
representar un recuento del número de veces que ocurre un suceso de algún
tipo, y debe tener solo valores enteros no negativos. Por defecto, se
utiliza el modelo NegBin 2 en el que la varianza condicionada del recuento
viene determinada por mu(1 + αmu), donde mu denota la media condicionada.
Pero si indicas la opción --model1, la varianza condicionada es mu(1 + α).

La serie de exposición (offset, opcional) funciona del mismo modo que para
la instrucción "poisson". El modelo de Poisson es una forma restringida de
la Binomial Negativa en la que α = 0 por construcción.

Por defecto, las desviaciones típicas se calculan utilizando una
aproximación numérica a la matriz Hessiana en la convergencia. Pero si
indicas la opción --opg, la matriz de covarianzas se basa en el Producto
Externo del vector Gradiente, PEG (OPG), y si indicas la opción --robust,
se calculan las desviaciones típicas CMV (QML), utilizando un "emparedado"
entre la inversa de la matriz Hessiana y el PEG.

Menú gráfico: /Modelo/Variable dependiente limitada/Datos de conteo

# nls Estimation

Argumentos: función [ derivadas ] 
Opciones:   --quiet (No presenta el modelo estimado)
            --robust (Desviaciones típicas robustas)
            --vcv (Presenta la matriz de covarianzas)
            --verbose (Presenta los detalles de las iteraciones)
            --no-gradient-check (Mira abajo)
Ejemplos:   wg_nls.inp, ects_nls.inp

Realiza la estimación de Mínimos Cuadrados No Lineales (MCNL o NLS)
utilizando una versión modificada del algoritmo de Levenberg-Marquardt.
Debes indicar la especificación de una función y de enunciar los
parámetros de esta, además de darles unos valores iniciales antes de la
estimación. Como opción, puedes especificar las derivadas de la función
de regresión con respecto a cada uno de los parámetros. Si no proporcionas
as derivadas, en su lugar debes indicar una lista de los parámetros que se
van a estimar (separados por espacios o comas), precedida por la palabra
clave params. En este último caso, se calcula una aproximación numérica
al Jacobiano.

Resulta más fácil mostrar lo que se requiere mediante un ejemplo. Lo que
sigue es un guion completo para estimar la función no lineal de consumo
establecida en el libro Econometric Analysis (capítulo 11 de la 4a edición
o capítulo 9 de la 5a) de William Greene. Los números a la izquierda de
las líneas son solo para tomar como referencia y no son parte de las
instrucciones. Ten en cuenta que cualquier indicador de opción, como sería
--vcv para presentar la matriz de covarianzas de los estimadores de los
parámetros, deberías de añadirlo a la instrucción final, end nls.

	1   open greene11_3.gdt
	2   ols C 0 Y
	3   scalar alfa = $coeff(0)
	4   scalar beta = $coeff(Y)
	5   scalar gamma = 1.0
	6   nls C = alfa + beta * Y^gamma
	7    deriv alfa = 1
	8    deriv beta = Y^gamma
	9    deriv gamma = beta * Y^gamma * log(Y)
	10  end nls --vcv

Con frecuencia es conveniente iniciar los parámetros con una referencia a
un modelo lineal relacionado; esto se logra aquí con las líneas de la 2 a
la 5. Los parámetros alfa, beta y gamma pueden establecerse con cualquier
valor inicial (no necesariamente basados en un modelo estimado con MCO),
aunque la convergencia del procedimiento de MCNL no está garantizada para
cualquier punto de inicio que se te antoje.

Las auténticas instrucciones de MCNL ocupan las líneas de la 6 hasta la
10. En la línea 6 se indica la instrucción "nls" en la que se declara la
variable dependiente, con un signo de igualdad a continuación, y seguido
este de la especificación de una función. La sintaxis para el lado derecho
de la expresión es la misma que la de la instrucción "genr". Las
siguientes 3 líneas especifican las derivadas de la función de regresión
con respecto a cada uno de los parámetros, de uno en uno. Cada línea
comienza con la palabra clave "deriv", establece el nombre de un parámetro,
un signo de igualdad y una expresión por la que puede calcularse la
derivada. En lugar de proporcionar las derivadas analíticas, como
alternativa puedes substituir las líneas de la 7 a la 9, por lo siguiente:

	params alfa beta gamma

La línea 10, "end nls", completa la instrucción y solicita la estimación.
Cualquier opción deberás de añadirla a esta línea.

Si proporcionas las derivadas analíticas, por defecto GRETL ejecuta una
verificación numérica de su credibilidad. Algunas veces esto puede
producir falsos positivos, por situaciones en las que las derivadas
correctas parecen ser incorrectas y la estimación se rechaza. Para tener
esto en cuenta o para conseguir un poco de velocidad adicional, puedes
indicar la opción --no-gradient-check. Obviamente, debes hacer esto solo
cuando tengas certeza de que el vector gradiente que has especificado es
correcto.

Nombres de parámetros

Al estimar un modelo no lineal, con frecuencia es conveniente nombrar los
parámetros de forma sucinta. Ahora bien, al presentar los resultados, puede
que desees utilizar etiquetas más informativas. Esto lo puedes lograr
mediante la palabra clave adicional param_names dentro del bloque de
instrucciones. Para un modelo con k parámetros, el argumento que sigue a
esta palabra clave debe ser una cadena de texto literal entre comillas que
contenga k nombres separados por espacios, el nombre de una variable de
cadena que contenga k de esos nombres, o el nombre de un array con k cadenas
de texto.

Para obtener otros detalles sobre la estimación MCNL (NLS), consulta El
manual de gretl (Capítulo 25).

Menú gráfico: /Modelo/Mínimos cuadrados no lineales

# normtest Tests

Argumento:  serie 
Opciones:   --dhansen (Contraste de Doornik-Hansen, por defecto)
            --swilk (Contraste de Shapiro-Wilk)
            --lillie (Contraste de Lilliefors)
            --jbera (Contraste de Jarque-Bera)
            --all (Hace todos los contrastes)
            --quiet (No presenta los resultados)

Realiza un contraste de Normalidad para la serie indicada. El tipo concreto
de contraste se controla con el indicador de opción (y se ejecuta el
contraste de Doornik-Hansen cuando no indicas ninguna opción). Advertencia:
Los contrastes de Doornik-Hansen y Shapiro-Wilk son más recomendables que
los otros, teniendo en cuenta sus mejores propiedades en muestras pequeñas.

Mediante los accesores "$test" y "$pvalue" puedes recuperar el estadístico
de contraste y su probabilidad asociada (valor p), respectivamente. Ten en
cuenta que cuando indicas la opción --all, el resultado guardado es el del
contraste de Doornik-Hansen.

Menú gráfico: /Variable/Contraste de Normalidad

# nulldata Dataset

Argumento:  longitud 
Opción:     --preserve (Retiene las variables que no son series)
Ejemplo:    nulldata 500

Establece un conjunto de datos "en blanco" que: incluye solo una constante
más una variable índice, tiene periodicidad 1 y contiene el número de
observaciones especificado en el argumento. Puedes utilizar esto con la
intención de hacer simulaciones, pues funciones como "uniform()" y
"normal()" generan series artificiales comenzando por el principio, para
rellenar el conjunto de datos. Esta instrucción puede ser muy útil en
combinación con "loop". Consulta también la opción "seed" (semilla) de la
instrucción "set".

Por defecto, esta instrucción vacía todos los datos del espacio vigente de
trabajo de GRETL, no solo las series sino también las matrices, los
escalares, las cadenas de texto, etc. Ahora bien, cuando indicas la opción
--preserve, se retiene cualquier variable que no sea una serie y esté
definida en ese momento.

Menú gráfico: /Archivo/Nuevo conjunto de datos

# ols Estimation

Argumentos: depvar indepvars 
Opciones:   --vcv (Presenta la matriz de covarianzas)
            --robust (Desviaciones típicas robustas)
            --cluster=clustvar (Desviaciones típicas agrupadas)
            --jackknife (Mira abajo)
            --simple-print (No presenta estadísticos auxiliares)
            --quiet (No presenta los resultados)
            --anova (Presenta una tabla ANOVA)
            --no-df-corr (Elimina la corrección de los grados de libertad)
            --print-final (Mira abajo)
Ejemplos:   ols 1 0 2 4 6 7
            ols y 0 x1 x2 x3 --vcv
            ols y 0 x1 x2 x3 --quiet

Calcula las estimaciones de mínimos cuadrados ordinarios (MCO u OLS) siendo
depvar la variable dependiente, e indepvars una lista de variables
independientes. Puedes especificar las variables con el nombre o con el
número; y utilizar el número cero para indicar el término constante.

Aparte de las estimaciones de los coeficientes y de las desviaciones
típicas, el programa también presenta las probabilidades asociadas
(valores p) a los estadísticos t (con dos colas) y F. Un 'valor p' por
debajo de 0.01 indica significación estadística a un nivel del 1 por
ciento, y se marca con ***. La marca ** indica niveles de significación
entre 1 y 5 por ciento, y la marca * indica niveles entre 5 y 10 por ciento.
También se presentan los estadísticos para elegir modelos (el Criterio de
Información de Akaike o AIC, y el Criterio de Información Bayesiano de
Schwarz). La fórmula utilizada para el AIC es la proporcionada por Akaike
(1974), en concreto, menos dos veces el logaritmo de la verosimilitud
maximizada más dos veces el número de parámetros estimados.

Si indicas la opción --no-df-corr, no se aplica la corrección habitual de
los grados de libertad al calcular la varianza estimada de la perturbación
(y por lo tanto, tampoco las desviaciones típicas de los estimadores de los
parámetros).

La opción --print-final es aplicable solo en el contexto de un bucle
("loop"), y dispone que la regresión se ejecute silenciosamente en todas
las iteraciones del bucle, excepto en la última. Consulta El manual de
gretl (Capítulo 13) para obtener más detalles.

Puedes recuperar varias variables internas después de la estimación. Por
ejemplo:

	series uh = $uhat

guarda los errores de la estimación bajo el nombre uh. Consulta la sección
"Accesores" de la Guía de funciones de GRETL para obtener más detalles.

Puedes ajustar la fórmula (versión "HC") específica que se va a utilizar
para generar las desviaciones típicas robustas cuando indicas la opción
--robust, mediante la instrucción "set". La opción --jackknife tiene como
consecuencia la selección de una hc_version de 3a. La opción --cluster
anula la selección de la versión HC, y produce las desviaciones típicas
robustas agrupando las observaciones según los distintos valores de
clustvar. Consulta El manual de gretl (Capítulo 22) para obtener más
detalles.

Menú gráfico: /Modelo/Mínimos Cuadrados Ordinarios
Otro acceso:  Botón con el símbolo beta en la barra de herramientas

# omit Tests

Argumento:  listavariables 
Opciones:   --test-only (No substituye el modelo vigente)
            --chi-square (Devuelve la forma Chi-cuadrado del contraste de Wald)
            --quiet (Presenta solo los resultados básicos del contraste)
            --silent (No presenta nada)
            --vcv (Presenta la matriz de covarianzas del modelo reducido)
            --auto[=alfa] (Eliminación secuencial, mira abajo)
Ejemplos:   omit 5 7 9
            omit seasonals --quiet
            omit --auto
            omit --auto=0.05
            Ver también restrict.inp, sw_ch12.inp, sw_ch14.inp

Esta instrucción debe ir después de una instrucción de estimación. En su
forma básica, calcula el estadístico de contraste de Wald para la
significación conjunta de las variables de listavariables, que debe ser un
subconjunto (aunque no necesariamente un subconjunto apropiado) de las
variables independientes del último modelo estimado. Puedes recuperar los
resultados del contraste utilizando los accesores "$test" y "$pvalue".

A no ser que la restricción elimine todos los regresores originales, por
defecto, se estima el modelo restringido y este substituye al original como
"modelo vigente" si tienes intención, por ejemplo, de recuperar los errores
con $uhat o hacer contrastes posteriores. Puedes impedir este comportamiento
mediante la opción --test-only.

Por defecto, se registra la forma F del contraste de Wald; pero puedes
utilizar la opción --chi-square para recoger la forma chi-cuadrado en su
lugar.

Si tanto estimas como representas el modelo restringido, la opción --vcv
tiene el efecto de presentar su matriz de covarianzas; en otro caso, esta
opción se ignora.

Como alternativa, cuando indicas la opción --auto, se lleva a cabo la
eliminación secuencial por pasos. En cada etapa se excluye la variable
ligada a la mayor probabilidad asociada (valor p), hasta que todas las que
queden estén ligadas a valores p que no sean mayores que algún valor de
corte. Por defecto, este es del 10 por ciento (con 2 colas) y puedes
ajustarlo añadiendo "=", y un valor entre 0 y 1 (sin espacios), como en el
cuarto ejemplo de arriba. Si indicas listavariables, este proceso se limita
solo a las variables de la lista; en otro caso, todos los regresores aparte
de la constante se tratan como candidatos a la exclusión. Ten en cuenta que
las opciones --auto y --test-only no puedes combinarlas.

Menú gráfico: Ventana de modelo: Contrastes/Omitir variables

# open Dataset

Argumento:  nombrearchivo 
Opciones:   --quiet (No presenta la lista de las series)
            --preserve (Retiene las variables que no son series)
            --select=seleccion (Leer solo las series indicadas, mira abajo)
            --frompkg=nombrepaquete (Mira abajo)
            --all-cols (Mira abajo)
            --www (Utiliza una base de datos del servidor de GRETL)
            --odbc (Utiliza una base de datos ODBC)
            Mira abajo para opciones adicionales especiales
Ejemplos:   open data4-1
            open voter.dta
            open fedbog.bin --www
            open dbnomics

Abre un archivo de datos o una base de datos (consulta El manual de gretl
(Capítulo 4) para ver una explicación de esta distinción). Las
consecuencias son algo diferentes en los dos casos. Cuando abres un archivo
de datos, se lee su contenido en el espacio de trabajo de GRETL,
substituyendo la base de datos vigente (si la hay). Para añadir datos al
conjunto vigente, en lugar de sustituirlo, consulta "append" o (para tener
mayor flexibilidad) "join". Cuando abres una base de datos, no se carga
inmediatamente ningún dato; más bien, se establece la fuente para llamadas
posteriores de la instrucción "data", que se utiliza para importar series
concretas. Para obtener más detalles respecto a las bases de datos,
consulta la sección titulada "Abriendo una base de datos" más abajo.

Si no indicas nombrearchivo con una ruta completa, GRETL busca en algunas
rutas destacadas para tratar de encontrar el archivo, de las que el
directorio vigente ("workdir") es la primera elección. Si no indicas el
sufijo en el nombre de archivo (como en el primer ejemplo de arriba), GRETL
asume que es un archivo de datos propio con sufijo .gdt. Basándose en el
nombre del archivo y varias reglas heurísticas, GRETL tratará de detectar
el formato del archivo de datos (propio, texto plano, CSV, MS Excel, Stata,
SPSS, etc.).

Cuando se utiliza la opción --frompkg, GRETL va a buscar el archivo
especificado de datos en el subdirectorio asociado al paquete de funciones
especificado por nombrepaquete.

Si el argumento nombrearchivo toma la forma de un identificador de recursos
uniforme (URI) que comienza por http:// o por https://, entonces GRETL
tratará de descargar el archivo de datos indicado, antes de abrirlo.

Por defecto, al abrir un nuevo archivo de datos se vacía la sesión vigente
de GRETL, lo que incluye la eliminación de todas las variables definidas,
incluyendo matrices, escalares y cadenas de texto. Si quieres mantener las
variables que tengas definidas en ese momento (que no sean series, pues
estas se eliminan obligatoriamente), utiliza la opción --preserve.

Archivos de hoja de cálculo

Al abrir un archivo de datos con formato de hoja de cálculo (Gnumeric, Open
Document o MS Excel), puedes facilitar tres parámetros adicionales después
del nombre del archivo. Primero, puedes escoger una hoja de cálculo
concreta dentro del archivo. Esto se hace, bien indicando el número de hoja
por medio de la sintaxis (e.g., --sheet=2), o bien indicando el nombre de la
hoja (si lo sabes) entre comillas, como en --sheet="MacroData" pues, por
defecto, se va a leer la primera hoja de cálculo del archivo. También
puedes especificar un desplazamiento de columna y/o de fila dentro de la
hoja de cálculo mediante, e.g.,

	--coloffset=3 --rowoffset=2

lo que va a provocar que GRETL ignore las 3 primeras columnas y las 2
primeras filas. Por defecto, hay un desplazamiento de 0 en ambas
dimensiones, es decir, se empieza a leer en la celda de arriba a la
izquierda.

Archivos de texto delimitado

Con archivos de texto plano, GRETL habitualmente espera encontrar las
columnas de datos delimitadas de algún modo estándar (en general mediante
coma, tabulador, espacio, o punto y coma). Por defecto, GRETL busca en la
primera columna las etiquetas o las fechas de las observaciones, si su
encabezado está vacío o bien contiene una cadena de texto sugerente tal
como "year", "date" o "obs". Puedes evitar que GRETL trate de forma especial
la primera columna indicando la opción --all-cols.

Texto de formato fijo

Un archivo de datos en texto con "formato fijo" es aquel que no tiene
delimitadores de columna, pero en el que los datos se disponen de acuerdo
con un conjunto conocido de especificaciones como, por ejemplo, "la variable
k ocupa 8 columnas comenzando en la columna 24". Para leer ese tipo de
archivos, debes añadir una cadena de texto con --fixed-cols=colspec, donde
colspec se compone de números enteros separados por comas. Estos enteros se
interpretan como un conjunto de pares. El primer elemento de cada par denota
una columna de inicio, medida en bytes desde el principio de la línea, en
la que el 1 indica el primer byte; y el segundo elemento de cada par indica
cuantos bytes se deben de leer para el campo indicado. Así, por ejemplo, si
indicas

	open fixed.txt --fixed-cols=1,6,20,3

entonces GRETL va a leer 6 bytes comenzando en la columna 1 para la variable
1; y para la variable 2, va a leer 3 bytes comenzando en la columna 20. Las
líneas que están en blanco, o que comienzan con # se ignoran; pero en caso
contrario se aplica el patrón de lectura de columnas, y cuando se encuentra
algo distinto a un valor numérico válido, se muestra un fallo. Cuando se
leen los datos satisfactoriamente, las variables se van a designar como v1,
v2, etc. Está en manos del usuario el facilitar nombres con significado y/o
descripciones, utilizando para ello las instrucciones "rename" y/o
"setinfo".

Por defecto, cuando importas un archivo que contiene series con valores en
formato de cadena de texto, se abre una caja de texto mostrándote el
contenido de string_table.txt, un archivo que contiene la correspondencia
entre las cadenas y su codificación numérica. Puedes eliminar este
proceder mediante la opción--quiet.

Cargando series seleccionadas

El uso de open con un archivo de datos como argumento (en contraposición al
caso con una base de datos, mira abajo) generalmente implica cargar todas
las series del archivo indicado. Sin embargo, solo en el caso de archivos
originales de GRETL (gdt e gdtb) es posible especificar un subconjunto de
series a cargar, mediante su nombre. Esto se consigue mediante la opción
--select, lo que requiere un argumento adjunto con uno de estos formatos: el
nombre de una única serie; una lista de nombres, separados mediante
espacios y delimitados por comillas; o el nombre de un 'array' de cadenas de
texto. Ejemplos:

	# Serie única
	open somefile.gdt --select=x1
	# Más de una serie
	open somefile.gdt --select="x1 x5 x27"
	# Método alternativo
	strings Sel = defarray("x1", "x5", "x27")
	open somefile.gdt --select=Sel

Abriendo una base de datos

Como se comentó antes, puedes utilizar la instrucción open para abrir un
archivo con una base de datos, y a continuación leerlo con la instrucción
"data". Los tipos de archivos que se admiten son las bases de datos propias
de GRETL, RATS 4.0 y PcGive.

Además de la lectura de estos tipos de archivos en la máquina local, se
admiten otros tres casos más. Primero, cuando indicas la opción www, GRETL
va a tratar de acceder a una base de datos propia de GRETL con el nombre que
proporciones, en el servidor de GRETL (por ejemplo, la base de datos
fedbog.bin con los tipos de interés de la Reserva Federal del tercer
ejemplo que se ha indicado más arriba). En segundo lugar, puedes usar la
instrucción "open dbnomics" para establecer que DB.NOMICS sea el origen
para leer bases de datos; sobre esto consulta dbnomics for gretl. En tercer
lugar, si indicas la opción --odbc, GRETL va a tratar de acceder a un banco
de datos ODBC. Esta opción se explica detalladamente en El manual de gretl
(Capítulo 42).

Menú gráfico: /Archivo/Abrir archivo de datos
Otro acceso:  Arrastrar un archivo de datos hasta la ventana principal de GRETL

# orthdev Transformations

Argumento:  listavariables 

Aplicable solo con datos de panel. Se obtiene una serie con desviaciones
ortogonales adelantadas para cada variable de listavariables y se guarda en
una nueva variable con el prefijoo_. De este modo "orthdev x y" genera las
nuevas variables o_x y o_y.

Los valores se guardan un paso por delante de su localización temporal
verdadera (es decir, o_x en la observación t va a contener la desviación
que pertenece a t - 1, hablando estrictamente). Esto es por compatibilidad
con las primeras diferencias pues así se va a perder la primera
observación de cada serie temporal, no la última.

# outfile Printing

Variantes:  outfile nombrearchivo
            outfile --buffer=strvar
            outfile --tempfile=strvar
Opciones:   --append (Añadir a un archivo, solo la primera variante)
            --quiet (Mira abajo)
            --buffer (Mira abajo)
            --tempfile (Mira abajo)
            --decpoint (Mira abajo)

La instrucción outfile inicia un bloque con el que se desvía todo
resultado a presentar, hacia un archivo o buffer (o, si lo deseas,
simplemente se descarta). Dicho bloque se termina con la instrucción "end
outfile", y después de ella los resultados vuelven al cauce por defecto.

Desvío a un archivo señalado

La primera variante que se muestra abajo envía los resultados al archivo
señalado por el argumento nombrearchivo. Por defecto, se crea un nuevo
archivo (o se sobrescribe uno ya existente). El archivo resultante se guarda
en la carpeta "workdir" de la configuración, vigente excepto que la cadena
de texto nombrearchivo contenga una especificación completa de la ruta.
Pero, si quieres añadir resultados a un archivo ya existente, utiliza la
opción --append.

En el sencillo ejemplo que sigue, los resultados de una determinada
regresión se escriben en el archivo señalado.

	open data4-10
	outfile regress.txt
	  ols ENROLL 0 CATHOL INCOME COLLEGE
	end outfile

Nombres de archivos ficticios especiales

Se admiten tres valores especiales para nombrearchivo, del siguiente modo:

  null: Se suprime la presentación de resultados hasta que finalice el
  redireccionado.

  stdout: El resultado se redirecciona al canal de "resultado típico".

  stderr: El resultado se redirecciona al canal de "error típico".

Desvío a un buffer de cadena

La opción --buffer se utiliza para guardar resultados en una variable de
cadena. El parámetro que se necesita para esta opción debe ser el nombre
de una variable de cadena ya existente, cuyo contenido se sobrescribirá.
Abajo se muestra el mismo ejemplo indicado anteriormente, modificado para
guardar una cadena. En este caso, al representar el contenido de model_out
se mostrarán los resultados redirigidos.

	open data4-10
	string model_out = ""
	outfile --buffer=model_out
	  ols ENROLL 0 CATHOL INCOME COLLEGE
	end outfile
	print model_out

Desvío a un archivo temporal

La opción --tempfile se utiliza para dirigir los resultados hacia un
archivo temporal, con un nombre generado automáticamente que se garantiza
que es único, en el directorio "punto" del usuario. Igual que en el caso
del desvío a un buffer, el parámetro de opción debe ser el nombre de una
variable de cadena: en este caso, su contenido se sobrescribe con el nombre
del archivo temporal. Atención: los archivos que se guardan en el
directorio 'punto', se van a depurar al salir del programa, por lo que no
uses esta modalidad si deseas que los resultados se conserven después de tu
sesión de GRETL.

Repetimos el sencillo ejemplo de arriba, con un par de líneas extra para
ilustrar la cuestión de que strvar te indica a donde van los resultados, y
que puedes recuperarlos utilizando la función "readfile".

	open data4-10
	string mitemp
	outfile --tempfile=mitemp
	  ols ENROLL 0 CATHOL INCOME COLLEGE
	end outfile
	printf "Los resultados se han dirigido a %s\n", mitemp
	printf "Los resultados fueron:\n%s\n", readfile(mitemp)
	# Limpiar cuando el archivo no se necesita más
	remove(mitemp)

En algunos casos, puedes querer ejercer un cierto control sobre el nombre
del archivo temporal. Esto puedes hacerlo indicando una variable de cadena
de texto que contenga seis X consecutivas, como en

	string mitemp = "tmpXXXXXX.csv"
	outfile --tempfile=mitemp
	...

En este caso, se va a sustituir XXXXXX por una cadena de caracteres
aleatorios que aseguren que el nombre del archivo es único, pero se
preservará el sufijo ".csv". Al igual que en el caso más simple de arriba,
el archivo se escribe de forma automática en el directorio "dot" del
usuario, y se modifica el contenido de la variable de cadena expresada
mediante el indicador opcional, para mantener la ruta completa al archivo
temporal.

Discreción

Los efectos de la opción --quiet son: se desactiva que se vuelvan a
presentar las órdenes de instrucción, y se muestran los mensajes
auxiliares mientras los resultados estén redirigidos. Es equivalente a
hacer

	set echo off
	set messages off

excepto que, cuando finaliza la redirección, se restablecen los valores
originales de las variables echo y messages. Esta opción está disponible
en todo caso.

Carácter decimal

El efecto de la opción --decpoint es asegurar que el carácter de punto
decimal (en contraposición a la coma) está vigente mientras la salida de
resultados está desviada. Cuando el bloque outfile finaliza, el carácter
decimal vuelve al estado en que estaba antes de el. Esta opción resulta
especialmente útil cuando el archivo texto que se va a crear, está
destinado a ser una entrada para algún otro programa que requiera que los
dígitos sigan las convenciones del inglés, como podría ser el caso de un
guion de Gnuplot o de R, por ejemplo.

Niveles de redirección

En general, solo puedes abrir un archivo de este modo en un momento dado,
por lo que las llamadas a esta instrucción no pueden anidarse. Sin embargo,
la utilización de esta instrucción se permite dentro de funciones
definidas por el usuario (siempre que el archivo de resultados se cierre
desde dentro de la misma función) de forma que puedes desviar esos
resultados temporalmente, y después devolverlos a un archivo de resultados
original en caso de que outfile esté en uso en ese momento por el
solicitante. Por ejemplo, el código

	function void f (string s)
	    outfile interno.txt
	      print s
	    end outfile
	end function

	outfile externo.txt --quiet
	  print "Fuera"
	  f("Dentro")
	  print "De nuevo fuera"
	end outfile

producirá un archivo llamado "externo.txt" que contiene las dos líneas

	Fuera
	De nuevo fuera

y un archivo llamado "interno.txt" que contiene la línea

	Dentro

# panel Estimation

Argumentos: depvar indepvars 
Opciones:   --vcv (Presenta la matriz de covarianzas)
            --fixed-effects (Estima con efectos fijos por grupo)
            --random-effects (Modelo de efectos aleatorios o MCG (GLS))
            --nerlove (Utiliza la transformación de Nerlove)
            --pooled (Estima mediante MCO combinados)
            --between (Estima el modelo entre-grupos)
            --robust (Desviaciones típicas robustas; mira abajo)
            --cluster=cvar (Desviaciones típicas conglomeradas; mira abajo)
            --time-dummies (Incluye variables ficticias temporales)
            --unit-weights (Mínimos Cuadrados Ponderados)
            --iterate (Estimación iterativa)
            --matrix-diff (Calcula el contraste de Hausman mediante la matriz-diferencia)
            --unbalanced=método (Solo efectos aleatorios; mira abajo)
            --quiet (Resultados menos detallados)
            --verbose (Resultados más detallados)
Ejemplos:   penngrow.inp, panel-robust.inp

Estima un modelo de panel. Por defecto, se utiliza el estimador de efectos
fijos; esto se pone en práctica restándoles las medias de grupo o unidad,
a los datos originales.

Cuando indicas la opción --random-effects, se calculan las estimaciones de
efectos aleatorios, utilizando por defecto el método de Swamy y Arora
(1972). Únicamente en este caso, la opción --matrix-diff fuerza el uso del
método de la matriz-diferencia (en contraposición al método de
regresión) para llevar a cabo el contraste de Hausman sobre la consistencia
del estimador de efectos aleatorios. También es específica del estimador
de efectos aleatorios, la opción --nerlove que escoge el método de Nerlove
(1971) en contraposición al de Swamy y Arora.

Como alternativa, cuando indicas la opción --unit-weights, el modelo se
estima mediante mínimos cuadrados ponderados, con las ponderaciones basadas
en la varianza residual para las unidades respectivas de sección cruzada de
la muestra. Únicamente en este caso, puedes añadir la opción --iterate
para generar estimaciones iterativas y, si la iteración converge, las
estimaciones resultantes son Máximo Verosímiles.

Como alternativa posterior, si indicas la opción --between, se estima el
modelo entre-grupos (es decir, se hace una regresión MCO utilizando las
medias de los grupos).

El procedimiento por defecto para calcular Desviaciones típicas robustas en
modelos con datos de panel, es el estimador HAC de Arellano (2003)
(conglomerado por unidad de panel). Las alternativas son las "Desviaciones
Típicas Corregidas de Panel" (Beck y Katz, 1995) y las desviaciones
típicas "Consistentes de Correlación Espacial" (Driscoll y Kraay, 1998).
Estas se pueden seleccionar mediante la instrución set panel_robust con los
argumentos pcse y scc, respectivamente. Otras alternativas a estas tres
opciones están disponibles mediante la opción --cluster; por favor,
consulta El manual de gretl (Capítulo 22) para tener más detalles. Si se
especifican las desviaciones típicas robustas, se ejecuta el contraste
conjunto F sobre los efectos fijos utilizando el método robusto de Welch
(1951).

La opción --unbalanced está disponible solo para modelos con efectos
aleatorios, y puedes usarla para elegir el método ANOVA que emplear con un
panel desequilibrado. Por defecto, GRETL emplea el método de Swamy-Arora
igual que se hace para los paneles equilibrados, excepto que utiliza la
media armónica de las longitudes de las series de tiempo individuales en
lugar de la T habitual. Bajo esta opción puedes especificar, bien bc para
usar el método de Baltagi y Chang (1994), o bien usar stata para emular la
opción sa de la instrucción xtreg de Stata.

Para obtener más detalles sobre la estimación de un panel, consulta El
manual de gretl (Capítulo 23).

Menú gráfico: /Modelo/Panel

# panplot Graphs

Argumento:  vardibujar 
Opciones:   --means (Serie temporal con medias de grupo)
            --overlay (Gráfico por grupo, superpuestos, N <= 130)
            --sequence (Gráfico por grupo, en secuencia, N <= 130)
            --grid (Gráfico por grupo, en cuadrícula, N <= 16)
            --stack (Gráfico por grupo, apilados, N <= 6)
            --boxplots (Gráfico de caja por grupo, en secuencia, N <= 150)
            --boxplot (Gráfico único de caja, todos los grupos)
            --output=nombrearchivo (Enviar el resultado a un archivo específico)
Ejemplos:   panplot x --overlay
            panplot x --means --output=display

Instrucción de dibujo específica para datos de panel: la serie vardibujar
se dibuja del modo que se especifica con alguna de las opciones.

Además de las opciones --means y --boxplot, el gráfico representa
explícitamente las variaciones en las dos dimensiones, la de serie temporal
y la de sección cruzada. Tales gráficos están limitados en lo que se
refiere al número de grupos (o también conocidos como individuos o
unidades) en el rango de la muestra vigente del panel. Por ejemplo, la
opción --overlay, que presenta una serie temporal para cada grupo en un
único gráfico, solo está disponible si el número de grupos, N, es menor
o igual a 130. (De otro modo, el gráfico llegaría a ser demasiado denso
para resultar instructivo.) Si un panel es demasiado largo para permitir la
especificación gráfica deseada, puedes escoger provisionalmente un rango
reducido de grupos o de unidades, como en

	smpl 1 100 --unit
	panplot x --overlay
	smpl full

Puedes usar la opción --output=nombrearchivo para controlar la forma y el
destino del resultado; consulta la instrucción "gnuplot" para obtener más
detalles.

Otro acceso:  Ventana principal: Menú emergente (selección única)

# panspec Tests

Opciones:   --nerlove (Utiliza el método de Nerlove para efectos aleatorios)
            --matrix_diff (Utiliza el método de la matriz-diferencia para el contraste de Hausman)
            --quiet (Suprime la presentación de resultados)

Esta instrucción está disponible únicamente después de estimar un modelo
con datos de panel utilizando MCO (consulta también "setobs"). Contrasta la
especificación combinada simple frente a las principales alternativas, la
de efectos fijos y la de efectos aleatorios.

La especificación de efectos fijos permite que la ordenada en el origen de
la regresión varíe de una unidad de sección cruzada a otra. Se presenta
un contraste F de Wald para la hipótesis nula de que las ordenadas en el
origen no difieren. La especificación de efectos aleatorios descompone la
varianza de cada perturbación en dos partes, una parte específica de la
unidad de sección cruzada y otra parte específica de cada observación
concreta. (Se puede calcular este estimador solo cuando el número de
unidades de sección cruzada en el conjunto de datos supera al número de
parámetros a estimar.) El estadístico de Multiplicadores de Lagrange de
Breusch-Pagan contrasta la hipótesis nula de que MCO combinados es adecuado
frente a la alternativa de efectos aleatorios.

MCO combinados pueden rechazarse frente a ambas alternativas. Mientras la
perturbación específica por unidad o grupo no esté correlacionada con las
variables independientes, el estimador de efectos aleatorios será más
eficiente que el de efectos fijos; en otro caso, el estimador de efectos
aleatorios será inconsistente y serán preferibles los efectos fijos. La
hipótesis nula del contraste de Hausman indica que la perturbación
específica de grupo no está así correlacionada (y por eso se prefiere el
estimador de efectos aleatorios). Un valor bajo de la probabilidad asociada
(valor p) al estadístico de este contraste va en contra de los efectos
aleatorios y a favor de los efectos fijos.

Las dos primeras opciones de esta instrucción se corresponden con la
estimación de efectos aleatorios. Por defecto, se utiliza el método de
Swamy y Arora, mediante el cálculo del estadístico de contraste de
Hausman, utilizando el método de regresión. Las opciones permiten utilizar
el estimador alternativo de la varianza de Nerlove, y /o la aproximación de
la matriz-diferencia al estadístico de Hausman.

Cuando se completa con éxito, los accesores "$test" y "$pvalue"
proporcionan 3 vectores que contienen los estadísticos de prueba y los
valores p para los tres contrastes indicados arriba: combinabilidad (Wald),
combinabilidad (Breusch-Pagan), y Hausman. Si solo quieres los resultados de
esta forma, puedes indicar la opción --quiet para saltarte la presentación
de resultados.

Ten en cuenta que luego de estimar la especificación de efectos aleatorios
con la instrucción "panel" , el test de Hausman se ejecuta automáticamente
y puedes recuperar los resultados mediante el accesor "$hausman".

Menú gráfico: Ventana de modelo: Contrastes/Especificaciones de panel

# pca Statistics

Argumento:  listavariables 
Opciones:   --covariance (Utiliza la matriz de covarianzas)
            --save[=n] (Guarda las componentes más importantes)
            --save-all (Guarda todas las componentes)
            --quiet (No presenta los resultados)

Análisis de Componentes Principales. Excepto cuando indicas la opción
--quiet, presenta los valores propios de la matriz de correlaciones (o de la
matriz de covarianzas cuando indicas la opción --covariance) para las
variables que forman listavariables, junto con la proporción de la varianza
conjunta representada por cada componente. También presenta los
correspondientes autovectores o "pesos de las componentes".

Si indicas la opción --save-all, entonces se guardan todas las componentes
como series en el conjunto de datos, con los nombres PC1, PC2, etcétera.
Estas variables artificiales se forman como la suma de los productos de (el
peso de la componente) por (X_i tipificada), donde X_i denota la variable
i-ésima de listavariables.

Si indicas la opción --save sin un valor del parámetro, se guardan las
componentes con valores propios mayores que la media (lo que significa
mayores que 1.0 cuando el análisis se basa en la matriz de correlaciones)
en el conjunto de datos, tal como se describió arriba. Si indicas un valor
para n con esta opción, entonces se guardan las n componentes más
importantes.

Consulta también la función "princomp".

Menú gráfico: /Ver/Componentes principales

# pergm Statistics

Argumentos: serie [ anchobanda ] 
Opciones:   --bartlett (Utiliza la ventana de retardo de Bartlett)
            --log (Utiliza la escala logarítmica)
            --radians (Muestra la frecuencia en radianes)
            --degrees (Muestra la frecuencia en grados)
            --plot=modo-o-nombrearchivo (Mira abajo)
            --silent (Omite la presentación del resultado)

Calcula y muestra el espectro de la serie especificada. Por defecto, se
indica el periodograma de la muestra, pero se utiliza opcionalmente una
ventana de retardo de Bartlett al estimar el espectro, (consulta por
ejemplo, el libro de Greene Econometric Analysis para ver una discusión
sobre esto). La anchura por defecto de la ventana de Bartlett es de dos
veces la raíz cuadrada del tamaño de la muestra, pero puedes establecer
esto de modo manual utilizando el parámetro anchobanda, hasta un máximo de
la mitad del tamaño de la muestra.

Cuando indicas la opción --log, se representa el espectro en una escala
logarítmica.

Las opciones (mutuamente excluyentes) --radians y --degrees afectan al
aspecto del eje de frecuencias cuando se dibuja el periodograma. Por
defecto, la frecuencia se escala por el número de períodos de la muestra,
pero esas dos opciones provocan que el eje se etiquete desde 0 hasta pi
radianes o desde 0 a 180degrees, respectivamente.

Por defecto, si GRETL no está en modo de procesamiento por lotes, se
muestra un gráfico del periodograma. Puedes ajustar esto mediante la
opción --plot. Los parámetros admisibles para esta opción son none (para
suprimir el gráfico), display (para representar un gráfico incluso en modo
de procesamiento por lotes), o un nombre de archivo. El efecto de indicar un
nombre de archivo es como se describe para la opción --output de la
instrucción "gnuplot".

Menú gráfico: /Variable/Periodograma
Otro acceso:  Ventana principal: Menú emergente (selección única)

# pkg Utilities

Argumentos: acción nombrepaquete 
Opciones:   --local (Instala desde un archivo local)
            --quiet (Mira abajo)
            --verbose (Mira abajo)
            --staging (Mira abajo)
Ejemplos:   pkg install armax
            pkg install /path/to/myfile.gfn --local
            pkg query ghosts
            pkg run-sample ghosts
            pkg unload armax

Esta instrucción proporciona una manera de instalar, consultar, descargar,
eliminar o indexar paquetes de funciones de GRETL. El argumento acción debe
ser alguno de entre install, query, run‑sample, unload, remove o index,
respectivamente. Una extensión para dar soporte a paquetes de archivos de
datos se describe más abajo.

install: En su forma más elemental, sin ningún indicador de opción y con
el argumento nombrepaquete expresado como el nombre "plano" de un paquete de
funciones de GRETL (como en el primer ejemplo de arriba), el efecto de esta
opción consiste en descargar el paquete que se especifica del servidor de
GRETL, (excepto que nombrepaquete comience con http://), e instalarlo en la
máquina local. En este caso, no es necesario expresar una extensión en el
nombre del archivo. Sin embargo, cuando indicas la opción --local, el
argumento nombrepaquete debe ser la ruta a un archivo de paquete en la
máquina local, que todavía no esté instalado, y expresado con una
extensión correcta (.gfn o .zip). En este caso, el efecto consiste en
copiar el archivo en su sitio (gfn), o descomprimirlo en su sitio (zip),
significando "en su sitio" allí donde lo va a encontrar la instrucción
"include" .

query: Por defecto, la consecuencia de esta opción es la presentación de
información básica sobre el paquete especificado (autor, versión, etc.).
Si el paquete incluye recursos extra (archivos de datos y/o guiones
adicionales) se incluye un listado de esos archivos. Si añades la opción
--quiet, no se presenta nada; en su lugar, se guarda la información del
paquete en forma de un 'bundle' de GRETL, al que se puede acceder mediante
"$result". Si no se puede encontrar ninguna información, este 'bundle'
estará vacío.

run-sample: Proporciona un recurso mediante linea de instrucciones para
ejecutar el guion de prueba incluido en el paquete especificado.

unload: Debes indicar el argumento pkgname en modo 'plano', sin ruta ni
extensión, como en el último ejemplo de arriba. La consecuencia de esto es
la descarga de ese paquete en cuestión de la memoria de GRETL (si está
cargado en ese momento), y también eliminarlo del menú de la Interfaz
Gráfica (GUI) al que esté añadido, si lo está a alguno.

remove: Realiza las acciones indicadas para unload y, además, elimina del
disco el(los) archivo(s) asociado(s) con el paquete indicado.

index: Es un caso especial en el que nombrepaquete debe sustituirse por la
palabra clave "addons": la consecuencia de ello es que se actualiza el
índice de los paquetes estándar que se conocen como "Complementos". Esa
actualización se realiza automáticamente de vez en cuando, pero en algún
caso puede resultar útil una actualización manual. En tal caso, la opción
--verbose provoca un resultado impreso sobre donde ha hecho GRETL la
búsqueda, y lo que ha encontrado. Siendo claros, aquí tienes un modo de
conseguir un resultado con el índice completo:

	pkg index addons --verbose

Paquetes de archivos de datos

Además de su utilización con paquetes de funciones, la instrucción pkg
install también se puede usar con paquetes de archivos de datos de tipo
tar.gz como se indican en el listado de
https://gretl.sourceforge.net/gretl_data.html. Por ejemplo, para instalar
los archivos de datos de Verbeek, se puede hacer

	pkg install verbeek.tar.gz

Ten en cuenta que install es la única operación admitida para esos
archivos.

Montaje

La opción --staging es un elemento de conveniencia para los desarrolladores
y está disponible únicamente en conjunción con la acción install cuando
se aplica a un paquete de función. Su efecto consiste en que se descarga el
paquete en cuestión del área de montaje (staging) de Sourceforge en lugar
de hacerlo de un área pública. Los paquetes en montaje todavía no están
aprobados para uso general; por eso, ignora esta opción a no ser que sepas
lo que estás haciendo.

Menú gráfico: /Archivo/Paquetes de funciones/Sobre servidor

# plot Graphs

Argumento:  [ datos ] 
Opciones:   --output=nombrearchivo (Envía el resultado al archivo indicado)
            --outbuf=nomecadea (Envía el resultado a la cadena de texto indicada)
Ejemplos:   nile.inp

El bloque plot proporciona una alternativa a la instrucción "gnuplot" que
puede ser más conveniente cuando estás generando un gráfico complicado
(con varias opciones y/o instrucciones Gnuplot para que se inserten en el
archivo gráfico). Además de la siguiente explicación, por favor, consulta
también El manual de gretl (Capítulo 6) para ver otros ejemplos.

Un bloque de tipo plot comienza con la palabra de instrucción plot.
Habitualmente va seguida de un argumento de datos que especifica los datos
que se van a representar, y que debe indicar el nombre de una lista, de una
matriz o de una única serie. Si no especificas datos de entrada, el bloque
debe contener en su lugar al menos una directriz para dibujar una fórmula;
ese tipo de directivas puedes indicarlas por medio de lineas de tipo literal
o printf (mira más abajo).

Cuando indicas una lista (o una matriz), se asume que el último término (o
la última columna de la matriz) es la variable del eje x y que los(as)
otros(as) son las variables del eje y, excepto cuando indicas la opción
--time-series, en cuyo caso todos los datos especificados van en el eje y.
La opción de proporcionar el nombre de una sola serie se restringe a los
datos de series temporales, en cuyo caso se asume que quieres un gráfico de
series temporales; en otro caso, se muestra un fallo.

La línea de comienzo se puede preceder de la expresión "savename <-" para
que se guarde un gráfico como icono en el programa de Interfaz Gráfica de
Usuario (GUI). El bloque acaba con end plot.

Dentro del bloque tienes cero o más líneas de los siguientes tipos,
identificadas por la palabra clave inicial:

  option: Especifica una opción simple.

  options: Especifica múltiples opciones en una sola línea, separadas por
  espacios.

  literal: Una instrucción que se va a pasar literalmente a Gnuplot.

  printf: Un enunciado printf cuyo resultado se pasará literalmente a
  Gnuplot.

Ten en cuenta que al lado de las opciones --output y --outbuf (que se deben
añadir a la línea con que acaba el bloque), todas las opciones que admite
la instrucción "gnuplot" también las admite la instrucción plot, pero
deben indicarse dentro del bloque, utilizando la sintaxis descrita más
arriba. En este contexto, no es necesario proporcionar el habitual doble
guion antes del indicador de opción. Para obtener más detalles sobre los
efectos de las distintas opciones, consulta "gnuplot".

La intención de utilizar el bloque plot se ilustra mejor con el ejemplo:

	string title = "Mi título"
	string xname = "Mi variable X"
	plot plotmat
	    options with-lines fit=none
	    literal set linetype 3 lc rgb "#0000ff"
	    literal set nokey
	    printf "set title '%s'", title
	    printf "set xlabel '%s'", xname
	end plot --output=display

Este ejemplo asume que plotmat es el nombre de una matriz que tiene 2
columnas por lo menos (o una lista que tiene 2 elementos por lo menos). Ten
en cuenta que se considera una buena praxis colocar (únicamente) la opción
--output en la última línea del bloque; otras opciones deberías
colocarlas dentro del bloque.

Dibujar un gráfico sin datos

El seguiente ejemplo muestra un caso de cómo especificar la representación
de una gráfica sin ter una fuente de datos.

	plot
	    literal set title 'Utilidad CRRA'
	    literal set xlabel 'c'
	    literal set ylabel 'u(c)'
	    literal set xrange[1:3]
	    literal set key top left
	    literal crra(x,s) = (x**(1-s) - 1)/(1-s)
	    printf "plot crra(x, 0) t 'sigma=0', \\"
	    printf " log(x) t 'sigma=1', \\"
	    printf " crra(x,3) t 'sigma=3"
	end plot --output=display

# poisson Estimation

Argumentos: depvar indepvars [ ; exposición ] 
Opciones:   --robust (Desviaciones típicas robustas)
            --cluster=clustvar (Consulta "logit" para más explicaciones)
            --vcv (Presenta la matriz de covarianzas)
            --verbose (Presenta los detalles de las iteraciones)
            --quiet (No presenta los resultados)
Ejemplos:   poisson y 0 x1 x2
            poisson y 0 x1 x2 ; S
            Ver también camtriv.inp, greene19_3.inp

Estima una regresión de Poisson. Se coge la variable dependiente para
representar el acaecimiento de sucesos de algún tipo, y debe tener solo
valores enteros no negativos.

Si una variable aleatoria discreta Y sigue una distribución de Poisson,
entonces

  Pr(Y = y) = exp(-v) * v^y / y!

para y = 0, 1, 2,.... La media y la varianza de la distribución son ambas
iguales a v. En el modelo de regresión de Poisson, el parámetro v está
representado como una función de una o más variables independientes. La
versión más habitual (y la única que admite GRETL) cumple

  v = exp(b0 + b1*x1 + b2*x2 + ...)

o, en otras palabras, el logaritmo de v es una función lineal de las
variables independientes.

Como opción, puedes añadir una variable de exposición ("offset") a la
especificación. Esta es una variable de escala, y su logaritmo se añade a
la función lineal de regresión (implícitamente, con un coeficiente de
1.0). Esto tiene sentido si esperas que el número de ocurrencias del evento
en cuestión es proporcional (manteniéndose lo demás constante) a algún
factor conocido. Por ejemplo, puedes suponer que el número de accidentes de
tráfico es proporcional al volumen de tráfico (manteniéndose lo demás
constante) y, en ese caso, el volumen de tráfico puede expresarse como una
variable "de exposición" en un modelo de Poisson del cociente de
accidentes. La variable de exposición debe ser estrictamente positiva.

Por defecto, se calculan las desviaciones típicas utilizando la inversa
negativa de la matriz Hessiana. Si especificas la opción --robust, entonces
se calculan en su lugar las desviaciones típicas CMV (QML) o de
Huber-White. En este caso, la matriz de covarianzas estimada es un
"emparedado" entre la inversa de la matriz Hessiana estimada y el producto
externo del vector gradiente.

Consulta también "negbin".

Menú gráfico: /Modelo/Variable dependiente limitada/Datos de conteo

# print Printing

Variantes:  print listavariables
            print
            print nombresobjetos
            print cadenaliteral
Opciones:   --byobs (Por observaciones)
            --no-dates (Utiliza números de observación simples)
            --range=inicio:parada (Mira abajo)
            --midas (Mira abajo)
            --tree (Específico para bundles; mira abajo)
Ejemplos:   print x1 x2 --byobs
            print my_matrix
            print "Esto es una cadena"
            print my_array --range=3:6
            print hflist --midas

Ten en cuenta que print es más bien una instrucción "básica" (con la
intención principal de presentar los valores de las series). Consulta
"printf" y "eval" para otras alternativas más avanzadas y menos
restrictivas.

En la primera variante mostrada arriba (consulta el primer ejemplo
también), listavariables debe ser una lista de series (bien una lista ya
definida, o bien una lista especificada mediante los nombres o números ID
de las series, separados por espacios). En este caso, esta instrucción
presenta los valores de las series de la lista. Por defecto, los datos se
presentan "por variable", pero si añades la opción --byobs se presentan
por observación. Cuando se presentan por observación, por defecto se
muestra la fecha (con datos de series temporales) o la cadena de texto de la
etiqueta de observación (en caso de que lo haya) al comienzo de cada
línea. Mediante la opción --no-dates se elimina la presentación de las
fechas o de las etiquetas; en su lugar se muestra un simple número de
observación. Consulta el párrafo final de estos comentarios para ver el
efecto de la opción --midas (que se aplica solo a una lista ya definida de
series).

Cuando no indicas ningún argumento (la segunda variante mostrada arriba)
entonces el efecto es similar al primer caso, excepto que se van a presentar
todas las series del conjunto vigente de datos. Las opciones que se admiten
son como se describieron más arriba.

La tercera variante (con el argumento nombresobjetos; mira el segundo
ejemplo) espera una lista de nombres, separados por espacios, de objetos
básicos de GRETL que no sean series (escalares, matrices, cadenas de texto,
bundles, arrays); y se muestra el valor de estos objetos. En el caso de los
'bundles', sus componentes se ordenan por tipo y alfabéticamente.

En la cuarta forma (tercer ejemplo), cadenaliteral debe ser una cadena de
texto puesta entre comillas (y no debe haber nada más siguiendo a la línea
de instrucción). Se presenta la cadena de texto en cuestión, seguida de un
carácter de línea nueva.

Puedes utilizar la opción --range para controlar el volumen de información
que se presenta. Los valores (enteros) de los marcadores de inicio y parada
pueden referirse a observaciones de series y de listas, a filas de matrices,
a elementos de 'arrays', y a líneas de cadenas de texto. En todos los
casos, el valor mínimo de inicio es 1, y el máximo valor de parada es el
"tamaño en forma de filas" del objeto en cuestión. Los valores negativos
de estos marcadores se usan para disponer un recuento hacia atrás, desde el
final. Puedes indicar estos marcadores en formato numérico, o mediante
nombres de variables escalares previamente definidas. Si omites inicio, se
considera implícitamente igual a 1; y si omites parada, eso significa ir
hasta el final del todo. Con series y listas, ten en cuenta que los
marcadores se refieren al rango muestral vigente.

La opción --tree es específica para presentar un bundle de GRETL. El
efecto de ello es que, si el paquete especificado contiene otros bundles o
arrays de ellos, se presentan sus contenidos. De lo contrario, solo se
presentan los elementos del nivel superior del bundle.

La opción --midas es especial para presentar una lista de series y, más
aún, es específica para conjuntos de datos que contienen una o más series
de alta frecuencia, cada una representada por una "MIDAS list". Cuando
indicas una de esas listas como argumento y agregas esta opción, la serie
se presenta por observación de su frecuencia "original".

Menú gráfico: /Datos/Mostrar valores

# printf Printing

Argumentos: formato , elementos 

Presenta valores escalares, series, matrices o cadenas de texto bajo el
control de una cadena de texto para dar formato (ofreciendo una parte de la
función printf del lenguaje de programación C). Los formatos numéricos
reconocidos son %e, %E, %f, %g, %G, %d y %x, en cada caso con los diversos
reguladores disponibles en C. Ejemplos: el formato %.10g presenta un valor
con 10 cifras significativas, y %12.6f presenta un valor con un ancho de 12
caracteres de los que 6 son decimales. Sin embargo, ten en cuenta que en
GRETL el formato %g es una buena elección por defecto para todos los
valores numéricos, y no tienes necesidad de complicarte demasiado. Debes
utilizar el formato %s para las cadenas de texto.

La propia cadena de formato debe estar puesta entre comillas, y los valores
que se van a presentar deben ir después de esa cadena de formato, separados
por comas. Estos valores deben tener la forma de, o bien (a) los nombres de
las variables, o bien (b) expresiones que generen alguna clase de resultado
que sea presentable, o bien (c) las funciones especiales varname() o date().
El siguiente ejemplo presenta los valores de dos variables, más el de una
expresión que se calcula:

	ols 1 0 2 3
	scalar b = $coeff[2]
	scalar se_b = $stderr[2]
	printf "b = %.8g, Desviación típica %.8g, t = %.4f\n",
          b, se_b, b/se_b

Las siguientes líneas ilustran el uso de las funciones 'varname' y 'date',
que presentan respectivamente el nombre de una variable (indicado por su
número ID) y una cadena de texto con una fecha (dada por un número natural
positivo que indica una observación).

	printf "El nombre de la variable %d es %s\n", i, varname(i)
	printf "La fecha de la observación %d es %s\n", j, date(j)

Cuando indicas un argumento matricial asociado a un formato numérico, se
presenta la matriz entera utilizando el formato especificado para cada
elemento. El mismo se aplica a las series, excepto que el rango de valores
presentados se rige por la configuración vigente de la muestra.

La longitud máxima de una cadena de formato es de 127 caracteres. Se
reconocen las secuencias de escape \n (nueva línea), \r (salto de línea),
\t (tabulación), \v (tabulación vertical) y \\ (barra inclinada a la
izquierda literal). Para presentar un signo por ciento literal, utiliza %%.

Como en C, puedes indicar los valores numéricos que forman parte del
formato (el ancho y/o la precisión) directamente como números, como en
%10.4f, o como variables. En este último caso, se ponen asteriscos en la
cadena de formato y se proporcionan los argumentos correspondientes por
orden. Por ejemplo:

	scalar ancho = 12
	scalar precision = 6
	printf "x = %*.*f\n", ancho, precision, x

# probit Estimation

Argumentos: depvar indepvars 
Opciones:   --robust (Desviaciones típicas robustas)
            --cluster=clustvar (Consulta "logit" para más explicaciones)
            --vcv (Presenta la matriz de covarianzas)
            --verbose (Presenta los detalles de las iteraciones)
            --quiet (No presenta los resultados)
            --p-values (Muestra los valores p en lugar de las pendientes)
            --estrella (Elige la variante pseudo-R-cuadrado)
            --random-effects (Estima un modelo Probit de panel con efectos aleatorios, EA)
            --quadpoints=k (Número de puntos de cuadratura para la estimación con EA)
Ejemplos:   ooballot.inp, oprobit.inp, reprobit.inp

Si la variable dependiente es una variable binaria (todos sus valores son 0
o 1), se obtienen estimaciones máximo verosímiles de los coeficientes de
las variables de indepvars mediante el método de Newton-Raphson. Como el
modelo es no lineal, las pendientes están condicionadas por los valores de
las variables independientes. Por defecto, se calculan las pendientes con
respecto a cada una de las variables independientes (en las medias de esas
variables) y estas pendientes substituyen a los valores p habituales en el
resultado de la regresión. Puedes prescindir de este proceder indicando la
opción --p-values. El estadístico chi-cuadrado contrasta la hipótesis
nula de que todos los coeficientes son cero, excepto el de la constante.

Por defecto, las desviaciones típicas se calculan utilizando la inversa
negativa de la matriz Hessiana. Si indicas la opción --robust, entonces se
calculan en su lugar las desviaciones típicas CMV (QML) o de Huber-White.
En este caso, la matriz de covarianzas estimadas es un "emparedado" entre la
inversa de la matriz Hessiana estimada y el producto externo del vector
gradiente. Para obtener más detalles, consulta el capítulo 10 del libro de
Davidson y MacKinnon (2004).

Por defecto, se va a presentar el estadístico pseudo-R-cuadrado que fue
sugerido por McFadden (1974); pero en el caso binario, si indicas la opción
--estrella se va a presentar en su lugar la variante recomendada por
Estrella (1998). Esta variante presumiblemente imita de forma más parecida
las propiedades del R^2 habitual en el contexto de la estimación de
mínimos cuadrados.

Si la variable dependiente no es binaria sino discreta, entonces se obtienen
las estimaciones de un Probit Ordenado. (Si la variable elegida como
dependiente no es discreta, se muestra un fallo.)

Probit para datos de panel

Con la opción --random-effects, se asume que cada perturbación está
compuesta por dos componentes Normalmente distribuidas: (a) un término
invariante en el tiempo que es específico de la unidad de sección cruzada
o "individuo" (y que se conoce como efecto individual), y (b) un término
que es específico de la observación concreta.

La evaluación de la verosimilitud de este modelo implica utilizar la
cuadratura de Gauss-Hermite para aproximar el valor de las esperanzas de
funciones de variables Normales. Puedes escoger el número de puntos de
cuadratura utilizados mediante la opción --quadpoints (por defecto es de
32). Utilizando más puntos se mejora la precisión de los resultados, pero
con el coste de más tiempo de cálculo; así, con muchos puntos de
cuadratura, la estimación con un conjunto de datos muy grande puede
consumir demasiado tiempo.

Además de las estimaciones habituales de los parámetros (y de los
estadísticos asociados) relacionados con los regresores incluidos, se
presenta alguna información adicional sobre la estimación de este tipo de
modelo:

  lnsigma2: La estimación máximo verosímil del logaritmo de la varianza
  del efecto individual;

  sigma_u: La estimación de la desviación típica del efecto individual; y

  rho: La estimación de la parte del efecto individual en la varianza
  compuesta de la perturbación (también conocida como la correlación
  intra-clase).

El contraste de Razón de Verosimilitudes respecto a la hipótesis nula de
que rho es igual a cero, proporciona un modo de evaluar si es necesaria la
especificación de efectos aleatorios. Si la hipótesis nula no se rechaza,
eso sugiere que es adecuada una simple especificación Probit combinada.

Menú gráfico: /Modelo/Variable dependiente limitada/Probit

# pvalue Statistics

Argumentos: distribución [ parámetros ] xvalor 
Ejemplos:   pvalue z zscore
            pvalue t 25 3.0
            pvalue X 3 5.6
            pvalue F 4 58 fval
            pvalue G shape scale x
            pvalue B bprob 10 6
            pvalue P lambda x
            pvalue W shape scale x
            Ver también mrw.inp, restrict.inp

Calcula el área que queda a la derecha del valor xvalor en la distribución
especificada (z para la Normal, t para la t de Student, X para la
Chi-cuadrado, F para la F, G para la Gamma, B para la Binomial, P para la
Poisson, exp para la Exponencial, o W para la Weibull).

Dependiendo del tipo de distribución, debes indicar la siguiente
información antes del valor xvalor: para las distribuciones t y
chi-cuadrado, los grados de libertad; para la F, los grados de libertad de
numerador y denominador; para la Gamma, los parámetros de forma y de
escala; para la distribución Binomial, la probabilidad de "éxito" y el
número de intentos; para la distribución de Poisson, el parámetro lambda
(que es tanto la media como la varianza); para la Exponencial, un parámetro
de escala; y para la distribución de Weibull, los parámetros de forma y de
escala. Como se mostró en los ejemplos de arriba, puedes indicar los
parámetros numéricos en formato numérico o como nombres de variables.

Los parámetros para la distribución Gamma se indican a veces como media y
varianza en lugar de forma y escala. La media es el producto de la forma y
la escala; la varianza es el producto de la forma y el cuadrado de la
escala. De este modo, puedes calcular la escala dividiendo la varianza entre
la media, y puedes calcular la forma dividiendo la media entre la escala.

Menú gráfico: /Herramientas/Buscador de valores P

# qlrtest Tests

Opciones:   --limit-to=lista (Limita el contraste a un subconjunto de regresores)
            --plot=modo-o-nombrearchivo (Mira abajo)
            --quiet (No presenta los resultados)

Para un modelo estimado con datos de series temporales mediante MCO, realiza
el contraste de la Razón de Verosimilitudes de Quandt (QLR) para un cambio
estructural en un punto desconocido en el tiempo, con un 15 por ciento de
recorte al comienzo y al final del período de la muestra.

Para cada punto potencial de cambio dentro del 70 por ciento central de las
observaciones, se realiza un contraste de Chow. Consulta "chow" para obtener
más detalles; pues, de igual modo que con el contraste habitual de Chow,
este es un contraste robusto de Wald cuando el modelo original se estima con
la opción --robust, y un contraste F en otro caso. Entonces el estadístico
QLR es el máximo de los estadísticos de contraste particulares.

Se obtiene una probabilidad asociada (valor p) asintótica utilizando el
método de Bruce Hansen (1997).

Además de los accesores "$test" y "$pvalue" típicos de los contrastes de
hipótesis, puedes utilizar "$qlrbreak" para recuperar el índice de la
observación en la que el estadístico de contraste se maximiza.

Puedes utilizar la opción --limit-to para limitar el conjunto de
interacciones con la variable ficticia de corte en los contrastes de Chow, a
un subconjunto de los regresores originales. El parámetro para esta opción
debe ser una lista ya definida en la que todos sus elementos se encuentren
entre los regresores originales, y en la que no debes incluir la constante.

Cuando ejecutas de modo interactivo (únicamente) esta instrucción, se
muestra por defecto un gráfico del estadístico de contraste de Chow, pero
puedes ajustar esto mediante la opción --plot. Los parámetros que se
admiten en esta opción son none (para eliminar el gráfico), display (para
mostrar un gráfico incluso cuando no se está en modo interactivo), o un
nombre de archivo. El efecto de proporcionar un nombre de archivo es como el
descrito para la opción --output de la instrucción "gnuplot".

Menú gráfico: Ventana de modelo: Contraste/Contraste de RV de Quandt

# qqplot Graphs

Variantes:  qqplot y
            qqplot y x
Opciones:   --z-scores (Mira abajo)
            --raw (Mira abajo)
            --output=nombrearchivo (Envía el gráfico al archivo especificado)

Indicando como argumento una única serie, muestra un gráfico de los
cuantiles empíricos de la serie seleccionada (indicada por su nombre o su
número ID) frente a los cuantiles de la distribución Normal. La serie debe
incluir cuando menos 20 observaciones válidas en el rango vigente de la
muestra. Por defecto, los cuantiles empíricos se dibujan frente a los
cuantiles de una distribución Normal que tiene las mismas media y varianza
que los datos de la muestra, pero dispones de dos alternativas: si indicas
la opción --z-scores, los datos se tipifican; mientras que si indicas la
opción --raw, se dibujan los cuantiles empíricos "en bruto" frente a los
cuantiles de la distribución Normal estándar.

La opción --output tiene como efecto el envío del resultado al archivo
especificado; utiliza "display" para forzar que el resultado se presente en
la pantalla. Consulta la instrucción "gnuplot" para obtener más detalles
sobre esta opción.

Dadas dos series como argumentos, y y x, se muestra un gráfico de los
cuantiles empíricos de y frente a los de x. Los valores de los datos no se
tipifican.

Menú gráfico: /Variable/Gráfico Q-Q normal
Menú gráfico: /Ver/Gráficos/Gráfico Q-Q

# quantreg Estimation

Argumentos: tau depvar indepvars 
Opciones:   --robust (Desviaciones típicas robustas)
            --intervals[=nivelconf] (Calcula los intervalos de confianza)
            --vcv (Presenta la matriz de covarianzas)
            --quiet (No presenta los resultados)
Ejemplos:   quantreg 0.25 y 0 xlista
            quantreg 0.5 y 0 xlista --intervals
            quantreg 0.5 y 0 xlista --intervals=.95
            quantreg tauvec y 0 xlista --robust
            Ver también mrw_qr.inp

Regresión de cuantiles. El primer argumento (tau) es el cuantil
condicionado para el que se quiere la estimación. Puedes indicarlo, bien
con un valor numérico, o bien con el nombre de una variable escalar
definida previamente; y el valor debe estar en el rango de 0.01 a 0.99.
(Como alternativa, puedes indicar un vector de valores para tau; mira abajo
para obtener más detalles.) El segundo y subsiguientes argumentos componen
una lista de regresión con el mismo patrón que "ols".

Sin la opción --intervals, se presentan las desviaciones típicas para las
estimaciones de los cuantiles. Por defecto, estas se calculan de acuerdo con
la fórmula asintótica indicada por Koenker y Bassett (1978), pero cuando
indicas la opción --robust, se calculan las desviaciones típicas que son
robustas con respecto a la heterocedasticidad, utilizando el método de
Koenker y Zhao (1994).

Cuando escoges la opción --intervals, se presentan los intervalos de
confianza para las estimaciones de los parámetros en lugar de las
desviaciones típicas. Estos intervalos se calculan usando el método de la
inversión del rango y, en general, son asimétricos con respecto a las
estimaciones puntuales. Las especificidades del cálculo están mediatizadas
por la opción --robust: sin esta, los intervalos se calculan bajo el
supuesto de perturbaciones IID (Koenker, 1994); y con ella se utiliza el
estimador robusto desarrollado por Koenker y Machado (1999).

Por defecto, se generan intervalos de confianza del 90 por ciento. Puedes
modificar esto añadiendo un nivel de confianza (expresado como una
fracción decimal) a la opción de intervalos, como en --intervals=0.95.

Vector tau de valores: en lugar de proporcionar un escalar, puedes indicar
el nombre de una matriz definida previamente. En este caso, las estimaciones
se calculan para todos los valores tau indicados, y los resultados se
presentan en un formato especial, mostrando la secuencia de las estimaciones
de cuantiles para cada regresor, de uno en uno.

Menú gráfico: /Modelo/Estimación Robusta/Regresión de cuantil

# quit Utilities

Sale de la modalidad vigente de GRETL.

  Cuando esta instrucción se invoca desde un guion, finaliza la ejecución
  de ese guion. En el contexto de gretlcli en modo de procesamiento por
  lotes, el propio gretlcli finaliza; en caso contrario, el programa
  retrocede a modo interactivo.

  Cuando se invoca desde la consola del programa de Interfaz Gráfica de
  Usuario (GUI), se cierra la ventana de la consola.

  Cuando se invoca desde gretlcli en modo interactivo, este programa
  finaliza.

Ten en cuenta que esta instrucción no puede invocarse desde dentro de
funciones ni de bucles.

La instrucción quit en ningún caso provoca que finalice el programa de
Interfaz Gráfica de Usuario (GUI) de GRETL. Esto se hace mediante la
opción Salir del apartado Archivo del menú, o mediante Ctrl+Q, o pulsando
con el ratón el control de cierre en la barra de título de la ventana
principal de GRETL.

# rename Dataset

Argumentos: serie nuevonombre 
Opciones:   --quiet (Suprime la presentación de resultados)
            --case (Cambia el formato de los nombres de todas las series; mira abajo)
Ejemplos:   rename x2 ingreso
            rename --case=lower

Sin la opción --case, esta instrucción cambia el nombre de serie
(identificada por su nombre o su número ID) a nuevonombre. El nuevo nombre
debe tener 31 caracteres como máximo, comenzar con una letra y estar
formado solo por letras, dígitos y/o el carácter de barra baja. Además,
no debe ser el nombre de un objeto de cualquier tipo que ya exista.

La opción --case permite cambiar el formato de todos los nombres de las
series en el conjunto de datos abierto vigente. Cuando se utiliza esta
opción, no se debe indicar ningún nombre para serie ni para nuevonombre.
Se admiten los siguientes tipos de formato:

  lower: Convierte todos los nombres de las series a minúsculas.

  upper: Convierte todos los nombres de las series a mayúsculas.

  camel: Convierte todos los nombres de las series a formato camello; lo que
  significa que se eliminan los guiones bajos y el carácter siguiente a los
  mismos (si hai alguno) se pone en mayúsculas. Por ejemplo, alguna_cosa se
  vuelve algunaCosa.

  snake: Convierte todos los nombres de las series a formato serpiente; lo
  que significa que cualquier letra mayúscula (excepto la primera del
  nombre) se convierte a minúscula, precedida por un guión bajo. Por
  ejemplo, algunaCosa se vuelve alguna_cosa.

Menú gráfico: /Variable/Editar atributos
Otro acceso:  Ventana principal: Menú emergente (selección única)

# reset Tests

Opciones:   --quiet (No presenta la regresión auxiliar)
            --silent (No presenta nada)
            --squares-only (Calcula el contraste usando solo los cuadrados)
            --cubes-only (Calcula el contraste usando solo los cubos)
            --robust (Usa errores típicos robustos en la regresión auxiliar)

Debe ir después de la estimación de un modelo mediante MCO. Lleva a cabo
el contraste RESET de Ramsey sobre la especificación (no lineal) de un
modelo, añadiéndole a la regresión los cuadrados y/o los cubos de los
valores ajustados, y calculando el estadístico F para contrastar la
hipótesis nula de que los parámetros de los términos añadidos son cero.
Por razones numéricas, los cuadrados y los cubos se reescalan utilizando la
desviación típica de los valores ajustados.

Se van a añadir tanto los cuadrados como los cubos, excepto que indiques
una de las opciones --squares-only o --cubes-only.

Puedes utilizar la opción --silent si tienes intención de hacer uso de los
accesores "$test" y/o "$pvalue" para guardar los resultados del contraste.

La opción --robust está implícita cuando en la regresión que se va a
comprobar se utilizaron errores típicos robustos.

Menú gráfico: Ventana de modelo: Contrastes/Contraste RESET de Ramsey

# restrict Tests

Opciones:   --quiet (No presenta las estimaciones restringidas)
            --silent (No presenta nada)
            --wald (Solo estimadores de sistema, mira abajo)
            --bootstrap (Cálculo del contraste con remuestreo automático, si es posible)
            --full (Solo MCO y VECMs, mira abajo)
Ejemplos:   hamilton.inp, restrict.inp

Impone un conjunto de restricciones (habitualmente lineales) sobre: (a) el
último modelo estimado o (b) un sistema de ecuaciones que se definió y
nombró previamente. En todos los casos, debes comenzar el conjunto de
restricciones con la palabra clave "restrict" y terminarlo con "end
restrict".

En caso de una única ecuación, las restricciones siempre se aplican
implícitamente al último modelo, y se evalúan tan pronto como se cierre
el bloque restrict.

En caso de un sistema de ecuaciones (definido mediante la instrucción
"system"), puedes poner el nombre del sistema de ecuaciones definido
previamente, después del "restrict" inicial. Cuando omites eso y el último
modelo fue un sistema, entonces las restricciones se aplican a ese último
modelo. Por defecto, las restricciones se evalúan cuando el sistema acaba
de estimarse, usando la instrucción "estimate". Pero cuando indicas la
opción --wald, la restricción se comprueba inmediatamente a través del
contraste chi-cuadrado de Wald en relación a la matriz de covarianzas. Ten
en cuenta que esta opción va a generar un fallo si ya has definido un
sistema, pero aún no lo has estimado.

Dependiendo del contexto, puedes expresar de varios modos las restricciones
que quieras contrastar. El más simple es como se indica a continuación:
cada restricción se expresa como una ecuación, con una combinación lineal
de parámetros a la izquierda del signo de igualdad y un valor escalar a la
derecha (bien una constante numérica, o bien el nombre de una variable
escalar).

En caso de una única ecuación, puedes referirte a sus parámetros con el
formato b[i], donde i representa la posición en la lista de regresores
(comenzando en el 1), o con el formato b[nombrevar], donde nombrevar es el
nombre del regresor en cuestión. En caso de un sistema, la referencia a los
parámetros se hace utilizando la letra b junto con dos números colocados
entre corchetes. El primer número representa la posición de la ecuación
dentro del sistema, y el segundo número indica la posición del regresor
dentro de la lista de ellos. Por ejemplo, b[2,1] denota el primer parámetro
de la segunda ecuación, mientras que b[3,2] denota el segundo parámetro de
la tercera ecuación. Puedes anteponer multiplicadores numéricos a los
elementos b de la ecuación que representa una restricción, por ejemplo
3.5*b[4].

Aquí tienes un ejemplo de un conjunto de restricciones para un modelo
estimado previamente:

	restrict
	 b[1] = 0
	 b[2] - b[3] = 0
	 b[4] + 2*b[5] = 1
	end restrict

Y aquí tienes un ejemplo de un conjunto de restricciones para aplicar a un
sistema ya definido. (Si el nombre del sistema no contiene espacios, no
hacen falta las comillas que lo delimitan.)

	restrict "Sistema 1"
	 b[1,1] = 0
	 b[1,2] - b[2,2] = 0
	 b[3,4] + 2*b[3,5] = 1
	end restrict

En caso de una única ecuación, las restricciones se evalúan por defecto
por medio del contraste de Wald, usando la matriz de covarianzas del modelo
en cuestión. Si estimaste el modelo original con MCO, entonces se presentan
las estimaciones de los coeficientes restringidos; para eliminar esto,
añade la opción --quiet a la instrucción restrict inicial. Como
alternativa al contraste de Wald, para modelos estimados únicamente
mediante MCO o MCP, puedes indicar la opción --bootstrap para realizar el
contraste de la restricción con remuestreo automático (bootstrap).

En caso de un sistema, el estadístico de contraste depende del estimador
elegido: un estadístico de Razón de Verosimilitudes cuando el sistema se
estima utilizando un método de Máxima Verosimilitud, o un estadístico F
asintótico, en otro caso.

Restricciones lineales: sintaxis alternativa

Tienes dos alternativas al método para expresar las restricciones descrito
más arriba. Primero, puedes escribir de forma compacta un conjunto de g
restricciones sobre el vector con los k parámetros (beta), como Rbeta - q =
0, donde R es una matriz de dimensión g x k y q es un vector de dimensión
g. Puedes expresar una restricción indicando los nombres de matrices
definidas previamente, cómodas para utilizar como R y q, como en

	restrict
	  R = Rmat
	  q = qvec
	end restrict

En segundo lugar, como variante que puede serte útil cando uses la función
restrict dentro de otra función, puedes elaborar un conjunto de enunciados
de restricción con el formato de un 'array' de cadenas de texto. Después
utiliza la palabra clave inject con el nombre del 'array'. Este es un
ejemplo simple:

	strings RS = array(2)
	RS[1] = "b[1,2] = 0"
	RS[2] = "b[2,1] = 0"
	restrict
	  inject RS
	end restrict	

Con el uso actual de este método, posiblemente preferirás utilizar la
función "sprintf" para elaborar las cadenas de texto, en base a la entrada
para una función.

Restricciones no lineales

Si quieres contrastar una restricción no lineal (lo que actualmente solo
está disponible para modelos de una única ecuación), debes indicar la
restricción con el nombre de una función, precedida por "rfunc = ", como
en

	restrict
	  rfunc = mifuncion
	end restrict

La función de restricción debe tener un único argumento const matrix, y
esto se completa automáticamente con el vector de parámetros. Y debiera
devolver un vector que es cero bajo la hipótesis nula, y no nulo en otro
caso. La dimensión del vector es igual al número de restricciones. Esta
función se utiliza como una "rellamada" de la rutina numérica para el
Jacobiano, de GRETL, que calcula el estadístico de contraste de Wald
mediante el método delta.

Aquí tienes un ejemplo sencillo de una función apropiada para comprobar
una restricción no lineal, concretamente que dos pares de valores de los
parámetros tienen una razón común.

	function matrix restr (const matrix b)
	  matrix v = b[1]/b[2] - b[4]/b[5]
	  return v
	end function

Cuando se completa con éxito la instrucción restrict, los accesores
"$test" y "$pvalue" proporcionan el estadístico de contraste y su
probabilidad asociada (valor p), respectivamente.

Cuando se contrastan restricciones sobre un modelo de una única ecuación
que fue estimado mediante MCO o sobre un Modelo de Vectores de Corrección
del Error (VECM), puedes utilizar la opción --full para disponer que las
estimaciones restringidas sean el "último modelo", con la intención de
hacer contrastes más adelante o de usar accesores como $coeff y $vcv. Ten
en cuenta que se aplican algunos detalles especiales en caso de que pruebes
restricciones sobre un VECM. Consulta El manual de gretl (Capítulo 33) para
obtener más detalles.

Menú gráfico: Ventana de modelo: Contrastes/Restricciones lineales

# rmplot Graphs

Argumento:  serie 
Opciones:   --trim (Mira abajo)
            --quiet (No presenta los resultados)
            --output=nombrearchivo (Mira abajo)

Gráfico Rango-Media: Esta instrucción genera un gráfico sencillo para
ayudar a decidir si una serie temporal, y(t), tiene una varianza constante o
no. Se coge la muestra completa (t=1,...,T) y se divide en pequeñas
submuestras de tamaño arbitrario k. La primera submuestra está compuesta
por y(1),...,y(k), la segunda por y(k+1), ..., y(2k), etcétera. Para cada
submuestra, se calcula la media de la serie en la muestra y el rango (=
máximo menos mínimo), y se construye un gráfico con las medias en el eje
horizontal y los rangos en el vertical. Así cada submuestra se representa
mediante un punto en este plano. Si la varianza de la serie es constante, se
esperaría que el rango de la submuestra sea independiente de la media de la
submuestra. Por eso, si observamos que los puntos se aproximan a una línea
con pendiente positiva, esto sugiere que la varianza de las series aumenta a
medida que lo hace la media; y si los puntos se aproximan a una línea con
pendiente negativa, esto sugiere que la varianza decrece al aumentar la
media.

Además del gráfico, GRETL muestra las medias y rangos para cada
submuestra, junto con el coeficiente de la pendiente de una regresión MCO
do rango sobre la media, y con la probabilidad asociada al estadístico para
contrastar la hipótesis nula de que esta pendiente es cero. Si el
coeficiente de la pendiente es significativo con un nivel de significación
del 10 por ciento, entonces se muestra en el gráfico la línea ajustada de
la regresión del rango sobre la media. Se registran tanto el estadístico t
para contrastar la hipótesis nula como la probabilidad asociada
correspondiente, y puedes recuperarlos usando los accesores "$test" y
"$pvalue", respectivamente.

Cuando indicas la opción --trim, se descartan los valores mínimo y máximo
de cada submuestra antes de calcular la media y el rango. Esto hace que sea
menos probable que los valores atípicos provoquen una distorsión en el
análisis.

Cuando indicas la opción --quiet, no se muestra el gráfico ni se presenta
el resultado; solo se indican el estadístico t y su probabilidad asociada
(valor p). Por otro lado, puedes controlar el formato del gráfico mediante
la opción --output; y esto funciona como se describe en conexión con la
instrucción "gnuplot".

Menú gráfico: /Variable/Gráfico rango-media

# run Programming

Argumento:  nombrearchivo 

Ejecuta las instrucciones de nombrearchivo y luego devuelve el control al
indicador interactivo. Esta instrucción está pensada para que la utilices
con el programa de líneas de instrucción gretlcli o con la "consola de
GRETL" en el programa de Interfaz Gráfica de Usuario (GUI).

Consulta también "include".

Menú gráfico: Icono 'Ejecutar' en la ventana del editor de guiones

# runs Tests

Argumento:  serie 
Opciones:   --difference (Utiliza las primeras diferencias de la variable)
            --equal (Los valores positivos y negativos son equiprobables)

Realiza el contraste no paramétrico "de rachas" para comprobar el carácter
aleatorio de la serie indicada, donde las rachas se definen como secuencias
de valores consecutivos positivos o negativos. Si quieres contrastar el
carácter aleatorio de las desviaciones respecto a la mediana, para una
variable llamada x1 que tiene una mediana no nula, puedes hacer lo
siguiente:

	series signx1 = x1 - median(x1)
	runs signx1

Cuando indicas la opción --difference, se van a calcular las primeras
diferencias de la serie antes del análisis, por lo que las rachas se
interpretarían como secuencias de aumentos o de diminuciones consecutivas
del valor de la variable.

Cuando indicas la opción --equal, la hipótesis nula también incorpora el
supuesto de que los valores positivos y negativos son igual de probables; de
lo contrario, el estadístico de contraste resulta invariante con respecto a
la "neutralidad" del proceso que generó la secuencia de valores, y el
contraste se centra únicamente en la independencia.

Menú gráfico: /Herramientas/Contrastes no paramétricos

# scatters Graphs

Argumentos: yvar ; xvars  o yvars ; xvar 
Opciones:   --with-lines (Genera gráficos de líneas)
            --matrix=nombrematriz (Representa las columnas de la matriz indicada)
            --output=nombrearchivo (Envía el resultado al archivo especificado)
Ejemplos:   scatters 1 ; 2 3 4 5
            scatters 1 2 3 4 5 6 ; 7
            scatters y1 y2 y3 ; x --with-lines

Genera gráficos de dos variables, bien de yvar frente a todas las variables
de xvars, o bien de todas las variables de yvars frente a xvar. En el primer
ejemplo de arriba, se coloca la variable 1 en el eje y y se dibujan 4
gráficos: el primero tiene la variable 2 en el eje x, el segundo con la
variable 3 en el eje x, etcétera. El segundo ejemplo representa cada una de
las variables de la 1 a la 6, frente a la variable 7 en el eje x. Repasar un
conjunto de esos gráficos puede ser un paso conveniente en el análisis
exploratorio de datos. El número máximo de gráficos es de 16, por lo que
se va a ignorar cualquier variable adicional en la lista.

Por defecto, los datos se muestran con puntos, pero si indicas la opción
--with-lines serán gráficos de líneas.

Para obtener más detalles sobre el uso de la opción --output, consulta la
instrucción "gnuplot".

Si especificas una matriz ya definida como origen de los datos, debes
expresar las listas x e y con números naturales positivos para indicar la
columna. Alternativamente, si no indicas esas listas, se representan todas
las columnas frente al tiempo o a una variable índice.

Consulta también la instrucción "tsplots" para ver un modo sencillo de
generar gráficos múltiples de series temporales, y la instrucción
"gridplot" para ver un modo más flexible de combinar gráficos en una
parrilla.

Menú gráfico: /Ver/Gráficos múltiples/Gráficos X-Y (scatters)

# sdiff Transformations

Argumento:  listavariables 

Se obtiene la diferencia estacional de cada una de las variables de
listavariables, y se guarda el resultado en una nueva variable con el
prefijo sd_. Esta instrucción está disponible solo para series de tiempo
estacionales.

Menú gráfico: /Añadir/Diferencias estacionales de las variables seleccionadas

# set Programming

Variantes:  set variable valor
            set --to-file=nombrearchivo
            set --from-file=nombrearchivo
            set stopwatch
            set
Ejemplos:   set svd on
            set csv_delim tab
            set horizon 10
            set --to-file=mysettings.inp

El uso más común de esta instrucción es la primera variante mostrada
arriba, en la que se utiliza para establecer el valor de un parámetro
escogido del programa (esto se discute con detalle más abajo). Los otros
usos son: con --to-file para escribir un archivo de guion que contenga todas
las configuraciones actuales de los parámetros; con --from-file para leer
un archivo de guion que contenga las configuraciones de los parámetros y
para aplicarlas a la sesión vigente; con stopwatch para poner a cero el
"cronómetro" de GRETL que puedes usar para medir el tiempo de CPU (consulta
los comentarios para el accesor "$stopwatch"); o para presentar las
configuraciones actuales, cuando indicas solo la palabra set.

Los valores establecidos mediante esta instrucción siguen vigentes durante
la duración de la sesión de GRETL, excepto que los cambies por medio de
una llamada posterior a "set". Los parámetros que puedes establecer de este
modo se enumeran más abajo. Ten en cuenta que se utilizan las
configuraciones de hc_version, hac_lag y hac_kernel cuando indicas la
opción --robust en una instrucción de estimación.

Las configuraciones disponibles se agrupan bajo las siguientes categorías:
interacción y comportamiento del programa, métodos numéricos, generación
de números aleatorios, estimación robusta, filtrado, estimación de series
temporales e interacción con GNU R.

Interacción y comportamiento del programa

Estas configuraciones se utilizan para controlar diversos aspectos del modo
en el que GRETL interactúa con el usuario.

  workdir: path. Establece el directorio por defecto para escribir y leer
  archivos en los casos en los que no se especifican las rutas completas.

  use_cwd: on u off (por defecto). Maneja la configuración del directorio
  de trabajo (workdir) inicial: si está en on, se hereda el directorio de
  trabajo desde el intérprete; en otro caso, se establece donde quiera que
  se seleccionó en la sesión previa de GRETL.

  echo: off u on (por defecto). Elimina (o acorta) la resonancia de los
  textos de las instrucciones en los resultados de GRETL.

  messages: off u on (por defecto). Elimina (o acorta) la presentación de
  mensajes sin fallo asociados a diversas instrucciones, por ejemplo cuando
  se genera una nueva variable o cuando se cambia el rango de la muestra.

  verbose: off, on (por defecto) o comments. Funciona como un "interruptor
  maestro" para echo y messages (mira más abajo), apagando o encendiendo
  los dos simultáneamente. El argumento comments apaga la resonancia y la
  aparición de mensajes, pero mantiene la presentación de comentarios de
  un guion.

  warnings: off u on (por defecto). Elimina (o acorta) la presentación de
  mensajes de advertencia cuando surgen problemas numéricos; por ejemplo,
  si un cálculo produce valores no finitos o si la convergencia de un
  proceso de optimización es cuestionable.

  csv_delim: comma (coma, por defecto), space (espacio), tab (tabulación) o
  semicolon (punto y coma). Establece el delimitador de columnas que se usa
  cuando se guardan datos en un archivo con formato CSV.

  csv_write_na: La cadena de texto que se utiliza para representar los
  valores ausentes cuando se escriben datos en un archivo con formato CSV.
  Máximo = 7 caracteres; por defecto es NA.

  csv_read_na: La cadena de texto que se coge para representar valores
  ausentes (NAs) cuando se leen datos con el formato CSV (máximo 7
  caracteres). La cadena por defecto depende de que se encuentre una columna
  de datos que contenga datos numéricos (la mayoría de las veces) o
  valores de cadena. Para datos numéricos, se considera que lo siguiente
  indica NAs: una celda vacía o cualquiera de las cadenas NA, N.A., na,
  n.a., N/A, #N/A, NaN, .NaN, ., .., -999, y -9999. Para datos con forma de
  cadenas de texto con valores, tan solo se cuenta como NA una celda en
  blanco o una celda que contenga una cadena vacía. Puedes volver a imponer
  esos valores por defecto indicando default como el valor de csv_read_na.
  Para especificar que tan solo se leen las celdas vacías como NAs, indica
  el valor "". Ten en cuenta que las celdas vacías siempre se leen como NAs
  con independencia de como esté configurada esta variable.

  csv_digits: Un entero positivo que especifica el número de dígitos
  significativos a usar cuando se escriben datos en formato CSV. Por
  defecto, se utilizan hasta 15 dígitos dependiendo de la precisión de los
  datos originales. Ten en cuenta que el resultado CSV emplea la función
  fprintf de la librería de C con la conversión "%g" , lo que significa
  que se prescinde de los ceros que quedan atrás.

  display_digits: Un entero de 3 a 6 que especifica el número de dígitos
  significativos a usar cuando se muestran los coeficientes de la regresión
  y las desviaciones típicas (siendo 6 por defecto). También puedes
  utilizar esta configuración para limitar el número de dígitos que se
  muestran con la instrucción "summary"; siendo en este caso 5 por defecto
  (y también el máximo).

  mwrite_g: on u off (por defecto). Cuando se escribe una matriz como texto
  en un archivo, GRETL por defecto utiliza notación científica con 18
  dígitos de precisión, asegurando de este modo que los valores guardados
  son una representación fiable de los números en memoria. Cuando se
  escriben datos básicos con no más que 6 dígitos de precisión, puedes
  preferir utilizar el formato %g para tener un archivo más compacto y
  fácil de leer; puedes hacer este cambio mediante set mwrite_g on.

  force_decpoint: on u off (por defecto). Fuerza a GRETL a utilizar el
  carácter de punto decimal, en un escenario donde otro carácter
  (probablemente la coma) es el separador decimal estándar.

  loop_maxiter: Un valor entero no negativo (por defecto es 100000).
  Establece el número máximo de iteraciones que se le permite a un bucle
  while, antes de parar (consulta "loop"). Ten en cuenta que esta
  configuración solo afecta a la variante while; su intención es
  protegerse ante infinitos bucles que surjan de forma inadvertida.
  Establecer que este valor sea 0 tiene el efecto de inhabilitar el límite
  (utilízalo con precaución).

  max_verbose: off (por defecto), on o full. Controla la verborrea de las
  instrucciones y de las funciones que utilizan métodos de optimización
  numérica. La opción on solo se aplica a funciones (tales como "BFGSmax"
  y "NRmax") que funcionan por defecto con discreción; su efecto consiste
  en que se muestra información básica sobre las iteraciones. Puedes usar
  la opción full para provocar un resultado más detallado, que incluye los
  valores de los parámetros y su respectivo gradiente de la función
  objetivo, en cada iteración. Esta opción se aplica tanto a las funciones
  del tipo mencionado antes, como a las instrucciones que se basan en
  optimización numérica como "arima", "probit" y "mle". En el caso de las
  instrucciones, su efecto consiste en hacer que su opción --verbose
  proporcione un mayor detalle. Consulta también El manual de gretl
  (Capítulo 37).

  debug: 1, 2 o 0 (por defecto). Esto se utiliza con las funciones definidas
  por el usuario. Establecer debug igual a 1 equivale a activar messages
  dentro de todas esas funciones, y establecer esta variable igual a 2 tiene
  el efecto adicional de activar max_verbose dentro de todas las funciones.

  shell_ok: on u off (por defecto). Permite ejecutar programas externos
  desde GRETL mediante el intérprete de sistema. Esto no está habilitado
  por defecto por razones de seguridad, y solo puedes habilitarlo mediante
  la Interfaz Gráfica de Usuario (Herramientas/Preferencias/General). Sin
  embargo, una vez activada, esta configuración permanecerá activa para
  sesiones futuras hasta que se desactive explícitamente.

  bfgs_verbskip: Un entero. Esta configuración afecta al comportamiento de
  la opción --verbose en aquellas instrucciones que utilizan BFGS como
  algoritmo de optimización, y se usa para compactar el resultado. Si
  bfgs_verbskip se establece en 3, por ejemplo, entonces la opción
  --verbose va a provocar que se presenten las iteraciones 3, 6, 9,
  etcétera.

  skip_missing: on (por defecto) u off. Controla el comportamiento de GRETL
  cuando se construye una matriz a partir de series de datos: por defecto se
  saltan las filas de datos que contienen uno o más valores ausentes, pero
  cuando se pone skip_missing en off, los valores ausentes se convierten en
  NaNs.

  matrix_mask: El nombre de una serie o la palabra clave null. Ofrece un
  mayor control que skip_missing cuando se construyen matrices a partir de
  series: las filas de datos seleccionadas para las matrices son aquellas
  con valores no nulos (y no ausentes) de las series especificadas. La
  máscara escogida permanece en vigor hasta que se substituye, o se elimina
  mediante la palabra clave null.

  quantile_type: Debes escoger entre Q6 (por defecto), Q7 o Q8. Selecciona
  el método concreto que utiliza la función "quantile". Para obtener más
  detalles, consulta Hyndman y Fan (1996) o la entrada de la Wikipedia
  disponible en https://en.wikipedia.org/wiki/Quantile.

  huge: Un número positivo muy grande (por defecto, 1.0E100). Esta
  configuración controla el valor que devuelve el accesor "$huge".

  assert: off (por defecto), warn o stop. Controla las consecuencias de un
  fallo (que el valor que se devuelva sea igual a 0) de la función
  "assert".

  datacols: Un número entero de 1 a 15, cuyo valor por defecto es 5.
  Establece el número máximo de series que se presentan conjuntamente
  cuando los datos se representan por observación.

  plot_collection: on, auto u off. Esta configuración afecta al modo en el
  que se muestran los gráficos durante el uso interactivo. Si está en on,
  los gráficos del mismo tamaño en pixels se reúnen en una "colección de
  gráficos", es decir, en una única pantalla de salida de resultados en la
  que puedes navegar entre los diversos gráficos yendo adelante y hacia
  atrás. Con el ajuste en off, por el contrario, se va a generar una
  pantalla distinta para cada gráfico, como en las versiones anteriores de
  GRETL. Finalmente, el ajuste en auto tiene como efecto que permite el modo
  de colección de gráficos solo para aquellos que se generan antes de que
  pasen 1.25 segundos después de otro (por ejemplo, como resultado de
  ejecutar instrucciones de representación gráfica dentro de un bucle).

Métodos numéricos

Estas configuraciones se utilizan para controlar los algoritmos numéricos
que utiliza GRETL para la estimación.

  optimizer: o auto (por defecto), o BFGS, o bien newton. Establece el
  algoritmo de optimización que se utiliza para varios estimadores Máximo
  Verosímiles, en los casos donde el BFGS y el de Newton-Raphson se pueden
  aplicar ambos. Por defecto, se utiliza el de Newton-Raphson cuando se
  disponga de una matriz Hessiana analítica; en otro caso, BFGS.

  bhhh_maxiter: Un entero, el número máximo de iteraciones para la rutina
  interna BHHH de GRETL, que se utiliza en la instrucción "arma" para la
  estimación MV condicional. Si la convergencia no se alcanza luego de
  bhhh_maxiter, el programa devuelve un fallo. Por defecto, se establece en
  500.

  bhhh_toler: Un valor de punto flotante o la cadena default. Esto se
  utiliza en la rutina interna BHHH de GRETL para verificar si la
  convergencia se alcanzó. El algoritmo termina de repetirse tan pronto
  como el incremento en el logaritmo de la verosimilitud entre iteraciones
  sea menor que bhhh_toler. El valor por defecto es 1.0E-06, y puedes
  restablecer este valor tecleando default en lugar de un valor numérico.

  bfgs_maxiter: Un entero, el número máximo de iteraciones para la rutina
  BFGS de GRETL, que se utiliza para "mle" (EMV), "gmm" (MXM) y varios
  estimadores específicos. Si no se alcanza la convergencia en el número
  indicado de iteraciones, el programa devuelve un fallo. El valor por
  defecto depende del contexto, pero habitualmente es del orden de 500.

  bfgs_toler: Un valor de punto flotante o la cadena default. Esto se
  utiliza en la rutina interna BFGS de GRETL para verificar si la
  convergencia se alcanzó. El algoritmo termina de repetirse tan pronto
  como la mejoría relativa en la función objetivo entre iteraciones sea
  menor que bfgs_toler. El valor por defecto es igual a la precisión de
  máquina elevada a 3/4, y puedes restablecer este valor tecleando default
  en lugar de un valor numérico.

  bfgs_maxgrad: Un valor de punto flotante. Esto se utiliza en la rutina
  interna BFGS de GRETL, para verificar si la norma del vector gradiente
  está razonablemente cerca de cero cuando se alcanza el criterio
  bfgs_toler. Se va a presentar una advertencia cuando la norma del vector
  gradiente exceda de 1; y se muestra un fallo si la norma excede
  bfgs_maxgrad. Actualmente, por defecto el valor de tolerancia es de 5.0.

  bfgs_richardson: on u off (por defecto). Utiliza la extrapolación de
  Richardson cuando calcules las derivadas numéricas en el contexto de la
  maximización BFGS.

  initvals: El nombre de una matriz que haya sido definida previamente.
  Permite establecer manualmente el vector inicial de parámetros en
  determinadas instrucciones de estimación que implican realizar
  optimización numérica como arma, garch, logit, probit, tobit, intreg,
  biprobit, duration; y también cuando se imponen ciertos tipos de
  restricciones que están vinculadas a modelos VEC. A diferencia de otras
  configuraciones, initvals no es persistente, pues se restablece su valor
  al de inicio por defecto, después de su primera utilización. Para
  obtener detalles en relación con la estimación ARMA consulta El manual
  de gretl (Capítulo 31).

  lbfgs: on u off (por defecto). Utiliza la versión de memoria limitada de
  BFGS (L-BFGS-B) en vez del algoritmo habitual. Esto puede ser ventajoso
  cuando la función que se maximiza no es globalmente cóncava.

  lbfgs_mem: Un valor entero en el rango de 3 a 20 (con un valor por defecto
  de 8). Esto determina el número de correcciones que se utilizan en la
  matriz de memoria limitada cuando se emplea L-BFGS-B.

  nls_toler: Un valor de punto flotante. Establece la tolerancia que se
  utiliza al juzgar si la convergencia se alcanza o no, en una estimación
  de mínimos cuadrados no lineales utilizando la instrucción "nls". El
  valor por defecto es igual a la precisión de máquina elevada a 3/4, y
  puedes restablecer este valor tecleando default en lugar de un valor
  numérico.

  svd: on u off (por defecto). Utiliza la Descompisición en Valores
  Singulares (SVD) en vez de las descomposiciones de Cholesky o la QR, en
  los cálculos de mínimos cuadrados. Esta opción se aplica a la función
  mols así como a varios cálculos internos, pero no a la instrucción
  "ols" habitual.

  force_qr: on u off (por defecto). Esto se aplica a la instrucción "ols".
  Por defecto, esta instrucción calcula las estimaciones de MCO utilizando
  la descomposición de Cholesky (el método más rápido), con QR como
  último recurso si los datos parecen demasiado mal condicionados. Puedes
  utilizar force_qr para saltarte el paso de Cholesky, pues en los casos
  "dudosos" esto puede asegurar una mayor precisión.

  fcp: on u off (por defecto). Utiliza el algoritmo de Fiorentini, Calzolari
  y Panattoni en vez del código propio de GRETL, cuando se calculan las
  estimaciones GARCH.

  gmm_maxiter: Un entero, el número máximo de iteraciones de la
  instrucción "gmm" de GRETL cuando se está en modo iterativo (en
  contraposición al de un paso o al de dos pasos). El valor por defecto es
  250.

  nadarwat_trim: Un entero, el parámetro de recorte utilizado en la
  función "nadarwat".

  fdjac_quality: Un entero (0, 1 o 2) que indica el algoritmo utilizado por
  la función "fdjac"; por defecto es 0.

  gmp_bits: Un entero, que debe ser una potencia de 2 con exponente entero
  (el valor predeterminado y mínimo es 256, y el máximo es 8192). Esto
  controla el número de bits que se utilizan para representar un número de
  punto flotante cuando se invoca la GMP (la Biblioteca de la Aritmética de
  Precisión Múltiple de GNU), sobre todo mediante la instrucción mpols.
  Los valores más grandes de ese entero proporcionan mayor precisión al
  coste de un mayor tiempo de cálculo. Esta configuración también se
  puede controlar por medio de la variable de entorno GRETL_MP_BITS.

Generación de números aleatorios

  seed: Un número natural positivo o la palabra clave auto. Establece la
  semilla para el generador de números pseudoaleatorios. Por defecto, esto
  se establece a partir del tiempo del sistema; pero si quieres generar
  secuencias repetibles de números aleatorios debes establecer la semilla
  manualmente. Para restablecer la semilla a un valor automático basado en
  el tiempo, usa auto.

Estimación robusta

  bootrep: Un entero. Establece el número de repeticiones de la
  instrucción "restrict" con la opción --bootstrap.

  garch_vcv: unset, hessian, im (matriz de información), op (matriz de
  producto externo), qml (estimador CMV o QML), o bw
  (Bollerslev-Wooldridge). Especifica la variante que se va a utilizar para
  estimar la matriz de covarianzas de los coeficientes para modelos GARCH.
  Cuando indicas unset (caso por defecto) entonces se utiliza la matriz
  Hessiana, excepto que se indique la opción "robust" para la instrucción
  garch, en cuyo caso se utiliza CMV (QML).

  arma_vcv: hessian (caso por defecto) u op (matriz de producto externo).
  Especifica la variante que se va a utilizar cuando se calcula la matriz de
  covarianzas para modelos ARIMA.

  force_hc: off (por defecto) u on. Por defecto, con datos de series
  temporales y cuando indicas la opción--robust con ols (MCO), se utiliza
  el estimador HAC. Si pones force_hc en "on", esto fuerza el cálculo de la
  Matriz de Covarianzas Consistente ante Heterocedasticidad (HCCM) habitual,
  que no tiene en cuenta la autocorrelación. Ten en cuenta que los VARs se
  tratan como un caso especial, pues cuando indicas la opción --robust el
  método por defecto es el de la HCCM habitual, pero puedes utilizar la
  opción --robust-hac para forzar que se emplee un estimador HAC.

  robust_z: off (por defecto) u on. Esto controla la distribución que se
  utiliza cuando se calculan las probabilidades asociadas (valores p)
  basadas en las desviaciones típicas robustas, en el contexto de los
  estimadores de mínimos cuadrados. Por defecto, GRETL utiliza la
  distribución t de Student pero si activas robust_z, se utiliza una
  distribución Normal.

  hac_lag: nw1 (por defecto), nw2, nw3 o un entero. Establece el valor del
  retardo máximo o ancho de banda (p) utilizado cuando se calculan las
  desviaciones típicas HAC (Consistentes ante Heterocedasticidad y
  Autocorrelación) utilizando el enfoque de Newey-West, para datos de
  series temporales. Las opciones nw1 y nw2 representan dos variantes de
  cálculo automático basadas en el tamaño de la muestra T: para nw1, p =
  0.75 * T^(1/3), y para nw2, p = 4 * (T/100)^(2/9). La nw3 solicita una
  elección del ancho de banda que se basa en los datos. Consulta también
  más abajo qs_bandwidth y hac_prewhiten.

  hac_kernel: bartlett (por defecto), parzen o qs (Espectral cuadrado).
  Establece el 'kernel', o patrón de ponderaciones, que se utiliza cuando
  se calculan las desviaciones típicas HAC.

  hac_prewhiten: on u off (por defecto). Utiliza el 'blanqueo' previo y la
  'vuelta a colorear' de Andrews-Monahan cuando se calculan las desviaciones
  típicas HAC. Esto también implica utilizar una elección del ancho de
  banda que se basa en los datos.

  hac_missvals: es (predeterminado), am u off. Establece la política
  respecto al cálculo de las desviaciones típicas HAC cuando la muestra
  utilizada en la estimación incluye observaciones incompletas: es invoca
  el método de la Misma Separación (Equal Spacing) de Datta y Du (2012);
  am elige el método de la Modulación de la Amplitud (Amplitude
  Modulation) de Parzen (1963); y off provoca que GRETL rechace dicha
  estimación. Para obtener más detalles, consulta El manual de gretl
  (Capítulo 22).

  hc_version: 0, 1, 2, 3 o 3a. Establece la variante que se usa al calcular
  las desviaciones típicas Consistentes ante Heterocedasticidad (HC) con
  datos de sección cruzada. Las 4 primeras opciones se corresponden con
  HC0, HC1, HC2 y HC3 discutidas por Davidson y MacKinnon en el capítulo 5
  de Econometric Theory and Methods. HC0 produce las denominadas
  habitualmente como "desviaciones típicas de White". La variante 3a es el
  procedimiento de la "navaja" de MacKinnon-White. A configuración por
  defecto habitualmente es 1, pero esto se puede cambiar en el cliente GUI,
  seleccionando la opción "HCCME" en el menú
  "Herramientas/Preferencias/General". Ten en cuenta que una configuración
  realizada mediante GUI persiste a lo largo de las sesiones de GRETL, en
  oposición al uso de la instrucción set que únicamente va a afectar a la
  sesión vigente.

  panel_robust: arellano (por defecto), pcse o scc. Esto selecciona el
  estimador de la matriz de covarianzas robustas para ser utilizado con los
  modelos con datos de panel. Consulta a instrución "panel" e o El manual
  de gretl (Capítulo 22) para obter máis detalles.

  qs_bandwidth: Ancho de banda para la estimación HAC en caso de que
  selecciones el kernel Espectral Cuadrado (QS). (A diferencia de los
  'kernels' de Bartlett y de Parzen, el ancho de banda QS no requiere ser un
  entero.)

Series temporales

  horizon: Un entero (por defecto se basa en la frecuencia de los datos).
  Establece el horizonte para las respuestas al impulso y las
  descomposiciones de la varianza de predicción en el contexto de
  autorregresiones de vectores.

  vecm_norm: phillips (por defecto), diag, first o none. Usada en el
  contexto de la estimación VECM mediante la instrucción "vecm" para
  identificar los vectores de cointegración. Consulta El manual de gretl
  (Capítulo 33) para obtener más detalles.

  boot_iters: Un entero, B. Establece el número de iteracciones 'bootstrap'
  que se usan cuando se calculan funciones de respuesta al impulso con
  intervalos de confianza. El valor por defecto es 1999. Es recomendable que
  B + 1 sea siempre divisible por 100α/2 de forma que, por ejemplo con α =
  0.1, B+1 debería ser múltiplo de 5. El mínimo valor aceptable para B es
  499.

Interacción con R

  R_lib: on (por defecto) u off. Cuando se envían instrucciones para que
  las ejecute R, utiliza la biblioteca compartida de R mejor que el
  ejecutable de R, si la biblioteca está disponible.

  R_functions: off (por defecto) u on. Reconoce funciones definidas en R
  como si fueran funciones propias (para eso se requiere el prefijo de
  asignación de nombres "R."). Consulta El manual de gretl (Capítulo 44)
  para obtener más detalles sobre este elemento y el anterior.

Miscelánea

  mpi_use_smt: on o bien off (por defecto). Este interruptor afecta al
  número de procesos que se inician en un bloque mpi dentro de un guion. Si
  el interruptor está en off, la cantidad por defecto de estos procesos es
  igual al número de núcleos físicos de la máquina local; si está en
  on, la cantidad por defecto de estos procesos es igual al número máximo
  de subprocesos, que coincidirá con el doble del número de núcleos
  físicos si estos pueden soportar SMT (Multiproceso Simultáneo, también
  denominado Hiperproceso). Esto se aplica únicamente si el usuario no
  indica el número de procesos, bien de forma directa o bien indirectamente
  (mediante la especificación de un archivo hosts para utilizar con MPI).

  graph_theme: una cadena de texto a elegir entre altpoints, classic, dark2
  (la vigente por defecto), ethan, iwanthue o sober. Esto establece el
  "tema" que se utiliza para los gráficos que genera GRETL. La opción
  classic supone volver al sencillo tema que estaba vigente con antelación
  a la versión 2020c de GRETL.

# setinfo Dataset

Argumento:  serie 
Opciones:   --description=cadena (Establece la descripción)
            --graph-name=cadena (Establece el nombre del gráfico)
            --discrete (Marca la serie como discreta)
            --continuous (Marca la serie como continua)
            --coded (Marca como una codificación)
            --numeric (Marca como no codificación)
            --midas (Marca como componente de datos de alta frecuencia)
Ejemplos:   setinfo x1 --description="Descripción de x1"
            setinfo y --graph-name="Alguna cadena"
            setinfo z --discrete

Si activas las opciones --description o --graph-name, el argumento debe ser
una única serie; de lo contrario, podrá ser una lista de series, en cuyo
caso la instrucción funciona sobre todos los elementos de la lista. Esta
instrucción configura 4 atributos como se indica a continuación.

Cuando indicas la opción --description seguida de una cadena de texto entre
comillas, esa cadena se utiliza para establecer la etiqueta descriptiva de
la variable. Esta etiqueta se muestra en respuesta a la instrucción
"labels", y también se muestra en la ventana principal del programa de
Interfaz Gráfica de Usuario (GUI).

Cuando especificas la opción --graph-name seguida de una cadena de texto
entre comillas, esa cadena se va a utilizar en los gráficos en lugar del
nombre de la variable.

Cuando indicas uno de los dos indicadores de opción --discrete o
--continuous, el carácter numérico de la variable se establece en
consonancia con eso. Por defecto, se tratan todas las series como continuas,
entonces determinar que una serie sea discreta va a afectar al modo en que
se maneja la variable en otras instrucciones y funciones, como por ejemplo
con "freq" o con "dummify".

Cuando indicas alguna de las dos opciones --coded o --numeric, el status de
la serie indicada se establece de acuerdo con eso. Por defecto, se tratan
todos los valores numéricos como que tienen sentido como tales, por lo
menos en la acepción habitual; pero establecer que una serie es coded
quiere decir que los valores numéricos son una codificación arbitraria de
características cualitativas.

La opción --midas establece una indicación que alude a que una determinada
serie contiene datos de una frecuencia mayor que la frecuencia base del
conjunto de datos; por ejemplo, si el conjunto de datos es trimestral, y las
series contienen valores para el mes 1, 2 o 3 de cada trimestre. (MIDAS =
Mixed Data Sampling.)

Menú gráfico: /Variable/Editar atributos
Otro acceso:  Ventana principal: Menú emergente

# setmiss Dataset

Argumentos: valor [ listavariables ] 
Ejemplos:   setmiss -1
            setmiss 100 x2

Permite que el programa interprete algún valor específico de dato
numérico (el primer parámetro de la instrucción) como un código para
"ausente", en caso de importar datos. Cuando este valor es el único
parámetro (como en el primer ejemplo de arriba), esa interpretación se va
a aplicar a todas las series del conjunto de datos. Cuando "valor" va
seguido de una lista de variables (indicadas por nombre o número), la
interpretación se limita a la(s) variable(s) especificada(s). Así, en el
segundo ejemplo, el valor 100 de los datos se interpreta como un código
para "ausente", pero solo para la variable x2.

Menú gráfico: /Datos/Establecer código de valor ausente

# setobs Dataset

Variantes:  setobs periodicidad obsinicio
            setobs varunidades vartiempo --panel-vars
Opciones:   --cross-section (Interpreta como de sección cruzada)
            --time-series (Interpreta como serie temporal)
            --special-time-series (Mira abajo)
            --stacked-cross-section (Interpreta como datos de panel)
            --stacked-time-series (Interpreta como datos de panel)
            --panel-vars (Utiliza variables índice, mira abajo)
            --panel-time (Mira abajo)
            --panel-groups (Mira abajo)
Ejemplos:   setobs 4 1990:1 --time-series
            setobs 12 1978:03
            setobs 1 1 --cross-section
            setobs 20 1:1 --stacked-time-series
            setobs unit year --panel-vars

Esta instrucción fuerza al programa a interpretar que el conjunto de datos
tiene una estructura específica.

En la primera forma de la instrucción, debes indicar la periodicidad
mediante un entero que represente la frecuencia en caso de que los datos
sean series temporales (1 = anuales; 4 = trimestrales; 12 = mensuales; 52 =
semanales; 5, 6, o 7 = diarios; 24 = horarios). En caso de datos de panel,
la periodicidad indica el número de líneas por bloque de datos; por lo
tanto, esto expresa o bien el número de unidades consecutivas cuando
indicas que son 'secciones cruzadas apiladas', o bien el número de
períodos de tiempo consecutivos cuando indicas 'series de tiempo apiladas'.
En caso de datos simples de sección cruzada, la periodicidad debe
establecerse en 1.

La observación de inicio representa la fecha inicial, en caso de tratarse
de datos de series temporales. Puedes indicar los años mediante 2 o 4
dígitos; y debes separar los subperíodos (por ejemplo, trimestres o meses)
del año mediante dos puntos. En caso de datos de panel, debes indicar la
observación inicial como 1:1, y en caso de datos de sección cruzada, como
1. Debes indicar las observaciones iniciales para datos diarios o semanales
con el formato YYYY-MM-DD (o simplemente como 1 para datos sin fechar).

Algunas periodicidades de series temporales tienen interpretaciones
estándar (por ejemplo, 12 = mensuales y 4 = trimestrales). Pero si tienes
datos de series temporales poco habituales para las que no se aplica la
interpretación estándar, puedes señalar esto indicando la opción
--special-time-series. En ese caso, GRETL no va a advertir de que tus datos
de (por ejemplo) frecuencia igual a 12, sean mensuales.

Cuando no seleccionas un indicador de opción explícito para determinar la
estructura de los datos, el programa va a tratar de adivinar la estructura a
partir de la información proporcionada.

La segunda forma de la instrucción (que requiere que indiques la opción
--panel-vars) puede utilizarse para imponer una interpretación de panel,
cuando el conjunto de datos contiene variables que identifican de forma
inequívoca las unidades de sección cruzada y los períodos de tiempo. El
conjunto de datos se va a ordenar como series de tiempo apiladas, en
función de los valores ascendentes de la variable de unidades
(varunidades).

Opciones específicas de Panel

Puedes usar opciones --panel-time y --panel-groups únicamente con un
conjunto de datos que ya fue definido previamente como un panel.

La intención de la opción --panel-time es determinar información
adicional relacionada con la dimensión temporal del panel. Debes indicar
esta siguiendo el patrón del primer formato de setobs apuntado más arriba.
Por ejemplo, puedes utilizar la siguiente forma de indicar que la dimensión
temporal de un panel es trimestral, comenzando en el primer trimestre de
1990:

	setobs 4 1990:1 --panel-time

La intención de la opción --panel-groups es crear una serie con valores en
cadenas de texto, que contenga los nombres de los grupos (individuos,
unidades atemporales) del panel. (Esto se va a utilizar cuando sea adecuado
en gráficos de panel.) Con esta opción indicas uno o dos argumentos, como
se indica a continuación.

Primer caso: Un único argumento es el nombre de una serie con valores en
cadenas de texto. Si el número de valores diferentes es igual al número de
grupos del panel, esa serie se utiliza para definir los nombres de los
grupos. Si resulta necesario, el contenido numérico de la serie se va a
ajustar de forma que los valores sean todos 1 para el primer grupo, todos 2
para el segundo grupo, etcétera. Cuando el número de valores diferentes en
cadenas de texto no coincide con el número de grupos, se muestra un fallo.

Segundo caso: El primer argumento es el nombre de una serie, y el segundo es
una cadena de texto literal o una variable de cadena que contiene un nombre
para cada grupo. Las series se van a generar si no existen ya. Cuando el
segundo argumento es una cadena de texto literal o una variable de cadena,
los nombres de los grupos deben estar separados por espacios; pero se un
nombre incluye espacios, debe delimitarse con comillas precedidas (cada una)
de barra inversa. Alternativamente, el segundo argumento puede ser un array
de cadenas de texto.

Por ejemplo, el siguiente código genera una serie que se va llamar Estado
en la que los nombres de la cadena cstrs se repiten cada uno T veces, y
siendo T la longitud de las series de tiempo del panel.

	string cstrs = sprintf("Francia Alemania Italia \"Reino Unido\"")
	setobs Estado cstrs --panel-groups

Menú gráfico: /Datos/Estructura del conjunto de datos

# setopt Programming

Argumentos: instrucción [ acción ] opciones 
Ejemplos:   setopt mle --hessian
            setopt ols persist --quiet
            setopt ols clear
            Ver también gdp_midas.inp

Esta instrucción permite la configuración previa de opciones para una
instrucción concreta. Normalmente esto no hace falta, pero puede ser útil
para los autores de funciones en HANSL, cuando quieren hacer que algunas
opciones de las instrucciones estén condicionadas al valor de un argumento
que proporcione quien las solicita.

Por ejemplo, supón que una función ofrece un interruptor booleano "quiet",
cuya intención es que se suprima la presentación de resultados de una
determinada regresión que se ejecuta dentro de la propia función. En ese
caso, se podría escribir:

	if quiet
	  setopt ols --quiet
	endif
	ols ...

Entonces, la opción --quiet se va a aplicar a la siguiente instrucción ols
únicamente si la variable quiet tiene un valor no nulo.

Por defecto, las opciones que se establecen de este modo solo se aplican a
la siguiente petición de la instrucción; por lo que no son persistentes.
Sin embargo, si indicas persist como valor para acción, las opciones se
continuarán aplicando a la instrucción indicada hasta nuevo aviso. El
'antídoto' a la acción persist es clear, pues este elimina cualquier
configuración guardada para la instrucción especificada.

Debes tener en cuenta que las opciones establecidas mediante setopt se
combinan con cualquier opción agregada directamente a la instrucción
apuntada. Así, por ejemplo, se puede añadir la opción --hessian a una
instrucción mle de forma incondicional, pero utilizar setopt para añadir
--quiet de forma condicional.

# shell Utilities

Argumento:  instrucshell 
Ejemplos:   ! ls -al
            ! dir c:\users
            launch notepad
            launch emacs myfile.txt

La prestación que se describe aquí no está activada por defecto. Mira
más abajo para los detalles.

Un signo de exclamación ("!") al comienzo de una línea de instrucción se
interpreta como una escapada del intérprete de usuario. Así puedes
ejecutar instrucciones del intérprete a tu antojo desde dentro de Gretl. El
argumento instrucshell se pasa a /bin/sh en sistemas de tipo Unix como Linux
y macOS, o a cmd.exe en MS Windows. Se ejecuta de forma sincrónica; es
decir, Gretl va a esperar a que se complete la instrucción antes de
proseguir. Si la instrucción da como resultado algún texto, este se
presenta en la consola o en la ventana de resultados de guiones.

Una variante del acceso síncrono con el intérprete, permite al usuario
"capturar" el resultado de una instrucción en una variable de cadena de
texto. Esto se puede lograr envolviendo la instrucción entre paréntesis,
precedidos por un signo dólar, como en

	string s = $(ls -l $HOME)

Por otro lado, la clave "launch", ejecuta un programa externo de forma
asíncrona (sin esperar a que se complete), como en el tercer y en el cuarto
ejemplos de arriba. Esto está pensado para abrir una aplicación en modo
interactivo. La RUTA del usuario se va a buscar para el ejecutable
especificado. En MS Windows, la instrucción se ejecuta directamente, sin
pasarla a cmd.exe (de ese modo las variables de entorno no se expanden
automáticamente).

Activación

Por razones de seguridad, la prestación de acceso con el intérprete no se
permite por defecto. Para activarla, marca el recuadro "Permitir
instrucciones de shell" bajo el menú Herramientas/Preferencias/General en
el programa de Interfaz Gráfica de usuario (GUI). Esto también hace que
estén disponibles las instrucciones del intérprete en el programa de
instrucciones en líneas (y resulta el único modo de hacerlo).

# smpl Dataset

Variantes:  smpl obsinicio obsfin
            smpl +i -j
            smpl varficticia --dummy
            smpl condición --restrict
            smpl --no-missing [ listavariables ]
            smpl --no-all-missing [ listavariables ]
            smpl --contiguous [ listavariables ]
            smpl n --random
            smpl full
            smpl
Opciones:   --dummy (El argumento es una variable ficticia)
            --restrict (Aplica una restricción booleana)
            --replace (Substituye cualquier restricción booleana existente)
            --no-missing (Limitarse a observaciones válidas)
            --no-all-missing (Omite observaciones vacías (mira abajo))
            --contiguous (Mira abajo)
            --random (Genera una submuestra aleatoria)
            --permanent (Mira abajo)
            --preserve-panel (Datos de panel: mira abajo)
            --unit (Datos de panel: muestra en la dimensión de sección cruzada)
            --time (Datos de panel: muestra en la dimensión temporal)
            --dates (Interpreta los números de observación como fechas)
            --quiet (No muestra el rango muestral)
Ejemplos:   smpl 3 10
            smpl 1960:2 1982:4
            smpl +1 -1
            smpl x > 3000 --restrict
            smpl y > 3000 --restrict --replace
            smpl 100 --random

Esta instrución solo puede utilizarse cuando está preparado un conjunto de
datos. Cuando no indicas ningún argumento, presenta el rango muestral
vigente; de outro modo, establece el rango muestral. Puedes definir el rango
de varias formas. En la primera alternativa (y en los dos primeros ejemplos)
de arriba, obsinicio y obsfin deben ser consistentes con la periodicidad de
los datos. Puedes substituir cualquiera de los dos mediante un punto y coma
para dejar ese valor sin cambiar. (Para más detalles sobre obsinicio y
obsfin, consulta la sección titulada "Fechas versus Índices secuenciales"
más abajo.) En la segunda forma, los números enteros i y j (pueden ser
positivos o negativos, y deben tener su signo) se consideran como
variaciones en relación al rango de la muestra existente. En la tercera
forma, varficticia debe ser una variable de señalización con valores 0 o 1
en cada observación; así la muestra se va a restringir a las observaciones
en las que el valor es 1. La cuarta forma, que utiliza --restrict, restringe
la muestra a las observaciones que cumplen la condición booleana que se
indica.

Puedes emplear las opciones --no-missing y --no-all-missing para excluir de
la muestra aquellas observaciones para las que hay ausencia de datos. La
primera variante excluye aquellas filas del conjunto de datos para las que,
por lo menos una variable, tiene un valor ausente; mientras que la segunda
variante excluye únicamente aquellas filas en las que todas las variables
tienen valores ausentes. En cada caso, la comprobación se limita a las
variables de listavariables cuando indicas este argumento; de lo contrario,
se aplica a todas las series (con la reserva de que, en caso de no tener
listavariables e indicar --no-all-missing, las variables genéricas index y
time se ignoran).

La opción --contiguous de smpl está pensada para usar con datos de series
temporales. Su efecto consiste en recortar cualquier observación al
comienzo y al final del rango de la muestra vigente que contenga valores
ausentes (bien para las variables de listavariables, o bien para todas las
series de datos si no indicas listavariables). Entonces se realiza una
verificación para comprobar si hay algún valor ausente en el rango que
queda; y si es así, se muestra un fallo.

Con la opción --random, el número de casos especificado se escoge
aleatoriamente del conjunto vigente de datos (sin substitución). Si quieres
ser capaz de replicar esa selección, debes establecer primero la semilla
para el generador de números aleatorios (consulta la instrucción "set").

La forma final (smpl full) restablece el rango completo de datos.

Ten en cuenta que las restricciones muestrales son, por defecto,
acumulativas; es decir, el punto de partida de cualquier instrucción smpl
es la muestra vigente. Si quieres que la instrucción actúe substituyendo
cualquier restricción ya existente, puedes añadir el indicador de opción
--replace al final de la instrucción. (Pero esta opción no es compatible
con la opción --contiguous.)

Puedes utilizar la variable interna obs junto con la opción --restrict de
smpl para excluir observaciones concretas de la muestra. Por ejemplo

	smpl obs!=4 --restrict

va a prescindir únicamente de la cuarta observación. Si los casos de los
datos se identifican mediante etiquetas,

	smpl obs!="USA" --restrict

va a prescindir de la observación con la etiqueta "USA".

Debe apuntarse una cuestión relacionada con las opciones --dummy,
--restrict y --no-missing de la instrucción smpl: la información
"estructural" del archivo de datos (relacionada con la naturaleza de series
de tiempo o de panel, de los datos) probablemente se va a perder cuando se
ejecute esta instrucción; pero puedes volver a imponer la estructura con la
instrucción "setobs" (consulta también la opción --preserve-panel más
abajo).

Fechas versus Índices secuenciales

Puedes utilizar la opción --dates para solucionar alguna posible
ambigüedad al interpretar obsinicio y obsfin en caso de usar datos de
series de tiempo anuales. Por ejemplo, ¿debería considerarse que 2010 se
refiere al año 2010, o a la dos mil décima observación? En la mayoría de
los casos, esto debiera salir bien automáticamente, pero puedes forzar la
interpretación en forma de fecha si lo necesitas. Esta opción también se
puede utilizar con datos que estén fechados diariamente para lograr que
smpl interprete, por ejemplo, 20100301 como el primero de marzo de 2010 en
lugar de un índice secuencial corriente. Ten en cuenta que esta ambigüedad
no aparece con las frecuencias de series de tiempo que sean distintas a la
anual y a la diaria; fechas como 1980:3 (tercer trimestre de 1980) y 2020:03
(marzo de 2020) no se pueden confundir con índices corrientes.

Opciones específicas para datos de panel

Las opciones --unit y --time son específicas para datos de panel. Te
permiten indicar, respectivamente, un rango de "unidades" o de períodos de
tiempo. Por ejemplo:

	# Limita la muestra a las primeras 50 unidades
	smpl 1 50 --unit
	# Limita la muestra a los períodos de tiempo de 2 a 20
	smpl 2 20 --time

Cuando se especifica la dimensión temporal de un conjunto de datos de panel
por medio de la instrucción "setobs" con la opción --panel-time, la
instrucción smpl con la opción --time puede expresarse en términos de
fechas en lugar de números de observación planos. Este es un ejemplo:

	# Indicar el tiempo de un panel como trimestral, empezando en el primero de 1990
	setobs 4 1990:1 --panel-time
	# Limitar la muestra desde 2000:1 hasta 2007:1
	smpl 2000:1 2007:1 --time

En GRETL, un conjunto de datos de panel debe estar siempre "teóricamente
equilibrado " -- es decir, cada unidad debe tener el mismo número de filas
de datos, aunque algunas filas no contengan más que NAs. Extraer una
submuestra mediante las opciones --restrict o --dummy puede destruir esta
estructura. En tal caso, puedes añadir la opción --preserve-panel para
solicitar que se reconstituya un panel teóricamente equilibrado, por medio
de la inserción de las "filas ausentes" que hagan falta.

Establecer la muestra como permanente o provisional

Por defecto, puedes deshacer las limitaciones que establezcas sobre el rango
de la muestra vigente, pues ejecutando smpl full puedes restaurar el
conjunto de datos completo. Sin embargo, puedes utilizar la opción
--permanent para sustituir el conjunto de datos restringido en lugar del
original. El efecto de indicar la opción --permanent sin otros argumentos
ni opciones, es el de reducir la base de datos al rango de la muestra
vigente.

Consulta El manual de gretl (Capítulo 5) para obtener otros detalles.

Menú gráfico: /Muestra

# spearman Statistics

Argumentos: serie1 serie2 
Opción:     --verbose (Presenta los datos por rangos)

Presenta el coeficiente de la correlación por rangos de Spearman para las
series serie1 y serie2. No tienes que jerarquizar manualmente las variables
por adelantado, pues la función ya se ocupa de ello.

La forma automática de jerarquizar es de mayor a menor (i.e. el valor más
grande de los datos adquiere el rango 1). Si necesitas invertir esta forma
de jerarquizar, genera una nueva variable que sea la negativa de la
original. Por ejemplo:

	series altx = -x
	spearman altx y

Menú gráfico: /Herramientas/Contrastes no paramétricos/Correlación

# square Transformations

Argumento:  listavariables 
Opción:     --cross (Genera los productos cruzados así como los cuadrados)

Genera nuevas series que son los cuadrados de las series de listavariables
(además de las variables con los productos cruzados entre cada dos, cuando
indicas la opción --cross). Por ejemplo, "square x y" va a generar las
variables sq_x = x al cuadrado, sq_y = y al cuadrado y (opcionalmente con
'cross') x_y = x por y. Cuando una determinada variable es una variable
ficticia, no se calcula su cuadrado pues obtendríamos la misma variable.

Menú gráfico: /Añadir/Cuadrados de las variables seleccionadas

# stdize Transformations

Argumento:  listavar 
Opciones:   --no-df-corr (Sin corrección de grados de libertad)
            --center-only (Sin dividir por desviación típica)

Por defecto, se obtiene una versión tipificada de cada una de las variables
de listavar, y cada resultado se guarda en una nueva serie con el prefijo
s_. Por ejemplo, la expresión "stdize x y" crea las nuevas series s_x y
s_y, cada una como resultado de centrar y dividir la original por su
desviación típica muestral (con la corrección de 1, en los grados de
libertad).

Cuando indicas la opción --no-df-corr, no se aplica ninguna corrección de
los graos de libertad en la desviación típica que se utiliza; será el
estimador máximo-verosímil. Si indicas la opción --center-only, las
series resultan de únicamente restar la media y, en ese caso, los nombres
de las resultantes tendrán el prefijo c_ en lugar de s_.

La funcionalidad de esta instrucción está disponible de forma en cierto
modo más flexible, mediante la función "stdize".

Menú gráfico: /Añadir/Tipificar las variables seleccionadas

# store Dataset

Argumentos: nombrearchivo [ listavariables ] 
Opciones:   --omit-obs (Mira abajo, sobre el formato CSV)
            --no-header (Mira abajo, sobre el formato CSV)
            --gnu-octave (Utiliza el formato GNU Octave)
            --gnu-R (Formato tratable con read.table)
            --gzipped[=nivel] (Aplica la compresión gzip)
            --jmulti (Utiliza el formato ASCII JMulti)
            --dat (Utiliza el formato ASCII PcGive)
            --decimal-comma (Utiliza la coma como carácter decimal)
            --database (Utiliza el formato de base de datos de GRETL)
            --overwrite (Mira abajo, sobre el formato de base de datos)
            --comment=cadena (Mira abajo)
            --matrix=nombrematriz (Mira abajo)

Guarda los datos en nombrearchivo. Por defecto, se guardan todas las series
ya definidas en ese momento, pero puedes utilizar el argumento
listavariables (opcional) para escoger un subconjunto de series. Si el
conjunto de datos es una submuestra, solo se guardan las observaciones del
rango vigente de la muestra.

El archivo resultante va a escribirse en el directorio ("workdir")
establecido en ese momento, excepto que la cadena nombrearchivo contenga una
especificación completa de la ruta.

Ten en cuenta que la instrucción store se comporta de modo especial en el
contexto de un "bucle progresivo"; consulta El manual de gretl (Capítulo
13) para obtener más detalles.

Formatos propios

Si nombrearchivo tiene extensión .gdt o .gtdb, ello implica que se guarden
los datos en uno de los formatos propios de GRETL. Además, si no indicas
una extensión, se considera implícitamente la .gdt, añadiéndose
automáticamente este sufijo. El formato gdt es de tipo XML, con opción de
compresión gzip; mientras que el formato gdtb es binario. El primero se
recomienda para conjuntos de datos de tamaño moderado (digamos, hasta
varios cientos de kilobytes de datos); con el formato binario es mucho mayor
la velocidad con conjuntos de datos muy grandes.

Cuando guardas los datos en formato gdt, puedes utilizar la opción
--gzipped para comprimirlos. El parámetro (opcional) de esta opción
controla el nivel de compresión (de 0 a 9): los valores mayores generan un
archivo más pequeño, pero la compresión lleva más tiempo. El nivel por
defecto es el 1; y un nivel de 0 significa que no se aplica ninguna
compresión.

Se admite un tipo especial de guardado "propio" en el programa de interfaz
GUI: si nombrearchivo tiene extensión .gretl y omites el argumento
listavariables, entonces se graba un archivo de sesión de GRETL. Este tipo
de archivos incluyen el conjunto de datos vigente junto con cualesquiera
objetos que tengan nombre, como modelos, gráficos y matrices.

Otros formatos

Hasta cierto punto, puedes controlar el formato en el que se escriben los
datos mediante la extensión o sufijo de nombrearchivo, como se indica a
continuación:

  .csv: Valores Separados por Comas (CSV).

  .txt o .asc: valores separados por espacios.

  .m: Formato matricial GNU Octave.

  .dta: Formato dta de Stata (versión 113).

Puedes usar los indicadores de opción relacionados con el formato mostrados
arriba para forzar la elección del formato, con independencia del nombre
del archivo (o para lograr que GRETL escriba en los formatos de PcGive o
JMulTi).

Opciones CSV

Los indicadores de opción --omit-obs y --no-header son específicos para
guardar datos en el formato CSV. Por defecto, si los datos son series
temporales o de panel, o si el conjunto de datos incluye marcadores
específicos de observación, el archivo resultante incluye una primera
columna que identifica las observaciones (e.g. por fecha). Cuando indicas la
opción --omit-obs, esta columna se omite. La opción --no-header elimina la
habitual representación de los nombres de las variables en el
encabezamiento de las columnas.

El indicador de opción --decimal-comma está también limitado a CSV. Su
efecto consiste en substituir el punto decimal con la coma decimal; y, por
añadido, se fuerza a que el separador de columnas sea el punto y coma, en
lugar de la coma.

Guardar en una base de datos

La posibilidad de guardar en el formato de base de datos de GRETL está
pensada construir conjuntos largos de series, con mezclas de frecuencias y
rangos de observaciones. En este momento, esta opción solo está disponible
para datos de series temporales de tipo anual, trimestral o mensual, o para
datos sin fecha (de sección cruzada). Una base de datos de GRETL toma la
forma de dos archivos: uno con sufijo .bin para guardar los datos en formato
binario, y un archivo de texto plano con el sufijo .idx para los metadatos.
Para indicar el nombre del archivo de salida en la linea de instrucciones,
debes indicar el sufijo .bin o no indicar ninguno.

Si se guarda en una base de datos que ya existe, el efecto por defecto
consiste en añadir series al contenido existente en la base de datos. En
este contexto, es un fallo que alguna de las series que se van a guardar
tenga el mismo nombre que alguna que ya está presente en la base de datos.
La opción --overwrite tiene como efecto que, si hay nombres de variables en
común, los datos recientemente guardados sustituyen a los valores previos.

La opción --comment está disponible cuando se guardan datos como base de
datos o como CSV. El parámetro que se requiere es una cadena en una línea,
entre comillas, ligada al indicador de opción mediante un signo de
igualdad. La cadena de texto se inserta como comentario en el archivo
índice de la base de datos o en el encabezamiento del CSV.

Escribir una matriz como conjunto de datos

La opción --matriz necesita un parámetro: el nombre de una matriz (que no
esté vacía). Entonces la consecuencia de la instrucción store es
efectivamente convertir la matriz en un conjunto de datos "en segundo
plano", y escribirlo como tal en un archivo. Las columnas de la matriz pasan
a ser series cuyos nombres se toman de los nombres adyacentes a las columnas
de la matriz (si los hay), o bien se asignan por defecto como v1, v2, etc.
Si la matriz tiene nombres adyacentes a las filas, estos se utilizan en el
conjunto de datos como "etiquetas de las observaciones".

Ten en cuenta que puedes escribir las matrices como tales en archivos,
consulta para ello la función "mwrite". Pero a veces te puede resultar
útil escribirlas en forma de conjuntos de datos.

Menú gráfico: /Archivo/Guardar datos; /Archivo/Exportar datos

# summary Statistics

Variantes:  summary [ listavariables ]
            summary --matrix=nombrematriz
Opciones:   --simple (Solo estadísticos básicos)
            --weight=wtvar (Variable de ponderación)
            --by=byvar (Mira abajo)
Ejemplos:   frontier.inp

En su primera forma, esta instrucción presenta un resumen estadístico de
las variables de listavariables, o de todas las variables del conjunto de
datos cuando omites listavariables. Por defecto, el resultado consiste en la
media, mediana, mínimo, máximo, desviación típica (sd), coeficiente de
variación (= sd/media), coeficiente de asimetría, exceso de curtosis,
percentiles del 5% y 95%, rango intercuartílico y número de observaciones
ausentes. Pero cuando indicas la opción --simple, el resultado se limita a
la media, la mediana, la desviación típica, el mínimo y el máximo.

Si indicas la opción --weight, en cuyo caso el parámetro wvar debería ser
el nombre de una serie que ofrezca las ponderaciones de cada observación,
los estadísticos se ponderan de acuerdo con ello.

Cuando indicas la opción --by, en cuyo caso el parámetro byvar debe ser el
nombre de una variable discreta, entonces se presentan los estadísticos
para las submuestras que se corresponden con los diferentes valores que toma
byvar. Por ejemplo, cuando byvar es una variable ficticia (binaria), se
presentan los estadísticos para los casos en los que byvar=0 y byvar=1.
Advertencia: En este momento, esta opción es incompatible con la opción
--weight.

Cuando indicas la forma alternativa, utilizando una matriz ya definida,
entonces se presenta el resumen estadístico para cada columna de la matriz.
En este caso, la opción --by no está disponible.

Puedes obtener, en forma de matriz, la tabla de estadísticos generada con
la instrucción summary, por medio del accesor "$result". Cuando indiques la
opción --by, se genera el efecto de este acceso unicamente si varlist
contiene una única serie.

Consulta también la función "aggregate" para ver otras formas más
flexibles de producción de estadísticos "factorizados".

Menú gráfico: /Ver/Estadísticos principales
Otro acceso:  Ventana principal: Menú emergente

# system Estimation

Variantes:  system method=estimador
            sysname <- system
Ejemplos:   "Klein Model 1" <- system
            system method=sur
            system method=3sls
            Ver también klein.inp, kmenta.inp, greene14_2.inp

Empieza un sistema de ecuaciones. Puedes indicar una de las dos formas de la
instrucción, dependiendo de si quieres guardar el sistema para estimarlo de
varias formas, o solo estimar el sistema una vez.

Para guardar el sistema debes asignarle un nombre como en el primer ejemplo
(si el nombre contiene espacios, debes delimitarlo con comillas). En este
caso, se estima el sistema utilizando la instrucción "estimate". Con un
sistema de ecuaciones ya guardado, puedes imponer restricciones (incluidas
restricciones entre ecuaciones) utilizando la instrucción "restrict".

Como alternativa, puedes especificar un estimador para el sistema utilizando
method= seguido de una cadena que identifique uno de los estimadores
admitidos: "ols" (Mínimos Cuadrados Ordinarios), "tsls" (Mínimos Cuadrados
en 2 Etapas) "sur" (Regresiones Aparentemente No Relacionadas), "3sls"
(Mínimos Cuadrados en 3 Etapas), "fiml" (Máxima Verosimilitud con
Información Total) o "liml" (Máxima Verosimilitud con Información
Limitada). En este caso, el sistema se estima una vez que esté completa su
definición.

Un sistema de ecuaciones se termina con la línea "end system". Dentro del
sistema pueden indicarse 4 tipos de enunciado, como los siguientes.

  "equation": Especifica una ecuación del sistema.

  "instr": Para estimar un sistema mediante Mínimos Cuadrados en 3 etapas,
  se indica una lista de instrumentos (mediante el nombre de la variable o
  su número). Alternativamente, puedes poner esta información en la línea
  "equation" usando la misma sintaxis que en la instrucción "tsls".

  "endog": Para un sistema de ecuaciones simultáneas, se indica una lista
  de variables endógenas. En principio, esto está pensado para utilizar
  con la estimación FIML, pero puedes utilizar este enfoque con Mínimos
  Cuadrados en 3 Etapas en lugar de indicar una lista "instr"; y entonces
  todas las variables que no se identifiquen como endógenas, se van a
  utilizar como instrumentos.

  "identity": Para utilizar con Máxima Verosimilitud con Información
  Completa (MVIC, FIML), se indica una identidad que enlaza dos o más
  variables del sistema. Este tipo de enunciado se ignora cuando se utiliza
  un estimador diferente al de MVIC.

Después de hacer la estimación utilizando las instrucciones "system" o
"estimate", puedes usar los siguientes accesores para recoger información
adicional:

  $uhat: Matriz con los errores de estimación, con una columna por
  ecuación.

  $yhat: Matriz con los valores ajustados, con una columna por ecuación.

  $coeff: Vector columna con los coeficientes de las ecuaciones (todos los
  coeficientes de la primera ecuación, seguidos por los de la segunda
  ecuación, etcétera).

  $vcv: Matriz con las covarianzas entre los coeficientes. Cuando hay k
  elementos en el vector $coeff, esta matriz tiene una dimensión de k por
  k.

  $sigma: Matriz con las covarianzas entre los errores de estimación de las
  ecuaciones cruzadas.

  $sysGamma, $sysA y $sysB: Matrices con los coeficientes en la forma
  estructural (mira abajo).

Si quieres recuperar los errores de estimación o los valores ajustados para
una ecuación en concreto, en forma de serie de datos, escoge una columna de
la matriz $uhat o $yhat, y asígnale la serie como en

	series uh1 = $uhat[,1]

Las matrices en la forma estructural se corresponden con la siguiente
representación de un modelo de ecuaciones simultáneas:

  Gamma y(t) = A y(t-1) + B x(t) + e(t)

Si hay n variables endógenas y k variables exógenas, Gamma es una matriz
de dimensión n x n y B es n x k. Cuando el sistema no contiene ningún
retardo de las variables endógenas, entonces la matriz A no está presente.
Si el retardo máximo de un regresor endógeno es p, la matriz A es de
dimensión n x np.

Menú gráfico: /Modelo/Ecuaciones simultáneas

# tabprint Printing

Opciones:   --output=nombrearchivo (Envía el resultado al archivo especificado)
            --format="f1|f2|f3|f4" (Especifica el formato TeX personalizado)
            --complete (Relacionado con TeX, mira abajo)

Debe ir después de la estimación de un modelo y presenta ese modelo en
formato de tabla. El formato se rige por la extensión del nombrearchivo
especificado: ".tex" para LaTeX, ".rtf" para RTF (Microsoft's Rich Text
Format) o ".csv" para el formato con separación mediante comas. El archivo
resultante va a escribirse en el directorio vigente ("workdir"), excepto que
la cadena nombrearchivo contenga una especificación completa de la ruta.

Cuando seleccionas el formato CSV, los valores se separan con comas excepto
que la coma decimal esté en vigor, en cuyo caso el separador es el punto y
coma.

Opciones específicas de resultados en LaTeX

Cuando indicas la opción --complete, el archivo LaTeX es un documento
completo, listo para procesar; de lo contrario, debe incluirse en un
documento.

Si quieres modificar la apariencia del resultado tabular, puedes especificar
un formato personalizado en filas utilizando la opción --format. La cadena
de formato debe estar puesta entre comillas y debe estar ligada a la opción
con un signo de igualdad. El patrón para las cadenas de formato es el
siguiente. Existen 4 campos que representan: el coeficiente, la desviación
típica, el ratio t y la probabilidad asociada, respectivamente. Debes
separar estos campos mediante barras verticales; y, o bien pueden tener una
especificación de tipo printf para el formato del valor numérico en
cuestión, o bien pueden dejarse en blanco para eliminar la presentación de
esa columna (sujeto esto a la condición de que no puedes dejar todas las
columnas en blanco). Aquí tienes unos pocos ejemplos:

	--format="%.4f|%.4f|%.4f|%.4f"
	--format="%.4f|%.4f|%.3f|"
	--format="%.5f|%.4f||%.4f"
	--format="%.8g|%.8g||%.4f"

La primera de estas especificaciones presenta los valores de todas las
columnas usando 4 dígitos decimales. La segunda elimina la probabilidad
asociada y presenta las razones t con 3 dígitos decimales. La tercera omite
el ratio t. La última también omite el t, y presenta tanto el coeficiente
como la desviación típica con 8 cifras significativas.

Una vez que estableces un formato personalizado de este modo, este se
recuerda y se utiliza a lo largo de lo que dure la sesión de GRETL. Para
revertir esto al formato por defecto, puedes utilizar la variante especial
--format=default.

Menú gráfico: Ventana de modelo: LaTeX

# textplot Graphs

Argumento:  listavariables 
Opciones:   --time-series (Gráfico por observación)
            --one-scale (Fuerza una escala única)
            --tall (Usa 40 filas)

Gráficos ASCII rápidos y sencillos. Sin la opción --time-series,
listavariables debe contener al menos 2 series, la última de ellas se toma
como la variable para el eje x, y se genera un gráfico de dispersión. En
este caso, puedes utilizar la opción --tall para generar un gráfico en la
que el eje y se representa mediante 40 filas de caracteres (por defecto son
20 filas).

Con la opción --time-series, se genera un gráfico por observación. En
este caso, puedes utilizar la opción --one-scale para forzar el uso de una
escala única; de lo contrario, si listavariables contiene más de una
serie, los datos pueden escalarse. Cada línea representa una observación,
con los valores de los datos dibujados horizontalmente.

Consulta también "gnuplot".

# tobit Estimation

Argumentos: depvar indepvars 
Opciones:   --llimit=cotaizq (Especifica la cota de la izquierda)
            --rlimit=cotader (Especifica la cota de la derecha)
            --vcv (Presenta la matriz de covarianzas)
            --robust (Desviaciones típicas robustas)
            --opg (Mira más abajo)
            --cluster=clustvar (Consulta "logit" para más explicaciones)
            --verbose (Presenta los detalles de las iteraciones)
            --quiet (No presenta los resultados)

Estima un modelo Tobit, que puede ser lo adecuado cuando la variable
dependiente está "censurada". Por ejemplo, cuando se observan valores
positivos y nulos en la adquisición de bienes duraderos por parte de los
hogares, y ningún valor negativo, puede llegar a pensarse que las
decisiones sobre esas compras son el resultado de una disposición
subyacente e inobservada a comprar, que puede ser negativa en algunos casos.

Por defecto, se asume que la variable dependiente está 'censurada' en el
cero por la izquierda, y que no está 'censurada' por la derecha. Sin
embargo, puedes usar las opciones --llimit y --rlimit para especificar un
patrón diferente para hacer la 'censura'. Ten en cuenta que si especificas
únicamente una cota por la derecha, entonces lo que se supone es que la
variable dependiente no está 'censurada' por la izquierda.

El modelo Tobit es un caso especial de la regresión de intervalos. Consulta
la instrucción "intreg" para obtener detalles adicionales, incluída una
explicación de las opciones --robust y --opg.

Menú gráfico: /Modelo/Variable dependiente limitada/Tobit

# tsls Estimation

Argumentos: depvar indepvars ; instrumentos 
Opciones:   --no-tests (No hace contrastes de diagnóstico)
            --vcv (Presenta la matriz de covarianzas)
            --quiet (No presenta los resultados)
            --no-df-corr (Sin corrección de los grados de libertad)
            --robust (Desviaciones típicas robustas)
            --cluster=clustvar (Desviaciones típicas agrupadas)
            --matrix-diff (Calcula el contraste de Hausman mediante diferencia de matrices)
            --liml (Utiliza Máxima Verosimilitud con Información Limitada)
            --gmm (Utiliza el Método Generalizado de los Momentos)
Ejemplos:   tsls y1 0 y2 y3 x1 x2 ; 0 x1 x2 x3 x4 x5 x6
            Ver también penngrow.inp

Calcula las estimaciones de Variables Instrumentales (VI), utilizando por
defecto Mínimos Cuadrados en 2 Etapas (TSLS), pero mira más abajo para
otras opciones. La variable dependiente es depvar, mientras que indepvars
expresa una lista de regresores (se presupone que incluye al menos una
variable endógena), e instrumentos indica una lista de instrumentos
(variables exógenas y/o predeterminadas). Si la lista instrumentos no es al
menos tan larga como indepvars, el modelo no está identificado.

En el ejemplo de arriba, las ys son las variables endógenas y las xs son
las variables exógenas. Ten en cuenta que los regresores exógenos deben
aparecer en ambas listas.

Para obtener más detalles en relación a los efectos de las opciones
--robust y --cluster, consulta la ayuda para "ols".

Contrastes específicos para MC2E

El resultado de las estimaciones de Mínimos Cuadrados en 2 Etapas incluyen
el contraste de Hausman y (si el modelo está sobreidentificado) el
contraste de sobreidentificación de Sargan. Para una buena explicación de
los dos contrastes, consulta el capítulo 8 de Davidson y MacKinnon (2004).

En el contraste de Hausman, la hipótesis nula es que las estimaciones MCO
son consistentes o, en otras palabras, que la estimación por medio de
variables instrumentales en realidad no es necesaria. Por defecto, este
contraste se aplica por el método de regresión pero si indicas la opción
--matrix-diff se utiliza el método de Papadopoulos (2023). En ambos casos
puedes emplear una variante robusta si indicas la opción --robust.

Un modelo de este tipo está sobreidentificado si hay más instrumentos de
los estrictamente requeridos. El contraste de sobreidentificación de Sargan
(Sargan, 1958) se basa en una regresión auxiliar de los errores de la
estimación del modelo, mediante Mínimos Cuadrados en 2 Etapas sobre la
lista completa de instrumentos. La hipótesis nula sostiene que todos los
instrumentos son válidos; pero se sospecha de la validez de esta hipótesis
si la regresión auxiliar tiene un grado de poder explicativo que es
significativo.

Estos estadísticos están disponibles luego de la conclusión satisfactoria
de la instrucción, con los nombres "$hausman" y "$sargan" (si es
aplicable), respectivamente.

Instrumentos débiles

Tanto para MC2E (TSLS) como para la estimación MVIL (LIML), se muestra el
resultado de un contraste adicional, puesto que el modelo se estima bajo el
supuesto de perturbaciones IID (es decir, no se escoge la opción --robust).
Este es un contraste de la debilidad de los instrumentos, pues instrumentos
débiles pueden llevar a serios problemas en la regresión de VI:
estimaciones sesgadas y/o tamaño incorrecto de los contrastes de hipótesis
basados en la matriz de covarianzas, con tasas de rechazo que superan mucho
el nivel de significación nominal (Stock, Wright y Yogo, 2002). El
estadístico es el del contraste F de la primera etapa si el modelo tiene
tan solo un regresor endógeno; de lo contrario, es el valor propio más
pequeño de la matriz de contrapartida del F de la primera etapa. Se
muestran los puntos críticos basados en el análisis Monte Carlo de Stock y
Yogo (2003), cuando estén disponibles.

R-cuadrado

El valor de R-cuadrado que se presenta para modelos estimados mediante
Mínimos Cuadrados en 2 Etapas es el cuadrado de la correlación entre la
variable dependiente y la variable con los valores ajustados.

Estimadores alternativos

Como alternativas a MC2E, el modelo puede estimarse mediante Máxima
Verosimilitud con Información Limitada (opción --liml) o mediante el
Método Generalizado de los Momentos (opción --gmm). Ten en cuenta que, si
el modelo está simplemente identificado, estos métodos deberían generar
los mismos resultados que MC2E; pero si está sobreidentificado, los
resultados en general van a diferir.

Cuando se escoge la estimación MGM (GMM), las siguientes opciones
adicionales pasan a estar disponibles:

  --two-step: Realiza MGM en 2 etapas en lugar de hacerlo en 1 etapa (por
  defecto).

  --iterate: Reitera MGM hasta la convergencia.

  --weights=Wmat: Especifica una matriz cuadrada de ponderaciones para
  utilizar cuando se calcula la función del criterio MGM. La dimensión de
  esta matriz debe ser igual al número de instrumentos. Por defecto, es una
  matriz identidad de dimensión adecuada.

Menú gráfico: /Modelo/Variables instrumentales

# tsplots Graphs

Argumento:  listavar 
Opciones:   --matrix=nombre (Representa las columnas de la matriz citada)
            --output=nombrearch (Envía el resultado al archivo indicado)
Ejemplos:   tsplots 1 2 3 4
            tsplots 1 2 3 4 --matrix=X

Proporciona un modo sencillo de representar gráficamente múltiples series
temporales (hasta un máximo de 16) en un único cuadro. Puedes indicar el
argumento listavar como una lista de números ID (o de nombres de series), o
como números de columnas en caso de introducir una matriz.

Consulta también "scatters" para ver formas de generar múltiples gráficos
de dispersión, y "gridplot" para ver un modo más flexible de combinar
gráficos en una parrilla.

Menú gráfico: /Ver/Gráficos múltiples/Series temporales

# var Estimation

Argumentos: orden ylista [ ; xlista ] 
Opciones:   --nc (Sin constante)
            --trend (Con tendencia lineal)
            --seasonals (Con variables ficticias estacionales)
            --robust (Desviaciones típicas robustas)
            --robust-hac (Desviaciones típicas HAC)
            --quiet (No muestra los resultados de las ecuaciones individuales)
            --silent (No presenta nada)
            --impulse-responses (Presenta las respuestas al impulso)
            --variance-decomp (Presenta las descomposiciones de la varianza)
            --lagselect (Muestra los criterios de selección de retardos)
            --minlag=retardo mínimo (Solo selección de retardo, mira abajo)
Ejemplos:   var 4 x1 x2 x3 ; time mydum
            var 4 x1 x2 x3 --seasonals
            var 12 x1 x2 x3 --lagselect
            Ver también sw_ch14.inp

Establece y estima (utilizando MCO) una autorregresión de vectores (VAR).
El primer argumento especifica el orden de retardos (o el orden máximo de
retardos, en caso de que indiques la opción --lagselect, mira más abajo).
El orden puedes indicarlo numéricamente o con el nombre de una variable
escalar preexistente. A continuación sigue la configuración de la primera
ecuación. No incluyas retardos entre los elementos de ylista pues se van a
añadir automáticamente. El punto y coma va a separar las variables
estocásticas (para las que se va a incluir un orden de retardos) de
cualquier variable exógena de xlista. Ten en cuenta que: (a) se incluye una
constante automáticamente (excepto que indiques la opción --nc), (b)
puedes añadir una tendencia con la opción --trend, y (c) puedes añadir
variables ficticias estacionales utilizando la opción --seasonals.

Mientras que una especificación VAR habitualmente incluye todos los
retardos desde 1 hasta el máximo que indiques, también puedes escoger un
grupo de retardos. Para hacer esto, substituye el argumento rutinario orden
(escalar), bien con el nombre de un vector ya definido previamente, o bien
con una lista de retardos separados con comas y colocada entre llaves.
Debajo se muestran dos modos de especificar que un VAR debe incluir los
retardos 1, 2 y 4 (pero no el 3):

	var {1,2,4} ylista
	matrix p = {1,2,4}
	var p ylista

Se desenvuelve una regresión por separado para cada una de las variables de
ylista. Los resultados para cada ecuación incluyen los contrastes F para
restricciones cero en todos los retardos de cada una de las variables, un
contraste F sobre la significación del retardo máximo y, cuando
especificas la opción --impulse-responses, las descomposiciones de la
varianza de la predicción y las respuestas al impulso.

Las descomposiciones de la varianza de la predicción y las respuestas al
impulso se basan en la descomposición de Cholesky de la matriz de
covarianzas contemporánea y, en este contexto, tiene importancia el orden
en el que indicas las variables (estocásticas). Así, la primera variable
de la lista se asume que es la "más exógena" dentro del período. Puedes
establecer el horizonte para las descomposiciones de la varianza y las
respuestas al impulso, utilizando la instrucción "set". Para recuperar una
función concreta de respuesta al impulso en forma matricial, consulta la
función "irf".

Cuando indicas la opción --robust, las desviaciones típicas se corrigen
del efecto de la heterocedasticidad. Como alternativa, puedes indicar la
opción --robust-hac para dar lugar a desviaciones típicas que sean
robustas con respecto tanto a la heterocedasticidad como a la
autocorrelación (HAC). En general, esta última corrección no debiera de
ser necesaria si el VAR incluye un número suficiente de retardos.

Selección de retardo

Cuando indicas la opción --lagselect, no se presenta el resultado habitual
del VAR. En su lugar, se toma el primer argumento como orden máximo de
retardo, y la salida de resultados consiste en una tabla que muestra cifras
comparativas calculadas para VARs que van desde el orden 1 (predeterminado)
hasta el máximo especificado. La tabla incluye el logaritmo de la
verosimilitud y el valor P de un contraste de Razón de Verosimilitudes (RV
o LR), seguidos de los Criterios de Información de Akaike (AIC), de Schwarz
(BIC) y de Hannan-Quinn (HQC). El contraste RV (LR) compara la
especificación de la fila i con la de la fila i - 1, considerando como
hipótesis nula que todos los parámetros añadidos en la fila i tienen
valores nulos. Puedes recuperar la tabla con los resultados, en formato de
matriz, mediante el accesor "$test".

Únicamente en el contexto de la selección de retardo, puedes usar la
opción --minlag para ajustar el nivel mínimo de retardos. Ajústalo a 0
para permitir la posibilidad de que el nivel óptimo de retardos sea cero
(queriendo ello decir realmente que no se requiere un VAR para nada). Por el
contrario, podrías hacer que --minlag=4 si crees que necesitas 4 retardos
por lo menos, de forma que se ahorre un poco de tiempo de cálculo.

Menú gráfico: /Modelo/Series temporales multivariantes

# varlist Dataset

Opción:     --type=nombretipo (Campo del listado)

Por defecto, presenta un listado de las series del conjunto vigente de datos
(si hay alguna); y puedes utilizar "ls" como alias.

Cuando indicas la opción --type, debe ir seguida (después de un signo de
igualdad) por uno de los siguientes tipos: series, scalar, matrix, list,
string, bundle, array o accessor. Su efecto consiste en presentar los
nombres de todos los objetos del tipo indicado que estén definidos en ese
momento.

Como caso especial, si el tipo es accessor, los nombres que se presentan son
aquellos de las variables internas disponibles en ese momento como
"accesores", como pueden ser "$nobs" y "$uhat" (sean cuales sean sus tipos
concretos).

# vartest Tests

Argumentos: serie1 serie2 

Calcula el estadístico F para contrastar la hipótesis nula de que las
varianzas poblacionales de las variables serie1 y serie2 son iguales, y
muestra su probabilidad asociada (valor p). Puedes obtener las estadísticas
del contraste y el valor p mediante los accesores "$test" y "$pvalue",
respectivamente. El siguiente código

		open AWM18.gdt
		vartest EEN EXR
		eval $test
		eval $pvalue

calcula el contraste, y muestra como recuperar más tarde el estadístico de
contraste y el valor p correspondiente:

		Contraste de igualdad de varianzas

		EEN: Número de observaciones = 192
		EXR: Número de observaciones = 188
		Cociente entre varianzas muestrales = 3.70707
		Hipótesis nula: Las dos varianzas poblacionales son iguales
		Estadístico de contraste: F(191,187) = 3.70707
		valor p (a dos colas) = 1.94866e-18

		3.7070716
		1.9486605e-18

Menú gráfico: /Herramientas/Calculadora de estadísticos de contraste

# vecm Estimation

Argumentos: orden rango ylista [ ; xlista ] [ ; rxlista ] 
Opciones:   --nc (Sin constante)
            --rc (Constante restringida)
            --uc (Constante no restringida)
            --crt (Constante y tendencia restringida)
            --ct (Constante y tendencia no restringida)
            --seasonals (Incluye variables ficticias estacionales centradas)
            --quiet (No muestra los resultados de las ecuaciones individuales)
            --silent (No presenta nada)
            --impulse-responses (Presenta las respuestas al impulso)
            --variance-decomp (Presenta las descomposiciones de la varianza)
Ejemplos:   vecm 4 1 Y1 Y2 Y3
            vecm 3 2 Y1 Y2 Y3 --rc
            vecm 3 2 Y1 Y2 Y3 ; X1 --rc
            Ver también denmark.inp, hamilton.inp

Un VECM es una forma de autorregresión de vectores o VAR (consulta "var"),
aplicable cuando las variables del modelo son individualmente integradas de
orden 1 (por lo tanto son paseos aleatorios, con o sin deriva) pero
presentan cointegración. Esta instrucción está íntimamente relacionada
con el contraste de cointegración de Johansen (consulta "johansen").

El parámetro orden de esta instrucción representa el orden de retardos del
sistema VAR. El número de retardos en el propio VECM (donde la variable
dependiente se indica como una primera diferencia) es de uno menos que
orden.

El parámetro rango representa el rango de cointegración o, en otras
palabras, el número de vectores cointegrantes. Este debe ser mayor que
cero, y menor o igual (generalmente menor) que el número de variables
endógenas indicadas en ylista.

El argumento ylista proporciona la lista de variables endógenas, expresadas
en niveles. La inclusión de términos de tipo determinístico en el modelo,
se controla con los indicadores de opción. Por defecto, cuando no indicas
ninguna opción, se incluye una "Constante no restringida", lo que permite
que haya una ordenada en el origen no nula en las relaciones de
cointegración, así como una tendencia en los niveles de las variables
endógenas. La literatura derivada del trabajo de Johansen (por ejemplo,
puedes consultar su libro de 1995) habitualmente se refiere a esto como el
"caso 3". Las primeras 4 opciones indicadas arriba (mutuamente excluyentes)
generan los casos 1, 2, 4 y 5, respectivamente. Los significados de estos
casos y los criterios que se usan para escoger un caso, se explican en El
manual de gretl (Capítulo 33).

Las listas (opcionales) xlista y rxlista te permiten especificar conjuntos
de variables exógenas que forman parte del modelo, bien sin restricciones
(xlista) o bien restringidas al espacio de cointegración (rxlista). Estas
listas se separan de ylista y unas de las otras, mediante punto y coma.

La opción --seasonals, que puedes combinar con cualquiera de las otras
opciones, especifica la inclusión de un conjunto de variables ficticias
estacionales centradas. Esta opción únicamente está disponible para datos
trimestrales o mensuales.

El primer ejemplo de arriba especifica un VECM, con un orden de retardos de
4 y un único vector de cointegración. Las variables endógenas son Y1, Y2
e Y3. El segundo ejemplo usa las mismas variables pero especifica un orden
de retardos de 3, y dos vectores de cointegración; también especifica una
"Constante restringida", que es adecuada cuando los vectores de
cointegración pueden tener ordenada en el origen no nula pero las variables
Y no tienen tendencia.

A continuación de la estimación de un VECM, tienes disponibles algunos
accesores especiales: $jalpha, $jbeta y $jvbeta recuperan las matrices α y
beta, y la varianza estimada de beta, respectivamente. Para recuperar la
función de respuesta ante un impulso determinado, en forma de matriz,
consulta la función "irf".

Menú gráfico: /Modelo/Series temporales multivariantes

# vif Tests

Opción:     --quiet (No presenta nada)
Ejemplos:   longley.inp

Debe ir después de la estimación de un modelo que incluya al menos 2
variables independientes. Calcula y muestra información de diagnóstico
relacionada con la multicolinealidad.

El Factor de Inflación de la Varianza (FIV) del regresor j se define como

  1/(1 - Rj^2)

donde R_j es el coeficiente de correlación múltiple entre ese regresor j y
los demás regresores. El factor tiene un valor mínimo de 1.0 cuando la
variable en cuestión es ortogonal con respecto a las otras variables
independientes. Neter, Wasserman y Kutner (1990) sugieren revisar el valor
más grande de los FIV para diagnosticar un alto grado de multicolinealidad;
así, un valor mayor que 10 se considera a veces indicativo de un grado de
multicolinealidad problemático.

Después de utilizar esta instrucción, puedes usar el accesor "$result"
para obtener un vector columna que incluya los FIV. Para tener un enfoque
más sofisticado para diagnosticar la multicolinealidad, consulta la
instrucción "bkw".

Menú gráfico: Ventana de modelo: Análisis/Colinealidad

# wls Estimation

Argumentos: varponder depvar indepvars 
Opciones:   --vcv (Presenta la matriz de covarianzas)
            --robust (Desviaciones típicas robustas)
            --quiet (No presenta los resultados)
            --allow-zeros (Mira abajo)

Calcula las estimaciones de mínimos cuadrados ponderados (MCP, WLS)
utilizando varponder como ponderación, depvar como variable dependiente e
indepvars como lista de variables independientes. Sea w la raíz cuadrada
positiva de varponder, entonces MCP es básicamente equivalente a la
regresión MCO de w * depvar sobre w * indepvars. Sin embargo, el R-cuadrado
se calcula de modo especial, concretamente como

  R^2 = 1 - ESS / WTSS

donde ESS es la suma de errores cuadrados de la regresión ponderada, y WTSS
denota la "Suma de cuadrados totales ponderados", que es igual a la suma de
errores cuadrados de la regresión de la variable dependiente ponderada
sobre únicamente la constante ponderada.

Como caso especial, si varponder es una variable ficticia 0/1, la
estimación MCP (WLS) es equivalente a MCO (OLS) en una muestra en la que se
excluyen todas las observaciones que tienen un valor de cero para varponder.
Por otro lado, la inclusión de ponderaciones iguales a cero se considera un
error, pero si realmente quieres mezclar ponderaciones iguales a cero con
ponderaciones positivas, puedes añadir la opción --allow-zeros.

Para aplicar la estimación de Mínimos Cuadrados Ponderados a datos de
panel, basada en las varianzas del error específico de cada unidad,
consulta la instrucción "panel" junto con la opción --unit-weights.

Menú gráfico: /Modelo/Otros modelos lineales/Mínimos cuadrados ponderados

# xcorrgm Statistics

Argumentos: serie1 serie2 [ orden ] 
Opciones:   --plot=modo-o-nombrearchivo (Mira abajo)
            --silent (Omite la presentación del resultado)
Ejemplo:    xcorrgm x y 12

Presenta y/o dibuja el correlograma cruzado de serie1 con serie2, las que
puedes especificar mediante sus nombres o sus números. Los valores son los
coeficientes de correlación muestrales entre el valor vigente de serie1 y
los sucesivos adelantos y retardos de serie2.

Si especificas un valor para orden, la longitud del correlograma cruzado se
limita a ese número de adelantos y retardos (al menos); de lo contrario, la
longitud se determina de forma automática en función de la frecuencia de
los datos y del número de observaciones.

Por defecto, cuando GRETL no está en modo de procesamiento por lotes, se
muestra un gráfico del correlograma cruzado. Puedes ajustar esto mediante
la opción --plot. Los parámetros admisibles para esta opción son none
(para omitir el gráfico), display (para generar un gráfico Gnuplot aunque
sea en modo de procesamiento por lotes), o un nombre de archivo. El efecto
de proporcionar un nombre de archivo es como se describió para la opción
--output de la instrucción "gnuplot".

Menú gráfico: /Ver/Correlograma cruzado
Otro acceso:  Ventana principal: Menú emergente (tras selección múltiple)

# xtab Statistics

Argumentos: listay [ ; listax ] 
Opciones:   --row (Muestra los porcentajes de fila)
            --column (Muestra los porcentajes de columna)
            --zeros (Muestra un cero en las entradas nulas)
            --no-totals (Elimina la presentación de los recuentos marginales)
            --matrix=nombrematr (Usa las frecuencias de la matriz indicada)
            --quiet (Suprime la presentación de resultados)
            --tex[=nombrearchivo] (Salida como LaTeX)
            --equal (Consulta el caso LaTeX más abajo)
Ejemplos:   xtab 1 2
            xtab 1 ; 2 3 4
            xtab --matrix=A
            xtab 1 2 --tex="xtab.tex"
            Ver también ooballot.inp

Indicando unicamente el argumento listay, calcula (y presenta por defecto)
una tabla de contingencia o una tabulación cruzada para cada combinación
de las variables incluidas en esa lista. Cuando indicas una segunda lista
(listax), cada variable de listay se cruza en una tabla por fila frente a
cada variable de lista (por columna). Puedes referirte a las variables de
estas listas mediante sus nombres o sus números. Ten en cuenta que todas
las variables tienen que estar marcadas como discretas. Como alternativa,
cuando indicas la opción --matrix, se trata la matriz indicada como un
conjunto calculado previamente de frecuencias a presentar como tabulación
cruzada (consulta también la función "mxtab"). En este caso deberás
omitir el argumento list.

Por defecto, la anotación de cada celda indica el recuento de la frecuencia
de casos. Las opciones --row y --column (que se excluyen mutuamente)
substituyen los recuentos con los porcentajes para cada fila o columna,
respectivamente. Por defecto, las celdas con un recuento de cero casos se
dejan en blanco, pero la opción --zeros tiene como efecto la presentación
explícita de los ceros, lo que te puede ser útil para importar la tabla
con un programa tal como una hoja de cálculo.

El contraste de independencia chi-cuadrado de Pearson se muestra si la
frecuencia esperada bajo independencia es cuando menos de 1.0e-7 para todas
las celdas. Una regla general habitual de la validez de este estadístico es
que, al menos el 80 por ciento de las celdas deben tener frecuencias
esperadas iguales a 5 o más; y si este criterio no se cumple, se presenta
una advertencia.

Si una tabla de contingencia es 2 por 2, se presenta el Contraste Exacto de
independencia de Fisher. Ten en cuenta que este contraste se basa en el
supuesto de que los totales por fila y por columna son fijos, lo que puede
ser o no ser adecuado dependiendo de cómo se generaron los datos. Debes
utilizar la probabilidad asociada (valor p) de la izquierda cuando la
hipótesis alternativa a la de independencia es la asociación negativa (los
valores tienden a agruparse en las celdas de abajo a la izquierda y de
arriba a la derecha); y debes utilizar el valor p de la derecha si la
alternativa es la asociación positiva. El valor p de dos colas para este
contraste se calcula mediante el método (b) de la sección 2.1 de Agresti
(1992): esto es la suma de las probabilidades de todas las tablas posibles
que tengan los totales de filas y columnas indicados, y que tengan una
probabilidad no mayor a la de la tabla observada.

El caso bivariante

En el caso de una tabulación cruzada bivariante (cuando se indica tan solo
una lista que tiene dos elementos) se guardan algunos resultados. Puedes
recuperar la tabla de contingencia en forma de matriz mediante el accesor
"$result". Además, si se cumple la condición del valor esperado, puedes
recuperar el estadístico del contraste chi-cuadrado de Pearson y su
probabilidad asociada (valor p) mediante los accesores "$test" y "$pvalue".
Si son estos los resultados que te interesan, puedes utilizar la opción
--quiet para eliminar la presentación habitual de resultados.

Salida LaTeX

Cuando indicas la opción --tex, la tabulación cruzada se presenta con el
formato de un entorno tabular LaTeX en línea (de donde podría copiarse y
pegarse) o, cuando se añade el parámetro nombrearchivo, se envía al
archivo ahí indicado. (Si en nombrearchivo no se especifica una ruta
completa, el archivo se escribe en el directorio vigente establecido,
"workdir".) No se calcula ningún estadístico de contraste. Puedes utilizar
--equal como opción adicional para señalar (mostrado en negrilla) el
recuento o porcentaje de celdas en las que las variables de la fila y
columna tienen el mismo valor numérico. Esta opción se ignora excepto que
indiques la opción --tex; y también cuando una o las dos variables de la
tabulación cruzada tenga valores de cadena de texto.

