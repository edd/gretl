
# add Tests

Argumento: 	<@var="listavariables"> 
Opciones: 	<@lit="--lm"> (Hace un contraste de ML; solo con MCO)
		<@lit="--quiet"> (Presenta solo los resultados básicos del contraste)
		<@lit="--silent"> (No presenta nada)
		<@lit="--vcv"> (Presenta la matriz de covarianzas del modelo ampliado)
		<@lit="--both"> (Solo para estimación VI; mira abajo)
Ejemplos: 	<@lit="add 5 7 9">
		<@lit="add xx yy zz --quiet">

Debes solicitar esta instrucción después de ejecutar una instrucción de estimación. Realiza un contraste conjunto (cuyos resultados puedes obtener con los accesores <@xrf="$test"> y <@xrf="$pvalue">) sobre la adición de las variables indicadas en el argumento, al último modelo estimado. 

Por defecto, se estima una versión “ampliada” del modelo original, que resulta al añadirle a este las variables del argumento <@var="listavariables">, como regresores. En este caso, el contraste es de tipo Wald sobre el modelo ampliado, pasando a ser este el “modelo vigente” en lugar del original. Debes tener esto en cuenta, por ejemplo, para usar <@lit="$uhat"> porque este permite recuperar los errores del que sea el modelo vigente en cada momento, o para hacer contrastes posteriores. 

Alternativamente, si indicas la opción <@opt="--⁠lm"> (que solo está disponible para aquellos modelos estimados mediante MCO), se realiza un contraste de Multiplicadores de Lagrange. Para eso, se ejecuta una regresión auxiliar en la que el error de estimación del último modelo se toma como variable dependiente; y las variables independientes son las de ese último modelo más las de <@var="listavariables">. Bajo la hipótesis nula de que las variables añadidas no tienen una capacidad explicativa adicional, el estadístico formado multiplicando el tamaño de la muestra por el R-cuadrado de esta regresión, tiene la distribución de una variable chi-cuadrado con tantos grados de libertad como el número de regresores añadidos. En este caso, el modelo original no se substituye por el modelo de la regresión auxiliar. 

La opción <@opt="--⁠both"> es específica del método de estimación de Mínimos Cuadrados en 2 Etapas. Indica que las nuevas variables deben añadirse tanto a la lista de los regresores como a la lista de los instrumentos, puesto que cuando no se indica nada, se añaden por defecto solo a la de regresores. 

Menú gráfico: Ventana de modelo: Contrastes/Añadir variables

# adf Tests

Argumentos: 	<@var="orden"> <@var="listavariables"> 
Opciones: 	<@lit="--nc"> (Sin constante)
		<@lit="--c"> (Con constante)
		<@lit="--ct"> (Con constante más tendencia)
		<@lit="--ctt"> (Con constante, más tendencia cuadrática)
		<@lit="--seasonals"> (Incluye variables ficticias estacionales)
		<@lit="--gls"> (Detrae la media o la tendencia usando MCG)
		<@lit="--verbose"> (Muestra los resultados de la regresión)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--difference"> (Usa las primeras diferencias de la variable)
		<@lit="--test-down">[=<@var="criterio">] (Orden de retardos automático)
		<@lit="--perron-qu"> (Mira abajo)
Ejemplos: 	<@lit="adf 0 y">
		<@lit="adf 2 y --nc --c --ct">
		<@lit="adf 12 y --c --test-down">
		Ver también <@inp="jgm-1996.inp">

Las opciones que se muestran abajo y la discusión que sigue, se corresponden sobre todo con el uso de la instrucción <@lit="adf"> con datos de típicas series temporales. Para utilizar esta instrucción con datos de panel, mira más abajo la sección titulada “Datos de panel”. 

Esta instrucción calcula un conjunto de contrastes de Dickey–Fuller sobre cada una de las variables del argumento, siendo la hipótesis nula la existencia de una raíz unitaria. (Ahora bien, cuando escoges la opción <@opt="--⁠difference">, se calcula la primera diferencia de la(s) variable(s) antes de hacer el contraste, y la discusión de abajo debes entenderla como referida a la(s) variable(s) transformada(s).) 

Por defecto, se muestran dos variantes del contraste: una basada en una regresión que utiliza una constante, y otra que utiliza una constante más una tendencia lineal. Puedes controlar las variantes que se presentan especificando uno o más de los indicadores de opción: <@opt="--⁠nc">, <@opt="--⁠c">, <@opt="--⁠ct">, o <@opt="--⁠ctt">. 

Puedes usar la opción <@opt="--⁠gls"> con las opciones <@opt="--⁠c"> y <@opt="--⁠ct"> (con constante, y con constante más tendencia). El efecto de esta opción es que la serie que se quiere contrastar, se detrae de la media o de la tendencia usando el procedimiento de Mínimos Cuadrados Generalizados propuesto <@bib="Elliott, Rothenberg y Stock (1996);ERS96">, que proporciona un contraste de mayor potencia que la aproximación estándar de Dickey–Fuller. Esta opción no es compatible con <@opt="--⁠nc">, <@opt="--⁠ctt"> ni <@opt="--⁠seasonals">. 

En todos los casos, la variable dependiente en la regresión del contraste, es la primera diferencia de la serie especificada (<@mth="y">), y la variable independiente clave es el primer retardo de <@mth="y">. La regresión se forma de modo que el coeficiente de la variable <@mth="y"> retardada, es igual a la raíz en cuestión, α, menos 1. Por ejemplo, el modelo con constante puede escribirse como 

  <@fig="adf1">

Bajo la hipótesis nula de existencia de una raíz unitaria, el coeficiente de la variable <@mth="y"> retardada es igual a cero. Bajo la hipótesis alternativa de que <@mth="y"> es estacionaria, este coeficiente es negativo. Entonces el contraste es propiamente de una cola. 

<subhead>Selección del orden de retardos</subhead> 

La versión más simple del contraste de Dickey–Fuller asume que la perturbación aleatoria de la regresión que se utiliza en el contraste no presenta autocorrelación. En la práctica, esto no es probable que suceda por lo que la especificación de la regresión a menudo se amplía incluyendo uno o más retardos de la variable dependiente, proporcionando un contraste de Dickey–Fuller aumentado (ADF). El argumento <@var="orden"> controla el número de esos retardos (<@mth="k">), eventualmente dependiendo del tamaño de la muestra (<@mth="T">). 

<indent>
• Para usar un valor fijo de <@mth="k">, especificado por el usuario: indica un valor no negativo para <@var="orden">. 
</indent>

<indent>
• Para usar un valor de <@mth="k"> dependiente de <@mth="T">: indica <@var="orden"> igual a –1. Así el orden se establece según lo aconsejado por <@bib="Schwert (1989);schwert89">: concretamente se toma la parte entera de calcular 12(<@mth="T">/100)<@sup="0.25">. 
</indent>

Sin embargo, en general no se sabe cuantos retardos serán necesarios para poder “blanquear” el residuo de la regresión de Dickey–Fuller. Por consiguiente, es habitual especificar el <@itl="máximo"> valor de <@mth="k">, y dejar que los datos 'decidan' el número concreto de retardos que se van a incluir. Esto se puede hacer por medio de la opción <@opt="--⁠test-down">. Y también puedes establecer el criterio con el que se determine un valor óptimo para <@mth="k">, utilizando el parámetro para esta opción que deberá ser uno de entre <@lit="AIC"> (por defecto), <@lit="BIC"> o <@lit="tstat">. 

Cuando pides que se compruebe hacia atrás mediante AIC o BIC, el orden de retardo final para la ecuación ADF es el que optimiza el criterio de información que elijas (de Akaike o Bayesiano de Schwarz). El procedimiento exacto dependerá de si indicas o no la opción <@opt="--⁠gls">. Cuando se especifica GLS (MCG), los criterios AIC y BIC son las versiones “modificadas” descritas en <@bib="Ng y Perron (2001);ng-perron01">; en otro caso, son las versiones estándar. En caso de MCG, dispones de un refinamiento. Cuando indicas la opción adicional <@opt="--⁠perron-qu">, la selección del orden de retardo se realiza mediante el método revisado que recomendaron <@bib="Perron y Qu (2007);perron-qu07">. En este caso, los datos se detraen primero mediante OLS (MCO) de la media o de la tendencia; GLS (MCG) se aplica una vez que ya se haya determinado el orden de retardo. 

Cuando pides que se pruebe hacia atrás mediante el método del estadístico <@mth="t">, el procedimiento es como se indica a continuación: 

<indent>
1. Se estima la regresión de Dickey–Fuller utilizando <@mth="k"> retardos de la variable dependiente. 
</indent>

<indent>
2. ¿Es significativo el último retardo? Si lo es, se ejecuta el contraste con un orden de retardos <@mth="k">. Si no lo es, se hace que <@mth="k"> = <@mth="k"> – 1, y se vuelve al paso 1 con un retardo menos. El proceso se repite hasta que sea significativo el último retardo de una regresión, o hasta que <@mth="k"> sea 0 (se haría el contraste con un orden de retardos igual a 0). 
</indent>

En el contexto del paso 2 de arriba, “significativo” quiere decir que el estadístico <@mth="t"> del último retardo tiene un valor <@itl="p"> asintótico de dos colas igual o menor que 0.10, frente a la distribución Normal. 

En resumen, si admitimos los diferentes argumentos de Perron, Ng, Qu y Schwert indicados arriba, la instrucción preferible para comprobar una serie <@lit="y"> es probable que sea: 

<code>          
   adf -1 y --c --gls --test-down --perron-qu
</code>

(O sustituyendo <@opt="--⁠ct"> en lugar de <@opt="--⁠c"> si la serie parece tener una tendencia.) El orden de retardo para el contraste será entonces determinado probándolo hacia atrás, mediante los cambios en AIC a partir del máximo de Schwert, con el refinamiento de Perron–Qu. 

Los valores <@itl="P"> para los contrastes de Dickey–Fuller están basados en estimaciones de tipo superficie de respuesta. Cuando no se aplica MCG (GLS), se toman de <@bib="MacKinnon (1996);mackinnon96">. De lo contrario, se toman de <@bib="Cottrell (2015);cottrell15"> o, cuando se prueba hacia atrás, de <@bib="Sephton (2021);sephton21">. Los valores <@itl="P"> son específicos para el tamaño de la muestra, excepto que estén etiquetados como asintóticos. 

<subhead>Datos de Panel</subhead> 

Cuando se utiliza la instrucción <@lit="adf"> con datos de panel para hacer un contraste de raíz unitaria de panel, las opciones aplicables y los resultados que se muestran son algo diferentes. 

Primero, mientras que puedes indicar una lista de variables para probar en el caso de series temporales típicas, con datos de panel solo puedes contrastar una variable por cada instrucción. Segundo, las opciones que manejan la inclusión de términos determinísticos pasan a ser mutuamente excluyentes: debes escoger una entre sin constante, con constante, y con constante más tendencia; por defecto es con constante. Además, la opción <@opt="--⁠seasonals"> no está disponible. Tercero, la opción <@opt="--⁠verbose"> aquí tiene un significado diferente: produce un breve informe del contraste para cada serie temporal individual (siendo este por defecto una presentación solo del resultado global). 

Se calcula el contraste global (Hipótesis nula: La serie en cuestión tiene una raíz unitaria para todas las unidades del panel) de una o las dos formas siguientes: utilizando el método de <@bib="Im, Pesaran y Shin (Journal of Econometrics, 2003);IPS03"> o la de <@bib="Choi (Journal of International Money and Finance, 2001);choi01"> El contraste de Choi requiere que estén disponibles las probabilidades asociadas (valores <@itl="P">) para los contrastes individuales; si este no es el caso (dependiendo de las opciones escogidas), se omite. El estadístico concreto proporcionado para el contraste de Im, Pesaran y Shin varía del modo siguiente: si el orden de retardo para el contraste no es cero, se muestra su estadístico <@mth="W">; por otro lado, si las longitudes de las series de tiempo difieren de un individuo a otro, se muestra su estadístico <@mth="Z">; en otro caso, se muestra su estadístico <@mth="t">-barra. Consulta también la instrucción <@ref="levinlin">. 

Menú gráfico: /Variable/Contrastes de raíz unitaria/Contraste aumentado de Dickey-Fuller

# anova Statistics

Argumentos: 	<@var="respuesta"> <@var="tratamiento"> [ <@var="control"> ] 
Opción: 	<@lit="--quiet"> (No presenta los resultados)

Análisis de la Varianza: El argumento <@var="respuesta"> deberá ser una serie que mida algún efecto de interés, y <@var="tratamiento"> deberá ser una variable discreta que codifique dos o más tipos de tratamiento (o no tratamiento). Para un ANOVA de dos factores, la variable <@var="control"> (que también será discreta) deberá codificar los valores de alguna variable de control. 

Excepto cuando indicas la opción <@opt="--⁠quiet">, esta instrucción presenta una tabla mostrando las sumas de cuadrados y los cuadrados de la media junto con un contraste <@mth="F">. Puedes recuperar el estadístico del contraste <@mth="F"> y su probabilidad asociada, utilizando los accesores <@xrf="$test"> y <@xrf="$pvalue">, respectivamente. 

La hipótesis nula del contraste <@mth="F"> es que la respuesta media es invariante con respecto al tipo de tratamiento o, en otras palabras, que el tratamiento no tiene efecto. Hablando estrictamente, el contraste solo es válido cuando la varianza de la respuesta es la misma para todos los tipos de tratamiento. 

Ten en cuenta que los resultados que muestra esta instrucción son de hecho un subconjunto de la información ofrecida por el siguiente procedimiento, que puedes preparar fácilmente en el GRETL. (1) Genera un conjunto de variables ficticias que codifiquen todos los tipos de tratamiento excepto uno. Para un ANOVA de dos factores, genera además un conjunto de variables ficticias que codifiquen todos los bloques de “control” excepto uno. (2) Haz la regresión de <@var="respuesta"> sobre una constante y las variables ficticias utilizando <@ref="ols">. Con un único factor, se presenta la tabla ANOVA mediante la opción <@opt="--⁠anova"> en esa función <@lit="ols">. En caso de dos factores, el contraste <@mth="F"> relevante lo encuentras utilizando la instrucción <@ref="omit"> luego de la regresión. Por ejemplo, (asumiendo que <@var="respuesta"> es <@lit="y">, que <@lit="xt"> codifica el tratamiento, y que <@lit="xb"> codifica los bloques de “control”): 

<code>          
   # Un factor
   list Fict_xt = dummify(xt)
   ols y 0 Fict_xt --anova
   # Dos factores
   list Fict_xb = dummify(xb)
   ols y 0 Fict_xt Fict_xb
   # Contraste de significación conjunta de Fict_xt
   omit Fict_xt --quiet
</code>

Menú gráfico: /Modelo/Otros modelos lineales/ANOVA

# append Dataset

Argumento: 	<@var="nombrearchivo"> 
Opciones: 	<@lit="--time-series"> (Mira abajo)
		<@lit="--fixed-sample"> (Mira abajo)
		<@lit="--update-overlap"> (Mira abajo)
		<@lit="--quiet"> (Presenta menos detalles de confirmación; mira abajo)
		Mira abajo para opciones adicionales especiales

Abre un archivo de datos y agrega el contenido al conjunto vigente de datos, si los nuevos datos son compatibles. El programa intentará detectar el formato del archivo de datos (propio, texto plano, CSV, Gnumeric, Excel, etc.). Por favor, ten en cuenta que dispones de la instrucción <@ref="join"> que ofrece un control mucho mayor para hacer coincidir los datos adicionales con la base de datos vigente. Observa también que añadir datos a un conjunto de datos ya existente es potencialmente bastante complicado; en ese sentido, consulta más abajo la sección titulada “Datos de panel”. 

Los datos añadidos pueden tener el formato de observaciones adicionales sobre series ya presentes en el conjunto de datos, y/o el formato de nuevas series. En caso de añadir series, la compatibilidad requiere (a) que el número de observaciones de los nuevos datos sea igual al número de datos actuales, o (b) que los nuevos datos conlleven clara información de las observaciones de modo que GRETL pueda deducir como colocar los valores. 

Un caso que no se admite es aquel en el que los nuevos datos comienzan antes y acaban después que los datos originales. Para añadir series en esa situación, puedes utilizar la opción <@opt="--⁠fixed-sample">; esto tiene como efecto que se suprime la adición de observaciones, por lo que así, la operación se restringe únicamente a añadir series nuevas. 

Cuando se selecciona un archivo de datos para agregar, puede haber un área de solapamiento con el conjunto de datos existente; es decir, una o más series pueden tener una o más observaciones en común entre los dos orígenes. Cuando indicas la opción <@opt="--⁠update-overlap">, la instrucción <@lit="append"> substituye cualquier observación solapada con los valores del archivo de datos escogido; en otro caso, los valores que en ese momento ya están en su sitio no se ven afectados. 

Las opciones especiales adicionales <@opt="--⁠sheet">, <@opt="--⁠coloffset">, <@opt="--⁠rowoffset"> y <@opt="--⁠fixed-cols"> funcionan del mismo modo que con <@ref="open">; consulta esa instrucción para obtener más explicaciones. 

Por defecto, se presenta alguna información sobre el conjunto de datos agregado. La opción <@opt="--⁠quiet"> reduce esa informacióna a un simple mensaje confirmatorio que solo indica la ruta hasta el archivo. Si quieres que la operación se complete de modo silencioso, indica la instrucción <@lit="set verbose off"> antes de agregar los datos, en combinación con la opción <@opt="--⁠quiet">. 

<subhead>Datos de panel</subhead> 

Cuando los datos se añaden a un conjunto de datos de panel, el resultado tan solo va a ser correcto si coinciden de forma apropiada tanto las “unidades” o “individuos”, como los períodos de tiempo. 

La instrucción <@lit="append"> debería manejar correctamente dos situaciones relativamente simples. Sirva <@mth="n"> para denotar el número de unidades atemporales y <@mth="T"> para denotar el número de períodos de tiempo del panel vigente; y sirva <@mth="m"> para denotar el número de observaciones de los nuevos datos. Si <@mth="m = n">, los nuevos datos se consideran invariantes en el tiempo, y se copian repetidos para cada período de tiempo. Por otro lado, si <@mth="m = T"> los datos se tratan como invariantes entre las unidades atemporales, y se copian repetidos para cada unidad atemporal. Si el panel es “cuadrado”, y <@mth="T = n"> aparece una ambigüedad. Por defecto, en este caso se tratan los nuevos datos como invariantes en el tiempo, pero puedes forzar a que GRETL los trate como series temporales (invariantes entre las unidades) con la opción <@opt="--⁠time-series">. 

Cuando se reconoce tanto al conjunto de datos vigente como a los datos que se van a añadir como datos de panel, aparecen dos situaciones. (1) La longitud de las series de tiempo, <@mth="T">, es diferente entre los dos. Entonces se presenta un fallo. (2) <@mth="T"> coincide. En ese caso se hace un supuesto muy simple, en particular que las unidades coinciden, <@itl="empezando por la primera unidad"> en ambos conjuntos de datos. Si ese supuesto no es correcto, deberás utilizar la instrucción <@ref="join"> en lugar de <@lit="append">. 

Menú gráfico: /Archivo/Añadir datos

# ar Estimation

Argumentos: 	<@var="retardos"> ; <@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--quiet"> (No presenta las estimaciones de los parámetros)
Ejemplo: 	<@lit="ar 1 3 4 ; y 0 x1 x2 x3">

Calcula las estimaciones de los parámetros utilizando el procedimiento iterativo generalizado de Cochrane–Orcutt; consulta la Sección 9.5 de <@bib="Ramanathan (2002);ramanathan02">. Las iteraciones acaban cuando la sucesión de sumas de errores cuadrados no difiere de un término al siguiente en más del 0.005 por ciento, o después de 20 iteraciones. 

Con <@var="retardos"> tienes que indicar una lista de retardos del término de perturbación, acabada en un punto y coma. En el ejemplo de arriba, el término de perturbación se especifica como 

  <@fig="arlags">

Menú gráfico: /Modelo/Series temporales univariantes/Errores AR (MCG)

# ar1 Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--hilu"> (Utiliza el procedimiento de Hildreth–Lu)
		<@lit="--pwe"> (Utiliza el estimador de Prais–Winsten)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--no-corc"> (No afina los resultados con Cochrane-Orcutt)
		<@lit="--loose"> (Utiliza un criterio de convergencia menos preciso)
		<@lit="--quiet"> (No presenta nada)
Ejemplos: 	<@lit="ar1 1 0 2 4 6 7">
		<@lit="ar1 y 0 xlista --pwe">
		<@lit="ar1 y 0 xlista --hilu --no-corc">

Calcula estimaciones MCG que sean viables para un modelo en el que el término de perturbación se asume que sigue un proceso autorregresivo de primer orden. 

El método utilizado por defecto es el procedimiento iterativo de Cochrane–Orcutt; por ejemplo, consulta la sección 9.4 de <@bib="Ramanathan (2002);ramanathan02">. El criterio para lograr la convergencia es que las estimaciones sucesivas del coeficiente de autocorrelación, no difieran en más de 1e-6 o, cuando indicas la opción <@opt="--⁠loose">, en más de 0.001. Si esto no se alcanza antes de que se hagan las 100 iteraciones, se muestra un fallo. 

Cuando indicas la opción <@opt="--⁠pwe">, se utiliza el estimador de Prais–Winsten. Esto implica una iteración similar a la de Cochrane–Orcutt; la diferencia está en que mientras que el método de Cochrane–Orcutt descarta la primera observación, el método de Prais–Winsten hace uso de ella. Para obtener más detalles consulta, por ejemplo, el capítulo 13 de <@bib="Greene (2000);greene00">. 

Cuando indicas la opción <@opt="--⁠hilu">, se utiliza el procedimiento de búsqueda de Hildreth–Lu. En ese caso, se afinan los resultados utilizando el método de Cochrane–Orcutt, excepto que especifiques la opción <@opt="--⁠no-corc">. Esta opción <@opt="--⁠no-corc"> se ignora para estimadores diferentes al del método de Hildreth–Lu. 

Menú gráfico: /Modelo/Series temporales univariantes/Errores AR (MCG)

# arch Estimation

Argumentos: 	<@var="orden"> <@var="depvar"> <@var="indepvars"> 
Opción: 	<@lit="--quiet"> (No presenta nada)
Ejemplo: 	<@lit="arch 4 y 0 x1 x2 x3">

En este momento, esta instrucción se mantiene por compatibilidad con versiones anteriores, pero sales ganando si utilizas el estimador máximo verosímil que ofrece la instrucción <@ref="garch">. Si quieres estimar un modelo ARCH sencillo, puedes usar el GARCH haciendo que su primer parámetro sea 0. 

Estima la especificación indicada del modelo permitiendo ARCH (Heterocedasticidad Condicional Autorregresiva). Primero, se estima el modelo mediante MCO, y luego se ejecuta una regresión auxiliar, en la que se regresa el error cuadrado de la primera sobre sus propios valores retardados. El paso final es la estimación por mínimos cuadrados ponderados, utilizando como ponderaciones las inversas de las varianzas de los errores ajustados con la regresión auxiliar. (Si la varianza que se predice para alguna observación de la regresión auxiliar, no es positiva, entonces se utiliza en su lugar el error cuadrado correspondiente). 

Los valores <@lit="alpha"> presentados debajo de los coeficientes son los parámetros estimados del proceso ARCH con la regresión auxiliar. 

Consulta también <@ref="garch"> y <@ref="modtest"> (opción <@opt="--⁠arch">). 

# arima Estimation

Argumentos: 	<@var="p"> <@var="d"> <@var="q"> [ ; <@var="P"> <@var="D"> <@var="Q"> ] ; <@var="depvar"> [ <@var="indepvars"> ] 
Opciones: 	<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--hessian"> (Mira abajo)
		<@lit="--opg"> (Mira abajo)
		<@lit="--nc"> (Sin constante)
		<@lit="--conditional"> (Utiliza Máxima Verosimilitud condicional)
		<@lit="--x-12-arima"> (Utiliza ARIMA X-12 o X-13 en la estimación)
		<@lit="--lbfgs"> (Utiliza el maximizador L-BFGS-B)
		<@lit="--y-diff-only"> (ARIMAX especial; mira abajo)
		<@lit="--lagselect"> (Mira abajo)
Ejemplos: 	<@lit="arima 1 0 2 ; y">
		<@lit="arima 2 0 2 ; y 0 x1 x2 --verbose">
		<@lit="arima 0 1 1 ; 0 1 1 ; y --nc">
		Ver también <@inp="armaloop.inp">, <@inp="auto_arima.inp">, <@inp="bjg.inp">

Advertencia: <@lit="arma"> es un alias aceptable para esta instrucción. 

Cuando no indicas la lista <@var="indepvars">, se estima un modelo univariante ARIMA (Autorregresivo, Integrado, de Medias móviles). Los valores <@var="p">, <@var="d"> y <@var="q"> representan el orden autorregresivo (AR), el orden de diferenciación y el orden de medias móviles (MA), respectivamente. Puedes indicar estos valores en formato numérico, o como nombres de variables escalares ya existentes. Por ejemplo, un valor de 1 para <@var="d"> significa que, antes de estimar los parámetros del ARMA, debe tomarse la primera diferencia de la variable dependiente. 

Si quieres incluir en el modelo solo retardos AR o MA específicos (en contraposición a todos los retardos hasta un orden indicado) puedes substituir por <@var="p"> y/o <@var="q"> bien (a) el nombre de una matriz definida previamente que contiene un conjunto de valores enteros, o bien (b) una expresión tal como <@lit="{1,4}">; es decir, un conjunto de retardos separados con comas y puestos entre llaves. 

Los valores enteros <@var="P">, <@var="D"> y <@var="Q"> (opcionales) representan el orden AR estacional, el orden de diferenciación estacional y el orden MA estacional, respectivamente. Estos órdenes solo los puedes aplicar cuando los datos tienen una frecuencia mayor que 1 (por ejemplo, con datos trimestrales o mensuales); y puedes indicarlas en formato numérico o como variables escalares. 

En el caso univariante, por defecto se incluye en el modelo una ordenada en el origen, pero puedes eliminar esto por medio de la opción <@opt="--⁠nc">. Cuando añades <@var="indepvars">, el modelo se convierte en un ARMAX; en este caso, debes incluir la constante explícitamente si quieres tener la ordenada en el origen (como en el segundo ejemplo de arriba). 

Dispones de una forma alternativa de sintaxis para esta instrucción: si no quieres aplicar diferencias (ni estacionales ni no estacionales), puedes omitir los dos campos <@var="d"> y <@var="D"> a la vez, mejor que introducir explícitamente 0. Además, <@lit="arma"> es un alias o sinónimo de <@lit="arima"> y así, por ejemplo, la siguiente instrucción es un modo válido de especificar un modelo ARMA(2, 1): 

<code>          
   arma 2 1 ; y
</code>

Por defecto, se utiliza la funcionalidad ARMA “propia” de GRETL, con la estimación Máximo Verosímil (MV) exacta; pero dispones de la opción de hacer la estimación mediante MV condicional. (Si el programa ARIMA X-12 está instalado en el ordenador, tienes la posibilidad de utilizarlo en vez del código propio. Ten en cuenta que, de igual modo, el más reciente X13 puede funcionar como un recambio automático.) Para otros detalles relacionados con estas opciones, consulta <@pdf="El manual de gretl#chap:timeseries"> (Capítulo 31). 

Cuando se utiliza código propio de MV exacta, las desviaciones típicas estimadas se basan por defecto en una aproximación numérica a la (inversa negativa de la) matriz Hessiana, con un último recurso al Producto Externo del vector Gradiente (PEG) si el cálculo de la matriz Hessiana numérica pudiera fallar. Puedes utilizar dos indicadores de opción (mutuamente excluyentes) para forzar esta cuestión: mientras que la opción <@opt="--⁠opg"> fuerza la utilización del método PEG, sin intentar calcular la matriz Hessiana, la opción <@opt="--⁠hessian"> inhabilita el último recurso a PEG. Ten en cuenta que un fallo en el cálculo de la matriz Hessiana numérica, generalmente es un indicador de que un modelo está mal especificado. 

La opción <@opt="--⁠lbfgs"> es específica de la estimación que utiliza código ARMA propio y Máxima Verosimilitud exacta; y solicita que se utilice el algoritmo de “memoria limitada” L-BFGS-B en lugar del maximizador BFGS habitual. Esto puede ser de ayuda en algunos casos en los que la convergencia es difícil de lograr. 

La opción <@opt="--⁠y-diff-only"> es específica de la estimación de modelos ARIMAX (modelos con orden de integración no nulo, en los que se incluyen regresores exógenos) y se aplica solo cuando se utiliza la Máxima Verosimilitud exacta propia de GRETL. Para esos modelos, el comportamiento por defecto consiste en calcular las primeras diferencias tanto de la variable dependiente como de los regresores; pero cuando indicas esta opción, solo se calcula para la variable dependiente, quedando los regresores en niveles. 

El valor del AIC de Akaike indicado en conexión con modelos ARIMA, se calcula de acuerdo con la definición que utiliza el ARIMA X-12, concretamente 

  <@fig="aic">

donde <@fig="ell"> es el logaritmo de la verosimilitud y <@mth="k"> es el número total de parámetros estimados. Observa que el ARIMA X-12 no produce criterios de información tales como AIC cuando la estimación es por Máxima Verosimilitud condicional. 

Las raíces AR y MA mostradas en conexión con la estimación ARMA se basan en la siguiente representación de un proceso ARMA(p, q): 

<mono>          
      	(1 - a_1*L - a_2*L^2 - ... - a_p*L^p)Y =
        c + (1 + b_1*L + b_2*L^2 + ... + b_q*L^q) e_t
</mono>

Por lo tanto, las raíces AR son las soluciones a 

<mono>          
       1 - a_1*z - a_2*z^2 - ... - a_p*L^p = 0
</mono>

y la estabilidad requiere que estas raíces se encuentren fuera del círculo de radio unitario. 

La cantidad “Frecuencia” presentada en conexión con las raíces AR y MA, es el valor λ que soluciona <@mth="z"> = <@mth="r"> * exp(i*2*π*λ) donde <@mth="z"> es la raíz en cuestión, y <@mth="r"> es su módulo. 

<subhead>Lag selection</subhead> 

Cuando se indica la opción <@opt="--⁠lagselect">, esta instrucción no proporciona estimaciones concretas, sino que en su lugar produce una tabla que presenta criterios de información y el logaritmo de la verosimilitud para ciertas especificaciones ARMA o ARIMA. Los órdenes de retardo <@mth="p"> y <@mth="q"> se consideran como máximos; y cuando se indica una especificación estacional, <@mth="P"> y <@mth="Q"> también se consideran como máximos. En todo caso, el orden mínimo se considera que es 0, y los resultados se muestran para todas las especificaciones desde la mínima hasta la máxima. Los grados de diferenciación en la instrucción, <@mth="d"> y/o <@mth="D">, se respetan pero no se tratan como objeto de investigación. Puedes obtener una matriz que contenga los resultados mediante el accesor <@xrf="$test">. 

Menú gráfico: /Modelo/Series temporales univariantes/ARIMA

# arma Estimation

Consulta <@ref="arima">; <@lit="arma"> es un alias. 

# bds Tests

Argumentos: 	<@var="orden"> <@var="x"> 
Opciones: 	<@lit="--corr1">=<@var="rho"> (Mira abajo)
		<@lit="--sdcrit">=<@var="multiple"> (Mira abajo)
		<@lit="--boot">=<@var="N"> (Mira abajo)
		<@lit="--matrix">=<@var="m"> (Utiliza una entrada matricial)
		<@lit="--quiet"> (Suprime la presentación de resultados)
Ejemplos: 	<@lit="bds 5 x">
		<@lit="bds 3 --matrix=m">
		<@lit="bds 4 --sdcrit=2.0">

Lleva a cabo el contraste BDS (<@bib="Brock, Dechert, Scheinkman y LeBaron, 1996;brock-etal96">) de no linealidad para la serie <@var="x">. En el contexto econométrico, esto se aplica habitualmente para comprobar si los residuos de una regresión incumplen la condición IID (distribución idéntica e independiente). El contraste se basa en un conjunto de integrales de correlación, preparadas para detectar la no linealidad de dimensión progresivamente mayor; y se establece el número de esas integrales con el argumento <@var="orden">. Estas deben ser por lo menos 2; con la primera integral se establece una referencia de partida, pero sin que permita un contraste. El contraste BDS es de tipo “portmanteau”: adecuado para detectar toda clase de desviaciones respecto a la linealidad, pero no esclarecedor del modo exacto en el que se incumple la condición. 

En lugar de indicar <@var="x"> como serie, puedes utilizar la opción <@opt="--⁠matrix"> para especificar una matriz como entrada, la cual debe tener forma de vector (columna o fila). 

<subhead>Criterio de proximidad</subhead> 

Las integrales de correlación están basadas en una medida de “proximidad” entre los puntos de datos, de forma que se consideran próximos a dos de esos puntos si están situados uno del otro a menos de ε. Dado que el contraste necesita que se especifique ε, por defecto, GRETL sigue la recomendación de <@bib="Kanzler (1999);kanzler99">: ε se escoge de modo que la integral de correlación de primer orden esté en torno a 0.7. Una alternativa habitual (que necesita menos cálculos) consiste en especificar ε como un múltiplo de la desviación típica de la serie de interés. La opción <@opt="--⁠sdcrit"> permite este último método; así, en el tercer ejemplo indicado más arriba, ε se determina que sea igual a dos veces la desviación típica de <@var="x">. La opción <@opt="--⁠corr1"> implica la utilización del método de Kanzler pero permite otra correlación objetivo diferente de 0.7. Deberías tener claro que estas dos opciones se excluyen mutuamente. 

<subhead>Muestreo repetido</subhead> 

Los estadísticos del contraste BDS tienen distribución asintótica de tipo <@mth="N">(0,1), pero el contraste rechaza demasiado la hipótesis nula de modo muy notable con muestras de tamaño entre pequeño y moderado. Por ese motivo, los valores <@mth="P"> se obtienen por defecto mediante muestreo repetido (bootstrapping) cuando <@var="x"> tiene una longitud menor que 600 (y con referencia a la distribución Normal, en caso contrario). Si quieres utilizar el muestreo repetido con muestras más largas, puedes forzar esta cuestión indicando un valor no nulo para la opción <@opt="--⁠boot">. Por el contrario, si no quieres que se haga el muestreo repetido con las muestras más pequeñas, indica un valor de cero para <@opt="--⁠boot">. 

Cuando se hace el muestreo repetido, el número de iteraciones por defecto es de 1999; pero puedes especificar un número diferente indicando un valor mayor que 1 con la opción <@opt="--⁠boot">. 

<subhead>Matriz accesoria matrix</subhead> 

Cuando se completa con éxito la ejecución de esta instrucción, <@xrf="$result"> proporciona los resultados del contraste en forma de una matriz con dos filas y <@var="orden"> – 1 columnas. La primera fila contiene los estadísticos de contraste y la segunda los valores <@mth="P">, de cada uno de los contrastes por dimensión, bajo la hipótesis nula de que <@var="x"> es lineal/IID. 

# biprobit Estimation

Argumentos: 	<@var="depvar1"> <@var="depvar2"> <@var="indepvars1"> [ ; <@var="indepvars2"> ] 
Opciones: 	<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--cluster">=<@var="clustvar"> (Consulta <@ref="logit"> para aclaración)
		<@lit="--opg"> (Mira abajo)
		<@lit="--save-xbeta"> (Mira abajo)
		<@lit="--verbose"> (Presenta información adicional)
Ejemplos: 	<@lit="biprobit y1 y2 0 x1 x2">
		<@lit="biprobit y1 y2 0 x11 x12 ; 0 x21 x22">
		Ver también <@inp="biprobit.inp">

Estima un modelo probit bivariante utilizando el método de Newton–Raphson para maximizar la verosimilitud. 

La lista de argumentos comienza con las dos variables (binarias) dependientes, seguidas de una lista de regresores. Cuando indicas una segunda lista (separada por un punto y coma) se entiende como un grupo de regresores específicos de la segunda ecuación, siendo <@var="indepvars1"> específica de la primera ecuación; en otro caso, <@var="indepvars1"> se considera que representa un conjunto de regresores común. 

Por defecto, las desviaciones típicas se calculan utilizando la matriz Hessiana analítica al converger. Pero si indicas la opción <@opt="--⁠opg">, la matriz de covarianzas se basa en el Producto Externo del vector Gradiente (PEG o OPG); o si indicas la opción <@opt="--⁠robust">, se calculan las desviaciones típicas cuasi máximo verosímiles (QML), utilizando un “emparedado” entre la inversa de la matriz Hessiana y el PEG. 

Observa que la estimación de rho, la correlación de los términos de error entre las dos ecuaciones, se incluye en el vector de coeficientes; es el último elemento de los accesores <@lit="coeff">, <@lit="stderr"> y <@lit="vcv">. 

Luego de una estimación correcta, el accesor <@lit="$uhat"> permite recuperar una matriz con 2 columnas que contiene los errores generalizados de las dos ecuaciones; es decir, los valores esperados de las perturbaciones condicionadas a los resultados observados y a las variables covariantes. Por defecto, <@lit="$yhat"> permite recuperar una matriz con 4 columnas que contiene las probabilidades estimadas de los 4 posibles resultados conjuntos para (<@mth="y"><@sub="1">, <@mth="y"><@sub="2">), en el orden (1,1), (1,0), (0,1), (0,0). Alternativamente, cuando indicas la opción <@opt="--⁠save-xbeta">, entonces <@lit="$yhat"> tiene 2 columnas y contiene los valores de las funciones índice de las ecuaciones respectivas. 

El resultado incluye un contraste de la hipótesis nula de que las perturbaciones de las dos ecuaciones no están correlacionadas. Este es un test de razón de verosimilitudes excepto que se solicite el estimador cuasi máximo verosímil (QML) de la varianza, en cuyo caso se usa el test de Wald. 

# bkw Tests

Opción: 	<@lit="--quiet"> (No presenta nada)
Ejemplos: 	<@inp="longley.inp">

Debe ir después de la estimación de un modelo que contenga al menos dos variables explicativas. Calcula y presenta información de diagnóstico en relación a la multicolinealidad, en concreto la Tabla BKW que está basada en el trabajo de <@bib="Belsley, Kuh y Welsch (1980);belsley-etal80">. Esta tabla presenta un sofisticado análisis del grado y de las causas de la multicolinealidad, mediante el examen de los autovalores de la inversa de la matriz de correlaciones. Para tener una explicación en detalle del enfoque BKW en relación a GRETL, y con diversos ejemplos, consulta <@bib="Adkins, Waters y Hill (2015);adkins15">. 

Después de utilizar esta instrucción, puedes usar el accesor <@xrf="$result"> para recuperar la tabla BKW en forma de matriz. Consulta también la instrucción <@ref="vif"> para obtener un enfoque más sencillo del diagnóstico de la multicolinealidad. 

Hay también una función denominada <@xrf="bkw"> que ofrece una mayor flexibilidad. 

Menú gráfico: Ventana de modelo: Análisis/Colinealidad

# boxplot Graphs

Argumento: 	<@var="listavariables"> 
Opciones: 	<@lit="--notches"> (Muestra el intervalo del 90 por ciento para la mediana)
		<@lit="--factorized"> (Mira abajo)
		<@lit="--panel"> (Mira abajo)
		<@lit="--matrix">=<@var="nombre"> (Representa las columnas de la matriz indicada)
		<@lit="--output">=<@var="nombrearchivo"> (Envía el resultado al archivo especificado)

Estos gráficos muestran la distribución de una variable. Una caja central encierra el 50 por ciento central de los datos; i.e. está deslindada por el primer y el tercer cuartiles. Un “bigote” se extiende desde cada límite de la caja con un rango igual a 1.5 veces el rango intercuartil. Las observaciones que están fuera de ese rango se consideran valores atípicos y se representan mediante puntos. Se dibuja una línea a lo ancho de la caja en la mediana. El signo “+” se utiliza para indicar la media. Si escoges la opción de mostrar un intervalo de confianza para la mediana, este se calcula mediante el método bootstrap y se muestra con formato de líneas con rayas horizontales por arriba y/o abajo de la mediana. 

La opción <@opt="--⁠factorized"> te permite examinar la distribución de la variable elegida condicionada al valor de algún factor discreto. Por ejemplo, si un conjunto de datos contiene una variable con los salarios y una variable ficticia con el género, puedes escoger la de los salarios como objetivo y la del género como el factor, para ver así los gráficos de cajas de salarios de hombres y mujeres, uno al lado del otro, como en 

<code>          
   boxplot salario genero --factorized
</code>

Ten en cuenta que en este caso debes especificar exactamente solo dos variables, con el factor indicado en segundo lugar. 

Cuando tienes un conjunto vigente de datos de panel y especificas solo una variable, la opción <@opt="--⁠panel"> produce una serie de gráficos de cajas (uno al lado del otro) en la que cada uno se corresponde con un grupo o “unidad” del panel. 

Generalmente se requiere el argumento <@var="listavariables"> que se refiere a una o más series del conjunto vigente de datos (indicadas bien por el nombre o bien por el número ID). Pero si, mediante la opción <@opt="--⁠matrix">, indicas una matriz ya definida, este argumento se convierte en opcional pues, por defecto, se dibuja un gráfico para cada columna de la matriz especificada. 

Los gráficos de cajas en GRETL se generan utilizando la instrucción gnuplot, y resulta posible especificar con mayor detalle el gráfico añadiendo instrucciones adicionales de Gnuplot, puestas entre llaves. Para obtener más detalles, consulta la ayuda para la instrucción <@ref="gnuplot">. 

En modo interactivo, el resultado se muestra inmediatamente. En modo de procesamiento por lotes, el proceder por defecto consiste en escribir un archivo de instrucciones de Gnuplot en el directorio de trabajo del usuario, con un nombre con el patrón <@lit="gpttmpN.plt">, comenzando con N = <@lit="01">. Puedes generar los gráficos más tarde utilizando el gnuplot (o bien wgnuplot bajo MS Windows). Puedes modificar este comportamiento mediante el uso de la opción <@opt="--⁠output="><@var="nombrearchivo">. Si quieres obtener más detalles, consulta la instrucción <@ref="gnuplot">. 

Menú gráfico: /Ver/Gráficos/Gráficos de caja

# break Programming

Salida de un bucle. Puedes utilizar esta instrucción solo dentro de un bucle; eso provoca que la ejecución de instrucciones salga del bucle actual (del más interior, si hay varios anidados). Consulta también <@ref="loop">, <@ref="continue">. 

# catch Programming

Sintaxis: 	<@lit="catch "><@var="command">

Esta no es una instrucción por si misma, pero puedes utilizarla como prefijo en la mayoría de las instrucciones habituales: su efecto es el de prevenir que acabe un guion de instrucciones si ocurre un fallo al ejecutar una de ellas. Si aparece un fallo, esto se registra con un código de fallo interno al que puedes acceder con <@xrf="$error"> (un valor de 0 indica éxito). Inmediatamente después de utilizar <@lit="catch"> deberías verificar siempre cual es el valor de <@lit="$error">, y realizar una acción adecuada si falló una de las instrucciones. 

No puedes utilizar la palabra clave <@lit="catch"> antes de <@lit="if">, <@lit="elif"> o <@lit="endif">. Además, no debe utilizarse en peticiones a funciones definidas por el usuario, pues se pretende utilizarla solo con las instrucciones de GRETL y con las peticiones a los operadores o funciones “internos”. Más aún, no puedes usar <@lit="catch"> combinada con la asignación mediante “flecha atrás” de modelos o gráficos, a iconos de sesión (consulta <@pdf="El manual de gretl#chap:modes"> (Capítulo 3)). 

# chow Tests

Variantes: 	<@lit="chow"> <@var="obs">
		<@lit="chow"> <@var="dummyvar"> <@lit="--dummy">
Opciones: 	<@lit="--dummy"> (Utiliza una variable ficticia ya existente)
		<@lit="--quiet"> (No presenta las estimaciones del modelo ampliado)
		<@lit="--limit-to">=<@var="lista"> (Limita el contraste a un subconjunto de regresores)
Ejemplos: 	<@lit="chow 25">
		<@lit="chow 1988:1">
		<@lit="chow mujer --dummy">

Debe ir a continuación de una regresión MCO (OLS). Si indicas un número de observación o una fecha, proporciona un contraste respecto a la hipótesis nula de que no existe cambio estructural en el punto de corte indicado. El procedimiento consiste en crear una variable ficticia que toma el valor 1 desde el punto de corte especificado por <@var="obs"> hasta el final de la muestra, y 0 en otro caso, así como generar términos de interacción entre esa ficticia y los regresores originales. Si indicas una ficticia, se contrasta esa hipótesis nula de homogeneidad estructural respecto a esa variable ficticia, y también se añaden términos de interacción. En cada caso se ejecuta una regresión ampliada incluyendo los términos adicionales. 

Por defecto, se calcula un estadístico <@mth="F">, considerando la regresión ampliada como el modelo sin restricciones y el modelo original como el restringido. Pero si el modelo original utilizó un estimador robusto para la matriz de covarianzas, el estadístico de contraste es uno de Wald con distribución chi-cuadrado; con su valor basado en un estimador robusto de la matriz de covarianzas de la regresión ampliada. 

Puedes utilizar la opción <@opt="--⁠limit-to"> para limitar el conjunto de términos de interacción con la variable ficticia de corte, a un subconjunto de los regresores originales. El argumento para esta opción debe ser una lista ya definida en la que todos sus elementos estén entre los regresores originales, y no debe incluir la constante. 

Menú gráfico: Ventana de modelo: Contrastes/Contraste de Chow

# clear Programming

Opciones: 	<@lit="--dataset"> (Elimina solo el conjunto de datos)
		<@lit="--functions"> (Elimina las funciones (unicamente))
		<@lit="--all"> (Elimina todo)

Por defecto, esta instrucción quita de la memoria el conjunto de datos vigente (si hay alguno), además de todas las variables guardadas (escalares, matrices, etc.). Ten en cuenta que también tienes este efecto al abrir un nuevo conjunto de datos, o al utilizar la instrucción <@lit="nulldata"> para crear un conjunto de datos vacío; por eso normalmente no necesitas hace uso explícito de <@lit="clear">. 

Cuando indicas la opción <@opt="--⁠dataset">, entonces solo se elimina el conjunto de datos (más cualquier lista de series definida); otros objetos guardados como matrices, escalares o 'bundles', se van a conservar. 

Cuando indicas la opción <@opt="--⁠functions">, entonces se elimina de la memoria cualquier función definida por el usuario y cualquier función definida en los paquetes que tengas cargados. El conjunto de datos y otras variables no se ven afectados. 

Cuando indicas la opción <@opt="--⁠all">, entonces la eliminación es completa: el conjunto de datos, las variables guardadas de todo tipo, además de las funciones definidas por el usuario y en paquetes. 

# coeffsum Tests

Argumento: 	<@var="listavariables"> 
Opción: 	<@lit="--quiet"> (No presenta nada)
Ejemplos: 	<@lit="coeffsum xt xt_1 xr_2">
		Ver también <@inp="restrict.inp">

Debe ir después de una regresión. Calcula la suma de los coeficientes de las variables del argumento <@var="listavariables">. Presenta esta suma junto con su desviación típica y la probabilidad asociada al estadístico para contrastar la hipótesis nula de que la suma es cero. 

Ten en cuenta la diferencia entre esto y la instrucción <@ref="omit">, pues esta última te permite contrastar la hipótesis nula de que los coeficientes de un subconjunto especificado de variables independientes son <@itl="todos"> nulos. 

La opción <@opt="--⁠quiet"> te puede ser de utilidad si únicamente deseas acceder a los valores de <@xrf="$test"> y de <@xrf="$pvalue"> que se registran después de terminar la estimación con éxito. 

Menú gráfico: Ventana de modelo: Contrastes/Suma de los coeficientes

# coint Tests

Argumentos: 	<@var="orden"> <@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--nc"> (Sin constante)
		<@lit="--ct"> (Con constante y tendencia)
		<@lit="--ctt"> (Con constante más tendencia cuadrática)
		<@lit="--seasonals"> (Con variables ficticias estacionales)
		<@lit="--skip-df"> (Sin contrastes DF sobre las variables individuales)
		<@lit="--test-down">[=<@var="criterio">] (Orden de retardos automático)
		<@lit="--verbose"> (Presenta detalles adicionales de las regresiones)
		<@lit="--silent"> (No presenta nada)
Ejemplos: 	<@lit="coint 4 y x1 x2">
		<@lit="coint 0 y x1 x2 --ct --skip-df">

Contraste de cointegración de <@bib="Engle–Granger (1987);engle-granger87">. El proceso por defecto consiste en: (1) realizar los contrastes de Dickey–Fuller respecto a la hipótesis nula de que cada una de las variables enumeradas tiene una raíz unitaria; (2) estimar la regresión de cointegración; y (3) hacer un contraste DF respecto a los errores que comete la regresión de cointegración. Cuando se indica la opción <@opt="--⁠skip-df">, se omite el paso (1). 

Si el orden especificado de retardos es positivo, todos los contrastes de Dickey–Fuller usan ese orden pero con este requisito: cuando se indica la opción <@opt="--⁠test-down">, el valor indicado se toma como un máximo, y el orden concreto de retardos que se utilizará en cada caso se obtiene probando hacia abajo. Consulta la instrucción <@ref="adf"> para obtener más detalles sobre este procedimiento. 

Por defecto, la regresión de cointegración contiene una constante pero, si quieres eliminar la constante, añade la opción <@opt="--⁠nc">. Si quieres ampliar la lista de términos determinísticos en la regresión de cointegración con tendencia lineal (o cuadrática), añade la opción <@opt="--⁠ct"> (o <@opt="--⁠ctt">). Estos indicadores de opción son mutuamente excluyentes. También tienes la posibilidad de añadir variables ficticias estacionales (en caso de utilizar datos trimestrales o mensuales). 

Los valores <@itl="P"> (probabilidades asociadas) de este contraste se basan en <@bib="MacKinnon (1996);mackinnon96">. El código relevante se incluye con el amable permiso del propio autor. 

Para obtener los contrastes de cointegración de Søren Johansen, consulta <@ref="johansen">. 

Menú gráfico: /Modelo/Series temporales multivariantes

# continue Programming

Puedes usar esta instrucción solo dentro de un bucle; su efecto consiste en saltarse los enunciados posteriores que haya dentro de la iteración vigente del bucle (más interno) vigente. Consulta también <@ref="loop">, <@ref="break">. 

# corr Statistics

Variantes: 	<@lit="corr ["> <@var="listavariables"> ]
		<@lit="corr --matrix="><@var="nombrematriz">
Opciones: 	<@lit="--uniform"> (Garantiza una muestra uniforme)
		<@lit="--spearman"> (Rho de Spearman)
		<@lit="--kendall"> (Tau de Kendall)
		<@lit="--verbose"> (Presenta jerarquías)
		<@lit="--plot">=<@var="modo-o-nombrearchivo"> (Mira abajo)
		<@lit="--triangle"> (Representa solo la mitad inferior, mira abajo)
		<@lit="--quiet"> (No presenta nada)
Ejemplos: 	<@lit="corr y x1 x2 x3">
		<@lit="corr ylista --uniform">
		<@lit="corr x y --spearman">
		<@lit="corr --matrix=X --plot=display">

Por defecto, presenta los coeficientes de correlación (correlación producto-momento de Pearson) por pares de las variables de <@var="listavariables">, o de todas las variables del conjunto de datos si no indicas <@var="listavariables">. El comportamiento típico de esta instrucción consiste en utilizar todas las observaciones disponibles para calcular cada coeficiente por parejas de variables, pero cuando indicas la opción <@lit="--uniform">, la muestra se limita (si es necesario) de modo que se utiliza el mismo conjunto de observaciones para todos los coeficientes. Esta opción es adecuada solo cuando hay un número diferente de valores ausentes en las variables utilizadas. 

Las opciones <@opt="--⁠spearman"> y <@opt="--⁠kendall"> (que son mutuamente excluyentes) permiten calcular, respectivamente, el coeficiente rho de correlación por rangos de Spearman y el coeficiente tau de correlación por rangos de Kendall en lugar del coeficiente de Pearson (por defecto). Cuando indicas alguna de estas opciones, <@var="listavariables"> debe contener solo dos variables. 

Cuando se calcula una correlación por rangos, puedes utilizar la opción <@opt="--⁠verbose"> para presentar los datos originales y su jerarquía (en otro caso, esta alternativa se ignora). 

Si <@var="listavariables"> contiene más de dos series y GRETL no está en modo de procesamiento por lotes, se muestra un gráfico de “mapa de calor” de la matriz de correlaciones. Puedes ajustar esto mediante la opción <@opt="--⁠plot">, en la que los parámetros que se admiten son: <@lit="none"> (para no mostrar el gráfico), <@lit="display"> (para presentar el gráfico incluso cuando se esté en modo de procesamiento por lotes), o un nombre de archivo. El efecto de indicar un nombre de archivo es como el descrito para la opción <@opt="--⁠output"> de la instrucción <@ref="gnuplot">. Cuando activas la representación del gráfico, puedes utilizar la opción <@opt="--⁠triangle"> para mostrar solo el mapa de calor del triángulo inferior de la matriz. 

Cuando indicas una forma alternativa, utilizando una matriz ya definida en lugar de una lista de series, las opciones <@opt="--⁠spearman"> y <@opt="--⁠kendall"> no están disponibles (pero consulta la función <@xrf="npcorr">). 

Puedes usar el accesor <@xrf="$result"> para obtener las correlaciones en forma de matriz. Ten en cuenta que cuando esta es la <@itl="matriz"> que te interese (y no solo los coeficientes de las parejas), entonces en caso de que haya valores ausentes se aconseja utilizar la opción <@opt="--⁠uniform">. A no ser que utilices una muestra común única, no se garantiza que la matriz de correlaciones sea semidefinida positiva, como debiera ser por construcción. 

Menú gráfico: /Ver/Matriz de correlación
Otro acceso: Ventana principal: Menú emergente (tras selección múltiple)

# corrgm Statistics

Argumentos: 	<@var="y"> [ <@var="orden"> ] 
Opciones: 	<@lit="--bartlett"> (Utiliza las desviaciones típicas de Bartlett)
		<@lit="--plot">=<@var="modo-o-nombrearchivo"> (Mira abajo)
		<@lit="--silent"> (No presenta nada)
		<@lit="--acf-only"> (Omite las autocorrelaciones parciales)
Ejemplos: 	<@lit="corrgm x 12">
		<@lit="corrgm GDP 12 --acf-only">

Muestra y/o representa los valores de la función de autocorrelación (FAC) de la serie <@var="y">, que puede especificarse por su nombre o por su número. Los valores se definen como ρ(<@mth="u"><@sub="t">, <@mth="u"><@sub="t-s">) donde <@mth="u"><@sub="t"> es la <@mth="t">-ésima observación de la variable <@mth="u">, y <@mth="s"> denota el número de retardos. 

A no ser que indiques la opción <@opt="--⁠acf-only">, también se presentan los coeficientes de la función de autocorrelación parcial (FACP, calculados utilizando el algoritmo de Durbin–Levinson), y que están libres de los efectos de los retardos intermedios. 

Se utilizan asteriscos para indicar la significación estadística de las autocorrelaciones individuales. Por defecto, esto se evalúa utilizando una desviación típica igual al cociente entre 1 y la raíz cuadrada del tamaño de la muestra; pero cuando indicas la opción <@opt="--⁠bartlett">, entonces se utilizan las desviaciones típicas de Bartlett para la FAC. Si resulta aplicable, esta opción también determina la banda de confianza que se dibuja en el gráfico de la FAC. Además, se muestra el estadístico <@mth="Q"> de Ljung–Box, que contrasta la hipótesis nula de que la serie es “ruido blanco” hasta el nivel de retardo indicado. 

Si especificas un valor para <@var="orden">, la longitud del correlograma se limita hasta ese número de retardos como máximo; en otro caso, la longitud se determina automáticamente como una función de la frecuencia de los datos y del número de observaciones. 

<subhead>Representación gráfica</subhead> 

Por defecto, si GRETL no está en modo de procesamiento por lotes, se genera un gráfico del correlograma. Esto puedes ajustarlo mediante la opción <@opt="--⁠plot"> en la que los parámetros que se admiten son: <@lit="none"> (para no mostrar el gráfico), <@lit="display"> (para presentar un gráfico incluso en modo de procesamiento por lotes); o un nombre de archivo. El efecto de indicar un nombre de archivo es como el descrito para la opción <@opt="--⁠output"> de la instrucción <@ref="gnuplot">. 

<subhead>Accesores</subhead> 

Cuando se complete con éxito esta instrucción, puedes usar los accesores <@xrf="$test"> y <@xrf="$pvalue"> para recuperar el estadístico <@mth="Q"> y su Probabilidad Asociada (valor <@mth="P">), evaluados para el retardo de orden mayor. Ten en cuenta que si únicamente quieres este contraste, puedes utilizar en su lugar la función <@xrf="ljungbox">. 

Menú gráfico: /Variable/Correlograma
Otro acceso: Ventana principal: Menú emergente (selección única)

# cusum Tests

Opciones: 	<@lit="--squares"> (Realiza el contraste CUSUMSQ)
		<@lit="--quiet"> (Solo presenta el contraste de Harvey–Collier)
		<@lit="--plot">=<@var="Modo-o-nombrearchivo"> (Mira abajo)

Debe ir después de la estimación de un modelo mediante MCO. Te permite realizar el contraste CUSUM de estabilidad de los parámetros (o el contraste CUSUMSQ si indicas la opción <@opt="--⁠squares">). Vas a obtener una serie con los errores de predicción adelantados un paso, ejecutando una serie de regresiones. En la primera regresión se utilizan las primeras <@mth="k"> observaciones y te permite generar la predicción de la variable dependiente en la observación <@mth="k"> + 1; en la segunda se utilizan las primeras <@mth="k"> + 1 observaciones y se genera una predicción para la observación <@mth="k"> + 2, y así sucesivamente (donde <@mth="k"> es el número de parámetros del modelo original). 

Se presenta la suma acumulada de los errores de predicción escalados (o los cuadrados de estos errores). La hipótesis nula de estabilidad de los parámetros se rechaza con un nivel de significación del 5 por ciento cuando la suma acumulada se separa de la banda de confianza del 95 por ciento. 

En el caso del contraste CUSUM, también se presenta el estadístico <@mth="t"> de Harvey–Collier para contrastar la hipótesis nula de estabilidad de los parámetros. Consulta el libro <@itl="Econometric Analysis"> de Greene para obtener más detalles. Para el contraste CUSUMSQ, se calcula la banda de confianza del 95 por ciento utilizando el algoritmo indicado en <@bib="Edgerton y Wells (1994);edgerton94">. 

Por defecto, cuando GRETL no está en modo de procesamiento por lotes, se muestra un gráfico con la serie acumulada y el intervalo de confianza. Puedes ajustar esto por medio de la opción <@opt="--⁠plot">. Los parámetros admisibles para esta opción son <@lit="none"> (para omitir el gráfico); <@lit="display"> (para visualizar un gráfico incluso en modo de procesamiento por lotes); o el nombre de un archivo. El efecto de proporcionar el nombre de un archivo es como el descrito para la opción <@opt="--⁠output"> de la instrucción <@ref="gnuplot">. 

Menú gráfico: Ventana de modelo: Contrastes/Contraste CUSUM(SQ)

# data Dataset

Argumento: 	<@var="listavariables"> 
Opciones: 	<@lit="--compact">=<@var="método"> (Especifica el método para compactar)
		<@lit="--quiet"> (No muestra los resultados excepto en caso de fallo)
		<@lit="--name">=<@var="identificador"> (Renombra series importadas)
		<@lit="--odbc"> (Importa de una base de datos ODBC)
		<@lit="--no-align"> (Específico para ODBC, mira abajo)

Lee las variables de <@var="listavariables"> de un archivo de base de datos (propio de GRETL, RATS 4.0 o PcGive) que debe abrirse previamente utilizando la instrucción <@ref="open">. Puedes usar la instrucción <@lit="data"> para importar series de DB.NOMICS o de una base de datos ODBC; para obtener detalles sobre estas variantes consulta <@mnu="gretlDBN"> o <@pdf="El manual de gretl#chap:odbc"> (Capítulo 42), respectivamente. 

Puedes establecer la frecuencia de los datos y el rango de la muestra mediante las instrucciones <@ref="setobs"> y <@ref="smpl">, antes de utilizar esta instrucción. Este es un ejemplo: 

<code>          
   open fedstl.bin
   setobs 12 2000:01
   smpl ; 2019:12
   data unrate cpiaucsl
</code>

Las instrucciones de arriba abren una base de datos (que se ofrece con GRETL) llamada <@lit="fedstl.bin">, determinan que los datos son mensuales, que empiezan en enero de 2000, que la muestra finaliza en diciembre de 2019, y que se importan las series denominadas <@lit="unrate"> (tasa de desempleo) y <@lit="cpiaucsl"> (IPC de todos). 

Si no especificas <@lit="setobs"> y <@lit="smpl"> de este modo, la frecuencia de los datos y el rango de la muestra se establecen utilizando la primera variable que se lee de la base de datos. 

Si las series que se van a leer son de frecuencia mayor que el conjunto de datos de trabajo, puedes especificar un método para compactar como aquí debajo: 

<code>          
   data LHUR PUNEW --compact=average
</code>

Los cinco métodos que permiten compactar de los que dispones son estos: “average” (toma la media de las observaciones de alta frecuencia), “last” (utiliza la última observación), “first”, “sum” y “spread”, pero si no especificas ningún método, por defecto se utiliza la media. El método “spread” es especial pues con él no se pierde ninguna información, sino que más bien esta se expande entre varias series, una por cada subperíodo. Así con ella cuando añades, por ejemplo, una serie mensual a un conjunto de datos trimestrales, se generan 3 series (una por cada mes del trimestre) cuyos nombres contienen los sufijos <@lit="m01">, <@lit="m02"> y <@lit="m03">. 

Cuando las series que se leen son de frecuencia <@itl="menor"> que la del conjunto de datos de trabajo, los valores de los datos añadidos sencillamente se repiten según se necesite; pero ten en cuenta que puedes utilizar la función <@xrf="tdisagg"> para solicitar que se haga una distribución o una interpolación (“desagregación temporal ”). 

En el caso de bases de datos propias (únicamente) de GRETL, puedes utilizar los caracteres “genéricos”, <@lit="*"> y <@lit="?"> en <@var="listavariables"> para importar series que coincidan con el patrón indicado. Por ejemplo, la siguiente expresión va a importar todas las series de la base de datos cuyos nombres comiencen por <@lit="cpi">: 

<code>          
   data cpi*
</code>

Puedes usar la opción <@opt="--⁠name"> para determinar un nombre distinto del nombre original en la base de datos, para la seire importada. El parámetro debe ser un identificador válido de GRETL. Esta opción se restringe al caso en el que especificas una única serie a importar. 

La opción <@opt="--⁠no-align"> se aplica solo para importar series mediante ODBC. Por defecto, se necesita que la solicitud ODBC devuelva información que indique a GRETL en qué filas del conjunto de datos situar los datos que se reciben (o que el número de valores que se reciben coincida, al menos, bien con la extensión del conjunto de datos o bien con la extensión del rango de la muestra vigente). Determinando la opción <@opt="--⁠no-align"> se relaja este requisito: si no se cumplen estas condiciones mencionadas, los valores que se reciben se colocan simplemente de forma consecutiva, comenzando en la primera fila del conjunto de datos. Si el número de esos valores es menor que el de filas en el conjunto de datos, las filas del final se rellenan con NAs; si el número es mayor que el de filas, se descartan los valores extra. Para obtener más información sobre cómo importar con ODBC, consulta <@pdf="El manual de gretl#chap:odbc"> (Capítulo 42). 

Menú gráfico: /Archivo/Bases de datos

# dataset Dataset

Argumentos: 	<@var="clave"> <@var="parámetros"> 
Opción: 	<@lit="--panel-time"> (Mira abajo 'addobs')
Ejemplos: 	<@lit="dataset addobs 24">
		<@lit="dataset addobs 2 --panel-time">
		<@lit="dataset insobs 10">
		<@lit="dataset compact 1">
		<@lit="dataset compact 4 last">
		<@lit="dataset expand">
		<@lit="dataset transpose">
		<@lit="dataset sortby x1">
		<@lit="dataset resample 500">
		<@lit="dataset renumber x 4">
		<@lit="dataset pad-daily 7">
		<@lit="dataset unpad-daily">
		<@lit="dataset clear">

Realiza diversas operaciones en el conjunto de datos como un todo, dependiendo de la <@var="clave"> indicada, que debe ser: <@lit="addobs">, <@lit="insobs">, <@lit="clear">, <@lit="compact">, <@lit="expand">, <@lit="transpose">, <@lit="sortby">, <@lit="dsortby">, <@lit="resample">, <@lit="renumber">, <@lit="pad-daily"> o <@lit="unpad-daily">. Advertencia: Con la excepción de la opción <@lit="clear">, estas acciones no están disponibles mientras tengas una submuestra del conjunto de datos, escogida por selección de los casos según algún criterio booleano. 

<@lit="addobs">: Debe estar seguido de un entero positivo, digamos <@mth="n">. Añade las <@mth="n"> observaciones adicionales al final del conjunto de datos de trabajo. Esto está pensado principalmente con el propósito de hacer predicciones. Los valores de la mayoría de las variables a lo largo del rango añadido se van a estipular como ausentes, pero ciertas variables determinísticas se reconocen y su contenido se extiende al rango añadido; en concreto, las variables con tendencia lineal simple y las variables ficticias periódicas. Si el conjunto de datos tiene la estructura de un panel, la acción predeterminada consiste en añadir <@mth="n"> unidades de sección cruzada al panel; pero si indicas la opción <@opt="--⁠panel-time">, el efecto consiste en añadir <@mth="n"> observaciones a las series temporales para cada unidad. 

<@lit="insobs">: Debe estar seguido de un entero positivo (no mayor que el número vigente de observaciones) que especifica la posición en la que se inserta una única observación. Todos los datos posteriores se desplazan un lugar y el conjunto de datos se amplía en una observación. Excepto a la constante, se le dan valores ausentes a todas las variables en la nueva observación. Esta acción no está disponible para conjuntos de datos de panel. 

<@lit="clear">: No necesita ningún parámetro. Vacía todos los datos vigentes, devolviendo el GRETL a su estado “vacío” inicial. 

<@lit="compact">: Esta acción solo está disponible para datos de series temporales, y compacta todas las series del conjunto de datos a una frecuencia menor. Requiere un parámetro, un entero positivo que represente la nueva frecuencia de los datos. En general, debe ser menor que la frecuencia vigente (por ejemplo, indicar un valor de 4 cuando la frecuencia vigente es 12, indica que se van a compactar los datos de mensuales a trimestrales). La única excepción es una nueva frecuencia de 52 (semanal) cuando los datos vigentes sean diarios (frecuencia 5, 6 o 7). También puedes indicar un segundo parámetro, en concreto uno de entre <@lit="sum">, <@lit="first">, <@lit="last"> o <@lit="spread">. Estos permiten especificar qué se va a compactar utilizando, respectivamente: la suma de los valores de frecuencia mayor, el valor de inicio-de-período, el valor de fin-de-período, o expandiendo los valores de frecuencia mayor entre varias series (una por cada subperíodo), pues por defecto se hace usando la media. 

En caso de querer compactar (unicamente) de frecuencia diaria a semanal, tienes disponibles las dos opciones especiales <@opt="--⁠repday"> y <@opt="--⁠weekstart">. La primera de ellas te permite seleccionar un “día representativo” de la semana que sirva como valor semanal. El parámetro de esta opción deberá consistir en un número entero entre 0 (Domingo) y 6 (Sábado), incluidos. Por ejemplo, indicando <@opt="--⁠repday=3"> estarás eligiendo el valor de los Miércoles como representativo del valor semanal. Si no indicas la opción <@opt="--⁠repday">, se necesita saber en que día de la semana pretendes comenzar, para alinear los datos correctamente. Con datos con frecuencia de 5 o de 6 días, siempre se toma el Lunes, pero con datos de 7 días tienes la posibilidad de elegir entre las opciones <@opt="--⁠weekstart=0"> (Domingo) y <@opt="--⁠weekstart=1"> (Lunes), en la que se toma el Lunes como la predeterminada. 

<@lit="expand">: Esta acción solo está disponible para datos de series temporales anuales o trimestrales, pues los datos anuales se pueden expandir a trimestrales o mensuales, y los datos trimestrales a mensuales. Todas las series del conjunto de datos se rellenan con la nueva frecuencia repitiendo los valores existentes. Si la base de datos original es anual, la expansión por defecto es la trimestral, pero la función <@lit="expand"> puede estar seguida de <@lit="12"> para solicitar que sea la mensual. Consulta la función <@xrf="tdisagg"> para obtener medios más sofisticados de convertir los datos a una frecuencia mayor. 

<@lit="transpose">: No necesita ningún parámetro adicional. Traspone el conjunto vigente de datos, es decir, cada observación (fila) del conjunto vigente de datos se va a tratar como una variable (columna), y cada variable como una observación. Esta acción te puede ser útil si los datos se leyeron de algún origen externo en el que las filas de la tabla de datos representan variables. 

<@lit="sortby">: Se requiere el nombre de una única serie o lista. Cuando indicas una serie, las observaciones de todas las variables del conjunto de datos se vuelven a ordenar según los valores ascendentes de la serie especificada. Cuando indicas una lista, la reordenación se hace jerárquicamente: si hay observaciones empatadas al reordenarse según la primera variable clave, entonces la segunda clave se utiliza para romper el empate, y así sucesivamente hasta que se rompa el empate o se agoten las claves. Ten en cuenta que esta acción está disponible solo para datos sin fecha. 

<@lit="dsortby">: Funciona como <@lit="sortby"> excepto que la reordenación se hace según los valores descendientes de la serie clave. 

<@lit="resample">: Construye un nuevo conjunto de datos mediante muestreo aleatorio (con substitución) de las filas del conjunto vigente de datos, y requiere que indiques como argumento el número concreto de filas que quieres incluir. Este puede ser menor, igual o mayor que el número de observaciones de los datos originales. Puedes recuperar el conjunto original de datos mediante la instrucción <@lit="smpl full">. 

<@lit="renumber">: Requiere el nombre de una serie ya existente seguida de un número entero entre 1 y el número de series del conjunto de datos menos 1. Mueve la serie especificada a la posición indicada del conjunto de datos, volviendo a numerar las demás series conforme a esto. (La posición 0 se ocupa con la constante, que no puede moverse.) 

<@lit="pad-daily">: Válido solo cuando el conjunto vigente de datos contiene datos con fechas diarias con un calendario incompleto. Tiene como efecto llenar los datos en un calendario completo insertando filas en blanco (es decir, filas que no contienen nada excepto <@lit="NA">s). Esta opción requiere un número entero como parámetro, concretamente el número de días por semana (5, 6 o 7), y que debe ser mayor o igual que la frecuencia vigente de los datos. Cuando se completa con éxito, el calendario de datos va a estar “completo” en relación a este valor. Por ejemplo, si días-por-semana es igual a 5, entonces se representan todos los días laborables, haya o no algún dato disponible para esos días. 

<@lit="unpad-daily">: Válido solo cuando el conjunto vigente de datos contiene datos con fechas diarias, en cuyo caso esto realiza la operación inversa a <@lit="pad-daily">. Es decir, se elimina cualquier fila que no contenga <@lit="NA">s, mientras que se conserva la propiedad de series temporales del conjunto de datos junto con las fechas de las observaciones individuales. 

Menú gráfico: /Datos

# delete Dataset

Variantes: 	<@lit="delete"> <@var="listavariables">
		<@lit="delete"> <@var="nombrevar">
		<@lit="delete --type="><@var="tipo">
		<@lit="delete"> <@var="nombrepaquete">
Opciones: 	<@lit="--db"> (Elimina series de la base de datos)
		<@lit="--force"> (Mira abajo)

Esta instrucción es un destructor. Deberías utilizarla con precaución pues no se pide confirmación. 

En la primera variante de arriba, <@var="listavariables"> es una lista de series, indicada por su nombre o número ID. Ten en cuenta que cuando eliminas series, se vuelve a numerar cualquier serie cuyo número ID sea mayor que los de las series de la lista que se elimina. Si indicas la opción <@opt="--⁠db">, las series de la lista no se eliminan con esta instrucción del conjunto vigente de datos, pero sí de la base de datos de GRETL (suponiendo que se abrió una de ellas y que el usuario tiene permisos para escribir en el archivo en cuestión). Consulta también la instrucción <@ref="open">. 

En la segunda variante, puedes indicar el nombre de un escalar, de una matriz, de una cadena de texto o de un bundle, para que se elimine. La opción <@opt="--⁠db"> no puede aplicarse en este caso. Ten en cuenta que no debes mezclar series y variables de diferentes tipos en una misma llamada a <@lit="delete">. 

En la tercera variante, la opción <@opt="--⁠type"> debes acompañarla con alguno de los siguientes nombres de tipos: <@lit="matrix">, <@lit="bundle">, <@lit="string">, <@lit="list">, <@lit="scalar"> o <@lit="array">; y su efecto consiste en eliminar todas las variables del tipo indicado. En este caso no debes indicar ningún argumento que no sea la opción. 

Puedes usar la cuarta variante para descargar un paquete de funciones. En este caso, debes proporcionar el sufijo <@lit=".gfn"> como en 

<code>          
   delete somepkg.gfn
</code>

Ten en cuenta que esto no elimina el archivo de paquete; únicamente descarga el paquete de la memoria. 

<subhead>Eliminar variables en un bucle</subhead> 

En general, no se permite eliminar variables en el contexto de un bucle, puesto que esto puede suponer un riesgo para la integridad del código del propio bucle. Sin embargo, si tienes total confianza en que la eliminación de una determinada variable va a ser inocua, puedes anular esta prohibición añadiendo la opción <@opt="--⁠force"> a la instrucción <@lit="delete">. 

Menú gráfico: Ventana principal: Menú emergente (selección única)

# diff Transformations

Argumento: 	<@var="listavariables"> 
Ejemplos: 	<@inp="penngrow.inp">, <@inp="sw_ch12.inp">, <@inp="sw_ch14.inp">

Con esta instrucción obtienes la primera diferencia de cada variable de <@var="listavariables">, y el resultado se guarda en una nueva variable con el prefijo <@lit="d_">. Así <@lit="diff x y"> genera las nuevas variables 

<mono>          
   d_x = x(t) - x(t-1)
   d_y = y(t) - y(t-1)
</mono>

Menú gráfico: /Añadir/Primeras diferencias de las variables seleccionadas

# difftest Tests

Argumentos: 	<@var="serie1"> <@var="serie2"> 
Opciones: 	<@lit="--sign"> (Contraste de los signos, por defecto)
		<@lit="--rank-sum"> (Contraste de la suma de rangos de Wilcoxon)
		<@lit="--signed-rank"> (Contraste de los rangos con signo de Wilcoxon)
		<@lit="--verbose"> (Presenta resultados adicionales)
		<@lit="--quiet"> (Suprime la presentación de resultados)
Ejemplos: 	<@inp="ooballot.inp">

Lleva a cabo un contraste no paramétrico sobre la diferencia entre dos poblaciones o grupos, en la que el contraste concreto depende de la opción seleccionada. 

Con la opción <@opt="--⁠sign">, se realiza el contraste de los signos. Este contraste se basa en el hecho de que, cuando se extraen dos muestras, <@mth="x"> e <@mth="y">, de forma aleatoria de una misma distribución, la probabilidad de que <@mth="x"><@sub="i"> > <@mth="y"><@sub="i">, para cada observación <@mth="i">, deberá ser igual a 0.5. El estadístico de contraste es <@mth="w">, es decir, el número de observaciones para las que se cumple que <@mth="x"><@sub="i"> > <@mth="y"><@sub="i">. Bajo la hipótesis nula, este estadístico sigue una distribución de probabilidad Binomial con parámetros (<@mth="n">, 0.5), donde <@mth="n"> indica el número de observaciones. 

Con la opción <@opt="--⁠rank-sum">, se realiza el contraste de la suma de rangos de Wilcoxon. Este contraste se desarrolla determinando el rango en jerarquía de las observaciones de ambas muestras juntas, desde la de menor valor hasta la de mayor, y luego calculando la suma de los rangos de las observaciones de una cualquiera de las dos muestras. No es necesario que las dos muestras tengan el mismo tamaño y, si son diferentes, se utiliza la muestra más pequeña para calcular la suma de los rangos. Bajo la hipótesis nula de que las muestras proceden de poblaciones con la misma mediana, la distribución de probabilidad de la suma de rangos puede calcularse para cualquier tamaño de muestra que se indique; y para muestras razonablemente largas, existe una estrecha aproximación Normal. 

Con la opción <@opt="--⁠signed-rank">, se realiza el contraste de los rangos con signo de Wilcoxon, que está ideada para pares de datos ligados como, por ejemplo, los pares de valores de una misma variable en una muestra de individuos, antes y después de algún tratamiento. El contraste se desarrolla calculando las diferencias entre las observaciones emparejadas <@mth="x"><@sub="i"> – <@mth="y"><@sub="i">, y determinando el rango de estas diferencias según su valor absoluto, además de asignándole a cada par, un rango con un signo que coincide con el signo de la diferencia. A continuación se calcula la suma de los rangos con signo positivo (<@mth="W"><@sub="+">). De igual modo que en el contraste de la suma de rangos, bajo la hipótesis nula de que la diferencia de las medianas es cero, este estadístico sigue una distribución de probabilidad bien definida, que converge a la Normal para muestras de tamaño razonable. 

Para los contrastes de Wilcoxon, cuando indicas la opción <@opt="--⁠verbose">, entonces se presenta la ordenación. (Esta opción no tiene efecto cuando se selecciona el contraste de los signos.) 

Al completarse con éxito, vas a tener disponibles los accesores <@xrf="$test"> y <@xrf="$pvalue">. Si únicamente quieres obtener estos valores, puedes añadir la opción <@opt="--⁠quiet"> a la instrucción. 

# discrete Transformations

Argumento: 	<@var="listavariables"> 
Opción: 	<@lit="--reverse"> (Marca las variables como continuas)
Ejemplos: 	<@inp="ooballot.inp">, <@inp="oprobit.inp">

Marca cada variable de <@var="listavariables"> como discreta pues, por defecto, todas las variables se tratan como continuas. Al hacer que una variable sea discreta, eso afecta al modo en el que se maneja esa variable en los gráficos de frecuencia, y también te permite escoger la variable para la instrucción <@ref="dummify">. 

Cuando especificas la opción <@opt="--⁠reverse">, la operación se invierte; es decir, las variables contenidas en <@var="listavariables"> se marcan como continuas. 

Menú gráfico: /Variable/Editar atributos

# dpanel Estimation

Argumento: 	<@var="p"> ; <@var="depvar"> <@var="indepvars"> [ ; <@var="instrumentos"> ] 
Opciones: 	<@lit="--quiet"> (No muestra el modelo estimado)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--two-step"> (Realiza la estimación MGM (GMM) en 2 etapas)
		<@lit="--system"> (Añade ecuaciones en niveles)
		<@lit="--collapse"> (Mira abajo)
		<@lit="--time-dummies"> (Añade variables ficticias temporales)
		<@lit="--dpdstyle"> (Imita el paquete DPD para Ox)
		<@lit="--asymptotic"> (Desviaciones típicas asintóticas sin corregir)
		<@lit="--keep-extra"> (Mira abajo)
Ejemplos: 	<@lit="dpanel 2 ; y x1 x2">
		<@lit="dpanel 2 ; y x1 x2 --system">
		<@lit="dpanel {2 3} ; y x1 x2 ; x1">
		<@lit="dpanel 1 ; y x1 x2 ; x1 GMM(x2,2,3)">
		Ver también <@inp="bbond98.inp">

Realiza la estimación de modelos dinámicos con datos de panel (es decir, modelos de panel que incluyen uno o más retardos de la variable dependiente) utilizando bien el método GMM-DIF o bien GMM-SYS. 

El parámetro <@var="p"> representa el orden de autorregresión para la variable dependiente. En el caso más sencillo, este parámetro es un valor escalar, pero también puedes indicar una matriz definida previamente para este argumento, para especificar con ello un conjunto de retardos (posiblemente no consecutivos) a utilizar. 

Debes indicar la variable dependiente y los regresores con sus valores en niveles, pues ya se van a diferenciar automáticamente (dado que este estimador utiliza la diferenciación para eliminar los efectos individuales). 

El último campo (opcional) de la instrucción es para especificar los instrumentos. Si no indicas ningún instrumento, se asume que todas las variables independientes son estrictamente exógenas. Si especificas cualquier instrumento, debes incluir en la lista cualquier variable independiente estrictamente exógena. Para los regresores predeterminados puedes utilizar la función <@lit="GMM"> para incluir un rango específico de retardos con el estilo diagonal por bloques, como se ilustra en el tercer ejemplo de arriba. El primer argumento de <@lit="GMM"> es el nombre de la variable en cuestión, el segundo es el retardo mínimo que se utiliza como instrumento, y el tercero es el retardo máximo. Puedes utilizar la misma sintaxis con la función <@lit="GMMlevel"> para especificar instrumentos de tipo GMM para las ecuaciones en niveles. 

Puedes usar la opción <@opt="--⁠collapse"> para limitar la proliferación de instrumentos de “estilo GMM”, lo que podría llegar a ser un problema con este estimador. Su efecto consiste en reducir ese tipo de instrumentos, de uno por cada retardo y por observación, a uno por cada retardo. 

Por defecto, se presentan los resultados de la estimación en 1 etapa (con las desviaciones típicas robustas) pero tienes la opción de escoger la estimación en 2 etapas. En ambos casos, se presentan los contrastes de autocorrelación de orden 1 y 2 , así como los contrastes de Sargan y/o de Hansen de sobreidentificación, y el estadístico del contraste de Wald para la significación conjunta de los regresores. Ten en cuenta que en este modelo en diferencias, la autocorrelación de primer orden no es una amenaza para la validez del modelo, pero la autocorrelación de segundo orden infringe los supuestos estadísticos vigentes. 

En el caso de la estimación en 2 etapas, las desviaciones típicas se calculan por defecto utilizando la corrección de muestra finita sugerida por <@bib="Windmeijer (2005);windmeijer05">. Generalmente se considera que las desviaciones típicas asintóticas estándar asociadas al estimador del método en 2 etapas, son una guía poco fiable para la inferencia, pero si por alguna razón quieres verlas, puedes utilizar la opción <@opt="--⁠asymptotic"> para desactivar la corrección de Windmeijer. 

Si indicas la opción <@opt="--⁠time-dummies">, se añade un conjunto de variables ficticias temporales a los regresores especificados. El número de estas variables ficticias es una menos que el número máximo de períodos usados en la estimación, para evitar que haya multicolinealidad perfecta con la constante. Las variables ficticias se introducen en forma de diferencias excepto que se indique la opción <@opt="--⁠dpdstyle">, en cuyo caso se introducen en niveles. 

De igual modo que con otras instrucciones para hacer la estimación, dispones de un 'bundle' <@xrf="$model"> después de realizarla. En caso de <@lit="dpanel">, puedes usar la opción <@opt="--⁠keep-extra"> para guardar información que quieras añadir a ese 'bundle', por ejemplo las matrices de ponderaciones y de instrumentos GMM. 

Para obtener otros detalles y ejemplos, consulta <@pdf="El manual de gretl#chap:dpanel"> (Capítulo 24). 

Menú gráfico: /Modelo/Panel/Modelo de panel dinámico

# dummify Transformations

Argumento: 	<@var="listavariables"> 
Opciones: 	<@lit="--drop-first"> (Excluye de la codificación al valor más bajo)
		<@lit="--drop-last"> (Excluye de la codificación al valor más alto)

Para cualquier variable adecuada de <@var="listavariables">, genera un conjunto de variables ficticias que codifican los distintos valores de esa variable. Las variables adecuadas son aquellas que se marcan explícitamente como discretas o aquellas que tienen un número claramente pequeño de valores, de los que todos ellos estén “claramente redondeadados” (múltiplos de 0.25). 

Por defecto, se añade una variable ficticia por cada valor diferente de la variable en cuestión. Por ejemplo, si una variable discreta <@lit="x"> tiene 5 valores diferentes, se añaden 5 variables ficticias al conjunto de datos, con los nombres <@lit="Dx_1">, <@lit="Dx_2">, etcétera. La primera variable ficticia va a tener el valor 1 en las observaciones donde <@lit="x"> toma su valor más pequeño y 0 en otro caso; la siguiente variable ficticia va a tener el valor 1 en las observaciones donde <@lit="x"> toma su segundo valor más pequeño, etcétera. Si añades uno de los indicadores de opción <@opt="--⁠drop-first"> o <@opt="--⁠drop-last">, entonces se omite del proceso de codificación bien el valor más bajo o bien el valor más alto de cada variable, respectivamente (lo que puede serte útil para evitar la “trampa de las variables ficticias”). 

También puedes insertar esta instrucción en el contexto de la especificación de una regresión. Por ejemplo, la siguiente línea especifica un modelo donde <@lit="y"> se regresa sobre el conjunto de variables ficticias que se codifican para <@lit="x">. (No puedes aplicar los indicadores de opción a <@lit="dummify"> en este contexto.) 

<code>          
   ols y dummify(x)
</code>

Otro acceso: Ventana principal: Menú emergente (selección única)

# duration Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> [ ; <@var="censuravar"> ] 
Opciones: 	<@lit="--exponential"> (Utiliza la distribución exponencial)
		<@lit="--loglogistic"> (Utiliza la distribución log-logística)
		<@lit="--lognormal"> (Utiliza la distribución log-normal)
		<@lit="--medians"> (Los valores ajustados son las medianas)
		<@lit="--robust"> (Desviaciones típicas robustas: CMV (QML))
		<@lit="--cluster">=<@var="clustervar"> (Consulta <@ref="logit"> para explicación)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--quiet"> (No presenta nada)
Ejemplos: 	<@lit="duration y 0 x1 x2">
		<@lit="duration y 0 x1 x2 ; cens">
		Ver también <@inp="weibull.inp">

Estima un modelo de duración en el que la variable dependiente (que debe ser positiva) representa la duración de algún estado de un asunto; por ejemplo, la duración del período de desempleo para una sección cruzada de encuestados. Por defecto, se utiliza la distribución de Weibull pero también están disponibles las distribuciones exponencial, log-logística y log-normal. 

Si algunas de las medidas de duración están censuradas por la derecha (e.g. el período del desempleo de un individuo aún no acabó dentro del período de observación), entonces debes indicar en el argumento posterior <@var="censuravar">, una serie en la que los valores no nulos indiquen los casos censurados por la derecha. 

Por defecto, los valores ajustados que obtienes mediante el accesor <@lit="$yhat"> son las medias condicionadas de las duraciones, pero cuando indicas la opción <@opt="--⁠medians"> entonces <@lit="$yhat"> te proporciona las medianas condicionadas en su lugar. 

Consulta <@pdf="El manual de gretl#chap:probit"> (Capítulo 38) para obtener más detalles. 

Menú gráfico: /Modelo/Variable dependiente limitada/Datos de duración

# elif Programming

Consulta <@ref="if">. 

# else Programming

Consulta <@ref="if">. Ten en cuenta que la instrucción <@lit="else"> necesita una línea para ella misma, antes de la siguiente instrucción condicional. Puedes añadirle un comentario, como en 

<code>          
   else # Correcto, hace algo distinto
</code>

Pero no puedes añadirle una instrucción, como en 

<code>          
   else x = 5 # Incorrecto!
</code>

# end Programming

Termina un bloque de instrucciones de algún tipo. Por ejemplo, <@lit="end system"> termina un sistema de ecuaciones (<@ref="system">). 

# endif Programming

Consulta <@ref="if">. 

# endloop Programming

Marca el final de un bucle de instrucciones. Consulta <@ref="loop">. 

# eqnprint Printing

Opciones: 	<@lit="--complete"> (Genera un documento completo)
		<@lit="--output">=<@var="nombrearchivo"> (Envía el resultado al archivo especificado)

Debe ir después de la estimación de un modelo y presenta el modelo estimado en formato de una ecuación LaTeX. Si especificas el nombre de un archivo utilizando la opción <@opt="--⁠output">, el resultado se dirige a ese archivo; en otro caso, se dirige a un archivo con un nombre con el estilo <@lit="equation_N.tex">, donde <@lit="N"> es el número de modelos estimados hasta ese momento en la sesión vigente. Consulta también <@ref="tabprint">. 

El archivo resultante va a escribirse en el directorio de trabajo (<@ref="workdir">) establecido en ese momento, excepto que la cadena <@var="nombrearchivo"> contenga una especificación completa de la ruta. 

Cuando especificas la opción <@opt="--⁠complete">, el archivo LaTeX es un documento completo (listo para procesar); en otro caso, debes incluirlo en un documento. 

Menú gráfico: Ventana de modelo: LaTeX

# equation Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> 
Ejemplo: 	<@lit="equation y x1 x2 x3 const">

Te permite especificar una de las ecuaciones de un sistema de ellas (consulta <@ref="system">). La sintaxis para especificar una ecuación de un sistema SUR es la misma que para, e.g., <@ref="ols">. Pero para una de las ecuaciones de un sistema a estimar con Mínimos Cuadrados en 3 etapas, puedes: (a) indicar una especificación de una ecuación como se estima con MCO y proporcionar una lista normal de instrumentos utilizando la palabra clave <@lit="instr"> (de nuevo, consulta <@ref="system">), o (b) utilizar la misma sintaxis de ecuaciones que para <@ref="tsls">. 

# estimate Estimation

Argumentos: 	[ <@var="nombresistema"> ] [ <@var="estimador"> ] 
Opciones: 	<@lit="--iterate"> (Itera hasta la convergencia)
		<@lit="--no-df-corr"> (Sin corrección de los grados de libertad)
		<@lit="--geomean"> (Mira abajo)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
Ejemplos: 	<@lit="estimate "Klein Model 1" method=fiml">
		<@lit="estimate Sys1 method=sur">
		<@lit="estimate Sys1 method=sur --iterate">

Solicita la estimación de un sistema de ecuaciones que debes definir previamente usando la instrucción <@ref="system">. Debes indicar primero el nombre del sistema, puesto entre comillas si el nombre contiene espacios. El estimador debe ser uno de los siguientes: <@lit="ols">, <@lit="tsls">, <@lit="sur">, <@lit="3sls">, <@lit="fiml"> o <@lit="liml">; y debes de ponerle antes la cadena de texto <@lit="method=">. Estos argumentos son opcionales si el sistema en cuestión ya se estimó, y ocupa el lugar del “último modelo”; en ese caso, el estimador que se toma por defecto será el utilizado previamente. 

Si el sistema en cuestión tuvo aplicadas un conjunto de restricciones (consulta la instrucción <@ref="restrict">), la estimación estará sujeta a las restricciones especificadas. 

Si el método de estimación es <@lit="sur"> o <@lit="3sls">, y especificas la opción <@opt="--⁠iterate">, se va a calcular el estimador iterativamente. En caso de SUR, si el procedimiento converge, los resultados son las estimaciones máximo verosímiles. La iteración de Mínimos Cuadrados en 3 Etapas (3sls), sin embargo, en general no converge a los resultados de la máxima verosimilitud con información completa (fiml). La opción <@opt="--⁠iterate"> se ignora para otros métodos de estimación. 

Si eliges los estimadores de ecuación a ecuación <@lit="ols"> o <@lit="tsls">, por defecto se aplica una corrección de los grados de libertad cuando se calculan las desviaciones típicas, pero puedes eliminar esto utilizando la opción <@opt="--⁠no-df-corr">. Esta opción no tiene efecto con los otros estimadores, y así no se aplica la corrección de los grados de libertad en ningún caso. 

Por defecto, la fórmula utilizada para calcular los elementos de la matriz de covarianzas de las ecuaciones cruzadas es 

  <@fig="syssigma1">

Cuando indicas la opción <@opt="--⁠geomean">, se aplica una corrección de los grados de libertad con lo que la fórmula en ese caso es 

  <@fig="syssigma2">

donde las <@mth="k">s denotan el número de parámetros independientes en cada ecuación. 

Cuando indicas la opción <@opt="--⁠verbose"> y especificas un método iterativo, se presentan detalles de las iteraciones. 

# eval Utilities

Argumento: 	<@var="expresión"> 
Ejemplos: 	<@lit="eval x">
		<@lit="eval inv(X'X)">
		<@lit="eval sqrt($pi)">

Esta instrucción hace que GRETL funcione como una sofisticada calculadora. El programa evalúa <@var="expresión"> y presenta su valor. El argumento puede ser el nombre de una variable, o algo más complicado. En cualquier caso, debe ser una expresión que puedas poner correctamente como el lado derecho de un enunciado de asignación (igualdad). 

En el uso interactivo (por ejemplo con la consola de GRETL), un signo igual funciona como una abreviatura de <@lit="eval">, como en 

<code>          
   =sqrt(x)
</code>

(con o sin espacio después de “=”). Pero esta variante no se admite en el modo de edición de guiones puesto que podría esconder fácilmente fallos de codificación. 

En la mayoría de los contextos, puedes usar la instrucción <@ref="print"> en lugar de <@lit="eval"> para obtener el mismo efecto. Consulta también <@ref="printf"> para aquellos casos en los que quieras combinar resultados de texto y numéricos. 

# fcast Prediction

Variantes: 	<@lit="fcast ["><@var="obsinicio obsfin"><@lit="] ["><@var="nombrev"><@lit="]">
		<@lit="fcast ["><@var="obsinicio obsfin"><@lit="]"> <@var="pasosadelante"> <@lit="["><@var="nombrev"><@lit="] --recursive">
Opciones: 	<@lit="--dynamic"> (Genera la predicción dinámica)
		<@lit="--static"> (Genera la predicción estática)
		<@lit="--out-of-sample"> (Genera la predicción postmuestral)
		<@lit="--no-stats"> (No presenta las estadísticas de predicción)
		<@lit="--stats-only"> (Presenta solo las estadísticas de predicción)
		<@lit="--quiet"> (No presenta nada)
		<@lit="--recursive"> (Mira abajo)
		<@lit="--all-probs"> (Mira abajo)
		<@lit="--plot">=<@var="nombrearchivo"> (Mira abajo)
Ejemplos: 	<@lit="fcast 1997:1 2001:4 f1">
		<@lit="fcast fit2">
		<@lit="fcast 2004:1 2008:3 4 rfcast --recursive">
		Ver también <@inp="gdp_midas.inp">

Debe ir después de una instrucción de estimación. Las predicciones se generan para cierto rango de observaciones que será, bien el definido cuando indicas <@var="obsinicio"> y <@var="obsfin"> (de ser posible), bien el definido por las observaciones que van a continuación del rango sobre el que se estimó el modelo cuando indicas la opción <@opt="--⁠out-of-sample">, o bien, en otro caso, el rango de la muestra definido en ese momento. Cuando solicitas una predicción 'out-of-sample' pero no hay disponibles observaciones relevantes, se muestra un fallo. Dependiendo de la naturaleza del modelo, también pueden generarse las desviaciones típicas (mira abajo). También mira abajo para indagar sobre el efecto especial de la opción <@opt="--⁠recursive">. 

Si el último modelo estimado tiene una única ecuación, entonces el argumento <@var="nombrev"> (opcional) tiene el siguiente efecto: no se presentan los valores de la predicción, sino que se guardan en el conjunto de datos con el nombre indicado. Si el último modelo es un sistema de ecuaciones, <@var="nombrev"> tiene un efecto distinto ya que, en concreto, escoge una variable endógena en particular para hacer la predicción (pues por defecto se generan las predicciones para todas las variables endógenas). En caso de un sistema o si no indicas <@var="nombrev">, puedes recuperar los valores de predicción utilizando el accesor <@xrf="$fcast"> y, si están disponibles, las desviaciones típicas mediante <@xrf="$fcse">. 

<subhead>Predicciones estáticas y dinámicas</subhead> 

La elección entre una predicción estática o dinámica se aplica únicamente en caso de modelos dinámicos, con una perturbación con un proceso autorregresivo y/o que incluyan uno o más valores retardados de la variable dependiente como regresores. Las predicciones estáticas son un paso adelantadas (basadas en los valores obtenidos en el período previo), mientras que las predicciones dinámicas emplean la regla de la cadena de predicción. Por ejemplo, si una predicción para <@mth="y"> en 2008 requiere como entrada un valor de <@mth="y"> en 2007, una predicción estática es imposible sin datos actualizados para 2007, pero una predicción dinámica para 2008 es posible si puedes substituir una predicción previa para <@mth="y"> en 2007. 

Por defecto se proporciona: (a) una predicción estática para alguna porción del rango de predicción que cae dentro del rango de la muestra sobre el que se estima el modelo, y (b) una predicción dinámica (si es relevante) fuera de la muestra. La opción <@opt="--⁠dynamic"> solicita una predicción dinámica a partir de la fecha lo más temprana posible, y la opción <@opt="--⁠static"> solicita una predicción estática incluso fuera de la muestra. 

<subhead>Predicciones recursivas</subhead> 

La opción <@opt="--⁠recursive"> está actualmente disponible solo para modelos de una sola ecuación, estimados mediante MCO. Cuando indicas esta opción las predicciones son recursivas; es decir, cada predicción se genera a partir de una estimación del modelo indicado, utilizando los datos a partir de un punto de inicio fijado (en concreto, el inicio del rango de la muestra para la estimación original) hasta la fecha de predicción menos <@mth="k">, el número de pasos adelantados que debes indicar en el argumento <@var="pasosadelante">. Las predicciones siempre son dinámicas si eso es pertinente. Ten en cuenta que debes indicar el argumento <@var="pasosadelante"> únicamente junto con la opción <@opt="--⁠recursive">. 

<subhead>Modelos ordenados y modelo multinomial</subhead> 

Cuando la estimación es mediante logit o probit ordenados, o logit multinomial, podrías estar interesado en las probabilidades estimadas de cada uno de los resultados discretos, mejor que únicamente en el resultado “más probable” para cada observación. Esto se consigue mediante la opción <@opt="--⁠all-probs">: el efecto de <@lit="fcast"> es en ese caso una matriz con una columna por cada resultado posible. Puedes usar el argumento <@var="vname"> para nombrar esa matriz, en cuyo caso no se presenta nada. Si no indicas <@var="vname">, entonces la matriz se puede recuperar mediante <@xrf="$fcast">. La opción <@opt="--⁠plot"> no es compatible con la opción <@opt="--⁠all-probs">. 

<subhead>Gráficos de predicciones</subhead> 

La opción <@opt="--⁠plot"> solicita que se produzca un archivo gráfico, que contiene una representación gráfica de la predicción. En el caso de un sistema, esta opción solo está disponible cuando se usa el argumento <@var="nombrev"> para seleccionar una única variable para hacer la predición. El sufijo del argumento <@var="nombrearchivo"> de esta opción, controla el formato del gráfico: <@lit=".eps"> para EPS, <@lit=".pdf"> para PDF, <@lit=".png"> para PNG, y <@lit=".plt"> para un archivo de instrucciones de Gnuplot. Puedes utilizar el nombre ficticio <@lit="display"> en substitución del nombre de archivo para forzar la representación del gráfico en una ventana. Por ejemplo, 

<code>          
   fcast --plot=fc.pdf
</code>

va a generar un gráfico con formato PDF. Se respetan los nombres de rutas que no ofrezcan dudas; en otro caso, los archivos se escriben en el directorio de trabajo de GRETL. 

<subhead>Desviaciones típicas</subhead> 

La naturaleza de las desviaciones típicas de las predicciones (si están disponibles) depende de la naturaleza del modelo y de la predicción. En modelos lineales estáticos, las desviaciones típicas se calculan utilizando el método bosquejado por <@bib="Davidson y MacKinnon (2004);davidson-mackinnon04">; ellos incorporan tanto la incertidumbre debida al proceso de la perturbación como la incertidumbre en los parámetros (resumida en la matriz de covarianzas de los estimadores de los parámetros). En modelos dinámicos, las desviaciones típicas de las predicciones se calculan únicamente en caso de una predicción dinámica, y no incorporan la incertidumbre en los parámetros. Para modelos no lineales, las desviaciones típicas de las predicciones no están disponibles actualmente. 

Menú gráfico: Ventana de modelo: Análisis/Predicciones

# flush Programming

Esta sencilla instrucción (sin argumentos, sin opciones) está ideada para ser usada en guiones que llevan algo de tiempo, y que deben ejecutarse con la Interfaz Gráfica de Usuario (GUI) de GRETL (el programa de líneas de instrucción lo ignora), para darle al usuario un indicio visual de que las cosas se están moviendo y GRETL no está “parado”. 

Generalmente, si lanzas un guion en la Interfaz Gráfica de Usuario (GUI), no se muestra el resultado hasta que se complete su ejecución, pero el efecto de invocar <@lit="flush"> es como se indica a continuación: 

<indent>
• En la primera llamada, GRETL abre una ventana, muestra los resultados hasta el presente y añade el mensaje “Procesando...”. 
</indent>

<indent>
• Tras invocaciones posteriores, se actualiza el texto que se muestra en la ventana de resultados, y se añade un nuevo mensaje “Procesando”. 
</indent>

Cuando se completa la ejecución del guion, cualquier resultado que quede pendiente se descarga automáticamente en la ventana de texto. 

Ten en cuenta que no tiene sentido que utilices <@lit="flush"> en guiones que tarden menos de (digamos) 5 segundos en ejecutarse. También ten en cuenta que no deberías utilizar esta instrucción en un lugar del guion donde no hay resultados posteriores que presentar, ya que el mensaje “Procesando” será entonces engañoso para el usuario. 

El siguiente código ilustra el uso que se pretende con <@lit="flush">: 

<code>          
       set echo off
       scalar n = 10
       loop i=1..n
           # Hacer una operación que lleve algo de tiempo
           loop 100 --quiet
               a = mnormal(200,200)
               b = inv(a)
           endloop
           # Presentar algunos resultados
           printf "Iteración %2d hecha\n", i
           if i < n
               flush
           endif
       endloop
</code>

# foreign Programming

Sintaxis: 	<@lit="foreign language="><@var="ling">
Opciones: 	<@lit="--send-data">[=<@var="lista">] (Carga previamente los datos; mira abajo)
		<@lit="--quiet"> (Elimina los resultados del programa externo)

Esta instrucción abre un modo especial en el que se admiten instrucciones que van a ejecutarse con otro programa. Puedes salir de este modo con <@lit="end foreign"> y, en ese punto, se ejecutan las instrucciones acumuladas. 

Actualmente los programas “externos” a los que se les da apoyo de este modo son GNU R (<@lit="language=R">), Python, Julia, GNU Octave (<@lit="language=Octave">), Ox de Jurgen Doornik y Stata. Los nombres de los lenguajes se reconocen en términos que no distinguen mayúsculas y minúsculas. 

Junto con R, Octave y Stata, la opción <@opt="--⁠send-data"> tiene como efecto el hacer accesibles los datos del espacio de trabajo del GRETL dentro del programa señalado. Por defecto, se envía el conjunto completo de datos, pero puedes limitar los datos que se van a enviar indicando el nombre de una lista de series definida previamente. Por ejemplo: 

<code>          
   list Rlist = x1 x2 x3
   foreign language=R --send-data=Rlist
</code>

Consulta <@pdf="El manual de gretl#chap:gretlR"> (Capítulo 44) para obtener más detalles y ejemplos. 

# fractint Statistics

Argumentos: 	<@var="serie"> [ <@var="orden"> ] 
Opciones: 	<@lit="--gph"> (Hace el contraste de Geweke y Porter-Hudak)
		<@lit="--all"> (Hace ambos contrastes)
		<@lit="--quiet"> (No presenta los resultados)

Comprueba la integración fraccional (“memoria larga”) de las series especificadas contrastando la hipótesis nula de que el orden de integración de la serie es cero. Por defecto, se utiliza el Estimador Local Whittle <@bib="(Robinson, 1995);robinson95">, pero cuando indicas la opción <@opt="--⁠gph">, se realiza el contraste GPH <@bib="(Geweke y Porter-Hudak, 1983);GPH83"> en su lugar. Cuando decidas indicar la opción <@opt="--⁠all">, entonces se van a presentar los resultados de ambos contrastes. 

Para obtener más detalles sobre este tipo de contraste, consulta <@bib="Phillips y Shimotsu (2004);phillips04">. 

Cuando no indicas el argumento <@var="orden"> (opcional), el orden para el(los) contraste(s) se establece automáticamente como el número menor entre <@mth="T">/2 y <@mth="T"><@sup="0.6">. 

Los órdenes estimados de integración fraccional y sus desviaciones típicas correspondientes están disponibles a través del accesor <@xrf="$result">. Con la opción <@opt="--⁠all">, encontrarás la estimación Local Whittle en la primera fila y la estimación GPH en la segunda. 

Puedes recuperar los resultados del test utilizando los accesores <@xrf="$test"> y <@xrf="$pvalue">. Estos valores se basan en el Estimador Local Whittle excepto cuando indicas la opción <@opt="--⁠gph">. 

Menú gráfico: /Variable/Contrastes de raíz unitaria/Integración fraccional

# freq Statistics

Argumento: 	<@var="variable"> 
Opciones: 	<@lit="--nbins">=<@var="n"> (Especifica el número de intervalos)
		<@lit="--min">=<@var="valormínimo"> (Especifica el mínimo, mira abajo)
		<@lit="--binwidth">=<@var="ancho"> (Especifica el ancho del intervalo, mira abajo)
		<@lit="--normal"> (Contrasta la distribución Normal)
		<@lit="--gamma"> (Contrasta la distribución Gamma)
		<@lit="--silent"> (No presenta nada)
		<@lit="--matrix">=<@var="nombrematriz"> (Utiliza una columna de la matriz indicada)
		<@lit="--plot">=<@var="modo-o-nombrearchivo"> (Mira abajo)
Ejemplos: 	<@lit="freq x">
		<@lit="freq x --normal">
		<@lit="freq x --nbins=5">
		<@lit="freq x --min=0 --binwidth=0.10">

Si no indicas opciones, muestra la distribución de frecuencias de la serie <@var="variable"> (indicada por su nombre o número) en formato tabular, con el número de intervalos y sus tamaños elegidos automáticamente, con o sin un gráfico adjunto tal como se explica más abajo. Cuando se completa la instrucción con éxito, puedes recuperar la tabla de frecuencias como una matriz utilizando el accesor <@xrf="$result">. 

Cuando indicas la opción <@opt="--⁠matrix">, entonces <@var="variable"> debe ser un número entero y se interpreta en este caso como un índice que escoge una columna de la matriz indicada. Si la matriz en cuestión es realmente un vector columna, puedes omitir este argumento <@var="variable">. 

Por defecto, la distribución de frecuencias utiliza un número de intervalos calculado automáticamente si los datos son continuos, o no agrupa en intervalos si los datos son discretos. Para controlar este aspecto puedes: (a) usar la instrucción <@ref="discrete"> para establecer el estatus de la <@var="variable">, o (b), si los datos son continuos, especificar <@itl="o"> el número de intervalos, o el valor mínimo junto con el ancho de los intervalos, como se muestra en los dos últimos ejemplos de arriba. La opción <@opt="--⁠min"> establece el límite inferior del intervalo situado más a la izquierda. 

Cuando indicas la opción <@opt="--⁠normal">, se calcula el estadístico chi-cuadrado de Doornik–Hansen para contrastar la Normalidad. Cuando indicas la opción <@opt="--⁠gamma">, el contraste de Normalidad se substituye por el contraste no paramétrico de Locke respecto a la hipótesis nula de que una variable sigue una distribución Gamma; consulta <@bib="Locke (1976);locke76">, y también <@bib="Shapiro y Chen (2001);shapiro-chen01">. Ten en cuenta que la forma en la que se indican en GRETL los parámetros de la distribución Gamma utilizada es (forma, escala). 

Por defecto, si GRETL no está en modo de procesamiento por lotes, se muestra un gráfico de la distribución, pero puedes ajustar esto mediante la opción <@opt="--⁠plot">. Los parámetros admisibles para esta opción son: <@lit="none"> (para suprimir el gráfico), <@lit="display"> (para mostrar un gráfico incluso cuando estés en modo de procesamiento por lotes), o un nombre de archivo. El efecto de indicar un nombre de archivo es como se describe para la opción <@opt="--⁠output"> de la instrucción <@ref="gnuplot">. 

La opción <@opt="--⁠silent"> elimina el resultado de texto habitual. Puedes utilizar esto junto con una u otra de las opciones para contrastes de distribución; entonces se registran el estadístico de prueba más su probabilidad asociada, y puedes recuperarlos utilizando los accesores <@xrf="$test"> y <@xrf="$pvalue">. También puedes usar esto junto con la opción <@opt="--⁠plot"> si únicamente quieres un histograma y no te interesa mirar el texto que lo acompaña. 

Ten en cuenta que GRETL no tiene una función que se corresponda con esta instrucción, pero resulta posible utilizar la función <@xrf="aggregate"> para lograr el mismo objetivo. Además, puedes obtener la distribución de frecuencias que se genera con la instrucción <@lit="freq">, en forma de matriz, por medio del accesor <@xrf="$result">. 

Menú gráfico: /Variable/Distribución de frecuencias

# funcerr Programming

Argumento: 	[ <@var="mensaje"> ] 

Solo es aplicable en el contexto de una función definida por el usuario (consulta <@ref="function">). Provoca que la ejecución de la función actual, finalice con la señalización de una condición de fallo. 

El argumento <@var="mensaje"> (opcional) puede tener la forma de una cadena de texto literal, o del nombre de una variable de cadena; si está presente, se presenta como parte del mensaje de fallo que se le muestra a quien invoca la función. 

Consulta también la función estrechamente vinculada, <@xrf="errorif">. 

# function Programming

Argumento: 	<@var="nombrefunción"> 

Abre un bloque de expresiones en las que se define una función. Este bloque debe estar terminado con <@lit="end function">. (Como excepción está el caso en el que desees eliminar una función definida por el usuario, pues lo puedes conseguir mediante la sencilla linea de instrucción <@lit="function foo delete"> para a función chamada “foo”.) Consulta <@pdf="El manual de gretl#chap:functions"> (Capítulo 14) para obtener más detalles. 

# garch Estimation

Argumentos: 	<@var="p"> <@var="q"> ; <@var="depvar"> [ <@var="indepvars"> ] 
Opciones: 	<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--quiet"> (No presenta nada)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--nc"> (Sin constante)
		<@lit="--stdresid"> (Tipifica los errores)
		<@lit="--fcp"> (Utiliza el algoritmo de Fiorentini, Calzolari y Panattoni)
		<@lit="--arma-init"> (Parámetros iniciales de la varianza partiendo de ARMA)
Ejemplos: 	<@lit="garch 1 1 ; y">
		<@lit="garch 1 1 ; y 0 x1 x2 --robust">
		Ver también <@inp="garch.inp">, <@inp="sw_ch14.inp">

Estima un modelo GARCH (GARCH, Heterocedasticidad Condicional Autorregresiva Generalizada), bien en un modelo univariante o bien incluyendo las variables exógenas indicadas si especificas <@var="indepvars">. Los valores enteros <@var="p"> y <@var="q"> (que puedes indicar en formato numérico o con nombres de variables escalares ya existentes) representan los órdenes de retardo en la ecuación de varianza condicional: 

  <@fig="garch_h">

Así, el parámetro <@var="p"> representa el orden Generalizado (o “AR”), mientras que <@var="q"> representa el orden normal ARCH (o “MA”). Cuando <@var="p"> es no nulo, <@var="q"> también debe ser no nulo; en otro caso, el modelo no está identificado. Con todo, puedes estimar un modelo ARCH normal estableciendo que <@var="q"> es un valor positivo, y que <@var="p"> es cero. La suma de <@var="p"> y <@var="q"> no debe ser mayor que 5. Ten en cuenta que se incluye automáticamente una constante en la ecuación media, excepto cunado indiques la opción <@opt="--⁠nc">. 

Por defecto, se utiliza el propio código de GRETL para estimar los modelos GARCH, pero también tienes la opción de usar el algoritmo de <@bib="Fiorentini, Calzolari y Panattoni (1996);fiorentini96">. El primero utiliza el maximizador BFGS mientras que el último usa la matriz de información para maximizar la verosimilitud, con una puesta a punto mediante la matriz Hessiana. 

Con esta instrucción dispones de diversas variantes de la matriz estimada de las covarianzas de los estimadores. Por defecto, se usa la matriz Hessiana excepto que indiques la opción <@opt="--⁠robust">, en cuyo caso se va a usar la matriz de covarianzas CMV (QML de White). También se pueden especificar otras posibilidades (e.g. la matriz de información o el estimador de Bollerslev–Wooldridge) mediante la clave <@lit="garch_vcv"> bajo la instrucción <@ref="set">. 

Por defecto, las estimaciones de los parámetros de la varianza se inician usando la varianza de la perturbación no condicionada de una estimación inicial por MCO (para la constante) y valores positivos pequeños (para los coeficientes que acompañan a los valores pasados tanto de las perturbaciones cuadradas como de la varianza de la perturbación). La opción <@opt="--⁠arma-init"> solicita que, para establecer los valores iniciales de estos parámetros, se utilice un modelo inicial ARMA, explotando la relación entre GARCH y ARMA expuesta en el capítulo 21 del libro <@itl="Time Series Analysis"> de Hamilton. En algunos casos, esto puede mejorar las posibilidades de convergencia. 

Puedes recuperar los errores GARCH y la varianza condicionada estimada con <@lit="$uhat"> y <@lit="$h">, respectivamente. Por ejemplo, para obtener la varianza condicional: 

<code>          
   series ht = $h
</code>

Cuando indicas la opción <@opt="--⁠stdresid">, se dividen los valores de <@lit="$uhat"> por la raíz cuadrada de <@mth="h"><@sub="t">. 

Menú gráfico: /Modelo/Series temporales univariantes/GARCH

# genr Dataset

Argumentos: 	<@var="nuevavariable"> <@var="= fórmula"> 

NOTA: Esta instrucción experimentó numerosos cambios y mejoras desde que se escribió el siguiente texto de ayuda, por eso para comprender y actualizar la información sobre esta instrucción, deberás seguir la referencia de <@pdf="El manual de gretl#chap:genr"> (Capítulo 10). Por otro lado, esta ayuda no contiene nada actualmente incorrecto, por lo que interpreta lo que sigue como “tienes esto, y más”. 

Para esta instrucción y en el contexto apropiado, las expresiones <@lit="series">, <@lit="scalar">, <@lit="matrix">, <@lit="string">, <@lit="bundle"> y <@lit="array"> son sinónimos. 

Genera nuevas variables, habitualmente mediante transformaciones de las variables ya existentes. Consulta también <@ref="diff">, <@ref="logs">, <@ref="lags">, <@ref="ldiff">, <@ref="sdiff"> y <@ref="square"> como atajos. En el contexto de una fórmula <@lit="genr">, debes hacer referencia a las variables ya existentes mediante su nombre, no con su número ID. La fórmula debe ser una combinación bien hecha de nombres de variables, constantes, operadores y funciones (descrito más abajo). Ten en cuenta que puedes encontrar más detalles sobre algunos aspectos de esta instrucción en <@pdf="El manual de gretl#chap:genr"> (Capítulo 10). 

Una instrucción <@lit="genr"> puede producir un resultado escalar o una serie. Por ejemplo, la fórmula <@lit="x2 = x * 2"> naturalmente produce una serie cuando la variable <@lit="x"> es una serie, y un escalar cuando <@lit="x"> es un escalar. Las fórmulas <@lit="x = 0"> y <@lit="mx = mean(x)"> naturalmente devuelven escalares. Bajo ciertas circunstancias, puedes querer tener un resultado escalar ampliado a una serie o vector; esto puedes hacerlo utilizando <@lit="series"> como un “alias” para la instrucción <@lit="genr">. Por ejemplo, <@lit="series x = 0"> produce una serie en la que todos sus valores se ponen a 0. También puedes utilizar <@lit="scalar"> como alias de <@lit="genr">. No es posible forzar a un resultado en forma de vector que sea un escalar, pero la utilización de esta palabra clave indica que el resultado <@itl="debiera de ser"> un escalar: si no lo es, aparece un fallo. 

Cuando una fórmula produce un resultado en forma de serie, el rango sobre el que se escribe ese resultado en la variable objetivo depende de la configuración vigente de la muestra. Por lo tanto, puedes definir una serie hecha a trozos utilizando la instrucción <@lit="smpl"> junto con <@lit="genr">. 

Se admiten los <@itl="operadores aritméticos">, en orden de prioridad: <@lit="^"> (elevar a la potencia); <@lit="*">, <@lit="/"> y <@lit="%"> (módulo o resto); <@lit="+"> y <@lit="-">. 

Los <@itl="operadores booleanos "> disponibles son (de nuevo, en orden de prioridad): <@lit="!"> (negación), <@lit="&&"> (Y lógico), <@lit="||"> (O lógico), <@lit=">">, <@lit="<">, <@lit="=="> (igual a), <@lit=">="> (mayor o igual que), <@lit="<="> (menor o igual que) y <@lit="!="> (no igual). También puedes utilizar los operadores booleanos en la construcción de variables ficticias: por ejemplo, <@lit="(x > 10)"> devuelve 1 en caso de que <@lit="x"> > 10, y 0 en otro caso. 

Las constantes integradas son <@lit="pi"> y <@lit="NA">. La última es el código de valor ausente: puedes iniciar una variable con el valor ausente mediante <@lit="scalar x = NA">. 

La instrucción <@lit="genr"> admite un amplio rango de funciones matemáticas y estadísticas, incluyendo todas las habituales más varias que son especiales de Econometría. Además, ofrece acceso a muchas variables internas que se definen durante la ejecución de las regresiones, la realización de contrastes de hipótesis, etcétera. Para ver un listado de funciones y accesores, consulta <@gfr="Referencia de funciones de gretl">. 

Además de los operadores y de las funciones indicados arriba, hay algunos usos especiales de <@lit="genr">: 

<indent>
• <@lit="genr time"> genera una variable de tendencia temporal (1,2,3,…) llamada <@lit="time">. Y <@lit="genr index"> tiene el mismo efecto, salvo que la variable se llama <@lit="index">. 
</indent>

<indent>
• <@lit="genr dummy"> genera tantas variables ficticias como sea la periodicidad de los datos. En caso de tener datos trimestrales (periodicidad 4), el programa genera <@lit="dq1"> = 1 para el primer trimestre y 0 para los otros trimestres, <@lit="dq2"> = 1 para el segundo trimestre y 0 para los otros trimestres, etcétera. Con datos mensuales, las variables ficticias se nombran <@lit="dm1">, <@lit="dm2">, etcétera; con datos diarios, se nombran <@lit="dd1">, <@lit="dd2">, etcétera; y con otras frecuencias, los nombres son <@lit="dummy_1">, <@lit="dummy_2">, etc. 
</indent>

<indent>
• <@lit="genr unitdum"> y <@lit="genr timedum"> generan conjuntos de variables ficticias especiales para utilizar con datos de panel, codificando las unidades de sección cruzada con la primera, y el período de tiempo de las observaciones con la segunda. 
</indent>

<@itl="Advertencia">: Con el programa en líneas de instrucción, las instrucciones <@lit="genr"> que recuperan datos relacionados con un modelo, siempre se refieren al modelo que se estimó más recientemente. Esto también es cierto en el programa de Interfaz Gráfica de Usuario (GUI), cuando utilizas <@lit="genr"> en la “consola de GRETL”o si introduces una fórmula usando la opción “Definir nueva variable” bajo el menú Añadir en la ventana principal. Con la GUI, sin embargo, tienes la opción de recuperar datos de cualquiera de los modelos que se muestran en ese momento en una ventana (sea o no sea el modelo estimado más recientemente). Puedes hacer esto bajo el menú “Guardar” de la ventana del modelo correspondiente. 

La variable especial <@lit="obs"> sirve como índice para las observaciones. Por ejemplo, <@lit="series dum = (obs==15)"> genera una variable ficticia que tiene valor 1 para la observación 15, y el valor 0 en otro caso. También puedes usar esta variable para escoger observaciones concretas por fecha o nombre. Por ejemplo, <@lit="series d = (obs>1986:4)">, <@lit="series d = (obs>"2008-04-01")">, o <@lit="series d = (obs=="CA")">. Cuando utilizas fechas diarias o marcadores de observación en este contexto, debes ponerlas entre comillas, pero puedes usar las fechas trimestrales y mensuales (con dos puntos) sin comillas. Ten en cuenta que, en caso de datos de series temporales anuales, el año no se distingue sintácticamente de un sencillo número entero. Por lo tanto, si quieres comparar observaciones frente a <@lit="obs"> por año, debes usar la función <@lit="obsnum"> para convertir así el año en un valor índice en base 1, como se hace en <@lit="series d = (obs>obsnum(1986))">. 

Puedes sacar los valores escalares de una serie en el contexto de una fórmula <@lit="genr">, utilizando la sintaxis <@var="varname"><@lit="["><@var="obs"><@lit="]"> en la que puedes indicar el valor <@var="obs"> por número o fecha. Ejemplos: <@lit="x[5]">, <@lit="CPI[1996:01]">. Para datos diarios, debes usar la forma <@var="YYYY-MM-DD">; e.g. <@lit="ibm[1970-01-23]">. 

Puedes modificar una observación individual de una serie mediante <@lit="genr">. Para hacer esto, debes añadir un número válido de observación o de fecha, entre corchetes, al nombre de la variable en el lado izquierdo de la fórmula. Por ejemplo, <@lit="genr x[3] = 30"> o <@lit="genr x[1950:04] = 303.7">. 

Menú gráfico: /Añadir/Definir nueva variable
Otro acceso: Ventana principal: Menú emergente

# gmm Estimation

Opciones: 	<@lit="--two-step"> (Estimación en 2 etapas)
		<@lit="--iterate"> (GMM iterados)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--quiet"> (No presenta nada)
		<@lit="--lbfgs"> (Utiliza L-BFGS-B en lugar del BFGS normal)
Ejemplos: 	<@inp="hall_cbapm.inp">

Realiza la estimación con el Método Generalizado de los Momentos (MGM o GMM) utilizando el algoritmo BFGS (Broyden, Fletcher, Goldfarb, Shanno). Debes especificar: (a) una o más instrucciones para actualizar las cantidades relevantes (típicamente errores GMM), (b) uno o más conjuntos de condiciones de ortogonalidad, (c) una matriz inicial de ponderaciones, y (d) un listado con los parámetros a estimar, todo puesto entre las etiquetas <@lit="gmm"> y <@lit="end gmm">. Cualquier opción deberá añadirse a la línea <@lit="end gmm">. 

Consulta <@pdf="El manual de gretl#chap:gmm"> (Capítulo 27) para obtener más detalles sobre esta instrucción. Aquí simplemente lo ilustramos con un ejemplo sencillo. 

<code>          
   gmm e = y - X*b
     orthog e ; W
     weights V
     params b
   end gmm
</code>

En el ejemplo de arriba, asumimos que tanto <@lit="y"> como <@lit="X"> son matrices de datos, <@lit="b"> es un vector de valores de los parámetros con la dimensión adecuada, <@lit="W"> es una matriz de instrumentos, y <@lit="V"> es una matriz adecuada de ponderaciones. La expresión 

<code>          
   orthog e ; W
</code>

indica que el vector de errores (<@lit="e">) es ortogonal, en principio, a cada uno de los instrumentos que constituyen las columnas de <@lit="W">. 

<subhead>Nombres de los parámetros</subhead> 

Al estimar un modelo no lineal, frecuentemente resulta conveniente que nombres los parámetros de modo conciso. Al presentar los resultados, sin embargo, puede que desees utilizar etiquetas más informativas. Puedes lograr esto mediante la palabra clave adicional <@lit="param_names"> dentro del bloque de instrucción. Para un modelo con <@mth="k"> parámetros, el argumento que sigue a esta palabra clave debe ser, una cadena de texto literal entre comillas que contenga <@mth="k"> nombres separados por espacios, el nombre de una variable de cadena que contenga <@mth="k"> de esos nombres, o el nombre de un array con <@mth="k"> cadenas de texto. 

Menú gráfico: /Modelo/Variables instrumentales/GMM

# gnuplot Graphs

Argumentos: 	<@var="yvars"> <@var="xvar"> [ <@var="varficticia"> ] 
Opciones: 	<@lit="--with-lines">[=<@var="varspec">] (Utiliza líneas, no puntos)
		<@lit="--with-lp">[=<@var="varspec">] (Utiliza líneas y puntos)
		<@lit="--with-impulses">[=<@var="varspec">] (Utiliza barras finas verticales)
		<@lit="--with-steps">[=<@var="varspec">] (Utiliza segmentos de líneas perpendiculares)
		<@lit="--time-series"> (Representa frente al tiempo)
		<@lit="--single-yaxis"> (Fuerza el uso de un único eje de ordenadas)
		<@lit="--y2axis">=<@var="yvar"> (Coloca la variable especificada en un segundo eje y)
		<@lit="--ylogscale">[=<@var="base">] (Utiliza la escala logarítmica para el eje vertical)
		<@lit="--control"> (Mira abajo)
		<@lit="--dummy"> (Mira abajo)
		<@lit="--fit">=<@var="espajuste"> (Mira abajo)
		<@lit="--font">=<@var="espfuente"> (Mira abajo)
		<@lit="--band">=<@var="espfranja"> (Mira abajo)
		<@lit="--matrix">=<@var="nombrematriz"> (Representa las columnas de la matriz indicada)
		<@lit="--output">=<@var="nombrearchivo"> (Envía el resultado al archivo especificado)
		<@lit="--outbuf">=<@var="nombrecadena"> (Envía el resultado a la cadena de texto especificada)
		<@lit="--input">=<@var="nombrearchivo"> (Coge la entrada de datos desde un archivo especificado)
		<@lit="--inbuf">=<@var="nombrecadena"> (Coge la entrada de datos desde la cadena de texto especificada)
Ejemplos: 	<@lit="gnuplot y1 y2 x">
		<@lit="gnuplot x --time-series --with-lines">
		<@lit="gnuplot wages educ gender --dummy">
		<@lit="gnuplot y x --fit=quadratic">
		<@lit="gnuplot y1 y2 x --with-lines=y2">

Las series de la lista <@var="yvars"> se dibujan frente a <@var="xvar">. Para un gráfico de una serie temporal puedes bien proponer <@lit="time"> en lugar de <@var="xvar">, o bien utilizar el indicador de opción <@opt="--⁠time-series">. Consulta también las instrucciones<@ref="plot"> y <@ref="panplot">. 

Por defecto, las posiciones de los datos se muestran con puntos, pero puedes anular esto indicando una de las siguientes opciones: <@opt="--⁠with-lines"> (líneas y puntos), <@opt="--⁠with-lp">, <@opt="--⁠with-impulses"> o <@opt="--⁠with-steps">. Cuando vas a representar más de una variable en el eje de la <@mth="y">, puedes limitar el efecto de estas opciones a un subconjunto de las variables utilizando el parámetro <@var="varspec">. Este deberá tener el formato de un listado con los nombres o números (en ambos casos separados por comas) de las variables que se van a representar respectivamente. Por ejemplo, en el último ejemplo de arriba se muestra como representar <@lit="y1"> e <@lit="y2"> frente a <@lit="x">, de tal modo que <@lit="y2"> se representa con una línea mientras <@lit="y1"> con puntos. 

Cuando <@var="yvars"> contiene más de una variable, podría ser preferible utilizar dos ejes y (a la izquierda y a la derecha). Por defecto, esto se realiza automáticamente, mediante una técnica heurística basada en las escalas relativas de las variables; pero puedes utilizar dos opciones (mutuamente excluyentes) para anular lo predeterminado. Como cabría esperar, la opción <@opt="--⁠single-yaxis"> evita el uso de un segundo eje, mientras que <@opt="--⁠y2axis="><@var="yvar"> especifica que se represente una cierta variable (únicamente) en relación al segundo eje. 

Cuando selecciones la opción <@opt="--⁠dummy">, debes indicar exactamente tres variables: una variable <@mth="y"> simple, una variable <@mth="x"> y <@var="dvar">, una variable discreta. El efecto de esto consiste en representar <@var="yvar"> frente a <@var="xvar"> con los puntos mostrados con colores diferentes dependiendo del valor de <@var="varficticia"> en la observación indicada. 

La opción <@opt="--⁠control"> es similar en el hecho de que se deben indicar exactamente tres variables: una única variable <@mth="y"> además de las dos variables “explicativas”, <@mth="x"> y <@mth="z">. El efecto de esto es la representación de <@var="y"> respecto a <@var="x">, con el control de <@mth="z">. Esta forma de representación puede ser útil para observar la relación entre <@mth="x"> e <@mth="y">, teniendo en cuenta el efecto que pueda tener <@mth="z"> en ambas. Estadísticamente, esto podría ser equivalente a una regresión de <@mth="y"> sobre las variables <@mth="x"> <@itl="y"> <@mth="z">. 

Puedes especificar que la escala del eje 'y' sea logarítmica en lugar de lineal, utilizando la opción <@opt="--⁠ylogscale">, junto con un parámetro de base. Por ejemplo, 

<code>          
   gnuplot y x --ylogscale=2
</code>

representa los datos de forma que el eje vertical se expresa con potencias de 2. Si omites la base, por defecto, se establece igual a 10. 

<subhead>Cogiendo datos de una matriz</subhead> 

En el caso básico se requieren los argumentos <@var="yvars"> y <@var="xvar"> que se refieren a series del conjunto vigente de datos (indicados por el nombre o por el número ID). Pero si mediante la opción <@opt="--⁠matrix">, indicas una matriz ya definida, estos argumentos se convierten en opcionales: si la matriz especificada tiene <@mth="k"> columnas, por defecto se tratan las primeras <@mth="k"> – 1 columnas como las <@var="yvars"> y la última columna se trata como <@var="xvar">. Sin embargo, cuando indicas la opción <@opt="--⁠time-series">, todas las <@mth="k"> columnas se representan frente al tiempo. Si quieres representar columnas escogidas de la matriz, debes especificar <@var="yvars"> y <@var="xvar"> con el formato de números de columna enteros positivos. Por ejemplo, si quieres un gráfico de dispersión de la columna 2 de la matriz <@lit="M"> frente a la columna 1, puedes hacer: 

<code>          
   gnuplot 2 1 --matrix=M
</code>

<subhead>Mostrar la línea del mejor ajuste</subhead> 

La opción <@opt="--⁠fit"> es solo aplicable en gráficos de dispersión de dos variables y en gráficos de series temporales individuales. Por defecto, el procedimiento en un gráfico de dispersión consiste en mostrar el ajuste MCO si el coeficiente de la pendiente es significativo a un nivel del 10 por ciento, mientras que el proceder para las series temporales es no mostrar ninguna línea de ajuste. Puedes solicitar un comportamiento diferente utilizando esta opción junto con alguno de los siguientes valores de los parámetros <@var="espajuste">. Ten en cuenta que si el gráfico es para una serie temporal individual, el lugar de <@mth="x"> lo ocupa 'time'. 

<indent>
• <@lit="linear">: Muestra el ajuste MCO lineal independientemente del nivel de significación estadística. 
</indent>

<indent>
• <@lit="none">: No muestra ninguna línea de ajuste. 
</indent>

<indent>
• <@lit="inverse"> (inversa), <@lit="quadratic"> (cuadrática), <@lit="cubic"> (cúbica), <@lit="semilog"> o <@lit="linlog">: Muestran una línea de ajuste basada en la regresión del tipo indicado. Con <@lit="semilog"> queremos decir una regresión del logaritmo de <@mth="y"> sobre <@mth="x">; entonces la línea ajustada representa la esperanza condicionada de <@mth="y">, obtenida por medio de la función exponencial. Con <@lit="linlog"> se quiere decir una regresión de <@mth="y"> sobre el logaritmo de <@mth="x">. 
</indent>

<indent>
• <@lit="loess">: Muestra el ajuste de una regresión robusta localmente ponderada (que también se conoce a veces como “lowess”). 
</indent>

<subhead>Representando una franja</subhead> 

Puedes utilizar la opción <@opt="--⁠band"> para representar una “franja” de algún tipo (típicamente representa un intervalo de confianza) junto con otros datos. El modo recomendado de especificar esa franja es mediante un ‘bundle', cuyo nombre se indica como parámetro de esa opción. Un paquete <@lit="band"> requiere dos elementos que son necesarios: una clave <@lit="center">, el nombre de una serie para el centro de la franja; y una clave <@lit="width">, el nombre de una serie que represente el ancho de la franja (ambos indicados como cadenas de texto entre comillas). Además, se admiten otros cuatro elementos opcionales, del modo que se indica a continuación. 

<indent>
• En la clave <@lit="factor">: un escalar que indica el factor por el que se debe multiplicar el ancho (siendo 1 el valor por defecto). 
</indent>

<indent>
• En la clave <@lit="style"> : una cadena de texto para especificar cómo se representa la franja; y que debe ser una de entre <@lit="line"> (línea, predeterminada), <@lit="fill"> (relleno), <@lit="dash"> (raya), <@lit="bars"> (barra) o <@lit="step"> (escalón). 
</indent>

<indent>
• En la clave <@lit="color">: un color para la franja, como una cadena de texto que contenga el nombre de un color de Gnuplot, o como una representación de RGB en hexadecimal (indicada como cadena de texto o como escalar). Por defecto, el color se selecciona automáticamente. 
</indent>

<indent>
• En la clave <@lit="title">: un título para la franja, para que aparezca en la clave o leyenda del gráfico. Por defecto, las franjas no tienen título. 
</indent>

Ten en cuenta que puedes acceder al listado de nombres de color que reconoce Gnuplot, mediante la instrucción del propio Gnuplot “<@lit="show colornames">”; o por medio de la consola de GRETL, utilizando: 

<code>          
   eval readfile("@gretldir/data/gnuplot/gpcolors.txt")
</code>

Aquí tienes dos ejemplos de uso, en los que se emplea la sintaxis abreviada <@lit="_()"> para definir un 'bundle'. El primero únicamente satisface los requerimientos mínimos, mientras que en el segundo se practican las tres opciones. Se está asumiendo que todas as series <@lit="y">, <@lit="x"> y <@lit="w"> están en el conjunto de datos vigente. 

<code>          
   bundle b1 = _(center="x", width="w")
   gnuplot y --time-series --with-lines --band=b1
   bundle b2 = _(center="x", width="w", factor=1.96, style="fill")
   b2.color=0xcccccc
   b2.title = "Intervalo del 95%"
   gnuplot y --time-series --with-lines --band=b2
</code>

Si el gráfico va a contener dos o más de esas bandas, el indicador de opción se debe expresar en plural, y su parámetro debe ser el nombre de un <@itl="array"> de paquetes (bundles), como (continuando el ejemplo anterior) en lo que sigue: 

<code>          
   bundles bb = defarray(b1, b2)
   gnuplot y --time-series --with-lines --bands=bb
</code>

Cuando se representa gráficamente una matriz, en lugar de datos de series, la única diferencia es que los elementos <@lit="centro"> (center) y <@lit="ancho"> (width) del ‘bundle’ de la franja se sustituyen por un único elemento en la clave <@lit="bandmat">; este debe ser el nombre entre comillas de una matriz con dos columnas, con el centro en la columna 1 y el ancho en la columna 2. 

<subhead>Sintaxis heredada de franja</subhead> 

La sintaxis descrita más arriba fue introducida en el GRETL 2023c. Previamente a esa edición, solo se podía especificar una franja por gráfico y la sintaxis era un poco diferente. El enfoque antiguo (que todavía se va a admitir hasta que se comunique lo contrario) divide los detalles de la franja en dos opciones. Primero, la opción <@opt="--⁠band"> que requiere como parámetro los nombres de dos series que indican el centro y el ancho, separadas por una coma. El <@lit="factor"> multiplicativo para el ancho se puede añadir como tercer elemento separado por otra coma. Ejemplos: 

<code>          
   gnuplot ... --band=x,w
   gnuplot ... --band=x,w,1.96
</code>

La segunda opción, <@opt="--⁠band-style">, admite uno o los dos indicadores del estilo y del color de la franja, en ese orden y separados de nuevo por una coma, como en estos ejemplos: 

<code>          
   gnuplot ... --band-style=fill
   gnuplot ... --band-style=dash,0xbbddff
   gnuplot ... --band-style=,black
   gnuplot ... --band-style=bars,blue
</code>

<subhead>Barras de recesión</subhead> 

También puedes utilizar la opción “band” para añadir “barras de recesión” a un gráfico. De este modo nos referimos a barras verticales que ocupan todo el rango de la dimensión <@mth="y"> del gráfico, y que indican la presencia (con barra) o ausencia (sin barra) de alguna característica cualitativa, en un gráfico de series de tiempo. Estas barras habitualmente se utilizan para indicar períodos de recesión; pero también puedes usarlas para señalar períodos de guerra, o cualquier cosa que pueda codificarse con una variable ficticia 0/1. 

En este contexto, la opción <@opt="--⁠band"> requiere un único parámetro: en la clave <@lit="dummy">, un nombre entre comillas de una serie 0/1 (o el nombre entre comillas de un vector columna adecuado, en caso de una matriz de datos). Las barras verticales estarán “activas” para las observaciones en las que esa serie o vector tome el valor 1 e “inactivas” cuando sea 0. Las claves de <@lit="centro"> (center), <@lit="ancho"> (width), <@lit="factor"> y <@lit="estilo"> (style) no son pertinentes, pero se puede usar <@lit="color">. Observa que solo puede usarse una de esas especificaciones por cada gráfico. Aquí tienes un ejemplo: 

<code>          
   open AWM17 --quiet
   series dum = obs >= 1990:1 && obs <= 1994:2
   bundle b = _(dummy="dum", color=0xcccccc)
   gnuplot YER URX --with-lines --time-series \
     --band=b --output=display {set key top left;}
</code>

<subhead>Controlando la salida de resultados</subhead> 

En modo interactivo, el gráfico se muestra inmediatamente pero, en modo de procesamiento por lotes, el procedimiento por defecto consiste en escribir un archivo de instrucciones Gnuplot en el directorio de trabajo del usuario, con un nombre con el patrón <@lit="gpttmpN.plt">, comenzando con N = <@lit="01">. Puedes generar los gráficos reales más tarde utilizando gnuplot (bajo MS Windows, wgnuplot). Y puedes modificar este proceder utilizando la opción <@opt="--⁠output="><@var="nombrearchivo">. Esta opción controla el nombre de archivo utilizado, y al mismo tiempo te permite especificar un formato concreto para el resultado mediante la extensión de tres letras del nombre del archivo, del siguiente modo: <@lit=".eps"> da como resultado la génesis de un archivo Encapsulated PostScript (EPS); <@lit=".pdf"> produce un PDF; <@lit=".png"> genera uno con formato PNG, <@lit=".emf"> solicita que sea EMF (Enhanced MetaFile), <@lit=".fig"> pide que sea un archivo Xfig, <@lit=".svg"> que sea un SVG (Scalable Vector Graphics) y <@lit=".html"> es para un lienzo de HTML. Si indicas el nombre ficticio de archivo “<@lit="display">”, entonces el gráfico se muestra en la pantalla, como en el modo interactivo. Y cuando indicas un nombre de archivo con cualquier extensión diferente a las que acaban de mencionarse, se escribe un archivo de instrucciones Gnuplot. 

Un medio alternativo para dirigir la salida de resultados es con la opción <@opt="--⁠outbuf="><@var="nombrecadena">. Esto escribe las instrucciones de Gnuplot en la cadena de texto indicada o en el “buffer”. Ten en cuenta que las opciones <@opt="--⁠output"> y <@opt="--⁠outbuf"> son mutuamente incompatibles. 

<subhead>Especificando una fuente</subhead> 

Puedes utilizar la opción <@opt="--⁠font"> para especificar una fuente concreta para el gráfico. El parámetro <@var="espfuente"> debe tener la forma del nombre de una fuente, seguida opcionalmente por un número que indique el tamaño en puntos, separado del nombre por una coma o espacio, todo ello puesto entre comillas, como en 

<code>          
   --font="serif,12"
</code>

Ten en cuenta que las fuentes disponibles para Gnuplot varían dependiendo de la plataforma, y si estás escribiendo una instrucción de gráfico que pretendes que sea portable, es mejor restringir el nombre de la fuente a las genéricas <@lit="sans"> o <@lit="serif">. 

<subhead>Añadiendo instrucciones Gnuplot</subhead> 

Dispones de una opción añadida de esta instrucción pues, a continuación de la especificación de las variables que se van a dibujar y del indicador de opción (si hay alguno), puedes añadir instrucciones literales de Gnuplot para controlar la apariencia del gráfico (por ejemplo, estableciendo el título del gráfico y/o rangos de los ejes). Estas instrucciones deben estar puestas entre llaves, y debes terminar cada instrucción Gnuplot con un punto y coma. Puedes utilizar una barra inversa para continuar un conjunto de instrucciones Gnuplot a lo largo de más de una línea. Aquí tienes un ejemplo de la sintaxis: 

<code>          
   { set title 'Mi Título'; set yrange [0:1000]; }
</code>

Menú gráfico: /Ver/Gráficos
Otro acceso: Ventana principal: Menú emergente, botón de gráficos en la barra de herramientas

# graphpg Graphs

Variantes: 	<@lit="graphpg add">
		<@lit="graphpg fontscale "><@var="escala">
		<@lit="graphpg show">
		<@lit="graphpg free">
		<@lit="graphpg --output="><@var="nombrearchivo">

La sesión “Página de gráficos” va a funcionar solo cuando tengas instalado el sistema de composición tipográfica LaTeX, y además puedas generar y ver un resultado PDF o PostScript. 

En la ventana de iconos de la sesión, puedes arrastrar hasta 8 gráficos sobre el icono de página de gráficos. Cuando pulses un doble clic sobre la página de gráficos (o pulses el botón derecho y elijas “Mostrar”), se va a componer una página que contiene los gráficos seleccionados y se va a abrir con un visor adecuado. Desde ahí deberías poder imprimir la página. 

Para vaciar la página de gráficos, pulsa el botón derecho del ratón sobre su icono y selecciona “Vaciar”. 

Ten en cuenta que en sistemas diferentes a MS Windows, podrías tener que ajustar la configuración del programa utilizado para ver archivos PDF o PostScript. Encuéntralo bajo la pestaña “Programas” en la caja de diálogo de las Preferencias generales de GRETL (bajo el menú Herramientas de la ventana principal). 

También es posible trabajar en la página de gráficos mediante un guion, o utilizando la consola (en el programa de Interfaz Gráfica de Usuario, GUI). Se le da apoyo a las siguientes instrucciones y opciones: 

Para añadir un gráfico a la página de gráficos, puedes indicar la instrucción <@lit="graphpg add"> luego de guardar un gráfico definido, como en 

<code>          
   grf1 <- gnuplot Y X
   graphpg add
</code>

Para mostrar la página de gráficos: <@lit="graphpg show">. 

Para vaciar la página de gráficos: <@lit="graphpg free">. 

Para ajustar la escala de la fuente utilizada en la página de gráficos, usa <@lit="graphpg fontscale"> <@var="escala">, donde <@var="escala"> es un múltiplo (por defecto igual a 1.0). De este modo, para hacer que el tamaño de la fuente sea un 50 por ciento mayor que el tamaño por defecto, puedes hacer 

<code>          
   graphpg fontscale 1.5
</code>

Para solicitar la impresión de la página del gráfico en un archivo, usa la opción <@opt="--⁠output="> más un nombre de archivo; este nombre debería tener la extensión “<@lit=".pdf">”, “<@lit=".ps">” o “<@lit=".eps">”. Por ejemplo: 

<code>          
   graphpg --output="myfile.pdf"
</code>

El archivo resultante va a escribirse en el directorio establecido en ese momento (<@ref="workdir">), excepto que la cadena <@var="nombrearchivo"> contenga una especificación completa de la ruta. 

En este contexto, para el resultado se utilizan líneas de colores por defecto; para utilizar patrones punto/raya en vez de colores, puedes añadir la opción <@opt="--⁠monochrome">. 

# gridplot Graphs

Argumento: 	<@var="plotspecs"> 
Opciones: 	<@lit="--fontsize">=<@var="fs"> (Tamaño de la fuente en puntos [10])
		<@lit="--width">=<@var="w"> (Ancho del gráfico en pixels [800])
		<@lit="--height">=<@var="h"> (Altura del gráfico en pixels [600])
		<@lit="--title">=<@var="cadena entre comillas"> (Añadir un título general)
		<@lit="--rows">=<@var="r"> (Mira abajo)
		<@lit="--cols">=<@var="c"> (Mira abajo)
		<@lit="--layout">=<@var="lmat"> (Mira abajo)
		<@lit="--output">=<@var="destino"> (Mira abajo)
		<@lit="--outbuf">=<@var="destino alternativo"> (Mira abajo)
Ejemplos: 	<@lit="gridplot myspecs --rows=3 --output=display">
		<@lit="gridplot myspecs --layout=lmat --output=compuesto.pdf">

Esta instrucción coge dos o más especificaciones individuales para gráficos y las ordena en una parrilla para generar un gráfico combinado. El único argumento requerido, <@var="plotspecs">, tiene el formato de un 'array' de cadenas de texto, cada una especificando un gráfico. Su instrucción asociada, <@ref="gpbuild">, ofrece un modo fácil de crear ese tipo de 'array'. 

<subhead>Especificando la parrilla</subhead> 

La forma de la parrilla se puede establecer por cualquiera de las tres opciones (mutuamente incompatibles) <@opt="--⁠rows">, <@opt="--⁠cols"> y <@opt="--⁠layout">. Si no se indica ninguna de esas opciones, el número de filas se establece como la raíz cuadrada del número de gráficos (el tamaño del 'array' de entrada), redondeado hacia el entero superior más próximo, de ser necesario. Entonces, el número de columnas se establece como el número de gráficos dividido por el número de filas, de nuevo redondeado hacia arriba, de ser necesario. Los gráficos se colocan en la parrilla por filas, en el mismo orden que en el 'array'. Si se indica la opción <@opt="--⁠rows">, ese valor ocupa la posición de selección automática, pero el número de columnas se establece automáticamente como se describió antes. En cambio, si se indica la opción <@opt="--⁠cols">, el número de filas se establece de forma automática. 

La opción <@opt="--⁠layout">, que necesita una matriz como parámetro, ofrece una alternativa más flexible. Esa matriz especifica la configuración de la parrilla de este modo: los elementos 0 piden celdas vacías en la parrilla, y los elementos enteros de 1 a <@mth="n"> se refieren a los subgráficos en el orden que tengan en el 'array'. Así por ejemplo, 

<code>          
   matrix m = {1,0,0; 2,3,0; 4,5,6}
   gridplot ... --layout=m ...
</code>

se refiere a una configuración triangular inferior de seis gráficos en una parrilla 3×3. Utilizando esta opción se pueden omitir algunos subgráficos, o incluso repetir alguno. 

<subhead>Opciones de salida</subhead> 

La opción <@opt="--⁠output"> puede utilizarse para indicar <@lit="display"> (presentar el gráfico inmediatamente) o el nombre de un archivo de salida. Como alternativa, se puede usar la opción <@opt="--⁠outbuf"> para hacer una salida directa (con el formato de un búfer de instrucciones de GNUPLOT) a la cadena de texto mencionada. En ausencia de estas opciones, la salida es un archivo de instrucciones de GNUPLOT nombrado de forma automática. Consulta <@ref="gnuplot"> para obtener más detalles. 

# gpbuild Graphs

Argumento: 	<@var="plotspecs"> 
Ejemplo: 	<@lit="gpbuild MyPlots">

Esta instrucción empieza un bloque en el que cualquier instrucción o llamada a una función que produce gráficos se trata de modo especial, con objeto de producir un 'array' de cadenas de texto de especificación de gráficos, para usarlas con la instrucción <@ref="gridplot">: el argumento <@var="plotspecs"> proporciona el nombre para ese 'array'. El bloque se acaba con la instrucción “<@lit="end gpbuild">”. 

<subhead>Dos restricciones</subhead> 

Dentro de un bloque <@lit="gpbuild"> solo tienen un tratamiento especial las instrucciones para representación gráfica; todas las otras instrucciones se ejecutan normalmente. Únicamente hay dos restricciones que advertir. 

<indent>
• Las instrucciones de representación <@itl="no"> deben incluir una especificación de salida en este contexto, puesto que eso estaría en conflicto con la redirección automática de la salida, hacia el 'array' de <@var="plotspecs">. Una excepción a esta regla se permite para <@lit="--output=display"> (que es bastante habitual como opción predeterminada en los paquetes de funciones relacionadas con representaciones gráficas); esta directiva se ignora de modo silencioso en favor del tratamiento automático. 
</indent>

<indent>
• Los gráficos que invoquen la directiva “<@lit="multiplot">” de GNUPLOT no son adecuados para ser incluidos en un bloque <@lit="gpbuild">. Esto es porque <@lit="gridplot"> utiliza internamente <@lit="multiplot">, y esas construcciones no se pueden anidar. 
</indent>

<subhead>Alternativa manual</subhead> 

Se puede preparar un 'array' de especificaciones de gráficos para utilizar con <@lit="gridplot"> sin usar un bloque <@lit="gpbuild">, como en el siguiente ejemplo: 

<code>          
   open data4-10
   strings MyPlots = array(3)
   gnuplot ENROLL CATHOL --outbuf=MyPlots[1]
   gnuplot ENROLL INCOME --outbuf=MyPlots[2]
   gnuplot ENROLL COLLEGE --outbuf=MyPlots[3]
</code>

En esencia, lo anterior es equivalente a 

<code>          
   open data4-10
   gpbuild MyPlots
      gnuplot ENROLL CATHOL
      gnuplot ENROLL INCOME
      gnuplot ENROLL COLLEGE
   end gpbuild
</code>

# heckit Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> ; <@var="ecuaciondeseleccion"> 
Opciones: 	<@lit="--quiet"> (No presenta los resultados)
		<@lit="--two-step"> (Realiza la estimación en 2 etapas)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--opg"> (Desviaciones típicas PEG (OPG))
		<@lit="--robust"> (Desviaciones típicas CMV (QML))
		<@lit="--cluster">=<@var="clustvar"> (Consulta <@ref="logit"> para más explicaciones)
		<@lit="--verbose"> (Presenta resultados adicionales)
Ejemplos: 	<@lit="heckit y 0 x1 x2 ; ys 0 x3 x4">
		Ver también <@inp="heckit.inp">

Modelo de selección de tipo Heckman. Al especificar esta instrucción, la lista antes del punto y coma representa las variables de la ecuación resultante, y la segunda lista representa las variables de la ecuación de selección. La variable dependiente de la ecuación de selección (<@lit="ys"> en el ejemplo de arriba) debe ser una variable binaria. 

Por defecto, los parámetros se estiman por el método de máxima verosimilitud. La matriz de covarianzas de los estimadores de los parámetros se calcula utilizando la inversa negativa de la matriz Hessiana. Si quieres hacer la estimación en 2 etapas, utiliza la opción <@opt="--⁠two-step">. En este caso, la matriz de covarianzas de los estimadores de los parámetros de la ecuación resultante se ajusta de modo adecuado según <@bib="Heckman (1979);heckman79">. 

Menú gráfico: /Modelo/Variable dependiente limitada/Heckit

# help Utilities

Variantes: 	<@lit="help">
		<@lit="help functions">
		<@lit="help"> <@var="instrucción">
		<@lit="help"> <@var="función">
Opción: 	<@lit="--func"> (Escoge la ayuda sobre las funciones)

Si no indicas ningún argumento, presenta la lista de instrucciones disponibles. Si indicas el argumento simple <@lit="functions">, presenta la lista de funciones disponibles (consulta <@ref="genr">). 

La expresión <@lit="help"> <@var="instrucción"> describe cada instrucción indicada (e.g. <@lit="help smpl">). La expresión <@lit="help"> <@var="función"> describe cada función indicada (e.g. <@lit="help ldet">). Algunas funciones tienen los mismos nombres que las instrucciones relacionadas (e.g. <@lit="diff">); en ese caso, por defecto se presenta la ayuda para la instrucción, pero puedes obtener ayuda para la función utilizando la opción <@opt="--⁠func">. 

Menú gráfico: /Ayuda

# hfplot Graphs

Argumentos: 	<@var="listaaltafrec"> [ ; <@var="listabajafrec"> ] 
Opciones: 	<@lit="--with-lines"> (Gráfico con líneas)
		<@lit="--time-series"> (Pon el tiempo en el eje de abscisas)
		<@lit="--output">=<@var="nombrearchivo"> (Envía el resultado al archivo especificado)

Proporciona un medio de dibujar una serie de alta frecuencia, posiblemente junto a una o más series observadas con la frecuencia base del conjunto de datos. El primer argumento debe ser una <@ref="MIDAS_list">; y los términos adicionales <@var="listabajafrec"> (opcionales) deberán ser series habituales (“de baja frecuencia”), después de un punto y coma. 

Para obtener más detalles sobre el efecto de la opción <@opt="--⁠output">, consulta la instrucción <@ref="gnuplot">. 

# hsk Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--no-squares"> (Mira abajo)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--quiet"> (No presenta nada)

Esta instrucción es aplicable cuando existe heterocedasticidad en forma de una función desconocida de los regresores, que puede aproximarse por medio de una relación cuadrática. En ese contexto, ofrece la posibilidad de obtener desviaciones típicas consistentes y estimaciones más eficientes de los parámetros, en comparación con MCO. 

El procedimiento implica (a) la estimación MCO del modelo de interés, seguido de (b) una regresión auxiliar para generar una estimación de la varianza de la perturbación, y finalmente (c) mínimos cuadrados ponderados, utilizando como ponderación la inversa de la varianza estimada. 

En la regresión auxiliar de (b), se regresa el logaritmo de los errores cuadrados de la primera estimación MCO, sobre los regresores originales y sus cuadrados (por defecto), o solo sobre los regresores originales (si indicas la opción <@opt="--⁠no-squares">). La transformación logarítmica se realiza para asegurar que las varianzas estimadas son todas no negativas. Denominando <@mth="u"><@sup="*"> a los valores ajustados por esta regresión, la serie con las ponderaciones para la estimación MCP (WLS) final se forma entonces como 1/exp(<@mth="u"><@sup="*">). 

Menú gráfico: /Modelo/Otros modelos lineales/con corrección de Heterocedasticidad

# hurst Statistics

Argumento: 	<@var="serie"> 
Opción: 	<@lit="--plot">=<@var="modo-o-nombrearchivo"> (Mira abajo)

Calcula el exponente de Hurst (una medida de persistencia o memoria larga) para una variable de tipo serie temporal que tenga por lo menos 128 observaciones. Puedes obtener el resultado (junto con su desviación típica) mediante el accesor <@xrf="$result">. 

<@bib="Mandelbrot (1983);mandelbrot83"> discute sobre el exponente de Hurst. En términos teóricos, este es el exponente (<@mth="H">) de la relación 

  <@fig="hurst">

donde RS expresa el “rango que se vuelve a escalar” de la variable <@mth="x"> en muestras de tamaño <@mth="n"> y <@mth="a"> es una constante. El rango reescalado es el rango (valor máximo menos mínimo) del valor acumulado o suma parcial de <@mth="x"> (luego de la substracción de su media muestral) en el período de la muestra, dividida por la desviación típica muestral. 

Como punto de referencia, si <@mth="x"> es una variable ruido blanco (con media y persistencia nulas) entonces el rango de su “paseo” (forma un paseo aleatorio) acumulado y escalado por su desviación típica, tiene un crecimiento igual a la raíz cuadrada del tamaño de la muestra, proporcionando un exponente de Hurst esperado de 0.5. Los valores del exponente que estén significativamente por encima de 0.5 indican persistencia, y los menores que 0.5 indican “antipersistencia” (autocorrelación negativa). En principio, el exponente está acotado entre 0 y 1, aunque en muestras finitas es posible obtener un exponente estimado mayor que 1. 

En GRETL, el exponente se estima utilizando submuestreo binario: se empieza con el rango completo de datos, después con las dos mitades del rango, después con los 4 cuartos, etcétera. Para tamaños de la muestra menores que el rango de datos, el valor RS es la media entre las muestras disponibles. El exponente se estima así como el coeficiente de la pendiente, en una regresión del logaritmo de RS sobre el logaritmo del tamaño de la muestra. 

Por defecto, si GRETL no está en modo de procesamiento por lotes, se muestra un gráfico del rango reescalado pero puedes ajustar esto mediante la opción <@opt="--⁠plot">. Los parámetros que se admiten para esta opción son <@lit="none"> (para suprimir el gráfico); <@lit="display"> (para presentar un gráfico incluso en caso de procesar por lotes); o un nombre de archivo. El efecto de indicar un nombre de archivo es como el descrito para la opción <@opt="--⁠output"> de la instrucción <@ref="gnuplot">. 

Menú gráfico: /Variable/Exponente de Hurst

# if Programming

Control de flujo para la ejecución de instrucciones. Se admiten 3 tipos de construcción, como las indicadas a continuación. 

<code>          
   # Forma simple
   if (poner la condición)
       instrucciones
   endif

   # Dos ramas
   if (poner la condición)
       instrucciones 1
   else
       instrucciones 2
   endif

   # Tres o más ramas
   if (poner la condición 1)
       instrucciones 1
   elif (poner la condición 2)
       instrucciones 2
   else
       instrucciones 3
   endif
</code>

La condición (<@var="condition">) debe ser una expresión booleana; para su sintaxis consulta <@ref="genr">. Puedes incluir más de un bloque <@lit="elif">. Además, puedes anidar los bloques <@lit="if"> … <@lit="endif">. 

# include Programming

Argumento: 	<@var="nombrearchivo"> 
Opción: 	<@lit="--force"> (Fuerza a volver a leer desde el archivo)
Ejemplos: 	<@lit="include myfile.inp">
		<@lit="include sols.gfn">

Ideado para utilizar en un guion de instrucciones, principalmente para incluir definiciones de funciones. El nombre del archivo (<@var="nombrearchivo">) debería tener la extensión <@lit="inp"> (un guion de texto plano) o <@lit="gfn"> (un paquete de funciones de GRETL). Las instrucciones de <@var="nombrearchivo"> se ejecutan y luego el control se devuelve al guion principal. 

La opción <@opt="--⁠force"> es específica de los archivos <@lit="gfn"> y su efecto consiste en forzar a GRETL a que vuelva a leer el paquete de funciones desde el archivo, incluso aunque ya esté cargado en la memoria. (Los archivos de texto plano <@lit="inp"> siempre se leen y se procesan en respuesta a esta instrucción.) 

Consulta también <@ref="run">. 

# info Dataset

Variantes: 	<@lit="info">
		<@lit="info --to-file="><@var="nombrearchivo">
		<@lit="info --from-file="><@var="nombrearchivo">

En su forma básica, presenta cualquier información adicional (metadatos) guardada con el archivo de datos vigente. Alternativamente, escribe esta información en un archivo (mediante la opción <@opt="--⁠to-file">), o lee los metadatos de un archivo especificado y los incorpora al conjunto de datos vigente (mediante <@opt="--⁠from-file">; en cuyo caso el texto debe tener un formato UTF-8 correcto). 

Menú gráfico: /Datos/Información del conjunto de datos

# intreg Estimation

Argumentos: 	<@var="minvar"> <@var="maxvar"> <@var="indepvars"> 
Opciones: 	<@lit="--quiet"> (No presenta los resultados)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--opg"> (Mira más abajo)
		<@lit="--cluster">=<@var="clustvar"> (Consulta <@ref="logit"> para más explicaciones)
Ejemplos: 	<@lit="intreg lo hi const x1 x2">
		Ver también <@inp="wtp.inp">

Estima un modelo de regresión por intervalos. Este modelo surge cuando la variable dependiente está imperfectamente observada para algunas observaciones (posiblemente todas). En otras palabras, se asume que el proceso generador de datos es 

  <@mth="y* = x b + u">

pero solo observamos <@mth="m <= y* <= M"> (el intervalo puede no tener límite por la izquierda o por la derecha). Ten en cuenta que para algunas observaciones <@mth="m"> puede ser igual a <@mth="M">. Las variables <@var="minvar"> y <@var="maxvar"> deben contener <@lit="NA">s para las observaciones sin límite por la izquierda o por la derecha, respectivamente. 

El modelo se estima mediante Máxima Verosimilitud, asumiendo la distribución Normal del término de perturbación aleatoria. 

Por defecto, las desviaciones típicas se calculan utilizando la inversa negativa de la matriz Hessiana. Cuando especificas la opción <@opt="--⁠robust">, entonces se calculan en su lugar las desviaciones típicas CMV (QML) o de Huber–White. En este caso, la matriz de covarianzas estimada es un “emparedado” entre la inversa de la matriz Hessiana estimada y el producto externo del vector gradiente. Como alternativa puedes indicar la opción<@opt="--⁠opg">, en cuyo caso las desviaciones típicas se basan únicamente en el producto externo del vector gradiente. 

Menú gráfico: /Modelo/Variable dependiente limitada/Regresión de intervalos

# johansen Tests

Argumentos: 	<@var="orden"> <@var="ylista"> [ ; <@var="xlista"> ] [ ; <@var="rxlista"> ] 
Opciones: 	<@lit="--nc"> (Sin constante)
		<@lit="--rc"> (Constante restringida)
		<@lit="--uc"> (Constante no restringida)
		<@lit="--crt"> (Constante y tendencia restringida)
		<@lit="--ct"> (Constante y tendencia no restringida)
		<@lit="--seasonals"> (Incluye variables ficticias estacionales centradas)
		<@lit="--asy"> (Guarda los valores p asintóticos)
		<@lit="--quiet"> (Presenta solo los contrastes)
		<@lit="--silent"> (No presenta nada)
		<@lit="--verbose"> (Presenta detalles de las regresiones auxiliares)
Ejemplos: 	<@lit="johansen 2 y x">
		<@lit="johansen 4 y x1 x2 --verbose">
		<@lit="johansen 3 y x1 x2 --rc">
		Ver también <@inp="hamilton.inp">, <@inp="denmark.inp">

Lleva a cabo el contraste de cointegración de Johansen entre las variables de <@var="ylista"> para el orden de retardos seleccionado. Para obtener más detalles sobre este contraste, consulta <@pdf="El manual de gretl#chap:vecm"> (Capítulo 33) o el capítulo 20 de <@bib="Hamilton (1994);hamilton94">. Las probabilidades asociadas (valores p) se calculan mediante la aproximación Gamma de Doornik <@bib="(Doornik, 1998);doornik98">. Se muestran dos conjuntos de valores p para el contraste de la traza: valores asintóticos directos y valores ajustados por el tamaño de la muestra. Por defecto, el accesor <@xrf="$pvalue"> genera la variante ajustada, pero puedes utilizar la opción <@opt="--⁠asy"> para obtener en su lugar los valores asintóticos. 

La inclusión de términos determinísticos en el modelo se controla mediante los indicadores de opción. Por defecto, si no especificas ninguna opción, se incluye una “constante no restringida”, que permite la presencia de una ordenada en el origen no nula en las relaciones de cointegración, así como una tendencia en los niveles de las variables endógenas. En la literatura generada a partir del trabajo de Johansen (por ejemplo, consulta su libro de 1995) se refiere esta situación como el “caso 3”. Las 4 primeras opciones indicadas arriba, que son mutuamente excluyentes, producen respectivamente los casos 1, 2, 4 y 5. Tanto el significado de estos casos como el criterio para seleccionar un caso se explican en <@pdf="El manual de gretl#chap:vecm"> (Capítulo 33). 

Las listas <@var="xlista"> y <@var="rxlista"> (opcionales) te permiten controlar las variables exógenas especificadas, y así estas entran en el sistema bien sin restricciones (<@var="xlista">) o bien restringidas al espacio de cointegración (<@var="rxlista">). Estas listas se separan de <@var="ylista"> y unas de las otras mediante un punto y coma. 

La opción <@opt="--⁠seasonals">, que puedes combinar con cualquiera de las otras opciones, especifica la inclusión de un conjunto de variables ficticias estacionales centradas. Esta opción está disponible solo para datos trimestrales o mensuales. 

La siguiente tabla se ofrece como guía para la interpretación de los resultados del contraste que se muestran, para el caso con 3 variables. <@lit="H0"> denota la hipótesis nula, <@lit="H1"> la hipótesis alternativa, y <@lit="c"> el número de relaciones de cointegración. 

<mono>          
         Rango   Contraste traza      Contraste Lmáx
                  H0     H1          H0     H1
         ---------------------------------------
          0      c = 0  c = 3       c = 0  c = 1
          1      c = 1  c = 3       c = 1  c = 2
          2      c = 2  c = 3       c = 2  c = 3
         ---------------------------------------
</mono>

Consulta también la instrucción <@ref="vecm">; y la instrucción <@ref="coint"> si quieres obtener el contraste de cointegración de Engle–Granger. 

Menú gráfico: /Modelo/Series temporales multivariantes

# join Dataset

Argumentos: 	<@var="nombrearchivo"> <@var="nombrevar"> 
Opciones: 	<@lit="--data">=<@var="nombrecolumna"> (Mira abajo)
		<@lit="--filter">=<@var="expresión"> (Mira abajo)
		<@lit="--ikey">=<@var="claveinterna"> (Mira abajo)
		<@lit="--okey">=<@var="claveexterna"> (Mira abajo)
		<@lit="--aggr">=<@var="método"> (Mira abajo)
		<@lit="--tkey">=<@var="nombrecoluma,cadenaformato"> (Mira abajo)
		<@lit="--verbose"> (Informe en marcha)

Esta instrucción incorpora una o más series desde el origen <@var="nombrearchivo"> (que debe ser bien un archivo de datos con el texto delimitado, o bien un archivo de datos “propio” de GRETL), con el nombre <@var="nombrevar">. Para obtener más detalles, consulta <@pdf="El manual de gretl#chap:join"> (Capítulo 7) pues aquí damos solo un breve resumen de las opciones disponibles. Consulta también <@ref="append"> para operaciones de anexión más simples. 

Puedes utilizar la opción <@opt="--⁠data"> para especificar el encabezamiento de los datos del archivo de origen, si difiere del nombre por el que los datos debieran de conocerse en GRETL. 

Puedes usar la opción <@opt="--⁠filter"> para especificar un criterio para filtrar los datos de origen (es decir, para escoger un subconjunto de las observaciones). 

Puedes utilizar las opciones <@opt="--⁠ikey"> y <@opt="--⁠okey"> para especificar una equivalencia entre las observaciones del conjunto vigente de datos y las observaciones de la fuente de datos (por ejemplo, los individuos pueden hacerse corresponder con el hogar al que pertenecen). 

La opción <@opt="--⁠aggr"> se utiliza cuando la equivalencia entre las observaciones del conjunto vigente de datos y las del origen no es de una a una. 

La opción <@opt="--⁠tkey"> se aplica solo cuando el conjunto vigente de datos tiene una estructura de serie temporal. Puedes usarla para especificar, bien el nombre de una columna que contenga fechas que van a ser emparejadas con el conjunto de datos, y/o bien el formato en el que las fechas se representan en esa columna. 

<subhead>Incorporación de más de una serie al mismo tiempo</subhead> 

Con la instrucción <@lit="join"> puedes manejar la incorporación de varias series al mismo tiempo. Esto sucede si el argumento <@var="nombrevar">: (a) consiste en una lista de nombres separados por espacios, en lugar de un único nombre; o (b) apunta a un 'array' de cadenas de texto, cuyos elementos deben ser los nombres de las series que se pretende incorporar. 

Sin embargo, este método tiene alguna limitación como el hecho de que la opción <@opt="--⁠data"> en este caso no está disponible. Y cuando incorporas múltiples series, estás obligado a aceptar los nombres “externos” que ya tienen. Las demás opciones se aplican de modo uniforme a todas las series que se incorporan mediante una instrucción concreta. 

# kdplot Graphs

Argumento: 	<@var="y"> 
Opciones: 	<@lit="--alt"> (Utiliza el kernel de Epanechnikov)
		<@lit="--scale">=<@var="s"> (Factor de ajuste del ancho de banda)
		<@lit="--output">=<@var="nombrearchivo"> (Envía el gráfico al archivo indicado)

Representa un gráfico con la estimación de la densidad del kernel para la serie <@var="y">. Por defecto, el kernel es Gaussiano pero si indicas la opción <@opt="--⁠alt">, se utiliza el kernel de Epanechnikov. Puedes ajustar el grado de suavizado mediante la opción <@opt="--⁠scale">, que tiene un valor predeterminado de 1.0 (los valores más grandes de <@var="s"> producen un resultado más suavizado). 

La opción <@opt="--⁠output"> tiene como efecto el envío del resultado al archivo que se indique en ella; utiliza la palabra “display” para forzar que el resultado aparezca en la pantalla. Consulta la instrucción <@ref="gnuplot"> para obtener más detalles sobre esta opción. 

Para obtener medios más flexibles para generar estimaciones de la densidad del kernel, con la posibilidad de recuperar el resultado en forma de matriz, consulta la función <@xrf="kdensity">. 

Menú gráfico: /Variable/Gráfico de la densidad estimada

# kpss Tests

Argumentos: 	<@var="orden"> <@var="listavariables"> 
Opciones: 	<@lit="--trend"> (Incluye una tendencia)
		<@lit="--seasonals"> (Incluye variables ficticias estacionales)
		<@lit="--verbose"> (Presenta los resultados de la regresión)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--difference"> (Utiliza la primera diferencia de la variable)
Ejemplos: 	<@lit="kpss 8 y">
		<@lit="kpss 4 x1 --trend">

Para utilizar esta instrucción con datos de panel, consulta la sección final de estas anotaciones. 

Calcula el contraste de estacionariedad KPSS <@bib="(Kwiatkowski et al, Journal of Econometrics, 1992);KPSS92"> para cada una de las variables indicadas (o para sus primeras diferencias, si escoges la opción <@opt="--⁠difference">). La hipótesis nula es que la variable en cuestión es estacionaria, bien alrededor de un nivel o, si marcas la opción <@opt="--⁠trend">, alrededor de una tendencia lineal determinística. 

El argumento <@var="orden"> determina el tamaño de la ventana utilizada para el suavizado de Bartlett. Cuando indicas un valor negativo, eso se toma como señal para que se utilice una ventana automática de tamaño 4(<@mth="T">/100)<@sup="0.25">, donde <@mth="T"> es el tamaño de la muestra. 

Si escoges la opción <@opt="--⁠verbose">, se presentan los resultados de la regresión auxiliar junto con la varianza estimada de la componente de paseo aleatorio de la variable. 

Los puntos críticos mostrados para el estadístico de contraste se basan en superficies de respuesta estimadas del modo establecido por <@bib="Sephton (Economics Letters, 1995);sephton95">, que son más fiables para muestras pequeñas que los valores indicados en el artículo original de KPSS. Cuando el estadístico de contraste cae entre los puntos críticos del 1 y del 10 por ciento, se muestra una probabilidad asociada (valor p) que se obtiene mediante interpolación lineal y no debe tomarse demasiado literalmente. Consulta la función <@xrf="kpsscrit"> para ver un medio de obtener esos puntos críticos con la ayuda del programa. 

<subhead>Datos de panel</subhead> 

Cuando se utiliza la instrucción <@lit="kpss"> con datos de panel, para realizar un contraste de raíz unitaria de panel, las opciones aplicables y los resultados mostrados son algo diferentes. Mientras que en el caso habitual de series temporales, puedes indicar una lista de variables para contrastar, con datos de panel solo puedes contrastar una variable por cada instrucción. Y la opción <@opt="--⁠verbose"> tiene un significado diferente, pues genera una breve presentación del contraste para cada serie temporal individual (ya que, por defecto, solo se muestra el resultado global). 

Cuando es posible, se calcula el contraste global (Hipótesis nula: La serie en cuestión es estacionaria para todas las unidades del panel) utilizando para ello el método de <@bib="Choi (Journal of International Money and Finance, 2001);choi01">. Esto no siempre es sencillo pues la dificultad está en que, mientras que el contraste de Choi se basa en las probabilidades asociadas de los contrastes con las series individuales, no tenemos actualmente un modo de calcular las probabilidades asociadas para el estadístico de contraste KPSS; debemos apoyarnos en unos pocos puntos críticos. 

Si el estadístico de contraste con una determinada serie, cae entre los puntos críticos del 1 y del 10 por ciento, podemos interpolar una probabilidad asociada. Pero si el valor del estadístico del contraste cae por debajo del correspondiente al 10 por ciento o si excede al del 1 por ciento, no se puede interpolar y como mucho se puede establecer un límite sobre el contraste de Choi global. Si el valor del estadístico de contraste individual cae por debajo del correspondiente al 10 por ciento para unas unidades y excede al del 1 por ciento para otras, ni siquiera se puede calcular un límite para el contraste global. 

Menú gráfico: /Variable/Contrastes de raíz unitaria/Contraste KPSS

# labels Dataset

Variantes: 	<@lit="labels ["> <@var="listavariables"> <@lit="]">
		<@lit="labels --to-file="><@var="nombrearchivo">
		<@lit="labels --from-file="><@var="nombrearchivo">
		<@lit="labels --delete">
Ejemplos: 	<@inp="oprobit.inp">

Con la primera forma, se presentan las etiquetas informativas (si existen) de las series de <@var="listavariables">, o de todas las series del conjunto de datos cuando no especificas <@var="listavariables">. 

Con la opción <@opt="--⁠to-file"> se escriben en el archivo indicado, las etiquetas de todas las series del conjunto de datos, una etiqueta por cada línea. Si no hay ninguna etiqueta, se muestra un fallo; y si algunas series tienen etiqueta y otras no, se presenta una línea en blanco para las series sin etiqueta. El archivo resultante se va a escribir en el directorio <@ref="workdir"> vigente en ese momento, excepto que la cadena <@var="nombrearchivo"> contenga una especificación completa de la ruta. 

Con la opción <@opt="--⁠from-file">, se lee el archivo especificado (que debe ser de texto plano) y se asignan etiquetas a las series del conjunto de datos, leyéndose una etiqueta por línea y usando líneas en blanco para indicar etiquetas en blanco. 

La opción <@opt="--⁠delete"> hace lo que cabría esperar pues elimina todas las etiquetas de las series del conjunto de datos. 

Menú gráfico: /Datos/Etiquetas de variables

# lad Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--no-vcv"> (No calcula la matriz de covarianzas)
		<@lit="--quiet"> (No presenta nada)

Calcula una regresión que minimiza la suma de las desviaciones absolutas de los valores ajustados respecto a los valores observados de la variable dependiente. Las estimaciones de los coeficientes se derivan utilizando el algoritmo del simplex de Barrodale–Roberts; y se presenta una advertencia si la solución no es única. 

Las desviaciones típicas se deducen utilizando el procedimiento 'bootstrap' con 500 extracciones. La matriz de covarianzas de los estimadores de los parámetros, que se presenta cuando indicas <@opt="--⁠vcv">, se basa en el mismo 'bootstrap'. Puesto que esta es una operación bastante costosa, la opción <@opt="--⁠no-vcv"> se proporciona para aquellos casos en los que no se necesita la matriz de covarianzas; cuando indicas esta opción, las desviaciones típicas no van a estar disponibles. 

Ten en cuenta que este método puede resultar lento cuando la muestra es muy larga o cuando hay muchos regresores. Por eso, en esos casos, puede ser mejor utilizar la instrucción <@ref="quantreg">. Dadas una variable dependiente <@lit="y"> junto con una lista <@lit="X"> de regresores, las siguientes instrucciones son básicamente equivalentes, con la excepción de que el método "quantreg" utiliza el algoritmo más rápido de Frisch–Newton, y que proporciona las desviaciones típicas analíticas en lugar de las de "bootstrapping". 

<code>          
   lad y const X
   quantreg 0.5 y const X
</code>

Menú gráfico: /Modelo/Estimación robusta/Mínima desviación absoluta

# lags Transformations

Argumentos: 	[ <@var="orden"> ; ] <@var="listaretardos"> 
Opción: 	<@lit="--bylag"> (Ordena los términos por retardo)
Ejemplos: 	<@lit="lags x y">
		<@lit="lags 12 ; x y">
		<@lit="lags 4 ; x1 x2 x3 --bylag">
		Ver también <@inp="sw_ch12.inp">, <@inp="sw_ch14.inp">

Genera nuevas series que contienen los valores retardados de cada una de las series de <@var="listavariables">. Por defecto, el número de retardos que se crean es igual a la periodicidad de los datos. Por ejemplo, si la periodicidad es 4 (trimestral), la instrucción <@lit="lags x"> genera 

<mono>          
   x_1 = x(t-1)
   x_2 = x(t-2)
   x_3 = x(t-3)
   x_4 = x(t-4)
</mono>

Puedes controlar el número de retardos generados mediante el primer parámetro opcional (que, si existe, debe estar seguido de un punto y coma). 

La opción <@opt="--⁠bylag"> tiene sentido solo cuando <@var="listavariables"> contiene más de una serie y el orden máximo de retardos es mayor que 1. Por defecto, se añaden los términos retardados al conjunto de datos, por variable: primero todos los retardos de la primera serie de la lista, después todos los retardos de la segunda serie, etcétera. Pero cuando indicas <@opt="--⁠bylag">, la ordenación se hace por retardos: primero el retardo 1 de todas las series de la lista, después el retardo 2 de todas as series de la lista, etcétera. 

Esta prestación también está disponible como función: consulta <@xrf="lags">. 

Menú gráfico: /Añadir/Retardos de las variables seleccionadas

# ldiff Transformations

Argumento: 	<@var="listavariables"> 

Se obtiene la primera diferencia del logaritmo natural de cada una de las series de <@var="listavariables">, y el resultado se guarda en una nueva serie con el prefijo <@lit="ld_">. Así <@lit="ldiff x y"> genera las nuevas variables 

<mono>          
   ld_x = log(x) - log(x(-1))
   ld_y = log(y) - log(y(-1))
</mono>

Menú gráfico: /Añadir/Diferencias de logaritmos de las variables seleccionadas

# leverage Tests

Opciones: 	<@lit="--save"> (Guarda las series resultantes)
		<@lit="--overwrite"> (Conformidad para sobrescribir series ya existentes)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--plot">=<@var="Modo-o-nombrearchivo"> (Mira abajo)
Ejemplos: 	<@inp="leverage.inp">

Debe ir después de una instrucción de MCO (<@lit="ols">). Calcula el apalancamiento (<@mth="h">, que debe caer en el rango entre 0 y 1) para cada punto de datos de la muestra sobre la que se estimó el modelo previo. Muestra el error (<@mth="u">) para cada observación junto con su apalancamiento y una medida de su influencia en las estimaciones, <@mth="uh">/(1 – <@mth="h">). Los “puntos de Leverage” para los que el valor de <@mth="h"> supera 2<@mth="k">/<@mth="n"> (donde <@mth="k"> es el número de parámetros que se estiman y <@mth="n"> es el tamaño de la muestra) se destacan mediante un asterisco. Para obtener más detalles sobre los conceptos de apalancamiento e influencia, consulta el capítulo 2 del libro de <@bib="Davidson y MacKinnon (1993);davidson-mackinnon93">. 

También se calculan los valores DFFITS: estos son iguales a los Errores tipificados (errores divididos por sus desviaciones típicas) multiplicados por la raíz cuadrada de <@mth="h">(1 – <@mth="h">). Proporcionan una medida de la diferencia en el ajuste de la observación <@mth="i"> dependiendo de si esa observación está incluida o no en la muestra de la estimación. Para más información sobre este apartado, consulta el capítulo 12 del libro de Maddala <@bib="Introduction to Econometrics;maddala92"> o <@bib="Belsley, Kuh y Welsch (1980);belsley-etal80">. Para más detalles sobre los Errores tipificados consulta más abajo, la sección titulada <@itl="Matriz mediante accesor">. 

Cuando especificas la opción <@opt="--⁠save"> con esta instrucción, los valores de apalancamiento, influencia y DFFITS se añaden al conjunto vigente de datos; en este contexto, puedes utilizar la opción <@opt="--⁠quiet"> para eliminar la presentación de los resultados. Los nombres por defecto de las series guardadas son <@lit="lever">, <@lit="influ"> y <@lit="dffits">, respectivamente. Si ya existen series con esos nombres, lo que suceda dependerá de si indicas la opción <@opt="--⁠overwrite">, pues en ese caso se van a sobrescribir las series ya existentes. En caso contrario, los nombres se van a ajustar para poder garantizar la unicidad, y las nuevas series generadas serán las 3 series con números ID más grandes del conjunto de datos. 

Después de la ejecución, el accesor <@xrf="$test"> devuelve el criterio de validación cruzada, que se define como la suma de las desviaciones cuadradas de la variable dependiente con relación a sus valores de predicción, estando la predicción para cada observación basada en una muestra de la que se excluye esa observación. (Este es el conocido como estimador <@itl="dejar-uno-fuera">). Para una discusión más amplia sobre el criterio de validación cruzada, consulta el libro de Davidson y MacKinnon <@itl="Econometric Theory and Methods">, páginas 685–686, y las referencias que contiene. 

Por defecto, si haces una llamada interactiva a esta instrucción, se muestra un gráfico con los valores de apanlancamiento e influencia. Puedes axustar esto mediante la opción <@opt="--⁠plot">. Los parámetros que se admiten para esta opción son <@lit="none"> (para suprimir el gráfico), <@lit="display"> (para mostrar un gráfico incluso al estar en modo de guiones), o un nombre de archivo. El efecto de indicar un nombre de archivo es como el descrito para la opción <@opt="--⁠output"> de la instrucción <@ref="gnuplot">. 

<subhead>Matriz mediante accesor</subhead> 

Además de la opción <@opt="--⁠save"> señalada antes, puedes recuperar los resultados de esta instrucción en formato de una matriz de tres columnas por medio del accesor <@xrf="$result">. Las dos primeras columnas de la mencionada matriz contienen los valores de apalancamiento y de influencia (como con <@opt="--⁠save">), pero la tercera columna contiene los Errores tipificados, en lugar de los valores DFFITS. Estos son errores “Tipificados externamente” o “navajeados (jackknifed)” —es decir, la desviación típica que está en el divisor para la observación <@mth="i"> utiliza la media de los cuadrados de los errores, omitiendo esa observación. Ese tipo de error puede interpretarse como un estadístico de prueba <@mth="t"> para la hipótesis de que una variable ficticia 0/1 que codifica de forma especial la observación <@mth="i">, tendría un coeficiente real nulo. Para obtener más detalles sobre la discusión adicional en torno a los Errores tipificados, consulta <@bib="Chatterjee e Hadi (1986);chatterjee-hadi86">. 

Los valores DFFITS también pueden obtenerse a partir de la matriz de <@lit="$result"> del siguiente modo: 

<code>          
   R = $result
   dffits = R[,3] .* sqrt(R[,1] ./ (1-R[,1]))
</code>

O utilizando series: 

<code>          
   series h = $result[,1]  # Apalancamiento
   series sr = $result[,3] # Error tipificado
   series dffits = sr * sqrt(h/(1-h))
</code>

Menú gráfico: Ventana de modelo: Análisis/Observaciones influyentes

# levinlin Tests

Argumentos: 	<@var="orden"> <@var="serie"> 
Opciones: 	<@lit="--nc"> (Sin constante)
		<@lit="--ct"> (Con constante y tendencia)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--verbose"> (Presenta los resultados por unidad)
Ejemplos: 	<@lit="levinlin 0 y">
		<@lit="levinlin 2 y --ct">
		<@lit="levinlin {2,2,3,3,4,4} y">

Realiza el contraste de raíz unitaria para panel descrita por <@bib="Levin, Lin y Chu (2002);LLC2002">. La hipótesis nula es que todas las series temporales individuales presentan una raíz unitaria, y la alternativa es que ninguna de las series tiene una raíz unitaria. (Es decir, se asume un mismo coeficiente común de AR(1), aunque en otros aspectos se permite que las propiedades estadísticas de las series varíen de unos individuos a otros.) 

Por defecto, las regresiones del contraste ADF incluyen una constante. Para eliminar la constante utiliza la opción <@opt="--⁠nc"> y para incluirla junto con una tendencia lineal utiliza la opción <@opt="--⁠ct">. (Consulta la instrucción <@ref="adf"> para una explicación de las regresiones del ADF.) 

Puedes indicar el orden de retardo con <@var="orden"> (no negativo) para hacer el contraste (controlando así el número de retardos de la variable dependiente a incluir en las regresiones del ADF) de una de estas dos formas. Cuando indicas un valor escalar, esto se aplica a todos los individuos del panel. La alternativa es proporcionar una matriz que contenga un orden específico de retardos para cada individuo; esta debe ser un vector con tantos elementos como individuos haya en el rango de la muestra vigente. Puedes especificar esa matriz con el nombre o construirla utilizando llaves, como se ilustró en el último ejemplo de arriba. 

Cuando indicas la opción <@opt="--⁠verbose">, se presentan los siguientes resultados para cada unidad del panel: <@lit="delta">, el coeficiente del nivel retardado en cada regresión ADF; <@lit="s2e">, la varianza estimada de las innovaciones; y <@lit="s2y">, la varianza estimada a largo plazo de la serie diferenciada. 

Observa que los contrastes de raíz unitaria en un panel también puedes hacerlos utilizando las instrucciones <@ref="adf"> y <@ref="kpss">. 

Menú gráfico: /Variable/Contrastes de raíz unitaria/Contraste Levin-Lin-Chu

# logistic Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--ymax">=<@var="máximo"> (Especifica el máximo de la variable dependiente)
		<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--cluster">=<@var="clustvar"> (Consulta <@ref="logit"> para una explicación)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--fixed-effects"> (Mira abajo)
		<@lit="--quiet"> (No presenta nada)
Ejemplos: 	<@lit="logistic y const x">
		<@lit="logistic y const x --ymax=50">

Regresión logística: Lleva a cabo una regresión MCO utilizando la transformación logística de la variable dependiente, 

  <@fig="logistic1">

En caso de usar datos de panel, la especificación puede incluír los efectos fijos individuales. 

La variable dependiente debe ser estrictamente positiva. Si todos sus valores están entre 0 y 1, por defecto se utiliza un valor de <@mth="y"><@sup="*"> (el máximo asintótico de la variable dependiente) igual a 1; si sus valores están entre 0 y 100, entonces <@mth="y"><@sup="*"> es 100 por defecto. 

Si quieres establecer un máximo diferente, utiliza la opción <@opt="--⁠ymax">. Ten en cuenta que el valor que indiques debe ser mayor que todos los valores observados de la variable dependiente. 

Los valores ajustados y los errores de la regresión se transforman automáticamente utilizando la inversa de la transformación logística: 

  <@fig="logistic2">

donde <@mth="x"> representa un valor ajustado o un error, obtenidos de la regresión MCO que utiliza la variable dependiente logística. De este modo puedes comparar los valores que se presentan con los de la variable dependiente original. La aproximación es necesaria pues la transformación inversa no es lineal, y por lo tanto la esperanza no se corresponde exactamente. 

La opción <@opt="--⁠fixed-effects"> solo es aplicable cuando el conjunto de datos tiene forma de panel. En ese caso, se le restan las medias de grupo de la transformación logística de la variable dependiente, y la estimación continúa como se hace habitualmente con efectos fijos. 

Ten en cuenta que si la variable dependiente es binaria, debes utilizar en su lugar la instrucción <@ref="logit">. 

Menú gráfico: /Modelo/Variable dependiente limitada/Logística
Menú gráfico: /Modelo/Panel/Logística FE

# logit Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--cluster">=<@var="clustvar"> (Desviaciones típicas agrupadas)
		<@lit="--multinomial"> (Estima un logit multinomial)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--p-values"> (Muestra los valores p en vez de las pendientes)
		<@lit="--estrella"> (Elige la variante pseudo-R-cuadrado)
Ejemplos: 	<@inp="keane.inp">, <@inp="oprobit.inp">

Si la variable dependiente es una variable binaria (todos sus valores son 0 o 1), se obtienen estimaciones máximo verosímiles de los coeficientes de las variables de <@var="indepvars"> mediante el método de Newton–Raphson. Como el modelo es no lineal, las pendientes están condicionadas por los valores de las variables independientes. Por defecto, se calculan las pendientes con respecto a cada una de las variables independientes (en las medias de esas variables), y estas pendientes substituyen los valores p habituales en el resultado de la regresión. Puedes prescindir de este proceder indicando la opción <@opt="--⁠p-values">. El estadístico chi-cuadrado contrasta la hipótesis nula de que todos los coeficientes son cero, excepto el de la constante. 

Por defecto, las desviaciones típicas se calculan utilizando la inversa negativa de la matriz Hessiana. Si indicas la opción <@opt="--⁠robust">, entonces se calculan en su lugar las desviaciones típicas CMV (QML) o de Huber–White. En este caso, la matriz de covarianzas estimadas es un “emparedado” entre la inversa de la matriz Hessiana estimada y el producto externo del vector gradiente; consulta el capítulo 10 del libro de <@bib="Davidson y MacKinnon (2004);davidson-mackinnon04">. Pero cuando indicas la opción <@opt="--⁠cluster">, entonces se generan las desviaciones típicas “robustas por agrupación”; consulta <@pdf="El manual de gretl#chap:robust_vcv"> (Capítulo 22) para obtener más detalles. 

Por defecto, se va a presentar el estadístico pseudo-R-cuadrado que fue sugerido por <@bib="McFadden (1974);mcfadden74">; pero en el caso binario, si indicas la opción <@opt="--⁠estrella"> se va a presentar en su lugar la variante recomendada por <@bib="Estrella (1998);estrella98">. Esta variante presumiblemente imita de forma más parecida las propiedades del <@mth="R"><@sup="2"> habitual en el contexto de la estimación de mínimos cuadrados. 

Si la variable dependiente es binaria, los coeficientes de logit van a representar los logaritmos de los ratios de probabilidades (cocientes entre la probabilidad de que <@mth="y"> = 1 y la de que <@mth="y"> = 0). En ese caso, el bundle de <@lit="$model"> disponible después de la estimación incluye un elemento adicional denominado <@lit="oddsratios">, una matriz con cuatro columnas que contienen el coeficiente (ratio de de probabilidades) exponenciado, más el error típico calculado mediante el método delta, y el intervalo con el 95 por ciento de confianza, para cada regresor. Ten en cuenta, sin embargo, que el intervalo de confianza se calcula como el exponente del intervalo para el coeficiente original. 

Si la variable dependiente no es binaria sino discreta, entonces por defecto se interpreta como una respuesta ordinal y se obtienen las estimaciones con un Logit Ordenado. Sin embargo, cuando indicas la opción <@opt="--⁠multinomial">, la variable dependiente se interpreta como una respuesta sin ordenar y se generan las estimaciones con un Logit Multinomial. (En otro caso, si la variable escogida como dependiente no es de tipo discreto, se muestra un fallo.) El accesor <@lit="$allprobs"> está disponible después de la estimación, para conseguir una matriz que contenga las probabilidades estimadas de los posibles valores de la variable dependiente para cada observación (con las observaciones por filas y los posibles valores por columnas). 

Si quieres utilizar un Logit para el análisis de proporciones donde, para cada observación, la variable dependiente es la proporción de casos que tienen una determinada característica (en vez de una variable con 1 o 0 para indicar si está presente o no la característica), no debes utilizar la instrucción <@lit="logit">, sino más bien construir la variable logit, como en 

<code>          
   series lgt_p = log(p/(1 - p))
</code>

y utilizar esta como la variable dependiente de una regresión MCO. Consulta el capítulo 12 de <@bib="Ramanathan (2002);ramanathan02">. 

Menú gráfico: /Modelo/Variable dependiente limitada/Logit

# logs Transformations

Argumento: 	<@var="listavariables"> 

Permite obtener el logaritmo natural de cada una de las series de <@var="listavariables"> y el resultado se guarda en una nueva serie con el prefijo <@lit="l_"> (“ele” y guion bajo). Por ejemplo, <@lit="logs x y"> genera las nuevas variables <@lit="l_x"> = ln(<@lit="x">) y <@lit="l_y"> = ln(<@lit="y">). 

Menú gráfico: /Añadir/Logaritmos de las variables seleccionadas

# loop Programming

Argumento: 	<@var="control"> 
Opciones: 	<@lit="--progressive"> (Permite formas especiales de ciertas instrucciones)
		<@lit="--verbose"> (Refleja las instrucciones y muestra mensajes confirmatorios)
		<@lit="--decr"> (Mira abajo)
Ejemplos: 	<@lit="loop 1000">
		<@lit="loop i=1..10">
		<@lit="loop while essdiff > .00001">
		<@lit="loop for (r=-.99; r<=.99; r+=.01)">
		<@lit="loop foreach i listaX">
		Ver también <@inp="armaloop.inp">, <@inp="keane.inp">

Esta instrucción abre un modo especial en el que el programa admite que las instrucciones se ejecuten repetidas veces. Terminas el proceso de ir introduciendo las instrucciones del bucle con <@lit="endloop"> y en este punto se ejecutan las instrucciones acumuladas. 

El parámetro <@var="control"> puede tener cualquiera de las 5 formas siguientes, tal como se muestra en los ejemplos: (a) un número entero que indica las veces a repetir las instrucciones de un bucle; (b) un rango de valores enteros para una variable índice; (c) la palabra “<@lit="while">” más una condición booleana; (d) la palabra “<@lit="for">” más 3 expresiones dentro de un paréntesis, separadas con punto y comas (que imita la orden <@lit="for"> en el lenguaje de programación C); o (e) la palabra “<@lit="foreach">” más una variable índice y una lista. 

La opción <@opt="--⁠decr"> es específica solo para la forma de bucle del tipo “rango de valores enteros”. Por defecto, el índice se incrementa en 1 en cada iteración; y si el valor de inicio es menor que el valor final, el bucle no va a funcionar. Pero cuando se proporcione un valor para <@opt="--⁠decr">, el índice se minora en 1 en cada iteración. 

Consulta <@pdf="El manual de gretl#chap:looping"> (Capítulo 13) para obtener todos los detalles y ejemplos. Ahí se explica el efecto de la opción <@opt="--⁠progressive"> (que está diseñada para ser utilizada con simulaciones de tipo Monte Carlo). No puedes utilizar todas las instrucciones de GRETL dentro de un bucle; por eso las instrucciones disponibles en este contexto también se exponen ahí. 

Por defecto, la ejecución de instrucciones se hace de modo más silencioso dentro de bucles que en otros contextos. Si quieres más retroalimentación con lo que esté sucediendo en un bucle, indica la opción <@opt="--⁠verbose">. 

# mahal Statistics

Argumento: 	<@var="listavariables"> 
Opciones: 	<@lit="--quiet"> (No presenta nada)
		<@lit="--save"> (Añade las distancias al conjunto de datos)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)

Calcula las distancias de Mahalanobis entre las series indicadas en <@var="listavariables">. La distancia de Mahalanobis es la distancia entre dos puntos en un espacio de dimensión <@mth="k">, escalada por la variación estadística en cada dimensión del espacio. Por ejemplo, si <@mth="p"> y <@mth="q"> son dos observaciones de un conjunto de <@mth="k"> variables con matriz de covarianzas <@mth="C">, entonces la distancia de Mahalanobis entre las observaciones viene dada por 

  <@fig="mahal">

donde (<@mth="p"> – <@mth="q">) es un vector de dimensión <@mth="k">. Esto se reduce a la distancia euclidiana en caso de que la matriz de covarianzas sea una matriz identidad. 

El espacio para el que se calculan las distancias está definido por las variables seleccionadas. Para cada observación del rango vigente de la muestra, la distancia se calcula entre la observación y el centroide de las variables escogidas. Esta distancia es la contrapartida multidimensional de una puntuación <@mth="z"> estándar, y puedes utilizarla para juzgar si una observación dada “pertenece” a un grupo de otras observaciones. 

Cuando indicas la opción <@opt="--⁠vcv">, se presentan tanto la matriz de covarianzas como su inversa. Cuando indicas la opción <@opt="--⁠save">, las distancias se guardan en el conjunto de datos con el nombre <@lit="mdist"> (o <@lit="mdist1">, <@lit="mdist2"> y así sucesivamente, si ya existe una variable con ese nombre). 

Menú gráfico: /Ver/Distancias de Mahalanobis

# makepkg Programming

Argumento: 	<@var="nombrearchivo"> 
Opciones: 	<@lit="--index"> (Escribe el archivo índice, auxiliar)
		<@lit="--translations"> (Escribe el archivo de cadenas de texto, auxiliar)
		<@lit="--quiet"> (Funciona sigilosamente)

Da soporte a la creación de un paquete de funciones de GRETL mediante la línea de instrucciones. El modo de funcionamiento de esta instrucción depende de la extensión del <@var="nombrearchivo">, que debe ser <@lit=".gfn"> o <@lit=".zip">. 

<subhead>Modo gfn</subhead> 

Escribe un archivo gfn. Se asume que puede accederse a un archivo de especificación de un paquete, que tiene el mismo nombre base que <@var="nombrearchivo"> pero con la extensión <@lit=".spec">, junto con cualquier archivo auxiliar al que haga referencia. También se asume que todas las funciones a empaquetar se leyeron en la memoria. 

<subhead>Modo zip</subhead> 

Escribe un archivo comprimido zip de un paquete (un gfn más otros elementos). En caso de encontrarse un archivo gfn con el mismo nombre base que <@var="nombrearchivo">, GRETL comprueba los archivos correspondientes <@lit="inp"> y <@lit="spec">, y si los encuentra a ambos, siendo por lo menos uno de ellos más nuevo que el archivo gfn, entonces se vuelve a generar el gfn; en otro caso, se utiliza el gfn existente. Cuando no se encuentra ese archivo, GRETL intenta primero generar el gfn. 

<subhead>Opciones de gfn</subhead> 

Los indicadores de opciones admiten la escritura de archivos auxiliares, pensados para utilizar con los “añadidos” de GRETL. El archivo índice es un corto documento XML que contiene información básica sobre el paquete, y que tiene su mismo nombre como base además de la extensión <@lit=".xml">. El archivo de traducciones contiene las cadenas de texto del paquete (en formato C) que podrían ser apropiadas para la traducción; para un paquete <@lit="foo"> este archivo se llama <@lit="foo-i18n.c">. Estos archivos no se generan si la instrucción opera en modo zip, y se utiliza un archivo gfn que ya existía. 

Para obtener más detalles sobre todo esto, consulta la <@mnu="Pkgbook"> de GRETL. 

Menú gráfico: /Archivo/Paquetes de funciones/Paquete nuevo

# markers Dataset

Variantes: 	<@lit="markers --to-file="><@var="nombrearchivo">
		<@lit="markers --from-file="><@var="nombrearchivo">
		<@lit="markers --to-array="><@var="nombre">
		<@lit="markers --from-array="><@var="nombre">
		<@lit="markers --from-series="><@var="nombre">
		<@lit="markers --delete">

Las opciones <@opt="--⁠to-file"> y <@opt="--⁠to-array"> proporcionan formas de guardar las cadenas de texto que son marcadores de observaciones del conjunto vigente de datos, en el archivo o 'array' que indiques. Si no hay ninguna de esas cadenas, se muestra un fallo. En el caso del archivo, las cadenas se escriben una por línea en ese archivo, y este se guarda en el directorio (<@ref="workdir">) establecido en ese momento, excepto que la cadena <@var="nombrearchivo"> contenga una especificación completa de la ruta. En el caso del 'array', si <@var="nombre"> es el identificador de un 'array' de cadenas de texto ya existente, ese 'array' se va a sobrescribir; en caso contrario, se crea uno nuevo. 

Con la opción <@opt="--⁠from-file">, se lee el archivo especificado (que debe ser de texto UTF-8) y se asignan las etiquetas de observación contenidas en este, uno por cada línea, a las filas del conjunto de datos. En general, debería haber como mínimo tantas etiquetas en el archivo como observaciones en el conjunto de datos; pero si el conjunto de datos es de tipo panel, también se acepta que el número de etiquetas en el archivo coincida con el número de unidades de sección cruzada (en cuyo caso las etiquetas se repiten para cada período de tiempo.) La opción <@opt="--⁠from-array"> funciona de modo similar, realizando la lectura a partir de un 'array' de cadenas de texto determinado. 

La opción <@opt="--⁠from-series"> ofrece un modo adecuado de crear etiquetas de observación, copiándolas de una serie con valores en forma de cadenas de texto. Se presenta un fallo cuando la serie indicada no tiene valores de cadena de texto. 

La opción <@opt="--⁠delete"> hace lo que ya esperarías, es decir, eliminar las cadenas de texto que etiquetan cada observación del conjunto de datos. 

Menú gráfico: /Datos/Etiquetas de las observaciones

# meantest Tests

Argumentos: 	<@var="serie1"> <@var="serie2"> 
Opción: 	<@lit="--unequal-vars"> (Asume que las varianzas no son iguales)

Calcula el estadístico <@mth="t"> para contrastar la hipótesis nula de que las medias en la población son iguales para las variables <@var="serie1"> y <@var="serie2">, y muestra su probabilidad asociada (valor p). 

Por defecto, el estadístico de contraste se calcula bajo el supuesto de que las varianzas son iguales para las dos variables. Con la opción <@opt="--⁠unequal-vars"> se asume que las varianzas son diferentes; y en este caso, los grados de libertad del estadístico de contraste se aproximan conforme a <@bib="Satterthwaite (1946);satter46">. 

Menú gráfico: /Herramientas/Calculadora de estadísticos de contraste

# midasreg Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> ; <@var="términosMIDAS"> 
Opciones: 	<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--levenberg"> (Mira abajo)
Ejemplos: 	<@lit="midasreg y 0 y(-1) ; mds(X, 1, 9, 1, theta)">
		<@lit="midasreg y 0 y(-1) ; mds(X, 1, 9, 0)">
		<@lit="midasreg y 0 y(-1) ; mdsl(XL, 2, theta)">
		Ver también <@inp="gdp_midas.inp">

Lleva a cabo la estimación por mínimos cuadrados (bien MCNL o bien MCO, dependiendo de la especificación) de un modelo MIDAS (Mixed Data Sampling). Este tipo de modelos incluye una o más variables independientes que se observan con una frecuencia mayor que a variable dependiente; para una buena y breve introducción consulta <@bib="Armesto, Engemann y Owyang (2010);armesto10">. 

Las variables de <@var="indepvars"> deben tener la misma frecuencia que la variable dependiente. Esta lista normalmente debe incluir <@lit="const"> o <@lit="0"> (ordenada en el origen), y habitualmente incluye uno o más retardos de la variable dependiente. Los términos de alta frecuencia se indican después de un punto y coma; cada uno tiene el formato de unos cuantos argumentos entre paréntesis, separados por comas, precedidos por <@lit="mds"> o por <@lit="mdsl">. 

<@lit="mds">: Esta variante generalmente requiere 5 argumentos, del modo siguiente: el nombre de una <@ref="MIDAS_list">, dos enteros que indican los retardos mínimo y máximo de alta frecuencia, un entero entre 0 y 4 (o una cadena de texto, mira abajo) que especifica el tipo de disposición de los parámetros que se va a usar, y el nombre de un vector que contiene los valores iniciales de los parámetros. El ejemplo de abajo solicita los retardos del 3 al 11 de las series de alta frecuencia representadas en la lista <@lit="X">, utilizando para ello una disposición de los parámetros de tipo 1 (Almon exponencial, mira abajo) con el vector de inicio <@lit="theta">. 

<code>          
   mds(X, 3, 11, 1, theta)
</code>

<@lit="mdsl">: Generalmente requiere 3 argumentos: el nombre de una lista de retardos MIDAS, un número entero (o una cadena de texto, mira abajo) para especificar el tipo de disposición de los parámetros y el nombre de un vector de inicio. En este caso, los retardos máximo y mínimo están implícitos en el argumento inicial de la lista. En el ejemplo de abajo <@lit="Xlags"> debe ser una lista que ya contenga todos los retardos que se necesiten; puedes construir una lista de ese tipo utilizando la función <@xrf="hflags">. 

<code>          
   mdsl(XLags, 1, theta)
</code>

Los tipos de disposición de parámetros que se admiten, se muestran abajo. En el contexto de las especificaciones <@lit="mds"> y <@lit="mdsl">, puedes indicarlos en forma de los códigos numéricos, o de las cadenas de texto entre comillas que se muestran después de los números: 

0 o <@lit=""umidas"">: MIDAS sin restricciones o U-MIDAS, en el que cada retardo tiene su propio coeficiente. 

1 o <@lit=""nealmon"">: Almon exponencial normalizada, que requiere por lo menos un parámetro y habitualmente utiliza dos. 

2 o <@lit=""beta0"">: Beta normalizada con un último retardo nulo, que requiere exactamente dos parámetros. 

3 o <@lit=""betan"">: Beta normalizada con un último retardo no nulo, que requiere exactamente tres parámetros. 

4 o <@lit=""almonp"">: Polinomio de Almon (no normalizada), que requiere por lo menos un parámetro. 

5 or <@lit=""beta1"">: Similar a <@lit="beta0">, pero con el primer parámetro fijado en 1 (dejando un único parámetro libre). 

Cuando la disposición de parámetros es U-MIDAS, no es necesario el vector de inicio del último argumento. En otros casos, puedes solicitar un inicio automático substituyendo el nombre del vector de parámetros inicial por alguna de estas dos formas: 

<indent>
• La palabra clave <@lit="null">: esto solo es admisible cuando la disposición de los parámetros tiene un número fijo de términos (los casos Beta, con 2 o 3 parámetros). También se acepta en el caso del Almon exponencial, lo que implica que ese es el valor por defecto de los dos parámetros. 
</indent>

<indent>
• Un valor entero que indica el número requerido de parámetros. 
</indent>

El método de estimación que utiliza esta instrucción depende de la especificación de los elementos de alta frecuencia. En el caso de U-MIDAS, el método es MCO (OLS); de lo contario, es mínimos cuadrados no lineales (MCNL o NLS). Cuando especificas las disposiciones de parámetros Almon exponencial normalizada o Beta normalizada, el método MCNL por defecto es una combinación de BFGS restringido y MCO, pero puedes indicar la opción <@opt="--⁠levenberg"> para forzar que se utilice el algoritmo de Levenberg–Marquardt. 

Menú gráfico: /Modelo/Series temporales univariantes/MIDAS

# mle Estimation

Argumentos: 	<@var="función logaritmo-verosimilitud"> [ <@var="derivadas"> ] 
Opciones: 	<@lit="--quiet"> (No muestra el modelo estimado)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--hessian"> (Basa la matriz de covarianzas en la Hessiana)
		<@lit="--robust">[=<@var="hac">] (Matriz de covarianzas CMV (QML) o HAC)
		<@lit="--cluster">=<@var="clustvar"> (Matriz de covarianzas robusta por agrupación)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--no-gradient-check"> (Mira abajo)
		<@lit="--auxiliary"> (Mira abajo)
		<@lit="--lbfgs"> (Utiliza L-BFGS-B en vez del BFGS habitual)
Ejemplos: 	<@inp="weibull.inp">, <@inp="biprobit_via_ghk.inp">, <@inp="frontier.inp">, <@inp="keane.inp">

Realiza la estimación de Máxima Verosimilitud (MV o ML) utilizando bien el algoritmo BFGS (Broyden, Fletcher, Goldfarb, Shanno) o bien el método de Newton. Debes especificar la función logaritmo de verosimilitud. Y debes expresar los parámetros de esta función, y asignarles valores iniciales antes de la estimación. Opcionalmente, el usuario puede especificar las derivadas de la función logaritmo de verosimilitud con respecto a cada uno de los parámetros; si no indicas las derivadas analíticas, se calcula una aproximación numérica. 

Este texto de ayuda asume que se utiliza, por defecto, el maximizador BFGS. Para obtener más información sobre el uso del método de Newton, por favor consulta <@pdf="El manual de gretl#chap:mle"> (Capítulo 26). 

Ejemplo sencillo: Supón que tenemos una serie <@lit="X"> con valores 0 o 1, y queremos obtener la estimación máximo verosímil de la probabilidad (<@lit="p">) de que <@lit="X"> = 1. (En este caso sencillo, se puede adelantar que la estimación MV de <@lit="p"> será simplemente equivalente a la proporción de Xs iguales a 1, en la muestra.) 

Primero se debe añadir el parámetro <@lit="p"> al conjunto de datos, e indicar su valor inicial. Por ejemplo, <@lit="scalar p = 0.5">. 

A continuación, se configura el bloque de instrucciones de estimación EMV: 

<code>          
   mle loglik = X*log(p) + (1-X)*log(1-p)
     deriv p = X/p - (1-X)/(1-p)
   end mle
</code>

La primera línea de arriba especifica la función logaritmo de verosimilitud. Comienza con la palabra clave <@lit="mle">, después se especifica la variable dependiente y se indica una expresión para el logaritmo de la verosimilitud (usando la misma sintaxis que en la instrucción <@lit="genr">). La siguiente línea (que es opcional) comienza con la palabra clave <@lit="deriv"> y proporciona la derivada de la función logaritmo de verosimilitud con respecto al parámetro <@lit="p">. Si no indicas las derivadas, debes incluir una orden utilizando la palabra clave <@lit="params"> que identifique los parámetros libres: estos se enumeran en una línea, separados por espacios y pueden ser bien escalares, bien vectores, o bien cualquier combinación de los dos. Por ejemplo, puedes cambiar lo de arriba por: 

<code>          
   mle loglik = X*log(p) + (1-X)*log(1-p)
     params p
   end mle
</code>

en cuyo caso se utilizarían derivadas numéricas. 

Ten en cuenta que cualquier indicador de opción debe añadirse a la línea final del bloque EMV (MLE). Por ejemplo: 

<code>          
   mle loglik = X*log(p) + (1-X)*log(1-p)
     params p
   end mle --quiet
</code>

<subhead>Matriz de covarianzas y desviaciones típicas</subhead> 

Cuando la función del logaritmo de la verosimilitud devuelve una serie o un vector que proporciona valores por observación, entonces las desviaciones típicas estimadas se basan por defecto en el Producto Externo del vector Gradiente (PEG); mientras que si indicas la opción <@opt="--⁠hessian">, por el contario se basan en la inversa negativa de la matriz Hessiana, que se aproxima numéricamente. Cuando indicas la opción <@opt="--⁠robust">, se utiliza un estimador CMV (QML, un “emparedado” entre la inversa negativa de la matriz Hessiana y el PEG). Si además añades el parámetro <@lit="hac"> a esta opción, el PEG se incrementa del modo de <@bib="Newey y West;newey-west87"> para permitir autocorrelación del gradiente. (Esto únicamente tiene sentido con datos de series de tiempo.) Ahora bien, cuando la función del logaritmo de la verosimilitud únicamente devuelve un valor escalar, el PEG no está disponible (por lo tanto tampoco el estimador CMV), y las desviaciones típicas tienen que calcularse necesariamente utilizando la matriz Hessiana numérica. 

En caso de que únicamente quieras las estimaciones del parámetro primario, puedes indicar la opción <@opt="--⁠auxiliary">, que elimina el cálculo de la matriz de covarianzas y de las desviaciones típicas. Esto va a ahorrar algunos ciclos de CPU y uso de memoria. 

<subhead>Comprobando las derivadas analíticas</subhead> 

Si proporcionas las derivadas analíticas, por defecto GRETL ejecuta una verificación numérica de su credibilidad. Algunas veces esto puede producir falsos positivos, por situaciones en las que las derivadas correctas parecen ser incorrectas y la estimación se rechaza. Para tener esto en cuenta o para conseguir un poco de velocidad adicional, puedes indicar la opción <@opt="--⁠no-gradient-check">. Obviamente, debes hacer esto solo cuando tengas certeza de que el vector gradiente que has especificado es correcto. 

<subhead>Nombres de parámetros</subhead> 

Al estimar un modelo no lineal, con frecuencia es conveniente nombrar los parámetros de forma sucinta. Ahora bien, al presentar los resultados, puede que desees utilizar etiquetas más informativas. Esto lo puedes conseguir mediante la palabra clave adicional <@lit="param_names"> dentro del bloque de instrucciones. Para un modelo con <@mth="k"> parámetros, el argumento que sigue a esta palabra clave debe ser una cadena de texto literal entre comillas que contenga <@mth="k"> nombres separados por espacios, el nombre de una variable de cadena que contenga <@mth="k"> de esos nombres, o el nombre de un array con <@mth="k"> cadenas de texto. 

Para una descripción más en profundidad de la estimación <@lit="mle"> consulta <@pdf="El manual de gretl#chap:mle"> (Capítulo 26). 

Menú gráfico: /Modelo/Máxima Verosimilitud

# modeltab Utilities

Variantes: 	<@lit="modeltab add">
		<@lit="modeltab show">
		<@lit="modeltab free">
		<@lit="modeltab --output="><@var="nombrearchivo">
		<@lit="modeltab --options="><@var="bundle">

Permite manejar la “Tabla de modelos” de GRETL; consulta <@pdf="El manual de gretl#chap:modes"> (Capítulo 3) para obtener más detalles. Las instrucciones subordinadas tienen los siguientes efectos: <@lit="add"> añade el último modelo estimado a la tabla de modelos, cuando sea posible; <@lit="show"> muestra la tabla de modelos en una ventana; y <@lit="free"> vacía la tabla. 

Para solicitar que se guarde la tabla de modelos, usa la opción <@opt="--⁠output="> más un nombre de archivo. Cuando el nombre del archivo tenga el sufijo “<@lit=".tex">”, el resultado va a estar en formato TeX; cuando el sufijo sea “<@lit=".rtf">”, el resultado tendrá formato RTF; y en otro caso, va a estar en texto plano. En caso de un resultado TeX, por defecto se genera un “trozo” adecuado para incluir en un documento; en cambio, si quieres un documento independiente, usa la opción <@opt="--⁠complete">, como por ejemplo 

<code>          
   modeltab --output="myfile.tex" --complete
</code>

Puedes utilizar el indicador <@opt="--⁠options="> (lo que requiere indicar el nombre de un 'bundle' de GRETL) para controlar algunos aspectos de formato en la tabla del modelo. Se admiten las siguientes claves: 

<indent>
• <@lit="colheads">: Entero de 1 a 4, que permite escoger entre los cuatro estilos admitidos para el encabezamiento de columnas: numeración Arábiga, numeración Romana, alfabética, o la utilización de los nombres bajo los que se guardaron los modelos. El predeterminado es 1 (numeración Arábiga). 
</indent>

<indent>
• <@lit="tstats">: Booleano, que permite sustituir las desviaciones típicas con los estadísticos t, o no (el predeterminado, 0). 
</indent>

<indent>
• <@lit="pvalues">: Booleano, que permite incluir los valores <@mth="P">, o no (el predeterminado, 0). 
</indent>

<indent>
• <@lit="asterisks">: Booleano, que permite mostrar asteriscos relativos al nivel de significación, o no (el predeterminado, 0). 
</indent>

<indent>
• <@lit="digits">: Entero de 2 a 6, que permite elegir el número de dígitos significativos que se muestran (el predeterminado, 4). 
</indent>

<indent>
• <@lit="decplaces">: Entero de 2 a 6, que permite elegir el número de posiciones decimales que se muestran. 
</indent>

Ten en cuenta que las dos últimas claves son mutuamente excluyentes. Ofrecen modos alternativos de especificar la precisión con la que se muestran los valores numéricos: o en términos de dígitos significativos, o en términos de posiciones decimales. Por defecto, son 4 dígitos significativos. 

Puedes indicar un 'bundle' de opciones mediante una instrucción independiente (como en el último de los ejemplos de más abajo), o se puede combinar con el proceso <@lit="show">, o con la opción <@opt="--⁠output">. Por ejemplo, el siguiente guion elabora una simple tabla de un modelo y la presenta, mostrando los valores <@mth="P"> en lugar de los asteriscos relativos al nivel de significación: 

<code>          
   open data9-7
   ols 1 0 2 3 4
   modeltab add
   ols 1 0 2 3
   modeltab add
   bundle myopts = _(pvalues=1, asterisks=0)
   modeltab show --options=myopts
</code>

Menú gráfico: Ventana de iconos de sesión: icono de Tabla de modelos

# modprint Printing

Argumentos: 	<@var="matrizcoef"> <@var="nombres"> [ <@var="estadicionales"> ] 
Opción: 	<@lit="--output">=<@var="nombrearchivo"> (Envía el resultado al archivo especificado)

Presenta la tabla de coeficientes y estadísticos adicionales opcionales para un modelo estimado “a mano”; es útil sobre todo para funciones escritas por el usuario. 

El argumento <@var="matrizcoef"> debe ser una matriz de dimensión <@mth="k"> por 2, que contiene <@mth="k"> coeficientes y <@mth="k"> desviaciones típicas asociadas. El argumento <@var="nombres"> debe proporcionar por lo menos <@mth="k"> nombres para etiquetar los coeficientes. Puedes indicarlo con el formato: (a) de una cadena de texto literal (puesta entre comillas) o de una variable de cadena, que contenga los nombres separados por comas o espacios, o (b) un array ya definido de cadenas de texto. 

El argumento <@var="estadicionales"> (opcional) es un vector que contiene <@mth="p"> estadísticos adicionales que se muestran debajo de la tabla de coeficientes. Si indicas este argumento, entonces <@var="nombres"> debe contener <@mth="k + p"> nombres, de forma que los <@mth="p"> nombres agregados se asocien a los estadísticos adicionales. 

Si no indicas el argumento <@var="estadicionales"> y la matriz <@var="matrizcoef"> tiene adjuntos los nombres de las filas, entonces puedes omitir el argumento <@var="nombres">. 

Para colocar el resultado en un archivo, utiliza la opción <@opt="--⁠output="> más un nombre de archivo. Cuando el nombre de archivo tenga el sufijo “<@lit=".tex">”, el resultado va a estar en formato TeX; cuando el sufijo sea “<@lit=".rtf">”, el resultado tendrá formato RTF; y en otro caso, va a estar en texto plano. En caso de un resultado TeX, por defecto se genera un “trozo” adecuado para incluir en un documento; en cambio, si quieres un documento independiente, usa la opción <@opt="--⁠complete">. 

El archivo resultante se escribe en el directorio (<@ref="workdir">) establecido en ese momento, excepto que la cadena <@var="nombrearchivo"> contenga una especificación completa de la ruta. 

# modtest Tests

Argumento: 	[ <@var="orden"> ] 
Opciones: 	<@lit="--normality"> (Normalidad de las perturbaciones)
		<@lit="--logs"> (No linealidad: logaritmos)
		<@lit="--squares"> (No linealidad: cuadrados)
		<@lit="--autocorr"> (Autocorrelación)
		<@lit="--arch"> (ARCH)
		<@lit="--white"> (Heterocedasticidad: contraste de White)
		<@lit="--white-nocross"> (Contraste de White: solo cuadrados)
		<@lit="--breusch-pagan"> (Heterocedasticidad: contraste de Breusch–Pagan)
		<@lit="--robust"> (Estimación con varianzas robustas para Breusch–Pagan)
		<@lit="--panel"> (Heterocedasticidad: por grupos)
		<@lit="--comfac"> (Restricción de factor común: solo modelos AR1)
		<@lit="--xdepend"> (Dependencia de sección cruzada: solo con datos de panel)
		<@lit="--quiet"> (No presenta los detalles)
		<@lit="--silent"> (No presenta nada)
Ejemplos: 	<@inp="credscore.inp">

Debe seguir inmediatamente a una instrucción de estimación. La discusión de abajo se aplica a la utilización de esta instrucción a continuación de la estimación de un modelo de una única ecuación; consulta <@pdf="El manual de gretl#chap:var"> (Capítulo 32) para una exposición de como opera <@lit="modtest"> después de la estimación de un VAR. 

Dependiendo de la opción que indiques, esta instrucción efectúa una de estas acciones: el contraste de Normalidad de la perturbación de Doornik–Hansen; un contraste de No Linealidad (logaritmos o cuadrados) con Multiplicadores de Lagrange; el contraste de Heterocedasticidad de White (con o sin productos cruzados) o el de Breusch–Pagan (<@bib="Breusch y Pagan, 1979;breusch-pagan79">); el contraste LMF de Autocorrelación <@bib="(Kiviet, 1986);kiviet86">; un contraste de ARCH (Heterocedasticidad Condicional Autorregresiva; consulta también la instrucción <@lit="arch">); un contraste de la restricción de Factor Común implícita en la estimación AR(1); o un contraste de Dependencia de sección cruzada en modelos con datos de panel. Con la excepción de los contrastes de Normalidad, de Factor Común y de Dependencia de sección cruzada, la mayor parte de las opciones de estos contrastes solo están disponibles para modelos estimados mediante MCO, pero mira más abajo para obtener más detalles en relación con Mínimos Cuadrados en 2 Etapas. 

El argumento <@lit="orden"> (opcional) es importante solo en caso de que escojas las opciones <@opt="--⁠autocorr"> o <@opt="--⁠arch">. Por defecto, estos contrastes se ejecutan utilizando un orden de retardos igual a la periodicidad de los datos, pero puedes ajustar esto indicando un orden de retardos específico. 

La opción <@opt="--⁠robust"> se aplica únicamente cuando seleccionas el contraste de Breusch–Pagan; su efecto consiste en que se utiliza el estimador robusto de la varianza propuesto por <@bib="Koenker (1981);koenker81">, haciendo el contraste menos sensible al supuesto de Normalidad. 

La opción <@opt="--⁠panel"> está disponible solo cuando el modelo se estima con datos de panel; y en este caso, se realiza un contraste de heterocedasticidad por grupos (es decir, de varianzas de las perturbaciones diferentes entre las unidades de sección cruzada). 

La opción <@opt="--⁠comfac"> está disponible solo cuando el modelo se estima mediante un método AR(1) tal como el de Hildreth–Lu. La regresión auxiliar toma la forma de un modelo dinámico relativamente no restringido, que se utiliza para contrastar la restricción de factor común implícita en la especificación AR(1). 

La opción <@opt="--⁠xdepend"> está disponible solo para modelos estimados con datos de panel. El estadístico de contraste es el desarrollado por <@bib="Pesaran (2004);pesaran04">. La hipótesis nula es que la perturbación se distribuye independientemente entre las unidades atemporales o los individuos. 

Por defecto, el programa presenta la regresión auxiliar en la que se basa el estadístico de contraste, si es aplicable. Puedes eliminar esto utilizando la opción <@opt="--⁠quiet"> (presentación mínima de resultados) o la opción <@opt="--⁠silent"> (no presenta ningún resultado). Puedes recuperar el estadístico de contraste y su probabilidad asociada (valor p) utilizando los accesores <@xrf="$test"> y <@xrf="$pvalue">, respectivamente. 

Cuando un modelo se estima por Mínimos Cuadrados en 2 Etapas (consulta <@ref="tsls">), se rompe el principio de Máxima Verosimilitud y GRETL ofrece algunos equivalentes: la opción <@lit="--autocorr"> calcula el estadístico de Godfrey para contrastar autocorrelación <@bib="(Godfrey, 1994);godfrey94"> mientras que la opción <@lit="--white"> produce el estadístico del contraste HET1 de heterocedasticidad <@bib="(Pesaran y Taylor, 1999);pesaran99">. 

Para contrastes adicionales de diagnóstico sobre los modelos, consulta <@ref="chow">, <@ref="cusum">, <@ref="reset"> y <@ref="qlrtest">. 

Menú gráfico: Ventana de modelo: Contrastes

# mpi Programming

Argumento: 	<@var="Mira abajo"> 

La instrucción <@lit="mpi"> comienza un bloque de expresiones (que se deben finalizar con <@lit="end mpi">) para ser ejecutadas usando el cómputo en paralelo de MPI (Interfaz de Paso de Mensajes). Consulta <@adb="gretl-mpi.pdf"> para obtener un informe completo de esta prestación. 

# mpols Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--simple-print"> (No presenta los estadísticos auxiliares)
		<@lit="--quiet"> (No presenta los resultados)

Calcula las estimaciones de MCO para el modelo especificado, utilizando aritmética de punto flotante con precisión múltiple, con la ayuda de la biblioteca Gnu Multiple Precision (GMP). Por defecto, se utilizan 256 bits de precisión en los cálculos, pero puedes aumentar esto mediante la variable de entorno <@lit="GRETL_MP_BITS">. Por ejemplo, cuando utilizas el intérprete Bash se te podría ocurrir la siguiente instrucción para establecer una precisión de 1024 bits antes de comenzar GRETL. 

<code>          
   export GRETL_MP_BITS=1024
</code>

Dispones de una opción (más bien rebuscada) para esta instrucción, principalmente con el propósito de hacer pruebas: cuando la lista <@var="indepvars"> va seguida de un punto y coma, más de una lista posterior de números, esos números se toman como potencias de <@var="x"> que se añaden a la regresión, donde <@var="x"> es la última variable de <@var="indepvars">. Estos términos adicionales se calculan y se guardan con precisión múltiple. En el siguiente ejemplo, se hace la regresión de <@lit="y"> sobre <@lit="x"> más la segunda, tercera y cuarta potencias de ese <@lit="x">: 

<code>          
   mpols y 0 x ; 2 3 4
</code>

Menú gráfico: /Modelo/Otros modelos lineales/MCO de alta precisión

# negbin Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> [ ; <@var="exposición"> ] 
Opciones: 	<@lit="--model1"> (Utiliza el modelo NegBin 1)
		<@lit="--robust"> (Matriz de covarianzas CMV (QML))
		<@lit="--cluster">=<@var="clustvar"> (Consulta <@ref="logit"> para una explicación)
		<@lit="--opg"> (Mira abajo)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--quiet"> (No presenta los resultados)
Ejemplos: 	<@inp="camtriv.inp">

Estima un modelo Binomial Negativo. Se toma la variable dependiente para representar un recuento del número de veces que ocurre un suceso de algún tipo, y debe tener solo valores enteros no negativos. Por defecto, se utiliza el modelo NegBin 2 en el que la varianza condicionada del recuento viene determinada por μ(1 + αμ), donde μ denota la media condicionada. Pero si indicas la opción <@opt="--⁠model1">, la varianza condicionada es μ(1 + α). 

La serie de exposición (<@lit="offset">, opcional) funciona del mismo modo que para la instrucción <@ref="poisson">. El modelo de Poisson es una forma restringida de la Binomial Negativa en la que α = 0 por construcción. 

Por defecto, las desviaciones típicas se calculan utilizando una aproximación numérica a la matriz Hessiana en la convergencia. Pero si indicas la opción <@opt="--⁠opg">, la matriz de covarianzas se basa en el Producto Externo del vector Gradiente, PEG (OPG), y si indicas la opción <@opt="--⁠robust">, se calculan las desviaciones típicas CMV (QML), utilizando un “emparedado” entre la inversa de la matriz Hessiana y el PEG. 

Menú gráfico: /Modelo/Variable dependiente limitada/Datos de conteo

# nls Estimation

Argumentos: 	<@var="función"> [ <@var="derivadas"> ] 
Opciones: 	<@lit="--quiet"> (No presenta el modelo estimado)
		<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--no-gradient-check"> (Mira abajo)
Ejemplos: 	<@inp="wg_nls.inp">, <@inp="ects_nls.inp">

Realiza la estimación de Mínimos Cuadrados No Lineales (MCNL o NLS) utilizando una versión modificada del algoritmo de Levenberg–Marquardt. Debes indicar la especificación de una función y de enunciar los parámetros de esta, además de darles unos valores iniciales antes de la estimación. Como opción, puedes especificar las derivadas de la función de regresión con respecto a cada uno de los parámetros. Si no proporcionas as derivadas, en su lugar debes indicar una lista de los parámetros que se van a estimar (separados por espacios o comas), precedida por la palabra clave <@lit="params">. En este último caso, se calcula una aproximación numérica al Jacobiano. 

Resulta más fácil mostrar lo que se requiere mediante un ejemplo. Lo que sigue es un guion completo para estimar la función no lineal de consumo establecida en el libro <@itl="Econometric Analysis"> (capítulo 11 de la 4a edición o capítulo 9 de la 5a) de William Greene. Los números a la izquierda de las líneas son solo para tomar como referencia y no son parte de las instrucciones. Ten en cuenta que cualquier indicador de opción, como sería <@opt="--⁠vcv"> para presentar la matriz de covarianzas de los estimadores de los parámetros, deberías de añadirlo a la instrucción final, <@lit="end nls">. 

<code>          
   1   open greene11_3.gdt
   2   ols C 0 Y
   3   scalar alfa = $coeff(0)
   4   scalar beta = $coeff(Y)
   5   scalar gamma = 1.0
   6   nls C = alfa + beta * Y^gamma
   7    deriv alfa = 1
   8    deriv beta = Y^gamma
   9    deriv gamma = beta * Y^gamma * log(Y)
   10  end nls --vcv
</code>

Con frecuencia es conveniente iniciar los parámetros con una referencia a un modelo lineal relacionado; esto se logra aquí con las líneas de la 2 a la 5. Los parámetros alfa, beta y gamma pueden establecerse con cualquier valor inicial (no necesariamente basados en un modelo estimado con MCO), aunque la convergencia del procedimiento de MCNL no está garantizada para cualquier punto de inicio que se te antoje. 

Las auténticas instrucciones de MCNL ocupan las líneas de la 6 hasta la 10. En la línea 6 se indica la instrucción <@lit="nls"> en la que se declara la variable dependiente, con un signo de igualdad a continuación, y seguido este de la especificación de una función. La sintaxis para el lado derecho de la expresión es la misma que la de la instrucción <@lit="genr">. Las siguientes 3 líneas especifican las derivadas de la función de regresión con respecto a cada uno de los parámetros, de uno en uno. Cada línea comienza con la palabra clave <@lit="deriv">, establece el nombre de un parámetro, un signo de igualdad y una expresión por la que puede calcularse la derivada. En lugar de proporcionar las derivadas analíticas, como alternativa puedes substituir las líneas de la 7 a la 9, por lo siguiente: 

<code>          
   params alfa beta gamma
</code>

La línea 10, <@lit="end nls">, completa la instrucción y solicita la estimación. Cualquier opción deberás de añadirla a esta línea. 

Si proporcionas las derivadas analíticas, por defecto GRETL ejecuta una verificación numérica de su credibilidad. Algunas veces esto puede producir falsos positivos, por situaciones en las que las derivadas correctas parecen ser incorrectas y la estimación se rechaza. Para tener esto en cuenta o para conseguir un poco de velocidad adicional, puedes indicar la opción <@opt="--⁠no-gradient-check">. Obviamente, debes hacer esto solo cuando tengas certeza de que el vector gradiente que has especificado es correcto. 

<subhead>Nombres de parámetros</subhead> 

Al estimar un modelo no lineal, con frecuencia es conveniente nombrar los parámetros de forma sucinta. Ahora bien, al presentar los resultados, puede que desees utilizar etiquetas más informativas. Esto lo puedes lograr mediante la palabra clave adicional <@lit="param_names"> dentro del bloque de instrucciones. Para un modelo con <@mth="k"> parámetros, el argumento que sigue a esta palabra clave debe ser una cadena de texto literal entre comillas que contenga <@mth="k"> nombres separados por espacios, el nombre de una variable de cadena que contenga <@mth="k"> de esos nombres, o el nombre de un array con <@mth="k"> cadenas de texto. 

Para obtener otros detalles sobre la estimación MCNL (NLS), consulta <@pdf="El manual de gretl#chap:nls"> (Capítulo 25). 

Menú gráfico: /Modelo/Mínimos cuadrados no lineales

# normtest Tests

Argumento: 	<@var="serie"> 
Opciones: 	<@lit="--dhansen"> (Contraste de Doornik–Hansen, por defecto)
		<@lit="--swilk"> (Contraste de Shapiro–Wilk)
		<@lit="--lillie"> (Contraste de Lilliefors)
		<@lit="--jbera"> (Contraste de Jarque–Bera)
		<@lit="--all"> (Hace todos los contrastes)
		<@lit="--quiet"> (No presenta los resultados)

Realiza un contraste de Normalidad para la <@var="serie"> indicada. El tipo concreto de contraste se controla con el indicador de opción (y se ejecuta el contraste de Doornik–Hansen cuando no indicas ninguna opción). Advertencia: Los contrastes de Doornik–Hansen y Shapiro–Wilk son más recomendables que los otros, teniendo en cuenta sus mejores propiedades en muestras pequeñas. 

Mediante los accesores <@xrf="$test"> y <@xrf="$pvalue"> puedes recuperar el estadístico de contraste y su probabilidad asociada (valor p), respectivamente. Ten en cuenta que cuando indicas la opción <@opt="--⁠all">, el resultado guardado es el del contraste de Doornik–Hansen. 

Menú gráfico: /Variable/Contraste de Normalidad

# nulldata Dataset

Argumento: 	<@var="longitud"> 
Opción: 	<@lit="--preserve"> (Retiene las variables que no son series)
Ejemplo: 	<@lit="nulldata 500">

Establece un conjunto de datos “en blanco” que: incluye solo una constante más una variable índice, tiene periodicidad 1 y contiene el número de observaciones especificado en el argumento. Puedes utilizar esto con la intención de hacer simulaciones, pues funciones como <@lit="uniform()"> y <@lit="normal()"> generan series artificiales comenzando por el principio, para rellenar el conjunto de datos. Esta instrucción puede ser muy útil en combinación con <@lit="loop">. Consulta también la opción “seed” (semilla) de la instrucción <@ref="set">. 

Por defecto, esta instrucción vacía todos los datos del espacio vigente de trabajo de GRETL, no solo las series sino también las matrices, los escalares, las cadenas de texto, etc. Ahora bien, cuando indicas la opción <@opt="--⁠preserve">, se retiene cualquier variable que no sea una serie y esté definida en ese momento. 

Menú gráfico: /Archivo/Nuevo conjunto de datos

# ols Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--cluster">=<@var="clustvar"> (Desviaciones típicas agrupadas)
		<@lit="--jackknife"> (Mira abajo)
		<@lit="--simple-print"> (No presenta estadísticos auxiliares)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--anova"> (Presenta una tabla ANOVA)
		<@lit="--no-df-corr"> (Elimina la corrección de los grados de libertad)
		<@lit="--print-final"> (Mira abajo)
Ejemplos: 	<@lit="ols 1 0 2 4 6 7">
		<@lit="ols y 0 x1 x2 x3 --vcv">
		<@lit="ols y 0 x1 x2 x3 --quiet">

Calcula las estimaciones de mínimos cuadrados ordinarios (MCO u OLS) siendo <@var="depvar"> la variable dependiente, e <@var="indepvars"> una lista de variables independientes. Puedes especificar las variables con el nombre o con el número; y utilizar el número cero para indicar el término constante. 

Aparte de las estimaciones de los coeficientes y de las desviaciones típicas, el programa también presenta las probabilidades asociadas (valores p) a los estadísticos <@mth="t"> (con dos colas) y <@mth="F">. Un 'valor p' por debajo de 0.01 indica significación estadística a un nivel del 1 por ciento, y se marca con <@lit="***">. La marca <@lit="**"> indica niveles de significación entre 1 y 5 por ciento, y la marca <@lit="*"> indica niveles entre 5 y 10 por ciento. También se presentan los estadísticos para elegir modelos (el Criterio de Información de Akaike o AIC, y el Criterio de Información Bayesiano de Schwarz). La fórmula utilizada para el AIC es la proporcionada por <@bib="Akaike (1974);akaike74">, en concreto, menos dos veces el logaritmo de la verosimilitud maximizada más dos veces el número de parámetros estimados. 

Si indicas la opción <@opt="--⁠no-df-corr">, no se aplica la corrección habitual de los grados de libertad al calcular la varianza estimada de la perturbación (y por lo tanto, tampoco las desviaciones típicas de los estimadores de los parámetros). 

La opción <@opt="--⁠print-final"> es aplicable solo en el contexto de un bucle (<@ref="loop">), y dispone que la regresión se ejecute silenciosamente en todas las iteraciones del bucle, excepto en la última. Consulta <@pdf="El manual de gretl#chap:looping"> (Capítulo 13) para obtener más detalles. 

Puedes recuperar varias variables internas después de la estimación. Por ejemplo: 

<code>          
   series uh = $uhat
</code>

guarda los errores de la estimación bajo el nombre <@lit="uh">. Consulta la sección “Accesores” de la Guía de funciones de GRETL para obtener más detalles. 

Puedes ajustar la fórmula (versión “HC”) específica que se va a utilizar para generar las desviaciones típicas robustas cuando indicas la opción <@opt="--⁠robust">, mediante la instrucción <@ref="set">. La opción <@opt="--⁠jackknife"> tiene como consecuencia la selección de una <@lit="hc_version"> de <@lit="3a">. La opción <@opt="--⁠cluster"> anula la selección de la versión HC, y produce las desviaciones típicas robustas agrupando las observaciones según los distintos valores de <@var="clustvar">. Consulta <@pdf="El manual de gretl#chap:robust_vcv"> (Capítulo 22) para obtener más detalles. 

Menú gráfico: /Modelo/Mínimos Cuadrados Ordinarios
Otro acceso: Botón con el símbolo beta en la barra de herramientas

# omit Tests

Argumento: 	<@var="listavariables"> 
Opciones: 	<@lit="--test-only"> (No substituye el modelo vigente)
		<@lit="--chi-square"> (Devuelve la forma Chi-cuadrado del contraste de Wald)
		<@lit="--quiet"> (Presenta solo los resultados básicos del contraste)
		<@lit="--silent"> (No presenta nada)
		<@lit="--vcv"> (Presenta la matriz de covarianzas del modelo reducido)
		<@lit="--auto">[=<@var="alfa">] (Eliminación secuencial, mira abajo)
Ejemplos: 	<@lit="omit 5 7 9">
		<@lit="omit seasonals --quiet">
		<@lit="omit --auto">
		<@lit="omit --auto=0.05">
		Ver también <@inp="restrict.inp">, <@inp="sw_ch12.inp">, <@inp="sw_ch14.inp">

Esta instrucción debe ir después de una instrucción de estimación. En su forma básica, calcula el estadístico de contraste de Wald para la significación conjunta de las variables de <@var="listavariables">, que debe ser un subconjunto (aunque no necesariamente un subconjunto apropiado) de las variables independientes del último modelo estimado. Puedes recuperar los resultados del contraste utilizando los accesores <@xrf="$test"> y <@xrf="$pvalue">. 

A no ser que la restricción elimine todos los regresores originales, por defecto, se estima el modelo restringido y este substituye al original como “modelo vigente” si tienes intención, por ejemplo, de recuperar los errores con <@lit="$uhat"> o hacer contrastes posteriores. Puedes impedir este comportamiento mediante la opción <@opt="--⁠test-only">. 

Por defecto, se registra la forma <@mth="F"> del contraste de Wald; pero puedes utilizar la opción <@opt="--⁠chi-square"> para recoger la forma chi-cuadrado en su lugar. 

Si tanto estimas como representas el modelo restringido, la opción <@opt="--⁠vcv"> tiene el efecto de presentar su matriz de covarianzas; en otro caso, esta opción se ignora. 

Como alternativa, cuando indicas la opción <@opt="--⁠auto">, se lleva a cabo la eliminación secuencial por pasos. En cada etapa se excluye la variable ligada a la mayor probabilidad asociada (valor p), hasta que todas las que queden estén ligadas a valores p que no sean mayores que algún valor de corte. Por defecto, este es del 10 por ciento (con 2 colas) y puedes ajustarlo añadiendo “<@lit="=">”, y un valor entre 0 y 1 (sin espacios), como en el cuarto ejemplo de arriba. Si indicas <@var="listavariables">, este proceso se limita solo a las variables de la lista; en otro caso, todos los regresores aparte de la constante se tratan como candidatos a la exclusión. Ten en cuenta que las opciones <@opt="--⁠auto"> y <@opt="--⁠test-only"> no puedes combinarlas. 

Menú gráfico: Ventana de modelo: Contrastes/Omitir variables

# open Dataset

Argumento: 	<@var="nombrearchivo"> 
Opciones: 	<@lit="--quiet"> (No presenta la lista de las series)
		<@lit="--preserve"> (Retiene las variables que no son series)
		<@lit="--select">=<@var="seleccion"> (Leer solo las series indicadas, mira abajo)
		<@lit="--frompkg">=<@var="nombrepaquete"> (Mira abajo)
		<@lit="--all-cols"> (Mira abajo)
		<@lit="--www"> (Utiliza una base de datos del servidor de GRETL)
		<@lit="--odbc"> (Utiliza una base de datos ODBC)
		Mira abajo para opciones adicionales especiales
Ejemplos: 	<@lit="open data4-1">
		<@lit="open voter.dta">
		<@lit="open fedbog.bin --www">
		<@lit="open dbnomics">

Abre un archivo de datos o una base de datos (consulta <@pdf="El manual de gretl#chap:datafiles"> (Capítulo 4) para ver una explicación de esta distinción). Las consecuencias son algo diferentes en los dos casos. Cuando abres un <@itl="archivo de datos">, se lee su contenido en el espacio de trabajo de GRETL, substituyendo la base de datos vigente (si la hay). Para añadir datos al conjunto vigente, en lugar de sustituirlo, consulta <@ref="append"> o (para tener mayor flexibilidad) <@ref="join">. Cuando abres una <@itl="base de datos">, no se carga inmediatamente ningún dato; más bien, se establece la fuente para llamadas posteriores de la instrucción <@ref="data">, que se utiliza para importar series concretas. Para obtener más detalles respecto a las bases de datos, consulta la sección titulada “Abriendo una base de datos” más abajo. 

Si no indicas <@var="nombrearchivo"> con una ruta completa, GRETL busca en algunas rutas destacadas para tratar de encontrar el archivo, de las que el directorio vigente (<@ref="workdir">) es la primera elección. Si no indicas el sufijo en el nombre de archivo (como en el primer ejemplo de arriba), GRETL asume que es un archivo de datos propio con sufijo <@lit=".gdt">. Basándose en el nombre del archivo y varias reglas heurísticas, GRETL tratará de detectar el formato del archivo de datos (propio, texto plano, CSV, MS Excel, Stata, SPSS, etc.). 

Cuando se utiliza la opción <@opt="--⁠frompkg">, GRETL va a buscar el archivo especificado de datos en el subdirectorio asociado al paquete de funciones especificado por <@var="nombrepaquete">. 

Si el argumento <@var="nombrearchivo"> toma la forma de un identificador de recursos uniforme (URI) que comienza por <@lit="http://"> o por <@lit="https://">, entonces GRETL tratará de descargar el archivo de datos indicado, antes de abrirlo. 

Por defecto, al abrir un nuevo archivo de datos se vacía la sesión vigente de GRETL, lo que incluye la eliminación de todas las variables definidas, incluyendo matrices, escalares y cadenas de texto. Si quieres mantener las variables que tengas definidas en ese momento (que no sean series, pues estas se eliminan obligatoriamente), utiliza la opción <@opt="--⁠preserve">. 

<subhead>Archivos de hoja de cálculo</subhead> 

Al abrir un archivo de datos con formato de hoja de cálculo (Gnumeric, Open Document o MS Excel), puedes facilitar tres parámetros adicionales después del nombre del archivo. Primero, puedes escoger una hoja de cálculo concreta dentro del archivo. Esto se hace, bien indicando el número de hoja por medio de la sintaxis (e.g., <@opt="--⁠sheet=2">), o bien indicando el nombre de la hoja (si lo sabes) entre comillas, como en <@opt="--⁠sheet="MacroData""> pues, por defecto, se va a leer la primera hoja de cálculo del archivo. También puedes especificar un desplazamiento de columna y/o de fila dentro de la hoja de cálculo mediante, e.g., 

<code>          
   --coloffset=3 --rowoffset=2
</code>

lo que va a provocar que GRETL ignore las 3 primeras columnas y las 2 primeras filas. Por defecto, hay un desplazamiento de 0 en ambas dimensiones, es decir, se empieza a leer en la celda de arriba a la izquierda. 

<subhead>Archivos de texto delimitado</subhead> 

Con archivos de texto plano, GRETL habitualmente espera encontrar las columnas de datos delimitadas de algún modo estándar (en general mediante coma, tabulador, espacio, o punto y coma). Por defecto, GRETL busca en la primera columna las etiquetas o las fechas de las observaciones, si su encabezado está vacío o bien contiene una cadena de texto sugerente tal como “year”, “date” o “obs”. Puedes evitar que GRETL trate de forma especial la primera columna indicando la opción <@opt="--⁠all-cols">. 

<subhead>Texto de formato fijo</subhead> 

Un archivo de datos en texto con “formato fijo” es aquel que no tiene delimitadores de columna, pero en el que los datos se disponen de acuerdo con un conjunto conocido de especificaciones como, por ejemplo, “la variable <@mth="k"> ocupa 8 columnas comenzando en la columna 24”. Para leer ese tipo de archivos, debes añadir una cadena de texto con <@opt="--⁠fixed-cols="><@var="colspec">, donde <@var="colspec"> se compone de números enteros separados por comas. Estos enteros se interpretan como un conjunto de pares. El primer elemento de cada par denota una columna de inicio, medida en bytes desde el principio de la línea, en la que el 1 indica el primer byte; y el segundo elemento de cada par indica cuantos bytes se deben de leer para el campo indicado. Así, por ejemplo, si indicas 

<code>          
   open fixed.txt --fixed-cols=1,6,20,3
</code>

entonces GRETL va a leer 6 bytes comenzando en la columna 1 para la variable 1; y para la variable 2, va a leer 3 bytes comenzando en la columna 20. Las líneas que están en blanco, o que comienzan con <@lit="#"> se ignoran; pero en caso contrario se aplica el patrón de lectura de columnas, y cuando se encuentra algo distinto a un valor numérico válido, se muestra un fallo. Cuando se leen los datos satisfactoriamente, las variables se van a designar como <@lit="v1">, <@lit="v2">, etc. Está en manos del usuario el facilitar nombres con significado y/o descripciones, utilizando para ello las instrucciones <@ref="rename"> y/o <@ref="setinfo">. 

Por defecto, cuando importas un archivo que contiene series con valores en formato de cadena de texto, se abre una caja de texto mostrándote el contenido de <@lit="string_table.txt">, un archivo que contiene la correspondencia entre las cadenas y su codificación numérica. Puedes eliminar este proceder mediante la opción<@opt="--⁠quiet">. 

<subhead>Cargando series seleccionadas</subhead> 

El uso de <@lit="open"> con un archivo de datos como argumento (en contraposición al caso con una base de datos, mira abajo) generalmente implica cargar todas las series del archivo indicado. Sin embargo, solo en el caso de archivos originales de GRETL (<@lit="gdt"> e <@lit="gdtb">) es posible especificar un subconjunto de series a cargar, mediante su nombre. Esto se consigue mediante la opción <@opt="--⁠select">, lo que requiere un argumento adjunto con uno de estos formatos: el nombre de una única serie; una lista de nombres, separados mediante espacios y delimitados por comillas; o el nombre de un 'array' de cadenas de texto. Ejemplos: 

<code>          
   # Serie única
   open somefile.gdt --select=x1
   # Más de una serie
   open somefile.gdt --select="x1 x5 x27"
   # Método alternativo
   strings Sel = defarray("x1", "x5", "x27")
   open somefile.gdt --select=Sel
</code>

<subhead>Abriendo una base de datos</subhead> 

Como se comentó antes, puedes utilizar la instrucción <@lit="open"> para abrir un archivo con una base de datos, y a continuación leerlo con la instrucción <@ref="data">. Los tipos de archivos que se admiten son las bases de datos propias de GRETL, RATS 4.0 y PcGive. 

Además de la lectura de estos tipos de archivos en la máquina local, se admiten otros tres casos más. Primero, cuando indicas la opción <@lit="www">, GRETL va a tratar de acceder a una base de datos propia de GRETL con el nombre que proporciones, en el servidor de GRETL (por ejemplo, la base de datos <@lit="fedbog.bin"> con los tipos de interés de la Reserva Federal del tercer ejemplo que se ha indicado más arriba). En segundo lugar, puedes usar la instrucción “<@lit="open dbnomics">” para establecer que DB.NOMICS sea el origen para leer bases de datos; sobre esto consulta <@mnu="gretlDBN">. En tercer lugar, si indicas la opción <@opt="--⁠odbc">, GRETL va a tratar de acceder a un banco de datos ODBC. Esta opción se explica detalladamente en <@pdf="El manual de gretl#chap:odbc"> (Capítulo 42). 

Menú gráfico: /Archivo/Abrir archivo de datos
Otro acceso: Arrastrar un archivo de datos hasta la ventana principal de GRETL

# orthdev Transformations

Argumento: 	<@var="listavariables"> 

Aplicable solo con datos de panel. Se obtiene una serie con desviaciones ortogonales adelantadas para cada variable de <@var="listavariables"> y se guarda en una nueva variable con el prefijo<@lit="o_">. De este modo <@lit="orthdev x y"> genera las nuevas variables <@lit="o_x"> y <@lit="o_y">. 

Los valores se guardan un paso por delante de su localización temporal verdadera (es decir, <@lit="o_x"> en la observación <@mth="t"> va a contener la desviación que pertenece a <@mth="t"> – 1, hablando estrictamente). Esto es por compatibilidad con las primeras diferencias pues así se va a perder la primera observación de cada serie temporal, no la última. 

# outfile Printing

Variantes: 	<@lit="outfile"> <@var="nombrearchivo">
		<@lit="outfile"> <@lit="--buffer="><@var="strvar">
		<@lit="outfile"> <@lit="--tempfile="><@var="strvar">
Opciones: 	<@lit="--append"> (Añadir a un archivo, solo la primera variante)
		<@lit="--quiet"> (Mira abajo)
		<@lit="--buffer"> (Mira abajo)
		<@lit="--tempfile"> (Mira abajo)
		<@lit="--decpoint"> (Mira abajo)

La instrucción <@lit="outfile"> inicia un bloque con el que se desvía todo resultado a presentar, hacia un archivo o buffer (o, si lo deseas, simplemente se descarta). Dicho bloque se termina con la instrucción “<@lit="end outfile">”, y después de ella los resultados vuelven al cauce por defecto. 

<subhead>Desvío a un archivo señalado</subhead> 

La primera variante que se muestra abajo envía los resultados al archivo señalado por el argumento <@var="nombrearchivo">. Por defecto, se crea un nuevo archivo (o se sobrescribe uno ya existente). El archivo resultante se guarda en la carpeta <@ref="workdir"> de la configuración, vigente excepto que la cadena de texto <@var="nombrearchivo"> contenga una especificación completa de la ruta. Pero, si quieres añadir resultados a un archivo ya existente, utiliza la opción <@opt="--⁠append">. 

En el sencillo ejemplo que sigue, los resultados de una determinada regresión se escriben en el archivo señalado. 

<code>          
   open data4-10
   outfile regress.txt
     ols ENROLL 0 CATHOL INCOME COLLEGE
   end outfile
</code>

<subhead>Nombres de archivos ficticios especiales</subhead> 

Se admiten tres valores especiales para <@var="nombrearchivo">, del siguiente modo: 

<indent>
• <@lit="null">: Se suprime la presentación de resultados hasta que finalice el redireccionado. 
</indent>

<indent>
• <@lit="stdout">: El resultado se redirecciona al canal de “resultado típico”. 
</indent>

<indent>
• <@lit="stderr">: El resultado se redirecciona al canal de “error típico”. 
</indent>

<subhead>Desvío a un buffer de cadena</subhead> 

La opción <@opt="--⁠buffer"> se utiliza para guardar resultados en una variable de cadena. El parámetro que se necesita para esta opción debe ser el nombre de una variable de cadena ya existente, cuyo contenido se sobrescribirá. Abajo se muestra el mismo ejemplo indicado anteriormente, modificado para guardar una cadena. En este caso, al representar el contenido de <@lit="model_out"> se mostrarán los resultados redirigidos. 

<code>          
   open data4-10
   string model_out = ""
   outfile --buffer=model_out
     ols ENROLL 0 CATHOL INCOME COLLEGE
   end outfile
   print model_out
</code>

<subhead>Desvío a un archivo temporal</subhead> 

La opción <@opt="--⁠tempfile"> se utiliza para dirigir los resultados hacia un archivo temporal, con un nombre generado automáticamente que se garantiza que es único, en el directorio “punto” del usuario. Igual que en el caso del desvío a un buffer, el parámetro de opción debe ser el nombre de una variable de cadena: en este caso, su contenido se sobrescribe con el nombre del archivo temporal. Atención: los archivos que se guardan en el directorio 'punto', se van a depurar al salir del programa, por lo que no uses esta modalidad si deseas que los resultados se conserven después de tu sesión de GRETL. 

Repetimos el sencillo ejemplo de arriba, con un par de líneas extra para ilustrar la cuestión de que <@var="strvar"> te indica a donde van los resultados, y que puedes recuperarlos utilizando la función <@xrf="readfile">. 

<code>          
   open data4-10
   string mitemp
   outfile --tempfile=mitemp
     ols ENROLL 0 CATHOL INCOME COLLEGE
   end outfile
   printf "Los resultados se han dirigido a %s\n", mitemp
   printf "Los resultados fueron:\n%s\n", readfile(mitemp)
   # Limpiar cuando el archivo no se necesita más
   remove(mitemp)
</code>

En algunos casos, puedes querer ejercer un cierto control sobre el nombre del archivo temporal. Esto puedes hacerlo indicando una variable de cadena de texto que contenga seis <@lit="X"> consecutivas, como en 

<code>          
   string mitemp = "tmpXXXXXX.csv"
   outfile --tempfile=mitemp
   ...
</code>

En este caso, se va a sustituir <@lit="XXXXXX"> por una cadena de caracteres aleatorios que aseguren que el nombre del archivo es único, pero se preservará el sufijo “<@lit=".csv">”. Al igual que en el caso más simple de arriba, el archivo se escribe de forma automática en el directorio “dot” del usuario, y se modifica el contenido de la variable de cadena expresada mediante el indicador opcional, para mantener la ruta completa al archivo temporal. 

<subhead>Discreción</subhead> 

Los efectos de la opción <@opt="--⁠quiet"> son: se desactiva que se vuelvan a presentar las órdenes de instrucción, y se muestran los mensajes auxiliares mientras los resultados estén redirigidos. Es equivalente a hacer 

<code>          
   set echo off
   set messages off
</code>

excepto que, cuando finaliza la redirección, se restablecen los valores originales de las variables <@lit="echo"> y <@lit="messages">. Esta opción está disponible en todo caso. 

<subhead>Carácter decimal</subhead> 

El efecto de la opción <@opt="--⁠decpoint"> es asegurar que el carácter de punto decimal (en contraposición a la coma) está vigente mientras la salida de resultados está desviada. Cuando el bloque <@lit="outfile"> finaliza, el carácter decimal vuelve al estado en que estaba antes de el. Esta opción resulta especialmente útil cuando el archivo texto que se va a crear, está destinado a ser una entrada para algún otro programa que requiera que los dígitos sigan las convenciones del inglés, como podría ser el caso de un guion de Gnuplot o de R, por ejemplo. 

<subhead>Niveles de redirección</subhead> 

En general, solo puedes abrir un archivo de este modo en un momento dado, por lo que las llamadas a esta instrucción no pueden anidarse. Sin embargo, la utilización de esta instrucción se permite dentro de funciones definidas por el usuario (siempre que el archivo de resultados se cierre desde dentro de la misma función) de forma que puedes desviar esos resultados temporalmente, y después devolverlos a un archivo de resultados original en caso de que <@lit="outfile"> esté en uso en ese momento por el solicitante. Por ejemplo, el código 

<code>          
   function void f (string s)
       outfile interno.txt
         print s
       end outfile
   end function

   outfile externo.txt --quiet
     print "Fuera"
     f("Dentro")
     print "De nuevo fuera"
   end outfile
</code>

producirá un archivo llamado “externo.txt” que contiene las dos líneas 

<code>          
   Fuera
   De nuevo fuera
</code>

y un archivo llamado “interno.txt” que contiene la línea 

<code>          
   Dentro
</code>

# panel Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--fixed-effects"> (Estima con efectos fijos por grupo)
		<@lit="--random-effects"> (Modelo de efectos aleatorios o MCG (GLS))
		<@lit="--nerlove"> (Utiliza la transformación de Nerlove)
		<@lit="--pooled"> (Estima mediante MCO combinados)
		<@lit="--between"> (Estima el modelo entre-grupos)
		<@lit="--robust"> (Desviaciones típicas robustas; mira abajo)
		<@lit="--cluster">=<@var="cvar"> (Desviaciones típicas conglomeradas; mira abajo)
		<@lit="--time-dummies"> (Incluye variables ficticias temporales)
		<@lit="--unit-weights"> (Mínimos Cuadrados Ponderados)
		<@lit="--iterate"> (Estimación iterativa)
		<@lit="--matrix-diff"> (Calcula el contraste de Hausman mediante la matriz-diferencia)
		<@lit="--unbalanced">=<@var="método"> (Solo efectos aleatorios; mira abajo)
		<@lit="--quiet"> (Resultados menos detallados)
		<@lit="--verbose"> (Resultados más detallados)
Ejemplos: 	<@inp="penngrow.inp">, <@inp="panel-robust.inp">

Estima un modelo de panel. Por defecto, se utiliza el estimador de efectos fijos; esto se pone en práctica restándoles las medias de grupo o unidad, a los datos originales. 

Cuando indicas la opción <@opt="--⁠random-effects">, se calculan las estimaciones de efectos aleatorios, utilizando por defecto el método de <@bib="Swamy y Arora (1972);swamy72">. Únicamente en este caso, la opción <@opt="--⁠matrix-diff"> fuerza el uso del método de la matriz-diferencia (en contraposición al método de regresión) para llevar a cabo el contraste de Hausman sobre la consistencia del estimador de efectos aleatorios. También es específica del estimador de efectos aleatorios, la opción <@opt="--⁠nerlove"> que escoge el método de <@bib="Nerlove (1971);nerlove71"> en contraposición al de Swamy y Arora. 

Como alternativa, cuando indicas la opción <@opt="--⁠unit-weights">, el modelo se estima mediante mínimos cuadrados ponderados, con las ponderaciones basadas en la varianza residual para las unidades respectivas de sección cruzada de la muestra. Únicamente en este caso, puedes añadir la opción <@opt="--⁠iterate"> para generar estimaciones iterativas y, si la iteración converge, las estimaciones resultantes son Máximo Verosímiles. 

Como alternativa posterior, si indicas la opción <@opt="--⁠between">, se estima el modelo entre-grupos (es decir, se hace una regresión MCO utilizando las medias de los grupos). 

El procedimiento por defecto para calcular Desviaciones típicas robustas en modelos con datos de panel, es el estimador HAC de <@bib="Arellano (2003);arellano03"> (conglomerado por unidad de panel). Las alternativas son las “Desviaciones Típicas Corregidas de Panel” (<@bib="Beck y Katz, 1995;beck-katz95">) y las desviaciones típicas “Consistentes de Correlación Espacial” (<@bib="Driscoll y Kraay, 1998;driscoll_kraay98">). Estas se pueden seleccionar mediante la instrución <@lit="set panel_robust"> con los argumentos <@lit="pcse"> y <@lit="scc">, respectivamente. Otras alternativas a estas tres opciones están disponibles mediante la opción <@opt="--⁠cluster">; por favor, consulta <@pdf="El manual de gretl#chap:robust_vcv"> (Capítulo 22) para tener más detalles. Si se especifican las desviaciones típicas robustas, se ejecuta el contraste conjunto <@mth="F"> sobre los efectos fijos utilizando el método robusto de <@bib="Welch (1951);welch51">. 

La opción <@opt="--⁠unbalanced"> está disponible solo para modelos con efectos aleatorios, y puedes usarla para elegir el método ANOVA que emplear con un panel desequilibrado. Por defecto, GRETL emplea el método de Swamy–Arora igual que se hace para los paneles equilibrados, excepto que utiliza la media armónica de las longitudes de las series de tiempo individuales en lugar de la <@mth="T"> habitual. Bajo esta opción puedes especificar, bien <@lit="bc"> para usar el método de <@bib="Baltagi y Chang (1994);baltagi-chang94">, o bien usar <@lit="stata"> para emular la opción <@lit="sa"> de la instrucción <@lit="xtreg"> de Stata. 

Para obtener más detalles sobre la estimación de un panel, consulta <@pdf="El manual de gretl#chap:panel"> (Capítulo 23). 

Menú gráfico: /Modelo/Panel

# panplot Graphs

Argumento: 	<@var="vardibujar"> 
Opciones: 	<@lit="--means"> (Serie temporal con medias de grupo)
		<@lit="--overlay"> (Gráfico por grupo, superpuestos, N <= 130)
		<@lit="--sequence"> (Gráfico por grupo, en secuencia, N <= 130)
		<@lit="--grid"> (Gráfico por grupo, en cuadrícula, N <= 16)
		<@lit="--stack"> (Gráfico por grupo, apilados, N <= 6)
		<@lit="--boxplots"> (Gráfico de caja por grupo, en secuencia, N <= 150)
		<@lit="--boxplot"> (Gráfico único de caja, todos los grupos)
		<@lit="--output">=<@var="nombrearchivo"> (Enviar el resultado a un archivo específico)
Ejemplos: 	<@lit="panplot x --overlay">
		<@lit="panplot x --means --output=display">

Instrucción de dibujo específica para datos de panel: la serie <@var="vardibujar"> se dibuja del modo que se especifica con alguna de las opciones. 

Además de las opciones <@opt="--⁠means"> y <@opt="--⁠boxplot">, el gráfico representa explícitamente las variaciones en las dos dimensiones, la de serie temporal y la de sección cruzada. Tales gráficos están limitados en lo que se refiere al número de grupos (o también conocidos como individuos o unidades) en el rango de la muestra vigente del panel. Por ejemplo, la opción <@opt="--⁠overlay">, que presenta una serie temporal para cada grupo en un único gráfico, solo está disponible si el número de grupos, <@mth="N">, es menor o igual a 130. (De otro modo, el gráfico llegaría a ser demasiado denso para resultar instructivo.) Si un panel es demasiado largo para permitir la especificación gráfica deseada, puedes escoger provisionalmente un rango reducido de grupos o de unidades, como en 

<code>          
   smpl 1 100 --unit
   panplot x --overlay
   smpl full
</code>

Puedes usar la opción <@opt="--⁠output="><@var="nombrearchivo"> para controlar la forma y el destino del resultado; consulta la instrucción <@ref="gnuplot"> para obtener más detalles. 

Otro acceso: Ventana principal: Menú emergente (selección única)

# panspec Tests

Opciones: 	<@lit="--nerlove"> (Utiliza el método de Nerlove para efectos aleatorios)
		<@lit="--matrix_diff"> (Utiliza el método de la matriz-diferencia para el contraste de Hausman)
		<@lit="--quiet"> (Suprime la presentación de resultados)

Esta instrucción está disponible únicamente después de estimar un modelo con datos de panel utilizando MCO (consulta también <@lit="setobs">). Contrasta la especificación combinada simple frente a las principales alternativas, la de efectos fijos y la de efectos aleatorios. 

La especificación de efectos fijos permite que la ordenada en el origen de la regresión varíe de una unidad de sección cruzada a otra. Se presenta un contraste <@mth="F"> de Wald para la hipótesis nula de que las ordenadas en el origen no difieren. La especificación de efectos aleatorios descompone la varianza de cada perturbación en dos partes, una parte específica de la unidad de sección cruzada y otra parte específica de cada observación concreta. (Se puede calcular este estimador solo cuando el número de unidades de sección cruzada en el conjunto de datos supera al número de parámetros a estimar.) El estadístico de Multiplicadores de Lagrange de Breusch–Pagan contrasta la hipótesis nula de que MCO combinados es adecuado frente a la alternativa de efectos aleatorios. 

MCO combinados pueden rechazarse frente a ambas alternativas. Mientras la perturbación específica por unidad o grupo no esté correlacionada con las variables independientes, el estimador de efectos aleatorios será más eficiente que el de efectos fijos; en otro caso, el estimador de efectos aleatorios será inconsistente y serán preferibles los efectos fijos. La hipótesis nula del contraste de Hausman indica que la perturbación específica de grupo <@itl="no"> está así correlacionada (y por eso se prefiere el estimador de efectos aleatorios). Un valor bajo de la probabilidad asociada (valor p) al estadístico de este contraste va en contra de los efectos aleatorios y a favor de los efectos fijos. 

Las dos primeras opciones de esta instrucción se corresponden con la estimación de efectos aleatorios. Por defecto, se utiliza el método de Swamy y Arora, mediante el cálculo del estadístico de contraste de Hausman, utilizando el método de regresión. Las opciones permiten utilizar el estimador alternativo de la varianza de Nerlove, y /o la aproximación de la matriz-diferencia al estadístico de Hausman. 

Cuando se completa con éxito, los accesores <@xrf="$test"> y <@xrf="$pvalue"> proporcionan 3 vectores que contienen los estadísticos de prueba y los valores p para los tres contrastes indicados arriba: combinabilidad (Wald), combinabilidad (Breusch–Pagan), y Hausman. Si solo quieres los resultados de esta forma, puedes indicar la opción <@opt="--⁠quiet"> para saltarte la presentación de resultados. 

Ten en cuenta que luego de estimar la especificación de efectos aleatorios con la instrucción <@lit="panel"> , el test de Hausman se ejecuta automáticamente y puedes recuperar los resultados mediante el accesor <@xrf="$hausman">. 

Menú gráfico: Ventana de modelo: Contrastes/Especificaciones de panel

# pca Statistics

Argumento: 	<@var="listavariables"> 
Opciones: 	<@lit="--covariance"> (Utiliza la matriz de covarianzas)
		<@lit="--save">[=<@var="n">] (Guarda las componentes más importantes)
		<@lit="--save-all"> (Guarda todas las componentes)
		<@lit="--quiet"> (No presenta los resultados)

Análisis de Componentes Principales. Excepto cuando indicas la opción <@opt="--⁠quiet">, presenta los valores propios de la matriz de correlaciones (o de la matriz de covarianzas cuando indicas la opción <@opt="--⁠covariance">) para las variables que forman <@var="listavariables">, junto con la proporción de la varianza conjunta representada por cada componente. También presenta los correspondientes autovectores o “pesos de las componentes”. 

Si indicas la opción <@opt="--⁠save-all">, entonces se guardan todas las componentes como series en el conjunto de datos, con los nombres <@lit="PC1">, <@lit="PC2">, etcétera. Estas variables artificiales se forman como la suma de los productos de (el peso de la componente) por (<@mth="X"><@sub="i"> tipificada), donde <@mth="X"><@sub="i"> denota la variable <@mth="i">-ésima de <@var="listavariables">. 

Si indicas la opción <@opt="--⁠save"> sin un valor del parámetro, se guardan las componentes con valores propios mayores que la media (lo que significa mayores que 1.0 cuando el análisis se basa en la matriz de correlaciones) en el conjunto de datos, tal como se describió arriba. Si indicas un valor para <@var="n"> con esta opción, entonces se guardan las <@var="n"> componentes más importantes. 

Consulta también la función <@xrf="princomp">. 

Menú gráfico: /Ver/Componentes principales

# pergm Statistics

Argumentos: 	<@var="serie"> [ <@var="anchobanda"> ] 
Opciones: 	<@lit="--bartlett"> (Utiliza la ventana de retardo de Bartlett)
		<@lit="--log"> (Utiliza la escala logarítmica)
		<@lit="--radians"> (Muestra la frecuencia en radianes)
		<@lit="--degrees"> (Muestra la frecuencia en grados)
		<@lit="--plot">=<@var="modo-o-nombrearchivo"> (Mira abajo)
		<@lit="--silent"> (Omite la presentación del resultado)

Calcula y muestra el espectro de la serie especificada. Por defecto, se indica el periodograma de la muestra, pero se utiliza opcionalmente una ventana de retardo de Bartlett al estimar el espectro, (consulta por ejemplo, el libro de Greene <@itl="Econometric Analysis"> para ver una discusión sobre esto). La anchura por defecto de la ventana de Bartlett es de dos veces la raíz cuadrada del tamaño de la muestra, pero puedes establecer esto de modo manual utilizando el parámetro <@var="anchobanda">, hasta un máximo de la mitad del tamaño de la muestra. 

Cuando indicas la opción <@opt="--⁠log">, se representa el espectro en una escala logarítmica. 

Las opciones (mutuamente excluyentes) <@opt="--⁠radians"> y <@opt="--⁠degrees"> afectan al aspecto del eje de frecuencias cuando se dibuja el periodograma. Por defecto, la frecuencia se escala por el número de períodos de la muestra, pero esas dos opciones provocan que el eje se etiquete desde 0 hasta π radianes o desde 0 a 180°, respectivamente. 

Por defecto, si GRETL no está en modo de procesamiento por lotes, se muestra un gráfico del periodograma. Puedes ajustar esto mediante la opción <@opt="--⁠plot">. Los parámetros admisibles para esta opción son <@lit="none"> (para suprimir el gráfico), <@lit="display"> (para representar un gráfico incluso en modo de procesamiento por lotes), o un nombre de archivo. El efecto de indicar un nombre de archivo es como se describe para la opción <@opt="--⁠output"> de la instrucción <@ref="gnuplot">. 

Menú gráfico: /Variable/Periodograma
Otro acceso: Ventana principal: Menú emergente (selección única)

# pkg Utilities

Argumentos: 	<@var="acción"> <@var="nombrepaquete"> 
Opciones: 	<@lit="--local"> (Instala desde un archivo local)
		<@lit="--quiet"> (Mira abajo)
		<@lit="--verbose"> (Mira abajo)
		<@lit="--staging"> (Mira abajo)
Ejemplos: 	<@lit="pkg install armax">
		<@lit="pkg install /path/to/myfile.gfn --local">
		<@lit="pkg query ghosts">
		<@lit="pkg run-sample ghosts">
		<@lit="pkg unload armax">

Esta instrucción proporciona una manera de instalar, consultar, descargar, eliminar o indexar paquetes de funciones de GRETL. El argumento <@var="acción"> debe ser alguno de entre <@lit="install">, <@lit="query">, <@lit="run‑sample">, <@lit="unload">, <@lit="remove"> o <@lit="index">, respectivamente. Una extensión para dar soporte a paquetes de archivos de datos se describe más abajo. 

<@lit="install">: En su forma más elemental, sin ningún indicador de opción y con el argumento <@var="nombrepaquete"> expresado como el nombre “plano” de un paquete de funciones de GRETL (como en el primer ejemplo de arriba), el efecto de esta opción consiste en descargar el paquete que se especifica del servidor de GRETL, (excepto que <@var="nombrepaquete"> comience con <@lit="http://">), e instalarlo en la máquina local. En este caso, no es necesario expresar una extensión en el nombre del archivo. Sin embargo, cuando indicas la opción <@opt="--⁠local">, el argumento <@var="nombrepaquete"> debe ser la ruta a un archivo de paquete en la máquina local, que todavía no esté instalado, y expresado con una extensión correcta (<@lit=".gfn"> o <@lit=".zip">). En este caso, el efecto consiste en copiar el archivo en su sitio (<@lit="gfn">), o descomprimirlo en su sitio (<@lit="zip">), significando “en su sitio” allí donde lo va a encontrar la instrucción <@ref="include"> . 

<@lit="query">: Por defecto, la consecuencia de esta opción es la presentación de información básica sobre el paquete especificado (autor, versión, etc.). Si el paquete incluye recursos extra (archivos de datos y/o guiones adicionales) se incluye un listado de esos archivos. Si añades la opción <@opt="--⁠quiet">, no se presenta nada; en su lugar, se guarda la información del paquete en forma de un 'bundle' de GRETL, al que se puede acceder mediante <@xrf="$result">. Si no se puede encontrar ninguna información, este 'bundle' estará vacío. 

<@lit="run-sample">: Proporciona un recurso mediante linea de instrucciones para ejecutar el guion de prueba incluido en el paquete especificado. 

<@lit="unload">: Debes indicar el argumento <@var="pkgname"> en modo 'plano', sin ruta ni extensión, como en el último ejemplo de arriba. La consecuencia de esto es la descarga de ese paquete en cuestión de la memoria de GRETL (si está cargado en ese momento), y también eliminarlo del menú de la Interfaz Gráfica (GUI) al que esté añadido, si lo está a alguno. 

<@lit="remove">: Realiza las acciones indicadas para <@lit="unload"> y, además, elimina del disco el(los) archivo(s) asociado(s) con el paquete indicado. 

<@lit="index">: Es un caso especial en el que <@var="nombrepaquete"> debe sustituirse por la palabra clave “<@lit="addons">”: la consecuencia de ello es que se actualiza el índice de los paquetes estándar que se conocen como “Complementos”. Esa actualización se realiza automáticamente de vez en cuando, pero en algún caso puede resultar útil una actualización manual. En tal caso, la opción <@opt="--⁠verbose"> provoca un resultado impreso sobre donde ha hecho GRETL la búsqueda, y lo que ha encontrado. Siendo claros, aquí tienes un modo de conseguir un resultado con el índice completo: 

<code>          
   pkg index addons --verbose
</code>

<subhead>Paquetes de archivos de datos</subhead> 

Además de su utilización con paquetes de funciones, la instrucción <@lit="pkg install"> también se puede usar con paquetes de archivos de datos de tipo <@lit="tar.gz"> como se indican en el listado de <@url="https://gretl.sourceforge.net/gretl_data.html">. Por ejemplo, para instalar los archivos de datos de Verbeek, se puede hacer 

<code>          
   pkg install verbeek.tar.gz
</code>

Ten en cuenta que <@lit="install"> es la única operación admitida para esos archivos. 

<subhead>Montaje</subhead> 

La opción <@opt="--⁠staging"> es un elemento de conveniencia para los desarrolladores y está disponible únicamente en conjunción con la acción <@lit="install"> cuando se aplica a un paquete de función. Su efecto consiste en que se descarga el paquete en cuestión del área de montaje (<@lit="staging">) de Sourceforge en lugar de hacerlo de un área pública. Los paquetes en montaje todavía no están aprobados para uso general; por eso, ignora esta opción a no ser que sepas lo que estás haciendo. 

Menú gráfico: /Archivo/Paquetes de funciones/Sobre servidor

# plot Graphs

Argumento: 	[ <@var="datos"> ] 
Opciones: 	<@lit="--output">=<@var="nombrearchivo"> (Envía el resultado al archivo indicado)
		<@lit="--outbuf">=<@var="nomecadea"> (Envía el resultado a la cadena de texto indicada)
Ejemplos: 	<@inp="nile.inp">

El bloque <@lit="plot"> proporciona una alternativa a la instrucción <@ref="gnuplot"> que puede ser más conveniente cuando estás generando un gráfico complicado (con varias opciones y/o instrucciones Gnuplot para que se inserten en el archivo gráfico). Además de la siguiente explicación, por favor, consulta también <@pdf="El manual de gretl#chap:graphs"> (Capítulo 6) para ver otros ejemplos. 

Un bloque de tipo <@lit="plot"> comienza con la palabra de instrucción <@lit="plot">. Habitualmente va seguida de un argumento de <@var="datos"> que especifica los datos que se van a representar, y que debe indicar el nombre de una lista, de una matriz o de una única serie. Si no especificas datos de entrada, el bloque debe contener en su lugar al menos una directriz para dibujar una fórmula; ese tipo de directivas puedes indicarlas por medio de lineas de tipo <@lit="literal"> o <@lit="printf"> (mira más abajo). 

Cuando indicas una lista (o una matriz), se asume que el último término (o la última columna de la matriz) es la variable del eje <@mth="x"> y que los(as) otros(as) son las variables del eje <@mth="y">, excepto cuando indicas la opción <@opt="--⁠time-series">, en cuyo caso todos los datos especificados van en el eje <@mth="y">. La opción de proporcionar el nombre de una sola serie se restringe a los datos de series temporales, en cuyo caso se asume que quieres un gráfico de series temporales; en otro caso, se muestra un fallo. 

La línea de comienzo se puede preceder de la expresión “<@var="savename"> <@lit="<-">” para que se guarde un gráfico como icono en el programa de Interfaz Gráfica de Usuario (GUI). El bloque acaba con <@lit="end plot">. 

Dentro del bloque tienes cero o más líneas de los siguientes tipos, identificadas por la palabra clave inicial: 

<indent>
• <@lit="option">: Especifica una opción simple. 
</indent>

<indent>
• <@lit="options">: Especifica múltiples opciones en una sola línea, separadas por espacios. 
</indent>

<indent>
• <@lit="literal">: Una instrucción que se va a pasar literalmente a Gnuplot. 
</indent>

<indent>
• <@lit="printf">: Un enunciado printf cuyo resultado se pasará literalmente a Gnuplot. 
</indent>

Ten en cuenta que al lado de las opciones <@opt="--⁠output"> y <@opt="--⁠outbuf"> (que se deben añadir a la línea con que acaba el bloque), todas las opciones que admite la instrucción <@ref="gnuplot"> también las admite la instrucción <@lit="plot">, pero deben indicarse dentro del bloque, utilizando la sintaxis descrita más arriba. En este contexto, no es necesario proporcionar el habitual doble guion antes del indicador de opción. Para obtener más detalles sobre los efectos de las distintas opciones, consulta <@ref="gnuplot">. 

La intención de utilizar el bloque <@lit="plot"> se ilustra mejor con el ejemplo: 

<code>          
   string title = "Mi título"
   string xname = "Mi variable X"
   plot plotmat
       options with-lines fit=none
       literal set linetype 3 lc rgb "#0000ff"
       literal set nokey
       printf "set title '%s'", title
       printf "set xlabel '%s'", xname
   end plot --output=display
</code>

Este ejemplo asume que <@lit="plotmat"> es el nombre de una matriz que tiene 2 columnas por lo menos (o una lista que tiene 2 elementos por lo menos). Ten en cuenta que se considera una buena praxis colocar (únicamente) la opción <@opt="--⁠output"> en la última línea del bloque; otras opciones deberías colocarlas dentro del bloque. 

<subhead>Dibujar un gráfico sin datos</subhead> 

El seguiente ejemplo muestra un caso de cómo especificar la representación de una gráfica sin ter una fuente de datos. 

<code>          
   plot
       literal set title 'Utilidad CRRA'
       literal set xlabel 'c'
       literal set ylabel 'u(c)'
       literal set xrange[1:3]
       literal set key top left
       literal crra(x,s) = (x**(1-s) - 1)/(1-s)
       printf "plot crra(x, 0) t 'sigma=0', \\"
       printf " log(x) t 'sigma=1', \\"
       printf " crra(x,3) t 'sigma=3"
   end plot --output=display
</code>

# poisson Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> [ ; <@var="exposición"> ] 
Opciones: 	<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--cluster">=<@var="clustvar"> (Consulta <@ref="logit"> para más explicaciones)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--quiet"> (No presenta los resultados)
Ejemplos: 	<@lit="poisson y 0 x1 x2">
		<@lit="poisson y 0 x1 x2 ; S">
		Ver también <@inp="camtriv.inp">, <@inp="greene19_3.inp">

Estima una regresión de Poisson. Se coge la variable dependiente para representar el acaecimiento de sucesos de algún tipo, y debe tener solo valores enteros no negativos. 

Si una variable aleatoria discreta <@mth="Y"> sigue una distribución de Poisson, entonces 

  <@fig="poisson1">

para <@mth="y"> = 0, 1, 2,…. La media y la varianza de la distribución son ambas iguales a <@mth="v">. En el modelo de regresión de Poisson, el parámetro <@mth="v"> está representado como una función de una o más variables independientes. La versión más habitual (y la única que admite GRETL) cumple 

  <@fig="poisson2">

o, en otras palabras, el logaritmo de <@mth="v"> es una función lineal de las variables independientes. 

Como opción, puedes añadir una variable de exposición (“offset”) a la especificación. Esta es una variable de escala, y su logaritmo se añade a la función lineal de regresión (implícitamente, con un coeficiente de 1.0). Esto tiene sentido si esperas que el número de ocurrencias del evento en cuestión es proporcional (manteniéndose lo demás constante) a algún factor conocido. Por ejemplo, puedes suponer que el número de accidentes de tráfico es proporcional al volumen de tráfico (manteniéndose lo demás constante) y, en ese caso, el volumen de tráfico puede expresarse como una variable “de exposición” en un modelo de Poisson del cociente de accidentes. La variable de exposición debe ser estrictamente positiva. 

Por defecto, se calculan las desviaciones típicas utilizando la inversa negativa de la matriz Hessiana. Si especificas la opción <@opt="--⁠robust">, entonces se calculan en su lugar las desviaciones típicas CMV (QML) o de Huber–White. En este caso, la matriz de covarianzas estimada es un “emparedado” entre la inversa de la matriz Hessiana estimada y el producto externo del vector gradiente. 

Consulta también <@ref="negbin">. 

Menú gráfico: /Modelo/Variable dependiente limitada/Datos de conteo

# print Printing

Variantes: 	<@lit="print"> <@var="listavariables">
		<@lit="print">
		<@lit="print"> <@var="nombresobjetos">
		<@lit="print"> <@var="cadenaliteral">
Opciones: 	<@lit="--byobs"> (Por observaciones)
		<@lit="--no-dates"> (Utiliza números de observación simples)
		<@lit="--range">=<@var="inicio:parada"> (Mira abajo)
		<@lit="--midas"> (Mira abajo)
		<@lit="--tree"> (Específico para bundles; mira abajo)
Ejemplos: 	<@lit="print x1 x2 --byobs">
		<@lit="print my_matrix">
		<@lit="print "Esto es una cadena"">
		<@lit="print my_array --range=3:6">
		<@lit="print hflist --midas">

Ten en cuenta que <@lit="print"> es más bien una instrucción “básica” (con la intención principal de presentar los valores de las series). Consulta <@ref="printf"> y <@ref="eval"> para otras alternativas más avanzadas y menos restrictivas. 

En la primera variante mostrada arriba (consulta el primer ejemplo también), <@var="listavariables"> debe ser una lista de series (bien una lista ya definida, o bien una lista especificada mediante los nombres o números ID de las series, separados por espacios). En este caso, esta instrucción presenta los valores de las series de la lista. Por defecto, los datos se presentan “por variable”, pero si añades la opción <@opt="--⁠byobs"> se presentan por observación. Cuando se presentan por observación, por defecto se muestra la fecha (con datos de series temporales) o la cadena de texto de la etiqueta de observación (en caso de que lo haya) al comienzo de cada línea. Mediante la opción <@opt="--⁠no-dates"> se elimina la presentación de las fechas o de las etiquetas; en su lugar se muestra un simple número de observación. Consulta el párrafo final de estos comentarios para ver el efecto de la opción <@opt="--⁠midas"> (que se aplica solo a una lista ya definida de series). 

Cuando no indicas ningún argumento (la segunda variante mostrada arriba) entonces el efecto es similar al primer caso, excepto que se van a presentar <@itl="todas"> las series del conjunto vigente de datos. Las opciones que se admiten son como se describieron más arriba. 

La tercera variante (con el argumento <@var="nombresobjetos">; mira el segundo ejemplo) espera una lista de nombres, separados por espacios, de objetos básicos de GRETL que no sean series (escalares, matrices, cadenas de texto, bundles, arrays); y se muestra el valor de estos objetos. En el caso de los 'bundles', sus componentes se ordenan por tipo y alfabéticamente. 

En la cuarta forma (tercer ejemplo), <@var="cadenaliteral"> debe ser una cadena de texto puesta entre comillas (y no debe haber nada más siguiendo a la línea de instrucción). Se presenta la cadena de texto en cuestión, seguida de un carácter de línea nueva. 

Puedes utilizar la opción <@opt="--⁠range"> para controlar el volumen de información que se presenta. Los valores (enteros) de los marcadores de <@var="inicio"> y <@var="parada"> pueden referirse a observaciones de series y de listas, a filas de matrices, a elementos de 'arrays', y a líneas de cadenas de texto. En todos los casos, el valor mínimo de <@var="inicio"> es 1, y el máximo valor de <@var="parada"> es el “tamaño en forma de filas” del objeto en cuestión. Los valores negativos de estos marcadores se usan para disponer un recuento hacia atrás, desde el final. Puedes indicar estos marcadores en formato numérico, o mediante nombres de variables escalares previamente definidas. Si omites <@var="inicio">, se considera implícitamente igual a 1; y si omites <@var="parada">, eso significa ir hasta el final del todo. Con series y listas, ten en cuenta que los marcadores se refieren al rango muestral vigente. 

La opción <@opt="--⁠tree"> es específica para presentar un bundle de GRETL. El efecto de ello es que, si el paquete especificado contiene otros bundles o arrays de ellos, se presentan sus contenidos. De lo contrario, solo se presentan los elementos del nivel superior del bundle. 

La opción <@opt="--⁠midas"> es especial para presentar una lista de series y, más aún, es específica para conjuntos de datos que contienen una o más series de alta frecuencia, cada una representada por una <@ref="MIDAS_list">. Cuando indicas una de esas listas como argumento y agregas esta opción, la serie se presenta por observación de su frecuencia “original”. 

Menú gráfico: /Datos/Mostrar valores

# printf Printing

Argumentos: 	<@var="formato"> <@lit=", "><@var="elementos"> 

Presenta valores escalares, series, matrices o cadenas de texto bajo el control de una cadena de texto para dar formato (ofreciendo una parte de la función <@lit="printf"> del lenguaje de programación C). Los formatos numéricos reconocidos son <@lit="%e">, <@lit="%E">, <@lit="%f">, <@lit="%g">, <@lit="%G">, <@lit="%d"> y <@lit="%x">, en cada caso con los diversos reguladores disponibles en C. Ejemplos: el formato <@lit="%.10g"> presenta un valor con 10 cifras significativas, y <@lit="%12.6f"> presenta un valor con un ancho de 12 caracteres de los que 6 son decimales. Sin embargo, ten en cuenta que en GRETL el formato <@lit="%g"> es una buena elección por defecto para todos los valores numéricos, y no tienes necesidad de complicarte demasiado. Debes utilizar el formato <@lit="%s"> para las cadenas de texto. 

La propia cadena de formato debe estar puesta entre comillas, y los valores que se van a presentar deben ir después de esa cadena de formato, separados por comas. Estos valores deben tener la forma de, o bien (a) los nombres de las variables, o bien (b) expresiones que generen alguna clase de resultado que sea presentable, o bien (c) las funciones especiales <@lit="varname()"> o <@lit="date()">. El siguiente ejemplo presenta los valores de dos variables, más el de una expresión que se calcula: 

<code>          
   ols 1 0 2 3
   scalar b = $coeff[2]
   scalar se_b = $stderr[2]
   printf "b = %.8g, Desviación típica %.8g, t = %.4f\n",
          b, se_b, b/se_b
</code>

Las siguientes líneas ilustran el uso de las funciones 'varname' y 'date', que presentan respectivamente el nombre de una variable (indicado por su número ID) y una cadena de texto con una fecha (dada por un número natural positivo que indica una observación). 

<code>          
   printf "El nombre de la variable %d es %s\n", i, varname(i)
   printf "La fecha de la observación %d es %s\n", j, date(j)
</code>

Cuando indicas un argumento matricial asociado a un formato numérico, se presenta la matriz entera utilizando el formato especificado para cada elemento. El mismo se aplica a las series, excepto que el rango de valores presentados se rige por la configuración vigente de la muestra. 

La longitud máxima de una cadena de formato es de 127 caracteres. Se reconocen las secuencias de escape <@lit="\n"> (nueva línea), <@lit="\r"> (salto de línea), <@lit="\t"> (tabulación), <@lit="\v"> (tabulación vertical) y <@lit="\\"> (barra inclinada a la izquierda literal). Para presentar un signo por ciento literal, utiliza <@lit="%%">. 

Como en C, puedes indicar los valores numéricos que forman parte del formato (el ancho y/o la precisión) directamente como números, como en <@lit="%10.4f">, o como variables. En este último caso, se ponen asteriscos en la cadena de formato y se proporcionan los argumentos correspondientes por orden. Por ejemplo: 

<code>          
   scalar ancho = 12
   scalar precision = 6
   printf "x = %*.*f\n", ancho, precision, x
</code>

# probit Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--cluster">=<@var="clustvar"> (Consulta <@ref="logit"> para más explicaciones)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--p-values"> (Muestra los valores p en lugar de las pendientes)
		<@lit="--estrella"> (Elige la variante pseudo-R-cuadrado)
		<@lit="--random-effects"> (Estima un modelo Probit de panel con efectos aleatorios, EA)
		<@lit="--quadpoints">=<@var="k"> (Número de puntos de cuadratura para la estimación con EA)
Ejemplos: 	<@inp="ooballot.inp">, <@inp="oprobit.inp">, <@inp="reprobit.inp">

Si la variable dependiente es una variable binaria (todos sus valores son 0 o 1), se obtienen estimaciones máximo verosímiles de los coeficientes de las variables de <@var="indepvars"> mediante el método de Newton–Raphson. Como el modelo es no lineal, las pendientes están condicionadas por los valores de las variables independientes. Por defecto, se calculan las pendientes con respecto a cada una de las variables independientes (en las medias de esas variables) y estas pendientes substituyen a los valores p habituales en el resultado de la regresión. Puedes prescindir de este proceder indicando la opción <@opt="--⁠p-values">. El estadístico chi-cuadrado contrasta la hipótesis nula de que todos los coeficientes son cero, excepto el de la constante. 

Por defecto, las desviaciones típicas se calculan utilizando la inversa negativa de la matriz Hessiana. Si indicas la opción <@opt="--⁠robust">, entonces se calculan en su lugar las desviaciones típicas CMV (QML) o de Huber–White. En este caso, la matriz de covarianzas estimadas es un “emparedado” entre la inversa de la matriz Hessiana estimada y el producto externo del vector gradiente. Para obtener más detalles, consulta el capítulo 10 del libro de <@bib="Davidson y MacKinnon (2004);davidson-mackinnon04">. 

Por defecto, se va a presentar el estadístico pseudo-R-cuadrado que fue sugerido por <@bib="McFadden (1974);mcfadden74">; pero en el caso binario, si indicas la opción <@opt="--⁠estrella"> se va a presentar en su lugar la variante recomendada por <@bib="Estrella (1998);estrella98">. Esta variante presumiblemente imita de forma más parecida las propiedades del <@mth="R"><@sup="2"> habitual en el contexto de la estimación de mínimos cuadrados. 

Si la variable dependiente no es binaria sino discreta, entonces se obtienen las estimaciones de un Probit Ordenado. (Si la variable elegida como dependiente no es discreta, se muestra un fallo.) 

<subhead>Probit para datos de panel</subhead> 

Con la opción <@opt="--⁠random-effects">, se asume que cada perturbación está compuesta por dos componentes Normalmente distribuidas: (a) un término invariante en el tiempo que es específico de la unidad de sección cruzada o “individuo” (y que se conoce como efecto individual), y (b) un término que es específico de la observación concreta. 

La evaluación de la verosimilitud de este modelo implica utilizar la cuadratura de Gauss-Hermite para aproximar el valor de las esperanzas de funciones de variables Normales. Puedes escoger el número de puntos de cuadratura utilizados mediante la opción <@opt="--⁠quadpoints"> (por defecto es de 32). Utilizando más puntos se mejora la precisión de los resultados, pero con el coste de más tiempo de cálculo; así, con muchos puntos de cuadratura, la estimación con un conjunto de datos muy grande puede consumir demasiado tiempo. 

Además de las estimaciones habituales de los parámetros (y de los estadísticos asociados) relacionados con los regresores incluidos, se presenta alguna información adicional sobre la estimación de este tipo de modelo: 

<indent>
• <@lit="lnsigma2">: La estimación máximo verosímil del logaritmo de la varianza del efecto individual; 
</indent>

<indent>
• <@lit="sigma_u">: La estimación de la desviación típica del efecto individual; y 
</indent>

<indent>
• <@lit="rho">: La estimación de la parte del efecto individual en la varianza compuesta de la perturbación (también conocida como la correlación intra-clase). 
</indent>

El contraste de Razón de Verosimilitudes respecto a la hipótesis nula de que <@lit="rho"> es igual a cero, proporciona un modo de evaluar si es necesaria la especificación de efectos aleatorios. Si la hipótesis nula no se rechaza, eso sugiere que es adecuada una simple especificación Probit combinada. 

Menú gráfico: /Modelo/Variable dependiente limitada/Probit

# pvalue Statistics

Argumentos: 	<@var="distribución"> [ <@var="parámetros"> ] <@var="xvalor"> 
Ejemplos: 	<@lit="pvalue z zscore">
		<@lit="pvalue t 25 3.0">
		<@lit="pvalue X 3 5.6">
		<@lit="pvalue F 4 58 fval">
		<@lit="pvalue G shape scale x">
		<@lit="pvalue B bprob 10 6">
		<@lit="pvalue P lambda x">
		<@lit="pvalue W shape scale x">
		Ver también <@inp="mrw.inp">, <@inp="restrict.inp">

Calcula el área que queda a la derecha del valor <@var="xvalor"> en la distribución especificada (<@lit="z"> para la Normal, <@lit="t"> para la <@mth="t"> de Student, <@lit="X"> para la Chi-cuadrado, <@lit="F"> para la <@mth="F">, <@lit="G"> para la Gamma, <@lit="B"> para la Binomial, <@lit="P"> para la Poisson, <@lit="exp"> para la Exponencial, o <@lit="W"> para la Weibull). 

Dependiendo del tipo de distribución, debes indicar la siguiente información antes del valor <@var="xvalor">: para las distribuciones <@mth="t"> y chi-cuadrado, los grados de libertad; para la <@mth="F">, los grados de libertad de numerador y denominador; para la Gamma, los parámetros de forma y de escala; para la distribución Binomial, la probabilidad de “éxito” y el número de intentos; para la distribución de Poisson, el parámetro λ (que es tanto la media como la varianza); para la Exponencial, un parámetro de escala; y para la distribución de Weibull, los parámetros de forma y de escala. Como se mostró en los ejemplos de arriba, puedes indicar los parámetros numéricos en formato numérico o como nombres de variables. 

Los parámetros para la distribución Gamma se indican a veces como media y varianza en lugar de forma y escala. La media es el producto de la forma y la escala; la varianza es el producto de la forma y el cuadrado de la escala. De este modo, puedes calcular la escala dividiendo la varianza entre la media, y puedes calcular la forma dividiendo la media entre la escala. 

Menú gráfico: /Herramientas/Buscador de valores P

# qlrtest Tests

Opciones: 	<@lit="--limit-to">=<@var="lista"> (Limita el contraste a un subconjunto de regresores)
		<@lit="--plot">=<@var="modo-o-nombrearchivo"> (Mira abajo)
		<@lit="--quiet"> (No presenta los resultados)

Para un modelo estimado con datos de series temporales mediante MCO, realiza el contraste de la Razón de Verosimilitudes de Quandt (QLR) para un cambio estructural en un punto desconocido en el tiempo, con un 15 por ciento de recorte al comienzo y al final del período de la muestra. 

Para cada punto potencial de cambio dentro del 70 por ciento central de las observaciones, se realiza un contraste de Chow. Consulta <@ref="chow"> para obtener más detalles; pues, de igual modo que con el contraste habitual de Chow, este es un contraste robusto de Wald cuando el modelo original se estima con la opción <@opt="--⁠robust">, y un contraste F en otro caso. Entonces el estadístico QLR es el máximo de los estadísticos de contraste particulares. 

Se obtiene una probabilidad asociada (valor p) asintótica utilizando el método de <@bib="Bruce Hansen (1997);hansen97">. 

Además de los accesores <@xrf="$test"> y <@xrf="$pvalue"> típicos de los contrastes de hipótesis, puedes utilizar <@xrf="$qlrbreak"> para recuperar el índice de la observación en la que el estadístico de contraste se maximiza. 

Puedes utilizar la opción <@opt="--⁠limit-to"> para limitar el conjunto de interacciones con la variable ficticia de corte en los contrastes de Chow, a un subconjunto de los regresores originales. El parámetro para esta opción debe ser una lista ya definida en la que todos sus elementos se encuentren entre los regresores originales, y en la que no debes incluir la constante. 

Cuando ejecutas de modo interactivo (únicamente) esta instrucción, se muestra por defecto un gráfico del estadístico de contraste de Chow, pero puedes ajustar esto mediante la opción <@opt="--⁠plot">. Los parámetros que se admiten en esta opción son <@lit="none"> (para eliminar el gráfico), <@lit="display"> (para mostrar un gráfico incluso cuando no se está en modo interactivo), o un nombre de archivo. El efecto de proporcionar un nombre de archivo es como el descrito para la opción <@opt="--⁠output"> de la instrucción <@ref="gnuplot">. 

Menú gráfico: Ventana de modelo: Contraste/Contraste de RV de Quandt

# qqplot Graphs

Variantes: 	<@lit="qqplot"> <@var="y">
		<@lit="qqplot"> <@var="y"> <@var="x">
Opciones: 	<@lit="--z-scores"> (Mira abajo)
		<@lit="--raw"> (Mira abajo)
		<@lit="--output">=<@var="nombrearchivo"> (Envía el gráfico al archivo especificado)

Indicando como argumento una única serie, muestra un gráfico de los cuantiles empíricos de la serie seleccionada (indicada por su nombre o su número ID) frente a los cuantiles de la distribución Normal. La serie debe incluir cuando menos 20 observaciones válidas en el rango vigente de la muestra. Por defecto, los cuantiles empíricos se dibujan frente a los cuantiles de una distribución Normal que tiene las mismas media y varianza que los datos de la muestra, pero dispones de dos alternativas: si indicas la opción <@opt="--⁠z-scores">, los datos se tipifican; mientras que si indicas la opción <@opt="--⁠raw">, se dibujan los cuantiles empíricos “en bruto” frente a los cuantiles de la distribución Normal estándar. 

La opción <@opt="--⁠output"> tiene como efecto el envío del resultado al archivo especificado; utiliza “display” para forzar que el resultado se presente en la pantalla. Consulta la instrucción <@ref="gnuplot"> para obtener más detalles sobre esta opción. 

Dadas dos series como argumentos, <@var="y"> y <@var="x">, se muestra un gráfico de los cuantiles empíricos de <@var="y"> frente a los de <@var="x">. Los valores de los datos no se tipifican. 

Menú gráfico: /Variable/Gráfico Q-Q normal
Menú gráfico: /Ver/Gráficos/Gráfico Q-Q

# quantreg Estimation

Argumentos: 	<@var="tau"> <@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--intervals">[=<@var="nivelconf">] (Calcula los intervalos de confianza)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--quiet"> (No presenta los resultados)
Ejemplos: 	<@lit="quantreg 0.25 y 0 xlista">
		<@lit="quantreg 0.5 y 0 xlista --intervals">
		<@lit="quantreg 0.5 y 0 xlista --intervals=.95">
		<@lit="quantreg tauvec y 0 xlista --robust">
		Ver también <@inp="mrw_qr.inp">

Regresión de cuantiles. El primer argumento (<@var="tau">) es el cuantil condicionado para el que se quiere la estimación. Puedes indicarlo, bien con un valor numérico, o bien con el nombre de una variable escalar definida previamente; y el valor debe estar en el rango de 0.01 a 0.99. (Como alternativa, puedes indicar un vector de valores para <@var="tau">; mira abajo para obtener más detalles.) El segundo y subsiguientes argumentos componen una lista de regresión con el mismo patrón que <@ref="ols">. 

Sin la opción <@opt="--⁠intervals">, se presentan las desviaciones típicas para las estimaciones de los cuantiles. Por defecto, estas se calculan de acuerdo con la fórmula asintótica indicada por <@bib="Koenker y Bassett (1978);koenker-bassett78">, pero cuando indicas la opción <@opt="--⁠robust">, se calculan las desviaciones típicas que son robustas con respecto a la heterocedasticidad, utilizando el método de <@bib="Koenker y Zhao (1994);koenker-zhao94">. 

Cuando escoges la opción <@opt="--⁠intervals">, se presentan los intervalos de confianza para las estimaciones de los parámetros en lugar de las desviaciones típicas. Estos intervalos se calculan usando el método de la inversión del rango y, en general, son asimétricos con respecto a las estimaciones puntuales. Las especificidades del cálculo están mediatizadas por la opción <@opt="--⁠robust">: sin esta, los intervalos se calculan bajo el supuesto de perturbaciones IID <@bib="(Koenker, 1994);koenker94">; y con ella se utiliza el estimador robusto desarrollado por <@bib="Koenker y Machado (1999);koenker-machado99">. 

Por defecto, se generan intervalos de confianza del 90 por ciento. Puedes modificar esto añadiendo un nivel de confianza (expresado como una fracción decimal) a la opción de intervalos, como en <@opt="--⁠intervals=0.95">. 

Vector <@var="tau"> de valores: en lugar de proporcionar un escalar, puedes indicar el nombre de una matriz definida previamente. En este caso, las estimaciones se calculan para todos los valores <@var="tau"> indicados, y los resultados se presentan en un formato especial, mostrando la secuencia de las estimaciones de cuantiles para cada regresor, de uno en uno. 

Menú gráfico: /Modelo/Estimación Robusta/Regresión de cuantil

# quit Utilities

Sale de la modalidad vigente de GRETL. 

<indent>
• Cuando esta instrucción se invoca desde un guion, finaliza la ejecución de ese guion. En el contexto de gretlcli en modo de procesamiento por lotes, el propio gretlcli finaliza; en caso contrario, el programa retrocede a modo interactivo. 
</indent>

<indent>
• Cuando se invoca desde la consola del programa de Interfaz Gráfica de Usuario (GUI), se cierra la ventana de la consola. 
</indent>

<indent>
• Cuando se invoca desde gretlcli en modo interactivo, este programa finaliza. 
</indent>

Ten en cuenta que esta instrucción no puede invocarse desde dentro de funciones ni de bucles. 

La instrucción <@lit="quit"> en ningún caso provoca que finalice el programa de Interfaz Gráfica de Usuario (GUI) de GRETL. Esto se hace mediante la opción <@lit="Salir"> del apartado <@lit="Archivo"> del menú, o mediante <@lit="Ctrl+Q">, o pulsando con el ratón el control de cierre en la barra de título de la ventana principal de GRETL. 

# rename Dataset

Argumentos: 	<@var="serie"> <@var="nuevonombre"> 
Opciones: 	<@lit="--quiet"> (Suprime la presentación de resultados)
		<@lit="--case"> (Cambia el formato de los nombres de todas las series; mira abajo)
Ejemplos: 	<@lit="rename x2 ingreso">
		<@lit="rename --case=lower">

Sin la opción <@opt="--⁠case">, esta instrucción cambia el nombre de <@var="serie"> (identificada por su nombre o su número ID) a <@var="nuevonombre">. El nuevo nombre debe tener 31 caracteres como máximo, comenzar con una letra y estar formado solo por letras, dígitos y/o el carácter de barra baja. Además, no debe ser el nombre de un objeto de cualquier tipo que ya exista. 

La opción <@opt="--⁠case"> permite cambiar el formato de <@itl="todos"> los nombres de las series en el conjunto de datos abierto vigente. Cuando se utiliza esta opción, no se debe indicar ningún nombre para <@var="serie"> ni para <@var="nuevonombre">. Se admiten los siguientes tipos de formato: 

<indent>
• <@var="lower">: Convierte todos los nombres de las series a minúsculas. 
</indent>

<indent>
• <@var="upper">: Convierte todos los nombres de las series a mayúsculas. 
</indent>

<indent>
• <@var="camel">: Convierte todos los nombres de las series a formato camello; lo que significa que se eliminan los guiones bajos y el carácter siguiente a los mismos (si hai alguno) se pone en mayúsculas. Por ejemplo, <@lit="alguna_cosa"> se vuelve <@lit="algunaCosa">. 
</indent>

<indent>
• <@var="snake">: Convierte todos los nombres de las series a formato serpiente; lo que significa que cualquier letra mayúscula (excepto la primera del nombre) se convierte a minúscula, precedida por un guión bajo. Por ejemplo, <@lit="algunaCosa"> se vuelve <@lit="alguna_cosa">. 
</indent>

Menú gráfico: /Variable/Editar atributos
Otro acceso: Ventana principal: Menú emergente (selección única)

# reset Tests

Opciones: 	<@lit="--quiet"> (No presenta la regresión auxiliar)
		<@lit="--silent"> (No presenta nada)
		<@lit="--squares-only"> (Calcula el contraste usando solo los cuadrados)
		<@lit="--cubes-only"> (Calcula el contraste usando solo los cubos)
		<@lit="--robust"> (Usa errores típicos robustos en la regresión auxiliar)

Debe ir después de la estimación de un modelo mediante MCO. Lleva a cabo el contraste RESET de Ramsey sobre la especificación (no lineal) de un modelo, añadiéndole a la regresión los cuadrados y/o los cubos de los valores ajustados, y calculando el estadístico <@mth="F"> para contrastar la hipótesis nula de que los parámetros de los términos añadidos son cero. Por razones numéricas, los cuadrados y los cubos se reescalan utilizando la desviación típica de los valores ajustados. 

Se van a añadir tanto los cuadrados como los cubos, excepto que indiques una de las opciones <@opt="--⁠squares-only"> o <@opt="--⁠cubes-only">. 

Puedes utilizar la opción <@opt="--⁠silent"> si tienes intención de hacer uso de los accesores <@xrf="$test"> y/o <@xrf="$pvalue"> para guardar los resultados del contraste. 

La opción <@opt="--⁠robust"> está implícita cuando en la regresión que se va a comprobar se utilizaron errores típicos robustos. 

Menú gráfico: Ventana de modelo: Contrastes/Contraste RESET de Ramsey

# restrict Tests

Opciones: 	<@lit="--quiet"> (No presenta las estimaciones restringidas)
		<@lit="--silent"> (No presenta nada)
		<@lit="--wald"> (Solo estimadores de sistema, mira abajo)
		<@lit="--bootstrap"> (Cálculo del contraste con remuestreo automático, si es posible)
		<@lit="--full"> (Solo MCO y VECMs, mira abajo)
Ejemplos: 	<@inp="hamilton.inp">, <@inp="restrict.inp">

Impone un conjunto de restricciones (habitualmente lineales) sobre: (a) el último modelo estimado o (b) un sistema de ecuaciones que se definió y nombró previamente. En todos los casos, debes comenzar el conjunto de restricciones con la palabra clave “restrict” y terminarlo con “end restrict”. 

En caso de una única ecuación, las restricciones siempre se aplican implícitamente al último modelo, y se evalúan tan pronto como se cierre el bloque <@lit="restrict">. 

En caso de un sistema de ecuaciones (definido mediante la instrucción <@ref="system">), puedes poner el nombre del sistema de ecuaciones definido previamente, después del “restrict” inicial. Cuando omites eso y el último modelo fue un sistema, entonces las restricciones se aplican a ese último modelo. Por defecto, las restricciones se evalúan cuando el sistema acaba de estimarse, usando la instrucción <@ref="estimate">. Pero cuando indicas la opción <@opt="--⁠wald">, la restricción se comprueba inmediatamente a través del contraste chi-cuadrado de Wald en relación a la matriz de covarianzas. Ten en cuenta que esta opción va a generar un fallo si ya has definido un sistema, pero aún no lo has estimado. 

Dependiendo del contexto, puedes expresar de varios modos las restricciones que quieras contrastar. El más simple es como se indica a continuación: cada restricción se expresa como una ecuación, con una combinación lineal de parámetros a la izquierda del signo de igualdad y un valor escalar a la derecha (bien una constante numérica, o bien el nombre de una variable escalar). 

En caso de una única ecuación, puedes referirte a sus parámetros con el formato <@lit="b["><@var="i"><@lit="]">, donde <@var="i"> representa la posición en la lista de regresores (comenzando en el 1), o con el formato <@lit="b["><@var="nombrevar"><@lit="]">, donde <@var="nombrevar"> es el nombre del regresor en cuestión. En caso de un sistema, la referencia a los parámetros se hace utilizando la letra <@lit="b"> junto con dos números colocados entre corchetes. El primer número representa la posición de la ecuación dentro del sistema, y el segundo número indica la posición del regresor dentro de la lista de ellos. Por ejemplo, <@lit="b[2,1]"> denota el primer parámetro de la segunda ecuación, mientras que <@lit="b[3,2]"> denota el segundo parámetro de la tercera ecuación. Puedes anteponer multiplicadores numéricos a los elementos <@lit="b"> de la ecuación que representa una restricción, por ejemplo <@lit="3.5*b[4]">. 

Aquí tienes un ejemplo de un conjunto de restricciones para un modelo estimado previamente: 

<code>          
   restrict
    b[1] = 0
    b[2] - b[3] = 0
    b[4] + 2*b[5] = 1
   end restrict
</code>

Y aquí tienes un ejemplo de un conjunto de restricciones para aplicar a un sistema ya definido. (Si el nombre del sistema no contiene espacios, no hacen falta las comillas que lo delimitan.) 

<code>          
   restrict "Sistema 1"
    b[1,1] = 0
    b[1,2] - b[2,2] = 0
    b[3,4] + 2*b[3,5] = 1
   end restrict
</code>

En caso de una única ecuación, las restricciones se evalúan por defecto por medio del contraste de Wald, usando la matriz de covarianzas del modelo en cuestión. Si estimaste el modelo original con MCO, entonces se presentan las estimaciones de los coeficientes restringidos; para eliminar esto, añade la opción <@opt="--⁠quiet"> a la instrucción <@lit="restrict"> inicial. Como alternativa al contraste de Wald, para modelos estimados únicamente mediante MCO o MCP, puedes indicar la opción <@opt="--⁠bootstrap"> para realizar el contraste de la restricción con remuestreo automático (bootstrap). 

En caso de un sistema, el estadístico de contraste depende del estimador elegido: un estadístico de Razón de Verosimilitudes cuando el sistema se estima utilizando un método de Máxima Verosimilitud, o un estadístico <@mth="F"> asintótico, en otro caso. 

<subhead>Restricciones lineales: sintaxis alternativa</subhead> 

Tienes dos alternativas al método para expresar las restricciones descrito más arriba. Primero, puedes escribir de forma compacta un conjunto de <@mth="g"> restricciones sobre el vector con los <@mth="k"> parámetros (β), como <@mth="R">β – <@mth="q"> = 0, donde <@mth="R"> es una matriz de dimensión <@itl="g">×<@itl="k"> y <@mth="q"> es un vector de dimensión <@mth="g">. Puedes expresar una restricción indicando los nombres de matrices definidas previamente, cómodas para utilizar como <@mth="R"> y <@mth="q">, como en 

<code>          
   restrict
     R = Rmat
     q = qvec
   end restrict
</code>

En segundo lugar, como variante que puede serte útil cando uses la función <@lit="restrict"> dentro de otra función, puedes elaborar un conjunto de enunciados de restricción con el formato de un 'array' de cadenas de texto. Después utiliza la palabra clave <@lit="inject"> con el nombre del 'array'. Este es un ejemplo simple: 

<code>          
   strings RS = array(2)
   RS[1] = "b[1,2] = 0"
   RS[2] = "b[2,1] = 0"
   restrict
     inject RS
   end restrict
</code>

Con el uso actual de este método, posiblemente preferirás utilizar la función <@xrf="sprintf"> para elaborar las cadenas de texto, en base a la entrada para una función. 

<subhead>Restricciones no lineales </subhead> 

Si quieres contrastar una restricción no lineal (lo que actualmente solo está disponible para modelos de una única ecuación), debes indicar la restricción con el nombre de una función, precedida por “<@lit="rfunc = ">”, como en 

<code>          
   restrict
     rfunc = mifuncion
   end restrict
</code>

La función de restricción debe tener un único argumento <@lit="const matrix">, y esto se completa automáticamente con el vector de parámetros. Y debiera devolver un vector que es cero bajo la hipótesis nula, y no nulo en otro caso. La dimensión del vector es igual al número de restricciones. Esta función se utiliza como una “rellamada” de la rutina numérica para el Jacobiano, de GRETL, que calcula el estadístico de contraste de Wald mediante el método delta. 

Aquí tienes un ejemplo sencillo de una función apropiada para comprobar una restricción no lineal, concretamente que dos pares de valores de los parámetros tienen una razón común. 

<code>          
   function matrix restr (const matrix b)
     matrix v = b[1]/b[2] - b[4]/b[5]
     return v
   end function
</code>

Cuando se completa con éxito la instrucción <@lit="restrict">, los accesores <@xrf="$test"> y <@xrf="$pvalue"> proporcionan el estadístico de contraste y su probabilidad asociada (valor p), respectivamente. 

Cuando se contrastan restricciones sobre un modelo de una única ecuación que fue estimado mediante MCO o sobre un Modelo de Vectores de Corrección del Error (VECM), puedes utilizar la opción <@opt="--⁠full"> para disponer que las estimaciones restringidas sean el “último modelo”, con la intención de hacer contrastes más adelante o de usar accesores como <@lit="$coeff"> y <@lit="$vcv">. Ten en cuenta que se aplican algunos detalles especiales en caso de que pruebes restricciones sobre un VECM. Consulta <@pdf="El manual de gretl#chap:vecm"> (Capítulo 33) para obtener más detalles. 

Menú gráfico: Ventana de modelo: Contrastes/Restricciones lineales

# rmplot Graphs

Argumento: 	<@var="serie"> 
Opciones: 	<@lit="--trim"> (Mira abajo)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--output">=<@var="nombrearchivo"> (Mira abajo)

Gráfico Rango–Media: Esta instrucción genera un gráfico sencillo para ayudar a decidir si una serie temporal, <@mth="y">(t), tiene una varianza constante o no. Se coge la muestra completa (t=1,...,T) y se divide en pequeñas submuestras de tamaño arbitrario <@mth="k">. La primera submuestra está compuesta por <@mth="y">(1),...,<@mth="y">(k), la segunda por <@mth="y">(k+1), ..., <@mth="y">(2k), etcétera. Para cada submuestra, se calcula la media de la serie en la muestra y el rango (= máximo menos mínimo), y se construye un gráfico con las medias en el eje horizontal y los rangos en el vertical. Así cada submuestra se representa mediante un punto en este plano. Si la varianza de la serie es constante, se esperaría que el rango de la submuestra sea independiente de la media de la submuestra. Por eso, si observamos que los puntos se aproximan a una línea con pendiente positiva, esto sugiere que la varianza de las series aumenta a medida que lo hace la media; y si los puntos se aproximan a una línea con pendiente negativa, esto sugiere que la varianza decrece al aumentar la media. 

Además del gráfico, GRETL muestra las medias y rangos para cada submuestra, junto con el coeficiente de la pendiente de una regresión MCO do rango sobre la media, y con la probabilidad asociada al estadístico para contrastar la hipótesis nula de que esta pendiente es cero. Si el coeficiente de la pendiente es significativo con un nivel de significación del 10 por ciento, entonces se muestra en el gráfico la línea ajustada de la regresión del rango sobre la media. Se registran tanto el estadístico <@mth="t"> para contrastar la hipótesis nula como la probabilidad asociada correspondiente, y puedes recuperarlos usando los accesores <@xrf="$test"> y <@xrf="$pvalue">, respectivamente. 

Cuando indicas la opción <@opt="--⁠trim">, se descartan los valores mínimo y máximo de cada submuestra antes de calcular la media y el rango. Esto hace que sea menos probable que los valores atípicos provoquen una distorsión en el análisis. 

Cuando indicas la opción <@opt="--⁠quiet">, no se muestra el gráfico ni se presenta el resultado; solo se indican el estadístico <@mth="t"> y su probabilidad asociada (valor p). Por otro lado, puedes controlar el formato del gráfico mediante la opción <@opt="--⁠output">; y esto funciona como se describe en conexión con la instrucción <@ref="gnuplot">. 

Menú gráfico: /Variable/Gráfico rango-media

# run Programming

Argumento: 	<@var="nombrearchivo"> 

Ejecuta las instrucciones de <@var="nombrearchivo"> y luego devuelve el control al indicador interactivo. Esta instrucción está pensada para que la utilices con el programa de líneas de instrucción gretlcli o con la “consola de GRETL” en el programa de Interfaz Gráfica de Usuario (GUI). 

Consulta también <@ref="include">. 

Menú gráfico: Icono 'Ejecutar' en la ventana del editor de guiones

# runs Tests

Argumento: 	<@var="serie"> 
Opciones: 	<@lit="--difference"> (Utiliza las primeras diferencias de la variable)
		<@lit="--equal"> (Los valores positivos y negativos son equiprobables)

Realiza el contraste no paramétrico “de rachas” para comprobar el carácter aleatorio de la <@var="serie"> indicada, donde las rachas se definen como secuencias de valores consecutivos positivos o negativos. Si quieres contrastar el carácter aleatorio de las desviaciones respecto a la mediana, para una variable llamada <@lit="x1"> que tiene una mediana no nula, puedes hacer lo siguiente: 

<code>          
   series signx1 = x1 - median(x1)
   runs signx1
</code>

Cuando indicas la opción <@opt="--⁠difference">, se van a calcular las primeras diferencias de la serie antes del análisis, por lo que las rachas se interpretarían como secuencias de aumentos o de diminuciones consecutivas del valor de la variable. 

Cuando indicas la opción <@opt="--⁠equal">, la hipótesis nula también incorpora el supuesto de que los valores positivos y negativos son igual de probables; de lo contrario, el estadístico de contraste resulta invariante con respecto a la “neutralidad” del proceso que generó la secuencia de valores, y el contraste se centra únicamente en la independencia. 

Menú gráfico: /Herramientas/Contrastes no paramétricos

# scatters Graphs

Argumentos: 	<@var="yvar"> ; <@var="xvars"> o <@var="yvars ; xvar"> 
Opciones: 	<@lit="--with-lines"> (Genera gráficos de líneas)
		<@lit="--matrix">=<@var="nombrematriz"> (Representa las columnas de la matriz indicada)
		<@lit="--output">=<@var="nombrearchivo"> (Envía el resultado al archivo especificado)
Ejemplos: 	<@lit="scatters 1 ; 2 3 4 5">
		<@lit="scatters 1 2 3 4 5 6 ; 7">
		<@lit="scatters y1 y2 y3 ; x --with-lines">

Genera gráficos de dos variables, bien de <@var="yvar"> frente a todas las variables de <@var="xvars">, o bien de todas las variables de <@var="yvars"> frente a <@var="xvar">. En el primer ejemplo de arriba, se coloca la variable 1 en el eje <@mth="y"> y se dibujan 4 gráficos: el primero tiene la variable 2 en el eje <@mth="x">, el segundo con la variable 3 en el eje <@mth="x">, etcétera. El segundo ejemplo representa cada una de las variables de la 1 a la 6, frente a la variable 7 en el eje <@mth="x">. Repasar un conjunto de esos gráficos puede ser un paso conveniente en el análisis exploratorio de datos. El número máximo de gráficos es de 16, por lo que se va a ignorar cualquier variable adicional en la lista. 

Por defecto, los datos se muestran con puntos, pero si indicas la opción <@opt="--⁠with-lines"> serán gráficos de líneas. 

Para obtener más detalles sobre el uso de la opción <@opt="--⁠output">, consulta la instrucción <@ref="gnuplot">. 

Si especificas una matriz ya definida como origen de los datos, debes expresar las listas <@var="x"> e <@var="y"> con números naturales positivos para indicar la columna. Alternativamente, si no indicas esas listas, se representan todas las columnas frente al tiempo o a una variable índice. 

Consulta también la instrucción <@ref="tsplots"> para ver un modo sencillo de generar gráficos múltiples de series temporales, y la instrucción <@ref="gridplot"> para ver un modo más flexible de combinar gráficos en una parrilla. 

Menú gráfico: /Ver/Gráficos múltiples/Gráficos X-Y (scatters)

# sdiff Transformations

Argumento: 	<@var="listavariables"> 

Se obtiene la diferencia estacional de cada una de las variables de <@var="listavariables">, y se guarda el resultado en una nueva variable con el prefijo <@lit="sd_">. Esta instrucción está disponible solo para series de tiempo estacionales. 

Menú gráfico: /Añadir/Diferencias estacionales de las variables seleccionadas

# set Programming

Variantes: 	<@lit="set"> <@var="variable"> <@var="valor">
		<@lit="set --to-file="><@var="nombrearchivo">
		<@lit="set --from-file="><@var="nombrearchivo">
		<@lit="set stopwatch">
		<@lit="set">
Ejemplos: 	<@lit="set svd on">
		<@lit="set csv_delim tab">
		<@lit="set horizon 10">
		<@lit="set --to-file=mysettings.inp">

El uso más común de esta instrucción es la primera variante mostrada arriba, en la que se utiliza para establecer el valor de un parámetro escogido del programa (esto se discute con detalle más abajo). Los otros usos son: con <@opt="--⁠to-file"> para escribir un archivo de guion que contenga todas las configuraciones actuales de los parámetros; con <@opt="--⁠from-file"> para leer un archivo de guion que contenga las configuraciones de los parámetros y para aplicarlas a la sesión vigente; con <@lit="stopwatch"> para poner a cero el “cronómetro” de GRETL que puedes usar para medir el tiempo de CPU (consulta los comentarios para el accesor <@xrf="$stopwatch">); o para presentar las configuraciones actuales, cuando indicas solo la palabra <@lit="set">. 

Los valores establecidos mediante esta instrucción siguen vigentes durante la duración de la sesión de GRETL, excepto que los cambies por medio de una llamada posterior a <@lit="set">. Los parámetros que puedes establecer de este modo se enumeran más abajo. Ten en cuenta que se utilizan las configuraciones de <@lit="hc_version">, <@lit="hac_lag"> y <@lit="hac_kernel"> cuando indicas la opción <@opt="--⁠robust"> en una instrucción de estimación. 

Las configuraciones disponibles se agrupan bajo las siguientes categorías: interacción y comportamiento del programa, métodos numéricos, generación de números aleatorios, estimación robusta, filtrado, estimación de series temporales e interacción con GNU R. 

<subhead>Interacción y comportamiento del programa</subhead> 

Estas configuraciones se utilizan para controlar diversos aspectos del modo en el que GRETL interactúa con el usuario. 

<indent>
• <@lit="workdir">: <@var="path">. Establece el directorio por defecto para escribir y leer archivos en los casos en los que no se especifican las rutas completas. 
</indent>

<indent>
• <@lit="use_cwd">: <@lit="on"> u <@lit="off"> (por defecto). Maneja la configuración del directorio de trabajo (<@lit="workdir">) inicial: si está en <@lit="on">, se hereda el directorio de trabajo desde el intérprete; en otro caso, se establece donde quiera que se seleccionó en la sesión previa de GRETL. 
</indent>

<indent>
• <@lit="echo">: <@lit="off"> u <@lit="on"> (por defecto). Elimina (o acorta) la resonancia de los textos de las instrucciones en los resultados de GRETL. 
</indent>

<indent>
• <@lit="messages">: <@lit="off"> u <@lit="on"> (por defecto). Elimina (o acorta) la presentación de mensajes sin fallo asociados a diversas instrucciones, por ejemplo cuando se genera una nueva variable o cuando se cambia el rango de la muestra. 
</indent>

<indent>
• <@lit="verbose">: <@lit="off">, <@lit="on"> (por defecto) o <@lit="comments">. Funciona como un “interruptor maestro” para <@lit="echo"> y <@lit="messages"> (mira más abajo), apagando o encendiendo los dos simultáneamente. El argumento <@lit="comments"> apaga la resonancia y la aparición de mensajes, pero mantiene la presentación de comentarios de un guion. 
</indent>

<indent>
• <@lit="warnings">: <@lit="off"> u <@lit="on"> (por defecto). Elimina (o acorta) la presentación de mensajes de advertencia cuando surgen problemas numéricos; por ejemplo, si un cálculo produce valores no finitos o si la convergencia de un proceso de optimización es cuestionable. 
</indent>

<indent>
• <@lit="csv_delim">: <@lit="comma"> (coma, por defecto), <@lit="space"> (espacio), <@lit="tab"> (tabulación) o <@lit="semicolon"> (punto y coma). Establece el delimitador de columnas que se usa cuando se guardan datos en un archivo con formato CSV. 
</indent>

<indent>
• <@lit="csv_write_na">: La cadena de texto que se utiliza para representar los valores ausentes cuando se escriben datos en un archivo con formato CSV. Máximo = 7 caracteres; por defecto es <@lit="NA">. 
</indent>

<indent>
• <@lit="csv_read_na">: La cadena de texto que se coge para representar valores ausentes (NAs) cuando se leen datos con el formato CSV (máximo 7 caracteres). La cadena por defecto depende de que se encuentre una columna de datos que contenga datos numéricos (la mayoría de las veces) o valores de cadena. Para datos numéricos, se considera que lo siguiente indica NAs: una celda vacía o cualquiera de las cadenas <@lit="NA">, <@lit="N.A.">, <@lit="na">, <@lit="n.a.">, <@lit="N/A">, <@lit="#N/A">, <@lit="NaN">, <@lit=".NaN">, <@lit=".">, <@lit="..">, <@lit="-999">, y <@lit="-9999">. Para datos con forma de cadenas de texto con valores, tan solo se cuenta como NA una celda en blanco o una celda que contenga una cadena vacía. Puedes volver a imponer esos valores por defecto indicando <@lit="default"> como el valor de <@lit="csv_read_na">. Para especificar que tan solo se leen las celdas vacías como NAs, indica el valor <@lit="""">. Ten en cuenta que las celdas vacías siempre se leen como NAs con independencia de como esté configurada esta variable. 
</indent>

<indent>
• <@lit="csv_digits">: Un entero positivo que especifica el número de dígitos significativos a usar cuando se escriben datos en formato CSV. Por defecto, se utilizan hasta 15 dígitos dependiendo de la precisión de los datos originales. Ten en cuenta que el resultado CSV emplea la función <@lit="fprintf"> de la librería de C con la conversión “<@lit="%g">” , lo que significa que se prescinde de los ceros que quedan atrás. 
</indent>

<indent>
• <@lit="display_digits">: Un entero de 3 a 6 que especifica el número de dígitos significativos a usar cuando se muestran los coeficientes de la regresión y las desviaciones típicas (siendo 6 por defecto). También puedes utilizar esta configuración para limitar el número de dígitos que se muestran con la instrucción <@ref="summary">; siendo en este caso 5 por defecto (y también el máximo). 
</indent>

<indent>
• <@lit="mwrite_g">: <@lit="on"> u <@lit="off"> (por defecto). Cuando se escribe una matriz como texto en un archivo, GRETL por defecto utiliza notación científica con 18 dígitos de precisión, asegurando de este modo que los valores guardados son una representación fiable de los números en memoria. Cuando se escriben datos básicos con no más que 6 dígitos de precisión, puedes preferir utilizar el formato <@lit="%g"> para tener un archivo más compacto y fácil de leer; puedes hacer este cambio mediante <@lit="set mwrite_g on">. 
</indent>

<indent>
• <@lit="force_decpoint">: <@lit="on"> u <@lit="off"> (por defecto). Fuerza a GRETL a utilizar el carácter de punto decimal, en un escenario donde otro carácter (probablemente la coma) es el separador decimal estándar. 
</indent>

<indent>
• <@lit="loop_maxiter">: Un valor entero no negativo (por defecto es 100000). Establece el número máximo de iteraciones que se le permite a un bucle <@lit="while">, antes de parar (consulta <@ref="loop">). Ten en cuenta que esta configuración solo afecta a la variante <@lit="while">; su intención es protegerse ante infinitos bucles que surjan de forma inadvertida. Establecer que este valor sea 0 tiene el efecto de inhabilitar el límite (utilízalo con precaución). 
</indent>

<indent>
• <@lit="max_verbose">: <@lit="off"> (por defecto), <@lit="on"> o <@lit="full">. Controla la verborrea de las instrucciones y de las funciones que utilizan métodos de optimización numérica. La opción <@lit="on"> solo se aplica a funciones (tales como <@xrf="BFGSmax"> y <@xrf="NRmax">) que funcionan por defecto con discreción; su efecto consiste en que se muestra información básica sobre las iteraciones. Puedes usar la opción <@lit="full"> para provocar un resultado más detallado, que incluye los valores de los parámetros y su respectivo gradiente de la función objetivo, en cada iteración. Esta opción se aplica tanto a las funciones del tipo mencionado antes, como a las instrucciones que se basan en optimización numérica como <@ref="arima">, <@ref="probit"> y <@ref="mle">. En el caso de las instrucciones, su efecto consiste en hacer que su opción <@opt="--⁠verbose"> proporcione un mayor detalle. Consulta también <@pdf="El manual de gretl#chap:numerical"> (Capítulo 37). 
</indent>

<indent>
• <@lit="debug">: <@lit="1">, <@lit="2"> o <@lit="0"> (por defecto). Esto se utiliza con las funciones definidas por el usuario. Establecer <@lit="debug"> igual a 1 equivale a activar <@lit="messages"> dentro de todas esas funciones, y establecer esta variable igual a <@lit="2"> tiene el efecto adicional de activar <@lit="max_verbose"> dentro de todas las funciones. 
</indent>

<indent>
• <@lit="shell_ok">: <@lit="on"> u <@lit="off"> (por defecto). Permite ejecutar programas externos desde GRETL mediante el intérprete de sistema. Esto no está habilitado por defecto por razones de seguridad, y solo puedes habilitarlo mediante la Interfaz Gráfica de Usuario (Herramientas/Preferencias/General). Sin embargo, una vez activada, esta configuración permanecerá activa para sesiones futuras hasta que se desactive explícitamente. 
</indent>

<indent>
• <@lit="bfgs_verbskip">: Un entero. Esta configuración afecta al comportamiento de la opción <@opt="--⁠verbose"> en aquellas instrucciones que utilizan BFGS como algoritmo de optimización, y se usa para compactar el resultado. Si <@lit="bfgs_verbskip"> se establece en 3, por ejemplo, entonces la opción <@opt="--⁠verbose"> va a provocar que se presenten las iteraciones 3, 6, 9, etcétera. 
</indent>

<indent>
• <@lit="skip_missing">: <@lit="on"> (por defecto) u <@lit="off">. Controla el comportamiento de GRETL cuando se construye una matriz a partir de series de datos: por defecto se saltan las filas de datos que contienen uno o más valores ausentes, pero cuando se pone <@lit="skip_missing"> en <@lit="off">, los valores ausentes se convierten en NaNs. 
</indent>

<indent>
• <@lit="matrix_mask">: El nombre de una serie o la palabra clave <@lit="null">. Ofrece un mayor control que <@lit="skip_missing"> cuando se construyen matrices a partir de series: las filas de datos seleccionadas para las matrices son aquellas con valores no nulos (y no ausentes) de las series especificadas. La máscara escogida permanece en vigor hasta que se substituye, o se elimina mediante la palabra clave <@lit="null">. 
</indent>

<indent>
• <@lit="quantile_type">: Debes escoger entre <@lit="Q6"> (por defecto), <@lit="Q7"> o <@lit="Q8">. Selecciona el método concreto que utiliza la función <@xrf="quantile">. Para obtener más detalles, consulta <@bib="Hyndman y Fan (1996);hyndman96"> o la entrada de la Wikipedia disponible en <@url="https://en.wikipedia.org/wiki/Quantile">. 
</indent>

<indent>
• <@lit="huge">: Un número positivo muy grande (por defecto, 1.0E100). Esta configuración controla el valor que devuelve el accesor <@xrf="$huge">. 
</indent>

<indent>
• <@lit="assert">: <@lit="off"> (por defecto), <@lit="warn"> o <@lit="stop">. Controla las consecuencias de un fallo (que el valor que se devuelva sea igual a 0) de la función <@xrf="assert">. 
</indent>

<indent>
• <@lit="datacols">: Un número entero de 1 a 15, cuyo valor por defecto es 5. Establece el número máximo de series que se presentan conjuntamente cuando los datos se representan por observación. 
</indent>

<indent>
• <@lit="plot_collection">: <@lit="on">, <@lit="auto"> u <@lit="off">. Esta configuración afecta al modo en el que se muestran los gráficos durante el uso interactivo. Si está en <@lit="on">, los gráficos del mismo tamaño en pixels se reúnen en una “colección de gráficos”, es decir, en una única pantalla de salida de resultados en la que puedes navegar entre los diversos gráficos yendo adelante y hacia atrás. Con el ajuste en <@lit="off">, por el contrario, se va a generar una pantalla distinta para cada gráfico, como en las versiones anteriores de GRETL. Finalmente, el ajuste en <@lit="auto"> tiene como efecto que permite el modo de colección de gráficos solo para aquellos que se generan antes de que pasen 1.25 segundos después de otro (por ejemplo, como resultado de ejecutar instrucciones de representación gráfica dentro de un bucle). 
</indent>

<subhead>Métodos numéricos</subhead> 

Estas configuraciones se utilizan para controlar los algoritmos numéricos que utiliza GRETL para la estimación. 

<indent>
• <@lit="optimizer">: o <@lit="auto"> (por defecto), o <@lit="BFGS">, o bien <@lit="newton">. Establece el algoritmo de optimización que se utiliza para varios estimadores Máximo Verosímiles, en los casos donde el BFGS y el de Newton–Raphson se pueden aplicar ambos. Por defecto, se utiliza el de Newton–Raphson cuando se disponga de una matriz Hessiana analítica; en otro caso, BFGS. 
</indent>

<indent>
• <@lit="bhhh_maxiter">: Un entero, el número máximo de iteraciones para la rutina interna BHHH de GRETL, que se utiliza en la instrucción <@lit="arma"> para la estimación MV condicional. Si la convergencia no se alcanza luego de <@lit="bhhh_maxiter">, el programa devuelve un fallo. Por defecto, se establece en 500. 
</indent>

<indent>
• <@lit="bhhh_toler">: Un valor de punto flotante o la cadena <@lit="default">. Esto se utiliza en la rutina interna BHHH de GRETL para verificar si la convergencia se alcanzó. El algoritmo termina de repetirse tan pronto como el incremento en el logaritmo de la verosimilitud entre iteraciones sea menor que <@lit="bhhh_toler">. El valor por defecto es 1.0E–06, y puedes restablecer este valor tecleando <@lit="default"> en lugar de un valor numérico. 
</indent>

<indent>
• <@lit="bfgs_maxiter">: Un entero, el número máximo de iteraciones para la rutina BFGS de GRETL, que se utiliza para <@lit="mle"> (EMV), <@lit="gmm"> (MXM) y varios estimadores específicos. Si no se alcanza la convergencia en el número indicado de iteraciones, el programa devuelve un fallo. El valor por defecto depende del contexto, pero habitualmente es del orden de 500. 
</indent>

<indent>
• <@lit="bfgs_toler">: Un valor de punto flotante o la cadena <@lit="default">. Esto se utiliza en la rutina interna BFGS de GRETL para verificar si la convergencia se alcanzó. El algoritmo termina de repetirse tan pronto como la mejoría relativa en la función objetivo entre iteraciones sea menor que <@lit="bfgs_toler">. El valor por defecto es igual a la precisión de máquina elevada a 3/4, y puedes restablecer este valor tecleando <@lit="default"> en lugar de un valor numérico. 
</indent>

<indent>
• <@lit="bfgs_maxgrad">: Un valor de punto flotante. Esto se utiliza en la rutina interna BFGS de GRETL, para verificar si la norma del vector gradiente está razonablemente cerca de cero cuando se alcanza el criterio <@lit="bfgs_toler">. Se va a presentar una advertencia cuando la norma del vector gradiente exceda de 1; y se muestra un fallo si la norma excede <@lit="bfgs_maxgrad">. Actualmente, por defecto el valor de tolerancia es de 5.0. 
</indent>

<indent>
• <@lit="bfgs_richardson">: <@lit="on"> u <@lit="off"> (por defecto). Utiliza la extrapolación de Richardson cuando calcules las derivadas numéricas en el contexto de la maximización BFGS. 
</indent>

<indent>
• <@lit="initvals">: El nombre de una matriz que haya sido definida previamente. Permite establecer manualmente el vector inicial de parámetros en determinadas instrucciones de estimación que implican realizar optimización numérica como <@lit="arma">, <@lit="garch">, <@lit="logit">, <@lit="probit">, <@lit="tobit">, <@lit="intreg">, <@lit="biprobit">, <@lit="duration">; y también cuando se imponen ciertos tipos de restricciones que están vinculadas a modelos VEC. A diferencia de otras configuraciones, <@lit="initvals"> no es persistente, pues se restablece su valor al de inicio por defecto, después de su primera utilización. Para obtener detalles en relación con la estimación ARMA consulta <@pdf="El manual de gretl#chap:timeseries"> (Capítulo 31). 
</indent>

<indent>
• <@lit="lbfgs">: <@lit="on"> u <@lit="off"> (por defecto). Utiliza la versión de memoria limitada de BFGS (L-BFGS-B) en vez del algoritmo habitual. Esto puede ser ventajoso cuando la función que se maximiza no es globalmente cóncava. 
</indent>

<indent>
• <@lit="lbfgs_mem">: Un valor entero en el rango de 3 a 20 (con un valor por defecto de 8). Esto determina el número de correcciones que se utilizan en la matriz de memoria limitada cuando se emplea L-BFGS-B. 
</indent>

<indent>
• <@lit="nls_toler">: Un valor de punto flotante. Establece la tolerancia que se utiliza al juzgar si la convergencia se alcanza o no, en una estimación de mínimos cuadrados no lineales utilizando la instrucción <@ref="nls">. El valor por defecto es igual a la precisión de máquina elevada a 3/4, y puedes restablecer este valor tecleando <@lit="default"> en lugar de un valor numérico. 
</indent>

<indent>
• <@lit="svd">: <@lit="on"> u <@lit="off"> (por defecto). Utiliza la Descompisición en Valores Singulares (SVD) en vez de las descomposiciones de Cholesky o la QR, en los cálculos de mínimos cuadrados. Esta opción se aplica a la función <@lit="mols"> así como a varios cálculos internos, pero no a la instrucción <@ref="ols"> habitual. 
</indent>

<indent>
• <@lit="force_qr">: <@lit="on"> u <@lit="off"> (por defecto). Esto se aplica a la instrucción <@ref="ols">. Por defecto, esta instrucción calcula las estimaciones de MCO utilizando la descomposición de Cholesky (el método más rápido), con QR como último recurso si los datos parecen demasiado mal condicionados. Puedes utilizar <@lit="force_qr"> para saltarte el paso de Cholesky, pues en los casos “dudosos” esto puede asegurar una mayor precisión. 
</indent>

<indent>
• <@lit="fcp">: <@lit="on"> u <@lit="off"> (por defecto). Utiliza el algoritmo de Fiorentini, Calzolari y Panattoni en vez del código propio de GRETL, cuando se calculan las estimaciones GARCH. 
</indent>

<indent>
• <@lit="gmm_maxiter">: Un entero, el número máximo de iteraciones de la instrucción <@ref="gmm"> de GRETL cuando se está en modo iterativo (en contraposición al de un paso o al de dos pasos). El valor por defecto es 250. 
</indent>

<indent>
• <@lit="nadarwat_trim">: Un entero, el parámetro de recorte utilizado en la función <@xrf="nadarwat">. 
</indent>

<indent>
• <@lit="fdjac_quality">: Un entero (0, 1 o 2) que indica el algoritmo utilizado por la función <@xrf="fdjac">; por defecto es 0. 
</indent>

<indent>
• <@lit="gmp_bits">: Un entero, que debe ser una potencia de 2 con exponente entero (el valor predeterminado y mínimo es 256, y el máximo es 8192). Esto controla el número de bits que se utilizan para representar un número de punto flotante cuando se invoca la GMP (la Biblioteca de la Aritmética de Precisión Múltiple de GNU), sobre todo mediante la instrucción <@lit="mpols">. Los valores más grandes de ese entero proporcionan mayor precisión al coste de un mayor tiempo de cálculo. Esta configuración también se puede controlar por medio de la variable de entorno <@lit="GRETL_MP_BITS">. 
</indent>

<subhead>Generación de números aleatorios</subhead> 

<indent>
• <@lit="seed">: Un número natural positivo o la palabra clave <@lit="auto">. Establece la semilla para el generador de números pseudoaleatorios. Por defecto, esto se establece a partir del tiempo del sistema; pero si quieres generar secuencias repetibles de números aleatorios debes establecer la semilla manualmente. Para restablecer la semilla a un valor automático basado en el tiempo, usa <@lit="auto">. 
</indent>

<subhead>Estimación robusta</subhead> 

<indent>
• <@lit="bootrep">: Un entero. Establece el número de repeticiones de la instrucción <@ref="restrict"> con la opción <@opt="--⁠bootstrap">. 
</indent>

<indent>
• <@lit="garch_vcv">: <@lit="unset">, <@lit="hessian">, <@lit="im"> (matriz de información), <@lit="op"> (matriz de producto externo), <@lit="qml"> (estimador CMV o QML), o <@lit="bw"> (Bollerslev–Wooldridge). Especifica la variante que se va a utilizar para estimar la matriz de covarianzas de los coeficientes para modelos GARCH. Cuando indicas <@lit="unset"> (caso por defecto) entonces se utiliza la matriz Hessiana, excepto que se indique la opción “robust” para la instrucción garch, en cuyo caso se utiliza CMV (QML). 
</indent>

<indent>
• <@lit="arma_vcv">: <@lit="hessian"> (caso por defecto) u <@lit="op"> (matriz de producto externo). Especifica la variante que se va a utilizar cuando se calcula la matriz de covarianzas para modelos ARIMA. 
</indent>

<indent>
• <@lit="force_hc">: <@lit="off"> (por defecto) u <@lit="on">. Por defecto, con datos de series temporales y cuando indicas la opción<@opt="--⁠robust"> con <@lit="ols"> (MCO), se utiliza el estimador HAC. Si pones <@lit="force_hc"> en “on”, esto fuerza el cálculo de la Matriz de Covarianzas Consistente ante Heterocedasticidad (HCCM) habitual, que no tiene en cuenta la autocorrelación. Ten en cuenta que los VARs se tratan como un caso especial, pues cuando indicas la opción <@opt="--⁠robust"> el método por defecto es el de la HCCM habitual, pero puedes utilizar la opción <@opt="--⁠robust-hac"> para forzar que se emplee un estimador HAC. 
</indent>

<indent>
• <@lit="robust_z">: <@lit="off"> (por defecto) u <@lit="on">. Esto controla la distribución que se utiliza cuando se calculan las probabilidades asociadas (valores p) basadas en las desviaciones típicas robustas, en el contexto de los estimadores de mínimos cuadrados. Por defecto, GRETL utiliza la distribución <@mth="t"> de Student pero si activas <@lit="robust_z">, se utiliza una distribución Normal. 
</indent>

<indent>
• <@lit="hac_lag">: <@lit="nw1"> (por defecto), <@lit="nw2">, <@lit="nw3"> o un entero. Establece el valor del retardo máximo o ancho de banda (<@mth="p">) utilizado cuando se calculan las desviaciones típicas HAC (Consistentes ante Heterocedasticidad y Autocorrelación) utilizando el enfoque de Newey-West, para datos de series temporales. Las opciones <@lit="nw1"> y <@lit="nw2"> representan dos variantes de cálculo automático basadas en el tamaño de la muestra <@mth="T">: para nw1, <@fig="nw1">, y para nw2, <@fig="nw2">. La <@lit="nw3"> solicita una elección del ancho de banda que se basa en los datos. Consulta también más abajo <@lit="qs_bandwidth"> y <@lit="hac_prewhiten">. 
</indent>

<indent>
• <@lit="hac_kernel">: <@lit="bartlett"> (por defecto), <@lit="parzen"> o <@lit="qs"> (Espectral cuadrado). Establece el 'kernel', o patrón de ponderaciones, que se utiliza cuando se calculan las desviaciones típicas HAC. 
</indent>

<indent>
• <@lit="hac_prewhiten">: <@lit="on"> u <@lit="off"> (por defecto). Utiliza el 'blanqueo' previo y la 'vuelta a colorear' de Andrews-Monahan cuando se calculan las desviaciones típicas HAC. Esto también implica utilizar una elección del ancho de banda que se basa en los datos. 
</indent>

<indent>
• <@lit="hac_missvals">: <@lit="es"> (predeterminado), <@lit="am"> u <@lit="off">. Establece la política respecto al cálculo de las desviaciones típicas HAC cuando la muestra utilizada en la estimación incluye observaciones incompletas: <@lit="es"> invoca el método de la Misma Separación (Equal Spacing) de <@bib="Datta y Du (2012);datta-du12">; <@lit="am"> elige el método de la Modulación de la Amplitud (Amplitude Modulation) de <@bib="Parzen (1963);parzen63">; y <@lit="off"> provoca que GRETL rechace dicha estimación. Para obtener más detalles, consulta <@pdf="El manual de gretl#chap:robust_vcv"> (Capítulo 22). 
</indent>

<indent>
• <@lit="hc_version">: 0, 1, 2, 3 o 3a. Establece la variante que se usa al calcular las desviaciones típicas Consistentes ante Heterocedasticidad (HC) con datos de sección cruzada. Las 4 primeras opciones se corresponden con HC0, HC1, HC2 y HC3 discutidas por Davidson y MacKinnon en el capítulo 5 de <@itl="Econometric Theory and Methods">. HC0 produce las denominadas habitualmente como “desviaciones típicas de White”. La variante 3a es el procedimiento de la “navaja” de MacKinnon–White. A configuración por defecto habitualmente es 1, pero esto se puede cambiar en el cliente GUI, seleccionando la opción “HCCME” en el menú “Herramientas/Preferencias/General”. Ten en cuenta que una configuración realizada mediante GUI persiste a lo largo de las sesiones de GRETL, en oposición al uso de la instrucción <@lit="set"> que únicamente va a afectar a la sesión vigente. 
</indent>

<indent>
• <@lit="panel_robust">: <@lit="arellano"> (por defecto), <@lit="pcse"> o <@lit="scc">. Esto selecciona el estimador de la matriz de covarianzas robustas para ser utilizado con los modelos con datos de panel. Consulta a instrución <@ref="panel"> e o <@pdf="El manual de gretl#chap:robust_vcv"> (Capítulo 22) para obter máis detalles. 
</indent>

<indent>
• <@lit="qs_bandwidth">: Ancho de banda para la estimación HAC en caso de que selecciones el kernel Espectral Cuadrado (QS). (A diferencia de los 'kernels' de Bartlett y de Parzen, el ancho de banda QS no requiere ser un entero.) 
</indent>

<subhead>Series temporales</subhead> 

<indent>
• <@lit="horizon">: Un entero (por defecto se basa en la frecuencia de los datos). Establece el horizonte para las respuestas al impulso y las descomposiciones de la varianza de predicción en el contexto de autorregresiones de vectores. 
</indent>

<indent>
• <@lit="vecm_norm">: <@lit="phillips"> (por defecto), <@lit="diag">, <@lit="first"> o <@lit="none">. Usada en el contexto de la estimación VECM mediante la instrucción <@ref="vecm"> para identificar los vectores de cointegración. Consulta <@pdf="El manual de gretl#chap:vecm"> (Capítulo 33) para obtener más detalles. 
</indent>

<indent>
• <@lit="boot_iters">: Un entero, <@mth="B">. Establece el número de iteracciones 'bootstrap' que se usan cuando se calculan funciones de respuesta al impulso con intervalos de confianza. El valor por defecto es 1999. Es recomendable que <@mth="B"> + 1 sea siempre divisible por 100α/2 de forma que, por ejemplo con α = 0.1, <@mth="B">+1 debería ser múltiplo de 5. El mínimo valor aceptable para B es 499. 
</indent>

<subhead>Interacción con R</subhead> 

<indent>
• <@lit="R_lib">: <@lit="on ">(por defecto) u <@lit="off">. Cuando se envían instrucciones para que las ejecute R, utiliza la biblioteca compartida de R mejor que el ejecutable de R, si la biblioteca está disponible. 
</indent>

<indent>
• <@lit="R_functions">: <@lit="off"> (por defecto) u <@lit="on">. Reconoce funciones definidas en R como si fueran funciones propias (para eso se requiere el prefijo de asignación de nombres “<@lit="R.">”). Consulta <@pdf="El manual de gretl#chap:gretlR"> (Capítulo 44) para obtener más detalles sobre este elemento y el anterior. 
</indent>

<subhead>Miscelánea</subhead> 

<indent>
• <@lit="mpi_use_smt">: <@lit="on"> o bien <@lit="off"> (por defecto). Este interruptor afecta al número de procesos que se inician en un bloque <@lit="mpi"> dentro de un guion. Si el interruptor está en <@lit="off">, la cantidad por defecto de estos procesos es igual al número de núcleos físicos de la máquina local; si está en <@lit="on">, la cantidad por defecto de estos procesos es igual al número máximo de subprocesos, que coincidirá con el doble del número de núcleos físicos si estos pueden soportar SMT (Multiproceso Simultáneo, también denominado Hiperproceso). Esto se aplica únicamente si el usuario no indica el número de procesos, bien de forma directa o bien indirectamente (mediante la especificación de un archivo <@lit="hosts"> para utilizar con MPI). 
</indent>

<indent>
• <@lit="graph_theme">: una cadena de texto a elegir entre <@lit="altpoints">, <@lit="classic">, <@lit="dark2"> (la vigente por defecto), <@lit="ethan">, <@lit="iwanthue"> o <@lit="sober">. Esto establece el “tema” que se utiliza para los gráficos que genera GRETL. La opción <@lit="classic"> supone volver al sencillo tema que estaba vigente con antelación a la versión 2020c de GRETL. 
</indent>

# setinfo Dataset

Argumento: 	<@var="serie"> 
Opciones: 	<@lit="--description">=<@var="cadena"> (Establece la descripción)
		<@lit="--graph-name">=<@var="cadena"> (Establece el nombre del gráfico)
		<@lit="--discrete"> (Marca la serie como discreta)
		<@lit="--continuous"> (Marca la serie como continua)
		<@lit="--coded"> (Marca como una codificación)
		<@lit="--numeric"> (Marca como no codificación)
		<@lit="--midas"> (Marca como componente de datos de alta frecuencia)
Ejemplos: 	<@lit="setinfo x1 --description="Descripción de x1"">
		<@lit="setinfo y --graph-name="Alguna cadena"">
		<@lit="setinfo z --discrete">

Si activas las opciones <@opt="--⁠description"> o <@opt="--⁠graph-name">, el argumento debe ser una única serie; de lo contrario, podrá ser una lista de series, en cuyo caso la instrucción funciona sobre todos los elementos de la lista. Esta instrucción configura 4 atributos como se indica a continuación. 

Cuando indicas la opción <@opt="--⁠description"> seguida de una cadena de texto entre comillas, esa cadena se utiliza para establecer la etiqueta descriptiva de la variable. Esta etiqueta se muestra en respuesta a la instrucción <@ref="labels">, y también se muestra en la ventana principal del programa de Interfaz Gráfica de Usuario (GUI). 

Cuando especificas la opción <@opt="--⁠graph-name"> seguida de una cadena de texto entre comillas, esa cadena se va a utilizar en los gráficos en lugar del nombre de la variable. 

Cuando indicas uno de los dos indicadores de opción <@opt="--⁠discrete"> o <@opt="--⁠continuous">, el carácter numérico de la variable se establece en consonancia con eso. Por defecto, se tratan todas las series como continuas, entonces determinar que una serie sea discreta va a afectar al modo en que se maneja la variable en otras instrucciones y funciones, como por ejemplo con <@ref="freq"> o con <@xrf="dummify">. 

Cuando indicas alguna de las dos opciones <@opt="--⁠coded"> o <@opt="--⁠numeric">, el status de la serie indicada se establece de acuerdo con eso. Por defecto, se tratan todos los valores numéricos como que tienen sentido como tales, por lo menos en la acepción habitual; pero establecer que una serie es <@lit="coded"> quiere decir que los valores numéricos son una codificación arbitraria de características cualitativas. 

La opción <@opt="--⁠midas"> establece una indicación que alude a que una determinada serie contiene datos de una frecuencia mayor que la frecuencia base del conjunto de datos; por ejemplo, si el conjunto de datos es trimestral, y las series contienen valores para el mes 1, 2 o 3 de cada trimestre. (MIDAS = Mixed Data Sampling.) 

Menú gráfico: /Variable/Editar atributos
Otro acceso: Ventana principal: Menú emergente

# setmiss Dataset

Argumentos: 	<@var="valor"> [ <@var="listavariables"> ] 
Ejemplos: 	<@lit="setmiss -1">
		<@lit="setmiss 100 x2">

Permite que el programa interprete algún valor específico de dato numérico (el primer parámetro de la instrucción) como un código para “ausente”, en caso de importar datos. Cuando este valor es el único parámetro (como en el primer ejemplo de arriba), esa interpretación se va a aplicar a todas las series del conjunto de datos. Cuando <@var="valor"> va seguido de una lista de variables (indicadas por nombre o número), la interpretación se limita a la(s) variable(s) especificada(s). Así, en el segundo ejemplo, el valor 100 de los datos se interpreta como un código para “ausente”, pero solo para la variable <@lit="x2">. 

Menú gráfico: /Datos/Establecer código de valor ausente

# setobs Dataset

Variantes: 	<@lit="setobs"> <@var="periodicidad"> <@var="obsinicio">
		<@lit="setobs"> <@var="varunidades"> <@var="vartiempo"> <@lit="--panel-vars">
Opciones: 	<@lit="--cross-section"> (Interpreta como de sección cruzada)
		<@lit="--time-series"> (Interpreta como serie temporal)
		<@lit="--special-time-series"> (Mira abajo)
		<@lit="--stacked-cross-section"> (Interpreta como datos de panel)
		<@lit="--stacked-time-series"> (Interpreta como datos de panel)
		<@lit="--panel-vars"> (Utiliza variables índice, mira abajo)
		<@lit="--panel-time"> (Mira abajo)
		<@lit="--panel-groups"> (Mira abajo)
Ejemplos: 	<@lit="setobs 4 1990:1 --time-series">
		<@lit="setobs 12 1978:03">
		<@lit="setobs 1 1 --cross-section">
		<@lit="setobs 20 1:1 --stacked-time-series">
		<@lit="setobs unit year --panel-vars">

Esta instrucción fuerza al programa a interpretar que el conjunto de datos tiene una estructura específica. 

En la primera forma de la instrucción, debes indicar la <@var="periodicidad"> mediante un entero que represente la frecuencia en caso de que los datos sean series temporales (1 = anuales; 4 = trimestrales; 12 = mensuales; 52 = semanales; 5, 6, o 7 = diarios; 24 = horarios). En caso de datos de panel, la periodicidad indica el número de líneas por bloque de datos; por lo tanto, esto expresa o bien el número de unidades consecutivas cuando indicas que son 'secciones cruzadas apiladas', o bien el número de períodos de tiempo consecutivos cuando indicas 'series de tiempo apiladas'. En caso de datos simples de sección cruzada, la periodicidad debe establecerse en 1. 

La observación de inicio representa la fecha inicial, en caso de tratarse de datos de series temporales. Puedes indicar los años mediante 2 o 4 dígitos; y debes separar los subperíodos (por ejemplo, trimestres o meses) del año mediante dos puntos. En caso de datos de panel, debes indicar la observación inicial como 1:1, y en caso de datos de sección cruzada, como 1. Debes indicar las observaciones iniciales para datos diarios o semanales con el formato YYYY-MM-DD (o simplemente como 1 para datos sin fechar). 

Algunas periodicidades de series temporales tienen interpretaciones estándar (por ejemplo, 12 = mensuales y 4 = trimestrales). Pero si tienes datos de series temporales poco habituales para las que no se aplica la interpretación estándar, puedes señalar esto indicando la opción <@opt="--⁠special-time-series">. En ese caso, GRETL no va a advertir de que tus datos de (por ejemplo) frecuencia igual a 12, sean mensuales. 

Cuando no seleccionas un indicador de opción explícito para determinar la estructura de los datos, el programa va a tratar de adivinar la estructura a partir de la información proporcionada. 

La segunda forma de la instrucción (que requiere que indiques la opción <@opt="--⁠panel-vars">) puede utilizarse para imponer una interpretación de panel, cuando el conjunto de datos contiene variables que identifican de forma inequívoca las unidades de sección cruzada y los períodos de tiempo. El conjunto de datos se va a ordenar como series de tiempo apiladas, en función de los valores ascendentes de la variable de unidades (<@var="varunidades">). 

<subhead>Opciones específicas de Panel</subhead> 

Puedes usar opciones <@opt="--⁠panel-time"> y <@opt="--⁠panel-groups"> únicamente con un conjunto de datos que ya fue definido previamente como un panel. 

La intención de la opción <@opt="--⁠panel-time"> es determinar información adicional relacionada con la dimensión temporal del panel. Debes indicar esta siguiendo el patrón del primer formato de <@lit="setobs"> apuntado más arriba. Por ejemplo, puedes utilizar la siguiente forma de indicar que la dimensión temporal de un panel es trimestral, comenzando en el primer trimestre de 1990: 

<code>          
   setobs 4 1990:1 --panel-time
</code>

La intención de la opción <@opt="--⁠panel-groups"> es crear una serie con valores en cadenas de texto, que contenga los nombres de los grupos (individuos, unidades atemporales) del panel. (Esto se va a utilizar cuando sea adecuado en gráficos de panel.) Con esta opción indicas uno o dos argumentos, como se indica a continuación. 

Primer caso: Un único argumento es el nombre de una serie con valores en cadenas de texto. Si el número de valores diferentes es igual al número de grupos del panel, esa serie se utiliza para definir los nombres de los grupos. Si resulta necesario, el contenido numérico de la serie se va a ajustar de forma que los valores sean todos 1 para el primer grupo, todos 2 para el segundo grupo, etcétera. Cuando el número de valores diferentes en cadenas de texto no coincide con el número de grupos, se muestra un fallo. 

Segundo caso: El primer argumento es el nombre de una serie, y el segundo es una cadena de texto literal o una variable de cadena que contiene un nombre para cada grupo. Las series se van a generar si no existen ya. Cuando el segundo argumento es una cadena de texto literal o una variable de cadena, los nombres de los grupos deben estar separados por espacios; pero se un nombre incluye espacios, debe delimitarse con comillas precedidas (cada una) de barra inversa. Alternativamente, el segundo argumento puede ser un array de cadenas de texto. 

Por ejemplo, el siguiente código genera una serie que se va llamar <@lit="Estado"> en la que los nombres de la cadena <@lit="cstrs"> se repiten cada uno <@mth="T"> veces, y siendo <@mth="T"> la longitud de las series de tiempo del panel. 

<code>          
   string cstrs = sprintf("Francia Alemania Italia \"Reino Unido\"")
   setobs Estado cstrs --panel-groups
</code>

Menú gráfico: /Datos/Estructura del conjunto de datos

# setopt Programming

Argumentos: 	<@var="instrucción"> [ <@var="acción"> ] <@var="opciones"> 
Ejemplos: 	<@lit="setopt mle --hessian">
		<@lit="setopt ols persist --quiet">
		<@lit="setopt ols clear">
		Ver también <@inp="gdp_midas.inp">

Esta instrucción permite la configuración previa de opciones para una instrucción concreta. Normalmente esto no hace falta, pero puede ser útil para los autores de funciones en HANSL, cuando quieren hacer que algunas opciones de las instrucciones estén condicionadas al valor de un argumento que proporcione quien las solicita. 

Por ejemplo, supón que una función ofrece un interruptor booleano “<@lit="quiet">”, cuya intención es que se suprima la presentación de resultados de una determinada regresión que se ejecuta dentro de la propia función. En ese caso, se podría escribir: 

<code>          
   if quiet
     setopt ols --quiet
   endif
   ols ...
</code>

Entonces, la opción <@opt="--⁠quiet"> se va a aplicar a la siguiente instrucción <@lit="ols"> únicamente si la variable <@lit="quiet"> tiene un valor no nulo. 

Por defecto, las opciones que se establecen de este modo solo se aplican a la siguiente petición de la <@var="instrucción">; por lo que no son persistentes. Sin embargo, si indicas <@lit="persist"> como valor para <@var="acción">, las opciones se continuarán aplicando a la instrucción indicada hasta nuevo aviso. El 'antídoto' a la acción <@lit="persist"> es <@lit="clear">, pues este elimina cualquier configuración guardada para la instrucción especificada. 

Debes tener en cuenta que las opciones establecidas mediante <@lit="setopt"> se combinan con cualquier opción agregada directamente a la instrucción apuntada. Así, por ejemplo, se puede añadir la opción <@opt="--⁠hessian"> a una instrucción <@lit="mle"> de forma incondicional, pero utilizar <@lit="setopt"> para añadir <@opt="--⁠quiet"> de forma condicional. 

# shell Utilities

Argumento: 	<@var="instrucshell"> 
Ejemplos: 	<@lit="! ls -al">
		<@lit="! dir c:\users">
		<@lit="launch notepad">
		<@lit="launch emacs myfile.txt">

La prestación que se describe aquí no está activada por defecto. Mira más abajo para los detalles. 

Un signo de exclamación (<@lit="!">) al comienzo de una línea de instrucción se interpreta como una escapada del intérprete de usuario. Así puedes ejecutar instrucciones del intérprete a tu antojo desde dentro de Gretl. El argumento <@var="instrucshell"> se pasa a <@lit="/bin/sh"> en sistemas de tipo Unix como Linux y macOS, o a <@lit="cmd.exe"> en MS Windows. Se ejecuta de forma sincrónica; es decir, Gretl va a esperar a que se complete la instrucción antes de proseguir. Si la instrucción da como resultado algún texto, este se presenta en la consola o en la ventana de resultados de guiones. 

Una variante del acceso síncrono con el intérprete, permite al usuario “capturar” el resultado de una instrucción en una variable de cadena de texto. Esto se puede lograr envolviendo la instrucción entre paréntesis, precedidos por un signo dólar, como en 

<code>          
   string s = $(ls -l $HOME)
</code>

Por otro lado, la clave <@lit="launch">, ejecuta un programa externo de forma asíncrona (sin esperar a que se complete), como en el tercer y en el cuarto ejemplos de arriba. Esto está pensado para abrir una aplicación en modo interactivo. La <@lit="RUTA"> del usuario se va a buscar para el ejecutable especificado. En MS Windows, la instrucción se ejecuta directamente, sin pasarla a <@lit="cmd.exe"> (de ese modo las variables de entorno no se expanden automáticamente). 

<subhead>Activación</subhead> 

Por razones de seguridad, la prestación de acceso con el intérprete no se permite por defecto. Para activarla, marca el recuadro “Permitir instrucciones de shell” bajo el menú Herramientas/Preferencias/General en el programa de Interfaz Gráfica de usuario (GUI). Esto también hace que estén disponibles las instrucciones del intérprete en el programa de instrucciones en líneas (y resulta el único modo de hacerlo). 

# smpl Dataset

Variantes: 	<@lit="smpl"> <@var="obsinicio obsfin">
		<@lit="smpl"> <@var="+i -j">
		<@lit="smpl"> <@var="varficticia"> <@lit="--dummy">
		<@lit="smpl"> <@var="condición"> <@lit="--restrict">
		<@lit="smpl"> <@lit="--no-missing [ "><@var="listavariables"> <@lit="]">
		<@lit="smpl"> <@lit="--no-all-missing [ "><@var="listavariables"> <@lit="]">
		<@lit="smpl"> <@lit="--contiguous [ "><@var="listavariables"> <@lit="]">
		<@lit="smpl"> <@var="n"> <@lit="--random">
		<@lit="smpl full">
		<@lit="smpl">
Opciones: 	<@lit="--dummy"> (El argumento es una variable ficticia)
		<@lit="--restrict"> (Aplica una restricción booleana)
		<@lit="--replace"> (Substituye cualquier restricción booleana existente)
		<@lit="--no-missing"> (Limitarse a observaciones válidas)
		<@lit="--no-all-missing"> (Omite observaciones vacías (mira abajo))
		<@lit="--contiguous"> (Mira abajo)
		<@lit="--random"> (Genera una submuestra aleatoria)
		<@lit="--permanent"> (Mira abajo)
		<@lit="--preserve-panel"> (Datos de panel: mira abajo)
		<@lit="--unit"> (Datos de panel: muestra en la dimensión de sección cruzada)
		<@lit="--time"> (Datos de panel: muestra en la dimensión temporal)
		<@lit="--dates"> (Interpreta los números de observación como fechas)
		<@lit="--quiet"> (No muestra el rango muestral)
Ejemplos: 	<@lit="smpl 3 10">
		<@lit="smpl 1960:2 1982:4">
		<@lit="smpl +1 -1">
		<@lit="smpl x > 3000 --restrict">
		<@lit="smpl y > 3000 --restrict --replace">
		<@lit="smpl 100 --random">

Esta instrución solo puede utilizarse cuando está preparado un conjunto de datos. Cuando no indicas ningún argumento, presenta el rango muestral vigente; de outro modo, establece el rango muestral. Puedes definir el rango de varias formas. En la primera alternativa (y en los dos primeros ejemplos) de arriba, <@var="obsinicio"> y <@var="obsfin"> deben ser consistentes con la periodicidad de los datos. Puedes substituir cualquiera de los dos mediante un punto y coma para dejar ese valor sin cambiar. (Para más detalles sobre <@var="obsinicio"> y <@var="obsfin">, consulta la sección titulada “Fechas versus Índices secuenciales” más abajo.) En la segunda forma, los números enteros <@var="i"> y <@var="j"> (pueden ser positivos o negativos, y deben tener su signo) se consideran como variaciones en relación al rango de la muestra existente. En la tercera forma, <@var="varficticia"> debe ser una variable de señalización con valores 0 o 1 en cada observación; así la muestra se va a restringir a las observaciones en las que el valor es 1. La cuarta forma, que utiliza <@opt="--⁠restrict">, restringe la muestra a las observaciones que cumplen la condición booleana que se indica. 

Puedes emplear las opciones <@opt="--⁠no-missing"> y <@opt="--⁠no-all-missing"> para excluir de la muestra aquellas observaciones para las que hay ausencia de datos. La primera variante excluye aquellas filas del conjunto de datos para las que, por lo menos una variable, tiene un valor ausente; mientras que la segunda variante excluye únicamente aquellas filas en las que <@itl="todas"> las variables tienen valores ausentes. En cada caso, la comprobación se limita a las variables de <@var="listavariables"> cuando indicas este argumento; de lo contrario, se aplica a todas las series (con la reserva de que, en caso de no tener <@var="listavariables"> e indicar <@opt="--⁠no-all-missing">, las variables genéricas <@lit="index"> y <@lit="time"> se ignoran). 

La opción <@opt="--⁠contiguous"> de <@lit="smpl"> está pensada para usar con datos de series temporales. Su efecto consiste en recortar cualquier observación al comienzo y al final del rango de la muestra vigente que contenga valores ausentes (bien para las variables de <@var="listavariables">, o bien para todas las series de datos si no indicas <@var="listavariables">). Entonces se realiza una verificación para comprobar si hay algún valor ausente en el rango que queda; y si es así, se muestra un fallo. 

Con la opción <@opt="--⁠random">, el número de casos especificado se escoge aleatoriamente del conjunto vigente de datos (sin substitución). Si quieres ser capaz de replicar esa selección, debes establecer primero la semilla para el generador de números aleatorios (consulta la instrucción <@ref="set">). 

La forma final (<@lit="smpl full">) restablece el rango completo de datos. 

Ten en cuenta que las restricciones muestrales son, por defecto, acumulativas; es decir, el punto de partida de cualquier instrucción <@lit="smpl"> es la muestra vigente. Si quieres que la instrucción actúe substituyendo cualquier restricción ya existente, puedes añadir el indicador de opción <@opt="--⁠replace"> al final de la instrucción. (Pero esta opción no es compatible con la opción <@opt="--⁠contiguous">.) 

Puedes utilizar la variable interna <@lit="obs"> junto con la opción <@opt="--⁠restrict"> de <@lit="smpl"> para excluir observaciones concretas de la muestra. Por ejemplo 

<code>          
   smpl obs!=4 --restrict
</code>

va a prescindir únicamente de la cuarta observación. Si los casos de los datos se identifican mediante etiquetas, 

<code>          
   smpl obs!="USA" --restrict
</code>

va a prescindir de la observación con la etiqueta “USA”. 

Debe apuntarse una cuestión relacionada con las opciones <@opt="--⁠dummy">, <@opt="--⁠restrict"> y <@opt="--⁠no-missing"> de la instrucción <@lit="smpl">: la información “estructural” del archivo de datos (relacionada con la naturaleza de series de tiempo o de panel, de los datos) probablemente se va a perder cuando se ejecute esta instrucción; pero puedes volver a imponer la estructura con la instrucción <@ref="setobs"> (consulta también la opción <@opt="--⁠preserve-panel"> más abajo). 

<subhead>Fechas versus Índices secuenciales</subhead> 

Puedes utilizar la opción <@opt="--⁠dates"> para solucionar alguna posible ambigüedad al interpretar <@var="obsinicio"> y <@var="obsfin"> en caso de usar datos de series de tiempo anuales. Por ejemplo, ¿debería considerarse que <@lit="2010"> se refiere al año 2010, o a la dos mil décima observación? En la mayoría de los casos, esto debiera salir bien automáticamente, pero puedes forzar la interpretación en forma de fecha si lo necesitas. Esta opción también se puede utilizar con datos que estén fechados diariamente para lograr que <@lit="smpl"> interprete, por ejemplo, 20100301 como el primero de marzo de 2010 en lugar de un índice secuencial corriente. Ten en cuenta que esta ambigüedad no aparece con las frecuencias de series de tiempo que sean distintas a la anual y a la diaria; fechas como 1980:3 (tercer trimestre de 1980) y 2020:03 (marzo de 2020) no se pueden confundir con índices corrientes. 

<subhead>Opciones específicas para datos de panel</subhead> 

Las opciones <@opt="--⁠unit"> y <@opt="--⁠time"> son específicas para datos de panel. Te permiten indicar, respectivamente, un rango de “unidades” o de períodos de tiempo. Por ejemplo: 

<code>          
   # Limita la muestra a las primeras 50 unidades
   smpl 1 50 --unit
   # Limita la muestra a los períodos de tiempo de 2 a 20
   smpl 2 20 --time
</code>

Cuando se especifica la dimensión temporal de un conjunto de datos de panel por medio de la instrucción <@ref="setobs"> con la opción <@opt="--⁠panel-time">, la instrucción <@lit="smpl"> con la opción <@opt="--⁠time"> puede expresarse en términos de fechas en lugar de números de observación planos. Este es un ejemplo: 

<code>          
   # Indicar el tiempo de un panel como trimestral, empezando en el primero de 1990
   setobs 4 1990:1 --panel-time
   # Limitar la muestra desde 2000:1 hasta 2007:1
   smpl 2000:1 2007:1 --time
</code>

En GRETL, un conjunto de datos de panel debe estar siempre “teóricamente equilibrado ”—es decir, cada unidad debe tener el mismo número de filas de datos, aunque algunas filas no contengan más que <@lit="NA">s. Extraer una submuestra mediante las opciones <@opt="--⁠restrict"> o <@opt="--⁠dummy"> puede destruir esta estructura. En tal caso, puedes añadir la opción <@opt="--⁠preserve-panel"> para solicitar que se reconstituya un panel teóricamente equilibrado, por medio de la inserción de las “filas ausentes” que hagan falta. 

<subhead>Establecer la muestra como permanente o provisional</subhead> 

Por defecto, puedes deshacer las limitaciones que establezcas sobre el rango de la muestra vigente, pues ejecutando <@lit="smpl full"> puedes restaurar el conjunto de datos completo. Sin embargo, puedes utilizar la opción <@opt="--⁠permanent"> para sustituir el conjunto de datos restringido en lugar del original. El efecto de indicar la opción <@opt="--⁠permanent"> sin otros argumentos ni opciones, es el de reducir la base de datos al rango de la muestra vigente. 

Consulta <@pdf="El manual de gretl#chap:sampling"> (Capítulo 5) para obtener otros detalles. 

Menú gráfico: /Muestra

# spearman Statistics

Argumentos: 	<@var="serie1"> <@var="serie2"> 
Opción: 	<@lit="--verbose"> (Presenta los datos por rangos)

Presenta el coeficiente de la correlación por rangos de Spearman para las series <@var="serie1"> y <@var="serie2">. No tienes que jerarquizar manualmente las variables por adelantado, pues la función ya se ocupa de ello. 

La forma automática de jerarquizar es de mayor a menor (i.e. el valor más grande de los datos adquiere el rango 1). Si necesitas invertir esta forma de jerarquizar, genera una nueva variable que sea la negativa de la original. Por ejemplo: 

<code>          
   series altx = -x
   spearman altx y
</code>

Menú gráfico: /Herramientas/Contrastes no paramétricos/Correlación

# square Transformations

Argumento: 	<@var="listavariables"> 
Opción: 	<@lit="--cross"> (Genera los productos cruzados así como los cuadrados)

Genera nuevas series que son los cuadrados de las series de <@var="listavariables"> (además de las variables con los productos cruzados entre cada dos, cuando indicas la opción <@opt="--⁠cross">). Por ejemplo, <@lit="square x y"> va a generar las variables <@lit="sq_x"> = <@lit="x"> al cuadrado, <@lit="sq_y"> = <@lit="y"> al cuadrado y (opcionalmente con 'cross') <@lit="x_y"> = <@lit="x"> por <@lit="y">. Cuando una determinada variable es una variable ficticia, no se calcula su cuadrado pues obtendríamos la misma variable. 

Menú gráfico: /Añadir/Cuadrados de las variables seleccionadas

# stdize Transformations

Argumento: 	<@var="listavar"> 
Opciones: 	<@lit="--no-df-corr"> (Sin corrección de grados de libertad)
		<@lit="--center-only"> (Sin dividir por desviación típica)

Por defecto, se obtiene una versión tipificada de cada una de las variables de <@var="listavar">, y cada resultado se guarda en una nueva serie con el prefijo <@lit="s_">. Por ejemplo, la expresión <@lit="stdize x y"> crea las nuevas series <@lit="s_x"> y <@lit="s_y">, cada una como resultado de centrar y dividir la original por su desviación típica muestral (con la corrección de 1, en los grados de libertad). 

Cuando indicas la opción <@opt="--⁠no-df-corr">, no se aplica ninguna corrección de los graos de libertad en la desviación típica que se utiliza; será el estimador máximo-verosímil. Si indicas la opción <@opt="--⁠center-only">, las series resultan de únicamente restar la media y, en ese caso, los nombres de las resultantes tendrán el prefijo <@lit="c_"> en lugar de <@lit="s_">. 

La funcionalidad de esta instrucción está disponible de forma en cierto modo más flexible, mediante la función <@xrf="stdize">. 

Menú gráfico: /Añadir/Tipificar las variables seleccionadas

# store Dataset

Argumentos: 	<@var="nombrearchivo"> [ <@var="listavariables"> ] 
Opciones: 	<@lit="--omit-obs"> (Mira abajo, sobre el formato CSV)
		<@lit="--no-header"> (Mira abajo, sobre el formato CSV)
		<@lit="--gnu-octave"> (Utiliza el formato GNU Octave)
		<@lit="--gnu-R"> (Formato tratable con read.table)
		<@lit="--gzipped">[=<@var="nivel">] (Aplica la compresión gzip)
		<@lit="--jmulti"> (Utiliza el formato ASCII JMulti)
		<@lit="--dat"> (Utiliza el formato ASCII PcGive)
		<@lit="--decimal-comma"> (Utiliza la coma como carácter decimal)
		<@lit="--database"> (Utiliza el formato de base de datos de GRETL)
		<@lit="--overwrite"> (Mira abajo, sobre el formato de base de datos)
		<@lit="--comment">=<@var="cadena"> (Mira abajo)
		<@lit="--matrix">=<@var="nombrematriz"> (Mira abajo)

Guarda los datos en <@var="nombrearchivo">. Por defecto, se guardan todas las series ya definidas en ese momento, pero puedes utilizar el argumento <@var="listavariables"> (opcional) para escoger un subconjunto de series. Si el conjunto de datos es una submuestra, solo se guardan las observaciones del rango vigente de la muestra. 

El archivo resultante va a escribirse en el directorio (<@ref="workdir">) establecido en ese momento, excepto que la cadena <@var="nombrearchivo"> contenga una especificación completa de la ruta. 

Ten en cuenta que la instrucción <@lit="store"> se comporta de modo especial en el contexto de un “bucle progresivo”; consulta <@pdf="El manual de gretl#chap:looping"> (Capítulo 13) para obtener más detalles. 

<subhead>Formatos propios</subhead> 

Si <@var="nombrearchivo"> tiene extensión <@lit=".gdt"> o <@lit=".gtdb">, ello implica que se guarden los datos en uno de los formatos propios de GRETL. Además, si no indicas una extensión, se considera implícitamente la <@lit=".gdt">, añadiéndose automáticamente este sufijo. El formato <@lit="gdt"> es de tipo XML, con opción de compresión gzip; mientras que el formato <@lit="gdtb"> es binario. El primero se recomienda para conjuntos de datos de tamaño moderado (digamos, hasta varios cientos de kilobytes de datos); con el formato binario es mucho mayor la velocidad con conjuntos de datos muy grandes. 

Cuando guardas los datos en formato <@lit="gdt">, puedes utilizar la opción <@opt="--⁠gzipped"> para comprimirlos. El parámetro (opcional) de esta opción controla el nivel de compresión (de 0 a 9): los valores mayores generan un archivo más pequeño, pero la compresión lleva más tiempo. El nivel por defecto es el 1; y un nivel de 0 significa que no se aplica ninguna compresión. 

Se admite un tipo especial de guardado “propio” en el programa de interfaz GUI: si <@var="nombrearchivo"> tiene extensión <@lit=".gretl"> y omites el argumento <@var="listavariables">, entonces se graba un archivo de sesión de GRETL. Este tipo de archivos incluyen el conjunto de datos vigente junto con cualesquiera objetos que tengan nombre, como modelos, gráficos y matrices. 

<subhead>Otros formatos</subhead> 

Hasta cierto punto, puedes controlar el formato en el que se escriben los datos mediante la extensión o sufijo de <@var="nombrearchivo">, como se indica a continuación: 

<indent>
• <@lit=".csv">: Valores Separados por Comas (CSV). 
</indent>

<indent>
• <@lit=".txt"> o <@lit=".asc">: valores separados por espacios. 
</indent>

<indent>
• <@lit=".m">: Formato matricial GNU Octave. 
</indent>

<indent>
• <@lit=".dta">: Formato dta de Stata (versión 113). 
</indent>

Puedes usar los indicadores de opción relacionados con el formato mostrados arriba para forzar la elección del formato, con independencia del nombre del archivo (o para lograr que GRETL escriba en los formatos de PcGive o JMulTi). 

<subhead>Opciones CSV</subhead> 

Los indicadores de opción <@opt="--⁠omit-obs"> y <@opt="--⁠no-header"> son específicos para guardar datos en el formato CSV. Por defecto, si los datos son series temporales o de panel, o si el conjunto de datos incluye marcadores específicos de observación, el archivo resultante incluye una primera columna que identifica las observaciones (e.g. por fecha). Cuando indicas la opción <@opt="--⁠omit-obs">, esta columna se omite. La opción <@opt="--⁠no-header"> elimina la habitual representación de los nombres de las variables en el encabezamiento de las columnas. 

El indicador de opción <@opt="--⁠decimal-comma"> está también limitado a CSV. Su efecto consiste en substituir el punto decimal con la coma decimal; y, por añadido, se fuerza a que el separador de columnas sea el punto y coma, en lugar de la coma. 

<subhead>Guardar en una base de datos</subhead> 

La posibilidad de guardar en el formato de base de datos de GRETL está pensada construir conjuntos largos de series, con mezclas de frecuencias y rangos de observaciones. En este momento, esta opción solo está disponible para datos de series temporales de tipo anual, trimestral o mensual, o para datos sin fecha (de sección cruzada). Una base de datos de GRETL toma la forma de dos archivos: uno con sufijo <@lit=".bin"> para guardar los datos en formato binario, y un archivo de texto plano con el sufijo <@lit=".idx"> para los metadatos. Para indicar el nombre del archivo de salida en la linea de instrucciones, debes indicar el sufijo <@lit=".bin"> o no indicar ninguno. 

Si se guarda en una base de datos que ya existe, el efecto por defecto consiste en añadir series al contenido existente en la base de datos. En este contexto, es un fallo que alguna de las series que se van a guardar tenga el mismo nombre que alguna que ya está presente en la base de datos. La opción <@opt="--⁠overwrite"> tiene como efecto que, si hay nombres de variables en común, los datos recientemente guardados sustituyen a los valores previos. 

La opción <@opt="--⁠comment"> está disponible cuando se guardan datos como base de datos o como CSV. El parámetro que se requiere es una cadena en una línea, entre comillas, ligada al indicador de opción mediante un signo de igualdad. La cadena de texto se inserta como comentario en el archivo índice de la base de datos o en el encabezamiento del CSV. 

<subhead>Escribir una matriz como conjunto de datos</subhead> 

La opción <@opt="--⁠matriz"> necesita un parámetro: el nombre de una matriz (que no esté vacía). Entonces la consecuencia de la instrucción <@lit="store"> es efectivamente convertir la matriz en un conjunto de datos “en segundo plano”, y escribirlo como tal en un archivo. Las columnas de la matriz pasan a ser series cuyos nombres se toman de los nombres adyacentes a las columnas de la matriz (si los hay), o bien se asignan por defecto como <@lit="v1">, <@lit="v2">, etc. Si la matriz tiene nombres adyacentes a las filas, estos se utilizan en el conjunto de datos como “etiquetas de las observaciones”. 

Ten en cuenta que puedes escribir las matrices como tales en archivos, consulta para ello la función <@xrf="mwrite">. Pero a veces te puede resultar útil escribirlas en forma de conjuntos de datos. 

Menú gráfico: /Archivo/Guardar datos; /Archivo/Exportar datos

# summary Statistics

Variantes: 	<@lit="summary ["> <@var="listavariables"> ]
		<@lit="summary --matrix="><@var="nombrematriz">
Opciones: 	<@lit="--simple"> (Solo estadísticos básicos)
		<@lit="--weight">=<@var="wtvar"> (Variable de ponderación)
		<@lit="--by">=<@var="byvar"> (Mira abajo)
Ejemplos: 	<@inp="frontier.inp">

En su primera forma, esta instrucción presenta un resumen estadístico de las variables de <@var="listavariables">, o de todas las variables del conjunto de datos cuando omites <@var="listavariables">. Por defecto, el resultado consiste en la media, mediana, mínimo, máximo, desviación típica (sd), coeficiente de variación (= sd/media), coeficiente de asimetría, exceso de curtosis, percentiles del 5% y 95%, rango intercuartílico y número de observaciones ausentes. Pero cuando indicas la opción <@opt="--⁠simple">, el resultado se limita a la media, la mediana, la desviación típica, el mínimo y el máximo. 

Si indicas la opción <@opt="--⁠weight">, en cuyo caso el parámetro <@var="wvar"> debería ser el nombre de una serie que ofrezca las ponderaciones de cada observación, los estadísticos se ponderan de acuerdo con ello. 

Cuando indicas la opción <@opt="--⁠by">, en cuyo caso el parámetro <@var="byvar"> debe ser el nombre de una variable discreta, entonces se presentan los estadísticos para las submuestras que se corresponden con los diferentes valores que toma <@var="byvar">. Por ejemplo, cuando <@var="byvar"> es una variable ficticia (binaria), se presentan los estadísticos para los casos en los que <@lit="byvar=0"> y <@lit="byvar=1">. Advertencia: En este momento, esta opción es incompatible con la opción <@opt="--⁠weight">. 

Cuando indicas la forma alternativa, utilizando una matriz ya definida, entonces se presenta el resumen estadístico para cada columna de la matriz. En este caso, la opción <@opt="--⁠by"> no está disponible. 

Puedes obtener, en forma de matriz, la tabla de estadísticos generada con la instrucción <@lit="summary">, por medio del accesor <@xrf="$result">. Cuando indiques la opción <@opt="--⁠by">, se genera el efecto de este acceso unicamente si <@var="varlist"> contiene una única serie. 

Consulta también la función <@xrf="aggregate"> para ver otras formas más flexibles de producción de estadísticos “factorizados”. 

Menú gráfico: /Ver/Estadísticos principales
Otro acceso: Ventana principal: Menú emergente

# system Estimation

Variantes: 	<@lit="system method="><@var="estimador">
		<@var="sysname"><@lit=" <- system">
Ejemplos: 	<@lit=""Klein Model 1" <- system">
		<@lit="system method=sur">
		<@lit="system method=3sls">
		Ver también <@inp="klein.inp">, <@inp="kmenta.inp">, <@inp="greene14_2.inp">

Empieza un sistema de ecuaciones. Puedes indicar una de las dos formas de la instrucción, dependiendo de si quieres guardar el sistema para estimarlo de varias formas, o solo estimar el sistema una vez. 

Para guardar el sistema debes asignarle un nombre como en el primer ejemplo (si el nombre contiene espacios, debes delimitarlo con comillas). En este caso, se estima el sistema utilizando la instrucción <@ref="estimate">. Con un sistema de ecuaciones ya guardado, puedes imponer restricciones (incluidas restricciones entre ecuaciones) utilizando la instrucción <@ref="restrict">. 

Como alternativa, puedes especificar un estimador para el sistema utilizando <@lit="method="> seguido de una cadena que identifique uno de los estimadores admitidos: <@lit="ols"> (Mínimos Cuadrados Ordinarios), <@lit="tsls"> (Mínimos Cuadrados en 2 Etapas) <@lit="sur"> (Regresiones Aparentemente No Relacionadas), <@lit="3sls"> (Mínimos Cuadrados en 3 Etapas), <@lit="fiml"> (Máxima Verosimilitud con Información Total) o <@lit="liml"> (Máxima Verosimilitud con Información Limitada). En este caso, el sistema se estima una vez que esté completa su definición. 

Un sistema de ecuaciones se termina con la línea <@lit="end system">. Dentro del sistema pueden indicarse 4 tipos de enunciado, como los siguientes. 

<indent>
• <@ref="equation">: Especifica una ecuación del sistema. 
</indent>

<indent>
• <@lit="instr">: Para estimar un sistema mediante Mínimos Cuadrados en 3 etapas, se indica una lista de instrumentos (mediante el nombre de la variable o su número). Alternativamente, puedes poner esta información en la línea <@lit="equation"> usando la misma sintaxis que en la instrucción <@ref="tsls">. 
</indent>

<indent>
• <@lit="endog">: Para un sistema de ecuaciones simultáneas, se indica una lista de variables endógenas. En principio, esto está pensado para utilizar con la estimación FIML, pero puedes utilizar este enfoque con Mínimos Cuadrados en 3 Etapas en lugar de indicar una lista <@lit="instr">; y entonces todas las variables que no se identifiquen como endógenas, se van a utilizar como instrumentos. 
</indent>

<indent>
• <@lit="identity">: Para utilizar con Máxima Verosimilitud con Información Completa (MVIC, FIML), se indica una identidad que enlaza dos o más variables del sistema. Este tipo de enunciado se ignora cuando se utiliza un estimador diferente al de MVIC. 
</indent>

Después de hacer la estimación utilizando las instrucciones <@lit="system"> o <@lit="estimate">, puedes usar los siguientes accesores para recoger información adicional: 

<indent>
• <@lit="$uhat">: Matriz con los errores de estimación, con una columna por ecuación. 
</indent>

<indent>
• <@lit="$yhat">: Matriz con los valores ajustados, con una columna por ecuación. 
</indent>

<indent>
• <@lit="$coeff">: Vector columna con los coeficientes de las ecuaciones (todos los coeficientes de la primera ecuación, seguidos por los de la segunda ecuación, etcétera). 
</indent>

<indent>
• <@lit="$vcv">: Matriz con las covarianzas entre los coeficientes. Cuando hay <@mth="k"> elementos en el vector <@lit="$coeff">, esta matriz tiene una dimensión de <@mth="k"> por <@mth="k">. 
</indent>

<indent>
• <@lit="$sigma">: Matriz con las covarianzas entre los errores de estimación de las ecuaciones cruzadas. 
</indent>

<indent>
• <@lit="$sysGamma">, <@lit="$sysA"> y <@lit="$sysB">: Matrices con los coeficientes en la forma estructural (mira abajo). 
</indent>

Si quieres recuperar los errores de estimación o los valores ajustados para una ecuación en concreto, en forma de serie de datos, escoge una columna de la matriz <@lit="$uhat"> o <@lit="$yhat">, y asígnale la serie como en 

<code>          
   series uh1 = $uhat[,1]
</code>

Las matrices en la forma estructural se corresponden con la siguiente representación de un modelo de ecuaciones simultáneas: 

  <@fig="structural">

Si hay <@mth="n"> variables endógenas y <@mth="k"> variables exógenas, Γ es una matriz de dimensión <@itl="n">×<@itl="n"> y <@mth="B"> es <@itl="n">×<@itl="k">. Cuando el sistema no contiene ningún retardo de las variables endógenas, entonces la matriz <@mth="A"> no está presente. Si el retardo máximo de un regresor endógeno es <@mth="p">, la matriz <@mth="A"> es de dimensión <@itl="n">×<@itl="np">. 

Menú gráfico: /Modelo/Ecuaciones simultáneas

# tabprint Printing

Opciones: 	<@lit="--output">=<@var="nombrearchivo"> (Envía el resultado al archivo especificado)
		<@lit="--format="f1|f2|f3|f4""> (Especifica el formato TeX personalizado)
		<@lit="--complete"> (Relacionado con TeX, mira abajo)

Debe ir después de la estimación de un modelo y presenta ese modelo en formato de tabla. El formato se rige por la extensión del <@var="nombrearchivo"> especificado: “<@lit=".tex">” para LaTeX, “<@lit=".rtf">” para RTF (Microsoft's Rich Text Format) o “<@lit=".csv">” para el formato con separación mediante comas. El archivo resultante va a escribirse en el directorio vigente (<@ref="workdir">), excepto que la cadena <@var="nombrearchivo"> contenga una especificación completa de la ruta. 

Cuando seleccionas el formato CSV, los valores se separan con comas excepto que la coma decimal esté en vigor, en cuyo caso el separador es el punto y coma. 

<subhead>Opciones específicas de resultados en LaTeX</subhead> 

Cuando indicas la opción <@opt="--⁠complete">, el archivo LaTeX es un documento completo, listo para procesar; de lo contrario, debe incluirse en un documento. 

Si quieres modificar la apariencia del resultado tabular, puedes especificar un formato personalizado en filas utilizando la opción <@opt="--⁠format">. La cadena de formato debe estar puesta entre comillas y debe estar ligada a la opción con un signo de igualdad. El patrón para las cadenas de formato es el siguiente. Existen 4 campos que representan: el coeficiente, la desviación típica, el ratio <@mth="t"> y la probabilidad asociada, respectivamente. Debes separar estos campos mediante barras verticales; y, o bien pueden tener una especificación de tipo <@lit="printf"> para el formato del valor numérico en cuestión, o bien pueden dejarse en blanco para eliminar la presentación de esa columna (sujeto esto a la condición de que no puedes dejar todas las columnas en blanco). Aquí tienes unos pocos ejemplos: 

<code>          
   --format="%.4f|%.4f|%.4f|%.4f"
   --format="%.4f|%.4f|%.3f|"
   --format="%.5f|%.4f||%.4f"
   --format="%.8g|%.8g||%.4f"
</code>

La primera de estas especificaciones presenta los valores de todas las columnas usando 4 dígitos decimales. La segunda elimina la probabilidad asociada y presenta las razones <@mth="t"> con 3 dígitos decimales. La tercera omite el ratio <@mth="t">. La última también omite el <@mth="t">, y presenta tanto el coeficiente como la desviación típica con 8 cifras significativas. 

Una vez que estableces un formato personalizado de este modo, este se recuerda y se utiliza a lo largo de lo que dure la sesión de GRETL. Para revertir esto al formato por defecto, puedes utilizar la variante especial <@opt="--⁠format=default">. 

Menú gráfico: Ventana de modelo: LaTeX

# textplot Graphs

Argumento: 	<@var="listavariables"> 
Opciones: 	<@lit="--time-series"> (Gráfico por observación)
		<@lit="--one-scale"> (Fuerza una escala única)
		<@lit="--tall"> (Usa 40 filas)

Gráficos ASCII rápidos y sencillos. Sin la opción <@opt="--⁠time-series">, <@var="listavariables"> debe contener al menos 2 series, la última de ellas se toma como la variable para el eje <@mth="x">, y se genera un gráfico de dispersión. En este caso, puedes utilizar la opción <@opt="--⁠tall"> para generar un gráfico en la que el eje <@mth="y"> se representa mediante 40 filas de caracteres (por defecto son 20 filas). 

Con la opción <@opt="--⁠time-series">, se genera un gráfico por observación. En este caso, puedes utilizar la opción <@opt="--⁠one-scale"> para forzar el uso de una escala única; de lo contrario, si <@var="listavariables"> contiene más de una serie, los datos pueden escalarse. Cada línea representa una observación, con los valores de los datos dibujados horizontalmente. 

Consulta también <@ref="gnuplot">. 

# tobit Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--llimit">=<@var="cotaizq"> (Especifica la cota de la izquierda)
		<@lit="--rlimit">=<@var="cotader"> (Especifica la cota de la derecha)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--opg"> (Mira más abajo)
		<@lit="--cluster">=<@var="clustvar"> (Consulta <@ref="logit"> para más explicaciones)
		<@lit="--verbose"> (Presenta los detalles de las iteraciones)
		<@lit="--quiet"> (No presenta los resultados)

Estima un modelo Tobit, que puede ser lo adecuado cuando la variable dependiente está “censurada”. Por ejemplo, cuando se observan valores positivos y nulos en la adquisición de bienes duraderos por parte de los hogares, y ningún valor negativo, puede llegar a pensarse que las decisiones sobre esas compras son el resultado de una disposición subyacente e inobservada a comprar, que puede ser negativa en algunos casos. 

Por defecto, se asume que la variable dependiente está 'censurada' en el cero por la izquierda, y que no está 'censurada' por la derecha. Sin embargo, puedes usar las opciones <@opt="--⁠llimit"> y <@opt="--⁠rlimit"> para especificar un patrón diferente para hacer la 'censura'. Ten en cuenta que si especificas únicamente una cota por la derecha, entonces lo que se supone es que la variable dependiente no está 'censurada' por la izquierda. 

El modelo Tobit es un caso especial de la regresión de intervalos. Consulta la instrucción <@ref="intreg"> para obtener detalles adicionales, incluída una explicación de las opciones <@opt="--⁠robust"> y <@opt="--⁠opg">. 

Menú gráfico: /Modelo/Variable dependiente limitada/Tobit

# tsls Estimation

Argumentos: 	<@var="depvar"> <@var="indepvars"> ; <@var="instrumentos"> 
Opciones: 	<@lit="--no-tests"> (No hace contrastes de diagnóstico)
		<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--no-df-corr"> (Sin corrección de los grados de libertad)
		<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--cluster">=<@var="clustvar"> (Desviaciones típicas agrupadas)
		<@lit="--matrix-diff"> (Calcula el contraste de Hausman mediante diferencia de matrices)
		<@lit="--liml"> (Utiliza Máxima Verosimilitud con Información Limitada)
		<@lit="--gmm"> (Utiliza el Método Generalizado de los Momentos)
Ejemplos: 	<@lit="tsls y1 0 y2 y3 x1 x2 ; 0 x1 x2 x3 x4 x5 x6">
		Ver también <@inp="penngrow.inp">

Calcula las estimaciones de Variables Instrumentales (VI), utilizando por defecto Mínimos Cuadrados en 2 Etapas (TSLS), pero mira más abajo para otras opciones. La variable dependiente es <@var="depvar">, mientras que <@var="indepvars"> expresa una lista de regresores (se presupone que incluye al menos una variable endógena), e <@var="instrumentos"> indica una lista de instrumentos (variables exógenas y/o predeterminadas). Si la lista <@var="instrumentos"> no es al menos tan larga como <@var="indepvars">, el modelo no está identificado. 

En el ejemplo de arriba, las <@lit="y">s son las variables endógenas y las <@lit="x">s son las variables exógenas. Ten en cuenta que los regresores exógenos deben aparecer en ambas listas. 

Para obtener más detalles en relación a los efectos de las opciones <@opt="--⁠robust"> y <@opt="--⁠cluster">, consulta la ayuda para <@ref="ols">. 

<subhead>Contrastes específicos para MC2E</subhead> 

El resultado de las estimaciones de Mínimos Cuadrados en 2 Etapas incluyen el contraste de Hausman y (si el modelo está sobreidentificado) el contraste de sobreidentificación de Sargan. Para una buena explicación de los dos contrastes, consulta el capítulo 8 de <@bib="Davidson y MacKinnon (2004);davidson-mackinnon04">. 

En el contraste de Hausman, la hipótesis nula es que las estimaciones MCO son consistentes o, en otras palabras, que la estimación por medio de variables instrumentales en realidad no es necesaria. Por defecto, este contraste se aplica por el método de regresión pero si indicas la opción <@opt="--⁠matrix-diff"> se utiliza el método de <@bib="Papadopoulos (2023);papadopoulos23">. En ambos casos puedes emplear una variante robusta si indicas la opción <@opt="--⁠robust">. 

Un modelo de este tipo está sobreidentificado si hay más instrumentos de los estrictamente requeridos. El contraste de sobreidentificación de Sargan <@bib="(Sargan, 1958);sargan58"> se basa en una regresión auxiliar de los errores de la estimación del modelo, mediante Mínimos Cuadrados en 2 Etapas sobre la lista completa de instrumentos. La hipótesis nula sostiene que todos los instrumentos son válidos; pero se sospecha de la validez de esta hipótesis si la regresión auxiliar tiene un grado de poder explicativo que es significativo. 

Estos estadísticos están disponibles luego de la conclusión satisfactoria de la instrucción, con los nombres <@xrf="$hausman"> y <@xrf="$sargan"> (si es aplicable), respectivamente. 

<subhead>Instrumentos débiles</subhead> 

Tanto para MC2E (TSLS) como para la estimación MVIL (LIML), se muestra el resultado de un contraste adicional, puesto que el modelo se estima bajo el supuesto de perturbaciones IID (es decir, no se escoge la opción <@opt="--⁠robust">). Este es un contraste de la debilidad de los instrumentos, pues instrumentos débiles pueden llevar a serios problemas en la regresión de VI: estimaciones sesgadas y/o tamaño incorrecto de los contrastes de hipótesis basados en la matriz de covarianzas, con tasas de rechazo que superan mucho el nivel de significación nominal <@bib="(Stock, Wright y Yogo, 2002);stock-wright-yogo02">. El estadístico es el del contraste <@mth="F"> de la primera etapa si el modelo tiene tan solo un regresor endógeno; de lo contrario, es el valor propio más pequeño de la matriz de contrapartida del <@mth="F"> de la primera etapa. Se muestran los puntos críticos basados en el análisis Monte Carlo de <@bib="Stock y Yogo (2003);stock-yogo03">, cuando estén disponibles. 

<subhead>R-cuadrado</subhead> 

El valor de R-cuadrado que se presenta para modelos estimados mediante Mínimos Cuadrados en 2 Etapas es el cuadrado de la correlación entre la variable dependiente y la variable con los valores ajustados. 

<subhead>Estimadores alternativos</subhead> 

Como alternativas a MC2E, el modelo puede estimarse mediante Máxima Verosimilitud con Información Limitada (opción <@opt="--⁠liml">) o mediante el Método Generalizado de los Momentos (opción <@opt="--⁠gmm">). Ten en cuenta que, si el modelo está simplemente identificado, estos métodos deberían generar los mismos resultados que MC2E; pero si está sobreidentificado, los resultados en general van a diferir. 

Cuando se escoge la estimación MGM (GMM), las siguientes opciones adicionales pasan a estar disponibles: 

<indent>
• <@opt="--⁠two-step">: Realiza MGM en 2 etapas en lugar de hacerlo en 1 etapa (por defecto). 
</indent>

<indent>
• <@opt="--⁠iterate">: Reitera MGM hasta la convergencia. 
</indent>

<indent>
• <@opt="--⁠weights="><@var="Wmat">: Especifica una matriz cuadrada de ponderaciones para utilizar cuando se calcula la función del criterio MGM. La dimensión de esta matriz debe ser igual al número de instrumentos. Por defecto, es una matriz identidad de dimensión adecuada. 
</indent>

Menú gráfico: /Modelo/Variables instrumentales

# tsplots Graphs

Argumento: 	<@var="listavar"> 
Opciones: 	<@lit="--matrix">=<@var="nombre"> (Representa las columnas de la matriz citada)
		<@lit="--output">=<@var="nombrearch"> (Envía el resultado al archivo indicado)
Ejemplos: 	<@lit="tsplots 1 2 3 4">
		<@lit="tsplots 1 2 3 4 --matrix=X">

Proporciona un modo sencillo de representar gráficamente múltiples series temporales (hasta un máximo de 16) en un único cuadro. Puedes indicar el argumento <@var="listavar"> como una lista de números ID (o de nombres de series), o como números de columnas en caso de introducir una matriz. 

Consulta también <@ref="scatters"> para ver formas de generar múltiples gráficos de dispersión, y <@ref="gridplot"> para ver un modo más flexible de combinar gráficos en una parrilla. 

Menú gráfico: /Ver/Gráficos múltiples/Series temporales

# var Estimation

Argumentos: 	<@var="orden"> <@var="ylista"> [ ; <@var="xlista"> ] 
Opciones: 	<@lit="--nc"> (Sin constante)
		<@lit="--trend"> (Con tendencia lineal)
		<@lit="--seasonals"> (Con variables ficticias estacionales)
		<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--robust-hac"> (Desviaciones típicas HAC)
		<@lit="--quiet"> (No muestra los resultados de las ecuaciones individuales)
		<@lit="--silent"> (No presenta nada)
		<@lit="--impulse-responses"> (Presenta las respuestas al impulso)
		<@lit="--variance-decomp"> (Presenta las descomposiciones de la varianza)
		<@lit="--lagselect"> (Muestra los criterios de selección de retardos)
		<@lit="--minlag">=<@var="retardo mínimo"> (Solo selección de retardo, mira abajo)
Ejemplos: 	<@lit="var 4 x1 x2 x3 ; time mydum">
		<@lit="var 4 x1 x2 x3 --seasonals">
		<@lit="var 12 x1 x2 x3 --lagselect">
		Ver también <@inp="sw_ch14.inp">

Establece y estima (utilizando MCO) una autorregresión de vectores (VAR). El primer argumento especifica el orden de retardos (o el orden máximo de retardos, en caso de que indiques la opción <@opt="--⁠lagselect">, mira más abajo). El orden puedes indicarlo numéricamente o con el nombre de una variable escalar preexistente. A continuación sigue la configuración de la primera ecuación. No incluyas retardos entre los elementos de <@var="ylista"> pues se van a añadir automáticamente. El punto y coma va a separar las variables estocásticas (para las que se va a incluir un <@var="orden"> de retardos) de cualquier variable exógena de <@var="xlista">. Ten en cuenta que: (a) se incluye una constante automáticamente (excepto que indiques la opción <@opt="--⁠nc">), (b) puedes añadir una tendencia con la opción <@opt="--⁠trend">, y (c) puedes añadir variables ficticias estacionales utilizando la opción <@opt="--⁠seasonals">. 

Mientras que una especificación VAR habitualmente incluye todos los retardos desde 1 hasta el máximo que indiques, también puedes escoger un grupo de retardos. Para hacer esto, substituye el argumento rutinario <@var="orden"> (escalar), bien con el nombre de un vector ya definido previamente, o bien con una lista de retardos separados con comas y colocada entre llaves. Debajo se muestran dos modos de especificar que un VAR debe incluir los retardos 1, 2 y 4 (pero no el 3): 

<code>          
   var {1,2,4} ylista
   matrix p = {1,2,4}
   var p ylista
</code>

Se desenvuelve una regresión por separado para cada una de las variables de <@var="ylista">. Los resultados para cada ecuación incluyen los contrastes <@mth="F"> para restricciones cero en todos los retardos de cada una de las variables, un contraste <@mth="F"> sobre la significación del retardo máximo y, cuando especificas la opción <@opt="--⁠impulse-responses">, las descomposiciones de la varianza de la predicción y las respuestas al impulso. 

Las descomposiciones de la varianza de la predicción y las respuestas al impulso se basan en la descomposición de Cholesky de la matriz de covarianzas contemporánea y, en este contexto, tiene importancia el orden en el que indicas las variables (estocásticas). Así, la primera variable de la lista se asume que es la “más exógena” dentro del período. Puedes establecer el horizonte para las descomposiciones de la varianza y las respuestas al impulso, utilizando la instrucción <@ref="set">. Para recuperar una función concreta de respuesta al impulso en forma matricial, consulta la función <@xrf="irf">. 

Cuando indicas la opción <@opt="--⁠robust">, las desviaciones típicas se corrigen del efecto de la heterocedasticidad. Como alternativa, puedes indicar la opción <@opt="--⁠robust-hac"> para dar lugar a desviaciones típicas que sean robustas con respecto tanto a la heterocedasticidad como a la autocorrelación (HAC). En general, esta última corrección no debiera de ser necesaria si el VAR incluye un número suficiente de retardos. 

<subhead>Selección de retardo</subhead> 

Cuando indicas la opción <@opt="--⁠lagselect">, no se presenta el resultado habitual del VAR. En su lugar, se toma el primer argumento como orden <@itl="máximo"> de retardo, y la salida de resultados consiste en una tabla que muestra cifras comparativas calculadas para VARs que van desde el orden 1 (predeterminado) hasta el máximo especificado. La tabla incluye el logaritmo de la verosimilitud y el valor <@mth="P"> de un contraste de Razón de Verosimilitudes (RV o LR), seguidos de los Criterios de Información de Akaike (AIC), de Schwarz (BIC) y de Hannan–Quinn (HQC). El contraste RV (LR) compara la especificación de la fila <@mth="i"> con la de la fila <@mth="i"> – 1, considerando como hipótesis nula que todos los parámetros añadidos en la fila <@mth="i"> tienen valores nulos. Puedes recuperar la tabla con los resultados, en formato de matriz, mediante el accesor <@xrf="$test">. 

Únicamente en el contexto de la selección de retardo, puedes usar la opción <@opt="--⁠minlag"> para ajustar el nivel mínimo de retardos. Ajústalo a 0 para permitir la posibilidad de que el nivel óptimo de retardos sea cero (queriendo ello decir realmente que no se requiere un VAR para nada). Por el contrario, podrías hacer que <@opt="--⁠minlag=4"> si crees que necesitas 4 retardos por lo menos, de forma que se ahorre un poco de tiempo de cálculo. 

Menú gráfico: /Modelo/Series temporales multivariantes

# varlist Dataset

Opción: 	<@lit="--type">=<@var="nombretipo"> (Campo del listado)

Por defecto, presenta un listado de las series del conjunto vigente de datos (si hay alguna); y puedes utilizar <@lit="ls"> como alias. 

Cuando indicas la opción <@opt="--⁠type">, debe ir seguida (después de un signo de igualdad) por uno de los siguientes tipos: <@lit="series">, <@lit="scalar">, <@lit="matrix">, <@lit="list">, <@lit="string">, <@lit="bundle">, <@lit="array"> o <@lit="accessor">. Su efecto consiste en presentar los nombres de todos los objetos del tipo indicado que estén definidos en ese momento. 

Como caso especial, si el tipo es <@lit="accessor">, los nombres que se presentan son aquellos de las variables internas disponibles en ese momento como “accesores”, como pueden ser <@xrf="$nobs"> y <@xrf="$uhat"> (sean cuales sean sus tipos concretos). 

# vartest Tests

Argumentos: 	<@var="serie1"> <@var="serie2"> 

Calcula el estadístico <@mth="F"> para contrastar la hipótesis nula de que las varianzas poblacionales de las variables <@var="serie1"> y <@var="serie2"> son iguales, y muestra su probabilidad asociada (valor p). Puedes obtener las estadísticas del contraste y el valor p mediante los accesores <@xrf="$test"> y <@xrf="$pvalue">, respectivamente. El siguiente código 

<code>          
   	open AWM18.gdt
   	vartest EEN EXR
   	eval $test
   	eval $pvalue
</code>

calcula el contraste, y muestra como recuperar más tarde el estadístico de contraste y el valor p correspondiente: 

<code>          
   	Contraste de igualdad de varianzas

   	EEN: Número de observaciones = 192
   	EXR: Número de observaciones = 188
   	Cociente entre varianzas muestrales = 3.70707
   	Hipótesis nula: Las dos varianzas poblacionales son iguales
   	Estadístico de contraste: F(191,187) = 3.70707
   	valor p (a dos colas) = 1.94866e-18

   	3.7070716
   	1.9486605e-18
</code>

Menú gráfico: /Herramientas/Calculadora de estadísticos de contraste

# vecm Estimation

Argumentos: 	<@var="orden"> <@var="rango"> <@var="ylista"> [ ; <@var="xlista"> ] [ ; <@var="rxlista"> ] 
Opciones: 	<@lit="--nc"> (Sin constante)
		<@lit="--rc"> (Constante restringida)
		<@lit="--uc"> (Constante no restringida)
		<@lit="--crt"> (Constante y tendencia restringida)
		<@lit="--ct"> (Constante y tendencia no restringida)
		<@lit="--seasonals"> (Incluye variables ficticias estacionales centradas)
		<@lit="--quiet"> (No muestra los resultados de las ecuaciones individuales)
		<@lit="--silent"> (No presenta nada)
		<@lit="--impulse-responses"> (Presenta las respuestas al impulso)
		<@lit="--variance-decomp"> (Presenta las descomposiciones de la varianza)
Ejemplos: 	<@lit="vecm 4 1 Y1 Y2 Y3">
		<@lit="vecm 3 2 Y1 Y2 Y3 --rc">
		<@lit="vecm 3 2 Y1 Y2 Y3 ; X1 --rc">
		Ver también <@inp="denmark.inp">, <@inp="hamilton.inp">

Un VECM es una forma de autorregresión de vectores o VAR (consulta <@ref="var">), aplicable cuando las variables del modelo son individualmente integradas de orden 1 (por lo tanto son paseos aleatorios, con o sin deriva) pero presentan cointegración. Esta instrucción está íntimamente relacionada con el contraste de cointegración de Johansen (consulta <@ref="johansen">). 

El parámetro <@var="orden"> de esta instrucción representa el orden de retardos del sistema VAR. El número de retardos en el propio VECM (donde la variable dependiente se indica como una primera diferencia) es de uno menos que <@var="orden">. 

El parámetro <@var="rango"> representa el rango de cointegración o, en otras palabras, el número de vectores cointegrantes. Este debe ser mayor que cero, y menor o igual (generalmente menor) que el número de variables endógenas indicadas en <@var="ylista">. 

El argumento <@var="ylista"> proporciona la lista de variables endógenas, expresadas en niveles. La inclusión de términos de tipo determinístico en el modelo, se controla con los indicadores de opción. Por defecto, cuando no indicas ninguna opción, se incluye una “Constante no restringida”, lo que permite que haya una ordenada en el origen no nula en las relaciones de cointegración, así como una tendencia en los niveles de las variables endógenas. La literatura derivada del trabajo de Johansen (por ejemplo, puedes consultar su libro de 1995) habitualmente se refiere a esto como el “caso 3”. Las primeras 4 opciones indicadas arriba (mutuamente excluyentes) generan los casos 1, 2, 4 y 5, respectivamente. Los significados de estos casos y los criterios que se usan para escoger un caso, se explican en <@pdf="El manual de gretl#chap:vecm"> (Capítulo 33). 

Las listas (opcionales) <@var="xlista"> y <@var="rxlista"> te permiten especificar conjuntos de variables exógenas que forman parte del modelo, bien sin restricciones (<@var="xlista">) o bien restringidas al espacio de cointegración (<@var="rxlista">). Estas listas se separan de <@var="ylista"> y unas de las otras, mediante punto y coma. 

La opción <@opt="--⁠seasonals">, que puedes combinar con cualquiera de las otras opciones, especifica la inclusión de un conjunto de variables ficticias estacionales centradas. Esta opción únicamente está disponible para datos trimestrales o mensuales. 

El primer ejemplo de arriba especifica un VECM, con un orden de retardos de 4 y un único vector de cointegración. Las variables endógenas son <@lit="Y1">, <@lit="Y2"> e <@lit="Y3">. El segundo ejemplo usa las mismas variables pero especifica un orden de retardos de 3, y dos vectores de cointegración; también especifica una “Constante restringida”, que es adecuada cuando los vectores de cointegración pueden tener ordenada en el origen no nula pero las variables <@lit="Y"> no tienen tendencia. 

A continuación de la estimación de un VECM, tienes disponibles algunos accesores especiales: <@lit="$jalpha">, <@lit="$jbeta"> y <@lit="$jvbeta"> recuperan las matrices α y β, y la varianza estimada de β, respectivamente. Para recuperar la función de respuesta ante un impulso determinado, en forma de matriz, consulta la función <@xrf="irf">. 

Menú gráfico: /Modelo/Series temporales multivariantes

# vif Tests

Opción: 	<@lit="--quiet"> (No presenta nada)
Ejemplos: 	<@inp="longley.inp">

Debe ir después de la estimación de un modelo que incluya al menos 2 variables independientes. Calcula y muestra información de diagnóstico relacionada con la multicolinealidad. 

El Factor de Inflación de la Varianza (FIV) del regresor <@mth="j"> se define como 

  <@fig="vif">

donde <@mth="R"><@sub="j"> es el coeficiente de correlación múltiple entre ese regresor <@mth="j"> y los demás regresores. El factor tiene un valor mínimo de 1.0 cuando la variable en cuestión es ortogonal con respecto a las otras variables independientes. <@bib="Neter, Wasserman y Kutner (1990);neter-etal90"> sugieren revisar el valor más grande de los FIV para diagnosticar un alto grado de multicolinealidad; así, un valor mayor que 10 se considera a veces indicativo de un grado de multicolinealidad problemático. 

Después de utilizar esta instrucción, puedes usar el accesor <@xrf="$result"> para obtener un vector columna que incluya los FIV. Para tener un enfoque más sofisticado para diagnosticar la multicolinealidad, consulta la instrucción <@ref="bkw">. 

Menú gráfico: Ventana de modelo: Análisis/Colinealidad

# wls Estimation

Argumentos: 	<@var="varponder"> <@var="depvar"> <@var="indepvars"> 
Opciones: 	<@lit="--vcv"> (Presenta la matriz de covarianzas)
		<@lit="--robust"> (Desviaciones típicas robustas)
		<@lit="--quiet"> (No presenta los resultados)
		<@lit="--allow-zeros"> (Mira abajo)

Calcula las estimaciones de mínimos cuadrados ponderados (MCP, WLS) utilizando <@var="varponder"> como ponderación, <@var="depvar"> como variable dependiente e <@var="indepvars"> como lista de variables independientes. Sea <@var="w"> la raíz cuadrada positiva de <@lit="varponder">, entonces MCP es básicamente equivalente a la regresión MCO de <@var="w"> <@lit="*"> <@var="depvar"> sobre <@var="w"> <@lit="*"> <@var="indepvars">. Sin embargo, el <@itl="R">-cuadrado se calcula de modo especial, concretamente como 

  <@fig="wlsr2">

donde ESS es la suma de errores cuadrados de la regresión ponderada, y WTSS denota la “Suma de cuadrados totales ponderados”, que es igual a la suma de errores cuadrados de la regresión de la variable dependiente ponderada sobre únicamente la constante ponderada. 

Como caso especial, si <@var="varponder"> es una variable ficticia 0/1, la estimación MCP (WLS) es equivalente a MCO (OLS) en una muestra en la que se excluyen todas las observaciones que tienen un valor de cero para <@var="varponder">. Por otro lado, la inclusión de ponderaciones iguales a cero se considera un error, pero si realmente quieres mezclar ponderaciones iguales a cero con ponderaciones positivas, puedes añadir la opción <@opt="--⁠allow-zeros">. 

Para aplicar la estimación de Mínimos Cuadrados Ponderados a datos de panel, basada en las varianzas del error específico de cada unidad, consulta la instrucción <@ref="panel"> junto con la opción <@opt="--⁠unit-weights">. 

Menú gráfico: /Modelo/Otros modelos lineales/Mínimos cuadrados ponderados

# xcorrgm Statistics

Argumentos: 	<@var="serie1"> <@var="serie2"> [ <@var="orden"> ] 
Opciones: 	<@lit="--plot">=<@var="modo-o-nombrearchivo"> (Mira abajo)
		<@lit="--silent"> (Omite la presentación del resultado)
Ejemplo: 	<@lit="xcorrgm x y 12">

Presenta y/o dibuja el correlograma cruzado de <@var="serie1"> con <@var="serie2">, las que puedes especificar mediante sus nombres o sus números. Los valores son los coeficientes de correlación muestrales entre el valor vigente de <@var="serie1"> y los sucesivos adelantos y retardos de <@var="serie2">. 

Si especificas un valor para <@var="orden">, la longitud del correlograma cruzado se limita a ese número de adelantos y retardos (al menos); de lo contrario, la longitud se determina de forma automática en función de la frecuencia de los datos y del número de observaciones. 

Por defecto, cuando GRETL no está en modo de procesamiento por lotes, se muestra un gráfico del correlograma cruzado. Puedes ajustar esto mediante la opción <@opt="--⁠plot">. Los parámetros admisibles para esta opción son <@lit="none"> (para omitir el gráfico), <@lit="display"> (para generar un gráfico Gnuplot aunque sea en modo de procesamiento por lotes), o un nombre de archivo. El efecto de proporcionar un nombre de archivo es como se describió para la opción <@opt="--⁠output"> de la instrucción <@ref="gnuplot">. 

Menú gráfico: /Ver/Correlograma cruzado
Otro acceso: Ventana principal: Menú emergente (tras selección múltiple)

# xtab Statistics

Argumentos: 	<@var="listay"> [ ; <@var="listax"> ] 
Opciones: 	<@lit="--row"> (Muestra los porcentajes de fila)
		<@lit="--column"> (Muestra los porcentajes de columna)
		<@lit="--zeros"> (Muestra un cero en las entradas nulas)
		<@lit="--no-totals"> (Elimina la presentación de los recuentos marginales)
		<@lit="--matrix">=<@var="nombrematr"> (Usa las frecuencias de la matriz indicada)
		<@lit="--quiet"> (Suprime la presentación de resultados)
		<@lit="--tex">[=<@var="nombrearchivo">] (Salida como LaTeX)
		<@lit="--equal"> (Consulta el caso LaTeX más abajo)
Ejemplos: 	<@lit="xtab 1 2">
		<@lit="xtab 1 ; 2 3 4">
		<@lit="xtab --matrix=A">
		<@lit="xtab 1 2 --tex="xtab.tex"">
		Ver también <@inp="ooballot.inp">

Indicando unicamente el argumento <@var="listay">, calcula (y presenta por defecto) una tabla de contingencia o una tabulación cruzada para cada combinación de las variables incluidas en esa lista. Cuando indicas una segunda lista (<@var="listax">), cada variable de <@var="listay"> se cruza en una tabla por fila frente a cada variable de <@var="lista"> (por columna). Puedes referirte a las variables de estas listas mediante sus nombres o sus números. Ten en cuenta que todas las variables tienen que estar marcadas como discretas. Como alternativa, cuando indicas la opción <@opt="--⁠matrix">, se trata la matriz indicada como un conjunto calculado previamente de frecuencias a presentar como tabulación cruzada (consulta también la función <@xrf="mxtab">). En este caso deberás omitir el argumento <@var="list">. 

Por defecto, la anotación de cada celda indica el recuento de la frecuencia de casos. Las opciones <@opt="--⁠row"> y <@opt="--⁠column"> (que se excluyen mutuamente) substituyen los recuentos con los porcentajes para cada fila o columna, respectivamente. Por defecto, las celdas con un recuento de cero casos se dejan en blanco, pero la opción <@opt="--⁠zeros"> tiene como efecto la presentación explícita de los ceros, lo que te puede ser útil para importar la tabla con un programa tal como una hoja de cálculo. 

El contraste de independencia chi-cuadrado de Pearson se muestra si la frecuencia esperada bajo independencia es cuando menos de 1.0e-7 para todas las celdas. Una regla general habitual de la validez de este estadístico es que, al menos el 80 por ciento de las celdas deben tener frecuencias esperadas iguales a 5 o más; y si este criterio no se cumple, se presenta una advertencia. 

Si una tabla de contingencia es 2 por 2, se presenta el Contraste Exacto de independencia de Fisher. Ten en cuenta que este contraste se basa en el supuesto de que los totales por fila y por columna son fijos, lo que puede ser o no ser adecuado dependiendo de cómo se generaron los datos. Debes utilizar la probabilidad asociada (valor p) de la izquierda cuando la hipótesis alternativa a la de independencia es la asociación negativa (los valores tienden a agruparse en las celdas de abajo a la izquierda y de arriba a la derecha); y debes utilizar el valor p de la derecha si la alternativa es la asociación positiva. El valor p de dos colas para este contraste se calcula mediante el método (b) de la sección 2.1 de <@bib="Agresti (1992);agresti92">: esto es la suma de las probabilidades de todas las tablas posibles que tengan los totales de filas y columnas indicados, y que tengan una probabilidad no mayor a la de la tabla observada. 

<subhead>El caso bivariante</subhead> 

En el caso de una tabulación cruzada bivariante (cuando se indica tan solo una lista que tiene dos elementos) se guardan algunos resultados. Puedes recuperar la tabla de contingencia en forma de matriz mediante el accesor <@xrf="$result">. Además, si se cumple la condición del valor esperado, puedes recuperar el estadístico del contraste chi-cuadrado de Pearson y su probabilidad asociada (valor p) mediante los accesores <@xrf="$test"> y <@xrf="$pvalue">. Si son estos los resultados que te interesan, puedes utilizar la opción <@opt="--⁠quiet"> para eliminar la presentación habitual de resultados. 

<subhead>Salida LaTeX</subhead> 

Cuando indicas la opción <@opt="--⁠tex">, la tabulación cruzada se presenta con el formato de un entorno <@lit="tabular"> LaTeX en línea (de donde podría copiarse y pegarse) o, cuando se añade el parámetro <@var="nombrearchivo">, se envía al archivo ahí indicado. (Si en <@var="nombrearchivo"> no se especifica una ruta completa, el archivo se escribe en el directorio vigente establecido, <@ref="workdir">.) No se calcula ningún estadístico de contraste. Puedes utilizar <@opt="--⁠equal"> como opción adicional para señalar (mostrado en negrilla) el recuento o porcentaje de celdas en las que las variables de la fila y columna tienen el mismo valor numérico. Esta opción se ignora excepto que indiques la opción <@opt="--⁠tex">; y también cuando una o las dos variables de la tabulación cruzada tenga valores de cadena de texto. 

