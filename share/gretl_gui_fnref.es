
## Accessors

# $ahat access
Resultado: 	serie 

Debe ejecutarse después de que el último modelo se haya estimado con datos de panel de efectos fijos o de efectos aleatorios. Devuelve una serie que contiene las estimaciones de los efectos individuales. 

# $aic access
Resultado: 	escalar 

Si puede calcularse, devuelve un escalar con el valor del Criterio de Información de Akaike (AIC) del último modelo estimado. Más detalles sobre el cálculo en <@pdf="El manual de gretl#chap:criteria"> (Capítulo 28). 

# $allprobs access
Resultado: 	matriz 

Debe ir después de una estimación con probit o logit ordenados, o con logit multinomial; y devolverá una matriz de orden <@itl="n">×<@itl="j">, donde <@mth="n"> es el número de observaciones utilizadas y <@mth="j"> indica el número de resultados posibles, que contiene la probabilidad estimada de cada resultado para cada observación. 

# $bic access
Resultado: 	escalar 

Si puede calcularse, devuelve un escalar con el valor del Criterio de Información Bayesiano (BIC) de Schwarz del último modelo estimado. Más detalles sobre el cálculo en <@pdf="El manual de gretl#chap:criteria"> (Capítulo 28). 

# $chisq access
Resultado: 	escalar 

Si puede calcularse, devuelve un escalar con el valor del estadístico chi-cuadrado global del contraste de Razón de Verosimilitudes del último modelo estimado. 

# $coeff access
Resultado: 	matriz o escalar 
Argumento: 	<@var="nombre">  (nombre de coeficiente, opcional)

Sin argumentos <@lit="$coeff"> devuelve un vector columna que contiene los coeficientes del último modelo estimado. Con el argumento opcional de texto <@lit="(nombre de un regresor)"> la función devuelve un escalar con el valor del parámetro estimado de ese regresor. Ver también <@ref="$stderr">, <@ref="$vcv">. 

Ejemplo: 

<code>          
     open bjg
     arima 0 1 1 ; 0 1 1 ; lg
     b = $coeff               # Devuelve un vector
     macoef = $coeff(theta_1) # Devuelve un escalar
</code>

Si el “modelo” en cuestión es un sistema de ecuaciones, el resultado depende de las características de este; para VARs y VECMs el resultado devuelto es una matriz con una columna por cada ecuación; de lo contario, es un vector columna que contiene los coeficientes de la primera ecuación seguidos por los coeficientes de la segunda ecuación y así sucesivamente. 

# $command access
Resultado: 	cadena 

Debe ejecutarse tras estimar un modelo, y devuelve la cadena con los caracteres de la instrucción utilizada (ejemplo: <@lit="ols"> o <@lit="probit">). 

# $compan access
Resultado: 	matriz 

Debe ejecutarse después de la estimación de un VAR o de un VECM, y devuelve la matriz compañera. 

# $datatype access
Resultado: 	escalar 

Devuelve un escalar entero que representa el tipo de datos que se están utilizando en ese momento: 0 = sin datos; 1 = datos de corte transversal; 2 = datos de series temporales; 3 = datos de panel. 

# $depvar access
Resultado: 	cadena 

Debe ejecutarse después de la estimación de un modelo con una única ecuación, y devuelve una cadena de texto con el nombre de la variable dependiente. 

# $df access
Resultado: 	escalar 

Devuelve un escalar con los grados de libertad del último modelo estimado. Si este consiste en un sistema de ecuaciones, el valor devuelto es el número de grados de libertad por cada ecuación. Si los grados de libertad de las diferentes ecuaciones no son los mismos en todas ellas, entonces el valor devuelto se calcula restando el número de observaciones menos la media del número de coeficientes de las ecuaciones (esta media se redondea al valor entero inmediatamente superior). 

# $diagpval access
Resultado: 	escalar 

Debe ejecutarse después de la estimación de un sistema de ecuaciones, y devuelve un escalar con la probabilidad asociada al valor del estadístico <@ref="$diagtest">. 

# $diagtest access
Resultado: 	escalar 

Debe ejecutarse después de la estimación de un sistema de ecuaciones. Devuelve un escalar con el valor del estadístico utilizado para contrastar la hipótesis nula de que la matriz de varianzas-covarianzas de las perturbaciones de las ecuaciones del sistema, es diagonal. Este es el contraste de Breusch-Pagan, excepto cuando el estimador es el de un SUR iterado (sin restricciones), pues en ese caso es un contraste de Razón de Verosimilitudes. Para obtener más detalles, véase <@pdf="El manual de gretl#chap:system"> (Capítulo 34) (también <@ref="$diagpval">). 

# $dotdir access
Resultado: 	cadena 

Este accesor devuelve una cadena de texto con la ruta donde GRETL guarda archivos temporalmente, por ejemplo cuando usa la función <@ref="mwrite"> con un tercer argumento distinto de cero. 

# $dw access
Resultado: 	escalar 

Devuelve (si es posible) un escalar con el valor del estadístico de Durbin–Watson para contrastar autocorrelación de primer orden en el último modelo estimado. 

# $dwpval access
Resultado: 	escalar 

Si se puede calcular, devuelve un escalar con el valor de la función de distribución acumulada (CDF) de Durbin–Watson, evaluada en el valor del estadístico de DW para el último modelo estimado; para ello se usa el procedimiento de cálculo <@bib="Imhof;imhof61">. Este es el valor p de un contraste de una cola, en el que la hipótesis alternativa es que existe autocorrelación positiva de primer orden. Si quieres el valor p de un contraste de dos colas, toma 2<@mth="P"> cuando DW < 2, o 2(1 – <@mth="P">) cuando DW > 2, donde <@mth="P"> es el valor que devuelve este accesor. 

Debido a la limitada precisión de la aritmética digital, el resultado del cálculo de la integral del método Imhof puede volverse negativo cuando el estadístico de Durbin-Watson está próximo a su límite inferior; por eso este accesor devuelve <@lit="NA"> en esa situación. Dado que cualquier otra modalidad de fallo tiene como resultado un error que se señaliza, posiblemente sea seguro asumir que un resultado NA indica que la verdadera probabilidad asociada es “muy pequeña”, aunque no sea posible cuantificarla. 

# $ec access
Resultado: 	matriz 

Debe ejecutarse después de la estimación de un VECM, y devuelve una matriz que contiene los términos de Corrección de Errores. El número de filas es igual al número de observaciones utilizadas, y el número de columnas es igual al orden de cointegración del sistema. 

# $error access
Resultado: 	escalar 

Devuelve un escalar con uno de los códigos internos de fallo del programa. Ese código es un valor no nulo cuando ocurre un fallo pero es capturado usando la función <@xrf="catch">. Ten en cuenta que, al utilizar este accesor, el código interno de fallo se vuelve nuevamente cero. Si deseas obtener el mensaje de fallo asociado a un <@lit="$error"> en concreto, es preciso guardar su valor en una variable provisional, por ejemplo utilizando el código: 

<code>          
     err = $error
     if (err)
         printf "Se obtuvo el fallo %d (%s)\n", err, errmsg(err)
     endif
</code>

Ver también <@xrf="catch">, <@ref="errmsg">. 

# $ess access
Resultado: 	escalar 

Si puede calcularse, devuelve un escalar con la suma de los cuadrados de los errores del último modelo estimado. 

# $evals access
Resultado: 	matriz 

Debe ejecutarse después de la estimación de un VECM, y devuelve un vector que contiene los autovalores que se utilizan en el cálculo del contraste de la traza para verificar si existe cointegración. 

# $fcast access
Resultado: 	matriz 

Debe ejecutarse después de la instrucción de predicción <@xrf="fcast">, y devuelve una matriz con los valores previstos. Si el modelo que se utiliza para hacer las predicciones es un sistema de ecuaciones, la matriz está formada por una columna para cada ecuación; en caso contrario, es un vector columna. 

# $fcse access
Resultado: 	matriz 

Si puede calcularse, debe ejecutarse después de procesar la instrucción <@xrf="fcast"> y devuelve una matriz con las desviaciones típicas de las predicciones. Si el modelo que se utiliza para hacer las predicciones es un sistema de ecuaciones, la matriz está formada por una columna para cada ecuación; en caso contrario, es un vector columna. 

# $fevd access
Resultado: 	matriz 

Debe ejecutarse después de la estimación de un VAR, y devuelve una matriz que contiene la descomposición de la varianza de los errores de predicción (FEVD, siglas en inglés). Esa matriz tiene <@mth="h"> filas que indican el número de períodos del horizonte de predicción, lo que puede escogerse de forma manual por medio de <@lit="set horizon"> o de forma automática en base a la frecuencia de los datos. 

Para un VAR con <@mth="p"> variables, la matriz tiene <@mth="p"> <@sup="2"> columnas: las primeras <@mth="p"> columnas contienen la FEVD para la primera variable del VAR; las <@mth="p"> columnas siguientes contienen la FEVD para la segunda variable del VAR y así sucesivamente. La fracción (decimal) del error de predicción de la variable <@mth="i"> causada por una innovación en la variable <@mth="j"> va a encontrarse entonces inspeccionando la columna (<@mth="i"> – 1) <@mth="p"> + <@mth="j">. 

Para una variante más flexible de esta funcionalidad, consulta la función <@ref="fevd">. 

# $Fstat access
Resultado: 	escalar 

Si puede calcularse, devuelve un escalar con el estadístico F del contraste de validez global del último modelo estimado. 

# $gmmcrit access
Resultado: 	escalar 

Debe ejecutarse después de un bloque <@lit="gmm"> (del Método Generalizado de los Momentos), y devuelve un escalar con el mínimo de la función objetivo. 

# $h access
Resultado: 	serie 

Debe ejecutarse después de la instrucción <@lit="garch">, y devuelve una serie con las varianzas condicionales estimadas. 

# $hausman access
Resultado: 	vector fila 

Debe ejecutarse después de estimar un modelo por medio de <@lit="tsls"> o <@lit="panel"> con la opción de efectos aleatorios, y devuelve un vector fila 1×3 que contiene en este orden: el valor del estadístico del contraste de Hausman, los grados de libertad que corresponden y la probabilidad asociada al valor del estadístico. 

# $hqc access
Resultado: 	escalar 

Si puede calcularse, devuelve un escalar con el valor del Criterio de Información de Hannan-Quinn para el último modelo estimado. Para detalles sobre el cálculo, consulta <@pdf="El manual de gretl#chap:criteria"> (Capítulo 28). 

# $huge access
Resultado: 	escalar 

Devuelve un escalar con un número positivo muy grande. Por defecto es igual a 1.0E100, pero puede cambiarse con la instrucción <@xrf="set">. 

# $jalpha access
Resultado: 	matriz 

Debe ejecutarse después de estimar un VECM, y devuelve la matriz de carga. El número de filas de esa matriz es igual al número de variables del VECM, y el número de columnas es igual al rango de cointegración. 

# $jbeta access
Resultado: 	matriz 

Debe ejecutarse después de estimar un VECM, y devuelve la matriz de cointegración. Su número de filas es igual al número de variables del VECM (más el número de variables exógenas que se restringen al espacio de cointegración, si hay alguna); y su número de columnas es igual al rango de cointegración. 

# $jvbeta access
Resultado: 	matriz cuadradax 

Debe ejecutarse después de estimar un VECM, y devuelve la matriz estimada de varianzas-covarianzas de los elementos de los vectores de cointegración. 

En caso de tratarse de una estimación sin restricciones, el número de filas de esa matriz es igual al número de elementos no restringidos del espacio de cointegración, después de la normalización de Phillips. Por el contrario, si se trata de la estimación de un sistema restringido por medio de la instrucción <@lit="restrict"> con la opción <@lit="--full">, se obtiene una matriz singular con <@mth="(n+m)r"> filas (donde <@mth="n"> es el número de variables endógenas, <@mth="m"> el número de variables exógenas restringidas al espacio de cointegración y <@mth="r"> el rango de cointegración). 

Ejemplo: el código... 

<code>          
     open denmark.gdt
     vecm 2 1 LRM LRY IBO IDE --rc --seasonals -q
     s0 = $jvbeta

     restrict --full
       b[1,1] = 1
       b[1,2] = -1
       b[1,3] + b[1,4] = 0
     end restrict
     s1 = $jvbeta

     print s0
     print s1
</code>

... origina el siguiente resultado: 

<code>          
     s0 (4 x 4)

          0,019751     0,029816  -0,00044837   -0,12227
          0,029816     0,31005   -0,45823      -0,18526
         -0,00044837  -0,45823    1,2169       -0,035437
         -0,12227     -0,18526   -0,035437      0,76062

     s1 (5 x 5)

     0,0000       0,0000       0,0000       0,0000       0,0000
     0,0000       0,0000       0,0000       0,0000       0,0000
     0,0000       0,0000      0,27398     -0,27398    -0,019059
     0,0000       0,0000     -0,27398      0,27398     0,019059
     0,0000       0,0000    -0,019059     0,019059    0,0014180
</code>

# $lang access
Resultado: 	cadena 

Devuelve una cadena de texto que representa el idioma que se está usando (si este puede determinarse). La cadena de texto está compuesta por dos letras del código de lenguaje ISO 639-1 (por ejemplo, <@lit="en"> para el idioma inglés, <@lit="jp"> para el japonés, <@lit="el"> para el griego) seguidas de un guion bajo más otras dos letras del código de país ISO 3166-1. Así, por ejemplo, el idioma portugués de Portugal se representa por <@lit="pt_PT"> mientras que el idioma portugués de Brasil se representa por <@lit="pt_BR">. 

Si no es posible determinar el idioma vigente, se devuelve el texto “<@lit="unknown">”. 

# $llt access
Resultado: 	serie 

Para una selección de modelos que se estiman por el método de Máxima Verosimilitud, la función devuelve una serie con los valores del logaritmo de la verosimilitud para cada observación. Por el momento esa función solo está disponible para logit y probit binarios, tobit y heckit. 

# $lnl access
Resultado: 	escalar 

Devuelve un escalar con el logaritmo de la verosimilitud del último modelo estimado (si fuese aplicable). 

# $macheps access
Resultado: 	escalar 

Devuelve un escalar con el valor de la “épsilon de la máquina”, lo que proporciona un límite superior para el error relativo debido al redondeo en la aritmética de punto flotante con doble precisión. 

# $mapfile access
Resultado: 	cadena 

Devuelve una cadena de texto con el nombre del archivo que se debe abrir para obtener los polígonos del mapa, cuando anteriormente se han cargado datos de un archivo GeoJSON o de un archivo ESRI de forma; en caso contrario, devuelve una cadena vacía. Esto está diseñado para ser utilizado con la función <@ref="geoplot">. 

# $mnlprobs access
Resultado: 	matriz 

Debe ejecutarse tras estimar un modelo logit multinomial (únicamente), y devuelve una matriz con las probabilidades estimadas de cada resultado posible, en cada observación de la muestra utilizada en la estimación del modelo. Cada línea representa una observación y cada columna un resultado. Dado que desde GRETL 2023a, este accesorio es obsoleto: por favor, utiliza <@ref="$allprobs"> en su lugar. 

# $model access
Resultado: 	bundle 

Debe ejecutarse después de estimar modelos con una única ecuación, y devuelve un “bundle” que contiene varias unidades de datos pertenecientes al modelo. Se incluyen todos los accesores habituales de los modelos, que son designados mediante claves iguales a los nombres de esos accesores habituales, sin el signo dólar inicial. Por ejemplo, los errores aparecen bajo la clave <@lit="uhat"> y la suma de cuadrados de los errores bajo <@lit="ess">. 

Dependiendo del estimador, puedes disponer de información adicional. Las claves para tal información es de esperar que sean explicativas por sí mismas. Para ver lo que está disponible, puedes guardar una copia del 'bundle' y mostrar su contenido, como por ejemplo con el código: 

<code>          
     ols y 0 x
     bundle b = $model
     print b
</code>

# $mpirank access
Resultado: 	entero 

Cuando se prepara GRETL con soporte MPI, y el programa está funcionando en modo MPI, devuelve la “jerarquía” en base 0 o número ID del proceso vigente. En caso contrario, devuelve –1. 

# $mpisize access
Resultado: 	entero 

Cuando se prepara GRETL con soporte MPI, y el programa está funcionando en modo MPI, devuelve el número de procesos MPI que están funcionando en ese momento. En caso contrario, devuelve 0. 

# $ncoeff access
Resultado: 	entero 

Devuelve un número entero con la cantidad de coeficientes estimados en el último modelo. 

# $nobs access
Resultado: 	entero 

Devuelve un número entero con la cantidad total de observaciones que están seleccionadas en la muestra vigente. Relacionado: <@ref="$tmax">. 

En caso de datos de panel, el valor que se devuelve es el número de observaciones combinadas (el número de unidades de sección cruzada multiplicado por el número de períodos de tempo). Si quieres saber el número de unidades de tiempo de un panel, utiliza <@ref="$pd">. Y el número de unidades de sección cruzada incluidas puede obtenerse mediante <@lit="$nobs"> dividido por <@lit="$pd">. 

# $now access
Resultado: 	vector 

Devuelve un vector con 2 elementos: el primero indica el número de segundos transcurridos desde el 01-01-1970 00:00:00 +0000 (UTC, ou Tempo Universal Coordinado), lo que se utiliza ampliamente en el mundo de la informática para representar el tiempo vigente; y el segundo indica la fecha vigente en formato “básico” ISO 8601, <@lit="YYYYMMDD">. Puedes utilizar la función <@ref="strftime"> para procesar el primer elemento, y la función <@ref="epochday"> para procesar el segundo elemento. 

# $nvars access
Resultado: 	entero 

Devuelve un número entero con la cantidad de series incluidas en el conjunto vigente de datos (contando con la constante). Dado que <@lit="const"> está siempre presente en cualquier conjunto de datos, la obtención del valor 0 indica que no hay conjunto de datos. Observa que al usar este accesorio dentro de una función, el número vigente de series accesibles puede caer por debajo de lo indicado por <@lit="$nvars">. 

# $obsdate access
Resultado: 	serie 

Puede ejecutarse cuando el conjunto vigente de datos está formado por series temporales con frecuencia decenal, anual, trimestral, mensual, fechadas semanalmente o fechadas diariamente. También puede utilizarse con datos de panel si la información temporal está ajustada correctamente (consulta la instrucción <@xrf="setobs">). Devuelve una serie formada por números con 8 dígitos con el patrón <@lit="YYYYMMDD"> (el formato de datos “básico” del ISO 8601), que corresponden al día de la observación, o al primer día de la observación en caso de una frecuencia temporal menor que la diaria. 

Estas series pueden resultar de utilidad cuando se emplea la instrucción <@xrf="join">. 

# $obsmajor access
Resultado: 	serie 

Devuelve una serie que contiene la componente mayor (de menor frecuencia) de cada observación. Esto significa el año para series de tiempo anuales, trimestrales, mensuales, semanales o diarias; el día para datos horarios; o el individuo en el caso de los datos de panel. Si los datos son de sección cruzada, la serie que se devuelve es simplemente el índice entero de las observaciones. 

Ver también <@ref="$obsminor">, <@ref="$obsmicro">. 

# $obsmicro access
Resultado: 	serie 

Puede ejecutarse cuando las observaciones del conjunto vigente de datos tienen una estructura mayor:menor:micro, como en las series temporales fechadas diariamente (año:mes:día). Devuelve una serie que contiene la componente micro (de mayor frecuencia) de cada observación (por ejemplo, el día). 

Ver también <@ref="$obsmajor">, <@ref="$obsminor">. 

# $obsminor access
Resultado: 	serie 

Puede ejecutarse cuando las observaciones del conjunto vigente de datos tienen una estructura mayor:menor, como en series temporales trimestrales (año:trimestre), series temporales mensuales (año:mes), datos de horas (día:hora) y datos de panel (individuo:período). Devuelve una serie que contiene la componente menor (de mayor frecuencia) de cada observación (por ejemplo, el mes). 

En caso de datos fechados diariamente, <@lit="$obsminor"> devuelve una serie con el mes de cada observación. 

Ver también <@ref="$obsmajor">, <@ref="$obsmicro">. 

# $panelpd access
Resultado: 	entero 

Específico para datos de panel, devuelve un entero con la periodicidad temporal (por ejemplo: 4 para datos trimestrales). Cuando no estableces la periodicidad en el conjunto de datos de panel activo, devuelve 1 de forma similar a <@ref="$pd"> para datos de tipo atemporal o sin fecha. Si el conjunto de datos no es de panel, se devuelve NA. 

Ver también <@ref="$pd">, <@ref="$datatype">, <@xrf="setobs">. 

# $parnames access
Resultado: 	array de cadenas 

Después de la estimación de un modelo uniecuacional, devuelve un 'array' de cadenas de texto que contienen los nombres de los parámetros del modelo. El número de nombres coincide con el número de elementos que tiene el vector <@ref="$coeff">. 

Para los modelos especificados mediante una lista de regresores, el resultado va a ser el mismo que el de 

<code>          
     varnames($xlist)
</code>

(consulta la función<@ref="varnames">) pero la función <@lit="$parnames"> es más general; pues también funciona para los modelos que no tienen una lista de regresores (<@xrf="nls">, <@xrf="mle">, <@xrf="gmm">). 

# $pd access
Resultado: 	entero 

Devuelve un número entero con la frecuencia o periodicidad de los datos (por ejemplo: 4 para datos trimestrales). En caso de datos de panel, el valor devuelto es la cantidad de períodos de tiempo del conjunto de datos. 

Ver también <@ref="$panelpd">. 

# $pi access
Resultado: 	escalar 

Devuelve un escalar con el valor de π con doble precisión. 

# $pkgdir access
Resultado: 	cadena 

Utilidad especial para ser utilizada por los autores de paquetes de función. Devuelve una cadena de texto vacía excepto que se esté ejecutando una función empaquetada, en cuyo caso devuelve la ruta completa (dependiendo de la plataforma) a donde está instalado el paquete. Por ejemplo, el valor devuelto podría ser... 

<code>          
     /usr/share/gretl/functions/foo
</code>

en caso de que este fuese el directorio en el que está localizado <@lit="foo.gfn">. Esto permite que el autor de un paquete de función pueda acceder a recursos tales como archivos de matrices que tenga incluidos en su paquete. 

# $pvalue access
Resultado: 	escalar o matriz 

Devuelve la probabilidad asociada al valor del estadístico de prueba que fue generado por la última instrucción explícita de contraste de hipótesis (por ejemplo: <@lit="chow">). Consulta <@pdf="El manual de gretl#chap:genr"> (Capítulo 10) para obtener más detalles. 

Generalmente devuelve un escalar, pero en algunos casos devuelve una matriz (por ejemplo, esto ocurre con las probabilidades asociadas a los valores de los estadísticos de la traza y del máximo-lambda del contraste de cointegración de Johansen). En este caso, los valores están dispuestos en la matriz del mismo modo que en los resultados presentados. 

Ver también <@ref="$test">. 

# $qlrbreak access
Resultado: 	escalar 

Debe ejecutarse después de la instrucción <@xrf="qlrtest"> (que permite hacer el contraste QLR para el cambio estructural en un punto desconocido). Devuelve un escalar con el número entero positivo que indexa la observación en la que se maximiza el valor del estadístico de contraste. 

# $result access
Resultado: 	matriz o bundle 

Proporciona información reservada, a continuación de algunas instrucciones que no tienen accesorios específicos. Las instrucciones en cuestión incluyen <@xrf="bds">, <@xrf="bkw"> <@xrf="corr">, <@xrf="fractint">, <@xrf="freq">, <@xrf="hurst">, <@xrf="leverage">, <@xrf="summary">, <@xrf="vif"> y <@xrf="xtab"> (en cuyos casos, el resultado es una matriz), además de <@xrf="pkg"> (en cuyo caso, se guarda opcionalmente un 'bundle'). 

# $rho access
Resultado: 	escalar 
Argumento: 	<@var="n">  (escalar, opcional)

Sin argumentos, este accesor devuelve el coeficiente de autocorrelación de primer orden para los errores del último modelo estimado. Ahora bien, con la sintaxis <@lit="$rho(n)"> después de la estimación de un modelo por medio de la instrucción <@lit="ar">, devuelve el valor estimado correspondiente al coeficiente ρ(<@mth="n">). 

# $rsq access
Resultado: 	escalar 

Si puede calcularse, devuelve un escalar con el valor del coeficiente <@mth="R"><@sup="2"> no corregido del último modelo estimado. En general, este será el <@mth="R"><@sup="2"> habitual (centrado), pero si la especificación del modelo no contiene una constante (ni un conjunto de regresores cuya “suma” resulte ser constante), entonces será la versión no centrada. En ese caso, puedes acceder a la versión centrada por medio de <@lit="$model.centered_R2">. 

# $sample access
Resultado: 	serie 

Debe ejecutarse después de estimar un modelo de una sola ecuación. Devuelve una serie con una variable ficticia que tiene valores iguales a: 1 en las observaciones utilizadas en la estimación, 0 en las observaciones de la muestra vigente no utilizadas en la estimación (posiblemente debido a valores ausentes), y NA en las observaciones fuera de la muestra vigente seleccionada. 

Si deseas calcular estadísticos basados en la muestra que se utiliza para un modelo dado, puede hacerse, por ejemplo con el código: 

<code>          
     ols y 0 xlist
     series sdum = $sample
     smpl sdum --dummy
</code>

# $sargan access
Resultado: 	vector fila 

Debe ejecutarse después de la instrucción <@lit="tsls">. Devuelve un vector fila 1×3 que contiene, en este orden: el valor del estadístico del contraste de Sobreidentificación de Sargan, los correspondientes grados de libertad y la probabilidad asociada al valor del estadístico. Si el modelo está exactamente identificado, el estadístico no se puede calcular y tratar de hacerlo provoca un fallo. 

# $seed access
Resultado: 	escalar 

Devuelve un escalar con el valor de la semilla del generador de números aleatorios de GRETL. Si estableces la semilla por ti mismo, este accesor no es necesario; pero puede resultar interesante cuando la semilla se establece automáticamente (basándose en el momento en que empezó la ejecución del programa). 

# $sigma access
Resultado: 	escalar o matriz 

Si el último modelo estimado fue uniecuacional, devuelve un escalar con la Desviación Típica de la regresión (S, o en otras palabras, la desviación típica de los errores del modelo con la oportuna corrección de los grados de libertad). Si el último modelo estimado fue un sistema de ecuaciones, devuelve una matriz con las varianzas-covarianzas de los errores de las ecuaciones del sistema. 

# $stderr access
Resultado: 	matriz o escalar 
Argumento: 	<@var="nombre">  (nombre de coeficiente, opcional)

Cuando se utiliza sin argumentos, <@lit="$stderr"> devuelve un vector columna que contiene las desviaciones típicas de los coeficientes del último modelo estimado. Con el argumento opcional <@lit="(nombre de un regresor)"> devuelve un escalar con el valor del parámetro estimado de ese regresor <@var="s">. 

Si el “modelo” es un sistema de ecuaciones, el resultado depende de las características de este: para VARs y VECMs, el valor devuelto es una matriz que contiene una columna por cada ecuación; en otro caso, es un vector columna que contiene los coeficientes de la primera ecuación seguidos por los coeficientes de la segunda ecuación y así sucesivamente. 

Ver también <@ref="$coeff">, <@ref="$vcv">. 

# $stopwatch access
Resultado: 	escalar 

Debe ejecutarse después de la instrucción <@lit="set stopwatch"> que activa la medición de tiempo de la CPU. Al usar este accesor por primera vez se obtiene un escalar con la cantidad de segundos de CPU que pasaron desde la instrucción <@lit="set stopwatch">. Con cada acceso, se reinicia el reloj, por lo que las sucesivas utilizaciones de <@lit="$stopwatch"> generan cada vez un escalar indicativo de los segundos de CPU desde el acceso previo. 

Cuando una función definida por el usuario está en ejecución, al usar la instrucción <@lit="set stopwatch"> y el accesor <@lit="$stopwatch">, estos resultan específicos para esa función —es decir, la medición del tiempo dentro de una función no interrumpe cualquier medición “global” que pueda estar haciéndose en un guion principal. 

# $sysA access
Resultado: 	matriz 

Debe ejecutarse después de estimar un sistema de ecuaciones simultáneas. Devuelve la matriz con los coeficientes de las variables endógenas retardadas (en caso de que existan), en la forma estructural del sistema. Consulta también la instrucción <@xrf="system">. 

# $sysB access
Resultado: 	matriz 

Debe ejecutarse después de estimar un sistema de ecuaciones simultáneas. Devuelve una matriz con los coeficientes de las variables exógenas, en la forma estructural del sistema. Consulta la instrucción <@xrf="system">. 

# $sysGamma access
Resultado: 	matriz 

Debe ejecutarse después de estimar un sistema de ecuaciones simultáneas. Devuelve una matriz con los coeficientes de las variables endógenas contemporáneas, en la forma estructural del sistema. Consulta la instrucción <@xrf="system">. 

# $sysinfo access
Resultado: 	bundle 

Devuelve un “bundle” que contiene información de las capacidades de GRETL y del sistema operativo en el que se está ejecutando. Los elementos del 'bundle' se indican a continuación: 

<indent>
• <@lit="gui_mode">: número entero igual a 1 si libgretl está siendo invocado por el programa GUI, y 0 en caso contrario. 
</indent>

<indent>
• <@lit="mpi">: número entero igual a 1 si el sistema admite MPI (Interfaz de Paso de Mensajes), y 0 en caso contrario. 
</indent>

<indent>
• <@lit="omp">: número entero igual a 1 si GRETL se compiló con soporte para Open MP, y 0 en caso contrario. 
</indent>

<indent>
• <@lit="ncores">: número entero que indica el número de núcleos físicos de procesador disponibles. 
</indent>

<indent>
• <@lit="nproc">: número entero que indica el número de procesadores disponibles, y que será mayor que <@lit="ncores"> si está habilitado el Hyper-threading. 
</indent>

<indent>
• <@lit="mpimax">: número entero que indica el máximo número de procesos MPI que pueden ejecutarse en paralelo. Es igual a cero si no se admite MPI; en caso contrario, es igual al valor de <@lit="nproc"> local, excepto que se especifique un archivo de hosts MPI, caso en el que es igual a la suma del número de procesadores o “slots” a lo largo de todas las máquinas a las que se hace referencia en el archivo. 
</indent>

<indent>
• <@lit="wordlen">: número entero igual a 32 o a 64 en sistemas de 32 bit y 64 bit, respectivamente. 
</indent>

<indent>
• <@lit="os">: cadena de texto que representa el sistema operativo, bien <@lit="linux">, <@lit="macos">, <@lit="windows"> o <@lit="outro">. Ten en cuenta que las versiones de GRETL previas a la '2021e' proporcionan la cadena <@lit="osx"> para el sistema operativo de Mac; por lo tanto, una expresión de comprobación para Mac independiente de la versión es <@lit="instring($sysinfo.os, "os")">. 
</indent>

<indent>
• <@lit="hostname">: cadena de texto con el nombre de la máquina (o “host”) en la que se está ejecutando el proceso vigente de GRETL. Si no es posible determinar el nombre, se produce una vuelta atrás del <@lit="localhost">. 
</indent>

<indent>
• <@lit="mem">: un vector bidimensional que contiene la memoria física total, y la memoria libre o disponible, expresadas en MB. Esta información puede no estar disponible en todos los sistemas, pero debiera estarlo en Windows, macOS y Linux. 
</indent>

<indent>
• <@lit="blas">: cadena de texto que identifica el proveedor de la biblioteca BLAS (Subprogramas Básicos de Álgebra Lineal) que utiliza GRETL. 
</indent>

<indent>
• <@lit="blas_version">: cadena de texto que identifica el número de la versión de la biblioteca BLAS que se utiliza. 
</indent>

<indent>
• <@lit="blascore">: (si es aplicable) una cadena de texto que identifica el tipo de CPU para el que está optimizada la biblioteca BLAS vigente. 
</indent>

<indent>
• <@lit="compiler">: una cadena de texto que identifica el compilador utilizado al generar libgretl. 
</indent>

<indent>
• <@lit="cpuid">: una cadena de texto que identifica el vendedor y el modelo de la CPU en la que está ejecutándose libgretl. 
</indent>

<indent>
• <@lit="gnuplot">: una cadena de texto que identifica la versión disponible de Gnuplot para que GRETL haga gráficos, con formato de 3 números separados por puntos que indican la versión principal, la versión secundaria y el nivel de parche. 
</indent>

<indent>
• <@lit="foreign">: un sub-bundle que contiene indicadores 0/1 para mostrar la presencia en el sistema, de cada uno de los programas “externos” que admite GRETL bajo las claves <@lit="julia">, <@lit="octave">, <@lit="ox">, <@lit="python">, <@lit="Rbin">, <@lit="Rlib"> y <@lit="stata">. Las dos claves que corresponden a R representan respectivamente, el ejecutable de R y la biblioteca compartida. 
</indent>

Fíjate en que puedes acceder a elementos individuales del 'bundle' mediante la notación del“punto”, sin necesidad de copiar el 'bundle' entero con un nombre de usuario específico. Por ejemplo con el código: 

<code>          
     if $sysinfo.os == "linux"
         # Haga algo que sea propio del Linux
     endif
</code>

# $system access
Resultado: 	bundle 

Debe seguir a la estimación de un sistema de ecuaciones, realizada con la instrucción <@xrf="system">, con <@xrf="var"> o con <@xrf="vecm">; y devuelve un 'bundle' que contiene muchos apartados de datos que se refieren al sistema. Se incluyen todos los accesores importantes y habituales del sistema, que se nombran mediante símbolos clave que son idénticos a los nombres habituales de los accesores, menos el símbolo de dólar inicial. Así, por ejemplo, los errores aparecen bajo la clave <@lit="uhat"> y los coeficientes bajo <@lit="coeff">. (Como excepciones están las claves <@lit="A">, <@lit="B">, e <@lit="Gamma">, que se corresponden con los accesores habituales sysA, sysB, y sysGamma.) Las claves para obtener información adicional se espera que debieran explicarse suficientemente por si mismas. Para comprobar lo que tienes a tu disposición, puedes obtener una copia del 'bundle' y representar su contenido, como en 

<code>          
     var 4 y1 y2 y2
     bundle b = $system
     print b
</code>

Puedes pasar un 'bundle' obtenido de este modo como argumento final (opcional) de las funciones <@ref="fevd"> e <@ref="irf">. 

# $T access
Resultado: 	entero 

Devuelve un número entero con el número de observaciones utilizadas en la estimación del último modelo. 

# $t1 access
Resultado: 	entero 

Devuelve un entero positivo con el número que indexa la primera observación de la muestra vigente seleccionada. 

# $t2 access
Resultado: 	entero 

Devuelve un entero positivo con el número que indexa la última observación de la muestra vigente seleccionada. 

# $test access
Resultado: 	escalar o matriz 

Devuelve el valor del estadístico de prueba que fue generado por la última instrucción explícita para un contraste de hipótesis (por ejemplo: <@lit="chow">). Consulta <@pdf="El manual de gretl#chap:genr"> (Capítulo 10) para obtener más detalles. 

Generalmente devuelve un escalar, pero en algunos casos devuelve una matriz (por ejemplo, eso ocurre con los estadísticos de la traza y del máximo-lambda del contraste de cointegración de Johansen). En este caso, los valores están dispuestos en la matriz del mismo modo que en los resultados presentados. 

Ver también <@ref="$pvalue">. 

# $time access
Resultado: 	serie 

Para datos de series temporales o de panel, genera un índice con enteros positivos del período temporal. En el caso del panel, la secuencia de valores se repite para cada unidad de sección cruzada. 

La instrucción “<@lit="genr time">” es una alternativa, con la diferencia de que la variante <@lit="genr"> genera de modo automático una serie denominada <@lit="time">, mientras que la denominación de la serie corresponde al solicitante cuando se utiliza <@lit="$time">, como en 

<code>          
     series trend = $time
</code>

Este accesor no está disponible para datos de sección cruzada. 

# $tmax access
Resultado: 	entero 

Devuelve un entero con el máximo valor válido establecido para indicar el final del rango de la muestra mediante la instrucción <@xrf="smpl">. En la mayoría de los casos, esto va a ser igual al número de observaciones del conjunto de datos; pero en una función de HANSL, el valor <@lit="$tmax"> podría ser menor, puesto que el acceso habitual a los datos dentro de las funciones, se limita al rango muestral establecido por el solicitante. 

Ten en cuenta que, en general, <@lit="$tmax"> no es igual a <@ref="$nobs">, que proporciona el número de observaciones del rango de la muestra vigente. 

# $trsq access
Resultado: 	escalar 

Devuelve el escalar <@mth="TR"><@sup="2"> (el tamaño de la muestra multiplicado por el R-cuadrado del último modelo), si está disponible. 

# $uhat access
Resultado: 	serie 

Devuelve una serie con los errores del último modelo estimado. Esto puede tener diferentes significados dependiendo de los estimadores utilizados. Por ejemplo, después de la estimación de un modelo ARMA, <@lit="$uhat"> contiene los errores de la predicción adelantados 1 paso; después de la estimación de un probit, contiene los errores generalizados. 

Cuando el “modelo” vigente en cuestión es un sistema de ecuaciones (un VAR, un VECM o un sistema de ecuaciones simultáneas), el <@lit="$uhat"> genera una matriz con los errores de estimación de cada ecuación, ordenados por columnas. 

# $unit access
Resultado: 	serie 

Solo es válido para datos de panel. Devuelve una serie con valor igual a 1 en todas las observaciones en la primera unidad o grupo, 2 en todas las observaciones en la segunda unidad o grupo, y así sucesivamente. 

# $vcv access
Resultado: 	matriz o escalar 
Argumentos:	<@var="nombre1">  (nombre de coeficiente, opcional)
		<@var="nombre2">  (nombre de coeficiente, opcional)

Cuando se utiliza sin argumentos, <@lit="$vcv"> devuelve una matriz cuadrada que contiene las varianzas-covarianzas estimadas de los coeficientes del último modelo estimado. Si este último era uniecuacional, se pueden indicar los nombres de dos regresores entre paréntesis, para así obtener un escalar con la covarianza estimada entre <@var="nombre1"> y <@var="nombre2">. Ver también <@ref="$coeff">, <@ref="$stderr">. 

Este accesor no está disponible para VARs o VECMs. Para modelos de ese tipo <@ref="$sigma"> y <@ref="$xtxinv">. 

# $vecGamma access
Resultado: 	matriz 

Debe ejecutarse después de estimar un VECM y devuelve una matriz en la que las matrices Gamma (con los coeficientes de las diferencias retardadas de las variables cointegradas) se agrupan unas al lado de las otras. Cada fila indica una ecuación; para un VECM con nivel de retardo <@mth="p"> existen <@mth="p"> – 1 submatrices. 

# $version access
Resultado: 	escalar 

Devuelve un escalar con un valor entero que designa la versión de GRETL. La versión actual de GRETL está formada por una cadena de texto que indica el año con formato de 4 dígitos seguido de una letra desde a hasta j, que representa las sucesivas actualizaciones dentro de cada año (por ejemplo, 2015d). El valor devuelto por este accesor está calculado multiplicando el año por 10, y sumándole un número que representa a la letra, en el orden léxico en base cero. Así, 2015d se representa mediante 20153. 

En versiones anteriores al GRETL 2015d, el identificador tenía el siguiente formato: x.y.z (tres números enteros separados por puntos); en ese caso, el valor de la función se calculaba con <@lit="10000*x + 100*y + z">. Por ejemplo, la última versión con el formato antiguo (1.10.2) se transcribía mediante 11002. De este modo el orden numérico de <@lit="$version"> fue preservado aún después de cambiar el esquema de las versiones. 

# $vma access
Resultado: 	matriz 

Debe ejecutarse después de estimar un VAR o un VECM, y devuelve una matriz que contiene la representación VMA hasta el orden especificado por medio de la instrucción <@lit="set horizon">. Para tener más detalles, consulta <@pdf="El manual de gretl#chap:var"> (Capítulo 32). 

# $windows access
Resultado: 	entero 

Devuelve un número entero con el valor 1 si GRETL se está ejecutando en Windows, y 0 en caso contrario. Poniendo como condición uno de estos valores, puedes escribir instrucciones “shell ” que puedan ejecutarse en diferentes sistemas operativos. 

Consulta también la instrucción <@xrf="shell">. 

# $workdir access
Resultado: 	cadena 

Este accesor devuelve una cadena de texto con la ruta desde la que lee y en la que escribe GRETL por defecto. Se ofrece una discusión más detallada en la Guía de instrucciones, en <@xrf="workdir">. Ten en cuenta que el usuario puede determinar esta cadena mediante la instrucción <@xrf="set">. 

# $xlist access
Resultado: 	lista 

Si el último modelo estimado era uniecuacional, este accesor va a devolver una lista con sus regresores. Si el último modelo era un sistema de ecuaciones, devuelve una lista “global” con las variables exógenas (en el mismo orden en el que aparecen con el accesor <@ref="$sysB">). Si el último modelo era un VAR, devuelve una lista con los regresores exógenos (si hay alguno), con excepción de los términos determinísticos habituales (la constante, la tendencia y los elementos estacionales). 

# $xtxinv access
Resultado: 	matriz 

Debe ejecutarse únicamente después de la estimación de un VAR o VECM, y devuelve la matriz <@mth="X'X"><@sup="-1">, donde <@mth="X"> es la matriz habitual con los regresores utilizados en cada ecuación. Aunque este accesorio está disponible para un VECM estimado con una restricción impuesta en α (la matriz de “cargas”), debe tenerse en cuenta que en ese caso no todos los coeficientes de los regresores varían libremente. 

# $yhat access
Resultado: 	serie 

Devuelve una serie con los valores estimados de la variable explicada de la última regresión. 

# $ylist access
Resultado: 	lista 

Si el último modelo estimado fue un VAR, un VECM o un sistema de ecuaciones simultáneas, este accesor devuelve una lista con las variables endógenas. Si el último modelo estimado fue uniecuacional, el accesor devuelve una lista con un único elemento, la variable dependiente. En el caso especial del modelo biprobit, la lista contiene dos elementos. 

## Built-in strings

# $dotdir straccess
Resultado: 	cadena 

Proporciona una cadena de texto con la ruta completa del directorio que utiliza GRETL para los archivos temporales. Para usarla en modo de sustitución para cadenas de texto, antepón el símbolo arroba (@dotdir). 

# $gnuplot straccess
Resultado: 	cadena 

Proporciona una cadena de texto con la ruta hasta el ejecutable 'gnuplot'. Para usarla en modo de sustitución para cadenas, antepón el símbolo arroba (@gnuplot). 

# $gretldir straccess
Resultado: 	cadena 

Proporciona una cadena de texto con la ruta completa al directorio de instalación de GRETL. Para usarla en modo de substitución para cadenas de texto, antepón el símbolo arroba (@gretldir). 

# $tramo straccess
Resultado: 	cadena 

Proporciona una cadena de texto con la ruta hasta el ejecutable 'tramo'. Para usarla en modo de sustitución para cadenas, antepón el símbolo arroba (@tramo). 

# $tramodir straccess
Resultado: 	cadena 

Proporciona una cadena de texto con la ruta hasta el directorio de datos de 'tramo'. Para usarla en modo de sustitución para cadenas, antepón el símbolo arroba (@tramodir). 

# $x12a straccess
Resultado: 	cadena 

Proporciona una cadena de texto con la ruta hasta el ejecutable 'x-12-arima'. Para usarla en modo de sustitución para cadenas, antepón el símbolo arroba (@x12a). 

# $x12adir straccess
Resultado: 	cadena 

Proporciona una cadena de texto con la ruta hasta el directorio de datos de 'x-12-arima'. Para usarla en modo de sustitución para cadenas, antepón el símbolo arroba (@x12adir). 

## Functions proper

# abs math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el valor absoluto de <@var="x">. 

# acos math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con los radianes del arco coseno de <@var="x">; es decir, proporciona el arco cuyo coseno es <@var="x"> (el argumento debe estar entre –1 y 1). 

# acosh math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el coseno hiperbólico inverso de <@var="x"> (solución positiva). Este último debe ser mayor que 1, pues de lo contrario la función devolverá NA. Ver también <@ref="cosh">. 

# aggregate stats
Resultado: 	matriz 
Argumentos:	<@var="x">  (serie, lista o matriz)
		<@var="segunvar">  (serie, lista o matriz)
		<@var="nombrefunc">  (cadena, opcional)

La mayor parte de lo que sigue asume que los dos primeros argumentos de esta función toman la forma de series o listas, pero consulta la sección “Entrada matricial” de abajo para un uso alternativo. 

En la forma más simple de uso de esta función, <@var="x"> se establece igual a <@lit="null">, <@var="segunvar"> es una serie individual y el tercer argumento se omite (o se establece igual a cero). En este caso, se devuelve una matriz con dos columnas que contiene: los distintos valores de <@var="segunvar"> ordenados de forma creciente en la primera columna, y el número de observaciones en las que <@var="segunvar"> toma cada uno de esos valores. Por ejemplo... 

<code>          
     open data4-1
     eval aggregate(null, bedrms)
</code>

... mostrará que la serie <@lit="bedrms"> tiene los valores 3 (en total 5 veces) y 4 (en total 9 veces). 

De modo más general, si <@var="segunvar"> es una lista con <@mth="n"> elementos, entonces las <@mth="n"> columnas a la izquierda contienen las combinaciones de los distintos valores de cada una de las <@mth="n"> series, y la columna de recuento contiene el número de observaciones en las que se produce cada combinación. Observa que siempre puedes encontrar la columna de recuento en la posición <@lit="nelem(segunvar) + 1">. 

<subhead>Especificar una función de agregación</subhead> 

Cuando indicas el tercer argumento, entonces <@var="x"> no debe ser <@lit="null">, y las <@mth="m"> columnas más a la derecha van a contener los valores del estadístico especificado por <@var="nombrefunc"> para cada una de las variables en <@var="x">. (De este modo, <@mth="m"> se iguala a 1 cuando <@var="x"> es una única serie, y se iguala a <@lit="nelem(x)"> cuando <@var="x"> es una lista.) El estadístico indicado se calcula en las submuestras respectivas que estén definidas por medio de las combinaciones indicadas en <@var="segunvar"> (en orden ascendente); estas combinaciones se muestran en la(s) primera(s) <@mth="n"> columna(s) de la matriz que se devuelve. 

Entonces, en el caso especial en el que <@var="x"> y <@var="segunvar"> son ambas series individuales, el valor que se devuelve es una matriz con tres columnas que va a contener respectivamente: los distintos valores de <@var="segunvar"> ordenados de forma creciente, el número de observaciones en las que <@var="segunvar"> toma cada uno de esos valores, y los valores del estadístico que especifica la función <@var="nombrefunc">, calculado para la serie <@var="x">, pero usando tan solo aquellas observaciones en las que <@var="segunvar"> toma el mismo valor que se especifica en la primera columna de la matriz. 

Las siguientes opciones de <@var="nombrefunc"> se mantienen de forma “original”: <@ref="sum">, <@ref="sumall">, <@ref="mean">, <@ref="sd">, <@ref="var">, <@ref="sst">, <@ref="skewness">, <@ref="kurtosis">, <@ref="min">, <@ref="max">, <@ref="median">, <@ref="nobs"> y <@ref="gini">, <@ref="isconst"> e <@ref="isdummy">. Cada una de estas funciones utiliza una serie como argumento y devuelve un valor escalar; por eso, en este sentido, puede decirse que de algún modo “agregan” la serie. Si no hay ninguha de estas funciones originales que haga lo que necesitas, puedes utilizar una función definida por el usuario como “agregador”; en ese caso, del mismo modo que las funciones originales, esa función debe tener como argumento únicamente una serie, y devolver un valor escalar. 

Ten en cuenta que, a pesar de que <@lit="aggregate"> hace el recuento de casos de forma automática, la opción <@lit="nobs">, no es redundante como función “agregadora”, puesto que proporciona el número de observaciones válidas (no ausentes) de <@var="x"> en cada combinación de <@var="segunvar">. 

Como ejemplo sencillo, supón que con <@lit="region"> se definen unos códigos para representar una distribución geográfica por regiones, utilizándose para ello enteros desde 1 hasta <@mth="n">, y que con <@lit="renta"> se representa la renta de los hogares. Entonces el código indicado a continuación debe producir una matriz de orden <@itl="n">×3 que contiene los códigos de las regiones, el recuento de observaciones de cada una, y la renta media de los hogares en cada una: 

<code>          
     matrix m = aggregate(renta, region, mean)
</code>

Como ejemplo de utilización con listas de variables, sea <@lit="genero"> una variable binaria hombre/mujer, sea <@lit="raza"> una variable categórica con tres valores, y considera el siguiente código: 

<code>          
     list BY = genero raza
     list X = renta edad
     matrix m = aggregate(X, BY, sd)
</code>

Invocar la función <@lit="aggregate"> producirá una matriz de orden 6×5. En las dos primeras columnas se expresan las 6 distintas combinaciones de los valores de 'genero' y 'raza'; la columna del medio contiene el recuento del número de casos para cada una de esas combinaciones; y las dos columnas más a la derecha contienen las desviaciones típicas muestrales de <@lit="renta"> y <@lit="edad">. 

Observa que si <@var="segunvar"> es una lista de variables, algunas combinaciones de los valores de <@var="segunvar"> pueden no estar presentes en los datos (produciéndose un recuento igual a cero). En ese caso, los valores de los estadísticos para <@var="x"> se registran como <@lit="NaN"> (es decir, no son números). Si quieres ignorar esos casos, puedes usar la función <@ref="selifr"> para escoger solo aquellas filas que no tengan recuento igual a cero. La columna a comprobar estará una posición a la derecha de la indicada por el número de variables de <@var="segunvar">, por lo que puede usarse el código: 

<code>          
     matrix m = aggregate(X, BY, sd)
     scalar c = nelem(BY)
     m = selifr(m, m[,c+1])
</code>

<subhead>Entrada matricial</subhead> 

En lugar de indicar <@var="x"> y <@var="segunvar"> como series o listas, puedes indicarlas en formato matricial. Ahora bien, si indicas los dos argumentos, deben coincidir en el mismo tipo (no puedes indicar una serie o una lista para uno de los argumentos, y una matriz para el otro); además, dos argumentos de tipo matricial deberán tener el mismo número de filas. Ten en cuenta también, que en este contexto se trata a las columnas de las matrices como si fuesen series, por ello la función de agregación del tercer argumento (opcional) deberá seguir el mesmo patrón descrito más arriba, tomando un argumento de tipo serie y devolviendo un escalar. 

# argname strings
Resultado: 	cadena 
Argumentos:	<@var="s">  (cadena)
		<@var="pordefecto">  (cadena, opcional)

Si <@var="s"> es el nombre de un parámetro hacia una función definida previamente por el usuario, devuelve una cadena de texto con el nombre del argumento correspondente (si este tiene un nombre a nivel de la llamada). Si el argumento es anónimo, se devuelve una cadena vacía excepto que indiques el argumento opcional <@var="pordefecto">, en cuyo caso se utiliza su valor como alternativa. 

# array data-utils
Resultado: 	mira más abajo 
Argumento: 	<@var="n">  (entero)

Esta es la función “generadora” básica de una nueva variable de tipo “array”. Al usar esta función es necesario que especifiques un tipo (en forma plural) para el 'array': <@lit="strings">, <@lit="matrices">, <@lit="bundles">, <@lit="lists"> o <@lit="arrays">. Devuelve un 'array' del tipo especificado con <@var="n"> elementos “vacíos” (por ejemplo, una cadena de texto (“string”) vacía o una matriz nula). Ejemplos de utilización: 

<code>          
     strings S = array(5)
     matrices M = array(3)
</code>

Consulta también <@ref="defarray">. 

# asin math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con los radianes del arco seno de <@var="x">; es decir, proporciona el arco cuyo seno es <@var="x"> (el argumento debe estar entre –1 y 1). 

# asinh math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el seno hiperbólico inverso de <@var="x">. Ver también <@ref="sinh">. 

# asort data-utils
Resultado: 	escalar 
Argumentos:	<@var="a">  (array)
		<@var="nombrefun">  (cadena)

Realiza una reordenación inmediata de los elementos del argumento <@var="a">, usando una función de comparación especificada por el solicitante bajo el control de la rutina de ordenación rápida. 

El argumento <@var="a"> puede ser de cualquiera de los tipos que se admiten para un 'array' de GRETL; en concreto <@lit="strings"> (cadenas), <@lit="matrices">, <@lit="bundles"> (paquetes), <@lit="lists"> (listas) o <@lit="arrays"> (ordenaciones). El argumento <@var="nombrefun"> debe ser el nombre de una función que tome dos argumentos <@lit="const">, cuyo tipo coincida con el tipo de los elementos de <@var="a">. Esta función debe devolver un valor entero con el siguiente patrón de comportamiento: 0 cuando los dos argumentos tengan el mesmo orden de clasificación, negativo cando el primero argumento clasifique antes que el segundo, o positivo cuando el segundo clasifique antes que el primero. (Los valores exactos non importan.) 

Por ejemplo, supón que quieres ordenar un 'array' de 'bundles', en los que cada uno de ellos contiene un escalar denominado <@lit="crit">, en función del valor creciente de <@lit="crit">. Entonces, la siguiente función sería adecuada para usarla con <@lit="asort">: 

<code>          
     function scalar my_bsort (const bundle b1, const bundle b2)
        return b1.crit - b2.crit
     end function
</code>

Si deseas preservar el 'array' sin ordenar, haz una copia de él antes de usarlo con <@lit="asort">. El valor que devuelve esta función cuando tiene éxito, es un 0. 

Consulta también la función <@ref="sort"> para una ordenación sencilla de un 'array' de cadenas de texto. 

# assert programming
Resultado: 	escalar 
Argumento: 	<@var="expr">  (escalar)

Esta función está dirigida a comprobar y depurar código HANSL. Su argumento habrá de ser una expresión cuyo valor sea un escalar. El valor que devuelve esta función es, o bien 1 cuando el valor del argumento <@var="expr"> no es cero (“verdadero” booleano o “éxito”), o bien 0 si el valor del argumento es cero (“falso” booleano o “fallo”). 

Por defecto, no hay otras consecuencias de que falle una llamada a <@lit="assert">, más que el hecho de que el valor que se devuelve es cero. Sin embargo, puedes utilizar la instrucción <@xrf="set"> para hacer que el fallo de una afirmación tenga más consecuencias. Hay tres niveles: 

<code>          
     # Presentar un mensaje de aviso, pero continuar con la ejecución
     set assert warn
     # Presentar un mensaje de aviso y detener la ejecución de un guion
     set assert stop
     # Presentar un mensaje a 'stderr' y abortar el programa
     set assert fatal
</code>

En la mayoría de los casos <@lit="stop"> es suficiente para detener la ejecución de un guion, pero en ciertos casos especiales (como dentro de una función invocada desde un bloque de instrucciones tal como en <@xrf="mle">), puede resultar necesario utilizar la opción <@lit="fatal"> para alcanzar una indicación clara de la afirmación que falla. Sin embargo, observa que en este caso el mensaje va a dirigirse a la salida de resultados del error típico. 

Puedes restablecer el funcionamiento por defecto mediante 

<code>          
     set assert off
</code>

A modo de sencillo ejemplo: Si en cierto punto de un guion HANSL, un escalar <@lit="x"> debe ser no negativo, el siguiente código mostrará un error si este no es el caso: 

<code>          
     set assert stop
     assert(x >= 0)
</code>

# atan math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con los radianes del arco tangente de <@var="x">; es decir, devuelve el arco cuya tangente es <@var="x">. 

Ver también <@ref="tan">, <@ref="atan2">. 

# atan2 math
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="y">  (escalar, serie o matriz)
		<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el valor principal de la arco tanxente de <@var="y">/<@var="x">, utilizando los signos de los dos argumentos indicados para determinar el cuadrante del resultado. El valor que se devuelve está en radianes, dentro del rango [–π, π]. 

Si los dos argumentos son de tipos difirentes, el tipo del resultado es el mismo que el del “mayor” de los dos, donde la jerarquía es matriz > serie > escalar. Por ejemplo, si <@var="y"> es un escalar, y <@var="x"> es un vector de dimensión <@mth="n"> (o viceversa), el resultado es un vector de dimensión <@mth="n">. Ten en cuenta que los argumentos de una matriz deben ser vectores; y que, si ningún argumento es un escalar, los dos argumentos deben ser de la misma longitud. 

Ver también <@ref="tan">, <@ref="tanh">. 

# atanh math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con la tangente hiperbólica inversa de <@var="x">. Ver también <@ref="tanh">. 

# atof strings
Resultado: 	escalar 
Argumento: 	<@var="s">  (cadena)

Función muy relacionada con la del lenguaje de programación C con el mismo nombre. Devuelve un escalar con el resultado de convertir la cadena de texto <@var="s"> (o su trozo relevante después de descartar cualquier espacio inicial en blanco) en un número de punto flotante. A diferencia de lo que ocurre en el lenguaje C, la función <@lit="atof"> siempre asume que el carácter decimal es el “<@lit=".">” (por cuestiones de portabilidad). Se ignoran todos los caracteres que siguen después de la parte de <@var="s"> que se convierte en número de punto flotante. 

Si, bajo el supuesto establecido, no pudiera convertirse ninguno de los caracteres de <@var="s"> que queden después de descartar los espacios en blanco, la función devuelve <@lit="NA">. 

<code>          
     # Ejemplos:
     x = atof("1.234") # Devuelve x = 1,234
     x = atof("1,234") # Devuelve x = 1
     x = atof("1.2y")  # Devuelve x = 1,2
     x = atof("y")     # Devuelve x = NA
     x = atof(",234")  # Devuelve x = NA
</code>

Consulta también <@ref="sscanf"> si quieres tener mayor flexibilidad en las conversiones de cadenas de texto en números. 

# bcheck programming
Resultado: 	escalar 
Argumentos:	<@var="objetivo">  (referencia a bundle)
		<@var="entrada">  (bundle, opcional)
		<@var="claves-requeridas">  (array de cadenas, opcional)

Principalmente pensada para que la utilicen los autores de paquetes de funciones. Este es el contexto en el que <@lit="bcheck"> puede ser útil: tienes una función que admite un argumento de tipo 'bundle' mediante el que el solicitante puede hacer varias elecciones. Algunos elementos del 'bundle' pueden tener valores predeterminados — por lo que el solicitante no está obligado a hacer una elección explícita — aunque pueden necesitarse otros elementos. Lo que quieres es determinar si el argumento que obtienes es correcto. El texto principal de abajo asume que el solicitante de tu función proporciona un 'bundle' de <@var="entrada">, pero consulta la sección titulada “Sin paquete de entrada”, en caso contrario. 

Para utilizar <@lit="bcheck">, construyes un modelo de 'bundle' que contenga todas las claves admitidas, con valores que ejemplifiquen el tipo asociado a cada clave, y lo pasas en forma de puntero como <@var="objetivo">. Para el segundo argumento, <@var="entrada">, pasas el 'bundle' que obtienes del solicitante. Entonces, esta función comprueba lo siguiente: 

<indent>
• Contiene la <@var="entrada"> alguna clave que no esté presente en el <@var="objetivo">? En tal caso, <@lit="bcheck"> devuelve un valor no nulo, indicando que la <@var="entrada"> es incorrecta. (Muy probablemente, la clave en cuestión está escrita incorrectamente). 
</indent>

<indent>
• Contiene la <@var="entrada">, bajo alguna de las claves indicadas, un objeto cuyo tipo no coincida con el del <@var="objetivo">? En tal caso, se devuelve un valor no nulo. 
</indent>

<indent>
• Si algunos elementos del <@var="objetivo"> necesitan una entrada del solicitante (por lo que el valor que indicas no es un valor predeterminado, si no solo un marcador de posición para indicar el tipo requerido), debes indicar un tercer argumento a <@lit="bcheck">: un 'array' de cadenas de texto que contenga las claves para las que no es opcional la entrada. Entonces, el valor devuelto será no nulo si falta alguno de los elementos requeridos de <@var="entrada">. 
</indent>

Además de lo anterior, podrías estar interesado en imponer límites inferiores y/o superiores al valor de uno o más de los elementos escalares del argumento de 'bundle'. En tal caso, añade un 'bundle' con el nombre <@lit="bounds"> a tu modelo de 'bundle'. Cada elemento de este 'bundle' secundario deberá tener una <@itl="clave"> que identifique un elemento del modelo de 'bundle'; su <@itl="valor"> deberá ser un vector 'doble' que contenga los límites inferiores y superiores. Pon un <@lit="NA"> en el lugar de uno de los límites, si no hay límite de ese lado. Así, por ejemplo, cuando se indica <@lit="x1"> en la entrada del solicitante, el siguiente código va a comprobar que está entre 1 y 5; y cuando se indica <@lit="x2">, que este no es negativo: 

<code>          
     template.bounds = _(x1={1,5}, x2={0,NA})
</code>

Cuando no se detectan fallos en ninguno de esos puntos, los valores indicados en <@var="entrada"> se copian a <@var="objetivo"> (substituyéndose los predeterminados por elecciones correctas en la parte del solicitante). Cuando se detecten fallos, se va a presentar un mensaje que indicará qué es lo que está mal en la <@var="entrada">. 

Para ofrecer un sencillo ejemplo, supón que tu 'bundle' de argumentos de la función admite una matriz <@lit="X"> (requerida), un escalar no negativo <@lit="z"> con valor 0 por defecto, y una cadena <@lit="s"> con el valor predeterminado “<@lit="display">”. Entonces, el siguiente fragmento de código sería adecuado para comprobar un 'bundle' de nombre <@lit="uservals"> proporcionado por el solicitante: 

<code>          
     bundle target = _(X={}, z=0, s="display")
     target.bounds = _(z={0,NA})
     strings req = defarray("X")
     err = bcheck(&target, uservals, req)
     if err
        # Reaccionar adecuadamente
     else
        # Proceder utilizando los valores en el objetivo
     endif
</code>

<subhead>Sin paquete de entrada</subhead> 

Si no se proporciona el 'bundle' de <@var="entrada"> a <@lit="bcheck">, se comporta del siguiente modo. Si el argumento <@var="claves-requeridas"> no se indica, devuelve el cero (puesto que no puede suceder ninguna de las condiciones de fallo que se mencionan más arriba), y <@var="objetivo"> no se modifica. En caso contrario, devuelve un valor no nulo puesto que está claro que una o más especificaciones deben estar ausentes. Esto significa que es seguro pasar una <@var="entrada"> nula a <@lit="bcheck">. 

# bessel math
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="tipo">  (carácter)
		<@var="v">  (escalar)
		<@var="x">  (escalar, serie o matriz)

Permite calcular una de las variantes de la función de Bessel de clase <@var="v"> con argumento <@var="x">. El valor que devuelve es del mismo tipo que este <@var="x">. La clase de la función se escoge con el primer argumento que debe ser <@lit="J">, <@lit="Y">, <@lit="I"> o <@lit="K">. Una buena discusión sobre las funciones de Bessel puede encontrarse en la Wikipedia, pero aquí se ofrecen unos breves comentarios. 

Caso <@lit="J">: función de Bessel de primera clase que se parece a una onda sinusoidal amortiguada. Se define para <@var="v"> real y <@var="x">; pero si <@var="x"> fuese negativo, entonces <@var="v"> debe ser un número entero. 

Caso <@lit="Y">: función de Bessel de segunda clase. Se define para <@var="v"> real y <@var="x">, pero con una singularidad en <@var="x"> = 0. 

Caso <@lit="I">: función de Bessel modificada de primera clase que presenta un crecimiento exponencial. Los argumentos que pueden usarse con ella son los mismos que en el caso <@lit="J">. 

Caso <@lit="K">: función de Bessel modificada de segunda clase que presenta un decrecimiento exponencial. Diverge en <@var="x"> = 0, no está definida para valores negativos de <@var="x">, y es simétrica en torno a <@var="v"> = 0. 

# BFGSmax numerical
Resultado: 	escalar 
Argumentos:	<@var="&b">  (referencia a matriz)
		<@var="f">  (llamada a función)
		<@var="g">  (llamada a función, opcional)

Devuelve un escalar con el resultado de una maximización numérica hecha con el método de Broyden, Fletcher, Goldfarb y Shanno. El argumento vectorial <@var="b"> debe contener los valores iniciales de un conjunto de parámetros, y el argumento <@var="f"> debe especificar una llamada a la función que va a calcular el criterio objetivo (escalar) que se quiere maximizar, dados los valores vigentes de los parámetros, así como cualesquiera otros datos que sean relevantes. Si lo que pretendes es en realidad minimizar el criterio objetivo, esta función devuelve el valor negativo de ese criterio objetivo. Cuando se completa con éxito su ejecución, <@lit="BFGSmax"> devuelve el valor maximizado del criterio objetivo, y <@var="b"> contiene finalmente los valores de los parámetros que proporcionan el máximo de ese criterio. 

El tercer argumento (opcional) establece una manera de proporcionar derivadas analíticas (en otro caso, el gradiente se computa numéricamente). La llamada <@var="g"> a la función gradiente debe tener como primer argumento a una matriz definida previamente que tenga el tamaño adecuado para poder almacenar el gradiente, indicado en forma de puntero. Así mismo, también necesita tener como argumento (en forma de puntero o no) al vector de parámetros. Otros argumentos son opcionales. 

Para más detalles y ejemplos, consulta <@pdf="El manual de gretl#chap:numerical"> (Capítulo 37). Ver también <@ref="BFGScmax">, <@ref="NRmax">, <@ref="fdjac">, <@ref="simann">. 

# BFGSmin numerical
Resultado: 	escalar 

Un alias de <@ref="BFGSmax">. Si invocas la función bajo este nombre, se ejecuta haciendo una minimización. 

# BFGScmax numerical
Resultado: 	escalar 
Argumentos:	<@var="&b">  (referencia a matriz)
		<@var="limites">  (matriz)
		<@var="f">  (llamada a función)
		<@var="g">  (llamada a función, opcional)

Devuelve un escalar con el resultado de una maximización con restricciones por medio del método L-BFGS-B (BFGS con memoria limitada, consulta <@bib="Byrd, Lu, Nocedal y Zhu, 1995;byrd-etal95">). El argumento vectorial <@var="b"> debe contener los valores iniciales de un conjunto de parámetros, el argumento <@var="limites"> debe contener las restricciones aplicadas a los valores de los parámetros (consulta más abajo), y el argumento <@var="f"> debe especificar una llamada a la función que va a calcular el criterio objetivo (escalar) que se quiere maximizar, dados los valores vigentes de los parámetros así como cualesquiera otros datos que sean relevantes. Si lo que pretendes realmente es minimizar el criterio objetivo, esta función debe devolver el valor negativo de ese criterio. Al completar con éxito su ejecución, <@lit="BFGScmax"> devuelve el valor máximo del criterio objetivo, dadas las restricciones de <@var="limites">, y <@var="b"> contiene finalmente los valores de los parámetros que maximizan el criterio. 

<subhead>Límites de los parámetros</subhead> 

La matriz <@var="limites"> debe tener 3 columnas, y un número de filas igual al número de elementos restringidos en el vector de parámetros. El primer elemento de una fila dada es el entero positivo que indexa el parámetro restringido; el segundo y el tercer elementos son los límites inferior y superior, respectivamente. Los valores <@lit="-$huge"> y <@lit="$huge"> deben usarse para indicar que el parámetro no posee restricciones inferiores o superiores, respectivamente. Por ejemplo, la siguiente expresión es la forma de especificar que el segundo elemento del vector de parámetros debe ser no negativo: 

<code>          
     matrix limites = {2, 0, $huge}
</code>

<subhead>Derivadas analíticas</subhead> 

El cuarto argumento (opcional) establece una manera de proporcionar derivadas analíticas (en otro caso, el gradiente se calcula numéricamente). La llamada <@var="g"> a la función gradiente debe tener como primer argumento a una matriz definida previamente que tenga el tamaño adecuado para poder almacenar el gradiente, indicado en forma de puntero. Así mismo, también necesita tener como argumento (en forma de puntero o no) al vector de parámetros. Otros argumentos son opcionales. 

Para más detalles y ejemplos, consulta <@pdf="El manual de gretl#chap:numerical"> (Capítulo 37). Ver también <@ref="BFGSmax">, <@ref="NRmax">, <@ref="fdjac">, <@ref="simann">. 

# BFGScmin numerical
Resultado: 	escalar 

Un alias de <@ref="BFGScmax">. Si invocas la función bajo este nombre, se ejecuta haciendo una minimización. 

# bin2dec matrix
Resultado: 	matriz 
Argumento: 	<@var="B">  (matriz)

Dada una matriz <@var="B"> que contenga únicamente ceros y unos, esta función interpreta cada fila de la matriz como si fuese una representación binaria de un entero de 32 bits sin signo; y devuelve un vector columna con las representaciones decimales de esos enteros. La matriz del argumento no puede tener más de 32 columnas; en caso contrario, se presenta un fallo. 

Ten en cuenta que se considera que el último bit significativo está en la primera columna. Así, la columna 1 se va a corresponder con <@mth="2"><@sup="0">, la columna 2 con <@mth="2"><@sup="1">, y así sucesivamente. Por ejemplo, la expresión 

<code>          
     scalar x = bin2dec({1,0,1})
</code>

guarda el valor 5 en <@mth="x">. 

La función <@ref="dec2bin"> realiza la transformación inversa. 

# bincoeff math
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="n">  (escalar, serie o matriz)
		<@var="k">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el coeficiente binomial. Este indica el número de formas en las que <@var="k"> elementos se poden escoger (sin repetición) de entre <@var="n"> elementos, independientemente de cómo estén ordenados. Esto también equivale al coeficiente del elemento (<@mth="k">+1)-ésimo en la expansión polinómica de la potencia de un binomio <@mth="(1+x)^n">. 

Para argumentos enteros,el resultado es <@mth="n!/(k!(n-k)!)">. Pero esta función también acepta argumentos no enteros, y en ese caso la fórmula de arriba se generaliza como Γ(<@mth="n">+1)/(Γ(<@mth="k">+1) × Γ(<@mth="n-k">+1)). 

Cuando <@var="k"> > <@var="n"> o <@var="k"> < 0, no hay una respuesta válida por lo que se muestra un fallo. 

Si los dos argumentos son de diferente tipo, el resultado será del tipo del “mayor” de los dos (siendo el criterio de ordenación matriz > serie > escalar). Por ejemplo, si <@var="n"> es un escalar, y <@var="k"> es un vector de dimensión <@mth="r"> (o viceversa), el resultado es un vector de dimensión <@mth="r">. Ten en cuenta que los argumentos matriciales deberán ser vectores. También que, si ningún argumento es un escalar, los dos habrán de tener la misma longitud. 

Consulta también <@ref="gammafun"> y <@ref="lngamma">. 

# binperms math
Resultado: 	matriz 
Argumentos:	<@var="n">  (entero)
		<@var="k">  (entero)

Permutaciones binarias: devuelve una matriz <@itl="p">×<@itl="n">, en la que cada una de sus filas contiene una ordenación diferente de <@mth="k"> unos y <@mth="n"> – <@mth="k"> ceros (en orden lexicográfico). El valor máximo admitido para <@mth="n"> es 64; tanto <@mth="n"> como <@mth="k"> deben ser no negativos; y <@mth="k"> no debe ser mayor que <@mth="n">; en caso contrario, se va a mostrar un fallo. Cuando <@mth="n"> = <@mth="k"> = 0 se devuelve una matriz vacía. 

Por ejemplo, con <@mth="n"> = 4 y <@mth="k"> = 2, el resultado es 

<code>          
     0   0   1   1 
     0   1   0   1 
     0   1   1   0 
     1   0   0   1 
     1   0   1   0 
     1   1   0   0
</code>

<@itl="Advertencia">: El número <@mth="p"> de permutaciones aumenta rápidamente en función de <@mth="n">; y resulta mayor cuando <@mth="k"> es aproximadamente la mitad de <@mth="n">. Entonces, podría interesarte comprobar con antelación la dimensión de la matriz que la función <@lit="binperms"> va a tratar de asignar. Como la función <@ref="bincoeff"> devuelve el valor de <@mth="p">, se puede calcular el tamaño en megabytes de la matriz resultante mediante 

<code>          
     MB = 8 * n * bincoeff(n, k) / 10^6
</code>

Para <@mth="n"> = 30, esto da como resultado cerca de 34 Mb cuando <@mth="k"> = 25, en torno a 7.211 Mb si <@mth="k"> = 20, y 20.758 Mb aproximadamente si <@mth="k"> = 18. 

# bkfilt timeseries
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="f1">  (entero, opcional)
		<@var="f2">  (entero, opcional)
		<@var="k">  (entero, opcional)

Devuelve una serie con el resultado de la aplicación del filtro paso-banda de Baxter–King a una serie <@var="y">. Los parámetros opcionales <@var="f1"> y <@var="f2"> representan, respectivamente, los límites inferior y superior del rango de frecuencias que se va a extraer, mientras que <@var="k"> representa el orden de aproximación que se va a utilizar. 

Si no se proporcionan esos argumentos, entonces los valores por defecto van a depender de la periodicidad del conjunto de datos. Para datos anuales los valores por defecto para <@var="f1">, <@var="f2"> y <@var="k"> son 2, 8 y 3 respectivamente; para datos trimestrales son 6, 32 y 12; y para datos mensuales son 18, 96 y 36. Esos valores se escogen para coincidir con la elección más común entre los usuarios, que consiste en la utilización de este filtro para extraer la componente de frecuencia del “ciclo de negocios”. Esto, a su vez, se define habitualmente comprendido entre 18 meses y 8 años. El filtro abarca 3 años de datos, en la elección por defecto. 

Si <@var="f2"> es mayor o igual al número de observaciones disponibles, entonces se ejecuta la versión “paso-bajo” del filtro, y la serie resultante debe considerarse como una estimación de la componente de tendencia, más que de la componente del ciclo. Ver también <@ref="bwfilt">, <@ref="hpfilt">. 

# bkw stats
Resultado: 	matriz 
Argumentos:	<@var="V">  (matriz)
		<@var="nombrespar">  (array de cadenas, opcional)
		<@var="detallado">  (booleano, opcional)

Ejecuta pruebas BKW de diagnóstico de multicolinealidad (consulta <@bib="Belsley, Kuh e Welsch (1980);belsley-etal80">) dada una matriz de covarianzas de las estimaciones de los parámetros, <@var="V">. El segundo argumento (opcional), puede ser una formación (array) de cadenas de texto o una cadena que contenga nombres separados por comas, y se usa para etiquetar las columnas que muestran las proporciones de varianza; el número de nombres debe coincidir con la dimensión de <@var="V">. Después de estimar un modelo en GRETL, puedes obtener argumentos adecuados para indicar en esta función por medio de los accesores <@ref="$vcv"> y <@ref="$parnames">. 

Por defecto, esta función opera silenciosamente, devolviendo tan solo la tabla BKW en forma de matriz, pero si indicas como tercer argumento un valor no nulo, la tabla se presenta junto con algunos análisis. 

También dispones de esta funcionalidad con formato de instrucción mediante <@xrf="bkw">, y se va a referir automáticamente al último modelo, sin requerir ningún argumento. 

# boxcox transforms
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="y">  (serie o matriz)
		<@var="d">  (escalar)

Devuelve el resultado de la transformación de Box–Cox con parámetro <@var="d"> de una serie positiva <@var="y"> (o de las columnas de una matriz <@var="y">). 

El resultado es (<@mth="y"><@sup="d"> - 1)/<@mth="d"> para <@mth="d"> distinto de cero, o log(<@mth="y">) para <@mth="d"> = 0. 

# bread data-utils
Resultado: 	bundle 
Argumentos:	<@var="nombrearchivo">  (cadena)
		<@var="importar">  (booleano, opcional)

Devuelve la lectura de un “bundle” desde un archivo especificado por el argumento <@var="nombrearchivo">. Por defecto, se asume que el bundle está representado en XML; y que se le aplicó la compresión gzip si <@var="nombrearchivo"> tiene extensión <@lit=".gz">. Pero si la extensión es <@lit=".json"> o <@lit=".geojson">, se asume que el contenido es de tipo JSON. 

En el caso XML, el archivo debe contener un elemento <@lit="gretl-bundle">, que se use para almacenar cero o más elementos <@lit="bundled-item">. Por ejemplo: 

<code>          
     <?xml version="1.0" encoding="UTF-8"?>
     <gretl-bundle name="temp">
          <bundled-item key="s" type="string">moo</bundled-item>
          <bundled-item key="x" type="scalar">3</bundled-item>
     </gretl-bundle>
</code>

Como cabría esperar, los archivos que se leen de forma adecuada por medio de <@lit="bread"> se generan mediante la función asociada <@ref="bwrite">. 

Si el nombre del archivo no contiene la especificación completa de la ruta al directorio donde está, entonces va a buscarse en varias localizaciones “probables”, comenzando por lo establecido como <@xrf="workdir"> vigente. Ahora bien, cuando se proporciona un valor no nulo para el argumento opcional <@var="importar">, el archivo va a buscarse en el directorio “punto” del usuario. En este caso, el argumento <@var="nombrearchivo"> deberá ser un nombre simple de archivo, sin la inclusión de la ruta al directorio. 

Si ocurre algún fallo (por ejemplo,si el archivo está mal formateado o es inaccesible), se devuelve el fallo por medio del accesor <@ref="$error">. 

Ver también <@ref="mread">, <@ref="bwrite">. 

# brename data-utils
Resultado: 	escalar 
Argumentos:	<@var="B">  (bundle)
		<@var="antiguaclave">  (cadena)
		<@var="nuevaclave">  (cadena)

Si el 'bundle' <@var="B"> contiene un elemento que tenga la clave <@var="antiguaclave">, esa clave se cambia por <@var="nuevaclave">; en caso contrario, se muestra un fallo. La función devuelve un 0 cuando se hace correctamente el cambio de nombre. 

Cambiar la clave de un elemento de un 'bundle' no es una tarea habitual, pero puede surgir esa necesidad en el contexto de funciones que operan con 'bundles', y <@lit="brename"> resulta ser una herramienta eficiente para ese trabajo. Ejemplo: 

<code>          
     # Establecer un 'bundle' que contiene una matriz grande
     bundle b
     b.X = mnormal(1000, 1000)
     if 0
         # 'Cambiar la clave manualmente'
         Xcopy = b.X
         delete b.X
         b.Y = Xcopy
         delete Xcopy
     else
         # frente a 'Cambiarla de forma eficiente'
         brename(b, "X", "Y")
     endif
</code>

El primer método exige que se copie esa gran matriz dos veces: primero fuera del 'bundle', y luego de nuevo dentro de él bajo una clave diferente. El método eficiente cambia la clave directamente. 

# bwfilt timeseries
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="n">  (entero)
		<@var="omega">  (escalar)

Devuelve una serie con lo que resulta al aplicar un filtro paso-bajo de Butterworth de orden <@var="n"> y frecuencia de corte <@var="omega">, en la serie <@var="y">. El corte se expresa en grados y debe ser mayor o igual a cero, y menor que 180. Los valores de corte más pequeños van a restringir el paso-banda a menores frecuencias, y así producen una tendencia más suave. Los valores mayores de <@var="n"> producen un corte más agudo, pero con el coste de poder tener inestabilidad numérica. 

La inspección preliminar del periodograma de la serie de interés es muy útil cuando se desea aplicar esta función. Para obtener más detalles, consulta <@pdf="El manual de gretl#chap:tsfilter"> (Capítulo 30). Ver también <@ref="bkfilt">, <@ref="hpfilt">. 

# bwrite data-utils
Resultado: 	entero 
Argumentos:	<@var="B">  (bundle)
		<@var="nombrearchivo">  (cadena)
		<@var="exportar">  (booleano, opcional)

Escribe el 'bundle' <@var="B"> en un archivo, serializado en XML; o como JSON, si <@var="nombrearchivo"> tiene extensión <@lit=".json"> o <@lit=".geojson">. Consulta <@ref="bread"> para obtener una descripción del formato cuando se usa XML. Si ya existe un archivo denominado <@var="nombrearchivo">, este va a sobrescribirse. Esta función devuelve el valor nominal 0 en caso de que concluya con éxito; si fracasa la escritura se muestra un fallo. 

El archivo de salida se guarda en el directorio <@xrf="workdir"> vigente, excepto que <@var="nombrearchivo"> contenga la ruta completa con el directorio en el que va a guardarse. Ahora bien, cuando se indica un valor no nulo para el argumento <@var="exportar">, el archivo se va a guardar en el directorio “punto” del usuario. En este caso, el argumento <@var="nombrearchivo"> deberá ser un nombre simple de archivo, sin la inclusión de la ruta al directorio. 

Dispones de la opción de compresión gzip, pero únicamente en caso de que el resultado sea de tipo XML. Esto se va a aplicar si <@var="nombrearchivo"> tiene la extensión <@lit=".gz">. 

Ver también <@ref="bread">, <@ref="mwrite">. 

# carg complex
Resultado: 	matriz 
Argumento: 	<@var="C">  (matriz compleja)

Devuelve una matriz real de dimensión <@itl="m">×<@itl="n"> que contiene el “argumento” complejo de cada elemento de la matriz compleja <@var="C"> de dimensión <@itl="m">×<@itl="n">. El argumento del número complejo <@mth="z"> = <@mth="x"> + <@mth="yi"> también puede calcularse mediante <@lit="atan2(y, x)">. 

Ver también <@ref="abs">, <@ref="cmod">, <@ref="atan2">. 

# cdemean transforms
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="tipificar">  (booleano, opcional)
		<@var="obviar_na">  (booleano, opcional)

Centra las columnas de la matriz <@var="X"> respecto a sus medias. Si el segundo argumento (opcional) tiene un valor no nulo, entonces los valores centrados se dividen además por las desviaciones típicas de cada columna (que se caculan utilizando <@mth="n"> – 1 como divisor, en el que <@mth="n"> es el número de filas de <@var="X">). 

Si indicas un valor no nulo para <@var="obviar_na"> se van a ignorar los valores ausentes; en caso contrario, si una columna de la matriz <@var="X"> contiene algún valor ausente, la columna correspondiente de la salida va a tener todos ausentes. 

Ten en cuenta que <@ref="stdize"> proporciona una funcionalidad más flexible. 

# cdf probdist
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="d">  (cadena)
		<@var="…">  (mira más abajo)
		<@var="x">  (escalar, serie o matriz)
Ejemplos: 	<@lit="p1 = cdf(N, -2.5)">
		<@lit="p2 = cdf(X, 3, 5.67)">
		<@lit="p3 = cdf(D, 0.25, -1, 1)">

Calcula el valor de la función de distribución acumulativa, y devuelve un resultado (del mismo tipo que el argumento) con la probabilidad <@mth="P(X ≤ x)">, donde la distribución de <@mth="X"> se especifica mediante la letra <@var="d">. Entre los argumentos <@var="d"> y <@var="x"> puede necesitarse algún argumento adicional escalar para especificar los parámetros de la distribución, tal y como se indica a continuación (pero observa que la distribución Normal tiene su propia función, por conveniencia, <@ref="cnorm">): 

<indent>
• Normal estándar (d = z, n o N): sin argumentos extras 
</indent>

<indent>
• Normal bivariante (D): coeficiente de correlación 
</indent>

<indent>
• Logística (lgt o s): sin más argumentos 
</indent>

<indent>
• t de Student (t): grados de libertad 
</indent>

<indent>
• Chi-cuadrado (c, x o X): grados de libertad 
</indent>

<indent>
• F de Snedecor (f o F): grados de libertad (num.), grados de libertad (den.) 
</indent>

<indent>
• Gamma (g o G): forma, escala 
</indent>

<indent>
• Beta (beta): 2 parámetros de forma 
</indent>

<indent>
• Binomial (b o B): probabilidad, cantidad de ensayos 
</indent>

<indent>
• Poisson (p o P): media 
</indent>

<indent>
• Exponencial (exp): escala 
</indent>

<indent>
• Weibull (w o W): forma, escala 
</indent>

<indent>
• Laplace (l o L): media; escala 
</indent>

<indent>
• Error Generalizado (E): forma 
</indent>

<indent>
• Chi-cuadrado no central (ncX): grados de libertad, parámetro de no centralidad 
</indent>

<indent>
• F no central (ncF): grados de libertad (num.), grados de libertad (den.), parámetro de no centralidad 
</indent>

<indent>
• t no central (nct): grados de libertad, parámetro de no centralidad 
</indent>

Ten en cuenta que, en la mayoría de los casos, existen alias para ayudar a memorizar los códigos. El caso de la Normal bivariante es especial: la la sintaxis es <@lit="x = cdf(D, rho, z1, z2)"> donde <@lit="rho"> es el coeficiente de correlación entre las variables <@lit="z1"> y <@lit="z2">. 

Ver también <@ref="pdf">, <@ref="critical">, <@ref="invcdf">, <@ref="pvalue">. 

# cdiv complex
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="Y">  (matriz)

Esta es una función heredada, anterior al soporte original de GRETL para matrices complejas. 

Devuelve una matriz con el resultado de dividir números complejos. Los dos argumentos deben componerse del mismo número de filas, <@mth="n">, y de una o dos columnas. La primera columna contiene la parte real, y la segunda (si existe) contiene la parte imaginaria. El resultado que se devuelve es una matriz de orden <@itl="n">×2 o, en caso de no existir la parte imaginaria, un vector con <@mth="n"> filas. Ver también <@ref="cmult">. 

# cdummify transforms
Resultado: 	lista 
Argumento: 	<@var="L">  (lista)

Esta función devuelve una lista en la que cada serie del argumento <@var="L"> que tenga el atributo “codificado”, se substituye por un conjunto de variables ficticias que representan cada uno de sus valores codificados, pero omitiendo el valor más pequeño. Si el argumento <@var="L"> no contiene ninguna serie codificada, el valor que se devuelve va a ser idéntico a <@var="L">. 

En caso de que se generen, las variables ficticias se nombran con el patrón <@lit="D"><@var="varname"><@lit="_"><@var="vi">, en el que <@var="vi"> indica el <@var="i"><@sup="-ésimo"> valor representado de la variable que se codifica. En caso de que algunos de los valores sean negativos, se va a insertar “m” antes del valor (absoluto) de <@var="vi">. 

Por ejemplo, supón que <@var="L"> contiene una serie codificada llamada <@lit="C1"> con los valores –9, –7, 0, 1 y 2. Entonces, las variables ficticias generadas van a ser <@lit="DC1_m7"> (que codifica cuando C1 = –7), <@lit="DC1_0"> (que codifica cuando C1 = 0), etcétera. 

Ver también <@ref="dummify">, <@ref="getinfo">. 

# ceil math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Función tope: devuelve un resultado (del tipo del argumento) con el menor entero que sea mayor o igual a <@var="x">. Ver también <@ref="floor">, <@ref="int">. 

# cholesky linalg
Resultado: 	matriz cuadradax 
Argumento: 	<@var="A">  (matriz definida positiva)

Realiza la descomposición de Cholesky de <@var="A">. Cuando <@var="A"> sea una matriz real, deberá ser simétrica y definida positiva; en ese caso, el resultado será una matriz triangular inferior <@mth="L"> que verificará <@mth="A = LL'">. Cuando <@var="A"> sea compleja, deberá ser Hermítica y definida positiva; y el resultado será una matriz compleja triangular inferior de forma que <@mth="A = LL^H">. En caso contrario, la función devolverá un error. 

Para el caso real, consulta también <@ref="psdroot"> y <@ref="Lsolve">. 

# chowlin timeseries
Resultado: 	matriz 
Argumentos:	<@var="Y">  (matriz)
		<@var="factorx">  (entero)
		<@var="X">  (matriz, opcional)

No se recomienda seguir utilizando esta función; en su lugar, utiliza <@ref="tdisagg">. 

Devuelve una matriz como resultado de expandir los datos de entrada, <@var="Y">, a una frecuencia mayor, con el método de <@bib="Chow y Lin (1971);chowlin71">. Se asume que las columnas de <@var="Y"> representan series de datos. La matriz que se devuelve tiene el mismo número de columnas que <@var="Y"> y <@var="factorx"> veces su número de filas. También se asume que cada valor de baja frecuencia debe tratarse como la media de <@var="factorx"> valores de alta frecuencia. 

El valor de <@var="factorx"> debe ser igual a 3 para expandir datos trimestrales a mensuales, 4 para hacerlo de anuales a trimestrales, o 12 de anuales a mensuales. Puedes usar el tercer argumento (opcional) para proveer una matriz de covariables con un objetivo de mayor frecuencia. 

Los regresores que se utilizan por defecto son una constante y una tendencia. Cuando se proporciona <@var="X">, sus columnas se utilizan como regresores adicionales. La función devuelve un fallo si el número de filas de <@var="X"> no es igual a <@var="factorx"> veces el número de filas de <@var="Y">. 

# cmod complex
Resultado: 	matriz 
Argumento: 	<@var="C">  (matriz compleja)

Devuelve una matriz real de dimensión <@itl="m">×<@itl="n"> que contiene el módulo complejo de cada elemento de la matriz compleja <@var="C"> de dimensión <@itl="m">×<@itl="n">. El módulo del número complejo <@mth="z"> = <@mth="x"> + <@mth="yi"> es igual a la raíz cuadrada de <@mth="x"><@sup="2"> + <@mth="y"><@sup="2">. 

Ver también <@ref="abs">, <@ref="carg">. 

# cmult complex
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="Y">  (matriz)

Esta es una función heredada, anterior al soporte original de GRETL para matrices complejas. 

Devuelve una matriz con el resultado de multiplicar números complejos. Los dos argumentos deben componerse del mismo número de filas, <@mth="n">, y de una o dos columnas. La primera columna contiene la parte real y la segunda (si existe) contiene la parte imaginaria. El resultado que se devuelve es una matriz de orden <@itl="n">×2 o, en caso de no existir la parte imaginaria, un vector con <@mth="n"> filas. Ver también <@ref="cdiv">. 

# cnorm probdist
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve la función de distribución acumulativa para una Normal estándar. Ver también <@ref="dnorm">, <@ref="qnorm">. 

# cnumber linalg
Resultado: 	escalar 
Argumento: 	<@var="X">  (matriz)

Devuelve un escalar con el número de condición de una matriz <@var="X"> de orden <@itl="n">×<@itl="k">, conforme se define en <@bib=" Belsley, Kuh y Welsch (1980);belsley-etal80">. Si las columnas de <@var="X"> son mutuamente ortogonales, el número de condición de <@var="X"> es la unidad. Por el contrario, un valor grande del número de condición se entiende como un indicio de alto grado de multicolinealidad; habitualmente se considera que el valor es “grande” si es mayor o igual a 50 (o, algunas veces, a 30). 

Los pasos para hacer los cálculos son: (1) formar una matriz <@mth="Z"> cuyas columnas sean el resultado de dividir cada columna de <@var="X"> por su respectiva norma euclidiana; (2) construir la matriz <@mth="Z'Z"> y obtener sus autovalores; y (3) calcular la raíz cuadrada de la razón entre el mayor y el menor autovalor. 

Ver también <@ref="rcond">. 

# cnameget strings
Resultado: 	cadena o array de cadenas 
Argumentos:	<@var="M">  (matriz)
		<@var="col">  (entero, opcional)

Si indicas el argumento <@var="col">, devuelve una cadena de texto con el nombre de la columna <@var="col"> de la matriz <@var="M">. Si las columnas de <@var="M"> no tienen nombre, entonces se devuelve una cadena vacía; y si <@var="col"> está fuera de los límites del número de columnas de esta matriz, se muestra un fallo. 

Si no indicas el segundo argumento, devuelve un 'array' de cadenas de texto que contiene los nombres de las columnas de <@var="M">, o un 'array' vacío si <@var="M"> no tiene asignados nombres de columnas. 

Ejemplo: 

<code>          
     matrix A = { 11, 23, 13 ; 54, 15, 46 }
     cnameset(A, "Col_A Col_B Col_C")
     string name = cnameget(A, 3)
     print name
</code>

Ver también <@ref="cnameset">. 

# cnameset matrix
Resultado: 	escalar 
Argumentos:	<@var="M">  (matriz)
		<@var="S">  (array de cadenas o lista)

Añade nombres a las columnas de la matriz de orden <@itl="T">×<@itl="k">, <@var="M">. Cuando <@var="S"> es una lista, los nombres son los de las series listadas (es necesario que esa lista tenga <@mth="k">elementos). Cuando <@var="S"> es un 'array' de cadenas de texto, deberá tener <@mth="k"> elementos. Como segundo argumento también se acepta una única cadea de texto; en ese caso, esta cadena necesita tener <@mth="k"> subcadenas separadas por espacios. Como caso especial, el hecho de indicar una cadena de texto vacía como segundo argumento, tiene como efecto la eliminación de cualquier nombre de columna existente. 

Devuelve el valor nominal 0 si las columnas son nombradas con éxito; en caso de que no funcione se muestra un fallo. Consulta también <@ref="rnameset">. 

Ejemplo: 

<code>          
     matrix M = {1, 2; 2, 1; 4, 1}
     strings S = array(2)
     S[1] = "Col1"
     S[2] = "Col2"
     cnameset(M, S)
     print M
</code>

# cols matrix
Resultado: 	entero 
Argumento: 	<@var="X">  (matriz)

Devuelve un entero con el número de columnas de la matriz <@var="X">. Ver también <@ref="mshape">, <@ref="rows">, <@ref="unvech">, <@ref="vec">, <@ref="vech">. 

# commute linalg
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz)
		<@var="m">  (entero)
		<@var="n">  (entero, opcional)
		<@var="post">  (entero, opcional)
		<@var="add_id">  (entero, opcional)

Devuelve el resultado de premultiplicar la matriz <@var="A"> por la matriz <@mth="K"><@sub="m,n"> de conmutación (utilizando un algoritmo que es más eficiente que la propia multiplicación explícita). Se asume que cada columna de <@var="A"> procede de una operación de vectorización sobre una matriz <@mth="m x n">. En particular, 

<code>          
     commute(vec(B), rows(B), cols(B))
</code>

proporciona vec(<@mth="B'">). Con el objeto de calcular la matriz de conmutación apropiada, aplica simplemente la función a una matriz identidad con el tamaño adecuado. Por ejemplo: 

<code>          
     K_32 = commute(I(6), 3, 2)
</code>

Por defecto, el argumento opcional <@var="n"> está establecido que sea igual a <@var="m">. Cuando el argumento opcional <@var="post"> no es cero, se lleva a cabo la multiplicación posterior en lugar de la multiplicación previa; y la opción Booleana <@var="add_id"> va a premultiplicar la matriz <@var="A"> por <@mth="I + K"><@sub="m,n"> en lugar de <@mth="K"><@sub="m,n">. 

# complex complex
Resultado: 	matriz compleja 
Argumentos:	<@var="A">  (escalar o matriz)
		<@var="B">  (escalar o matriz, opcional)

Devuelve una matriz compleja, en la que se toma <@var="A"> para ofrecer la parte real y <@var="B"> para la parte imaginaria. Si <@var="A"> es de dimensión <@itl="m">×<@itl="n"> y <@var="B"> es un escalar, el resultado es una matriz <@itl="m">×<@itl="n"> con una parte imaginaria constante (y de forma similar en el caso recíproco, pero con una parte real constante). Si ambos argumentos son matrices, deben tener las mismas dimensiones. Si omites el segundo argumento, la parte imaginaria se establece por defecto como cero. Ver también <@ref="cswitch">. 

# conj complex
Resultado: 	matriz compleja 
Argumento: 	<@var="C">  (matriz compleja)

Devuelve una matriz compleja de dimensión <@itl="m">×<@itl="n"> que contiene el conjugado complejo de cada elemento de la matriz compleja <@var="C"> de dimensión <@itl="m">×<@itl="n">. El conjugado de un número complejo <@mth="z"> = <@mth="x"> + <@mth="yi"> es igual a <@mth="x"> – <@mth="yi">. 

Ver también <@ref="carg">, <@ref="abs">. 

# contains data-utils
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="x">  (escalar, serie o matriz)
		<@var="S">  (matriz)

Proporciona un medio de determinar si un objeto numérico <@var="x"> está contenido en alguno de los elementos de una matriz <@var="S"> (que cumple el papel de un conjunto). 

El valor que se devuelve es un objeto del mismo tamaño que <@var="x"> que contiene valores de 1 en las posiciones donde el valor <@var="x"> coincide con algún elemento de <@var="S">, y ceros en las demás. Por ejemplo, el código 

<code>          
     matrix A = mshape(seq(1,9), 3, 3)
     matrix C = contains(A, {1, 5, 9})
</code>

produce 

<code>          
     A (3 x 3)

     1   4   7
     2   5   8
     3   6   9

     C (3 x 3)

     1   0   0
     0   1   0
     0   0   1
</code>

Esta función puede ser particularmente útil cuando <@var="x"> es una serie que contiene una codificación muy refinada para una característica cualitativa, y quieres reducir esto a un número de categorías menor. Puedes meter en <@var="S"> un conjunto de valores a consolidar, y obtener una variable ficticia con el valor 1 para las observaciones que coinciden con este conjunto, y el valor 0 para las demás. 

Puesto que <@var="S"> funciona como un conjunto, debiera ser un vector sin valores repetidos para tener una mayor eficiencia; sin embargo, se acepta una matriz cualquiera. 

# conv2d linalg
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz)
		<@var="B">  (matriz)

Devuelve una matriz con el cálculo de la convolución bidimensional (2D) de dos matrices <@var="A"> y <@var="B">. Si <@var="A"> es de orden <@itl="r">×<@itl="c"> y <@var="B"> es de orden <@itl="m">×<@itl="n">, entonces la matriz que se devuelve tendrá <@mth="r+m-1"> filas y <@mth="c+n-1"> columnas. 

Ver también <@ref="fft">, <@ref="filter">. 

# cquad complex
Resultado: 	matriz 
Argumento: 	<@var="Z">  (matriz)

Dada una matriz compleja <@var="Z"> de orden <@itl="m">×<@itl="n">, esta instrucción devuelve una matriz real de orden <@itl="m">×<@itl="n"> que contiene las "cuadranzas" de cada uno de los elementos de <@var="Z">. La cuadranza de un número complejo <@mth="z"> = <@mth="a"> + <@mth="bi"> se define como <@mth="a"><@sup="2"> + <@mth="b"><@sup="2">. Por tanto, es igual al cuadrado del módulo de <@mth="z">, y también es igual a <@mth="z"> multiplicado por su conjugado complejo; pero el cálculo directo que realiza <@lit="cquad"> es considerablemente más rápido que cualquiera de las otras propuestas alternativas. 

# corr stats
Resultado: 	escalar 
Argumentos:	<@var="y1">  (serie o vector)
		<@var="y2">  (serie o vector)

Devuelve un escalar con el valor del coeficiente de correlación entre <@var="y1"> e <@var="y2">. Los argumentos deben ser dos series o dos vectores del mismo tamaño. Ver también <@ref="cov">, <@ref="mcov">, <@ref="mcorr">, <@ref="npcorr">. 

# corresp stats
Resultado: 	escalar 
Argumentos:	<@var="a">  (serie o vector)
		<@var="b">  (serie o vector)

En base a una tabulación cruzada de <@var="a"> y <@var="b">, devuelve un código de tipo entero que indica la clase de correspondencia entre las dos variables, del siguiente modo. 

<indent>
• Código = 2: hay una relación de tipo 1 a 1. 
</indent>

<indent>
• Código = 1: hay una relación de tipo 1 a n (<@var="a"> “anida” <@var="b">, puede interpretarse como una función de <@var="b"> en sentido matemático). 
</indent>

<indent>
• Código = –1: hay una relación de tipo n a 1 (<@var="b"> “anida” <@var="a">, puede interpretarse como una función de <@var="a">). 
</indent>

<indent>
• Código = 0: hay ninguna relación. 
</indent>

Ten en cuenta que estos códigos se basan exclusivamente en los valores muestrales de los dos argumentos. En caso de que <@var="b"> sea el cuadrado de <@var="a">, por ejemplo, el resultado va a diferir dependiendo de si <@var="a"> contiene algunos pares de valores que únicamente se diferencien en el signo (código = –1), o no (código = 2). 

Un caso de posible utilización consiste en comprobar si dos series discretas codifican la misma información. Por ejemplo, las siguientes expresiones 

<code>          
     open grunfeld.gdt
     c = corresp($unit, firm)
</code>

proporcionan <@lit="c"> = 2, indicando que la serie <@lit="firm"> es de hecho un único identificador para las unidades de sección cruzada en ese conjunto de datos de panel. 

Consulta también <@ref="mxtab">. 

# corrgm timeseries
Resultado: 	matriz 
Argumentos:	<@var="x">  (serie, matriz o lista)
		<@var="p">  (entero)
		<@var="y">  (serie o vector, opcional)

Cuando se proporcionan solo los dos primeros argumentos, la función devuelve una matriz con el correlograma de <@var="x"> para los retardos desde 1 hasta <@var="p">. Si <@mth="k"> es el número de elementos de <@var="x"> (igual a 1 si <@var="x"> es una serie, igual al número de columnas si <@var="x"> es una matriz, o igual al número de elementos si <@var="x"> es una lista), el valor que se devuelve es una matriz con <@var="p"> filas y 2<@mth="k"> columnas, en la que las <@mth="k"> primeras columnas contienen las respectivas autocorrelaciones, y las restantes contienen las respectivas autocorrelaciones parciales. 

Cuando se indica el tercer argumento, esta función calcula el correlograma cruzado desde <@mth="+"><@var="p"> hasta <@mth="-"><@var="p"> para cada uno de los <@mth="k"> elementos de <@var="x"> e <@var="y">. La matriz que se devuelve se compone de 2<@mth="p"> + 1 filas y <@mth="k"> columnas. Si <@var="x"> es una serie o una lista, e <@var="y"> es un vector, este último es necesario que tenga tantas filas como el número total de observaciones que hay en la muestra seleccionada en vigor. 

# cos math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el coseno de <@var="x">. Ver también <@ref="sin">, <@ref="tan">, <@ref="atan">. 

# cosh math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el coseno hiperbólico de <@var="x">. 

Ver también <@ref="acosh">, <@ref="sinh">, <@ref="tanh">. 

# cov stats
Resultado: 	escalar 
Argumentos:	<@var="y1">  (serie o vector)
		<@var="y2">  (serie o vector)

Devuelve un escalar con la covarianza entre <@var="y1"> e <@var="y2">. Los argumentos deben ser dos series, o bien dos vectores de la misma longitud. Ver también <@ref="corr">, <@ref="mcov">, <@ref="mcorr">. 

# critical probdist
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="c">  (carácter)
		<@var="…">  (mira más abajo)
		<@var="p">  (escalar, serie o matriz)
Ejemplos: 	<@lit="c1 = critical(t, 20, 0.025)">
		<@lit="c2 = critical(F, 4, 48, 0.05)">

Permite calcular valores críticos, y devuelve un resultado del mismo tipo que el introducido. El valor <@mth="x"> que se devuelve va a cumplir <@mth="P(X > x) = p">, donde la distribución de <@mth="X"> se determina por la letra <@var="c">. Entre los argumentos <@var="d"> y <@var="x">, puede necesitarse algún otro adicional (escalar) para indicar los parámetros de la distribución. Esto se hace de este modo: 

<indent>
• Normal estándar (c = z, n o N): sin argumentos extras 
</indent>

<indent>
• t de Student (t): grados de libertad 
</indent>

<indent>
• Chi-cuadrado (c, x o X): grados de libertad 
</indent>

<indent>
• F de Snedecor (f o F): grados de libertad (num.), grados de libertad (den.) 
</indent>

<indent>
• Binomial (b o B): probabilidad, cantidad de ensayos 
</indent>

<indent>
• Poisson (p o P): media 
</indent>

<indent>
• Laplace (l o L): media; escala 
</indent>

<indent>
• Error Generalizado (E): forma 
</indent>

Ver también <@ref="cdf">, <@ref="invcdf">, <@ref="pvalue">. 

# cswitch complex
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz)
		<@var="modo">  (escalar)

Reinterpreta una matriz real como si contuviese valores complejos, o viceversa. La acción concreta depende de <@var="modo"> (que deberá tener un valor de 1, 2, 3 o 4), como se explica a continuación: 

Modo 1: El argumento <@var="A"> debe ser una matriz real con un número par de columnas. La función devuelve una matriz con la mitad de las columnas, con valores complejos formados usando las columnas impares de <@var="A"> para las partes reales, y las columnas pares para las partes imaginarias. 

Modo 2: Permite realizar la operación inversa a la del modo 1. El argumento <@var="A"> debe ser una matriz compleja, y el resultado que se devuelve es una matriz real que tendrá el doble de columnas que las de <@var="A">. 

Modo 3: El argumento <@var="A"> debe ser una matriz real con un número par de filas. La función devuelve una matriz con la mitad de las filas, con valores complejos formados usando las filas impares de <@var="A"> para las partes reales, y las filas pares para las partes imaginarias. 

Modo 4: Permite realizar la operación inversa a la del modo 3. El argumento <@var="A"> debe ser una matriz compleja, y el resultado que se devuelve es una matriz real que tendrá el doble de filas que las de <@var="A">. 

Ver también <@ref="complex">. 

# ctrans complex
Resultado: 	matriz compleja 
Argumento: 	<@var="C">  (matriz compleja)

Devuelve una matriz compleja de dimensión <@itl="n">×<@itl="m"> que contiene la traspuesta conjugada de la matriz compleja <@var="C"> de dimensión <@itl="m">×<@itl="n">. El operador <@lit="'"> (trasponer) realiza también la trasposición conjugada de matrices complejas. Puedes utilizar la función <@ref="transp"> con matrices complejas, pero esto va a hacer la trasposición “directa” (no la conjugada). 

# cum transforms
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (serie o matriz)

Acumula <@var="x"> (es decir, crea una suma móvil). Cuando <@var="x"> es una serie, produce una serie <@mth="y"> en la que cada uno de sus elementos es igual a la suma de los valores de <@var="x"> hasta la observación correspondiente. El punto de partida para la acumulación es la primera observación no ausente de la muestra vigente seleccionada. Cuando se encuentra algún valor ausente en <@mth="x">, se van a establecer como ausentes los valores consiguientes de <@mth="y">. Cuando <@var="x"> es una matriz, sus elementos se acumulan por columnas. 

En el caso de datos de panel, la acumulación se produce en la dimensión temporal, comenzando de nuevo para cada unidad del panel. 

Si deseas que la acumulación ignore los valores ausentes (es decir, para tratarlos como si fuesen ceros), puedes aplicar la función <@ref="misszero"> al argumento, como en 

<code>          
     series cx = cum(misszero(x))
</code>

Ver también <@ref="diff">. 

# curl data-utils
Resultado: 	entero 
Argumento: 	<@var="&b">  (referencia a bundle)

Ofrece un medio bastante flexible de obtener un “buffer” de texto que contiene datos de un servidor de internet, utilizando la biblioteca 'libcurl'. Al escribirla, el argumento de tipo 'bundle' <@var="b">, debe contener una cadena de texto llamada <@lit="URL"> que indica la dirección completa del recurso en el 'host' de destino. Otros elementos opcionales se presentan a continuación: 

<indent>
• “<@lit="header">”: una cadena de texto que especifica un 'header' HTTP que va a enviarse al 'host'. 
</indent>

<indent>
• “<@lit="postdata">”: una cadena de texto que contiene los datos que van a enviarse al 'host'. 
</indent>

Los campos <@lit="header"> y <@lit="postdata"> se destinan para usarse con una solicitud HTTP de tipo <@lit="POST">. Si está presente <@lit="postdata">, va implícito el método <@lit="POST">; en caso contrario, va implícito el método <@lit="GET">. (Pero observa que para sencillas solicitudes <@lit="GET">, la función <@ref="readfile"> ofrece una interfaz más simple.) 

Se reconoce otro elemento opcional del 'bundle': si está presente un escalar llamado <@lit="include"> y tiene un valor no nulo, esto se entiende como una solicitud para incluir el 'header' recibido del 'host', en el cuerpo del resultado. 

Al completarse la solicitud, el texto recibido del servidor se añade al 'bundle' con la clave “<@lit="output">”. 

La función va a fallar si hay una equivocación al formular la solicitud (por ejemplo, si no existe una <@lit="URL"> en la entrada); en caso contrario, va a devolver el valor 0 si la solicitud prospera, o un valor no nulo si no lo hace. En este último caso, se añade el mensaje de fallo de la biblioteca 'curl' al 'bundle', con el identificador “<@lit="errmsg">”. Ten en cuenta, sin embargo, que “éxito” en este sentido no significa necesariamente que obtienes los datos que deseabas; en realidad significa tan solo que se recibió alguna respuesta del servidor. Debes comprobar el contenido del “buffer” de salida (que de hecho puede ser un mensaje tal como “Página no encontrada”). 

Aquí tenemos un ejemplo de cómo utilizar esta función: para bajar algunos datos de la web de la US Bureau of Labor Statistics, que requiere el envío de una consulta JSON. Observa el uso de <@ref="sprintf"> para insertar comillas en los datos <@lit="POST">. 

<code>          
     bundle req
     req.URL = "http://api.bls.gov/publicAPI/v1/timeseries/data/"
     req.include = 1
     req.header = "Content-Type: application/json"
     string s = sprintf("{\"seriesid\":[\"LEU0254555900\"]}")
     req.postdata = s
     err = curl(&req)
     if err == 0
         s = req.output
         string line
         loop while getline(s, &line)
             printf "%s\n", line
         endloop
     endif
</code>

Consulta también las funciones <@ref="jsonget"> y <@ref="xmlget"> para ver modos de procesamiento de datos recibidos en formato JSON y XML, respectivamente. 

# dayspan calendar
Resultado: 	entero 
Argumentos:	<@var="d1">  (entero)
		<@var="d2">  (entero)
		<@var="duracsemana">  (entero)

Devuelve un número entero con el número de días (relevantes) entre los días de época <@var="d1"> y <@var="d2">, ambos incluidos, considerando la duración de semana indicada por el argumento <@var="duracsemana">. Este debe ser igual a 5, 6 o 7 (indicando el valor 6 que no se cuentan los domingos, y el 5 que no se cuentan ni los sábados ni los domingos). 

Para obtener los días de época en el formato más familiar de las fechas, consulta <@ref="epochday">. Relacionado con esto, consulta <@ref="smplspan">. 

# dec2bin matrix
Resultado: 	matriz 
Argumento: 	<@var="x">  (matriz)

Esta función devuelve la representación binaria de los números contenidos en el vector columna <@var="x">, guardando cada dígito binario en una columna de la matriz que se devuelve, y que siempre tiene 32 columnas. Cada elemento de <@var="x"> debe ser un entero entre 0 y <@mth="2"><@sup="32"><@mth="-1">. En caso contrario, se va a mostrar un fallo. 

Ten en cuenta que se considera que el último bit significativo está en la primera columna. Así, la columna 1 se va a corresponder con <@mth="2"><@sup="0">, la columna 2 con <@mth="2"><@sup="1">, y así sucesivamente. Por ejemplo, la expresión 

<code>          
     matrix B = dec2bin(5)
</code>

produce un vector fila lleno de ceros, excepto para las posiciones 1 y 3. 

La función <@ref="bin2dec"> realiza la transformación inversa. 

# defarray data-utils
Resultado: 	mira más abajo 
Argumento: 	... (mira más abajo)

Permite definir <@itl="detalladamente"> una variable de tipo “array”, proporcionando uno o más elementos. Al utilizar esta función debes especificar el tipo de 'array' (en forma plural): <@lit="strings">, <@lit="matrices">, <@lit="bundles"> o <@lit="lists">. Cada uno de los argumentos debe ser un objeto del mismo tipo que el tipo especificado en la definición del 'array'. En caso de completarse con éxito, la función devuelve como resultado un 'array' con <@mth="n"> elementos, donde <@mth="n"> es igual al número de argumentos. 

<code>          
     strings S = defarray("foo", "bar", "baz")
     matrices M = defarray(I(3), X'X, A*B, P[1:])
</code>

Consulta también <@ref="array">. 

# defbundle data-utils
Resultado: 	bundle 
Argumento: 	... (mira más abajo)

Te permite la carga inicial de una variable de tipo 'bundle' <@itl="extensamente">, proporcionando cero o más parejas con el formato <@var="clave">, <@var="elemento">. Si contamos los argumentos desde 1, cada argumento numerado impar debe evaluar una cadena de texto (clave), y cada argumento numerado par debe evaluar un objeto de un tipo que pueda incluirse en un 'bundle'. 

Un par de ejemplos sencillos: 

<code>          
     bundle b1 = defbundle("s", "Sample string", "m", I(3))
     bundle b2 = defbundle("yn", normal(), "x", 5)
</code>

El primer ejemplo genera un 'bundle' cuyos elementos son una cadena de texto y una matriz; el segundo, un 'bundle' con un elemento que es una serie y otro que es escalar. Ten en cuenta que no puedes especificar un tipo para cada argumento cuando utilizas esta función, entonces debes aceptar el tipo “natural” de argumento en cuestión. Si quieres añadir una serie con un valor constante de 5 a un 'bundle' llamado <@lit="b1"> sería necesario hacer algo como lo siguiente (después de definir <@lit="b1">): 

<code>          
     series b1.s5 = 5
</code>

Si no indicas ningún argumento para esta función, eso equivale a generar un 'bundle' vacío (o a vaciar un 'bundle' existente de su contenido), como podrías hacer mediante 

<code>          
     bundle b = null
</code>

<subhead>Variantes de sintaxis</subhead> 

Dispones de dos formas alternativas de sintaxis para definir 'bundles'. En ambos casos, la palabra clave <@lit="defbundle"> se sustituye por un carácter de subrayado. En la primera variante, los elementos separados por comas tienen la forma <@lit="clave=valor">, donde la clave se entiende que debe ser una cadena de texto literal y no requiere que la pongas entre comillas. Este es un ejemplo: 

<code>          
     bundle b = _(x=5, strval="Alguna cadena", m=I(3))
</code>

Esta forma resulta particularmente conveniente para producir un 'bundle' anónimo sobre la marcha como argumento de una función, como en 

<code>          
     b = regls(ys, LX, _(lfrac=0.35, stdize=0))
</code>

donde la función <@lit="regls"> tiene un argumento opcional de tipo 'bundle' que contiene varios parámetros. 

La segunda variante está pensada para el caso en que quieras empaquetar varios objetos ya existentes en un 'bundle': simplemente indica sus nombres sin comillas: 

<code>          
     bundle b = _(x, y, z)
</code>

En este caso, el objeto <@lit="x"> se copia en un 'bundle' con la clave “<@lit="x">”. De forma similar se hace tanto para <@lit="y"> como para <@lit="z">. 

Estas formas alternativas implican teclear menos que en la versión íntegra de <@lit="defbundle()">, y probablemente muchas veces son más convenientes, pero ten en cuenta que son menos flexibles. Solo en la versión íntegra puedes manejar las claves indicándolas como variables de cadena de texto en lugar de cadenas literales. 

# deflist data-utils
Resultado: 	lista 
Argumento: 	... (mira más abajo)

Genera una lista (de series ya definidas) dados uno o más argumentos apropiados. Cada argumento debe ser, una serie ya definida (indicada por su nombre o el número entero ID), una lista ya definida, o una expresión que se corresponda con una lista (incluyendo un vector que pueda interpretarse como un conjunto de números ID de series). 

Un aspecto a tener en cuenta es que esta función simplemente encadena sus argumentos para producir la lista que devuelve. Cuando se pretende que el valor que devuelva no contenga duplicados (que no se refiera a ninguna serie más de una vez), depende del solicitante asegurarse de que se satisfaga ese requerimiento. 

# deseas timeseries
Resultado: 	serie 
Argumentos:	<@var="x">  (serie)
		<@var="opciones">  (bundle, opcional)

La intención principal de esta función es producir una versión desestacionalizada de la serie <@var="x"> (mensual o trimestral) de entrada, utilizando para ello X-13ARIMA-SEATS; esto estará disponible únicamente si está instalado X-13ARIMA-SEATS. Si omites el 'bundle' necesario para el segundo argumento (opcional), el ajuste estacional se hace incluyendo toda las opciones de X-13ARIMA establecidas en sus valores por defecto (procedimiento completamente automático). Cuando indicas el 'bundle' de <@var="opciones">, se podría incluir cualquiera de las siguientes especificaciones para las opciones. 

<indent>
• <@lit="verbose">: ¿Qué presentar? 0 = nada (por defecto); 1 = confirmación de las opciones que están seleccionadas; 2 = confirmación de las opciones más el resultado de X-13ARIMA. 
</indent>

<indent>
• <@lit="seats">: 1 para utilizar el algoritmo SEATS en lugar del algoritmo predeterminado X11 para el ajuste estacional, o 0. 
</indent>

<indent>
• <@lit="airline">: 1 para utilizar la especificación “airline” (0,1,1)(0,1,1) de modelos ARIMA en lugar de la selección de modelos automática predeterminada, o 0. 
</indent>

<indent>
• <@lit="arima">: Puede utilizarse para imponer una especificación ARIMA escogida, en formato de un vector de 6 elementos que contenga números enteros pequeños y no negativos. Estos se indican con la simbología (p,d,q,P,D,Q) de la notación tradicional de las series de tiempo: los primeros tres términos representan los órdenes AR, de Integración y MA no estacionales; y los tres últimos indican las contrapartidas estacionales. Cuando se indican tanto la opción <@lit="airline"> como la <@lit="arima">, tiene prioridad la <@lit="arima">. 
</indent>

<indent>
• <@lit="outliers">: Permite la detección y corrección de valores atípicos (elecciones de 1 hasta 7), o 0 (predeterminado) para omitir esta característica. Los tres tipos de valores atípicos disponibles con sus códigos numéricos son: 1 = valor atípico aditivo (ao), 2 = paso de nivel (ls), 4 = cambio temporal (tc). Para combinar las opciones puedes añadir códigos, por ejemplo: 1 + 2 + 4 = 7 para activar las tres a un tiempo. Ten en cuenta que la elección 3 = 1 + 2 (ao con ls) es la predeterminada en X-13ARIMA-SEATS, y se selecciona mediante la casilla de valores atípicos en la ventana de diálogo de GRETL para el ajuste estacional por medio de X13. 
</indent>

<indent>
• <@lit="critical">: Un escalar positivo con el valor crítico para definir los valores atípicos, siendo automático el predeterminado que se hace en función del tamaño de la muestra. Relevante solo cuando indicas la opción <@lit="outliers">. 
</indent>

<indent>
• <@lit="logtrans">: Debería pasarse la serie de entrada a logaritmos? 0 = no, 1 = si, 2 = selección automática (por defecto). Ten en cuenta que no se recomienda que indiques una serie de entrada ya en logaritmos; si quieres que se utilice el logaritmo, indica el nivel “de base” pero especificando después <@lit="logtrans=1">. 
</indent>

<indent>
• <@lit="trading_days">: Deberían incluirse los días de operación? 0 = no, 1 = si, 2 = automático (por defecto). 
</indent>

<indent>
• <@lit="working_days">: Una versión más simple de <@lit="trading_days"> con una única distinción entre días de la semana y fines de semana, en vez de los efectos de los días particulares. 0 = no (por defecto), 1 = si, 2 = automático. Utiliza solo una de las dos opciones, <@lit="trading_days"> o <@lit="working_days">. 
</indent>

<indent>
• <@lit="easter">: 1 para permitir el efecto de la Pascua, como complemento a <@lit="trading_days"> o a <@lit="working_days">, o 0 (por defecto). 
</indent>

<indent>
• <@lit="output">: Una cadena de texto para escoger el tipo de serie del resultado: <@lit=""sa""> para desestacionalizado (por defecto), <@lit=""trend""> para la tendencia estimada, o <@lit=""irreg""> para la componente irregular. 
</indent>

<indent>
• <@lit="save_spc">: Indicador booleano, 0 por defecto; mira abajo. 
</indent>

<subhead>Resultados ampliados</subhead> 

En algunos casos podrías desear obtener los tres resultados disponibles del X-13ARIMA mediante una única llamada a <@lit="deseas">. Esto se admite del siguiente modo. Pasa el 'bundle' <@var="opciones"> en formato de puntero, e indica la cadena de texto <@lit=""all""> bajo la clave <@lit="output">. El valor directo que se devuelve entonces es la serie ajustada estacionalmente, pero cuando se completa con éxito <@var="opciones"> va a contener una matriz denominada <@lit="results"> con tres columnas: ajustada estacionalmente, tendencia e irregular. A continuación tienes un ejemplo (en el que se descarta el valor del resultado directo). 

<code>          
     bundle b = _(output="all")
     deseas(y, &b)
     series y_dseas = b.results[,1]
     series y_trend = b.results[,2]
     series y_irreg = b.results[,3]
</code>

<subhead>Guardando la especificación de X-13ARIMA</subhead> 

Puedes utilizar el indicador <@lit="save_spc"> para guardar el contenido del archivo de entrada X-13ARIMA que escribe GRETL. El 'bundle' con las opciones debe pasarse en formato de puntero, y la especificación (como cadena de texto) puede encontrarse bajo la clave <@lit="x13a_spc">. El siguiente código ilustra como se guarda esta en un archivo bajo el nombre <@lit="especif.spc"> en el directorio de trabajo del usuario. (Observa que la extensión <@lit=".spc"> es requerida por X-13ARIMA.) 

<code>          
     bundle b = _(save_spc=1)
     deseas(y, &b)
     outfile especif.spc
        print b.x13a_spc
     end outfile
</code>

# det linalg
Resultado: 	escalar 
Argumento: 	<@var="A">  (matriz cuadradax)

Devuelve un escalar con el valor del determinante de <@var="A">, calculado mediante la descomposición LU. Si lo que realmente quieres es el logaritmo natural del determinante, debes en cambio invocar <@ref="ldet">. Ver también <@ref="rcond">, <@ref="cnumber">. 

# diag matrix
Resultado: 	matriz 
Argumento: 	<@var="X">  (matriz)

Devuelve un vector columna con los valores de la diagonal principal de <@var="X">. Observa que si <@var="X"> es una matriz de orden <@itl="m">×<@itl="n">, el número de elementos del vector resultante es igual a min(<@mth="m">, <@mth="n">). Ver también <@ref="tr">. 

# diagcat matrix
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz)
		<@var="B">  (matriz)

Devuelve una matriz con la suma directa de <@var="A"> y <@var="B">; es decir, una matriz que abarca a <@var="A"> en la esquina superior izquierda y a <@var="B"> en la esquina inferior derecha. Si <@var="A"> y <@var="B"> son ambas cuadradas, la matriz resultante es diagonal por bloques. 

# diff transforms
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="y">  (serie, matriz o lista)

Devuelve un resultado (del mismo tipo que el argumento) con las primeras diferencias. Si <@var="y"> es una serie o una lista de series, los valores iniciales son <@lit="NA">; si <@var="y"> es una matriz, la diferenciación se hace por columnas y los valores iniciales son 0. 

Cuando esta función devuelve una lista, cada una de las variables de la misma se nombra de modo automático conforme al patrón <@lit="d_"><@var="varname">, donde <@var="varname"> se substituye por el nombre de la serie original. De ser necesario, el nombre se va a truncar; e incluso se ajustará en caso de que el conjunto de nombres que se construye así, dé lugar a que alguno de ellos no sea único. 

Ver también <@ref="cum">, <@ref="ldiff">, <@ref="sdiff">. 

# digamma math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el valor de la función digamma (o Psi) de <@var="x">, es decir, la derivada del logaritmo de la función Gamma. 

Ver también <@ref="lngamma">, <@ref="trigamma">. 

# distance math
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="metrica">  (cadena, opcional)
		<@var="Y">  (matriz, opcional)

Calcula las distancias entre puntos sobre una métrica que puede ser <@lit="euclidean"> (a predeterminada), <@lit="manhattan">, <@lit="hamming">, <@lit="chebyshev">, <@lit="cosine"> o <@lit="mahalanobis">. Puedes indicar la cadena de texto que identifica la métrica, truncándola de forma que no resulte ambigua. Las otras métricas adicionales, de correlación y la euclídea tipificada se admiten mediante transformaciones simples de las anteriores (mira más abajo). 

Cada fila de la matriz <@var="X"> (que es <@itl="m">×<@itl="n">) se trata como un punto de un espacio <@mth="n">-dimensional; en un contexto econométrico, esto probablemente represente una única observación que comprenda los valores de <@mth="n"> variables. 

<subhead>Casos típicos</subhead> 

Esta sección se aplica a todas las métricas, excepto a la distancia de Mahalanobis, para la que la sintaxis es levemente diferente (mira más abajo). 

Si no indicas <@var="Y">, el valor que se devuelve es un vector columna de longitud <@mth="m">(<@mth="m"> – 1)/2 que comprende el subconjunto no redundante de todas las distancias por parejas entre los <@mth="m"> puntos (las filas de <@var="X">). Entonces, dado un vector de este tipo denominado <@lit="d">, puedes generar la matriz simétrica completa con las distancias entre los puntos(con ceros en la diagonal principal, naturalmente) por medio de 

<code>          
     D = unvech(d, 0)
</code>

puesto que <@lit="d"> es similar al vector columna resultante al usar la función vech sobre <@lit="D">, sin los elementos de la diagonal principal. El segundo argumento (opcional) de <@ref="unvech"> indica que debe rellenarse la diagonal con ceros. 

Si indicas <@var="Y">, debe ser una matriz <@itl="p">×<@itl="n"> en la que cada una de sus filas se trate otra vez como un punto en el espacio <@mth="n">-dimensional. En este caso, e valor que se devuelve es una matriz <@itl="m">×<@itl="p"> cuyo elemento <@mth="i,j"> contiene la distancia que hay entre la fila <@mth="i"> de la matriz <@var="X"> y la fila <@mth="j"> de la matriz <@var="Y">. 

Para obtener las distancias desde un punto de referencia dado (por ejemplo, el centroide) hasta cada uno de los <@mth="n"> puntos de datos, indica <@var="Y"> como una única fila. 

<subhead>Definiciones de las métricas admitidas</subhead> 

<indent>
• <@lit="euclidean">: la raíz cuadrada de la suma de las desviaciones elevadas al cuadrado, en cada una de las dimensiones. 
</indent>

<indent>
• <@lit="manhattan">: a suma de los valores absolutos de las desviaciones, en cada una de las dimensiones. 
</indent>

<indent>
• <@lit="hamming">: la proporción de las dimensiones en las que las desviaciones no son nulas (acotada entonces por 0 y 1). 
</indent>

<indent>
• <@lit="chebyshev">: el mayor de los valores absolutos de las desviaciones en cualquiera de las dimensiones. 
</indent>

<indent>
• <@lit="cosine">: 1 menos el coseno del ángulo que se forma entre los “puntos”, considerados como vectores. 
</indent>

<subhead>Distancia de Mahalanobis</subhead> 

Las distancias de Mahalanobis se definen como distancias euclídeas, entre los puntos considerados (filas de la matriz <@var="X">) y un centroide dado, escaladas mediante la inversa de una matriz de covarianzas. En el caso más simple, el centroide está constituido por las medias muestrales de las variables (columnas de <@var="X">) y la matriz de covarianzas está formada por las covarianzas entre ellas en la muestra. 

Esto se puede obtener indicando como segundo argumento la cadena de texto “mahalanobis” o cualquier abreviatura no ambigua, como en 

<code>          
     dmahal = distance(X, "mahal")
</code>

En este caso, el tercer argumento <@var="Y"> no se admite, y el valor que se devuelve es un vector columna de longitud <@mth="m"> con las distancias de Mahalanobis desde el centroide de <@var="X"> (es decir, su media muestral). En la práctica, la matriz del resultado en este caso es la misma que obtienes al ejecutar la instrucción <@xrf="mahal"> sobre una lista de series que se correspondan con las columnas de la matriz <@var="X">. 

Para obtener las distancias de Mahalanobis usando un centroide distinto, <@lit="mu">, y/o la inversa de la matriz de covarianzas, <@lit="ICV">, puedes utilizar la siguiente sintaxis: 

<code>          
     dmahal = distance(X*cholesky(ICV), "euc", mu)
</code>

<subhead>Otras métricas</subhead> 

Puedes obtener las distancias euclídeas tipificadas y de correlaciones del modo siguiente: 

<code>          
     # Euclídea tipificada
     dseu = distance(stdize(X), "eu")
     # Correlación (basada en el coseno)
     dcor = distance(stdize(X', -1)', "cos")
</code>

# dnorm probdist
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del mismo tipo que el argumento) con el valor de la densidad de la distribución de probabilidad Normal estándar en <@var="x">. Para obtener la densidad de una distribución Normal no estándar en <@mth="x">, transforma tipificando <@mth="x"> en <@mth="z">, aplícale a esto la función <@lit="dnorm"> y multiplica el resultado por el Jacobiano de la transformación <@mth="z">, es decir , 1/σ, conforme se ilustra a continuación: 

<code>          
     mu = 100
     sigma = 5
     x = 109
     fx = (1/sigma) * dnorm((x-mu)/sigma)
</code>

Ver también <@ref="cnorm">, <@ref="qnorm">. 

# dropcoll transforms
Resultado: 	lista 
Argumentos:	<@var="X">  (lista)
		<@var="epsilon">  (escalar, opcional)

Devuelve una lista con los mismos elementos que <@var="X">, pero excluyendo las series que causan multicolinealidad perfecta. En consecuencia, si todas las series que hay en <@var="X"> son linealmente independientes, la lista que resulta es simplemente una copia de <@var="X">. 

El algoritmo usa la descomposición QR (transformación de Householder), por lo que está sujeto a error de precisión finita. Con el objeto de calibrar la sensibilidad del algoritmo, puedes especificar un segundo parámetro (opcional) <@var="epsilon"> para hacer el contraste de multicolinealidad más o menos estricto, según desees. Por defecto, el valor para <@var="epsilon"> es 1.0e-8, pero ajustando <@var="epsilon"> dándole valores mayores, se eleva la probabilidad de que se descarte una de las series. 

El ejemplo 

<code>          
     nulldata 20
     set seed 9876
     series foo = normal()
     series bar = normal()
     series foobar = foo + bar
     list X = foo bar foobar
     list Y = dropcoll(X)
     list print X
     list print Y
     # Indica un épsilon con un valor muy pequeño
     list Y = dropcoll(X, 1.0e-30)
     list print Y
</code>

produce 

<code>          
     ? list print X
     foo bar foobar
     ? list print Y
     foo bar
     ? list Y = dropcoll(X, 1.0e-30)
     Se ha reemplazado la lista Y
     ? list print Y
     foo bar foobar
</code>

# dsort matrix
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (serie, vector o array de cadenas)

Ordena <@var="x"> de forma decreciente, descartando observaciones con valores ausentes cuando <@var="x"> es una serie. Ver también <@ref="sort">, <@ref="values">. 

# dummify transforms
Resultado: 	lista 
Argumentos:	<@var="x">  (serie)
		<@var="omitval">  (escalar, opcional)

El argumento <@var="x"> debe ser una serie discreta. Esta función devuelve una lista con un conjunto de variables ficticias, una para cada uno de los diferentes valores de la serie. Por defecto, el menor valor se trata como la categoría omitida y no va a representarse explícitamente. 

El segundo argumento (opcional) indica el valor de <@var="x"> que debe ser tratado como categoría omitida. Cuando se indica un único argumento, el efecto es equivalente al de utilizar la instrucción: <@lit="dummify(x, min(x))">. Para producir un conjunto completo de variables ficticias, es decir, sin omitir ninguna categoría, puedes usar <@lit="dummify(x, NA)">. 

Las variables que se generan se nombran automáticamente de acuerdo con el siguiente patrón: <@lit="D"><@var="nombrevariable"><@lit="_"><@var="i"> donde <@var="nombrevariable"> indica el nombre de la serie original e <@var="i"> es un índice entero positivo. De ser necesario, la porción original del nombre se va a truncar, e incluso se ajustará en caso de que el conjunto de nombres que se construye así, dé lugar a que alguno de ellos no sea único. 

# easterday calendar
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="y">  (escalar, serie o matriz)

Poniendo un año como argumento <@var="y">, devuelve un resultado del mismo tipo que este, con la fecha del domingo de Pascua de ese año en el calendario gregoriano, con el formato <@mth="mes + día/100">. Por ejemplo, en 2014 la fecha del domingo de Pascua fue el 20 de abril, lo que se representa con esta convención como 4,2. (Observa que el día 2 de abril se devuelve como 4,02.) El siguiente código muestra como pueden extraerse el día y el mes del valor que devuelve esta función: 

<code>          
     scalar e = easterday(2014)
     scalar m = floor(e)
     scalar d = round(100*(e-m))
</code>

# ecdf stats
Resultado: 	matriz 
Argumento: 	<@var="y">  (serie o vector)

Calcula la función de distribución acumulativa (CDF) empírica de <@var="y">. El resultado se devuelve en formato de matriz con dos columnas: la primera contiene los valores únicos ordenados de <@var="y">; y la segunda contiene la frecuencia relativa acumulada, es decir el número de casos en los que su valor es menor o igual al valor correspondiente de la primera columna, dividido por el número total de observaciones. 

# eigen linalg
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz cuadradax)
		<@var="&V">  (referencia a matriz, o <@lit="null">)
		<@var="&W">  (referencia a matriz, o <@lit="null">)

Calcula los autovalores (y opcionalmente los autovectores derechos y/o izquierdos) de la matriz <@var="A"> de dimensión <@itl="n">×<@itl="n">, que puede ser real o compleja. Los autovalores se devuelven en un vector columna complejo. Para obtener la norma de los autovalores puedes utilizar la función <@ref="abs">, que admite argumentos complejos. 

Si quieres recuperar los autovectores derechos (como en el caso de una matriz compleja de dimensión <@itl="n">×<@itl="n">), indica el nombre de una matriz ya existente, precedido por <@lit="&"> para indicar la “dirección” de la matriz en cuestión, como segundo argumento. De otro modo, puedes omitir este argumento. 

Para recuperar los autovectores izquierdos (otra vez, como en una matriz compleja), indica la dirección de una matriz como tercer argumento. Ten en cuenta que, si quieres los autovectores izquierdos pero no los derechos, debes usar la palabra clave <@lit="null"> como marcador para el segundo argumento. 

Ver también <@ref="eigensym">, <@ref="eigsolve">, <@ref="svd">. 

# eigengen linalg
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz cuadradax)
		<@var="&U">  (referencia a matriz, o <@lit="null">)

<@itl="Esta es una función heredada, anterior al soporte original de GRETL para matrices complejas. No debes usarla en los guiones nuevos que escribas en lenguaje HANSL. Utiliza "> <@ref="eigen"> <@itl=" en su lugar."> 

Calcula los autovalores y, opcionalmente, los autovectores de la matriz <@var="A"> de orden <@itl="n">×<@itl="n">. Cuando todos los autovalores son reales, se devuelve una matriz <@itl="n">×1. En caso contrario, el resultado es una matriz <@itl="n">×2, con una primera columna que contiene los elementos reales, y una segunda columna con los elementos imaginarios. No se garantiza que los autovalores se vayan a clasificar en ningún orden en particular. 

Hay dos opciones para el segundo argumento: que se trate del nombre de una matriz ya existente precedida por <@lit="&"> (para indicar la “dirección” de la matriz en cuestión), en cuyo caso en esta matriz se guarda un resultado auxiliar; o que se trate de la palabra clave <@lit="null">, en cuyo caso no se produce el resultado auxiliar. 

Cuando el segundo argumento no es nulo, se va a sobrescribir la matriz especificada con el resultado auxiliar (y no es necesario que la matriz existente tenga la dimensión adecuada para recibir el resultado). El resultado en la matriz <@var="U"> se organiza del siguiente modo: 

<indent>
• Si el <@mth="i">-ésimo autovalor es real, la <@mth="i">-ésima columna de <@mth="U"> va a contener el autovector correspondiente; 
</indent>

<indent>
• Si el <@mth="i">-ésimo autovalor es complejo, la <@mth="i">-ésima columna de <@mth="U"> va a contener la parte real del autovector correspondiente, y la siguiente columna la parte imaginaria. El autovector del autovalor conjugado es el conjugado del autovector. 
</indent>

En otras palabras, los autovectores se guardan en el mismo orden que los autovalores; ahora bien, los autovectores reales ocupan una columna, mientras que los autovectores complejos ocupan dos (y la parte real se guarda primero). Aún así, el número total de columnas es <@mth="n">, pues el autovector conjugado se ignora. 

Ver también <@ref="eigensym">, <@ref="eigsolve">, <@ref="qrdecomp">, <@ref="svd">. 

# eigensym linalg
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz simétrica)
		<@var="&U">  (referencia a matriz, o <@lit="null">)

Funciona del mismo modo que la función <@ref="eigen">, excepto que el argumento <@var="A"> debe ser simétrico (por lo que, en este caso, se pueden acortar los cálculos), y los autovalores se devuelven en orden ascendente. Si deseas obtener los autovalores en orden descendente (y tener los autovectores reordenados en consecuencia), puedes hacer lo siguiente: 

<code>          
     matrix U
     e = eigensym(A, &U)
     Tmp = msortby((-e' | U)',1)'
     e = -Tmp[1,]'
     U = Tmp[2:,]
     # Ahora los autovalores de mayor a menor
     print e U
</code>

Aviso: Si lo que te interesa es la descomposición espectral de una matriz de la forma <@mth="X'X">, es preferible calcular el argumento mediante el operador <@lit="X'X">, en lugar de utilizar la sintaxis más general <@lit="X'*X">. La primera expresión utiliza un algoritmo especializado que ofrece mayor eficiencia desde el punto de vista del cómputo, y garantiza que el resultado va a ser exactamente simétrico. 

# eigsolve linalg
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz simétrica)
		<@var="B">  (matriz simétrica)
		<@var="&U">  (referencia a matriz, o <@lit="null">)

Resuelve el problema del autovalor generalizado de tipo |<@mth="A"> – λ<@mth="B">| = 0, donde ambas <@mth="A"> y <@mth="B"> son matrices simétricas, y <@mth="B"> se define positiva. Se devuelve directamente una matriz con los autovalores ordenados de forma ascendente. Cuando utilizas el tercer argumento (opcional), este debe ser el nombre de una matriz ya existente, precedida por <@lit="&">. En este caso, los autovectores generalizados se escriben en esta matriz que se indica. 

# epochday calendar
Resultado: 	escalar o serie 
Argumentos:	<@var="año">  (escalar o serie)
		<@var="mes">  (escalar o serie)
		<@var="día">  (escalar o serie)

Devuelve un escalar o una serie, con el número del día especificado por el año, mes y día, en ese orden, en la época actual. El número del día es igual a 1 para el día 1 de enero del año 1 después de Cristo, en el calendario Gregoriano proléptico, y a 733786 para la fecha 01-01-2010. Si alguno de los argumentos es una serie, el valor que se devuelve también tendrá la forma de una serie; en caso contrario, se devuelve un escalar. 

Por defecto, los valores de los argumentos <@var="año">, <@var="mes"> y <@var="día"> se presupone que se están indicando de acuerdo con calendario Gregoriano, pero si el año tiene un valor negativo, la interpretación cambia a la del calendario Juliano. 

También se admite una petición alternativa: si indicas un único argumento, se va a considerar que es una fecha (o una serie de fechas) en formato numérico ISO 8601 “básico”, <@lit="YYYYMMDD">. De esta forma, las dos siguientes peticiones producen el mismo resultado, concretamente 700115. 

<code>          
     eval epochday(1917, 11, 7)
     eval epochday(19171107)
</code>

Para la inversa de esta función consulta <@ref="isodate">, y también <@ref="juldate"> (para el calendario Juliano). Para ver otros medios de conversión de fechas en días de época, consulta <@ref="strpday">. 

# errmsg programming
Resultado: 	cadena 
Argumento: 	<@var="errno">  (entero)

Devuelve una cadena de texto con el mensaje de fallo de GRETL asociada a <@var="errno">, que debe ser un número entero. Consulta también <@ref="$error">. 

# errorif programming
Resultado: 	escalar 
Argumentos:	<@var="condicion">  (booleano)
		<@var="mensaje">  (cadena)

Esta función solo se aplica en el contexto de una función definida por el usuario, o dentro de un bloque <@xrf="mpi">. Si la <@var="condicion"> se valora como no nula, ello implica que la ejecución de la función vigente finalice con la presentación de un mensaje condicionado a que se produzca un fallo; entonces el argumento <@var="mensaje"> se presentará como parte del mensaje de fallo que se muestra al llamar a la función en cuestión. 

El valor que se devuelve con esta función (1) es simplemente nominal. 

# exists data-utils
Resultado: 	entero 
Argumento: 	<@var="nombre">  (cadena)

Devuelve un escalar no nulo si <@var="nombre">(que debe ser un identificador válido de GRETL) denomina un objeto con una definición vigente, sea un escalar, una serie, una matriz, una lista, una cadena de texto, un 'bundle' o un 'array'. En otro caso devuelve 0. 

El pretendido uso de esto es para el caso en que una función definida por el usuario tenga un parámetro opcional con un <@lit="nulo"> por defecto. El redactor de la función puede utilizar <@lit="exists()">, indicando el nombre del parámetro, para comprobar si el invocador proporciona un argumento. Por favor, pero ten en cuenta que las listas son una excepción en este aspecto: si un parámetro de una lista tiene un <@lit="nulo"> por defecto y el invocador no proporciona un argumento, la función coge una lista vacía en lugar de no tomar ninguna lista; por lo tanto, la función <@lit="exists"> siempre va a devolver no nulo. Para comprobar el vacío en el argumento de una lista, utiliza <@ref="nelem">. 

Para comprobaciones relacionadas, consulta <@ref="typeof"> e <@ref="inbundle">. 

# exp math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con <@mth="e"><@sup="x">. Ten en cuenta que, con argumento matricial, se aplica elemento a elemento. Para la función exponencial matricial consulta <@ref="mexp">. 

# fcstats stats
Resultado: 	matriz 
Argumentos:	<@var="y">  (serie o vector)
		<@var="f">  (serie, lista o matriz)
		<@var="U2">  (booleano, opcional)

Genera una matriz que contiene varios estadísticos que sirven para evaluar <@var="f"> como predicción de los datos observados <@var="y">. 

Cuando <@var="f"> es una serie o un vector, el resultado es un vector columna. Cuando <@var="f"> es una lista con <@mth="k"> elementos o una matriz de dimensión <@itl="T">×<@itl="k">, el resultado tiene <@mth="k"> columnas en las que cada una contiene los estadísticos del término correspondiente (serie de la lista o columna de la matriz) como predicción de <@var="y">. 

En todo caso, la dimensión “vertical” de los datos introducidos (la longitud de la muestra vigente para una serie o lista, y el número de filas para una matriz) debe coincidir entre los dos argumentos. 

Las filas de la matriz que se devuelven son como se indica a continuación: 

<code>          
     1  Error medio
     2  Raíz del Error cuadrático medio
     3  Error absoluto medio
     4  Porcentaje de error medio
     5  Porcentaje de error absoluto medio
     6  U de Theil (U1 o U2)
     7  Proporción de sesgo, UM
     8  Proporción de regresión, UR
     9  Proporción de perturbación, UD
</code>

La variante del U de Theil que se presenta por defecto depende de la naturaleza de los datos: cuando se sabe que son series de tiempo, se muestra el U2; en caso contrario, se produce el U1. Pero puedes forzar esta elección por medio del último argumento opcional: indica un valor no nulo para forzar el U2, o un valor de cero para forzar el U1. 

Para obtener más detalles sobre el cálculo de esos estadísticos y de la interpretación de los valores de <@mth="U">, consulta <@pdf="El manual de gretl#chap:forecast"> (Capítulo 35). 

# fdjac numerical
Resultado: 	matriz 
Argumentos:	<@var="b">  (vector columna)
		<@var="llamaf">  (llamada a función)
		<@var="h">  (escalar, opcional)

Permite calcular una aproximación numérica al Jacobiano asociado al <@mth="n">-vector <@var="b">, así como la función de transformación especificada por el argumento <@var="llamaf">. Al apelar a esta función debes utilizar <@var="b"> como primer argumento de la misma (bien directamente o en forma de puntero), seguido de cualquier argumento adicional que pueda necesitarse; y como resultado se debiera producir una matriz <@itl="m">×1. Cuando se ejecuta con éxito, <@lit="fdjac"> va a devolver una matriz <@itl="m">×<@itl="n"> que contiene el Jacobiano. 

Puedes utilizar el tercer argumento (opcional) para determinar el tamaño de la medida <@mth="h"> que se usa en el mecanismo de aproximación (mira más abajo). Cuando omites este argumento, el tamaño de la medida se determina automáticamente. 

Aquí tienes un ejemplo de su uso: 

<code>          
     matrix J = fdjac(theta, mifunc(&theta, X))
</code>

La función puede utilizar tres métodos distintos: diferencia simple hacia adelante, diferencia bilateral o extrapolación de 4-nodos de Richardson. Estas se corresponden respectivamente con: 

<@mth="J"><@sub="0"> = <@mth="(f(x+h) - f(x))/h"> 

<@mth="J"><@sub="1"> = <@mth="(f(x+h) - f(x-h))/2h"> 

<@mth="J"><@sub="2"> = <@mth="[8(f(x+h) - f(x-h)) - (f(x+2h) - f(x-2h))] /12h"> 

Estas tres alternativas generalmente proporcionan una conciliación entre precisión y velocidad. Puedes elegir entre los distintos métodos mediante la instrucción <@xrf="set">: especifica el valor 0, 1 o 2 para la variable <@lit="fdjac_quality">. El valor por defecto es 0. 

Para más detalles y ejemplos, consulta <@pdf="El manual de gretl#chap:numerical"> (Capítulo 37). 

Ver también <@ref="BFGSmax">, <@ref="numhess">, <@xrf="set">. 

# feval programming
Resultado: 	mira más abajo 
Argumentos:	<@var="nombrefuncion">  (cadena)
		... (mira más abajo)

Principalmente útil para los creadores de funciones. El primer argumento debe ser el nombre de una función; los restantes argumentos se pasarán a la función especificada. Esto permite tratar la propia función identificada mediante <@var="nombrefuncion"> como una variable en si misma. El valor que se devuelve es cualquier cosa que produzca la función indicada, dados los argumentos especificados. 

El ejemplo de abajo, ilustra algunos de sus posibles usos. 

<code>          
     function scalar utilidad (scalar c, scalar sigma)
         return (c^(1-sigma)-1)/(1-sigma)
     end function

     strings S = defarray("log", "utilidad")

     # Llamada a una función integrada de 1 argumento
     x = feval(S[1], 2.5)
     # Llamada a una función definida por el usuario
     x = feval(S[2], 5, 0.5)
     # Llamada a una función integrada de 2 argumentos
     func = "zeros"
     m = feval(func, 5-2, sqrt(4))
     print m
     # Llamada a una función integrada de 3 argumentos
     x = feval("monthlen", 12, 1980, 5)
</code>

Existe una frágil analogía entre la función <@lit="feval"> y <@ref="genseries">: ambas funciones convierten en variable un elemento sintáctico que habitualmente se fija al tiempo en el que se redacta un guion. 

Ver también <@ref="fevalb">. 

# fevalb programming
Resultado: 	mira más abajo 
Argumentos:	<@var="nombrefunc">  (cadena)
		<@var="b">  (bundle)

Esta es una variante de la función <@ref="feval"> que afronta una situación en la que se pueden encontrar los creadores de funciones, cuando no se sabe de antemano el número y los tipos de argumentos que se van a pasar a la función indicada. En lugar de pasar los argumentos individualmente, se pasan como miembros de un argumento <@var="b"> de tipo 'bundle'. 

Dado que el orden de los miembros de un 'bundle' de GRETL es indeterminado, se requiere algún mecanismo para asegurarnos de que estos se pasan a la función en cuestión, en el orden correcto. Esto se asegura automáticamente si el orden lexicográfico de las claves que hay en el 'bundle', indica también el orden de los argumentos. Por ejemplo, las claves podrían ser <@lit="arg1">, <@lit="arg2">, etcétera (o <@lit="arg01">, <@lit="arg02">, etcétera en el improbable caso de que la función tuviese más de nueve argumentos). Como alternativa, el 'bundle' puede contener un 'array' cadenas de texto bajo la clave reservada <@lit="arglist">. Este 'array' debe contener exactamente las claves del 'bundle' <@var="b"> (excepto el propio <@lit="arglist">), en el orden deseado. 

Los ejemplos de abajo ilustran ambas propuestas, aplicadas a la función <@ref="monthlen">. 

<code>          
     # Utilizando el orden lexicográfico
     bundle b = _(arg1=12, arg2=1980, arg3=5)
     n = feval("monthlen", b)

     # Utilizando arglist
     bundle b = _(month=12, year=1980, wkdays=5)
     b.arglist = defarray("month", "year", "wkdays")
     n = feval("monthlen", b)
</code>

Ver también <@ref="feval">. 

# fevd timeseries
Resultado: 	matriz 
Argumentos:	<@var="efecto">  (entero)
		<@var="motivo">  (entero)
		<@var="sys">  (bundle, opcional)

Esta función proporciona una alternativa más flexible que el accesor <@ref="$fevd"> para obtener una matriz de descomposición de la varianza del error de predicción (FEVD), después de estimar un VAR o un VECM. Si el argumento final (opcional), solo está disponible cuando el último modelo estimado fue un VAR o un VECM. Como alternativa, puedes guardar en un 'bundle' la información sobre estos tipos de sistemas, mediante el accesor <@ref="$system">, y posteriormente pasarle la función <@lit="fevd">. 

Los argumentos de la función, <@var="efecto"> e <@var="motivo">, tienen la forma de índices enteros positivos de las variables endógenas del sistema, tomando el 0 para representar “todas”. El siguiente fragmento de código, ilustra su uso. En el primer ejemplo, la matriz <@lit="fe1"> contiene las partes de la FEVD para <@lit="y1"> debidas a cada parte de <@lit="y1">, <@lit="y2"> e <@lit="y3"> (por lo tanto, las filas suman 1 en total). En el segundo, <@lit="fe2"> contiene la contribución de <@lit="y2"> a la varianza del error de predicción de las tres variables (entonces, las filas no suman 1 en total). En el tercer caso, lo que se devuelve es un vector columna que muestra la “parte propia” de la FEVD de <@lit="y1">. 

<code>          
     var 4 y1 y2 y3
     bundle vb = $system
     matrix fe1 = fevd(1, 0, vb)
     matrix fe2 = fevd(0, 2, vb)
     matrix fe3 = fevd(1, 1, vb)
</code>

El número de períodos (filas) sobre los que se traza la descomposición, se determina automáticamente en base a la frecuencia de los datos, pero puedes ignorar esto mediante el argumento <@lit="horizon"> de la instrucción <@xrf="set">, como en <@lit="set horizon 10">. 

Ver también <@ref="irf">. 

# fft linalg
Resultado: 	matriz 
Argumento: 	<@var="X">  (matriz)

Devuelve una matriz con el resultado de la transformación discreta de Fourier. La matriz <@var="X"> del argumento puede ser real o compleja. El resultado es una matriz compleja que tiene la misma dimensión que <@var="X">. 

Si fuese necesario calcular la transformación de Fourier sobre varios vectores con el mismo número de elementos, es más eficiente agruparlos en una matriz, en lugar de ejecutar <@lit="fft"> para cada vector por separado. Ver también <@ref="ffti">. 

# ffti linalg
Resultado: 	matriz 
Argumento: 	<@var="X">  (matriz)

Devuelve una matriz con <@mth="n"> columnas, con el resultado de la transformación inversa de Fourier discreta. Se asume que la matriz <@var="X"> consta de <@mth="n"> vectores columna complejos. 

Cuando necesites aplicar la transformación inversa de Fourier sobre varios vectores con el mismo número de elementos, resulta más eficiente agrupar los vectores en una matriz que ejecutar <@lit="ffti"> para cada uno por separado. Ver también <@ref="fft">. 

# filter timeseries
Resultado: 	mira más abajo 
Argumentos:	<@var="x">  (serie o matriz)
		<@var="a">  (escalar o vector, opcional)
		<@var="b">  (escalar o vector, opcional)
		<@var="y0">  (escalar, opcional)
		<@var="x0">  (escalar o vector, opcional)

Devuelve el resultado de aplicar un filtro parecido a un ARMA, al argumento <@var="x">. La transformación puede escribirse como 

<@mth="y"><@sub="t"> = <@mth="a"><@sub="0"> <@mth="x"><@sub="t"> + <@mth="a"><@sub="1"> <@mth="x"><@sub="t-1"> + ... <@mth="a"><@sub="q"> <@mth="x"><@sub="t-q"> + <@mth="b"><@sub="1"> <@mth="y"><@sub="t-1"> + ... <@mth="b"><@sub="p"><@mth="y"><@sub="t-p"> 

Si el argumento <@var="x"> es una serie, el resultado que se devuelve también es una serie. En caso contrario, si <@var="x"> es una matriz con <@mth="T"> filas y <@mth="k"> columnas, lo que se devuelve es la matriz del mismo tamaño que resulta de aplicar el filtro columna por columna. 

Los argumentos <@var="a"> y <@var="b"> son opcionales. Pueden ser escalares, vectores o la palabra clave <@lit="null">. 

Cuando <@var="a"> es un escalar, se va a utilizar como <@mth="a"><@sub="0"> y eso implicará que <@mth="q=0">. Cuando es un vector con <@mth="q+1"> elementos, va a contener los coeficientes desde <@mth="a"><@sub="0"> hasta <@mth="a"><@sub="q">. Cuando <@var="a"> es <@lit="null"> o se omite, esto es equivalente a definir <@mth="a"><@sub="0"> <@mth="=1"> y <@mth="q=0">. 

Cuando <@var="b"> es un escalar, se va a utilizar como <@mth="b"><@sub="1"> e implicará que <@mth="p=1">. Cuando es un vector con <@mth="p"> elementos, va a contener los coeficientes desde <@mth="b"><@sub="1"> hasta <@mth="b"><@sub="p">. Cuando <@var="b"> es <@lit="null"> o se omite, esto es equivalente a definir <@mth="B(L)=1">. 

El argumento escalar opcional <@var="y0"> se utiliza para representar todos los valores de <@mth="y"> anteriores al inicio de la muestra (se usa solo cuando <@mth="p > 0">). Cuando se omite, se entiende que es igual a 0. Similarmente, puedes usar el argumento opcional <@var="x0"> para especificar uno o más valores de <@mth="x"> anteriores al inicio de la muestra (información solo relevante cuando <@mth="q > 0">). En caso contario, se asume que los valores de <@var="x"> anteriores al inicio de la muestra son 0. 

Ver también <@ref="bkfilt">, <@ref="bwfilt">, <@ref="fracdiff">, <@ref="hpfilt">, <@ref="movavg">, <@ref="varsimul">. 

Ejemplo: 

<code>          
     nulldata 5
     y = filter(index, 0.5, -0.9, 1)
     print index y --byobs
     x = seq(1,5)' ~ (1 | zeros(4,1))
     w = filter(x, 0.5, -0.9, 1)
     print x w
</code>

produce 

<code>          
          index            y

          1            1     -0,40000
          2            2      1,36000
          3            3      0,27600
          4            4      1,75160
          5            5      0,92356

          x (5 x 2)

          1   1
          2   0
          3   0
          4   0
          5   0

          w (5 x 2)

          -0,40000     -0,40000
           1,3600       0,36000
           0,27600     -0,32400
           1,7516       0,29160
           0,92356     -0,26244
</code>

# firstobs data-utils
Resultado: 	entero 
Argumentos:	<@var="y">  (serie)
		<@var="enmuestra">  (booleano, opcional)

Devuelve el número entero positivo que indexa la primera observación no ausente de la serie <@var="y">. Por defecto, se analiza todo el rango de la muestra, de forma que, si está activa alguna forma de submuestreo, el valor que se devuelve puede ser menor que el valor devuelto por el accesor <@ref="$t1">. Pero si indicas un valor no nulo en <@var="enmuestra">, solo se va a tener en cuenta el rango de la muestra vigente. Ver también <@ref="lastobs">. 

# fixname strings
Resultado: 	cadena 
Argumentos:	<@var="nombresobrio">  (cadena)
		<@var="underscore">  (booleano, opcional)

En principio, esta función está ideada para utilizarse en conjunto con la instrucción <@xrf="join">. Devuelve una cadena con el resultado de la conversión de <@var="nombresobrio"> en un identificador válido de GRETL; debe iniciarse con una letra, debe contener solo letras ASCII, dígitos y/o guion bajo, y no debe tener más de 31 caracteres. Las reglas que se utilizan en la conversión son: 

1. Quitar, del principio del nombre, cualquier carácter que no sea una letra. 

2. Hasta que se alcanza el límite de los 31 caracteres o hasta que se agota lo indicado en el argumento: transcribe los caracteres “legales”, substituye uno o varios espacios consecutivos por un guion bajo (excepto que el carácter anterior transcrito sea un guion bajo, pues entonces se elimina el espacio), y omite los otros tipos de caracteres “ilegales”. 

Si estás convencido de que la entrada no es demasiado larga (entonces susceptible de ser truncada), puedes querer substituir secuencias de uno o más caracteres ilícitos mediante un guion bajo (en lugar de solo eliminarlos) pues esto podría generar un identificador más legible. Para lograr este efecto, proporciona un valor no nulo para el segundo argumento (opcional). Pero esto no es recomendable en el contexto de la instrucción <@xrf="join">, puesto que el nombre “fijado” automáticamente no va a utilizar guiones bajos de esta manera. 

# flatten data-utils
Resultado: 	mira más abajo 
Argumentos:	<@var="A">  (array de matrices o cadenas)
		<@var="alt">  (entero o cadena, opcional)

“Allana” bien una formación de matrices en una única matriz, o bien una formación de cadenas de texto en una única cadena. 

<subhead>Matrices</subhead> 

En el caso de las matrices, el modo en que se juntan las matrices de <@var="A"> depende del argumento <@var="alt">, que debe tener un valor de 0 (horizontalmente), 1 (verticalmente) o 2 (“tipo vector”). La mejor manera de explicar la diferencia entre las tres alternativas es, por ejemplo, el código: 

<code>          
     X = {1,3,5; 2,4,6}
     A = defarray(X, X+6)
     U = flatten(A,0) # = A[1] ~ A[2]
     V = flatten(A,1) # = A[1] | A[2]
     W = flatten(A,2) # = vec(A[1]) ~ vec(A[2])
</code>

que produce las siguientes tres matrices: 

<code>          
     U (2 x 6)

     1    3    5    7    9   11 
     2    4    6    8   10   12 

     V (4 x 3)

     1    3    5 
     2    4    6 
     7    9   11 
     8   10   12 

     W (6 x 2)

     1    7 
     2    8 
     3    9 
     4   10 
     5   11 
     6   12
</code>

Se muestra un fallo cuando las matrices del 'array' no sean convenientes para la operación. Consulta la función <@ref="msplitby"> para la operación inversa. 

<subhead>Cadenas de texto</subhead> 

En caso de cadenas de texto, el resultado por defecto mantiene las cadenas de <@var="A">, ordenadas una en cada línea. Si indicas un valor numérico no nulo para <@var="alt">, las cadenas se separan mediante espacios en lugar de líneas nuevas, pero también se admite un uso alternativo de <@var="alt">: puedes indicar una cadena de texto específica para utilizar como separador. La función inversa para el caso de las cadenas de texto es <@ref="strsplit">. 

# floor math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="y">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el valor del mayor entero que es menor o igual que <@var="x">. Ten en cuenta que <@ref="int"> y <@lit="floor"> tienen efectos distintos con argumentos negativos:<@lit="int(-3.5)"> genera –3, mientras que <@lit="floor(-3.5)"> genera –4. 

# fracdiff timeseries
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="d">  (escalar)

Devuelve una serie con la diferencia fraccionaria de orden <@var="d"> de la serie <@var="y">. 

Observa que, en teoría, la diferenciación fraccionaria supone un filtro infinitamente largo. Los valores de <@mth="y"><@sub="t"> anteriores a la muestra, en la práctica se asume que son iguales a cero. 

Puedes utilizar valores negativos para <@var="d">, y en ese caso la función realiza la integración fraccionaria. 

# fzero numerical
Resultado: 	escalar 
Argumentos:	<@var="fcall">  (llamada a función)
		<@var="inicio">  (escalar o vector, opcional)
		<@var="toler">  (escalar, opcional)

Trata de encontrar una raíz simple de una función continua <@mth="f"> (normalmente no lineal) —es decir, un valor de la variable escalar <@mth="x"> que hace que <@mth="f">(<@mth="x">) = 0. El argumento <@var="fcall"> debe proporcionar una llamada a la función en cuestión. <@var="fcall"> puede incluir un número arbitrario de argumentos, pero el primero debe ser un escalar que represente el papel de <@mth="x">. Cuando se complete la función con éxito, se va a devolver el valor de la raíz. 

El método utilizado es el de <@bib="Ridders (1979);ridders79">. Esto requiere un intervalo inicial {<@mth="x"><@sub="0">, <@mth="x"><@sub="1">} tal que ambos valores <@mth="x"> pertenezcan al dominio de la función, y que los respectivos valores de la función sean de signo contrario. Probablemente, vas a obtener mejores resultados si eres capaz de proporcionar, mediante el segundo argumento, un vector bidimensional que contenga puntos finales adecuados para el intervalo. Si esto falla, puedes proporcionar un único valor escalar, y <@lit="fzero"> tratará de encontrar una pareja. Si omites el segundo argumento, el valor de <@mth="x"><@sub="0"> se inicia con un pequeño número positivo, y luego se va a buscar un valor adecuado para <@mth="x"><@sub="1">. 

Puedes usar el argumento <@var="toler"> (opcional) para ajustar la máxima diferencia absoluta que resulte aceptable entre <@mth="f">(<@mth="x">) y cero, siendo esta igual a 1.0e–14 por defecto. 

Por defecto, esta función opera silenciosamente, pero puedes mostrar la evolución del método iterativo ejecutando la instrucción “<@lit="set max_verbose on">” antes de llamar a <@lit="fzero">. 

A continuación se indican algunos ejemplos sencillos: 

<code>          
     # Aproximar 'pi' encontrando el valor que anula la
     # función sin() en el intervalo de 2.8 a 3.2
     x = fzero(sin(x), {2.8, 3.2})
     printf "\nx = %.12f vs pi = %.12f\n\n", x, $pi

     # Aproximar la 'constante Omega' comezando en x = 0.5
     function scalar f(scalar x)
         return log(x) + x
     end function
     x = fzero(f(x), 0.5)
     printf "x = %.12f f(x) = %.15f\n", x, f(x)
</code>

# gammafun math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el valor de la función Gamma de <@var="x">. 

Consulta también <@ref="bincoeff"> y <@ref="lngamma">. 

# genseries programming
Resultado: 	escalar 
Argumentos:	<@var="nombrevar">  (cadena)
		<@var="rhs">  (serie)

Le proporciona al guionista un procedimiento adecuado para generar series cuyos nombres no se conocen a priori; y/o de crear series y añadirlas a una lista por medio de una única operación (devuelve un escalar). 

El primer argumento proporciona el nombre de la serie que se va a crear (o modificar); y puede ser un texto literal, una cadena de texto o una expresión cuyo resultado sea una cadena de texto. El segundo argumento, <@var="rhs"> (“lado derecho” en inglés), define la serie original: esto puede ser el nombre de una serie existente o una expresión cuyo resultado sea una serie, en el modo en que aparece habitualmente al lado derecho del símbolo de igualdad cuando se definen series. 

El valor que devuelve esta función es un escalar con el número ID de la serie en el conjunto de datos, que es adecuado para incluir la serie en una lista (o –1 en caso de fallar la ejecución de la función). 

Por ejemplo, supón que quieres añadir <@mth="n"> series aleatorias con distribución de probabilidad Normal al conjunto de datos, y colocarlas en una lista. El siguiente código hace eso: 

<code>          
     nulldata 10
     list Normales = null
     scalar n = 3
     loop i = 1 .. n
         Normales += genseries(sprintf("norm%d", i), normal())
     endloop
</code>

Al finalizar la ejecución, la lista <@lit="Normales"> va a contener las series <@lit="norm1">, <@lit="norm2"> y <@lit="norm3">. 

A aquellos que encontréis útil la función <@lit="genseries">, quizás os interese explorar la función <@ref="feval">. 

# geoplot data-utils
Resultado: 	nada 
Argumentos:	<@var="archivomap">  (cadena)
		<@var="carga">  (serie, opcional)
		<@var="opciones">  (bundle, opcional)

Solicita la producción de un mapa, cuando se dispone de datos geográficos adecuados. En la mayoría de los casos el argumento <@var="mapfile"> debe proporcionarse como <@ref="$mapfile">, lo que indica un accesor con el que se va a recuperar el nombre del archivo que sea relevante, de tipo GeoJSON o de tipo ESRI de forma. El argumento opcional <@var="carga"> se usa para indicar el nombre de una serie con la que se colorean las regiones del mapa. Y el argumento final de tipo "bundle" te permite que puedas establecer numerosas opciones. 

Puedes consultar <@adb="geoplot.pdf"> con la documentación sobre la función, para obtener detalles y ejemplos completos. Ahí se explican todos los ajustes que se pueden configurar mediante el argumento <@var="opciones">. 

# getenv programming
Resultado: 	cadena 
Argumento: 	<@var="s">  (cadena)

Cuando ya está definida una variable de entorno con el nombre del argumento <@var="s">, la función devuelve el valor de esa variable como cadena de texto; en caso contrario, devuelve una cadena de texto vacía. Consulta también <@ref="ngetenv">. 

# getinfo data-utils
Resultado: 	bundle 
Argumento: 	<@var="y">  (serie)

Devuelve información sobre la serie especificada, la cual puedes indicar mediante su nombre o su número ID. El 'bundle' que se devuelve contiene todos los atributos que se pueden establecer por medio de la instrucción <@xrf="setinfo">. Y también contiene información adicional relevante para series que se generaron como transformaciones de datos primarios (mediante retardos, logaritmos, etc.); esto incluye la palabra de la instrucción de GRETL para la transformación con la clave “transform”, y el nombre de la serie asociada primaria con la clave “parent”. Para las series retardadas, puedes encontrar el número específico de retardos bajo la clave “lag”. 

Aquí tienes un ejemplo de su uso: 

<code>          
     open data9-7
     lags QNC
     bundle b = getinfo(QNC_2)
     print b
</code>

Al ejecutar lo anterior, podemos ver: 

<code>          
     has_string_table = 0
     lag = 2
     parent = QNC
     name = QNC_2
     graph_name =
     coded = 0
     discrete = 0
     transform = lags
     description = = QNC(t - 2)
</code>

Para comprobar si la serie 5 de un conjunto de datos es un término retardado, puedes hacer este tipo de cosas: 

<code>          
     if getinfo(5).lag != 0
        printf "La serie 5 es un retardo de %s\n", getinfo(5).parent
     endif
</code>

Ten en cuenta que puedes utilizar la notación con el punto para acceder a los elementos de un 'bundle', incluso cuando el 'bundle' es “anónimo” (no guardado con su propio nombre). 

# getkeys data-utils
Resultado: 	array de cadenas 
Argumento: 	<@var="b">  (bundle)

Devuelve un 'array' de las cadenas de texto que contienen las claves que identifican el contenido de <@var="b">. Si el 'bundle' está vacío, se devuelve un 'array' vacío. 

# getline strings
Resultado: 	escalar 
Argumentos:	<@var="origen">  (cadena)
		<@var="&destino">  (referencia a cadena)

Esta función lee filas consecutivas de <@var="origen">, que debe ser una cadena de texto ya definida. Con cada llamada a la función se escribe una línea de texto en <@var="destino"> (que también debe ser una cadena de texto indicada en formato de puntero) sin el carácter de línea nueva. El valor que se devuelve es un escalar igual a 1, cuando existe algo por leer (incluidas filas en blanco), o igual a 0 si todas las filas de <@var="origen"> ya se leyeron. 

A continuación se presenta un ejemplo en el que el contenido de un archivo de texto se divide en filas: 

<code>          
     string s = readfile("data.txt")
     string line
     scalar i = 1
     loop while getline(s, &line)
         printf "line %d = '%s'\n", i++, line
     endloop
</code>

En el ejemplo se puede asegurar que, cuando finalice el bucle, el texto de <@var="origen"> ya está agotado. Si no deseas agotarlo todo, puedes hacer una llamada normal a <@lit="getline">, seguida de una nueva llamada de “limpieza”, cambiando el argumento <@var="destino"> por <@lit="null"> (o dejarlo en blanco), con lo que se reinicia la lectura de <@var="origen">, como en 

<code>          
     getline(s, &line) # Obtiene una única fila
     getline(s, null) # Reinicia la lectura
</code>

Ten en cuenta que, aunque avanza la posición de lectura cada vez que se ejecuta <@lit="getline">, el argumento <@var="origen"> no se altera con esa función; solo cambia <@var="destino">. 

# ghk stats
Resultado: 	matriz 
Argumentos:	<@var="C">  (matriz)
		<@var="A">  (matriz)
		<@var="B">  (matriz)
		<@var="U">  (matriz)
		<@var="&dP">  (referencia a matriz, o <@lit="null">)

Calcula la aproximación GHK (Geweke, Hajivassiliou, Keane) a la función de distribución Normal multivariante; puedes consultar, por ejemplo, <@bib="Geweke (1991);geweke91">. El valor que se devuelve es un vector <@itl="n">×1 de probabilidades. 

El argumento matricial <@var="C"> (<@itl="m">×<@itl="m">) debe aportar el factor de Cholesky (matriz triangular inferior) de la matriz de covarianzas de <@mth="m"> variables Normales. Los argumentos matriciales <@var="A"> y <@var="B"> deben ser ambos <@itl="n">×<@itl="m">; e indicar respectivamente los límites inferior y superior que se aplican a las variables en cada una de las <@mth="n"> observaciones. Donde las variables no tengan límites, eso se debe indicar usando la constante <@ref="$huge"> o su negativo. 

La matriz <@var="U"> debe ser <@itl="m">×<@itl="r">, donde <@mth="r"> indica el número de extracciones pseudoaleatorias de una distribución Uniforme. Para crear <@var="U"> son adecuadas las funciones <@ref="muniform"> y <@ref="halton">. 

Debajo se ilustra esto con un ejemplo relativamente simple, en el que las probabilidades multivariantes pueden calcularse analíticamente. Las series <@lit="P"> y <@lit="Q"> deben ser numéricamente muy semejantes una a la otra, denotando como <@lit="P"> a la probabilidad “verdadera” y como <@lit="Q"> a su aproximación GHK: 

<code>          
     nulldata 20
     series inf1 = -2*uniform()
     series sup1 = 2*uniform()
     series inf2 = -2*uniform()
     series sup2 = 2*uniform()

     scalar rho = 0.25
     matrix V = {1, rho; rho, 1}

     series P = cdf(D, rho, inf1, inf2) - cdf(D, rho, sup1, inf2) \
     - cdf(D, rho, inf1, sup2) + cdf(D, rho, sup1, sup2)

     C = cholesky(V)
     U = halton(2, 100)

     series Q = ghk(C, {inf1, inf2}, {sup1, sup2}, U)
</code>

El argumento opcional <@var="dP"> se usa para obtener la matriz <@itl="n">×<@itl="k"> de derivadas analíticas de las probabilidades, donde <@mth="k"> equivale a 2<@mth="m"> + <@mth="m">(<@mth="m"> + 1)/2. Las primeras <@mth="m"> columnas van a contener las derivadas con respecto a los límites inferiores; las <@mth="m"> siguientes van a recoger las derivadas con respecto a los límites superiores; y las restantes columnas van a recoger las derivadas con respecto a los elementos singulares de la matriz <@mth="C">, en el orden que sigue la semivectorización “vech” de una matriz simétrica. 

# gini stats
Resultado: 	escalar 
Argumento: 	<@var="y">  (serie o vector)

Devuelve un escalar con el índice de desigualdad de Gini para la serie o vector (no negativos) <@var="y">. Un valor de Gini igual a cero indica igualdad perfecta. El máximo valor de Gini para una serie con <@mth="n"> elementos es (<@mth="n"> – 1)/<@mth="n">, lo que sucede cuando únicamente un elemento tiene un valor positivo; por lo tanto, un valor de Gini igual a 1.0 es el límite que se alcanza cuando una serie muy larga tiene máxima desigualdad. 

# ginv linalg
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz)
		<@var="tol">  (escalar, opcional)

Devuelve la matriz <@mth="A"><@sup="+">, la matriz pseudoinversa de Moore–Penrose o inversa generalizada de una matriz <@var="A"> de orden <@itl="r">×<@itl="c">, calculada mediante la descomposición en valores singulares. 

El resultado de esta operación depende del número de valores singulares de la matriz <@var="A"> que numéricamente se consideran iguales a 0. Puedes usar el parámetro opcional <@var="tol"> para retocar este aspecto. Se consideran los valores singulares iguales a 0 cuando son menores que <@mth="m × tol × s">, donde <@mth="m"> es el mayor valor de entre <@mth="r"> y <@mth="c">, siendo <@mth="s"> lo que expresa el valor singular más grande. Cuando omites el segundo argumento, se establece que <@var="tol"> sea igual al épsilon de la máquina (consulta <@ref="$macheps">). En algunos casos, puedes desear establecer que <@var="tol"> sea un valor más grande (p.e. 1.0e-9) con objeto de evitar que se sobrestime el rango de la matriz <@var="A"> (lo que podría dar lugar a resultados numéricamente inestables). 

Esta matriz posee las siguientes propiedades: <@mth="A"> <@mth="A"><@sup="+"> <@mth="A"> = <@mth="A"> y <@mth="A"><@sup="+"> <@mth="A"> <@mth="A"><@sup="+"> = <@mth="A"><@sup="+">. Además de eso, los productos <@mth="A"> <@mth="A"><@sup="+"> y <@mth="A"><@sup="+"> <@mth="A"> son simétricos por construcción. 

Ver también <@ref="inv">, <@ref="svd">. 

# GSSmax numerical
Resultado: 	escalar 
Argumentos:	<@var="&b">  (referencia a matriz)
		<@var="f">  (llamada a función)
		<@var="toler">  (escalar, opcional)

Maximización unidimensional mediante el método Golden Section Search (GSS). La matriz <@var="b"> del argumento debe ser un vector de 3 elementos. Al definirla, el primer elemento se ignora, mientras que el segundo y tercer elementos establecen los límites inferior y superior de la búsqueda. El argumento <@var="fncall"> deberá especificar una llamada a la función que devuelve el valor del concepto a maximizar; el término 1 de <@var="b"> (que deberá contener el valor vigente del parámetro que se ajusta cuando se invoca la función) debe indicarse como primer argumento; cualquier otro argumento requerido puede ir entonces a continuación. La función en cuestión deberá ser unimodal (no debe tener otro máximo local que no sea el máximo global) en el rango estipulado, pues de lo contrario no se asegura que GSS encuentre el máximo. 

Al completarse con éxito, <@lit="GSSmax"> devolverá el valor óptimo del concepto que se quiere maximizar, mientras que <@var="b"> contendrá el valor óptimo del parámetro junto con los límites de su ventana de valores. 

El tercer argumento (opcional) puede utilizarse para establecer la tolerancia para alcanzar la convergencia; es decir, la amplitud máxima admisible de la ventana final de valores del parámetro. Si no indicas este argumento, se utiliza el valor 0.0001. 

Si tu objetivo realmente es alcanzar un mínimo, puedes bien cambiar la función considerando el negativo del criterio, o bien, alternativamente, puedes invocar la función <@lit="GSSmax">bajo el alias <@lit="GSSmin">. 

Aquí tienes un ejemplo sencillo de utilización: 

<code>          
     function scalar trigfunc (scalar theta)
         return 4 * sin(theta) * (1 + cos(theta))
     end function

     matrix m = {0, 0, $pi/2}
     eval GSSmax(&m, trigfunc(m[1]))
     printf "\n%10.7f", m
</code>

# GSSmin numerical
Resultado: 	escalar 

Un alias de <@ref="GSSmax">. Si invocas la función bajo este nombre, se ejecuta haciendo una minimización. 

# halton matrix
Resultado: 	matriz 
Argumentos:	<@var="m">  (entero)
		<@var="r">  (entero)
		<@var="desfase">  (entero, opcional)

Devuelve una matriz <@itl="m">×<@itl="r"> que contiene <@mth="m"> secuencias de Halton de longitud <@mth="r">. Las secuencias se construyen utilizando los primeros <@mth="m"> números primos. Por defecto, se descartan los primeros 10 elementos de cada una de las secuencias, aunque puedes ajustar esto por medio del argumento opcional <@var="desfase">, que debe ser un número entero no negativo. Para obtener más detalles puedes consultar <@bib="Halton y Smith (1964);halton64">. 

# hdprod linalg
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="Y">  (matriz, opcional)

Devuelve la matriz que resulta del producto directo horizontal de dos matrices. Los dos argumentos deben tener el mismo número <@mth="r"> de filas. El valor que se devuelve es una matriz que tiene <@mth="r"> filas, y en la que la <@mth="i">-ésima fila es el producto de Kronecker de las respectivas filas de las matrices <@var="X"> e <@var="Y">. Si omites <@var="Y">, se aplica la sintaxis “breve” (mira abajo). 

Si <@var="X"> es una matriz <@mth="r x k"> e <@var="Y"> es una matriz <@mth="r x m">, el resultado será una matriz con <@mth="r"> filas y con <@mth="k x m"> columnas. 

Esta operación se llama “producto directo horizontal” de acuerdo con la forma en la que se pone en funcionamiento, y se aplica en el lenguaje de programación GAUSS. Su equivalente en el álgebra matricial estándar podría denominarse producto horizontal (row-wise) de Khatri-Rao, o producto “de división de caras” (face-splitting) en la literatura sobre el procesado de señales. 

Ejemplo: el código... 

<code>          
     A = {1,2,3; 4,5,6}
     B = {0,1; -1,1}
     C = hdprod(A, B)
</code>

produce la siguiente matriz: 

<code>          
          0    1    0    2    0    3
         -4    4   -5    5   -6    6
</code>

<subhead>Sintaxis breve</subhead> 

Si <@var="X"> e <@var="Y"> son la misma matriz, entonces cada fila del resultado representa la vectorización de una matriz simétrica. En estos casos, puedes omitir el segundo argumento; sin embargo, la matriz que se va a devolver contendrá solo las columnas no redundantes y, consecuentemente, tendrá <@mth="k(k+1)/2"> columnas. Por ejemplo, 

<code>          
     A = {1,2,3; 4,5,6}
     C = hdprod(A)
</code>

genera 

<code>          
     1    2    3    4    6    9 
     16   20   24   25   30   36
</code>

Ten en cuenta que la <@mth="i">-ésima fila de <@mth="C"> es <@mth="vech(a"><@sub="i"> <@mth="a"><@sub="i"><@mth="')">, donde <@mth="a"><@sub="i"> es la <@mth="i">-ésima fila de <@mth="A">. 

Cuando utilices la sintaxis breve con matrices complejas, el segundo argumento que se va a suponer implícito será el <@itl="conjugado"> del primero, de tal forma que hará que cada fila del resultado sea la vectorización simétrica de una matriz Hermítica. 

# hfdiff midas
Resultado: 	lista 
Argumentos:	<@var="hfvars">  (lista)
		<@var="multiplicador">  (escalar)

Dada una <@xrf="MIDAS_list">, la función devuelve otra lista de la misma longitud que contiene las primeras diferencias de alta frecuencia. El segundo argumento es opcional y, por defecto, igual a 1: puedes utilizarlo para multiplicar las diferencias por alguna constante. 

# hfldiff midas
Resultado: 	lista 
Argumentos:	<@var="hfvars">  (lista)
		<@var="multiplicador">  (escalar)

Dada una <@xrf="MIDAS_list">, la función devuelve otra lista de la misma longitud que contiene las diferencias logarítmicas de alta frecuencia. El segundo argumento es opcional y, por defecto, igual a 1: puede utilizarse para multiplicar las diferencias por alguna constante; por ejemplo, podrías darle el valor 100 para obtener aproximadamente las variaciones porcentuales. 

# hflags midas
Resultado: 	lista 
Argumentos:	<@var="retardomin">  (entero)
		<@var="retardomax">  (entero)
		<@var="hfvars">  (lista)

Dada una <@xrf="MIDAS_list">, <@var="hfvars">, la función devuelve otra lista con los retardos de alta frecuencia desde <@var="retardomin"> hasta <@var="retardomax">. Debes utilizar valores positivos para indicar los retardos, y negativos para indicar los adelantos. Por ejemplo, si <@var="retardomin"> es –3, y <@var="retardomax"> es 5, entonces la lista que se va a devolver contendrá 9 series: 3 adelantos, el valor actual y 5 retardos. 

Ten en cuenta que el retardo 0 de alta frecuencia se corresponde con el primer período de alta frecuencia, dentro de un período de baja frecuencia; por ejemplo, correspondería con el primer mes dentro de un trimestre o con el primer día dentro de un mes. 

# hflist midas
Resultado: 	lista 
Argumentos:	<@var="x">  (vector)
		<@var="m">  (entero)
		<@var="prefijo">  (cadena)

Produce una <@xrf="MIDAS_list"> de <@var="m"> series a partir del vector <@var="x">, donde <@var="m"> indica la razón entre la frecuencia (mayor) de las observaciones de la variable <@var="x"> y la frecuencia base (menor) del conjunto vigente de datos. El valor de <@var="m"> debe ser mayor o igual a 3, y el tamaño de <@var="x"> debe ser igual a <@var="m"> veces el tamaño del rango de la muestra vigente. 

Los nombres de las series de la lista que se devuelve, se construyen a partir del <@var="prefijo"> indicado (que debe ser una cadena de texto, de una longitud máxima de 24 caracteres ASCII, y válida como identificador de GRETL), a la que se añade uno o más dígitos que representan el subperíodo de la observación. Si alguno de esos nombres repite el de algún objeto ya existente, se muestra un fallo. 

# hpfilt timeseries
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="lambda">  (escalar, opcional)
		<@var="una-parte">  (booleano, opcional)

Devuelve una serie que recoge la componente cíclica del filtro de Hodrick–Prescott aplicado a la serie <@var="y">. Si no se indica el parámetro de suavizado <@var="lambda">, GRETL usa valores por defecto basados en la periodicidad de los datos; en concreto, el parámetro es igual a 100 veces el cuadrado de la periodicidad (100 para datos anuales, 1600 para datos trimestrales, etc). 

Por defecto, el filtro es el de la habitual versión de dos partes (pasado y futuro), pero si indicas el tercer argumento (opcional) mediante un valor no nulo, se calcula la variante de una sola parte (sin mirada hacia adelante) del modo que se indica en <@bib="Stock y Watson (1999);stock-watson1999">. 

El uso más habitual del filtro HP es para la eliminación de la tendencia, pero si estás interesado en la propia tendencia, es fácil obtenerla mediante substracción, como en el ejemplo siguiente: 

<code>          
     series hptrend = y - hpfilt(y)
</code>

Ver también <@ref="bkfilt">, <@ref="bwfilt">. 

# hyp2f1 math
Resultado: 	escalar o matriz 
Argumentos:	<@var="a">  (escalar)
		<@var="b">  (escalar)
		<@var="c">  (escalar)
		<@var="x">  (escalar o matriz)

Devuelve el valor de la función hipergeométrica de Gauss para el argumento real <@var="x">. 

Cuando <@var="x"> es un escalar, el valor que se devuelve va a ser un escalar; en caso contrario, va a ser una matriz con la misma dimensión que <@var="x">. 

# I matrix
Resultado: 	matriz 
Argumentos:	<@var="n">  (entero)
		<@var="m">  (entero, opcional)

Si omites <@var="m">, devuelve una matriz identidad de orden <@var="n">. En caso contrario, devuelve una matriz <@itl="n">×<@itl="m"> que tiene unos en la diagonal principal y ceros en el resto de la matriz. 

# Im complex
Resultado: 	matriz 
Argumento: 	<@var="C">  (matriz compleja)

Devuelve una matriz real con la misma dimensión que <@var="C">, que contiene la parte imaginaria de la matriz del argumento. Consulta también <@ref="Re">. 

# imaxc stats
Resultado: 	vector fila 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector fila que indica cual es la fila que tiene el valor más grande, por cada columna de la matriz <@var="X">.Para columnas que tengan valores <@lit="NA">s, el resultado también se establece como <@lit="NA">, excepto cuando no sea cero el argumento opcional <@var="obviar_na">, en cuyo caso se devuelve el índice de la mayor entrada válida. 

Ver también <@ref="imaxr">, <@ref="iminc">, <@ref="maxc">. 

# imaxr stats
Resultado: 	vector columna 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector columna que indica cual es la columna que tiene el valor más grande, por cada fila de la matriz <@var="X">.Para filas que tengan valores <@lit="NA">s, el resultado también se establece como <@lit="NA">, excepto cuando no sea cero el argumento opcional <@var="obviar_na">, en cuyo caso se devuelve el índice de la mayor entrada válida. 

Ver también <@ref="imaxc">, <@ref="iminr">, <@ref="maxr">. 

# imhof probdist
Resultado: 	escalar 
Argumentos:	<@var="M">  (matriz)
		<@var="x">  (escalar)

Calcula la Prob(<@mth="u'Au"> < <@mth="x">) para una forma cuadrática de variables Normales estándar, <@mth="u">, usando el procedimiento desarrollado por <@bib="Imhof (1961);imhof61">. 

Si el primer argumento <@var="M"> es una matriz cuadrada, se toma para que represente a <@mth="A">. Si es un vector columna, se toman sus elementos como si fuesen los autovalores calculados previamente de <@mth="A">, y en otro caso se presenta un fallo. 

Ver también <@ref="pvalue">. 

# iminc stats
Resultado: 	vector fila 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector fila que indica cual es la fila que tiene el valor más pequeño, por cada columna de la matriz <@var="X">.Para columnas que tengan valores <@lit="NA">s, el resultado también se establece como <@lit="NA">, excepto cuando no sea cero el argumento opcional <@var="obviar_na">, en cuyo caso se devuelve el índice de la menor entrada válida. 

Ver también <@ref="iminr">, <@ref="imaxc">, <@ref="minc">. 

# iminr stats
Resultado: 	vector columna 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector columna que indica cual es la columna que tiene el valor más pequeño, por cada fila de la matriz <@var="X">.Para filas que tengan valores <@lit="NA">s, el resultado también se establece como <@lit="NA">, excepto cuando no sea cero el argumento opcional <@var="obviar_na">, en cuyo caso se devuelve el índice de la menor entrada válida. 

Ver también <@ref="iminc">, <@ref="imaxr">, <@ref="minr">. 

# inbundle data-utils
Resultado: 	entero 
Argumentos:	<@var="b">  (bundle)
		<@var="llave">  (cadena)

Comprueba si el 'bundle' <@var="b"> contiene un elemento con el nombre <@var="llave">. Devuelve un entero con el código del tipo de elemento: 0 en caso de no encontrarlo y, en caso de hacerlo, 1 para un escalar, 2 para una serie, 3 para una matriz, 4 para una cadena de texto, 5 para un 'bundle', 6 para un 'array' y 7 para una lista. En base al valor de su código, la función <@ref="typestr"> se puede usar para obtener la cadena de texto que expresa el tipo de elemento que es. 

# infnorm linalg
Resultado: 	escalar 
Argumento: 	<@var="X">  (matriz)

Devuelve un escalar con la norma-infinito de la matriz <@var="X">, es decir, el máximo valor que se obtiene al sumar los valores absolutos de los elementos de la matriz <@var="X"> que hay en cada fila. 

Ver también <@ref="onenorm">. 

# inlist data-utils
Resultado: 	entero 
Argumentos:	<@var="L">  (lista)
		<@var="y">  (serie)

Devuelve un entero positivo con la posición de <@var="y"> en la lista <@var="L">, o 0 si <@var="y"> no está presente en <@var="L">. 

El segundo argumento puedes indicarlo tanto con el nombre de la serie como con el entero positivo que identifica la serie (ID). Cuando sabes que existe una serie con un nombre concreto (por ejemplo, <@lit="foo">), puedes ejecutar esta función de la siguiente forma: 

<code>          
     pos = inlist(L, foo)
</code>

Con la expresión anterior estás pidiendo: “Indícame con un entero la posición de la serie <@lit="foo"> en la lista <@lit="L"> (o 0 si no está incluida en esa lista)”. De cualquier modo, si no tienes certeza de que exista una serie con un nombre concreto, debes indicar ese nombre entre comillas de esta forma: 

<code>          
     pos = inlist(L, "foo")
</code>

En este caso, lo que estás solicitando es: “Si existe una serie llamada <@lit="foo"> en la lista <@lit="L">, indícame su posición; en caso de que no exista, devuelve un 0.” 

# instring strings
Resultado: 	entero 
Argumentos:	<@var="s1">  (cadena)
		<@var="s2">  (cadena)
		<@var="ign_mayus">  (booleano, opcional)

Este es un booleano relativo de <@ref="strstr">: devuelve 1 si <@var="s1"> contiene <@var="s2">, y 0 en caso contrario. De este modo, la expresión condicional 

<code>          
     if instring("gatada", "gata")
</code>

es equivalente lógicamente (pero más eficiente) que 

<code>          
     if strlen(strstr("gatada", "gata")) > 0
</code>

Si el argumento opcional <@var="ign_mayus"> no es cero, la búsqueda no distinguirá mayúsculas de minúsculas. Por ejemplo: 

<code>          
     instring("Gatada", "gata")
</code>

devuelve 0, pero 

<code>          
     instring("Gatada", "gata", 1)
</code>

devuelve 1. 

# instrings strings
Resultado: 	mira más abajo 
Argumentos:	<@var="S">  (array de cadenas)
		<@var="cotejo">  (cadena)
		<@var="simple">  (booleano, opcional)

Comprueba si los elementos del 'array' de cadenas de texto <@var="S"> son iguales a <@var="cotejo">. Por defecto, devuelve un vector columna de longitud igual al número de coincidencias que se producen, y que contiene la posición que ocupa cada coincidencia dentro del 'array' (o bien una matriz vacía en caso de no haber coincidencias). 

Ejemplo: 

<code>          
     strings S = defarray("A", "B", "C", "B")
     eval instrings(S, "B")
     2
     4
</code>

Cuando se indica un valor no nulo para el argumento opcional <@var="simple">, el valor que se devuelve es un escalar: 1 si <@var="cotejo"> se encuentra en <@var="S">, y 0 de lo contrario. En este caso, la implementación es capaz de coger un atajo, por lo que es más eficiente si solo quieres una respuesta booleana. 

# int math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con la parte entera de <@var="x">, truncando la parte decimal; o <@lit="NA"> si no se puede representar el resultado como un entero de 32-bit con signo (si no cae en el intervalo [–2147483648, 2147483647]). 

Nota: <@lit="int"> y <@ref="floor"> producen distintos efectos con argumentos negativos: <@lit="int(-3.5)"> genera –3, mientras que <@lit="floor(-3.5)"> genera –4. Ver también <@ref="ceil">, <@ref="floor">, <@ref="round">. 

# interpol timeseries
Resultado: 	serie 
Argumento: 	<@var="x">  (serie)

Devuelve una serie en la que los valores ausentes de <@var="x"> se imputan mediante interpolación lineal, tanto para datos de series temporales como para la dimensión temporal de un conjunto de datos de panel. Pero no se hace extrapolación; los valores ausentes se reemplazan únicamente si están precedidos y seguidos a la vez de observaciones válidas. 

# inv linalg
Resultado: 	matriz 
Argumento: 	<@var="A">  (matriz cuadradax)

Devuelve la matriz inversa de <@var="A">. Cuando esta última es una matriz singular o no cuadrada, se produce un mensaje de fallo y no se devuelve nada. Ten en cuenta que GRETL comprueba automáticamente la estructura de <@var="A">, y utiliza el procedimiento numérico más eficiente para realizar la inversión. 

Los tipos de matriz que GRETL comprueba automáticamente son: identidad, diagonal, simétrica definida positiva, simétrica definida no positiva, y triangular. 

Nota: En buena lógica, solo debes utilizar esta función cuando tratas de aplicar la inversa de <@var="A"> más de una vez. Cuando únicamente necesitas calcular, por ejemplo, una expresión de la forma <@mth="A"><@sup="-1"><@mth="B">, es preferible que utilices los operadores de “división”: <@lit="\"> y <@lit="/">. Para obtener más detalles, puedes consultar <@pdf="El manual de gretl#chap:matrices"> (Capítulo 17). 

Ver también <@ref="ginv">, <@ref="invpd">. 

# invcdf probdist
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="d">  (cadena)
		<@var="…">  (mira más abajo)
		<@var="u">  (escalar, serie o matriz)

Calcula la inversa de la función de distribución acumulativa. Para una distribución continua devuelve un resultado (del tipo del argumento) con el valor de <@mth="x"> que cumple <@mth="P(X ≤ x) = u">, con <@var="u"> dentro del intervalo entre 0 y 1. Para una distribución discreta (Binomial o Poisson), devuelve el valor más pequeño de <@mth="x"> para el que se cumple <@mth="P(X ≤ x) ≥ u">. 

La distribución de <@mth="X"> se especifica mediante la letra <@var="d">. Entre los argumentos <@var="d"> y <@var="u">, puedes necesitar algún argumento escalar adicional para especificar los parámetros de la distribución de que se trate. Esto se hace del modo que se indica a continuación: 

<indent>
• Normal estándar (c = z, n o N): sin argumentos extras 
</indent>

<indent>
• Gamma (g o G): forma, escala 
</indent>

<indent>
• t de Student (t): grados de libertad 
</indent>

<indent>
• Chi-cuadrado (c, x o X): grados de libertad 
</indent>

<indent>
• F de Snedecor (f o F): grados de libertad (num.), grados de libertad (den.) 
</indent>

<indent>
• Binomial (b o B): probabilidad, cantidad de ensayos 
</indent>

<indent>
• Poisson (p o P): media 
</indent>

<indent>
• Laplace (l o L): media, escala 
</indent>

<indent>
• Error Generalizado (E): forma 
</indent>

<indent>
• Chi-cuadrado no central (ncX): grados de libertad, parámetro de no centralidad 
</indent>

<indent>
• F no central (ncF): grados de libertad (num.), grados de libertad (den.), parámetro de no centralidad 
</indent>

<indent>
• t no central (nct): grados de libertad, parámetro de no centralidad 
</indent>

Ver también <@ref="cdf">, <@ref="critical">, <@ref="pvalue">. 

# invmills probdist
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con la razón inversa de Mills en <@var="x">, es decir, la razón entre la densidad Normal estándar y el complementario de la función de distribución Normal estándar, ambas evaluadas en <@var="x">. 

Esta función utiliza un algoritmo adecuado que proporciona una precisión mucho mejor que la que se alcanza haciendo los cálculos con <@ref="dnorm"> y <@ref="cnorm">; ahora bien, la diferencia entre los dos métodos es considerable solo para valores muy negativos de <@var="x">. 

Ver también <@ref="cdf">, <@ref="cnorm">, <@ref="dnorm">. 

# invpd linalg
Resultado: 	matriz cuadradax 
Argumentos:	<@var="A">  (matriz definida positiva)
		<@var="&logdet">  (referencia a escalar, opcional)

Devuelve la matriz cuadrada resultante de invertir la matriz simétrica definida positiva <@var="A">. Para matrices muy grandes, esta función es ligeramente más rápida que <@ref="inv"> puesto que con ella no se comprueba si la matriz es simétrica. Por esta razón, la función debe utilizarse con prudencia. 

Si está presente el argumento <@var="&logdet"> (opcional), el escalar resultante va a contener (si acaba con éxito la función) el logaritmo del determinante de la matriz <@var="A">. Disponer de esto puede ser muy interesante en algunos casos; por ejemplo, en el contexto de evaluación del logaritmo de una versosimilitud de tipo Normal (Gaussiana), porque el logaritmo del determinante es un subproducto del algoritmo de la inversion, y conseguirlo mediante el argumento <@var="&logdet"> evita cálculos adicionales. 

Nota: Si pretendes invertir una matriz de la forma <@mth="X'X">, donde <@mth="X"> es una matriz muy grande, es preferible que la calcules mediante el operador principal <@lit="X'X"> en lugar de usar la sintaxis más general <@lit="X'*X">. La primera expresión utiliza un algoritmo especializado que tiene una doble ventaja: resulta más eficiente desde el punto de vista del cómputo; y va a garantizar que la matriz resultante esté libre, por construcción, de los artefactos de precisión de máquina que pudieran convertirla en numéricamente no simétrica. 

# irf timeseries
Resultado: 	matriz 
Argumentos:	<@var="efecto">  (entero)
		<@var="impacto">  (entero)
		<@var="alfa">  (escalar entre 0 y 1, opcional)
		<@var="sys">  (bundle, opcional)

Proporciona una matriz con las funciones estimadas de respuesta al impulso correspondientes a un VAR o un VECM, trazadas sobre un determinado horizonte de predicción. Sin el argumento final (opcional), esta función sirve solo cuando el último modelo estimado fue un VAR o un VECM. Como alternativa, puedes guardar la información sobre uno de esos sistemas como 'bundle', mediante el accesor <@ref="$system">, y posteriormente aplicarle la función <@lit="irf">. 

Los argumentos <@var="efecto"> e <@var="impacto"> son índices, con formato de números enteros, de las variables endógenas del sistema; y se usa 0 para indicar “todas”. Las respuestas (expresadas en las unidades de la variable <@var="efecto">) lo son ante una innovación de una desviación típica en la variable <@var="impacto">. Cando le asignas un valor positivo adecuado a <@var="alfa">, las estimaciones incluyen un intervalo de confianza de 1 – α (de esta forma, por ejemplo, indica 0.1 si deseas obtener un intervalo del 90 por ciento). 

El siguiente fragmento de código ilustra su uso. En el primer ejemplo, la matriz <@lit="ir1"> contiene las respuestas de <@lit="y1"> ante las innovaciones en cada una de las <@lit="y1">, <@lit="y2"> e <@lit="y3"> (son estimaciones por punto ya que se omite <@var="alfa">). En el segundo ejemplo, <@lit="ir2"> contiene las respuestas de todas las variables de efecto a una innovación en <@lit="y2">, con intervalos de confianza del 90 por ciento. En este caso, la matriz que se devuelve tendrá 9 columnas: cada vía de respuesta ocupa 3 columnas contiguas que indican la estimación por punto, el límite inferior y el límite superior. El último ejemplo produce una matriz con 27 columnas: 3 columnas para cada respuesta ante cada variable de efecto, multiplicadas por cada una de las tres variables de choque. 

<code>          
     var 4 y1 y2 y3
     matrix ir1 = irf(1, 0)
     matrix ir2 = irf(0, 2, 0.1)
     matrix ir3 = irf(0, 0, 0.1)
</code>

El número de períodos (filas) sobre los que se traza la respuesta se determina automáticamente dependiendo de la frecuencia de los datos; pero eso puede ajustarse por medio de la instrucción <@xrf="set">, como por ejemplo con <@lit="set horizon 10">. 

Cuando se presentan los intervalos de confianza, estos se generan mediante la técnica de muestreo repetido 'bootstrapping' de los residuos originales. Se asume que el orden del retardo del VAR o del VECM ya es suficiente como para eliminar la autocorrelación de los residuos. Por defecto, el número de repeticiones del muestreo 'bootstrap' es de 1999, pero puedes ajustar esto mediante la instrucción <@xrf="set">, como en 

<code>          
     set boot_iters 2999
</code>

Ver también <@ref="fevd">, <@ref="vma">. 

# irr math
Resultado: 	escalar 
Argumento: 	<@var="x">  (serie o vector)

Devuelve un escalar con la Tasa Interna de Rendimiento (TIR) para <@var="x">, considerada como una secuencia de pagos (negativos) e ingresos (positivos). Ver también <@ref="npv">. 

# iscomplex data-utils
Resultado: 	escalar 
Argumento: 	<@var="nombre">  (cadena)

Comprueba si <@var="nombre"> es el identificador de una matriz compleja. El valor que se devuelve es alguno de los siguientes: 

<@lit="NA">: <@var="nombre"> no identifica a una matriz. 

<@lit="0">: <@var="nombre"> identifica una matriz real, en su totalidad formada por números normales de punto flotante (“dobles”, en la terminología de C). 

<@lit="1">: <@var="nombre"> identifica una matriz “en principio” compleja, formada por números que tienen tanto una parte real como otra imaginaria, pero en los que las partes imaginarias son nulas. 

<@lit="2">: la matriz en cuestión contiene, al menos, un valor “auténticamente” complejo, con una parte imaginaria que no es nula. 

# isconst data-utils
Resultado: 	entero 
Argumentos:	<@var="y">  (serie o vector)
		<@var="codigo-panel">  (entero, opcional)

Sin el segundo argumento (opcional), devuelve el número entero igual a 1 cuando <@var="y"> tenga un valor constante a lo largo de la muestra vigente seleccionada (o a lo largo de toda su extensión si <@var="y"> es un vector); en otro caso, devuelve el entero 0. 

El segundo argumento solo se acepta cuando <@var="y"> es una serie, y el conjunto vigente de datos es un panel. En este caso, un valor de <@var="codigo-panel"> igual a 0 solicita que la función verifique si la serie no varía con el paso del tiempo; y un valor igual a 1 hace que la función verifique si la serie no varía transversalmente (es decir, si el valor de <@var="y"> en cada período de tiempo, es el mismo para todos los grupos). 

Si <@var="y"> es una serie, las observaciones con valores ausentes se ignoran durante la verificación de la invariabilidad de la serie. 

# isdiscrete data-utils
Resultado: 	entero 
Argumento: 	<@var="nombre">  (cadena)

Si <@var="nombre"> es una cadena que identifica una serie ya definida, y si está marcada como de tipo discreto, la función devuelve un entero igual a1; en caso contrario, devuelve 0. Si <@var="nombre"> no identifica una serie, la función devuelve <@lit="NA">. 

# isdummy data-utils
Resultado: 	entero 
Argumento: 	<@var="x">  (serie o vector)

Si todos los valores contenidos en <@var="x"> son iguales a 0 o a 1 (o ausentes), devuelve un entero con el recuento de unos; si no, devuelve 0. 

# isnan data-utils
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar o matriz)

Dado un argumento escalar, devuelve 1 si <@var="x"> no es un número, “Not a Number” (NaN); en caso contrario, devuelve 0. Dada una matriz como argumento, devuelve otra matriz de la misma dimensión que contiene valores iguales a 1 en las posiciones en las que los elementos que les corresponden de la matriz de entrada son NaN, y 0 en las demás posiciones. 

# isoconv calendar
Resultado: 	entero 
Argumentos:	<@var="fecha">  (serie)
		<@var="&año">  (referencia a serie)
		<@var="&mes">  (referencia a serie)
		<@var="&día">  (referencia a serie, opcional)

Dada la serie <@var="fecha"> que contiene fechas en el formato ISO 8601 “básico” (<@lit="YYYYMMDD">), esta función convierte las componentes de año, mes y (opcionalmente) día en nuevas series designadas por el segundo y siguientes argumentos. Un ejemplo de su aplicación, asumiendo que la serie <@lit="fechas"> contiene valores adecuados de 8 dígitos, sería: 

<code>          
     series y, m, d
     isoconv(fechas, &y, &m, &d)
</code>

Esta función devuelve el valor nominal 0 en caso de completarse con éxito; en caso de que no funcione, se muestra un fallo. 

# isocountry strings
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="origen">  (cadena o array de cadenas)
		<@var="resultado">  (entero, opcional)

Esta función está relacionada con las cuatro notaciones para países que están incluídas en el estándar ISO 3166; concretamente 

<indent>
1. Nombre de país 
</indent>

<indent>
2. Código alfa-2 (dos letras mayúsculas) 
</indent>

<indent>
3. Código alfa-3 (tres letras mayúsculas) 
</indent>

<indent>
4. Código numérico (3 dígitos) 
</indent>

Cuando indicas un país con alguna de esas formas, el resultado es su representación en la forma (de 1 a 4) que elijas mediante el argumento opcional <@var="resultado">. Si omites ese argumento, la conversión por defecto se hace del siguiente modo: cuando el argumento <@var="origen"> es un nombre de un país, el resultado es el código de 2 letras del país; en caso contrario, el resultado es el nombre del país. Debajo se ilustran varias solicitudes válidas con formato interactivo. 

<code>          
     ? eval isocountry("Bolivia")
     BO
     ? eval isocountry("Bolivia", 3)
     BOL
     ? eval isocountry("GB")
     United Kingdom of Great Britain and Northern Ireland
     ? eval isocountry("GB", 3)
     GBR
     ? strings S = defarray("ES", "DE", "SD")
     ? strings C = isocountry(S)
     ? print C
     Array de strings, longitud 3
     [1] "Spain"
     [2] "Germany"
     [3] "Sudan"
     ? matrix m = {4, 840}
     ? C = isocountry(m)
     ? print C
     Array de strings, longitud 2
     [1] "Afghanistan"
     [2] "United States of America"
</code>

Cuando <@var="origen"> tiene la forma 4 (código numérico), esto puede indicarse mediante una cadena de texto o un 'array' de cadenas (por ejemplo, “032” para Argentina) o con formato numérico. En este último caso, <@var="origen"> puede indicarse como una serie o como un vector, pero se va a mostrar un fallo si alguno de los números está fuera del rango de 0 a 999. 

En todos los casos (incluso cuando elijas el formato 4 de resultados) se devuelve una cadena de texto o un 'array' de cadenas; si necesitas los valores numéricos, puedes obtenerlos usando la función <@ref="atof">. Cuando <@var="origen"> no coincide con ninguna entrada de la tabla ISO 3166, el resultado es una cadena vacía, y en ese caso se muestra una advertencia. 

# isodate calendar
Resultado: 	mira más abajo 
Argumentos:	<@var="ed">  (escalar, serie o matriz)
		<@var="como-cadena">  (booleano, opcional)

El argumento <@var="ed"> se interpreta como un día de época (que tomará el valor 1 para el primer día de enero del año 1 después de Cristo, en el calendario Gregoriano proléptico). El valor que se devuelve por defecto es un número de 8 dígitos del mismo tipo que <@var="ed">, o una serie compuesta por números de esa clase. Se sigue el patrón <@lit="YYYYMMDD"> (formato ISO 8601 “básico”) para proporcionar la fecha en el calendario Gregoriano que se corresponde al día en la época actual. 

Si el segundo argumento <@var="como-cadena"> (opcional) es no nulo, la función no devuelve un valor numérico sino más bien una cadena de texto que sigue el patrón <@lit="YYYY-MM-DD"> (formato ISO 8601 “extendido”), o una serie con valores en formato de texto si <@var="ed"> es una serie, o un 'array' de cadenas de texto si <@var="ed"> es un vector. Para ver un medio más flexible de obtener representaciones de los días de época con cadenas de texto, consulta <@ref="strfday">. 

En relación a la función inversa consulta <@ref="epochday">. Consulta también <@ref="juldate">. 

# isoweek calendar
Resultado: 	mira más abajo 
Argumentos:	<@var="año">  (escalar o serie)
		<@var="mes">  (escalar o serie)
		<@var="dia">  (escalar o serie)

Devuelve el número de semana (en formato ISO 8601) que se corresponde con la(s) fecha(s) especificada(s) por los tres argumentos, o <@lit="NA"> si la fecha no es válida. Ten en cuenta que los tres argumentos deben ser todos del mismo tipo, bien escalares (enteros) o bien series. 

Las semanas en formato ISO se numeran de 01 a 53. La mayoría de los años teñen 52 semanas, pero una media de 71 de 400 años tienen 53 semanas. La semana 01, según la definición ISO 8601, es la semana que contiene el primer jueves del año en el calendario Gregoriano. Para obtener una explicación completa, consulta <@url="https://en.wikipedia.org/wiki/ISO_week_date">. 

También se admite una solicitud alternativa: cuando se indica un único argumento, se considera que es una fecha (o una serie de fechas) en formato numérico “básico” ISO 8601, <@lit="YYYYMMDD">. De este modo, las siguientes dos solicitudes generan el mismo resultado, concretamente 13. 

<code>          
     eval isoweek(2022, 4, 1)
     eval isoweek(20220401)
</code>

# iwishart probdist
Resultado: 	matriz 
Argumentos:	<@var="S">  (matriz simétrica)
		<@var="v">  (entero)

Dada <@var="S"> (una matriz de orden <@itl="p">×<@itl="p"> definida positiva), esta función devuelve una matriz generada a partir de una realización de la distribución Inversa de Wishart con <@var="v"> grados de libertad, donde <@var="v"> no debe ser menor que <@mth="p">. El resultado que se devuelve también es una matriz <@itl="p">×<@itl="p">. Se utiliza el algoritmo de <@bib="Odell y Feiveson (1966);odell-feiveson66">. 

# jsonget data-utils
Resultado: 	cadena 
Argumentos:	<@var="buf">  (cadena)
		<@var="ruta">  (cadena)
		<@var="&nleer">  (referencia a escalar, opcional)

Como argumento <@var="buf"> deberás utilizar un buffer JSON, tal como puede recuperarse de un sitio web adecuado mediante la función <@ref="curl">; y como argumento <@var="ruta"> deberás usar una especificación de tipo JsonPath. 

Esta función devuelve una cadena de texto que representa los datos que se encuentran en el buffer en la ruta especificada. Se admiten los tipos de datos “double” (punto flotante), “int” (entero) y cadena de texto. En caso de enteros o de puntos flotantes, se devuelve su representación como cadenas de texto (usando para los segundos, “C” local). Si el objeto al que se refiere la <@var="ruta"> es un 'array', sus elementos se imprimen en la cadena de texto devuelta, uno por cada fila. 

Por defecto, se muestra un fallo si <@var="ruta"> no coincide en el buffer JSON; pero este comportamiento se modifica si indicas el tercer argumento (opcional) pues, en este caso, el argumento recupera un recuento de las coincidencias, devolviéndose una cadena vacía si no hay ninguna. Llamada de ejemplo: 

<code>          
     ngot = 0
     ret = jsonget(jbuf, "$.some.thing", &ngot)
</code>

Ahora bien, todavía se va a mostrar un fallo en caso de hacer una solicitud mal configurada. 

Puedes encontrar una exposición fidedigna de la sintaxis JsonPath en <@url="http://goessner.net/articles/JsonPath/">. De cualquier modo, observa que el soporte de <@lit="jsonget"> lo proporciona <@lit="json-glib">, que no necesariamente soporta todos los elementos de JsonPath. Y además, la funcionalidad concreta que desarrolla <@lit="json-glib"> puede ser muy diferente, dependiendo de la versión que tengas en tu sistema. Puedes consultar <@url="https://wiki.gnome.org/Projects/JsonGlib"> si necesitas tener más detalles. 

Dicho esto, los siguientes operadores debieran de estar disponibles para <@lit="jsonget">: 

<indent>
• nodo raíz, por medio del carácter <@lit="$"> 
</indent>

<indent>
• operador descendente recursivo: <@lit=".."> 
</indent>

<indent>
• operador comodín: <@lit="*"> 
</indent>

<indent>
• operador subíndice: <@lit="[]"> 
</indent>

<indent>
• operador de notación de conjunto, por ejemplo <@lit="[i,j]"> 
</indent>

<indent>
• operador de truncado: <@lit="[principio:fin:paso]"> 
</indent>

# jsongetb data-utils
Resultado: 	bundle 
Argumentos:	<@var="buf">  (cadena)
		<@var="ruta">  (cadena, opcional)

Como argumento <@var="buf"> deberás utilizar un buffer JSON, tal como puede recuperarse de un sitio web adecuado mediante la función <@ref="curl">. La especificación y el efecto del argumento opcional <@var="ruta"> se describe más abajo. 

Lo que se devuelve es un 'bundle' cuya estructura básicamente refleja la de la entrada: los objetos JSON se convierten en 'bundles' de GRETL, y los 'arrays' JSON se convierten en 'arrays' de GRETL; cada uno de ellos puede contener cadenas de texto, 'bundles' o 'arrays'. Los nodos de “valor” JSON se convierten en componentes de 'bundles' o elementos de 'arrays'; en el último caso, los valores numéricos se convierten en cadenas de texto utilizando <@lit="sprintf">. Ten en cuenta que, aunque la especificación JSON permite 'arrays' de tipo mixto, estos no se poden manejar mediante <@lit="jsongetb"> puesto que los 'arrays' de GRETL deben ser de tipo único. 

Puedes usar el argumento <@var="ruta"> para limitar los elementos JSON incluidos en el 'bundle' que se devuelve. Ten en cuenta que esto no es un “JsonPath” tal como se describe en la ayuda para <@ref="jsonget">; esto es una sencilla composición sujeta a la siguiente especificación: 

<indent>
• <@var="ruta"> es una formación de elementos separados por una barra, donde esta barra (“/”) indica el desplazamiento a un nivel “más bajo” en el árbol JSON representado por <@var="buf">. Se permite una barra inicial pero no es necesaria, pues implícitamente la ruta siempre comienza en la raíz. No debes incluir caracteres extraños para espacios en blanco. 
</indent>

<indent>
• Cada elemento que se separa con una barra debe tener una de las siguientes formas: (a) un nombre únicamente, en cuyo caso solo se va a incluir un elemento JSON cuyo nombre coincida en el nivel estructural indicado; o (b) “*” (asterisco), en cuyo caso se van a incluir todos aquellos elementos del nivel indicado; o (c) un 'array' de nombres separados con comas y delimitados por llaves (“{” y “}”), en cuyo caso solo se van a incluir los elementos JSON cuyos nombres coincidan con uno de los nombres indicados. 
</indent>

Consulta también la función orientada a cadenas <@ref="jsonget">; pues, dependiendo de tu intención, una de estas funciones puede serte de más ayuda que la otra. 

# juldate calendar
Resultado: 	mira más abajo 
Argumentos:	<@var="ed">  (escalar, serie o matriz)
		<@var="como-cadena">  (booleano, opcional)

Esta funcion simplemente opera como <@ref="isodate">, excepto en que las fechas del resultado son relativas al calendario Juliano en vez del Gregoriano. 

# kdensity nonparam
Resultado: 	matriz 
Argumentos:	<@var="x">  (serie, lista o matriz)
		<@var="escala">  (escalar, opcional)
		<@var="control">  (booleano, opcional)

Calcula una estimación (o un conjunto de estimaciones) de la densidad kernel para el argumento <@var="x">, que puede ser una serie única, una lista o una matriz con más de una columna. La matriz que se devuelve tiene <@mth="k"> + 1 columnas, siendo <@mth="k"> el número de elementos (series o columnas) de <@var="x">. La primera columna incluye un conjunto de abscisas equidistantes, y el resto de las columnas incluyen la densidad (o densidades) estimada correspondiente a cada una de ellas. 

La fórmula utilizada para calcular la densidad estimada en cada punto de referencia (<@mth="x">) es 

  <@fig="kernel1">

donde <@mth="n"> denota el número de puntos con datos, <@mth="h"> es un parámetro de “ancho de banda”, y <@mth="k">() es la función Kernel. Cuanto mayor sea el valor del parámetro de ancho de banda, más suave va a ser la densidad estimada. 

El parámetro <@var="escala"> (opcional) puedes usarlo para ajustar el grado de suavizado en relación al valor por defecto que es 1.0; este se corresponde con la regla general propuesta por <@bib="Silverman (1986);silverman86">, concretamente 

  <@fig="kernel2">

donde <@mth="s"> indica la desviación típica de los datos e IQR es el rango intercuartil. El parámetro <@var="control"> (opcional) actúa como un booleano: 0 (valor por defecto) significa que se utiliza el kernel gaussiano; un valor no nulo cambia al kernel de Epanechnikov. 

Puedes obtener un gráfico de los resultados utilizando la instrucción <@xrf="gnuplot">, como se indica abajo. Ten en cuenta que la columna que contiene las abscisas debe ir al final para representar el gráfico. 

<code>          
     matrix d = kdensity(x)
     # Si x tiene un único elemento
     gnuplot 2 1 --matrix=d --with-lines --fit=none
     # Si x tiene dos elementos
     gnuplot 2 3 1 --matrix=d --with-lines --fit=none
</code>

# kdsmooth sspace
Resultado: 	entero 
Argumentos:	<@var="&kb">  (referencia a bundle)
		<@var="MSE">  (booleano, opcional)

Realiza el suavizado de las perturbaciones de un 'bundle' de Kalman, configurado previamente mediante la instrucción <@ref="ksetup">; y devuelve el entero 0 cuando se completa con éxito, o un número no nulo cuando se encuentran problemas numéricos. Y deberías comprobar el valor que se devuelve, antes de hacer uso de los resultados. 

Cuando se completa con éxito la operación, las perturbaciones suavizadas van a estar disponibles como <@lit="kb.smdist">. 

El argumento <@var="MSE"> (opcional) determina el contenido de la clave <@lit="kb.smdisterr">. Cuando es 0 o se omite, esta matriz va a estar compuesta por las desviaciones típicas incondicionales de las perturbaciones suavizadas, que habitualmente se utilizan para calcular los denominados <@itl="errores auxiliares">. Pero, en caso contrario, <@lit="kb.smdisterr"> va a contener las raíces de las desviaciones cuadradas medias entre los errores auxiliares y sus valores verdaderos. 

Para obtener más detalles, consulta <@pdf="El manual de gretl#chap:kalman"> (Capítulo 36). 

Ver también <@ref="ksetup">, <@ref="kfilter">, <@ref="ksmooth">, <@ref="ksimul">. 

# kfilter sspace
Resultado: 	escalar 
Argumento: 	<@var="&kb">  (referencia a bundle)

Realiza el filtrado hacia adelante de un 'bundle' de Kalman configurado previamente mediante la instrucción <@ref="ksetup">, y devuelve el escalar 0 cuando se completa con éxito, o el escalar 1 cuando se encuentran problemas numéricos. 

Cuando se completa con éxito, los errores de predicción adelantados un paso van a estar disponibles como <@lit="kb.prederr">, y la secuencia de sus matrices de covarianzas como <@lit="kb.pevar">. Por otro lado, <@lit="kb.llt"> permitirá que tengas acceso a un <@mth="T">-vector que va a contener el logaritmo de la verosimilitud de cada observación. 

Para obtener más detalles, consulta <@pdf="El manual de gretl#chap:kalman"> (Capítulo 36). 

Ver también <@ref="kdsmooth">, <@ref="ksetup">, <@ref="ksmooth">, <@ref="ksimul">. 

# kmeier nonparam
Resultado: 	matriz 
Argumentos:	<@var="d">  (serie o vector)
		<@var="cens">  (serie o vector, opcional)

Devuelve una matriz con el cálculo del estimador no paramétrico de Kaplan–Meier de la función de supervivencia (<@bib="Kaplan y Meier, 1958;kaplan-meier">), dada una muestra <@var="d"> de datos de duración, posiblemente acompañada de un registro de estado de censura, <@var="cens">. La matriz que se devuelve tiene tres columnas que contienen, respectivamente: los valores únicos ordenados en <@var="d">, la estimación de la función de supervivencia que se corresponde con los valores de duración de la columna 1, y la desviación típica (para muestras grandes) del estimador, calculados mediante el método de <@bib="Greenwood (1926);greenwood26">. 

Cuando indicas la serie <@var="cens">, se utiliza el valor 0 para señalar que una observación no está censurada, mientras que el valor 1 indica que una observación está censurada del lado derecho (es decir, el período de observación del individuo en cuestión concluyó antes de la duración, o el período se registró como finalizado). Cuando no indicas <@var="cens">, se asume que todas las observaciones son no censuradas. (Aviso: la semántica de <@var="cens"> puede extenderse en algún punto para cubrir otros tipos de censura.) 

Ver también <@ref="naalen">. 

# kpsscrit stats
Resultado: 	matriz 
Argumentos:	<@var="T">  (escalar)
		<@var="tendenc">  (booleano)

Devuelve un vector fila que contiene los valores críticos a los niveles de 10, 5 y 1 por ciento del contraste KPSS para la estacionariedad de una serie temporal. El argumento <@var="T"> debe indicar el número de observaciones, y el argumento <@var="tendenc"> debe ser igual a 1 si el contraste incluye una constante (o 0 en caso contrario). 

Los valores críticos que se ofrecen están basados en superficies de respuesta estimadas del modo que está establecido por <@bib="Sephton (Economics Letters,1995);sephton95">. Consulta también la instrucción <@xrf="kps">. 

# ksetup sspace
Resultado: 	bundle 
Argumentos:	<@var="Y">  (serie, matriz o lista)
		<@var="Z">  (escalar o matriz)
		<@var="T">  (escalar o matriz)
		<@var="Q">  (escalar o matriz)
		<@var="R">  (matriz, opcional)

Configura un 'bundle' de Kalman, es decir, un objeto que contiene toda la información necesaria para definir un modelo de espacio de los estados lineal, de la forma 

  <@fig="kalman1">

en la que Var<@mth="(u) = R">, y con la ecuación de transición de estado 

  <@fig="kalman2">

en la que Var<@mth="(v) = Q">. 

Los objetos que creas mediante esta función puedes utilizalos más adelante, con la intervención de las siguientes funciones específicas: <@ref="kfilter"> para hacer filtrado, <@ref="ksmooth"> y <@ref="kdsmooth"> para suavizado, y <@ref="ksimul"> para hacer simulaciones. 

En realidad, el tipo de modelos que GRETL puede manejar es mucho más amplio que el implicado en la anterior representación: es posible disponer de modelos variantes en el tiempo, de modelos con precedentes difusos y con variable exógena en la ecuación de medida, y de modelos con innovaciones con correlaciones cruzadas. Para obtener más detalles, consulta <@pdf="El manual de gretl#chap:kalman"> (Capítulo 36). 

Ver también <@ref="kdsmooth">, <@ref="kfilter">, <@ref="ksmooth">, <@ref="ksimul">. 

# ksimul sspace
Resultado: 	matriz 
Argumentos:	<@var="&kb">  (referencia a bundle)
		<@var="U">  (matriz)
		<@var="extra">  (booleano, opcional)

Devuelve una matriz. Utiliza un 'bundle' de tipo Kalman previamente definido con la función <@ref="ksetup"> para llevar a cabo simulaciones, tomando las perturbaciones de la matriz <@var="U">. Por defecto, la matriz que se devuelve (que tendrá tantas filas como <@var="U">) contiene valores simulados de lo(s) observable(s), pero cuando indicas un valor no nulo para <@var="extra">, también se va a incluir el estado simulado. En este último caso, cada fila contiene primero el estado, y después lo(s) observable(s). 

Para obtener más detalles, consulta <@pdf="El manual de gretl#chap:kalman"> (Capítulo 36). 

Ver también <@ref="ksetup">, <@ref="kfilter">, <@ref="ksmooth">. 

# ksmooth sspace
Resultado: 	entero 
Argumento: 	<@var="&kb">  (referencia a bundle)

Realiza un suavizado de punto fijo (hacia atrás) de un 'bundle' de Kalman previamente configurado mediante <@ref="ksetup">; y devuelve un 0 cuando se ejecuta con éxito, o un número no nulo cuando se encuentran problemas numéricos. Y deberías comprobar el valor que se devuelve, antes de hacer uso de los resultados. 

Cuando se completa con éxito, vas a tener a tu disposición el estado ya suavizado como <@lit="kb.state">, y la secuencia de sus matrices de varianzas-covarianzas como <@lit="kb.stvar">. Para obtener más detalles, consulta <@pdf="El manual de gretl#chap:kalman"> (Capítulo 36). 

Ver también <@ref="ksetup">, <@ref="kdsmooth">, <@ref="kfilter">, <@ref="ksimul">. 

# kurtosis stats
Resultado: 	escalar 
Argumento: 	<@var="x">  (serie)

Devuelve el exceso de curtosis de la serie <@var="x">, descartando cualquier observación ausente. 

# lags transforms
Resultado: 	lista o matriz 
Argumentos:	<@var="p">  (escalar o vector)
		<@var="y">  (serie, lista o matriz)
		<@var="xretardo">  (booleano, opcional)

Cuando el primer argumento es un escalar, genera los retardos del 1 al <@var="p"> de la serie <@var="y">. Cuando <@var="y"> es una lista, genera esos retardos para todas las series que contiene esa lista. Cuando <@var="y"> es una matriz, genera esos retardos para todas las columnas de la matriz. En caso de que <@var="p"> = 0, e <@var="y"> sea una serie o una lista, el retardo máximo toma por defecto la periodicidad de los datos; aparte de eso <@var="p"> deberá ser positivo. 

Cuando el primer argumento es un vector, los retardos generados son los que están especificados en ese vector. En este caso, un uso habitual podría ser el de poner, por ejemplo, <@var="p"> como <@lit="seq(3,7)">, omitiendo entonces el primer y segundo retardos. Así y todo, también es correcto indicar un vector con saltos como en <@lit="{3,5,7}">, aunque los retardos deberán indicarse siempre en orden ascendente. 

En caso de que el resultado sea una lista, se nombran automáticamente las variables generadas con el patrón <@var="nombrevar"><@lit="_"><@var="i">, en el que <@var="nombrevar"> estará indicando el nombre de la serie original, e <@var="i"> expresará el retardo concreto de cada caso. La parte original del nombre se va a truncar cuando así resulte necesario, e incluso podrá ajustarse oportunamente para garantizar que resulte único dentro del conjunto de nombres que así se vayan a construir. 

Cuando el segundo argumento <@var="y"> es una lista o una matriz con más de una columna, y el nivel de retardo es mayor que 1, la disposición por defecto de los elementos en la lista que se devuelve es por orden de variable: primero se devuelven todos los retardos de la primera serie o columna contenida en ese argumento, seguidos de todos los de la segunda, y así sucesivamente. El tercer argumento (opcional) puedes usarlo para cambiar esto: si <@var="xretardo"> es no nulo, entonces los elementos se ordenan por retardo: el primer retardo de todas las series o columnas, después el segundo retardo de todas las series o columnas, etc. 

Consulta también <@ref="mlag"> para la utilización con matrices. 

# lastobs data-utils
Resultado: 	entero 
Argumentos:	<@var="y">  (serie)
		<@var="enmuestra">  (booleano, opcional)

Devuelve el número entero positivo que indexa la última observación no ausente de la serie <@var="y">. Por defecto, se analiza todo el rango de la muestra, de forma que, si está activa alguna forma de submuestreo, el valor que se devuelve puede ser mayor que el valor devuelto por el accesor <@ref="$t2">. Pero si indicas un valor no nulo en <@var="enmuestra">, solo se va a tener en cuenta el rango de la muestra vigente. Ver también <@ref="firstobs">. 

# ldet linalg
Resultado: 	escalar 
Argumento: 	<@var="A">  (matriz cuadradax)

Devuelve un escalar con el logaritmo natural del determinante de <@mth="A">, calculado mediante la descomposición LU. Ten en cuenta que esto es más eficiente que invocar <@ref="det"> y tomar el logaritmo del resultado. Además, en algunos casos <@lit="ldet"> es capaz de devolver un resultado válido incluso cuando el determinante de <@mth="A"> es numéricamente “infinito” (excediendo el número máximo de doble precisión de la librería de C). Ver también <@ref="rcond">, <@ref="cnumber">. 

# ldiff transforms
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="y">  (serie o lista)

Devuelve un resultado (del tipo del argumento) con las primeras diferencias del logaritmo de este; los valores iniciales se consideran <@lit="NA">. 

Cuando se devuelve una lista, las variables individuales se nombran de forma automática siguiendo el patrón <@lit="ld_"><@var="varname">, en el que <@var="varname"> indica el nombre de la serie original. La parte original del nombre se va a truncar cuando así resulte necesario, e incluso podrá ajustarse para garantizar que sea único dentro del conjunto de nombres que así se vayan a construir. 

Ver también <@ref="diff">, <@ref="sdiff">. 

# lincomb transforms
Resultado: 	serie 
Argumentos:	<@var="L">  (lista)
		<@var="b">  (vector)

Devuelve una nueva serie calculada como una combinación lineal de las series de la lista <@var="L">. Los coeficientes vienen dados por el vector <@var="b">, cuyo tamaño debe ser igual al número de series que hay en <@var="L">. 

Ver también <@ref="wmean">. 

# linearize transforms
Resultado: 	serie 
Argumento: 	<@var="x">  (serie)

Para ejecutarlo es preciso tener instalado el TRAMO. Devuelve una serie que es una versión “linealizada” del argumento; es decir, una serie en la que cualquier valor ausente se substituye por valores interpolados, y en la que las observaciones anómalas se ajustan. Para eso se utiliza un mecanismo completamente automático del TRAMO. Para obtener más detalles, consulta la documentación del TRAMO. 

Ten en cuenta que, si la serie del argumento no posee valores ausentes ni observaciones que el TRAMO considere anómalas, esta función devuelve una copia de la serie original. 

# ljungbox stats
Resultado: 	escalar 
Argumentos:	<@var="y">  (serie)
		<@var="p">  (entero)

Devuelve un escalar con el cálculo del estadístico Q de Ljung–Box para la serie <@var="y">, utilizando el nivel de retardo <@var="p">, a lo largo de la muestra seleccionada en ese momento. El nivel de retardo debe ser mayor o igual a 1, y menor que el número de observaciones disponibles. 

Ese valor del estadístico puedes cotejarlo con la distribución Chi-cuadrado con <@var="p"> grados de libertad, para verificar la hipótesis nula de que la serie <@var="y"> no tiene autocorrelación. Ver también <@ref="pvalue">. 

# lngamma math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el logaritmo de la función Gamma de <@var="x">. 

Consulta también <@ref="bincoeff"> y <@ref="gammafun">. 

# loess nonparam
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="x">  (serie)
		<@var="d">  (entero, opcional)
		<@var="q">  (escalar, opcional)
		<@var="robusta">  (booleano, opcional)

Realiza una regresión polinómica ponderada localmente, y devuelve una serie que contiene los valores previstos de <@var="y"> para cada valor no ausente de <@var="x">. El método que se utiliza es del tipo que está descrito por <@bib="William Cleveland (1979);cleveland79">. 

Los argumentos <@var="d"> y <@var="q"> (opcionales) permiten especificar: el grado del polinomio de <@var="x"> y que proporción de los puntos de datos se van a utilizar en la estimación local, respectivamente. Los valores que se les suponen por defecto son <@var="d"> = 1 y <@var="q"> = 0.5; y otros valores admisibles para <@var="d"> son 0 y 2. Cuando establezcas <@var="d"> = 0, vas a reducir la regresión local a una forma de media móvil. El valor de <@var="q"> debe de ser mayor que 0, y no puede ser mayor que 1; los valores más grandes producen un resultado final más suavizado. 

Cuando se especifica un valor no nulo para el argumento <@var="robusta">, las regresiones locales se reiteran dos veces, con modificaciones en las ponderaciones en base a los errores de la iteración previa, y de modo que tengan menos influencia las observaciones anómalas. 

Revisa también la función <@ref="nadarwat"> y, por añadido, consulta <@pdf="El manual de gretl#chap:nonparam"> (Capítulo 40) para obtener más detalles sobre métodos no paramétricos. 

# log math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie, matriz o lista)

Devuelve un resultado (del tipo del argumento) con el logaritmo natural de <@var="x">, generando <@lit="NA"> si este no es positivo. Aviso: <@lit="ln"> es un seudónimo admisible para <@lit="log">. 

Cuando se devuelve una lista, las variables individuales se nombran de forma automática siguiendo el patrón <@lit="l_"><@var="varname">, en el que <@var="varname"> indica el nombre de la serie original. La parte original del nombre va a truncarse cuando así resulte necesario, e incluso podrá ajustarse para garantizar que sea único dentro del conjunto de nombres que así se vayan a construir. 

Observa que, en caso de que el argumento sea una matriz, la función opera elemento a elemento. Para la función logarítmica matricial, consulta <@ref="mlog">. 

# log10 math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el logaritmo en base 10 de <@var="x">, generando <@lit="NA"> si este no es positivo. 

# log2 math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el logaritmo en base 2 de <@var="x">, generando <@lit="NA"> si este no es positivo. 

# logistic math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del mismo tipo que el argumento <@var="x">) con la función FDA logística de este; es decir, 1/(1+<@mth="e"><@sup="–x">). Si <@var="x"> es una matriz, la función se aplica a cada elemento. 

# lpsolve math
Resultado: 	bundle 
Argumento: 	<@var="specs">  (bundle)

Soluciona un problema de programación lineal, utilizando la biblioteca lpsolve. Consulta <@adb="gretl-lpsolve.pdf"> para obtener más detalles y ejemplos de uso. 

# lower matrix
Resultado: 	matriz cuadradax 
Argumento: 	<@var="A">  (matriz)

Devuelve una matriz triangular inferior de orden <@itl="n">×<@itl="n">: los elementos de la diagonal principal y de debajo de esta son iguales a los elementos correspondientes de <@var="A">, y los demás son iguales a cero. 

Ver también <@ref="upper">. 

# lrcovar timeseries
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz)
		<@var="sinmedia">  (booleano, opcional)

Devuelve una matriz con las varianzas y covarianzas de largo plazo de las columnas de la matriz <@var="A">. Primero, a los datos se les resta la media, excepto que se asigne un cero al segundo argumento (opcional). Puedes escoger el tipo de kernel y el parámetro de truncado del retardo (el tamaño de la ventana), antes de llamar a esta función mediante las opciones relacionadas con el HAC que ofrece la instrucción <@xrf="set">, tales como <@lit="hac_kernel">, <@lit="hac_lag">, o <@lit="hac_prewhiten">. Consulta también la sección sobre datos de series de tiempo y matrices de covarianzas HAC en <@pdf="El manual de gretl#chap:robust_vcv"> (Capítulo 22). 

Ver también <@ref="lrvar">. 

# lrvar timeseries
Resultado: 	escalar 
Argumentos:	<@var="y">  (serie o vector)
		<@var="k">  (entero, opcional)
		<@var="mu">  (escalar, opcional)

Devuelve un escalar con la varianza de largo plazo del argumento <@var="y">, calculada usando un núcleo (“kernel”) de Bartlett con tamaño de ventana igual a <@var="k">. Si omites el segundo argumento (o le asignas un valor negativo), el tamaño de la ventana se establece por defecto igual a la parte entera de la raíz cúbica del tamaño de la muestra. 

Para el cálculo de la varianza, la serie <@var="y"> se centra con respecto al parámetro opcional <@var="mu">; y cando este se omite o es <@lit="NA">, se utiliza la media muestral. 

Para una contrapartida multivariante, consulta <@ref="lrcovar">. 

# Lsolve linalg
Resultado: 	matriz 
Argumentos:	<@var="L">  (matriz)
		<@var="b">  (matriz)

Soluciona <@mth="x"> en <@mth="Ax = b">, donde <@var="L"> es el factor de Cholesky triangular inferior de la matriz definida positiva <@mth="A">, que cumple <@mth="LL' = A">. Puedes obtener un <@var="L"> apropiado utilizando la función <@ref="cholesky"> con <@mth="A"> como argumento. 

Los siguientes dos cálculos deberían producir el mismo resultado (dependiendo de la precisión de la máquina), pero la primera variante permite la reutilización de un factor de Cholesky calculado previamente, y por lo tanto debería ser substancialmente más rápido si estás solucionando de forma repetida para una misma <@mth="A">, y distintos valores de <@mth="b">. El aumento de velocidad será mayor, cuanto mayor sea la dimensión de <@mth="A">. 

<code>          
     # Variante 1
     matrix L = cholesky(A)
     matrix x = Lsolve(L, b)
     # Variante 2
     matrix x = A \ b
</code>

# mat2list data-utils
Resultado: 	lista 
Argumentos:	<@var="X">  (matriz)
		<@var="prefijo">  (cadena, opcional)

Esta es una función conveniente para elaborar una lista de series utilizando las columnas de una matriz apropiada como entrada. La dimensión de las filas de <@var="X"> debe ser igual a la longitud del conjunto de datos vigente, o al número de observaciones del rango de la muestra vigente. 

Las series de la lista que se devuelve se nombran del siguiente modo. Primero, cuando se proporciona el argumento opcional <@var="prefijo">, la serie creada de la columna <@mth="i"> de <@var="X"> se nombra añadiendo <@mth="i"> a la cadena de texto proporcionada, como en <@lit="prefijo1">, <@lit="prefijo2">, etcétera. En caso contrario, si la matriz <@var="X"> tiene un conjunto de nombres de las columnas (consulta <@ref="cnameset">), se utilizan esos nombres. Finalmente, si no se cumple ninguna de las condiciones anteriores, los nombres son <@lit="columna1">, <@lit="columna2">, etcétera. Ten en cuenta que esta política puede tener como consecuencia que se sobrescriban series ya existentes; si no quieres que ello suceda, toma la precaución de nombrar las columnas explícitamente mediante la función <@lit="cnameset">, o indica la opción <@var="prefix">. 

Aquí tienes un ejemplo ilustrativo de su uso: 

<code>          
     matrix X = mnormal($nobs, 8)
     list L = mat2list(X, "xnorm")
     # o alternativamente, si no necesitas crear la propia X
     list L = mat2list(mnormal($nobs, 8), "xnorm")
</code>

Esto va a añadir al conjunto de datos, ocho series de longitud completa nombradas <@lit="xnorm1">, <@lit="xnorm2">, etcétera. 

# max stats
Resultado: 	depende de la entrada 
Argumentos:	<@var="x">  (escalar, serie o matriz)
		<@var="y">  (escalar, serie o matriz, opcional)

Esta función tiene dos modos básicos, más un caso especial. 

El primer modo se activa cuando se indica un único argumento de tipo escalar, de tipo serie o de tipo matriz. El valor que se va a devolver es un escalar, que expresa el valor válido máximo “dentro” del argumento: si <@var="x"> es una serie, expresa su valor máximo dentro del rango vigente de la muestra; o, si <@var="x"> es una matriz, su mayor elemento (ignorándose los valores ausentes). En caso de que el argumento sea escalar se admite por razones de exhaustividad; simplemente se va a obtener de vuelta su mismo valor. 

El segundo modo se activa cando se indican dos argumentos. Los argumentos <@var="x"> e <@var="y"> deben ser del mismo tipo; y deben ser escalares, series o matrices (si son matrices, deben tener la misma dimensión). El valor que se devuelve es un objeto del mismo tipo que los argumentos, que contiene el máximo (o máximos) “entre” o “transversales”. Cuando los argumentos son escalares, obtienes el mayor de los dos; si son series, obtienes una serie constituida por los valores que resultan ser los mayores al comparar los dos valores de las series para cada observación, en el rango vigente de la muestra; si son matrices, obtienes una matriz que contiene los valores que resultan ser los mayores al comparar los dos valores de las matrices para cada fila y columna. Para cada una de las comparaciones por parejas, si alguno de los términos está ausente, el resultado es también un valor ausente. 

<subhead>El caso especial</subhead> 

Este aparece cuando se indica un argumento con una única lista. El valor que se devuelve es una serie que contiene, en cada observación del rango vigente de la muestra, el mayor de los valores que tienen las series de la lista, en cada observación. 

Ver también <@ref="min">. 

# maxc stats
Resultado: 	vector fila 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector fila que contiene el valor más grande de cada columna de la matriz <@var="X">. Para columnas que tengan valores <@lit="NA">s, el resultado también se establece como <@lit="NA">, excepto cuando no sea cero el argumento opcional <@var="obviar_na">, en cuyo caso se devuelve la mayor entrada válida. 

Ver también <@ref="imaxc">, <@ref="maxr">, <@ref="minc">. 

# maxr stats
Resultado: 	vector columna 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector columna que contiene el valor más grande de cada fila de la matriz <@var="X">. Para filas que tengan valores <@lit="NA">s, el resultado también se establece como <@lit="NA">, excepto cuando no sea cero el argumento opcional <@var="obviar_na">, en cuyo caso se devuelve la mayor entrada válida. 

Ver también <@ref="imaxc">, <@ref="maxc">, <@ref="minr">. 

# mcorr stats
Resultado: 	matriz 
Argumento: 	<@var="X">  (matriz)

Calcula una matriz de correlaciones (de Pearson), tratando cada columna de la matriz argumento <@var="X"> como si fuese una variable. Ver también <@ref="corr">, <@ref="cov">, <@ref="mcov">. 

# mcov stats
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="gl_corr">  (entero, opcional)

Calcula una matriz de varianzas-covarianzas, tratando cada columna de la matriz argumento <@var="X"> como si fuese una variable. El divisor es <@mth="n"> – 1, en el que <@mth="n"> es el número de filas de <@var="X">; excepto que indiques el segundo argumento (opcional), en cuyo caso se utiliza <@mth="n"> – <@var="gl_corr">. 

Ver también <@ref="corr">, <@ref="cov">, <@ref="mcorr">. 

# mcovg stats
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="u">  (vector, opcional)
		<@var="w">  (vector, opcional)
		<@var="p">  (entero)

Devuelve la matriz covariograma para otra matriz <@var="X"> de orden <@itl="T">×<@itl="k"> (que generalmente contiene regresores), un vector <@var="u"> de orden <@mth="T"> (opcional, que suele contener los errores), un vector <@var="w"> de orden <@mth="p">+1 (opcional, que contiene unas ponderaciones), y un número entero <@var="p"> que indica el nivel de retardo y debe ser mayor o igual a 0. 

La matriz que se devuelve es la suma para <@mth="j"> desde <@mth="-p"> hasta <@mth="p"> de <@mth="w(|j|) * X(t)X(t-j)' * u(t)u(t-j)">, donde <@mth="X(t)'"> es la <@mth="t">-ésima fila de <@var="X">. 

Si <@var="u"> viene indicado como <@lit="nulo">, los términos <@mth="u"> se omiten, y si <@var="w"> viene indicado como <@lit="nulo">, todas las ponderaciones se asume que son 1.0. 

Por ejemplo, el siguiente trozo de código 

<code>          
     set seed 123
     X = mnormal(6,2)
     Retardo = mlag(X,1)
     Adelanto = mlag(X,-1)
     print X Retardo Adelanto
     eval X'X
     eval mcovg(X, , , 0)
     eval X'(X + Retardo + Adelanto)
     eval mcovg(X, , , 1)
</code>

produce este resultado: 

<code>          
     ? print X Retardo Adelanto
     X (6 x 2)

       -0,76587      -1,0600
       -0,43188      0,30687
       -0,82656      0,40681
        0,39246      0,75479
        0,36875       2,5498
        0,28855     -0,55251

     Retardo (6 x 2)

         0,0000       0,0000
       -0,76587      -1,0600
       -0,43188      0,30687
       -0,82656      0,40681
        0,39246      0,75479
        0,36875       2,5498

     Adianto (6 x 2)

       -0,43188      0,30687
       -0,82656      0,40681
        0,39246      0,75479
        0,36875       2,5498
        0,28855     -0,55251
         0,0000       0,0000

     ? eval X'X
         1,8295       1,4201
         1,4201       8,7596

     ? eval mcovg(X,,, 0)
         1,8295       1,4201
         1,4201       8,7596

     ? eval X'(X + Retardo + Adianto)
         3,0585       2,5603
         2,5603       10,004

     ? eval mcovg(X,,, 1)
         3,0585       2,5603
         2,5603       10,004
</code>

# mean stats
Resultado: 	escalar o serie 
Argumentos:	<@var="x">  (serie o lista)
		<@var="parcial">  (booleano, opcional)

Si <@var="x"> es una serie, la función devuelve un escalar con su media muestral, ignorando cualquier observación ausente. 

Si <@var="x"> es una lista, la función devuelve una serie <@mth="y"> tal que <@mth="y"><@sub="t"> indica la media de los valores de las variables de esa lista en la observación <@mth="t">. Por defecto, la media se registra como <@lit="NA">, si hay algún valor ausente en <@mth="t">; pero si le das un valor no nulo a <@var="parcial">, cualquier valor no ausente se usará para crear el estadístico. 

El siguiente ejemplo ilustra el funcionamiento de la función: 

<code>          
     open denmark.gdt
     eval mean(LRM)
     list L = dataset
     eval mean(L)
</code>

La primera solicitud devolverá un escalar con el valor medio de la serie <@var="LRM">, y la segunda devolverá una serie. 

Ver también <@ref="median">, <@ref="sum">, <@ref="max">, <@ref="min">, <@ref="sd">, <@ref="var">. 

# meanc stats
Resultado: 	vector fila 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector fila con cada una de las medias de cada columna de <@var="X">. Si indicas un valor no nulo para el segundo argumento (opcional), se ignoran los valores ausentes; de otra forma, el resultado es <@lit="NA"> para cada columna que contenga valores ausentes. 

Por ejemplo, la siguiente porción de código... 

<code>          
     matrix m = mnormal(5, 2)
     m[1,2] = NA
     print m
     eval meanc(m)
</code>

genera este resultado: 

<code>          
     ? print m
     m (5 x 2)

      -0,098299          nan
         1,1829      -1,2817
        0,46037     -0,92947
         1,4896     -0,91970
        0,91918      0,47748

     ? eval meanc(m)
        0,79075          nan
</code>

Ver también <@ref="meanr">, <@ref="sumc">, <@ref="maxc">, <@ref="minc">, <@ref="sdc">, <@ref="prodc">. 

# meanr stats
Resultado: 	vector columna 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector columna con cada una de las medias de cada fila de <@var="X">. Si indicas un valor no nulo para el segundo argumento (opcional), se ignoran los valores ausentes; de otra forma, el resultado es <@lit="NA"> para cada fila que contenga valores ausentes. Ver también <@ref="meanc">, <@ref="sumr">. 

# median stats
Resultado: 	escalar o serie 
Argumento: 	<@var="x">  (serie o lista)

Si <@var="x"> es una serie, la función devuelve un escalar con su mediana muestral, ignorando cualquier observación ausente. 

Si <@var="x"> es una lista, la función devuelve una serie <@mth="y"> tal que <@mth="y"><@sub="t"> indica la mediana de los valores de las variables de esa lista en la observación <@mth="t">, o <@lit="NA"> en caso de que exista algún valor ausente en <@mth="t">. 

El siguiente ejemplo ilustra el funcionamiento de la función: 

<code>          
     set verbose off
     open denmark.gdt
     eval median(LRM)
     list L = dataset
     series m = median(L)
</code>

La primera solicitud devolverá un escalar con el valor mediano de la serie <@var="LRM">, y la segunda devolverá una serie. 

Ver también <@ref="mean">, <@ref="sum">, <@ref="max">, <@ref="min">, <@ref="sd">, <@ref="var">. 

# mexp linalg
Resultado: 	matriz cuadradax 
Argumento: 	<@var="A">  (matriz cuadradax)

Calcula la matriz exponencial de una matriz cuadrada <@var="A">. Si <@var="A"> es una matriz real, se utiliza para ello el algoritmo 11.3.1 de <@bib="Golub y Van Loan (1996);golub96">. Si <@var="A"> es una matriz compleja, el algoritmo utiliza la descomposición en autovalores y <@var="A"> debe ser diagonalizable. 

Consulta también <@ref="mlog">. 

# mgradient midas
Resultado: 	matriz 
Argumentos:	<@var="p">  (entero)
		<@var="theta">  (vector)
		<@var="tipo">  (entero o cadena)

Derivadas analíticas para las ponderaciones de un MIDAS. Denotando como <@mth="k"> al número de elementos que componen el vector <@var="theta"> de hiperparámetros, esta función devuelve una matriz de orden <@itl="p">×<@itl="k">, que contiene el gradiente del vector de ponderaciones (tal como lo calcula la función <@ref="mweights">) con respecto a los elementos de <@var="theta">. El primer argumento representa el nivel de retardo deseado, y el último argumento especifica el tipo de disposición de parámetros. Consulta la función <@lit="mweights"> para tener una relación de los valores admisibles para <@var="tipo">. 

Ver también <@ref="midasmult">, <@ref="mlincomb">, <@ref="mweights">. 

# midasmult midas
Resultado: 	matriz 
Argumentos:	<@var="mod">  (bundle)
		<@var="acumular">  (booleano)
		<@var="v">  (entero)

Devuelve el cálculo de los multiplicadores MIDAS. El argumento <@var="mod"> debe ser un 'bundle' que incluya un modelo MIDAS, del tipo que se genera mediante la instrucción <@xrf="midasreg"> y que es accesible por medio de la clave <@ref="$model">. La función devuelve una matriz con los multiplicadores implícitos MIDAS para la variable <@var="v"> en la primera columna, y las desviaciones típicas correspondientes en la segunda columna. Si el argumento <@var="acumular"> no es cero, los multiplicadores se acumulan. 

Observa que automáticamente se proporciona la matriz que se devuelve con etiquetas adecuadas para las filas, de forma que resultan indicadas para usar como primer argumento de la instrucción <@xrf="modprint">. Por ejemplo, el código 

<code>          
     open gdp_midas.gdt
     list dIP = ld_indpro*
     smpl 1985:1 ;
     midasreg ld_qgdp 0 ; mds(dIP, 0, 6, 2)
     matrix ip_m = midasmult($model, 0, 1)
     modprint ip_m
</code>

genera el siguiente resultado: 

<code>          
             Coeficiente   Desv. típica     z       Valor p
  ---------------------------------------------------------
  dIP_0      0,343146      0,0957752     3,583     0,0003   ***
  dIP_1      0,402547      0,0834904     4,821     1,43e-06 ***
  dIP_2      0,176437      0,0673776     2,619     0,0088   ***
  dIP_3      0,0601876     0,0621927     0,9678    0,3332
  dIP_4      0,0131263     0,0259137     0,5065    0,6125
  dIP_5      0,000965260   0,00346703    0,2784    0,7807
  dIP_6      0,00000       0,00000      NA        NA
</code>

Ver también <@ref="mgradient">, <@ref="mweights">, <@ref="mlincomb">. 

# min stats
Resultado: 	depende de la entrada 
Argumentos:	<@var="x">  (escalar, serie o matriz)
		<@var="y">  (escalar, serie o matriz)

Por favor, consulta la ayuda para <@ref="max">; esta función opera exactamente del mismo modo, excepto porque devuelve el mínimo o mínimos. 

# minc stats
Resultado: 	vector fila 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector fila que contiene el valor más pequeño de cada columna de la matriz <@var="X">. Para columnas que tengan valores <@lit="NA">s, el resultado también se establece como <@lit="NA">, excepto cuando no sea cero el argumento opcional <@var="obviar_na">, en cuyo caso se devuelve la menor entrada válida. 

Ver también <@ref="iminc">, <@ref="maxc">, <@ref="minr">. 

# minr stats
Resultado: 	vector columna 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector columna que contiene el valor más pequeño de cada fila de la matriz <@var="X">. Para filas que tengan valores <@lit="NA">s, el resultado también se establece como <@lit="NA">, excepto cuando no sea cero el argumento opcional <@var="obviar_na">, en cuyo caso se devuelve la menor entrada válida. 

Ver también <@ref="iminr">, <@ref="maxr">, <@ref="minc">. 

# missing data-utils
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o lista)

Devuelve una variable binaria (del mismo tipo que el argumento) que toma el valor 1 cuando <@var="x"> es <@lit="NA">. Si ese argumento es una serie, se hace la comprobación para cada elemento. Cuando sea <@var="x"> una lista de series, devuelve una serie que toma el valor 1 en las observaciones en las que al menos una de las series presenta un valor ausente, y el 0 en otro caso. Por ejemplo, el siguiente código 

<code>          
    nulldata 3
    series x = normal()
    x[2] = NA
   	series x_ismiss = missing(x)
   	print x x_ismiss --byobs
</code>

establece un valor ausente en la segunda observación de <@var="x">, y crea una nueva serie booleana <@var="x_ismiss"> que identifica la observación ausente. 

<code>          
   	             y     y_ismiss

   	1    -1,551247            0
   	2                         1
   	3    -2,244616            0
</code>

Ver también <@ref="misszero">, <@ref="ok">, <@ref="zeromiss">. 

# misszero data-utils
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado del tipo del argumento, cambiando los <@lit="NA">s por ceros. Si <@var="x"> es una serie o una matriz, se cambia elemento a elemento. Por ejemplo, el siguiente código 

<code>          
   	nulldata 3
   	series x = normal()
   	x[2] = NA
   	y = misszero(x)
   	print x y --byobs
</code>

establece un valor ausente en la segunda observación de <@var="x">, y crea una nueva serie <@var="y"> en la que se sustituye la observación ausente por un cero: 

<code>          
                x            y

   	1    0,7355250    0,7355250
   	2                     0,000
   	3   -0,2465936   -0,2465936
</code>

Ver también <@ref="missing">, <@ref="ok">, <@ref="zeromiss">. 

# mlag matrix
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="p">  (escalar o vector)
		<@var="m">  (escalar, opcional)

Mueve hacia arriba o abajo las filas de la matriz <@var="X">. Cuando <@var="p"> es un escalar positivo, la función devuelve una matriz semejante a <@var="X">, pero con los valores de cada columna desplazados <@var="p"> filas hacia abajo, y con las primeras <@var="p"> filas cubiertas con el valor <@var="m">. Cuando <@var="p"> es un número negativo, la matriz que se devuelve se parece a <@var="X">, pero con los valores de cada columna desplazados hacia arriba, y las últimas filas cubiertas con el valor <@var="m">. Si omites <@var="m">, se entiende que es igual a cero. 

Si <@var="p"> es un vector, la operación indicada en el párrafo anterior se realiza con cada uno de los elementos de <@var="p">, y las matrices resultantes se unen horizontalmente. El siguiente código ilustra este uso, introduciendo para ello una matriz <@var="X"> que tiene dos columnas, y el argumento <@var="p"> que indica los retardos 1 y 2. También se determina que los valores ausentes tengan el valor NA, en contraposición al 0 establecido por defecto. 

<code>          
   matrix X = mnormal(5, 2)
   print X
   eval mlag(X, {1, 2}, NA)
</code>

<code>      
   m (5 x 2)

       1,5953    -0,070740
    -0,52713     -0,47669
      -2,2056     -0,28112
      0,97753       1,4280
      0,49654      0,18532

          nan          nan          nan          nan
     1,5953    -0,070740          nan          nan
      -0,52713     -0,47669       1,5953    -0,070740
      -2,2056     -0,28112     -0,52713     -0,47669
      0,97753       1,4280      -2,2056     -0,28112
</code>

Consulta también <@ref="lags">. 

# mlincomb midas
Resultado: 	serie 
Argumentos:	<@var="hfvars">  (lista)
		<@var="theta">  (vector)
		<@var="tipo">  (entero o cadena)

Esta es una función MIDAS muy oportuna que combina las funciones <@ref="lincomb"> y <@ref="mweights">. Dada la lista <@var="hfvars">, elabora una serie que es una suma ponderada de los elementos de esa lista. Las ponderaciones se basan en el vector <@var="theta"> de hiperparámetros y en el tipo de disposición de parámetros: consulta la función <@lit="mweights"> para obtener más detalles. Ten en cuenta que <@ref="hflags"> generalmente es el mejor modo de crear una lista apropiada para que sea el primer argumento de esta función. 

Para ser más explícitos, la expresión 

<code>          
     series s = mlincomb(hfvars, theta, 2)
</code>

es equivalente a 

<code>          
     matrix w = mweights(nelem(hfvars), theta, 2)
     series s = lincomb(hfvars, w)
</code>

pero utilizar la función <@lit="mlincomb">, permite economizar algo al teclear y también en algunos ciclos de uso de CPU. 

# mlog linalg
Resultado: 	matriz cuadradax 
Argumento: 	<@var="A">  (matriz cuadradax)

Devuelve una matriz con el logaritmo matricial de <@var="A">. El algoritmo que se usa se basa en la descomposición en autovalores, por lo que necesita que la matriz <@var="A"> sea diagonalizable. Consulta también <@ref="mexp">. 

# mnormal matrix
Resultado: 	matriz 
Argumentos:	<@var="r">  (entero)
		<@var="c">  (entero, opcional)

Devuelve una matriz formada con valores generados de forma pseudoaleatoria mediante variables con distribución Normal estándar, y que va a tener <@var="r"> filas y <@var="c"> columnas. Si lo omites, el número de columnas se establece en 1 (vector columna) por defecto. Ver también <@ref="normal">, <@ref="muniform">. 

# mols stats
Resultado: 	matriz 
Argumentos:	<@var="Y">  (matriz)
		<@var="X">  (matriz)
		<@var="&U">  (referencia a matriz, o <@lit="null">)
		<@var="&V">  (referencia a matriz, o <@lit="null">)

Devuelve una matriz <@itl="k">×<@itl="n"> de estimaciones de parámetros obtenidos mediante la regresión de Mínimos Cuadrados Ordinarios de la matriz <@var="Y"> de orden <@itl="T">×<@itl="n"> sobre la matriz <@var="X"> de orden <@itl="T">×<@itl="k">. 

Cuando se indica el tercer argumento, y no es <@lit="null">, la función va a generar una nueva matriz <@var="U"> de orden <@itl="T">×<@itl="n">, que contiene los errores. Cuando se indica el último argumento, y no es <@lit="null">, la matriz <@var="V"> que se genera va a ser de orden <@itl="k">×<@itl="k">, y contiene (a) la matriz de covarianzas de los estimadores de los parámetros, si <@var="Y"> tiene solo una columna, o (b) la matriz <@mth="X'X"><@sup="-1"> si <@var="Y"> tiene varias columnas. 

Por defecto, las estimaciones se obtienen por medio de la descomposición de Cholesky, con un último recurso a la descomposición QR si las columnas de <@var="X"> tienen alto grado de multicolinealidad. Puedes forzar el uso de la descomposición SVD mediante la instrucción <@lit="set svd on">. 

Ver también <@ref="mpols">, <@ref="mrls">. 

# monthlen calendar
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="mes">  (escalar o serie)
		<@var="año">  (escalar o serie)
		<@var="duracsemana">  (entero)

Devuelve un resultado (del mismo tipo que el argumento) que expresa cuántos días (relevantes) tiene un mes de un año (en el calendario Gregoriano proléptico). El argumento <@var="duracsemana">, que debe ser igual a 5, 6 o 7, indica el número de días de la semana que se deben contar (con el valor 6 no se cuentan los domingos, y con 5 no se cuentan ni los sábados ni los domingos). 

El valor que se devuelva va a ser un escalar si son escalares tanto <@var="mes"> como <@var="año">; en caso contrario será una serie. 

Por ejemplo, si tienes abierto un conjunto de datos mensuales, la expresión 

<code>          
     series wd = monthlen($obsminor, $obsmajor, 5)
</code>

devolverá una serie que va contener el número de días laborables de cada uno de los meses de la muestra. 

# movavg timeseries
Resultado: 	serie 
Argumentos:	<@var="x">  (serie)
		<@var="p">  (escalar)
		<@var="control">  (entero, opcional)
		<@var="y0">  (escalar, opcional)

Devuelve una serie que es una media móvil de <@var="x"> y, dependiendo del valor del parámetro <@var="p">, resultará una media móvil simple o ponderada exponencialmente. 

Cuando <@var="p"> > 1, la función calcula una media móvil simple de <@var="p"> elementos; es decir, calcula la media aritmética de <@mth="x"> desde el período <@mth="t"> hasta el período <@mth="t-p+1">. Cuando indicas un valor no nulo para el argumento <@var="control"> (opcional), la media móvil “se centra”; en caso contrario, “se retarda”. El otro argumento <@var="y0"> no se va a tener en cuenta. 

Cuando <@var="p"> es un fracción decimal entre 0 y 1, la función calcula una media móvil exponencial: 

<@mth="y(t) = p*x(t) + (1-p)*y(t-1)"> 

Por defecto, la serie <@mth="y"> que se devuelve, se inicia utilizando el primer valor válido de <@var="x">. Pero puedes utilizar el parámetro <@var="control"> para especificar un número de observaciones iniciales, de forma que su media se tomará como <@mth="y(0)">; un valor de cero para <@var="control"> indica que deben tomarse todas las observaciones para calcular ese valor. Otra posibilidad consiste en que puedes especificar el valor inicial utilizando el argumento opcional <@var="y0">; en ese caso, el argumento <@var="control"> no va a tenerse en cuenta. 

# mpiallred mpi
Resultado: 	entero 
Argumentos:	<@var="&object">  (referencia a objeto)
		<@var="op">  (cadena)

Solo disponible cuando GRETL está en modo MPI (consulta <@mnu="gretlMPI">); deberán invocarlo todos los procesos. Esta función opera igual que <@ref="mpireduce"> excepto por el hecho de que todos los procesos (no solo el proceso principal) reciben una copia del objeto “reducido” en lugar del original. Por lo tanto, esto es equivalente a lo que hace la función <@lit="mpireduce"> seguida por una llamada a la función <@ref="mpibcast">, pero más eficiente. 

# mpibarrier mpi
Resultado: 	entero 

Solo disponible cuando GRETL está en modo MPI (consulta <@mnu="gretlMPI">); no requiere argumentos. Fuerza la sincronización de los procesos MPI: ningún proceso puede continuar más allá de la barrera hasta que la alcancen todos ellos. 

<code>          
     # Ninguno pasa hasta que todos lleguen aquí
     mpibarrier()
</code>

# mpibcast mpi
Resultado: 	entero 
Argumentos:	<@var="&objeto">  (referencia a objeto)
		<@var="raíz">  (entero, opcional)

Solo disponible cuando GRETL está en modo MPI (consulta <@mnu="gretlMPI">); deberán invocarlo todos los procesos. Difunde el argumento <@var="objeto">, que deberás indicar en forma puntero, a todos los procesos. El objeto en cuestión (una matriz, un 'bundle', un escalar, un 'array' una cadena de texto o una lista) debe indicarse en todos los procesos anteriores a la difusión. Ningún proceso puede continuar después de una llamada a <@lit="mpibcast"> hasta que todos los procesos lo consigan ejecutar con éxito. 

Por defecto, se entiende que la “raíz” u origen de la difusión es el proceso MPI con rango 0; pero puedes ajustar esto mediante el segundo argumento (opcional), que deberá ser un número entero entre 0 y el número de procesos MPI menos 1. 

A continuación, tenemos un ejemplo sencillo. Cuando se complete con éxito, cada proceso va a tener una copia de la matriz <@lit="X"> definida en el rango 0. 

<code>          
     matrix X
     if $mpirank == 0
         X = mnormal(T, k)
     endif
     mpibcast(&X)
</code>

# mpirecv mpi
Resultado: 	objeto 
Argumento: 	<@var="src">  (entero)

Solo disponible cuando GRETL está en modo MPI (consulta <@mnu="gretlMPI">). Para mayor aclaración, mira la función <@ref="mpisend">, con la que <@lit="mpirecv"> deberá siempre emparejarse. El argumento <@var="src"> especifica la jerarquía del proceso del que se va a recibir el objeto, en el rango que va desde 0 hasta el número de procesos MPI menos 1. 

# mpireduce mpi
Resultado: 	entero 
Argumentos:	<@var="&objeto">  (referencia a objeto)
		<@var="op">  (cadena)
		<@var="raíz">  (entero, opcional)

Solo disponible cuando GRETL está en modo MPI (consulta <@mnu="gretlMPI">); deberán invocarlo todos los procesos. Esta función reúne objetos (escalares, matrices o 'arrays') con un nombre determinado indicados en forma de puntero, de todos los procesos, y los “reduce” a un único objeto en el nodo raíz. 

El argumento <@lit="op"> especifica la operación o método de reducción. Los métodos admitidos para los escalares son <@lit="sum"> (suma), <@lit="prod"> (producto), <@lit="max"> (máximo) y <@lit="min"> (mínimo). Para las matrices, los métodos son <@lit="sum">, <@lit="prod"> (producto de Hadamard), <@lit="hcat"> (concatenación horizontal) y <@lit="vcat"> (concatenación vertical). Para los 'arrays' solo se admite <@lit="acat"> (concatenación). 

Por defecto, se entiende que la “raíz” o meta de la reducción es el proceso MPI con rango 0; pero puedes ajustar esto mediante el tercer argumento (opcional), que deberá ser un entero entre 0 y el número de procesos MPI menos 1. 

A continuación, tenemos un ejemplo. Cuando se complete con éxito lo dicho antes, el proceso raíz va a tener una matriz <@lit="X"> que será la suma de las matrices <@lit="X"> de todos los procesos. 

<code>          
     matrix X
     X = mnormal(T, k)
     mpireduce(&X, sum)
</code>

# mpiscatter mpi
Resultado: 	entero 
Argumentos:	<@var="&M">  (referencia a matriz)
		<@var="op">  (cadena)
		<@var="raíz">  (entero, opcional)

Solo disponible cuando GRETL está en modo MPI (consulta <@mnu="gretlMPI">); deberán invocarlo todos los procesos. Esta función distribuye trozos de una matriz del proceso raíz, a todos los procesos. Debes anunciar la matriz en todos los procesos que preceden a invocar a <@lit="mpiscatter">, y debes indicarlo en forma de puntero. 

El argumento <@lit="op"> debe ser <@lit="byrows"> o bien <@lit="bycols">. Denotemos con <@mth="q"> al cociente entre el número de filas de la matriz que se va a dispersar, y el número de procesos. En el caso <@lit="byrows">, el proceso raíz va a enviar las primeras <@mth="q"> filas al proceso 0; las siguientes <@mth="q"> al proceso 1, etcétera. Si queda un remanente del reparto de filas, se añade a la última asignación. El caso <@lit="bycols"> es exactamente análogo pero el reparto de la matriz se hace por columnas. 

A continuación, tenemos un ejemplo. Si tenemos 4 procesos, cada uno (incluido el raíz) va a tener una porción 2500×10 de la <@lit="X"> original, tal como se encontraba en el proceso raíz. Si quisieras mantener la matriz completa en el proceso raíz, es necesario que hagas una copia de la misma antes de invocar a <@lit="mpiscatter">. 

<code>          
     matrix X
     if $mpirank == 0
         X = mnormal(10000, 10)
     endif
     mpiscatter(&X, byrows)
</code>

# mpisend mpi
Resultado: 	entero 
Argumentos:	<@var="objeto">  (objeto)
		<@var="destino">  (entero)

Solo disponible cuando GRETL está en modo MPI (consulta <@mnu="gretlMPI">). Envía el objeto indicado (una matriz, un 'bundle', un 'array', un escalar, una cadena de texto o una lista) desde el proceso vigente hasta el identificado por el entero <@var="destino"> (desde 0 hasta el número de procesos MPI menos 1). 

Una llamada a esta función debe siempre estar emparejada con una llamada a <@ref="mpirecv"> en el proceso <@var="destino">, como en el siguiente ejemplo en el que se envía una matriz desde el rango 2 hasta el rango 3. 

<code>          
     if $mpirank == 2
         matrix C = cholesky(A)
         mpisend(C, 3)
     elif $mpirank == 3
         matrix C = mpirecv(2)
     endif
</code>

# mpols stats
Resultado: 	matriz 
Argumentos:	<@var="Y">  (matriz)
		<@var="X">  (matriz)
		<@var="&U">  (referencia a matriz, o <@lit="null">)

Funciona igual que <@ref="mols">, devolviendo una matriz, salvo que los cálculos se hacen con alta precisión utilizando la biblioteca GMP. 

Por defecto, GMP utiliza 256 bits para cada número de punto flotante, pero puedes ajustar esto utilizando la variable de contexto <@lit="GRETL_MP_BITS">; por ejemplo, <@lit="GRETL_MP_BITS=1024">. 

# mrandgen matrix
Resultado: 	matriz 
Argumentos:	<@var="d">  (cadena)
		<@var="p1">  (escalar o matriz)
		<@var="p2">  (escalar o matriz, condicional)
		<@var="p3">  (escalar, condicional)
		<@var="filas">  (entero)
		<@var="columnas">  (entero)
Ejemplos: 	<@lit="matrix mx = mrandgen(u, 0, 100, 50, 1)">
		<@lit="matrix mt14 = mrandgen(t, 14, 20, 20)">
		<@lit="matrix D = mrandgen(dir, {0.5,1,2,4}, 30)">

Con una excepción (mira más abajo), esta función opera de la misma forma que la función <@ref="randgen"> excepto por el hecho de que devuelve una matriz en lugar de una serie. Los argumentos iniciales (cuyo número depende de la distribución escogida) para esta función ya se describen para <@lit="randgen">, pero deben estar seguidos por dos números enteros para especificar las dimensiones de filas y de columnas que va a tener la matriz aleatoria deseada. Si indicas <@var="p1"> o <@var="p2"> en forma matricial, deben tener un número de elementos que sea igual al producto de <@var="filas"> por <@var="columnas">. 

Un caso excepcional es la distribución de Dirichlet. Esta es una distribución multivariante, y hacer una llamada a <@lit="mrandgen"> con “dir” como primer parámetro acarrea una sintaxis especial: el segundo argumento debe ser un vector <@mth="a">, positivo con <@mth="k"> elementos, y el tercer argumento debe ser un escalar <@mth="r">. La función va a devolver una matriz de orden <@itl="r">×<@itl="k"> en la que cada fila es una extracción independiente de una distribución de Dirichlet con parámetro <@mth="a">. 

El primero de los ejemplos precedentes solicita un vector columna de 50 elementos, extraídos a partir de una distribución Uniforme en [0,100]. El segundo ejemplo especifica una matriz aleatoria de orden 20×20, con valores extraídos de una distribución <@mth="t"> con 14 grados de libertad; y el tercero devuelve una matriz de orden 30×4 que contiene 30 extracciones de la distribución de Dirichlet indicada. 

Ver también <@ref="mnormal">, <@ref="muniform">. 

# mread data-utils
Resultado: 	matriz 
Argumentos:	<@var="nombrearchivo">  (cadena)
		<@var="importar">  (booleano, opcional)

Lee una matriz guardada en el archivo llamado <@var="nombrearchivo">. Si en el nombre no está especificada la ruta completa hasta el archivo, se va a buscar en algunas localizaciones que se consideren “probables”, empezando por el directorio de trabajo establecido en ese momento en <@xrf="workdir">. No obstante, cuando se indica un valor no nulo para el segundo argumento <@var="importar"> (opcional) de la función, el archivo se busca en el directorio “punto” del usuario. Esto tiene la intención de que se use esta función junto con las que exportan matrices, y que se ofrecen en el contexto de la instrucción <@xrf="foreign">. En ese caso, el argumento <@var="nombrearchivo"> debe ser un nombre de archivo simple, sin indicar la ruta hasta el archivo. 

Actualmente la función reconoce cuatro formatos de archivo: 

<subhead>Formato de texto original</subhead> 

Estos archivos se identifican mediante la extensión “<@lit=".mat">”, y son completamente compatibles con el formato de archivo de matriz Ox. Cuando el nombre del archivo tiene la extensión “<@lit=".gz">”, se asume que al guardar los datos se ha aplicada la compresión gzip. El archivo se asume que es de texto plano, de acuerdo con la siguiente especificación: 

<indent>
• El archivo comienza con ningún o con un número cualquiera de comentarios, definidos por líneas que comienzan con el carácter numeral, <@lit="#">; estas líneas se ignoran. 
</indent>

<indent>
• La primera línea que no sea un comentario debe contiene dos enteros, separados por un carácter de tabulación, para indicar el número de filas y de columnas, respectivamente. 
</indent>

<indent>
• Las columnas se separan por medio de tabulaciones. 
</indent>

<indent>
• El separador decimal es el carácter punto, “<@lit=".">”. 
</indent>

<subhead>Archivos binarios</subhead> 

Los archivos con extensión “<@lit=".bin">” se asume que están en formato binario. La extensión “<@lit=".gz">” también se reconoce para la compresión gzip. Los primeros 19 bytes contienen los caracteres <@lit="gretl_binary_matrix">; los siguientes 8 bytes contienen dos enteros de 32 bits que proporcionan el número de filas y de columnas; y lo que resta del archivo contiene los elementos de la matriz ordenados por orden de mayor columna, con formato “dobles” en extremo menor (little-endian). Si ejecutas GRETL en un sistema de extremo mayor (big-endian), los valores binarios se convierten a extremo menor cuando se escriben, y se convierten a extremo mayor cuando se leen. 

<subhead>Archivos con texto delimitado</subhead> 

Si el nombre del archivo que se va a leer tiene la extensión “<@lit=".csv">”, las reglas que administran la lectura del archivo según su formato son diferentes, y más laxas. En este caso, el conjunto de datos presentes <@itl="no"> debe estar precedido por una línea que especifique el número de filas y de columnas. GRETL tratará de determinar el delimitador utilizado (coma, espacio, o punto y coma), y hará lo que pueda para importar la matriz, admitiendo el uso de la coma como separador decimal, si es necesario. Ten en cuenta que el delimitador no debe ser el carácter de tabulación, dado el riesgo de confundir ese tipo de archivos con los que tienen el formato de matrices “original” de GRETL. 

<subhead>Archivos de conjuntos de datos de GRETL</subhead> 

Los archivos que tengan extensión “<@lit=".gdt">” o “<@lit=".gdtb">” se tratan como archivos originales de datos de GRETL, tal como los crea la instrucción <@xrf="store"> (guardar). En tal caso, la matriz que se va a devolver contiene los valores numéricos de las series del conjunto de datos, ordenadas en columnas. Ten en cuenta que las series con valores en cadenas de texto no se leen como tales; la matriz solo va a contener sus codificaciones numéricas. 

Ver también <@ref="bread">, <@ref="mwrite">. 

# mreverse matrix
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="porcolumna">  (booleano, opcional)

Devuelve una matriz que contiene las filas de <@var="X"> en orden inverso; o las columnas en orden inverso si el segundo argumento tiene un valor no nulo. 

# mrls stats
Resultado: 	matriz 
Argumentos:	<@var="Y">  (matriz)
		<@var="X">  (matriz)
		<@var="R">  (matriz)
		<@var="q">  (vector columna)
		<@var="&U">  (referencia a matriz, o <@lit="null">)
		<@var="&V">  (referencia a matriz, o <@lit="null">)

Mínimos cuadrados restringidos: Genera la matriz de orden <@itl="k">×<@itl="n"> con los parámetros estimados mediante la regresión de mínimos cuadrados de la matriz <@var="Y"> de orden <@itl="T">×<@itl="n">, sobre la matriz <@var="X"> de orden <@itl="T">×<@itl="k">, sujeta al conjunto de restricciones lineales de los parámetros <@mth="RB "> = <@mth="q">, donde <@mth="B"> representa el vector que formarían los parámetros apilados unos sobre los otros. <@var="R"> debe tener <@mth="kn"> columnas, y cada línea de ella indica los coeficientes de una de las restricciones lineales. El número de filas de <@var="q"> debe coincidir con el número de filas de <@var="R">. 

Si el quinto argumento de la función no es <@lit="null">, entonces la matriz <@var="U"> de orden <@itl="T">×<@itl="n"> va a contener los errores. Cuando proporcionas un argumento final que no es <@lit="null">, entonces la matriz <@var="V"> de orden <@itl="k">×<@itl="k"> va a guardar la contrapartida restringida de la matriz <@mth="X'X"><@sup="-1">. Puedes construir la matriz de varianzas-covarianzas de los estimadores de la ecuación <@mth="i"> multiplicando la submatriz apropiada de <@var="V"> por una estimación de la varianza de la perturbación de esa ecuación. 

# mshape matrix
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="r">  (entero)
		<@var="c">  (entero, opcional)

Reordena los elementos de la matriz <@var="X"> en una nueva matriz que tiene <@var="r"> filas y <@var="c"> columnas. Los elementos se leen y se guardan comenzando por el de la primera columna y primera fila de <@var="X">, y siguiendo con los de las siguientes filas hasta acabar con los de esa columna; y luego con las demás columnas. Si <@var="X"> tiene menos elementos que <@mth="k">= <@mth="rc">, estos se van a repetir de forma cíclica. En otro caso, si <@var="X"> tiene más elementos, solo se utilizan los primeros <@mth="k"> elementos. 

Si omites e tercer argumento, por defecto <@var="c"> se establece igual a 1 si <@var="X"> es 1×1; en otro caso, se establece igual a <@mth="N">/<@var="r"> donde <@mth="N"> representa el número total de elementos que hay en <@var="X">. Sin embargo, cuando <@mth="N"> no es un múltiplo entero de <@var="r"> se presenta un error. 

Ver también <@ref="cols">, <@ref="rows">, <@ref="unvech">, <@ref="vec">, <@ref="vech">. 

# msortby matrix
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="j">  (entero)

Devuelve una matriz con las mismas filas de la matriz del argumento <@var="X"> reordenadas de forma creciente de acuerdo con los elementos de la columna <@var="j">. Este orden es estable: las filas que comparten el mismo valor en la columna <@var="j"> no se intercambian. 

# msplitby matrix
Resultado: 	array de matrices 
Argumentos:	<@var="X">  (matriz)
		<@var="v">  (escalar o matriz)
		<@var="porcolum">  (booleano)

Devuelve una formación de matrices, como resultado de separar horizontal o verticalmente la matriz <@var="X">, bajo el control de los argumentos <@var="v"> y <@var="porcolum">. Si el argumento <@var="porcolum"> no es nulo, la matriz se va a separar por columnas; en caso contrario y como predeterminado, se hará por filas. 

El argumento <@var="v"> puede ser un vector o un escalar. 

<indent>
• Vector: Debe tener una longitud igual a la dimensión relevante (de filas o de columnas) de la matriz <@var="X">; además debe contener números enteros positivos. El mayor entero establece la longitud del 'array' que se devuelve. Cada elemento de <@var="v"> indica el índice que tiene, en el 'array', la matriz a la que deberá asignarse la correspondiente fila de <@var="X">. 
</indent>

<indent>
• Escalar: La dimensión relevante de la matriz <@var="X"> (fila o columna, según determine <@var="porcolum">) debe ser un múltiplo exacto del valor de este escalar. La matriz <@var="X"> se va a separar en trozos con <@var="v"> filas o columnas en cada uno de ellos. 
</indent>

En el siguiente ejemplo se separan las filas de una matriz 4×3 en tres matrices: las dos primeras filas se asignan a la primera matriz; la segunda matriz se deja vacía; la tercera y cuarta matrices incluye la tercera y cuarta filas de <@var="X">, respectivamente. 

<code>          
     matrix X = {1,2,3; 4,5,6; 7,8,9; 10,11,12}
     matrices M = msplitby(X, {1,1,3,4})
     print M
</code>

La orden de impresión depara 

<code>          
     Array de matrices, longitud 4
     [1] 2 x 3
     [2] null
     [3] 1 x 3
     [4] 1 x 3
</code>

El siguiente ejemplo separa <@var="X"> equitativamente: 

<code>          
     matrix X = {1,2,3; 4,5,6; 7,8,9; 10,11,12}
     matrices MM = msplitby(X, 2)
     print MM[1]
     print MM[2]
</code>

que depara 

<code>          
     ? print MM[1]
     1   2   3 
     4   5   6 

     ? print MM[2]
     7    8    9 
     10   11   12
</code>

Consulta la función <@ref="flatten"> para la operación inversa. 

# muniform matrix
Resultado: 	matriz 
Argumentos:	<@var="r">  (entero)
		<@var="c">  (entero, opcional)

Devuelve una matriz formada con números generados de forma pseudoaleatoria mediante variables con distribución Uniforme (0,1), y que va a tener <@var="r"> filas y <@var="c"> columnas. Si lo omites, el número de columnas se establece en 1 (vector columna), por defecto. Aviso: El método predilecto para generar números pseudoaleatorios con distribución Uniforme es el que usa la función <@ref="randgen1">. 

Ver también <@ref="mnormal">, <@ref="uniform">. 

# mweights midas
Resultado: 	matriz 
Argumentos:	<@var="p">  (entero)
		<@var="theta">  (vector)
		<@var="tipo">  (entero o cadena)

Devuelve un vector de orden <@mth="p"> con las ponderaciones MIDAS que se aplican a los <@mth="p"> retardos de una serie de alta frecuencia, basado en el vector <@var="theta"> de hiperparámetros. 

El argumento <@var="tipo"> identifica el tipo de disposición de parámetros que va a regular el número <@mth="k"> de elementos que se solicitan para <@var="theta">: 1 = para Almon exponencial normalizada (<@mth="k"> debe ser cuando menos igual a1, habitualmente 2); 2 = para Beta normalizada con el retardo final nulo (<@mth="k"> = 2); 3 = para Beta normalizada con el retardo final no nulo (<@mth="k"> = 3); y 4 = para Almon polinómico (<@mth="k"> debe ser cuando menos igual a 1). Ten en cuenta que, en el caso de Beta normalizada, los dos primeros elementos de <@var="theta"> deben ser positivos. 

Puedes indicar el <@var="tipo"> como un código entero, tal y como se muestra más abajo, o mediante una de las siguientes cadenas de texto (respectivamente): <@lit="nealmon">, <@lit="beta0">, <@lit="betan"> o <@lit="almonp">. Si utilizas una cadena de texto, esta deberá estar situada entre comillas. Por ejemplo, las dos siguientes expresiones son equivalentes: 

<code>          
     W = mweights(8, theta, 2)
     W = mweights(8, theta, "beta0")
</code>

Ver también <@ref="mgradient">, <@ref="midasmult">, <@ref="mlincomb">. 

# mwrite data-utils
Resultado: 	entero 
Argumentos:	<@var="X">  (matriz)
		<@var="nombrearchivo">  (cadena)
		<@var="exportar">  (booleano, opcional)

Escribe la matriz del argumento <@var="X"> en un archivo con el nombre <@var="nombrearchivo">. Por defecto, este archivo va a ser de texto plano y, en la primera línea, va a contener dos números enteros que representan el número de filas y de columnas separados (respectivamente) por un carácter de tabulación. En las siguientes filas, los elementos de la matriz se muestran con notación científica, separados por tabulaciones (una línea por fila). Para evitar confusiones al leerlos, los archivos que se escriban en este formato deben ser denominados con el sufijo “<@lit=".mat">”. Para formatos alternativos, mira más abajo. 

Cuando ya existe un archivo llamado <@var="nombrearchivo">, se va a sobrescribir. La ejecución de la función devuelve un valor nominal de 0 si se completa con éxito; si fracasa la escritura, se muestra un fallo. 

El archivo con los resultados va a escribirse en el directorio establecido como vigente, <@xrf="workdir">, excepto que la cadena de texto del argumento <@var="nombrearchivo"> especifique el directorio con la ruta completa. No obstante, si indicas un valor no nulo para el argumento <@var="exportar">, el archivo con los resultados va a escribirse en el directorio “punto” del usuario, donde estará accesible por defecto mediante las funciones para cargar matrices que se ofrecen en el contexto de la instrucción <@xrf="foreign">. En este caso, debes indicar un simple nombre de archivo para el segundo argumento, sin la parte que expresa la ruta al directorio. 

Las matrices guardadas mediante la forma que tiene por defecto la función <@lit="mwrite">, pueden leerse fácilmente con otros programas. Consulta <@pdf="El manual de gretl#chap:matrices"> (Capítulo 17) para obtener más detalles. 

Tres matizaciones, que se excluyen mutuamente, de esta función están disponibles como se indica a continuación: 

<indent>
• Si el argumento <@var="nombrearchivo"> tiene la extensión “<@lit=".gz">”, entonces el archivo se guarda con el formato descrito más arriba, pero usando la compresión gzip. 
</indent>

<indent>
• Si el argumento <@var="nombrearchivo"> tiene la extensión “<@lit=".bin">”, entonces la matriz se guarda con formato binario. En este caso, los primeros 19 bytes contienen los caracteres <@lit="gretl_binary_matrix">; los siguientes 8 bytes contienen dos enteros de 32 bits que proporcionan el número de filas y de columnas; y lo que resta del archivo contiene los elementos de la matriz ordenados por orden de mayor columna, con formato “dobles” en extremo menor (little-endian). Si ejecutas GRETL en un sistema de extremo mayor (big-endian), los valores binarios se convierten a extremo menor cuando se escriben, y se convierten a extremo mayor cuando se leen. 
</indent>

<indent>
• Si el argumento <@var="nombrearchivo"> tiene la extensión “<@lit=".csv">”, entonces la matriz se guarda con formato de separación con comas, sin la línea de encabezamiento que indique el número de filas y de columnas que la siguen. Esto podría hacer más sencillo el tratamiento con programas de terceros, pero no es recomendable cuando se pretende leer el archivo con los elementos de la matriz mediante GRETL. 
</indent>

Ten en cuenta que, si vas a leer el archivo con la matriz utilizando otro software ajeno, no resulta aconsejable que utilices las opciones gzip ni binario. Pero si lo quieres para que lo lea GRETL, estos dos formatos alternativos permiten ahorrar espacio; y con el formato binario logras una lectura más rápida de matrices grandes. El formato gzip no es recomendable para matrices muy grandes porque la descompresión puede ser bastante lenta. 

Ver también <@ref="mread">. Para escribir una matriz en un archivo, como conjunto de datos, consulta <@xrf="store">. 

# mxtab stats
Resultado: 	matriz 
Argumentos:	<@var="x">  (serie o vector)
		<@var="y">  (serie o vector)

Devuelve una matriz que incluye la tabulación cruzada de los valores contenidos en <@var="x"> (por filas) e <@var="y"> (por columnas). Los dos argumentos de esta función deben ser del mismo tipo (ser series ambas o vectores columna ambos). Generalmente se espera (aunque no es imprescindible) que los argumentos tengan valores discretos, con menos valores diferentes que observaciones. De otro modo, la tabulación cruzada puede resultar muy grande y no muy informativa. 

Ver también <@ref="values">, <@ref="corresp">. 

# naalen nonparam
Resultado: 	matriz 
Argumentos:	<@var="d">  (serie o vector)
		<@var="cens">  (serie o vector, opcional)

Devuelve el cálculo del estimador no paramétrico de Nelson–Aalen de la función de riesgo (<@bib="Nelson, 1972;nelson72">; <@bib="Aalen, 1978;aalen78">), dada una muestra <@var="d"> de datos de duración, que posiblemente esté acompañada de un registro de estado de censura, <@var="cens">. La matriz que devuelve la función tiene tres columnas que contienen, respectivamente: los valores únicos ordenados en <@var="d">, la estimación de la función de riesgo acumulado que se corresponde con los valores de duración de la columna 1, y la desviación típica del estimador. 

Cuando indicas la serie <@var="cens">, se utiliza el valor 0 para señalar que una observación no está censurada, mientras que el valor 1 indica que una observación está censurada del lado derecho (es decir, el período de observación del individuo en cuestión concluyó antes de la duración o el período se registró como finalizado). Cuando no indicas <@var="cens">, se asume que todas las observaciones son no censuradas. (Aviso: la semántica de <@var="cens"> puede extenderse en algún punto para cubrir otros tipos de censura.) 

Ver también <@ref="kmeier">. 

# nadarwat nonparam
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="x">  (serie)
		<@var="h">  (escalar, opcional)
		<@var="LOO">  (booleano, opcional)
		<@var="recorte">  (escalar, opcional)

Calcula una serie con la estimación no paramétrica de la media condicional de <@var="y"> dado <@var="x">, de Nadaraya-Watson. La serie que devuelve la función, contiene <@mth="m(x"><@sub="i"><@mth=")">, los valores de las estimaciones de <@mth="E(y"><@sub="i"><@mth="|x"><@sub="i"><@mth=")"> para cada uno de los elementos no ausentes de la serie <@var="x">. 

La función núcleo (kernel) empleada por este estimador viene dada por <@mth="K = exp(-x"><@sup="2"><@mth="/2h)"> cuando <@mth="|x|<T">, y es igual a cero en otro caso. (<@mth="T"> = Parámetro de recorte.) 

Los tres argumentos opcionales modulan el comportamiento del estimador tal como se describe más abajo. 

<subhead>Ancho de banda</subhead> 

Puedes usar el argumento <@var="h"> para controlar el ancho de banda (“bandwidth”), mediante un número real positivo. Habitualmente este es un número pequeño, pues valores más grandes de <@var="h"> hacen que <@mth="m(x)"> sea más suave. Una elección popular es hacer que <@var="h"> sea proporcional a <@mth="n"><@sup="-0.2">. Si omites <@var="h"> o lo igualas a cero, el ancho de banda se establece por defecto con un valor determinado por los datos, utilizando la proporcionalidad que se acaba de mencionar, pero introduciendo la dispersión de los datos de <@var="x"> tal como la mide el rango inter-cuartil o la desviación estándar; consulta <@pdf="El manual de gretl#chap:nonparam"> (Capítulo 40) para obtener más detalles. 

<subhead>Dejar-una-fuera</subhead> 

“Dejar-una-fuera” es una variante del algoritmo, que omite la observación <@mth="i">-ésima cando se evalúa <@mth="m(x"><@sub="i"><@mth=")">. Esto hace que el estimador de Nadaraya–Watson sea numéricamente más robusto, y por eso se recomienda habitualmente utilizarlo cuando el estimador se calcula con intención de hacer inferencias. Esta variante no está permitida por defecto, pero se activa cuando se indica un valor no nulo para el argumento <@var="LOO">. 

<subhead>Recorte</subhead> 

Puedes usar el argumento <@var="recorte"> para controlar el grao de “recorte” que se impone para prevenir problemas numéricos, cuando la función 'kernel' se está evaluando demasiado lejos de cero. Este parámetro se expresa como un múltiplo de <@var="h">, siendo 4 el valor por defecto. En algunos casos, puede ser preferible utilizar un valor mayor que 4. De nuevo, consulta <@pdf="El manual de gretl#chap:nonparam"> (Capítulo 40) para obtener más detalles. 

Consulta también <@ref="loess">. 

# nelem data-utils
Resultado: 	entero 
Argumento: 	<@var="L">  (lista, matriz, bundle o array)

Devuelve un entero con el número de elementos que hay en el argumento; este puede ser una lista, una matriz, un 'bundle' o un 'array' (pero no una serie). 

# ngetenv programming
Resultado: 	escalar 
Argumento: 	<@var="s">  (cadena)

Devuelve un escalar con el valor numérico de una variable de contexto que tiene el nombre del argumento <@var="s">, se esa variable está definida y se tiene un valor numérico; en otro caso devuelve NA. Consulta también <@ref="getenv">. 

# nlines strings
Resultado: 	escalar 
Argumento: 	<@var="buf">  (cadena)

Devuelve un escalar con la cantidad de filas completas (es decir, filas que rematan con el carácter de nueva línea) en <@var="buf">. 

Ejemplo: 

<code>          
        string web_page = readfile("http://gretl.sourceforge.net/")
        scalar number = nlines(web_page)
        print number
</code>

# NMmax numerical
Resultado: 	escalar 
Argumentos:	<@var="&b">  (referencia a matriz)
		<@var="f">  (llamada a función)
		<@var="maxevalfunc">  (entero, opcional)

Devuelve un escalar con el resultado de una maximización numérica hecha con el método del simplex sin derivadas de Nelder–Mead. El argumento <@var="b"> debe contener los valores iniciales de un conjunto de parámetros, y el argumento <@var="f"> debe especificar una llamada a la función que va a calcular el criterio objetivo (escalar) que se quiere maximizar, dados los valores vigentes de los parámetros, así como cualesquiera otros datos que sean relevantes. Cuando se completa con éxito su ejecución, <@lit="NMmax"> devuelve el valor maximizado del criterio objetivo, y <@var="b"> contiene finalmente los valores de los parámetros que producen el máximo. 

Puedes utilizar el tercer argumento (opcional) para indicar el número máximo de evaluaciones de la función; si lo omites o lo estableces igual a cero, el máximo se toma por defecto igual a 2000. Como indicación especial para esta función, puedes poner un valor negativo para o argumento <@var="maxevalfunc">. En ese caso, se toma su valor absoluto y <@lit="NMmax"> muestra un fallo si el mejor valor encontrado para la función objetivo después de realizar el máximo número de evaluaciones de la función, no es un óptimo local. Por otra parte, en este sentido la no convergencia no se trata como un fallo. 

Si tu objetivo realmente es alcanzar un mínimo, puedes bien cambiar la función considerando el negativo del criterio, o bien, alternativamente, puedes invocar la función <@lit="NMmax">bajo el alias <@lit="NMmin">.. 

Para más detalles y ejemplos, consulta <@pdf="El manual de gretl#chap:numerical"> (Capítulo 37). Ver también <@ref="simann">. 

# NMmin numerical
Resultado: 	escalar 

Un alias de <@ref="NMmax">. Si invocas la función bajo este nombre, se ejecuta haciendo una minimización. 

# nobs stats
Resultado: 	escalar o serie 
Argumento: 	<@var="x">  (serie o lista)

Si <@var="x"> es una serie, devuelve el número de observaciones no ausentes de esa serie, en la muestra vigente seleccionada. 

Si <@var="x"> es una lista, devuelve una serie <@mth="y"> tal que <@mth="y"><@sub="t"> representa el recuento de las series de la lista que no tienen un valor ausente en la observación <@mth="t">. 

Ver también <@ref="pnobs">, <@ref="pxnobs">. 

# normal probdist
Resultado: 	serie 
Argumentos:	<@var="μ">  (escalar)
		<@var="σ">  (escalar)

Devuelve una serie generada con una variable pseudoaleatoria gaussiana de media μ y desviación típica σ. Si no indicas ningún argumento, los valores que se devuelven son los de una variable con distribución de probabilidad Normal estándar, <@mth="N">(0,1). Los valores se producen utilizando el método Ziggurat (<@bib="Marsaglia y Tsang, 2000;marsaglia00">). 

Ver también <@ref="randgen">, <@ref="mnormal">, <@ref="muniform">. 

# normtest stats
Resultado: 	matriz 
Argumentos:	<@var="y">  (serie o vector)
		<@var="método">  (cadena, opcional)

Devuelve una matriz con los resultados de realizar un(os) contraste(s) de Normalidad sobre <@var="y">. La función realiza por defecto el contraste de Doornik–Hansen, pero puedes utilizar el argumento <@var="método"> (opcional) para escoger una alternativa. Indica: <@lit="swilk"> para ejecutar el contraste de Shapiro–Wilk, <@lit="jbera"> para realizar el contraste de Jarque–Bera, o <@lit="lillie"> para efectuar el contraste de KS-Lilliefors. O indica <@lit="all"> en el argumento del <@var="método"> para realizar los cuatro contrastes. 

Puedes indicar el segundo argumento con formato entre comillas o sin ellas. En este último caso, también puedes indicar una cadena de texto cuyo valor sea el nombre de uno de los métodos, por el que se va a substituir cuando se ejecuta. 

La matriz que se devuelve es de orden 1×2 para un único contraste, o 4×2 cuando se hacen todos los contrastes. Los estadísticos de prueba se encuentran en la primera columna, y las probabilidades asociadas en la segunda. El estadístico de prueba no sigue la misma distribución de probabilidad en todos los casos. Para Doornik–Hansen y Jarque–Bera es una Chi-cuadrado(2); en los otros métodos se trata de un estadístico peculiar para cuyas probabilidades asociadas se requiere un cálculo especial. 

Consulta también la instrucción <@xrf="normtest">. 

# npcorr stats
Resultado: 	matriz 
Argumentos:	<@var="x">  (serie o vector)
		<@var="y">  (serie o vector)
		<@var="método">  (cadena, opcional)

Devuelve un vector fila con los cálculos de una medida de correlación entre <@var="x"> e <@var="y">, utilizando un método no paramétrico. Si indicas el tercer argumento, este debe ser <@lit="kendall"> (para el método por defecto, el tau de Kendall, versión b) o bien <@lit="spearman"> (para el rho de Spearman). 

El resultado que se devuelve es un vector fila con 3 valores que indican: la medición de la correlación, el valor del estadístico de contraste de la hipótesis nula de incorrelación, y la probabilidad asociada a ese valor. Observa que, si el tamaño de la muestra es muy pequeño, el estadístico de contraste y/o la probabilidad puede ser <@lit="NaN"> (no es número, o ausente). 

Consulta también <@ref="corr"> para la correlación de Pearson. 

# npv math
Resultado: 	escalar 
Argumentos:	<@var="x">  (serie o vector)
		<@var="r">  (escalar)

Devuelve un escalar con el Valor Actual Neto de <@var="x">, considerado este como una secuencia de pagos (negativos) e ingresos (positivos), evaluados a una tasa de descuento anual que debes indicar en el argumento <@var="r"> como fracción decimal entre 0 y 1, no como porcentaje (por ejemplo 0.05, y no 5<@lit="%">). El primer valor de la serie/vector del primer argumento se considera que está fechado “ahora”, y no se descuenta. Para imitar una función VAN en la que se descuente el primer valor, añade un cero al principio de la serie/vector del primer argumento. 

El tipo de frecuencia de los datos que admite esta función puede ser anual, trimestral, mensual y sin fecha (este tipo se trata como si fuera anual). 

Ver también <@ref="irr">. 

# NRmax numerical
Resultado: 	escalar 
Argumentos:	<@var="&b">  (referencia a matriz)
		<@var="f">  (llamada a función)
		<@var="g">  (llamada a función, opcional)
		<@var="h">  (llamada a función, opcional)

Devuelve un escalar con el resultado de una maximización numérica hecha con el método de Newton–Raphson. El argumento <@var="b"> debe contener los valores iniciales del conjunto de parámetros, y el argumento <@var="f"> debe indicar una llamada a la función que va a calcular el criterio objetivo (escalar) que quieres maximizar, dados los valores vigentes de los parámetros, así como cualquier otro dato relevante. Si lo que quieres realmente es minimizar el criterio objetivo, esta función debería devolver el valor negativo del mismo. Cuando se completa con éxito su ejecución, <@lit="NRmax"> devuelve el valor maximizado del criterio objetivo, y <@var="b"> va a contener los valores de los parámetros que proporcionan el máximo de ese criterio. 

El tercer y cuarto argumentos (opcionales) proporcionan modos de indicar, respectivamente, las derivadas analíticas y una matriz hessiana analítica (negativa). Las funciones a las que se refieren estos argumentos <@var="g"> y <@var="h"> deben tener, como primer elemento, una matriz definida con anterioridad que sea del rango correcto para poder contener el vector gradiente o la matriz hessiana, indicados en forma de puntero. Además, otro de sus elementos, debe ser el vector de parámetros (en forma de puntero o no). Otro tipo de elementos son opcionales. Si omites cualquiera de los argumentos opcionales (o los dos), se utiliza una aproximación numérica. 

Para más detalles y ejemplos, consulta <@pdf="El manual de gretl#chap:numerical"> (Capítulo 37). Ver también <@ref="BFGSmax">, <@ref="fdjac">. 

# NRmin numerical
Resultado: 	escalar 

Un alias de <@ref="NRmax">. Si invocas la función bajo este nombre, se ejecuta haciendo una minimización. 

# nullspace linalg
Resultado: 	matriz 
Argumento: 	<@var="A">  (matriz)

Devuelve una matriz con el cálculo del espacio nulo a la derecha correspondiente a la matriz <@var="A">, hecho mediante la descomposición en valores singulares: el resultado es una matriz <@mth="B"> que hace que el producto <@mth="AB"> sea una matriz nula. Como excepción, si la matriz <@var="A"> tiene rango completo por columnas, el resultado que se devuelve es una matriz vacía. Por otro lado, si <@var="A"> es de orden <@itl="m">×<@itl="n">, entonces <@mth="B"> va a ser <@mth="n"> por (<@mth="n"> – <@mth="r">), donde <@mth="r"> es el rango de <@var="A">. 

Si <@var="A"> no tiene rango completo por columnas, entonces al concatenar verticalmente la matriz <@var="A"> y la matriz traspuesta de <@var="B">, se genera una matriz con rango completo. 

Ejemplo: 

<code>          
      A = mshape(seq(1,6),2,3)
      B = nullspace(A)
      C = A | B'

      print A B C

      eval A*B
      eval rank(C)
</code>

produce... 

<code>          
      ? print A B C
      A (2 x 3)

      1   3   5
      2   4   6

      B (3 x 1)

      -0,5
         1
      -0,5

      C (3 x 3)

         1      3      5
         2      4      6
      -0,5      1   -0,5

      ? eval A*B
      -4,4409e-16
      -4,4409e-16

      ? eval rank(C)
      3
</code>

Ver también <@ref="rank">, <@ref="svd">. 

# numhess numerical
Resultado: 	matriz 
Argumentos:	<@var="b">  (vector columna)
		<@var="fcall">  (llamada a función)
		<@var="d">  (escalar, opcional)

Calcula una aproximación numérica a la matriz hessiana asociada al vector <@mth="n">-dimensional <@var="b">, y a la función objetivo que se especifique mediante el argumento <@var="fcall">. La llamada a la función debe tener <@var="b"> como primer argumento (bien directamente o bien en forma de puntero), seguido de cualquier argumento adicional que pueda ser necesario, y debe devolver como resultado un escalar. Al completarse con éxito <@lit="numhess"> devuelve una matriz <@itl="n">×<@itl="n"> que contiene la hessiana, y que es exactamente simétrica por construcción. 

El método utiliza la extrapolación de Richardson, con cuatro pasos. Puedes usar el tercer argumento (opcional) para establecer la fracción <@mth="d"> del valor del parámetro que se utiliza para determinar el tamaño del paso inicial. Cuando omites este argumento, por defecto va a ser <@mth="d"> = 0.01. 

Aquí tienes un ejemplo de su uso: 

<code>          
     matrix H = numhess(theta, myfunc(&theta, X))
</code>

Ver también <@ref="BFGSmax">, <@ref="fdjac">. 

# obs data-utils
Resultado: 	serie 

Devuelve una serie de números enteros consecutivos, correspondiendo el 1 con el inicio del conjunto de datos. Ten en cuenta que el resultado no va a depender de que tengas escogida una submuestra. Esta función es útil especialmente con conjuntos de datos de series temporales. Advertencia: Puedes escribir <@lit="t"> en lugar de <@lit="obs">, con el mismo efecto. 

Ver también <@ref="obsnum">. 

# obslabel data-utils
Resultado: 	cadena o array de cadenas 
Argumento: 	<@var="t">  (escalar o vector)

Si <@var="t"> es un escalar, devuelve una única cadena de texto que representa el marcador de etiquetado de la observación <@var="t">. Puedes realizar la operación inversa mediante la función <@ref="obsnum">. 

Si <@var="t"> es un vector, devuelve un 'array' de cadenas de texto que representan los marcadores de etiquetado de las observaciones indicadas por los elementos de <@var="t">. 

En cualquier caso, los valores <@var="t"> deben ser enteros que puedan resultar válidos como índices enteros de las observaciones en el conjunto de datos vigente; en otro caso, se muestra un aviso de fallo. 

# obsnum data-utils
Resultado: 	entero 
Argumento: 	<@var="s">  (cadena)

Devuelve el número entero que indica la observación que se corresponde con la cadena del argumento <@mth="s">. Ten en cuenta que el resultado no va a depender de que tengas escogida una submuestra. Esta función es útil con conjuntos de datos de series temporales. Por ejemplo, el siguiente código ... 

<code>          
     open denmark
     k = obsnum(1980:1)
</code>

... genera <@lit="k = 25">, indicando que el primer trimestre de 1980 es la vigésimo quinta observación de la base de datos <@lit="denmark">. 

Ver también <@ref="obs">, <@ref="obslabel">. 

# ok data-utils
Resultado: 	mira más abajo 
Argumento: 	<@var="x">  (escalar, serie, matriz o lista)

Cuando el argumento <@var="x"> es un escalar, esta función devuelve 1 se <@var="x"> no es <@lit="NA">, y 0 en otro caso. Cuando <@var="x"> es una serie, devuelve otra serie que toma el valor 1 en las observaciones en las que el argumento no tiene valores ausentes, y toma el valor cero en los demás. Si <@var="x"> es una lista, el resultado es una serie con 0 en las observaciones en las que al menos una serie de la lista tiene un valor ausente, y 1 en otro caso. 

Cuando el argumento <@var="x"> es una matriz, la función devuelve otra matriz de la misma dimensión que <@var="x">, con el valor 1 en las posiciones que se corresponden con elementos finitos de <@var="x">, y con el valor 0 en las posiciones en las que los elementos no son finitos (o bien infinitos, o bien “no números”, para el estándar IEEE 754). 

Ver también <@ref="missing">, <@ref="misszero">, <@ref="zeromiss">. Pero ten en cuenta que estas funciones no son aplicables a matrices. 

# onenorm linalg
Resultado: 	escalar 
Argumento: 	<@var="X">  (matriz)

Devuelve un escalar con la norma 1 de la matriz <@var="X">, es decir, el máximo de los resultados de sumar los valores absolutos de los elementos de <@var="X"> por columnas. 

Ver también <@ref="infnorm">, <@ref="rcond">. 

# ones matrix
Resultado: 	matriz 
Argumentos:	<@var="r">  (entero)
		<@var="c">  (entero, opcional)

Devuelve una matriz con <@mth="r"> filas y <@mth="c"> columnas, cubierta con valores iguales a 1. Si lo omites, el número de columnas se establece en 1 (vector columna), por defecto. 

Ver también <@ref="seq">, <@ref="zeros">. 

# orthdev panel
Resultado: 	serie 
Argumento: 	<@var="y">  (serie)

Se aplica tan solo si el conjunto vigente de datos tiene una estructura de panel, y devuelve una serie con el cálculo de las desviaciones ortogonales adelantadas para la variable <@var="y">. 

Algunas veces se utiliza esta transformación en lugar de la diferenciación para eliminar los efectos individuales de los datos de panel. Por compatibilidad con las primeras diferencias, las desviaciones se guardan adelantadas un paso de su localización temporal verdadera (es decir, el valor en la observación <@mth="t"> es la desviación que, expresándolo de manera estricta, pertenece a <@mth="t"> – 1). De este modo, se pierde la primera observación en cada serie temporal, no la última. 

Ver también <@ref="diff">. 

# pdf probdist
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="d">  (cadena)
		<@var="…">  (mira más abajo)
		<@var="x">  (escalar, serie o matriz)
Ejemplos: 	<@lit="f1 = pdf(N, -2.5)">
		<@lit="f2 = pdf(X, 3, y)">
		<@lit="f3 = pdf(W, forma, escala, y)">

Calcula el valor de la función de densidad de probabilidad, y devuelve un resultado (del mismo tipo que el argumento) con la densidad en <@var="x"> de la distribución identificada por el código <@var="d">. Consulta <@ref="cdf"> para obtener más detalles acerca de los argumentos (escalares) exigidos. Esta función <@lit="pdf"> acepta las distribuciones: Normal, <@mth="t"> de Student, Chi-cuadrado, <@mth="F">, Gamma, Beta, Exponencial, Weibull, Laplace, Error Generalizado, Binomial y Poisson. Ten en cuenta que para la Binomial y la Poisson, lo que se calcula de hecho es la masa de probabilidad en el punto especificado. Para <@mth="t"> de Student, Chi-cuadrado y <@mth="F"> también están disponibles sus variantes no centrales. 

Para la distribución Normal, consulta también <@ref="dnorm">. 

# pergm timeseries
Resultado: 	matriz 
Argumentos:	<@var="x">  (serie o vector)
		<@var="anchobanda">  (escalar, opcional)

Si solo indicas la serie o vector del primer argumento, se calcula su periodograma en la muestra. Si indicas el escalar del segundo argumento, calcula la estimación del espectro de <@var="x"> con una ventana de retardos de Bartlett con un ancho de banda igual a ese escalar, hasta un máximo igual a la mitad del número de observaciones (<@mth="T">/2). 

Devuelve una matriz con <@mth="T">/2 filas y dos columnas: la primera de estas contiene la frecuencia (ω) desde 2π/<@mth="T"> hasta π, y la segunda de las columnas contiene la densidad espectral correspondiente. 

# pexpand panel
Resultado: 	serie 
Argumentos:	<@var="v">  (vector)
		<@var="por_elemento">  (booleano, opcional)

Se aplica tan solo si el conjunto vigente de datos tiene una estructura de panel, y por defecto realiza la operación inversa a la que hace <@ref="pshrink">. Es decir, dado un vector que tiene una longitud igual al número de elementos de la muestra (de panel) vigente seleccionada, esta función devuelve una serie en la cual cada valor del argumento se repite <@mth="T"> veces, donde <@mth="T"> expresa la longitud temporal del panel. De este modo, la serie resultante es invariante con respecto al tiempo. 

Si indicas un valor para <@var="por_elemento"> que no sea cero, la longitud de <@var="v"> deberá ser igual a <@mth="T">, y la repetición se realiza a través de los elementos del panel. 

# pmax panel
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="máscara">  (serie, opcional)

Se aplica tan solo si el conjunto vigente de datos tiene una estructura de panel, y devuelve una serie que contiene cada uno de los valores máximos de la variable <@var="y"> en cada unidad de corte transversal (repitiéndolo en los períodos temporales de cada una de estas). 

Cuando indicas el segundo argumento (opcional), se van a ignorar aquellas observaciones en las que el valor de <@var="máscara"> sea igual a cero. 

Ver también <@ref="pmin">, <@ref="pmean">, <@ref="pnobs">, <@ref="psd">, <@ref="pxsum">, <@ref="pshrink">, <@ref="psum">. 

# pmean panel
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="máscara">  (serie, opcional)

Se aplica tan solo si el conjunto vigente de datos tiene una estructura de panel, y devuelve una serie que contiene cada una de las medias temporales de la variable <@var="y"> en cada unidad de corte transversal (repitiendo cada valor en los períodos temporales de cada una de estas). Las observaciones ausentes se ignoran al calcular las medias. 

Cuando indicas el segundo argumento (opcional), se van a ignorar aquellas observaciones en las que el valor de <@var="máscara"> sea igual a cero. 

Ver también <@ref="pmax">, <@ref="pmin">, <@ref="pnobs">, <@ref="psd">, <@ref="pxsum">, <@ref="pshrink">, <@ref="psum">. 

# pmin panel
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="máscara">  (serie, opcional)

Se aplica tan solo si el conjunto vigente de datos tiene una estructura de panel, y devuelve una serie que contiene cada uno de los valores mínimos de la variable <@var="y"> en cada unidad de corte transversal (repitiendo cada valor en los períodos temporales de cada una de estas). 

Cuando indicas el segundo argumento (opcional), se van a ignorar aquellas observaciones en las que el valor de <@var="máscara"> sea igual a cero. 

Ver también <@ref="pmax">, <@ref="pmean">, <@ref="pnobs">, <@ref="psd">, <@ref="pshrink">, <@ref="psum">. 

# pnobs panel
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="máscara">  (serie, opcional)

Se aplica tan solo si el conjunto vigente de datos tiene una estructura de panel, y devuelve una serie que contiene el número de observaciones válidas de la variable <@var="y"> en cada unidad de corte transversal (repitiéndolo en los períodos temporales de cada una de estas). 

Cuando indicas el segundo argumento (opcional), se van a ignorar aquellas observaciones en las que el valor de <@var="máscara"> sea igual a cero. 

Ver también <@ref="pmax">, <@ref="pmin">, <@ref="pmean">, <@ref="psd">, <@ref="pshrink">, <@ref="psum">. 

# polroots math
Resultado: 	matriz 
Argumento: 	<@var="a">  (vector)

Devuelve las raíces de un polinomio. Si el polinomio es de grado <@mth="p">, el vector <@var="a"> debe contener <@mth="p"> + 1 coeficientes en orden ascendente; es decir, comenzando con la constante y finalizando con el coeficiente de <@mth="x"><@sup="p">. 

El valor que se devuelve es un vector columna complejo con longitud igual a <@mth="p">. 

# polyfit transforms
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="q">  (entero)

Devuelve una serie, ajustando una tendencia polinómica de orden <@var="q"> a la serie del argumento <@var="y">, utilizando el método de polinomios ortogonales. La serie que se genera contiene los valores ajustados. 

# princomp stats
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="p">  (entero)
		<@var="matrizcov">  (booleano, opcional)

Sea <@var="X"> una matriz de orden <@itl="T">×<@itl="k">, que contiene <@mth="T"> observaciones sobre <@mth="k"> variables. El argumento <@var="p"> debe ser un número entero positivo menor que o igual a <@mth="k">. Esta función devuelve una matriz <@mth="P">, de orden <@itl="T">×<@itl="p">, que contiene las <@mth="p"> primeras componentes principales de <@var="X">. 

El tercer argumento (opcional) opera como un conmutador booleano: si no es cero, las componentes principales se calculan en base a la matriz de varianzas-covarianzas de las columnas de <@var="X"> (por defecto se utiliza la matriz de correlaciones). 

Los elementos de la matriz <@mth="P"> que se devuelve, se calculan como la suma desde <@mth="i"> hasta <@mth="k"> de <@mth="Z"><@sub="ti"> veces <@mth="v"><@sub="ji">, donde <@mth="Z"><@sub="ti"> representa el valor estandarizado (o simplemente el valor centrado, si utilizas la matriz de covarianzas) de la variable <@mth="i"> en la observación <@mth="t">, y <@mth="v"><@sub="ji"> representa el <@mth="j">-ésimo autovector de la matriz de correlaciones (o la matriz de covarianzas) entre las <@mth="X"><@sub="i">s, con los autovectores ordenados de acuerdo con los valores decrecientes de los autovalores correspondientes. 

Ver también <@ref="eigensym">. 

# prodc stats
Resultado: 	vector fila 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector fila con los productos de los elementos de cada columna de la matriz <@var="X">. Si indicas un valor no nulo para el segundo argumento (opcional), se ignoran los valores ausentes; de otra forma, el resultado es <@lit="NA"> para cada columna que tenga valores ausentes. Observa que indicar <@var="obviar_na"> es equivalente a tratar los valores como si fuesen 1s. 

Ver también <@ref="prodr">, <@ref="meanc">, <@ref="sdc">, <@ref="sumc">. 

# prodr stats
Resultado: 	vector columna 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector columna con los productos de los elementos de cada fila de la matriz <@var="X">. Si indicas un valor no nulo para el segundo argumento (opcional), se ignoran los valores ausentes; de otra forma, el resultado es <@lit="NA"> para cada fila que contenga valores ausentes. Observa que indicar <@var="obviar_na"> es equivalente a tratar los valores como si fuesen 1s. 

Ver también <@ref="prodc">, <@ref="meanr">, <@ref="sumr">. 

# psd panel
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="máscara">  (serie, opcional)

Se aplica tan solo si el conjunto vigente de datos tiene una estructura de panel, y devuelve una serie que contiene la desviación típica (muestral) de la variable <@mth="y">, en cada unidad de corte transversal (repitiendo cada valor en los períodos temporales de cada una de estas). El denominador que se utiliza es el tamaño de la muestra en cada unidad menos 1, excepto que solo haya 1 única observación válida para una unidad dada (pues en este caso se devuelve 0) o que no haya ninguna (en este caso se devuelve <@lit="NA">). 

Cuando indicas el segundo argumento (opcional), se van a ignorar aquellas observaciones en las que el valor de <@var="máscara"> sea igual a cero. 

Nota: Esta función permite comprobar si una variable cualquiera (por ejemplo, <@lit="X">) es invariante a lo largo del tiempo, por medio de la condición <@lit="max(psd(X)) == 0">. 

Ver también <@ref="pmax">, <@ref="pmin">, <@ref="pmean">, <@ref="pnobs">, <@ref="pshrink">, <@ref="psum">. 

# psdroot linalg
Resultado: 	matriz cuadradax 
Argumentos:	<@var="A">  (matriz simétrica)
		<@var="probapsd">  (booleano, opcional)

Devuelve la matriz cuadrada que resulta de aplicarle a la matriz simétrica <@var="A"> del argumento, una variante generalizada de la descomposición de Cholesky. La matriz del argumento debe ser semidefinida positiva (aunque puede ser singular) pero, si no es cuadrada, se muestra un mensaje de fallo. La simetría se asume y no se comprueba; solo se lee el triángulo inferior de <@var="A">. El resultado es una matriz triangular inferior, <@mth="L">, que cumple <@mth="A = LL'">. Los elementos indeterminados de la solución se establecen como iguales a cero. 

Para forzar la comprobación de que <@var="A"> es semidefinida positiva, indica un valor no nulo para el segundo argumento (opcional). En ese caso, se muestra un fallo si el máximo valor absoluto de <@mth="A – LL'"> es mayor que 1.0e-8. Este tipo de comprobación también puedes hacerla manualmente: 

<code>          
     L = psdroot(A)
     chk = maxc(maxr(abs(A - L*L')))
</code>

Para el caso en el que la matriz <@var="A"> es definida positiva, consulta <@ref="cholesky">. 

# pshrink panel
Resultado: 	matriz 
Argumento: 	<@var="y">  (serie)

Se aplica tan solo si el conjunto vigente de datos tiene una estructura de panel, y devuelve un vector que contiene cada una de las primeras observaciones válidas de la serie <@var="y"> en cada unidad de corte transversal del panel, a lo largo del rango de la muestra vigente. Si la serie tiene alguna unidad sin observaciones válidas, esa unidad se ignora. 

Esta función te proporciona un modo de compactar las series que te van a devolver algunas funciones tales como <@ref="pmax"> y <@ref="pmean">, en las que se repite un mismo valor en los diferentes períodos de tiempo de una misma unidad de corte transversal. 

Consulta <@ref="pexpand"> para la operación inversa. 

# psum panel
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="máscara">  (serie, opcional)

Se aplica tan solo si el conjunto vigente de datos tiene una estructura de panel, y devuelve una serie en la que cada valor es la suma de la variable <@var="y"> en los distintos períodos temporales de cada unidad de corte transversal. En cada una de estas, la suma así calculada se repite para cada período temporal. Las observaciones ausentes se ignoran en el cálculo de las sumas. 

Cuando indicas el segundo argumento (opcional), se van a ignorar aquellas observaciones en las que el valor de <@var="máscara"> sea igual a cero. 

Ver también <@ref="pmax">, <@ref="pmean">, <@ref="pmin">, <@ref="pnobs">, <@ref="psd">, <@ref="pxsum">, <@ref="pshrink">. 

# pvalue probdist
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="c">  (carácter)
		<@var="…">  (mira más abajo)
		<@var="x">  (escalar, serie o matriz)
Ejemplos: 	<@lit="p1 = pvalue(z, 2.2)">
		<@lit="p2 = pvalue(X, 3, 5.67)">
		<@lit="p2 = pvalue(F, 3, 30, 5.67)">

Calcula valores <@mth="P"> de probabilidad, y devuelve un resultado (del mismo tipo que el argumento) con la probabilidad <@mth="P(X > x)">, donde la distribución de probabilidad de <@mth="X"> se indica con la letra <@var="c">. Entre los argumentos <@var="d"> y <@var="p">, puedes necesitar algún argumento adicional escalar para especificar los parámetros de la distribución de que se trate. Para más detalles, consulta <@ref="cdf">. Las distribuciones soportadas por la función <@lit="pvalue"> son: Normal estándar, <@mth="t">, Chi-cuadrado, <@mth="F">, Gamma, Binomial, Poisson, Exponencial, Weibull, Laplace y Error Generalizado. 

Ver también <@ref="critical">, <@ref="invcdf">, <@ref="urcpval">, <@ref="imhof">. 

# pxnobs panel
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="máscara">  (serie, opcional)

Se aplica tan solo si el conjunto vigente de datos tiene una estructura de panel, y devuelve una serie que contiene el número de observaciones válidas de <@var="y"> en cada período de tiempo (el valor calculado se repite en cada una de las unidades de corte transversal). 

Cuando indicas el segundo argumento (opcional), se van a ignorar aquellas observaciones en las que el valor de <@var="máscara"> sea igual a cero. 

Ten en cuenta que esta función opera en la otra dimensión del panel, diferente a la de la función <@ref="pnobs">. 

# pxsum panel
Resultado: 	serie 
Argumentos:	<@var="y">  (serie)
		<@var="máscara">  (serie, opcional)

Se aplica tan solo si el conjunto vigente de datos tiene estructura de panel, y devuelve una serie en la que cada valor es la suma de <@var="y"> en las distintas unidades de corte transversal de cada período temporal. Las sumas así calculadas se repiten en cada unidad de corte transversal. 

Cuando indicas el segundo argumento (opcional), se van a ignorar aquellas observaciones en las que el valor de <@var="máscara"> sea igual a cero. 

Ten en cuenta que esta función opera en la otra dimensión del panel, diferente a la de la función <@ref="psum">. 

# qform linalg
Resultado: 	matriz 
Argumentos:	<@var="x">  (matriz)
		<@var="A">  (matriz simétrica)

Devuelve una matriz con el resultado de calcular la forma cuadrática <@mth="Y = xAx'">. Si la matriz simétrica <@var="A"> del argumento es de tipo genérico, cuando utilizas esta función en lugar de la típica multiplicación de matrices, garantizas una mayor rapidez y mejor precisión. Sin embargo, en el caso especial de que <@var="A"> sea una matriz identidad, la simple expresión <@lit="x'x"> resulta mucho mejor ca <@lit="qform(x',I(rows(x))">. 

En el caso especial de que <@var="A"> sea una matriz diagonal, puedes indicar el segundo argumento como un vector del tamaño apropiado, en el que se entiende que está contenida la diagonal principal de <@var="A">. En ese caso, se utiliza un algoritmo más eficiente. 

Si <@var="x"> y <@var="A"> no son matrices conformables, o si <@var="A"> no es simétrica, la función devuelve un fallo. 

# qlrpval probdist
Resultado: 	escalar 
Argumentos:	<@var="X2">  (escalar)
		<@var="gl">  (entero)
		<@var="p1">  (escalar)
		<@var="p2">  (escalar)

Devuelve un escalar con la probabilidad asociada (<@mth="P">) al valor del estadístico para hacer el contraste LR de Quandt (o sup-Wald) de cambio estructural en un punto desconocido (consulta <@xrf="qlrtest">), según <@bib="Bruce Hansen (1997);hansen97">. 

El primer argumento, <@var="X2">, indica el valor del estadístico de contraste de Wald máximo (en formato chi-cuadrado), y el segundo, <@var="gl">, indica sus grados de libertad. El tercer y el cuarto argumentos, representan los puntos de inicio y de fin del rango central de observaciones sobre el que se van a calcular los sucesivos estadísticos de Wald de los contrastes, y debes expresarlos como fracciones decimales en relación al rango total de estimación. Por ejemplo, si quieres adoptar el enfoque estándar de recorte del 15 por ciento, debes establecer <@var="p1"> igual a 0.15 y <@var="p2"> igual a 0.85. 

Ver también <@ref="pvalue">, <@ref="urcpval">. 

# qnorm probdist
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con los cuantiles de una Normal estándar que se corresponden con cada valor del argumento. Si <@var="x"> no está entre 0 y 1, se devuelve <@lit="NA">. Ver también <@ref="cnorm">, <@ref="dnorm">. 

# qrdecomp linalg
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="&R">  (referencia a matriz, o <@lit="null">)
		<@var="&P">  (referencia a matriz, o <@lit="null">)

Devuelve una matriz con el cálculo de una “tenue” descomposición QR de una matriz <@var="X"> de orden <@itl="m">×<@itl="n"> siendo <@mth="m"> ≥ <@mth="n">, de forma que <@mth="X = QR"> donde <@mth="Q"> es una matriz <@itl="m">×<@itl="n"> ortogonal, y <@mth="R"> es una matriz <@itl="n">×<@itl="n"> triangular superior. La matriz <@mth="Q"> se devuelve directamente, mientras que puedes obtener <@mth="R"> mediante el segundo argumento (opcional). 

Si indicas el tercer argumento (opcional), la descomposición utiliza el pivotado de columnas y, cuando se completa con éxito, <@var="P"> contiene la ordenación final de las columnas en forma de un vector fila. Si las columnas no están realmente reordenadas, <@var="P"> se va a equipar a <@ref="seq"><@lit="(1, n)">. 

Ver también <@ref="eigengen">, <@ref="eigensym">, <@ref="svd">. 

# quadtable stats
Resultado: 	matriz 
Argumentos:	<@var="n">  (entero)
		<@var="tipo">  (entero, opcional)
		<@var="a">  (escalar, opcional)
		<@var="b">  (escalar, opcional)

Devuelve una matriz <@itl="n">×2 para utilizar con la cuadratura Gaussiana (en integración numérica). La primera columna contiene los nodos o abscisas, y la segunda las ponderaciones. 

El primer argumento especifica el número de puntos (filas) que se van a calcular. El segundo argumento codifica el tipo de cuadratura: utiliza 1 para la Gauss–Hermite (la establecida por defecto); 2 para la Gauss–Legendre; o 3 para la Gauss–Laguerre. El sentido de los parámetros <@var="a"> y <@var="b"> (opcionales) depende del <@var="tipo"> seleccionado, como se explica a continuación. 

La cuadratura Gaussiana es un método para aproximar numéricamente la integral definida de alguna función que te interese. Supongamos que la función se representa mediante el producto <@mth="f(x)W(x)">. Los distintos tipos de cuadratura difieren en la especificación de la componente <@mth="W(x)">: en el caso de la Hermite esto es igual a exp(–<@mth="x"><@sup="2">); en el caso de la Laguerre es igual a exp(–<@mth="x">); y en el caso de la Legendre simplemente es <@mth="W(x)"> = 1. 

Para cada especificación de <@mth="W">, puede calcularse un conjunto de nodos (<@mth="x"><@sub="i">) y un conjunto de ponderaciones (<@mth="w"><@sub="i">), de tal modo que la suma desde <@mth="i">=1 hasta <@mth="n"> de <@mth="w"><@sub="i"> <@mth="f">(<@mth="x"><@sub="i">) se va a aproximar a la integral deseada. Para esto se va a utilizar el método de <@bib="Golub y Welsch (1969);golub69">. 

Cuando se selecciona el tipo de Gauss–Legendre, puedes utilizar los argumentos opcionales <@var="a"> y <@var="b"> para controlar los límites inferior y superior de la integración, siendo en este caso los valores por defecto –1 y 1. (En la cuadratura de Hermite, los límites están fijados en menos y más infinito; mientras que en el caso de la cuadratura de Laguerre, están fijados en 0 e infinito.) 

En el caso de Hermite, <@var="a"> y <@var="b"> juegan papeles diferentes: pueden utilizarse para substituir la forma por defecto de <@mth="W">(<@mth="x">) por la distribución Normal de probabilidad con media <@var="a"> y desviación típica <@var="b"> (con la que está estrechamente emparentada). Por ejemplo, si indicas los valores 0 y 1 para estos parámetros, respectivamente, vas a provocar que <@mth="W">(<@mth="x">) sea la función de densidad de probabilidad Normal estándar; lo que es equivalente a multiplicar los nodos por defecto por la raíz cuadrada de dos, y dividir las ponderaciones por la raíz cuadrada de π. 

# quantile stats
Resultado: 	escalar o matriz 
Argumentos:	<@var="y">  (serie o matriz)
		<@var="p">  (escalar entre 0 y 1)

Si <@var="y"> es una serie, devuelve un escalar que representa el cuantil <@var="p"> de la misma. Por ejemplo, cuando <@mth="p"> = 0.5, se devuelve la mediana. 

Si <@var="y"> es una matriz, devuelve un vector fila que contiene los <@var="p"> cuantiles de las diferentes columnas de <@var="y">; es decir, cada una de sus columnas se trata como una serie. 

Además, para una matriz <@var="y"> se admite una forma alternativa del segundo argumento: puedes indicar <@var="p"> como un vector. En ese caso, el valor que se te devuelve es una matriz de orden <@itl="m">×<@itl="n">, en la que <@var="m"> indica el número de elementos de <@var="p"> y <@var="n"> indica el número de columnas de <@var="y">. 

<@bib="Hyndman y Fan (1996);hyndman96"> describen nueve métodos distintos para calcular los cuantiles muestrales. En GRETL, por defecto, el método es el que ellos denominan <@mth="Q"><@sub="6"> (que también lo es en Python, por defecto). En cambio, puedes seleccionar los métodos <@mth="Q"><@sub="7"> (que es el usado por defecto en R) o <@mth="Q"><@sub="8"> (que es el recomendado por Hyndman y Fan) por medio de la instrucción <@xrf="set">, como en 

<code>          
     set quantile_type Q7 # o Q8
</code>

Por ejemplo, el código 

<code>          
     set verbose off
     matrix x = seq(1,7)'
     set quantile_type Q6
     printf "Q6: %g\n", quantile(x, 0.45)
     set quantile_type Q7
     printf "Q7: %g\n", quantile(x, 0.45)
     set quantile_type Q8
     printf "Q8: %g\n", quantile(x, 0.45)
</code>

produce el siguiente resultado: 

<code>          
     Q6: 3.6
     Q7: 3.7
     Q8: 3.63333
</code>

# randgen probdist
Resultado: 	serie 
Argumentos:	<@var="d">  (cadena)
		<@var="p1">  (mira más abajo)
		<@var="p2">  (escalar o serie, condicional)
		<@var="p3">  (escalar, condicional)
Ejemplos: 	<@lit="series x = randgen(u, 0, 100)">
		<@lit="series t14 = randgen(t, 14)">
		<@lit="series y = randgen(B, 0.6, 30)">
		<@lit="series g = randgen(G, 1, 1)">
		<@lit="series P = randgen(P, mu)">

Devuelve una serie calculada con un generador universal de números aleatorios. El argumento <@var="d"> es una cadena de texto (que generalmente está formada por un solo carácter) que permite especificar el tipo de distribución de probabilidad de la que se extraen los números pseudoaleatorios. Los argumentos de <@var="p1"> a <@var="p3"> especifican los parámetros de la distribución escogida; el número de estos parámetros (y, en algunos casos, su naturaleza) depende de esa distribución. 

Para otras distribuciones diferentes a la Beta-Binomial y a la Discreta genérica, los parámetros <@var="p1"> y (caso de ser aplicable) <@var="p2"> puedes indicarlos en formato de escalar o de serie. Cuando los utilizas en formato escalar, la serie que resulta procede de distribuciones idénticamente distribuidas. Cuando utilizas series para los argumentos <@var="p1"> o <@var="p2">, la serie resultante procede de distribuciones condicionadas al valor de los parámetros en cada observación. 

Los dos casos especiales tienen los siguientes requisitos: 

<indent>
• Beta-binomial: los tres parámetros deben ser escalares. 
</indent>

<indent>
• Discreta genérica: se necesita un único parámetro; en concreto un vector <@mth="k"> cuyos elementos representen las probabilidades de una variable aleatoria con valores enteros con soporte de 1 a <@mth="k">. 
</indent>

A continuación se indican detalles más específicos: el código de texto para cada tipo de distribución se muestra entre paréntesis, seguido de la interpretación del argumento <@var="p1"> y, cuando es aplicable, de las interpretaciones de <@var="p2"> y de <@var="p3">. 

<indent>
• Uniforme (continua) (u o U): mínimo, máximo 
</indent>

<indent>
• Uniforme (discreta) (i): mínimo, máximo 
</indent>

<indent>
• Normal (z, n o N): media, desviación típica 
</indent>

<indent>
• t de Student (t): grados de libertad 
</indent>

<indent>
• Chi-cuadrado (c, x o X): grados de libertad 
</indent>

<indent>
• F de Snedecor (f o F): grados de libertad (num.), grados de libertad (den.) 
</indent>

<indent>
• Gamma (g o G): forma, escala 
</indent>

<indent>
• Binomial (b o B): probabilidad, cantidad de ensayos 
</indent>

<indent>
• Poisson (p o P): media 
</indent>

<indent>
• Exponencial (exp): escala 
</indent>

<indent>
• Logística (lgt o s): posición, escala 
</indent>

<indent>
• Weibull (w o W): forma, escala 
</indent>

<indent>
• Laplace (l o L): media, escala 
</indent>

<indent>
• Error Generalizado (E): forma 
</indent>

<indent>
• Beta (beta): forma1, forma2 
</indent>

<indent>
• Beta-Binomial (bb): ensayos, forma1, forma2 
</indent>

<indent>
• Discreta genérica (disc): probabilidades 
</indent>

Ver también <@ref="normal">, <@ref="uniform">, <@ref="mrandgen">, <@ref="randgen1">. 

# randgen1 probdist
Resultado: 	escalar 
Argumentos:	<@var="d">  (carácter)
		<@var="p1">  (escalar)
		<@var="p2">  (escalar, condicional)
Ejemplos: 	<@lit="scalar x = randgen1(z, 0, 1)">
		<@lit="scalar g = randgen1(g, 3, 2.5)">

Funciona del mismo modo que <@ref="randgen"> excepto por el hecho de que devuelve un escalar en lugar de una serie. 

El primer ejemplo de arriba devuelve un valor extraído de la distribución Normal estándar, mientras que el segundo devuelve un valor extraído de la distribución Gamma con un parámetro de forma igual a 3 y de escala a 2.5. 

Ver también <@ref="mrandgen">. 

# randint probdist
Resultado: 	entero 
Argumentos:	<@var="min">  (entero)
		<@var="max">  (entero)

Devuelve un entero pseudoaleatorio en el intervalo cerrado [<@var="min">, <@var="max">]. Ver también <@ref="randgen">. 

# randperm probdist
Resultado: 	vector 
Argumentos:	<@var="n">  (entero)
		<@var="k">  (entero, opcional)

Si solo indicas el primer argumento, devuelve un vector fila que contiene una permutación aleatoria de los números enteros desde 1 hasta ese valor <@var="n">, sin repetición de elementos. Cuando indiques el segundo argumento, deberá ser un número entero positivo dentro del rango de 1 a <@var="n">; en ese caso la función devuelve un vector fila que contiene <@var="k"> número enteros seleccionados aleatoriamente desde 1 hasta <@var="n">, sin reemplazamiento. 

Si quieres extraer una muestra de <@mth="k"> filas de una matriz <@lit="X"> que tiene <@mth="n"> filas (y sin reemplazamiento), puedes conseguir eso tal como se muestra debajo: 

<code>          
     matrix S = X[randperm(n, k),]
</code>

Y si deseas mantener el orden original de las filas en la muestra: 

<code>          
     matrix S = X[sort(randperm(n, k)),]
</code>

Consulta también la función <@ref="resample"> para remuestrear con reemplazamiento. 

# randstr strings
Resultado: 	cadena 
Argumento: 	<@var="n">  (entero)

Devuelve una cadena de texto aleatoria de <@var="n"> bytes de ancho. En la cadena se van a incluir con la misma probabilidad los números que van de <@lit="0"> a <@lit="9">, y las letras minúsculas que van de <@lit="a"> hasta <@lit="f">; y se interpreta como un entero hexadecimal. Su uso previsto es como un identificador único. Por ejemplo, con <@var="n"> = 16, se genera una cadena de entre cerca de 10<@sup="19"> posibilidades, por lo que será única con una probabilidad próxima a 1. 

# rank linalg
Resultado: 	entero 
Argumentos:	<@var="X">  (matriz)
		<@var="tol">  (escalar, opcional)

Devuelve un entero con el rango de la matriz <@var="X"> de orden <@itl="r">×<@itl="c">, calculado numéricamente mediante la descomposición en valores singulares. 

El resultado de esta operación es el número de valores singulares de la matriz <@var="X"> que numéricamente se consideran mayores que 0. El parámetro opcional <@var="tol"> puedes usarlo para retocar este aspecto. Se va a considerar que los valores singulares no son nulos cuando resultan ser mayores que <@mth="m × tol × s">, donde <@mth="m"> es el mayor valor de entre <@mth="r"> y <@mth="c">, siendo <@mth="s"> lo que expresa el valor singular más grande. Cuando omites el segundo argumento, se establece que <@var="tol"> sea igual al épsilon de la máquina (consulta <@ref="$macheps">). En algunos casos, puedes querer establecer que <@var="tol"> sea un valor más grande (p.e. 1.0e-9) con el fin de evitar que se sobrestime el rango de la matriz <@var="X"> (lo que podría dar lugar a resultados numéricamente inestables). 

Ver también <@ref="svd">. 

# ranking stats
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="y">  (serie o vector)

Devuelve una serie o vector con las posiciones jerárquicas de los valores de <@mth="y">. La observación <@mth="i"> tiene una posición en la jerarquía que viene determinada por el número de elementos que son menores que <@mth="y"><@sub="i">, más la mitad del número de elementos que son iguales a <@mth="y"><@sub="i">. (Intuitivamente, puedes imaginarlo como la jerarquía en un torneo de ajedrez, en el que cada vitoria supone conceder un punto al ganador, y cada empate supone conceder medio punto). Se añade un 1 de forma que el número más pequeño para una posición es 1, y no 0. 

Ver también <@ref="sort">, <@ref="sortby">. 

# rcond linalg
Resultado: 	escalar 
Argumento: 	<@var="A">  (matriz cuadradax)

Devuelve un escalar con el número de condición recíproco de la matriz cuadrada <@var="A"> respecto a la norma 1. En muchos casos, este mide de forma más adecuada que el determinante, la sensibilidad de <@var="A"> a las operaciones numéricas tales como la inversión. 

El valor se calcula como el inverso (o recíproco) del resultado de multiplicar la norma 1 de la matriz cuadrada <@var="A">, por la norma 1 de la matriz inversa de <@var="A">. 

Ver también <@ref="det">, <@ref="ldet">, <@ref="onenorm">. 

# Re complex
Resultado: 	matriz 
Argumento: 	<@var="C">  (matriz compleja)

Devuelve una matriz real con la misma dimensión que <@var="C">, y que contiene la parte real de la matriz de ese argumento. Consulta también <@ref="Im">. 

# readfile strings
Resultado: 	cadena 
Argumentos:	<@var="nombrearchivo">  (cadena)
		<@var="código">  (cadena, opcional)

Si existe (y puede leerse) un archivo con el nombre del argumento <@var="nombrearchivo">, la función devuelve una cadena de texto que incluye el contenido de ese archivo; en caso contrario, indica un fallo. Si <@var="nombrearchivo"> no indica una especificación de la ruta completa al archivo, se va a buscar en varias localizaciones “probables”, comenzando por el directorio vigente en ese momento, <@xrf="workdir">. Si el archivo en cuestión está comprimido con gzip, se maneja del modo evidente. 

Si <@var="nombrearchivo"> comienza con un identificador de un protocolo de internet que sea admisible (<@lit="http://">, <@lit="ftp://"> o <@lit="https://">), se activa una orden a 'libcurl' para que descargue el recurso. Para otras operaciones de descarga más complicadas, consulta también <@ref="curl">. 

Cuando el texto que se quiere leer no está codificado en UTF-8, GRETL va a tratar de volver a codificarlo desde el tipo vigente de codificación local (si este no es UTF-8), o desde ISO-8859-15 en otro caso. Si este sencillo funcionamiento por defecto no cumple con tus necesidades, puedes usar el segundo argumento (opcional) para especificar un tipo de codificación. Por ejemplo, si quieres leer texto que está en el tipo de página de código Microsoft 1251, y este no es tu tipo de código local, deberás indicar <@lit=""cp1251""> como segundo argumento. 

Ejemplos: 

<code>          
        string web_page = readfile("http://gretl.sourceforge.net/")
        print web_page

        string current_settings = readfile("@dotdir/.gretl2rc")
        print current_settings
</code>

Consulta también las funciones <@ref="sscanf"> y <@ref="getline">. 

# regsub strings
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="s">  (cadena, array de cadenas o serie con cadenas de valores)
		<@var="hallada">  (cadena)
		<@var="substit">  (cadena)

Si <@var="s"> é una cadena única, devuelve una cadena de texto con una copia de <@var="s"> en la que todos los casos en los que ocurre el patrón <@var="hallada">, se substituyen por <@var="substit">. Los dos argumentos <@var="hallada"> y <@var="substit"> se interpretan como expresiones regulares de estilo Perl. Si <@var="s"> es un 'array' de cadenas de texto o una serie que contiene cadenas de valores, esta operación se realiza con cada una de las cadenas del 'array' o de la serie. 

Consulta también la función <@ref="strsub"> para la substitución simple de cadenas de texto. 

# remove data-utils
Resultado: 	entero 
Argumento: 	<@var="nombrearchivo">  (cadena)

Si el archivo del argumento <@var="nombrearchivo"> existe y si el usuario puede modificarlo, esta función lo elimina y devuelve un 0. Si no existe el archivo, o no puede eliminarse por alguna razón, la función devuelve un código no nulo indicando un fallo. 

Cuando <@var="nombrearchivo"> no especifica la ruta completa, entonces se asume que el archivo al que se refiere, está en el directorio vigente de trabajo (<@xrf="workdir">). 

# replace data-utils
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="x">  (serie o matriz)
		<@var="hallar">  (escalar o vector)
		<@var="substit">  (escalar o vector)

Devuelve un resultado (del tipo de) <@var="x"> cambiando sus elementos que sean iguales al elemento <@mth="i">-ésimo de <@var="hallar"> por el concordante de <@var="substit">. 

Cuando el segundo argumento (<@var="hallar">) es un escalar, el tercer argumento (<@var="substit">) también debe ser un escalar. Cuando ambos son vectores, deben tener el mismo número de elementos. Pero cuando <@var="hallar"> es un vector y <@var="substit"> es un escalar, entonces todas las coincidencias de aquel se substituyen en <@var="x"> por <@var="substit">. 

Ejemplo: 

<code>          
     a = {1,2,3;3,4,5}
     halla = {1,3,4}
     subst = {-1,-8, 0}
     b = replace(a, halla, subst)
     print a b
</code>

produce... 

<code>          
          a (2 x 3)

          1   2   3
          3   4   5

          b (2 x 3)

          -1    2   -8
          -8    0    5
</code>

# resample stats
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="x">  (serie o matriz)
		<@var="tamañobloque">  (entero, opcional)
		<@var="extracciones">  (entero, opcional)

La descripción inicial de esta función se refiere a los casos con datos de corte transversal o con series temporales; mira más abajo para los casos con datos de panel. 

Devuelve el resultado (del tipo del argumento) que se obtiene haciendo un remuestreo de <@var="x"> con reemplazamiento. Si el argumento es una serie, cada valor <@mth="y"><@sub="t"> de la serie que se devuelve, se obtiene de entre todos los valores de <@mth="x"><@sub="t"> que tienen la misma probabilidad. Cuando el argumento es una matriz, cada fila de la matriz que se devuelve, se obtiene de las filas de <@var="x"> que tienen la misma probabilidad. Consulta también <@ref="randperm"> para extraer una muestra de filas de una matriz sin reemplazamiento. 

El argumento <@var="tamañobloque"> (opcional) representa el tamaño del bloque para hacer el remuestreo moviendo bloques. Cuando se indique este argumento, deberá ser un entero positivo mayor o igual a 2. Como consecuencia, el resultado se va a componer por selección aleatoria con reemplazamiento, de entre todas las posibles secuencias contiguas de longitud <@var="tamañobloque"> del argumento. (En caso de que el argumento sea una matriz, esto significa filas contiguas.) Si la longitud de los datos no es un número entero que sea múltiplo del tamaño del bloque, el último bloque seleccionado se trunca para que se ajuste. 

<subhead>Número de extracciones</subhead> 

Por defecto, el número de observaciones que se vuelven a extraer para obtener el resultado es igual al del argumento indicado —si <@var="x"> fuese una serie, sería la longitud del rango muestral vigente; si <@var="x"> fuese una matriz, sería el número de sus filas. En el caso matricial, <@itl="solo"> puedes ajustar esto mediante el tercer argumento (opcional), que habrá de ser un número entero positivo. Ten en cuenta que si el argumento <@var="tamañobloque"> es mayor que 1, el argumento <@var="extracciones"> se refiere al número de observaciones individuales, no al número de bloques. 

<subhead>Datos de panel</subhead> 

Cuando el argumento <@var="x"> es una serie, y el conjunto de datos tiene formato de panel, no se admite hacer el muestreo por repetición moviendo bloques. La forma básica de hacer este tipo de muestreo está admitida, pero tiene su propia interpretación: se hace el muestreo por repetición de los datos “por individuo”. Supón que tienes un panel en el que se observan 100 individuos a lo largo de 5 períodos. Entonces, la serie que se devuelve también va a estar compuesta por 100 bloques de 5 observaciones: cada bloque va a obtenerse con igual probabilidad de las 100 series temporales individuales, conservándose el orden de las series temporales. 

# rgbmix data-utils
Resultado: 	array de cadenas 
Argumentos:	<@var="color1">  (cadena)
		<@var="color2">  (cadena)
		<@var="f">  (matriz)
		<@var="grafico">  (booleano, opcional)

Dados dos colores y un vector <@var="f"> de longitud <@mth="n"> que contenga valores en [0,1], esta función devuelve un 'array' de <@mth="n"> cadenas de texto, en el que su elemento <@mth="i"> contiene el código RGB hexadecimal para una mixtura de tipo (1-<@mth="f"><@sub="i">) × <@lit="color1"> + <@mth="f"><@sub="i"> × <@lit="color2">. La media ponderada se toma sobre los canales de Rojo, Verde y Azul de los colores de entrada. 

Puedes especificar los argumentos de color con nombres reconocidos por <@xrf="gnuplot">, o como valores hexadecimales con el formato <@lit="0xrrggbb"> o <@lit="#rrggbb">. En el primero de estos formatos, se pueden indicar numéricamente los valores hex; de otro modo, son necesarias las cadenas de texto. Si indicas un valor no nulo para el argumento <@var="grafico">, se produce un gráfico que muestra las mezclas de colores. 

Esta función ofrece un medio de generar un conjunto de colores relacionados con un propósito de representación gráfica, siendo su uso principal la especificación de múltiples franjas en un gráfico (por ejemplo, para indicar intervalos de confianza con más de un nivel). A continuación hay tres ejemplos: el primero genera sucesivos aclarados de un azul inicial; el segundo genera progresivos oscurecimientos de un tono rosa; y el tercero, una transición del rojo al amarillo. 

<code>          
     f = {0, 0.5, 0.75, 0.875, 0.9375}
     mezclas = rgbmix(0x1b43dc, "white", f, 1)
     print mezclas
     f = {0, 0.1, 0.2, 0.3, 0.4}
     rgbmix(0xefd0d3, "black", f, 1)
     f = {0, 0.2, 0.4, 0.6, 0.8, 1}
     rgbmix("red", "yellow", f, 1)
</code>

El resultado de la instrucción <@lit="print"> para el primer ejemplo será 

<code>          
     [1] "0x1b43dc"
     [2] "0x8da1ee"
     [3] "0xc6d0f6"
     [4] "0xe2e8fb"
     [5] "0xf1f3fd"
</code>

# round math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado, del tipo del argumento, que lo redondea al entero más próximo. Ten en cuenta que si <@mth="x"> está justo entre dos enteros, el redondeo se hace "alejándose de cero" de modo que, por ejemplo, 2.5 se redondea a 3, pero <@lit="round(-3.5)"> devuelve –4. Esta convención es común en software de hojas de cálculo, pero otro tipo de software puede generar resultados diferentes. Ver también <@ref="ceil">, <@ref="floor">, <@ref="int">. 

# rnameget strings
Resultado: 	cadena o array de cadenas 
Argumentos:	<@var="M">  (matriz)
		<@var="r">  (entero, opcional)

Si indicas el argumento <@var="r">, devuelve una cadena con el nombre de la fila <@var="r"> de la matriz <@var="M">. Si las filas de <@var="M"> no tienen nombre, entonces se devuelve una cadena vacía; y si <@var="r"> está fuera de los límites del número de filas de esta matriz, se muestra un fallo. 

Si no indicas el segundo argumento, devuelve un 'array' de cadenas de texto que contiene los nombres de las filas de <@var="M">, o un 'array' vacío si la matriz no tiene asignados nombres para sus filas. 

Ejemplo: 

<code>          
     matrix A = { 11, 23, 13 ; 54, 15, 46 }
     rnameset(A, "Primera Segunda")
     string name = rnameget(A, 2)
     print name
</code>

Ver también <@ref="rnameset">. 

# rnameset matrix
Resultado: 	entero 
Argumentos:	<@var="M">  (matriz)
		<@var="S">  (array de cadenas o lista)

Permite añadir nombres a las filas de una matriz <@var="M"> de orden <@itl="m">×<@itl="n">. Cuando el argumento <@var="S"> se refiere a una lista, los nombres se toman de las series de la lista (que deberá tener <@mth="m"> elementos). Cuando <@var="S"> es un 'array' de cadenas de texto, deberá tener <@mth="m"> elementos. Se admite también que indiques una única cadena de texto como segundo argumento; en este caso esta deberá tener <@mth="m"> subcadenas de texto separadas por espacios. 

Se devuelve el valor nominal 0 cuando las filas se nombran con éxito; en caso de fracaso, se muestra un fallo. Consulta también <@ref="cnameset">. 

Ejemplo: 

<code>          
     matrix M = {1, 2; 2, 1; 4, 1}
     strings S = array(3)
     S[1] = "Fila1"
     S[2] = "Fila2"
     S[3] = "Fila3"
     rnameset(M, S)
     print M
</code>

# rows matrix
Resultado: 	entero 
Argumento: 	<@var="X">  (matriz)

Devuelve un entero con el número de filas de la matriz <@var="X">. Ver también <@ref="cols">, <@ref="mshape">, <@ref="unvech">, <@ref="vec">, <@ref="vech">. 

# schur complex
Resultado: 	matriz compleja 
Argumentos:	<@var="A">  (matriz compleja)
		<@var="&Z">  (referencia a matriz, o <@lit="null">)
		<@var="&w">  (referencia a matriz, o <@lit="null">)

Realiza la descomposición de Schur de la matriz compleja <@var="A"> del argumento, devolviendo una matriz triangular superior compleja <@mth="T">. Cuando indicas un segundo argumento que non sea <@lit="null"> (nulo), recoge una matriz compleja <@mth="Z"> que contiene los vectores de Schur asociados a <@mth="A"> y <@mth="T">, tales que <@mth="A"> = <@mth="ZTZ"><@sup="H">. Cuando indicas el tercer argumento, recoge los autovalores de la matriz <@mth="A"> en un vector columna complejo. 

# sd stats
Resultado: 	escalar o serie 
Argumentos:	<@var="x">  (serie o lista)
		<@var="parcial">  (booleano, opcional)

Si <@var="x"> es una serie, la función devuelve un escalar con su desviación típica muestral, descartando las observaciones ausentes. 

Si <@var="x"> es una lista, la función devuelve una serie <@mth="y"> tal que <@mth="y"><@sub="t"> representa la desviación típica muestral de los valores de las series de la lista, en la observación <@mth="t">. Por defecto, la desviación típica se registra como <@lit="NA">, si hay algún valor ausente en <@mth="t">; pero si le das un valor no nulo a <@var="parcial">, cualquier valor no ausente se usará para crear el estadístico. 

Ver también <@ref="var">. 

# sdc stats
Resultado: 	vector fila 
Argumentos:	<@var="X">  (matriz)
		<@var="gl">  (escalar, opcional)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector fila con las desviaciones típicas de las columnas de la matriz <@var="X">. Si <@var="gl"> es positivo, se utiliza como divisor para las varianzas de las columnas; en otro caso, el divisor es igual al número de filas que tiene <@var="X"> (es decir, en ese caso no se aplica la corrección por los grados de libertad). Si indicas un valor no nulo para el tercer argumento (opcional), se ignoran los valores ausentes; de otra forma, el resultado es <@lit="NA"> para cada columna que contenga valores ausentes. Ver también <@ref="meanc">, <@ref="sumc">. 

# sdiff transforms
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="y">  (serie o lista)

Devuelve un resultado con el cálculo de las diferencias estacionales: <@mth="y(t) - y(t-k)">, donde <@mth="k"> indica la periodicidad del conjunto vigente de datos (consulta <@ref="$pd"> o <@ref="$panelpd">). Los valores iniciales se definen como <@lit="NA">. 

Cuando se devuelve una lista, cada variable individual de esta se nombra de forma automática siguiendo el patrón <@lit="sd_"><@var="nombrevar">, en el que <@var="nombrevar"> indica el nombre de la serie original. La parte original del nombre va a truncarse cuando así resulte necesario, e incluso podrá ajustarse para garantizar que sea único dentro del conjunto de nombres que así se vayan a construir. 

Ver también <@ref="diff">, <@ref="ldiff">. 

# seasonals data-utils
Resultado: 	lista 
Argumentos:	<@var="base">  (entero, opcional)
		<@var="centro">  (booleano, opcional)

Se aplica tan solo si el conjunto vigente de datos tiene una estructura de series temporales con periodicidad mayor que 1. Devuelve una lista con variables ficticias que representan cada período o estación, y que se nombran como <@lit="S1">, <@lit="S2">, etc. 

Utiliza el argumento <@var="base"> (opcional) para excluir de la lista a la variable ficticia que representa uno de los períodos. Por ejemplo, si le asignas un valor igual a 1 teniendo un conjunto de datos trimestrales, obtienes una lista que solo tiene las variables ficticias de los trimestres 2, 3 y 4. Si omites este argumento o es igual a 0, se generan variables ficticias para todos los períodos; y si no es cero, deberá ser un entero comprendido entre 1 y la periodicidad de los datos. 

El argumento <@var="centro">, si no es nulo, indica que las variables ficticias van a centrarse; es decir, sus valores van a calcularse restándole las medias en la población. Por ejemplo, con datos trimestrales, las variables ficticias estacionales centradas van a tener valores iguales a –0.25 y 0.75 en vez de 0 y 1. 

Con datos de frecuencia semanal, el resultado concreto depende de si los datos tienen fecha o no. Si tienen fecha, se crean hasta 53 series estacionales, basadas en el número de semana ISO 8601 (consulta <@ref="isoweek">); si no la tienen, el número máximo de series es 52 (y durante un período prolongado las series “estacionales” se van a desfasar con el año del calendario). En caso de disponer de datos semanales, si deseas generar series estacionales mensuales puedes hacerlo del siguiente modo: 

<code>          
     series month = $obsminor
     list months = dummify(month)
</code>

Para obtener más detalles, consulta <@ref="dummify">. 

# selifc matrix
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz)
		<@var="b">  (vector fila)

Devuelve una matriz tras seleccionar solo aquellas columnas de <@var="A"> en las que el elemento correspondiente de <@var="b"> no es nulo. El <@var="b"> debe ser un vector fila con el mismo número de columnas que <@var="A">. 

Ver también <@ref="selifr">. 

# selifr matrix
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz)
		<@var="b">  (vector columna)

Devuelve una matriz tras seleccionar solo aquellas filas de <@var="A"> en las que el elemento correspondiente de <@var="b"> no es nulo. El <@var="b"> debe ser un vector columna con el mismo número de filas que <@var="A">. 

Ver también <@ref="selifc">, <@ref="trimr">. 

# seq matrix
Resultado: 	vector fila 
Argumentos:	<@var="a">  (escalar)
		<@var="b">  (escalar)
		<@var="k">  (escalar, opcional)

Con solo dos argumentos, devuelve un vector fila con la secuencia creciente (sumando 1) desde <@var="a"> hasta <@var="b">, si el primer argumento es menor que el segundo; o con la secuencia decreciente (restando 1) si el primer argumento es mayor que el segundo. 

Si indicas el tercer argumento <@var="k"> (opcional), la función va a devolver un vector fila con la secuencia iniciada en <@var="a">, y ampliada (o disminuida en el caso inverso de que <@var="a"> sea mayor que <@var="b">) en <@var="k"> unidades a cada paso. La secuencia finaliza en el mayor valor posible que sea menor o igual a <@var="b"> (o en el menor valor posible que sea mayor o igual a <@var="b">, en el caso inverso). El argumento <@var="k "> debe ser positivo. 

Ver también <@ref="ones">, <@ref="zeros">. 

# setnote data-utils
Resultado: 	entero 
Argumentos:	<@var="b">  (bundle)
		<@var="clave">  (cadena)
		<@var="nota">  (cadena)

Inserta una nota descriptiva para un objeto que se identifica por la <@var="clave">, dentro de un 'bundle' <@var="b">. Se va a mostrar esa nota cuando se utilice la instrucción <@lit="print"> con el 'bundle'. Esta función devuelve un entero igual a 0 en caso de ejecutarse con éxito, y un valor no nulo en caso de fallo (por ejemplo, si no existe ningún objeto <@var="clave"> en el 'bundle' <@var="b">). 

# sgn math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve la función signo de <@var="x">; es decir, 0 si <@var="x"> es cero, 1 si <@var="x"> es positivo, –1 si <@var="x"> es negativo, o <@lit="NA"> si <@var="x"> es No Numérico. 

# simann numerical
Resultado: 	escalar 
Argumentos:	<@var="&b">  (referencia a matriz)
		<@var="f">  (llamada a función)
		<@var="maxit">  (entero, opcional)

Pone en práctica el templado simulado, que puede ser útil para mejorar la determinación del punto de partida de un problema de optimización numérica. 

Indicando el primer argumento, se establece el valor inicial de un vector de parámetros; e indicando el segundo argumento, se especifica una llamada a una función que devuelve el valor escalar de la función objetivo a maximizar. El tercer argumento (opcional) especifica el número máximo de iteraciones (que por defecto es de 1024). Cuando se completa con éxito, la función <@lit="simann"> devuelve un escalar con el valor final de la función objetivo a maximizar, y <@var="b"> contiene el vector de parámetros asociado. 

Para obtener más detalles y un ejemplo, consulta <@pdf="El manual de gretl#chap:numerical"> (Capítulo 37). Ver también <@ref="BFGSmax">, <@ref="NRmax">. 

# sin math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el seno de <@var="x">. Ver también <@ref="cos">, <@ref="tan">, <@ref="atan">. 

# sinh math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con el seno hiperbólico de <@var="x">. 

Ver también <@ref="asinh">, <@ref="cosh">, <@ref="tanh">. 

# skewness stats
Resultado: 	escalar 
Argumento: 	<@var="x">  (serie)

Devuelve un escalar con el valor del coeficiente de asimetría de la serie <@var="x">, descartando cualquier observación ausente. 

# sleep programming
Resultado: 	escalar 
Argumento: 	<@var="ns">  (escalar)

Esta función no tiene ningún uso directo en Econometría, pero puede ser de utilidad para comprobar métodos de computación en paralelo. Simplemente provoca que se “duerma” la línea de cómputo vigente (es decir, que se pare) durante <@var="ns"> segundos. El argumento debe ser un escalar no negativo. Al “despertar”, la función devuelve el escalar 0. 

# smplspan data-utils
Resultado: 	escalar 
Argumentos:	<@var="obsinicio">  (cadena)
		<@var="obsfin">  (cadena)
		<@var="pd">  (entero)

Devuelve el número de observaciones que hay contando desde <@var="obsinicio"> hasta <@var="obsfin"> (ambas incluidas), para datos de series temporales que tienen una frecuencia <@var="pd">. 

Deberías indicar los dos primeros argumentos en el formato que prefiere GRETL para datos de tipo anual, trimestral o mensual (por ejemplo, <@lit="1970">, <@lit="1970:1"> o <@lit="1970:01"> para cada una de esas frecuencias, respectivamente) o como fechas en el formato ISO 8601, <@lit="YYYY-MM-DD">. 

El argumento <@var="pd"> debe ser bien 1, 4 o 12 (datos anuales, trimestrales o mensuales), bien una de las frecuencias diarias (5, 6, 7), o bien 52 (semanal). Si <@var="pd"> es igual a 1, 4 o 12, entonces las fechas ISO 8601 se aceptan para los dos primeros argumentos, si indican el inicio del período en cuestión. Por ejemplo, <@lit="2015-04-01"> se admite en lugar de <@lit="2015:2"> para representar el segundo trimestre de 2015. 

Si ya tienes un conjunto de datos con frecuencia <@var="pd"> preparado, y con un rango suficiente de observaciones, entonces puedes imitar fácilmente el comportamiento de esta función utilizando la función <@ref="obsnum">. La ventaja de <@lit="smplspan"> consiste en que puedes calcular el número de observaciones sin necesidad de tener preparado un conjunto apropiado de datos (ni ningún conjunto de datos). A continuación, un ejemplo: 

<code>          
     scalar T = smplspan("2010-01-01", "2015-12-31", 5)
     nulldata T
     setobs 5 2010-01-01
</code>

Esto genera 

<code>          
     ? scalar T = smplspan("2010-01-01", "2015-12-31", 5)
     Se ha generado el escalar T = 1565
     ? nulldata T
     Periodicidad: 1, máx. obs: 1565
     Rango de observaciones: 1 a 1565
     ? setobs 5 2010-01-01
     Rango completo de datos: 2010-01-01 - 2015-12-31 (n = 1565)
</code>

Después de lo anterior, puedes tener confianza en que la última observación del conjunto de datos que se va a generar por medio de <@xrf="nulldata"> va a ser <@lit="2015-12-31">. Ten en cuenta que el número 1565 sería más bien complicado calcularlo de otro modo. 

# sort matrix
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (serie, vector o array de cadenas)

Devuelve un resultado del tipo de <@var="x"> con los valores ordenados de forma ascendente. Las observaciones con valores ausentes se descartan cuando <@mth="x"> es una serie, pero se ordenan al final si <@mth="x"> es un vector. Ver también <@ref="dsort">, <@ref="values">. Para matrices, en especial, consulta <@ref="msortby">. 

# sortby stats
Resultado: 	serie 
Argumentos:	<@var="y1">  (serie)
		<@var="y2">  (serie)

Devuelve una serie que contiene los elementos de <@var="y2"> ordenados de acuerdo con los valores crecientes del primer argumento <@var="y1">. Ver también <@ref="sort">, <@ref="ranking">. 

# sphericorr stats
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="modo">  (entero)
		<@var="&J">  (referencia a matriz, o <@lit="null">)

Permite hacer la representación en coordenadas esféricas de una matriz de correlaciones, o la operación inversa, dependiendo del valor del parámetro <@var="modo">. 

Cuando se omite <@var="modo">, o es igual a 0, se asume que <@var="X"> es una matriz de correlaciones de orden <@itl="n">×<@itl="n">. El valor que se devuelve es un vector que tiene <@mth="n(n-1)/2"> elementos entre 0 y π. En este modo, se ignora la referencia a <@var="J">. 

Cuando <@var="modo"> es igual a 1 o a 2, se realiza la transformación inversa, por lo que <@var="X"> debe ser un vector que tenga <@mth="n(n-1)/2"> elementos entre 0 y π. El valor que se devuelve ahora es la matriz <@mth="R"> de correlaciones cuando la opción <@var="modo"> es igual a 1; o su factor <@mth="K"> de Cholesky cuando <@var="modo"> es igual a 2. En estos casos, cuando se indica, el puntero opcional a la matriz <@var="J"> permite recuperar el Jacobiano de vech(<@mth="R">) o de vech(<@mth="K">) con respecto a <@mth="X">. 

Ten en cuenta que la representación en coordenadas esféricas hace muy sencillo el cálculo del log-determinante de la matriz de correlaciones <@mth="R">: 

<code>          
    omega = sphericorr(X)
    log_det = 2 * sum(log(sin(omega)))
</code>

# sprintf strings
Resultado: 	cadena 
Argumentos:	<@var="formato">  (cadena)
		... (mira más abajo)

Devuelve una cadena de texto (“string”) que se construye representando los valores de los argumentos (indicados por los puntos de arriba) que acompañan a la instrucción, bajo el control del argumento <@var="formato">. Tiene la intención de darte gran flexibilidad para crear cadenas de texto. Utiliza <@var="formato"> para indicar el modo preciso en el que quieres que se presenten los argumentos. 

En general, el argumento <@var="formato"> debe ser una expresión que se corresponda con una cadena de texto, pero en la mayoría de los casos solo va a ser una cadena de texto literal (una secuencia alfanumérica entrecomillada). Algunas secuencias de caracteres de formato tienen un significado especial: aquellas que comienzan con el símbolo (%) se interpretan como “comodines” para los elementos que contiene la lista de argumentos. Además, caracteres especiales (por ejemplo, el de nueva línea) se representan por medio de una combinación de símbolos que comienza con una barra diagonal inversa. 

Por ejemplo, el código de abajo... 

<code>          
     scalar x = sqrt(5)
     string claim = sprintf("sqrt(%d) es (aproximadamente) %6.4f.\n", 5, x)
     print claim
</code>

va a producir... 

<code>          
     sqrt(5) es (aproximadamente) 2.2361.
</code>

La expresión <@lit="%d"> en la cadena de formato, indica que se quiere un número entero en ese preciso lugar de la salida que se va a presentar, y dado que esa es la expresión con el símbolo “por ciento” que está más a la izquierda, se empareja con el primer argumento, es decir 5. La segunda secuencia especial es <@lit="%6.4f">, y representa un valor con 6 dígitos de ancho como mínimo, y con 4 dígitos después del separador decimal. El número de esas secuencias debe coincidir con la cantidad de argumentos que acompañan a la cadena de texto para el formato. 

Consulta la página de ayuda de la instrucción <@xrf="printf"> para obtener más detalles en relación con la sintaxis que puedes utilizar en las cadenas de texto para el formato. 

# sqrt math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado, del mismo tipo que <@var="x">, con la raíz cuadrada positiva de este. Genera <@lit="NA"> para valores negativos de este. 

Observa que, si el argumento es una matriz, se realiza la operación para cada elemento. Para la “raíz cuadrada matricial” consulta <@ref="cholesky">. 

# square transforms
Resultado: 	lista 
Argumentos:	<@var="L">  (lista)
		<@var="productos-cruz">  (booleano, opcional)

Devuelve una lista que contiene los cuadrados de las variables de la lista <@var="L">, con sus elementos nombrados de acuerdo con el siguiente patrón :<@lit="sq_"><@var="nombrevariable">. Cuando indicas el segundo argumento (opcional) y tiene un valor no nulo, la lista también va a incluir los productos cruzados de los elementos de la lista <@var="L">, que se nombrarán de acuerdo con el formato del patrón <@var="var1"><@lit="_"><@var="var2">. De ser necesario, el nombre de las series de los argumentos va a truncarse e incluso ajustarse el nombre del resultado final, para evitar la duplicación de nombres en la lista que se devuelve. 

# sscanf strings
Resultado: 	entero 
Argumentos:	<@var="origen">  (cadena o array de cadenas)
		<@var="formato">  (cadena)
		... (mira más abajo)

Lee valores indicados por el argumento <@var="origen"> bajo el control del argumento <@var="formato">, y asigna estos valores a uno o más de los argumentos siguientes, indicados por los puntos de arriba. Devuelve un entero con el número de valores que se asignan. Esta función es una versión simplificada de la función <@lit="sscanf"> del lenguaje C de programación, con una extensión para escanear una matriz entera, y que se describe más abajo bajo el título “Escaneando una matriz”. Ten en cuenta que indicar una formación de cadenas de texto como <@var="origen"> solo se acepta en caso de que escanees una matriz. 

Como argumento <@var="origen"> puedes usar una cadena de texto literal entrecomillada, o bien el nombre de una cadena de texto que hayas definido previamente. El argumento <@var="formato"> se indica de modo similar a la cadena del argumento “formato” en <@xrf="printf"> (mira más abajo); en esta última función, <@var="elementos"> debe ser una lista de variables definidas antes, separadas por comas y que son los objetivos de la conversión de <@var="origen">. (Para los acostumbrados a C: podéis fijar previamente los nombres de las variables numéricas con <@lit="&">, pero no es necesario.) 

El texto literal en el argumento <@var="formato"> se compara con <@var="origen">. Los elementos que especifican la conversión empiezan con el carácter <@lit="%">, y las conversiones que están admitidas incluyen: <@lit="%f">, <@lit="%g"> o <@lit="%lf"> para números de punto flotante; <@lit="%d"> para números enteros; y <@lit="%s"> para cadenas de texto. Puedes insertar un entero positivo después del símbolo de porcentaje, que establece el número máximo de caracteres que se van a leer para la conversión indicada. Como forma alternativa, puedes insertar un carácter literal de asterisco, <@lit="*">, después del símbolo de porcentaje para eliminar la conversión (saltándose así cualquier carácter que, de otro modo, podría haberse convertido al tipo indicado). Por ejemplo, la expresión <@lit="%3d"> convierte los siguientes 3 caracteres de <@var="origen"> en un entero, en caso de que sea posible; y la expresión <@lit="%*g"> permite saltarse tantos caracteres de <@var="origen"> como los que podrían convertirse en un número de punto flotante simple. 

Además de la conversión <@lit="%s"> para cadenas de texto, también está disponible una versión simplificada del formato C <@lit="%"><@var="N"><@lit="["><@var="chars"><@lit="]">. En este formato, <@var="N"> representa el número máximo de caracteres que se van a leer, y <@var="chars"> expresa un conjunto de caracteres que sean admisibles, expresados entre corchetes: el proceso de lectura finaliza cuando se alcanza <@var="N">, o cuando se encuentra un carácter que no está en <@var="chars">. Puedes cambiar el funcionamiento de <@var="chars">indicando el circunflejo <@lit="^"> como primer carácter; en ese caso, el proceso de lectura finaliza cuando se encuentra un carácter que está indicado en el conjunto. (A diferencia de lo que sucede en C, el guion no juega ningún papel especial en el conjunto <@var="chars">.) 

Si la cadena de texto del origen no coincide (exactamente) con el formato, el número de conversiones puede quedarse corta respecto al número de argumentos indicados. Esto no es por si mismo un fallo en lo que atañe a GRETL. Así y todo, podrías querer comprobar el número de conversiones que se completaron; esto se indica en el valor que se devuelve Some simple examples follow: 

<code>          
     # Escaneando valores escalares
     scalar x
     scalar y
     sscanf("123456", "%3d%3d", x, y)
     # Escaneando valores de cadena de texto
     string s = "uno dos"
     string s1
     string s2
     sscanf(s, "%s %s", s1, s2)
     print s1 s2
</code>

<subhead>Escaneando una matriz</subhead> 

El escaneado de matrices debe señalarse mediante la especificación especial de conversión, “<@lit="%m">”. Puedes indicar el número máximo de filas a leer, insertando un número entero entre el signo “<@lit="%">” y la “<@lit="m">” indicativa de matriz. Se permiten dos variantes: que <@var="origen"> indique una cadena de texto única que represente una matriz, y que <@var="origen"> indique una formación de cadenas de texto. Estas opciones se describen por turno. 

Si <@var="origen"> es un argumento de cadena de texto única, el escáner lee una línea de la entrada y cuenta el número de campos numéricos (separados por espacios o por tabuladores). Esto define el número de columnas de la matriz. Por defecto, el proceso de lectura continúa con todas las líneas (filas) que contengan el mismo número de columnas numéricas, pero el número máximo de filas puede limitarse mediante el valor entero opcional mencionado antes. 

Si <@var="origen"> es una formación de cadenas de texto, el resultado va a ser forzosamente un vector columna, del que cada elemento va a ser la conversión numérica de la cadena correspondiente, o <@lit="NA"> si la cadena de texto no representa un número. A continuación, tienes varios ejemplos: 

<code>          
     # Escaneando una única cadena de texto
     string s = sprintf("1 2 3 4\n5 6 7 8")
     print s
     matrix m
     sscanf(s, "%m", m)
     print m
     # Escaneando una formación de cadenas de texto
     strings S = defarray("1.1", "2.2", "3.3", "4.4", "5.5")
     sscanf(S, "%4m", m)
     print m
</code>

# sst stats
Resultado: 	escalar 
Argumento: 	<@var="y">  (serie o vector)

Devuelve un escalar con la suma de los cuadrados de las desviaciones respecto a la media (SCT), de las observaciones no ausentes de la serie o vector <@var="y">. Ver también <@ref="var">. 

# stack panel
Resultado: 	serie 
Argumentos:	<@var="L">  (lista)
		<@var="n">  (entero)
		<@var="desplazamiento">  (entero, opcional)

Diseñado para el manejo de datos con formato de series de tiempo apiladas, que necesita GRETL para datos de panel. El valor que se devuelve es una serie que se consigue apilando de forma “vertical”, grupos de <@var="n"> observaciones de cada serie de la lista <@var="L">. Por defecto, se usan las primeras <@var="n"> observaciones (ello se corresponde con <@var="desplazamiento"> = 0), pero puedes trasladar el punto de inicio indicando un valor positivo para <@var="desplazamiento">. Si la serie resultante fuese más larga que el conjunto de datos vigente, se añaden tantas observaciones como sean necesarias. 

Con esta función puedes manejar el caso de un archivo de datos que tiene series de tiempo colocadas unas al lado de otras, para un grupo de unidades de sección cruzada. Y también cuando se considera el tiempo en sentido horizontal, y cada fila representa una unidad atemporal. 

Consulta la sección titulada “Panel data specifics” en <@pdf="El manual de gretl#chap:datafiles"> (Capítulo 4) para obtener detalles y ejemplos de su utilización. 

# stdize transforms
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="X">  (serie, lista o matriz)
		<@var="v">  (entero, opcional)
		<@var="obviar_na">  (booleano, opcional)

Por defecto, devuelve un resultado del mismo tipo que el argumento, con la versión tipificada de esa serie, lista o matriz: el argumento se centra y se divide por su desviación típica muestral (con corrección de 1, en los grados de libertad). En caso de que el argumento sea una matriz, los resultados se calculan por columnas. 

Puedes usar el segundo argumento (opcional) para modular el resultado. Un valor no negativo de ese <@var="v"> permite configurar la corrección en los grados de libertad que se utilizan para la desviación típica; así <@var="v"> = 0 solicita utilizar el estimador máximo-verosímil. Como caso especial, si estableces que <@var="v"> sea igual a –1, únicamente se va a centrar el primer argumento. 

Por defecto, los valores ausentes se obvian en el caso de una entrada de tipo serie o lista, pero no en el caso de una entrada de tipo matriz. Para que se ignoren los valores ausentes en el caso matricial, indica un valor no nulo para <@var="obviar_na">. 

# strfday calendar
Resultado: 	depende de la entrada 
Argumentos:	<@var="dia_epoca">  (escalar, serie o matriz)
		<@var="formato">  (cadena, opcional)

Esta función opera como <@ref="strftime">, convirtiendo un valor numérico en una cadena de texto regida por <@var="formato">, excepto cuando la entrada sea un “día de época” (para cuya definición puedes consultar la función <@ref="epochday">). Dado que la resolución es de tipo diario, solo se manejan formatos relacionados con fechas; los formatos relacionados con tiempo proporcionan resultados indefinidos. 

Si omites el segundo argumento, el formato por defecto se establece en ISO 8601 extendido, <@lit="YYYY-MM-DD">. 

# strftime calendar
Resultado: 	depende de la entrada 
Argumentos:	<@var="tm">  (escalar, serie o matriz)
		<@var="formato">  (cadena, opcional)
		<@var="desplazamiento">  (escalar, opcional)

El argumento <@var="tm"> se utiliza para proporcionar el “tiempo Unix”, es decir el número de segundos desde el comienzo del año 1970, de acuerdo con el UTC. El valor que se devuelve es una cadena de texto que proporciona la fecha y/u hora correspondiente, bien en un formato especificado mediante el segundo argumento (opcional) o bien, por defecto, mediante la “representación preferida de fecha y hora en el entorno local vigente” tal como determinaría la biblioteca del sistema C. Mira más abajo para más detalles sobre la especificación del formato. 

Puedes usar el argumento <@var="desplazamiento"> (opcional) para especificar un desplazamiento en segundos en relación a UTC, seleccionando así una zona horaria diferente a la predeterminada (que es siempre la hora local). Por ejemplo, un desplazamiento de 3600 selecciona la Hora Central Europea, mientras que 0 selecciona la GMT. El valor absoluto de <@var="desplazamiento"> no debe exceder de 86400 (24 horas). 

El tipo concreto que se devuelve, depende del tipo de <@var="tm">: cuando <@var="tm"> es un escalar, un vector o una serie, el resultado es una única cadena de texto, un 'array' de cadenas o una serie con cadenas de valores, respectivamente. 

Puedes obtener valores apropiados de <@var="tm"> para utilizar con esta función mediante el accesor <@ref="$now"> o la función <@ref="strptime">. 

Ten en cuenta que mientras que <@var="tm"> se considera en relación al UTC, el resultado predeterminado de esta función es “local” (en relación con la zona horaria establecida en el ordenador servidor). Por eso, un mismo <@var="tm"> va a mostrar un tiempo distinto (y quizás una distinta fecha) en zonas horarias diferentes. Pero si quieres una cadena de texto que represente el UTC en lugar del tiempo local, GRETL puede hacerlo; mira más abajo. 

<subhead>Opciones de formato</subhead> 

Puedes encontrar las opciones típicas de formato consultando la página sobre <@lit="strftime"> del manual, en los sistemas que tengan esas páginas; o por medio de uno de los muchos sitios web que presentan información relevante, como por ejemplo <@url="https://devhints.io/strftime">. Además de los formatos típicos, GRETL reconoce una opción especial : si <@var="formato"> es “<@lit="8601">” simplemente, la fecha y el tiempo se presentan en el formato ISO 8601. 

# stringify strings
Resultado: 	entero 
Argumentos:	<@var="y">  (serie)
		<@var="S">  (array de cadenas)

Proporciona un modo de definir valores de cadena de texto para la serie <@var="y">. Para que esto funcione, deben cumplirse dos condiciones: la serie objetivo no debe tener otra cosa que no sean valores enteros positivos (ninguno de ellos menor que 1); y el 'array' <@var="S"> debe tener por lo menos <@mth="n"> elementos, siendo <@mth="n"> el mayor valor de <@var="y">. Además, cada elemento de <@var="S"> debe tener un formato UTF-8 válido. Si no se cumple alguna de estas condiciones, se presenta un fallo. El valor nominal que devuelve esta función es cero, al completarse con éxito. 

Una alternativa a <@lit="stringify"> que puede ser de utilidad en algunos contextos es la asignación directa de un 'array' de cadenas de texto a una serie: esto genera una serie cuyos valores se toman de la serie de forma secuencial; el número de elementos del 'array' debe ser igual a la longitud total del conjunto de datos o a la longitud del rango de la muestra vigente, y los valores se pueden repetir como sea necesario. 

Ver también <@ref="strvals">, <@ref="strvsort">. 

# strlen strings
Resultado: 	entero 
Argumento: 	<@var="s">  (cadena o array de cadenas)

Cuando <@var="s"> es una cadena de texto simple, devuelve el número de caracteres UTF-8 que contiene. Ten en cuenta que esto no es igual al número de bytes, si algunos caracteres están fuera del intervalo de impresión ASCII. Cuando desees obtener el número de bytes, puedes usar la función <@ref="nelem">. Por ejemplo: 

<code>          
     string s = "¡Olé!"
     printf "strlen(s) = %d, nelem(s) = %d\n", strlen(s), nelem(s)
</code>

debería devolver 

<code>          
     strlen(s) = 5, nelem(s) = 7
</code>

Si el argumento es un 'array' de cadenas de texto, el valor que se devuelve es un vector columna que contiene el número de caracteres de cada cadena. También se acepta que uses como argumento una serie con cadenas de valores; en ese caso, el valor que se devuelve es una serie que contiene el largo de las cadenas de valores a lo largo del rango muestral vigente. 

# strncmp strings
Resultado: 	entero 
Argumentos:	<@var="s1">  (cadena)
		<@var="s2">  (cadena)
		<@var="n">  (entero, opcional)

Compara las dos cadenas de texto de los argumentos, y devuelve un entero que es menor, igual o mayor que 0 cuando <@var="s1"> es (respectivamente) menor, igual o mayor que <@var="s2">, hasta los <@var="n"> primeros caracteres. Cuando se omite <@var="n">, la comparación continúa hasta donde resulte posible. 

Ten en cuenta que, si solo quieres comprobar si dos cadenas de texto son iguales, puedes hacerlo sin necesidad de utilizar esta función, como con la indicación <@lit="if (s1 == s2)...">. 

# strpday calendar
Resultado: 	depende de la entrada 
Argumentos:	<@var="s">  (cadena, array de cadenas o serie con cadenas de valores)
		<@var="formato">  (cadena, opcional)

Esta función se comporta similarmente a la función <@ref="strptime">, excepto en que el valor que se devuelve es de tipo “día de época” (para cuya definición puedes consultar <@ref="epochday">). Dado que la solución es de tipo diario, se ignora cualquier información que haya en <@var="s">, sobre el momento del día. 

# strptime calendar
Resultado: 	depende de la entrada 
Argumentos:	<@var="s">  (cadena, array de cadenas o serie con cadenas de valores)
		<@var="formato">  (cadena, opcional)

Esta función es la recíproca de <@ref="strftime">. Analiza una o más cadenas de texto que expresan tiempo o fecha, utilizando el <@var="formato"> especificado; y devuelve el número de segundos transcurridos desde el inicio de 1970 según el Tiempo Universal Coordinado (UTC). El tipo concreto de valor que se devuelve depende del de <@var="s">: si <@var="s"> es una cadena de texto, un 'array' de cadenas o una serie con cadenas de valores, el resultado es un escalar, un vector columna o una serie numérica, respectivamente. 

Si omites la opción <@var="formato">, este se establece por defecto en ISO 8601 “extendido”, <@lit="YYYY-MM-DD"> (lo que se traduce a “<@lit="%Y-%m-%d">” como formato de strptime). 

Como caso especial, puedes indicar el primer argumento como un entero de 8 dígitos, conforme al formato de fechas “básico” de ISO 8601, <@lit="YYYYMMDD"> (o un vector o serie que contenga esos valores). En ese caso, deberás omitir la opción <@var="formato">. 

Ten en cuenta que el primer argumento de esta función se considera en relación con la zona horaria que está establecida en el ordenador servidor. Entonces, por ejemplo, la llamada 

<code>          
     strptime("13/02/2009 23:31.30", "%d/%m/%Y %H:%M.%S")
</code>

va a devolver como resultado 1234567890, si el tiempo de tu sistema está establecido en UTC; pero si lo está en la zona horaria de Europa Central (UTC+01:00), el resultado será 1234564290. 

Puedes encontrar las opciones de <@var="formato"> si consultas la página sobre <@lit="strptime"> del manual, en sistemas que dispongan de las mismas; o por medio de uno de los muchos sitios web que presentan información relevante, como por ejemplo <@url="http://man7.org/linux/man-pages/man3/strptime.3.html">. 

El ejemplo de abajo muestra como puedes convertir información de fechas de uno a otro formato. 

<code>          
     scalar tm = strptime("Sunday 17/02/19", "%A %d/%m/%y")
     eval strftime(tm) # Resultado por defecto
     eval strftime(tm, "%A, %d de %B de %Y")
</code>

En el entorno local de España, el resultado es 

<code>          
     07/02/2019 0:00:00
     febrero 07, 2019
</code>

# strsplit strings
Resultado: 	cadena o array de cadenas 
Argumentos:	<@var="s">  (cadena)
		<@var="sep">  (cadena, opcional)
		<@var="i">  (entero, opcional)

En su funcionamiento básico, con un único argumento, devuelve el 'array' de cadenas de texto que resulta al separar el contenido de <@var="s"> conforme a los espacios vacíos que tiene (es decir, conforme a cualquier combinación de los caracteres de espacio, tabulación y/o línea nueva). 

Puedes utilizar el segundo argumento (opcional) para especificar el separador que se usa para separar <@var="s">. Por ejemplo... 

<code>          
     string Cesta = "Plátano,Manzana,Yaca,Naranja"
     strings S = strsplit(Cesta,",")
</code>

va a separar el primer argumento de la función en un 'array' de cuatro cadenas de texto, usando la coma como elemento separador. 

Las secuencias de barra diagonal izquierda para escapar, indicadas mediante “<@lit="\n">”, “<@lit="\r">” y “<@lit="\t">”, se considera que representan una línea nueva, un salto de línea y una tabulación cuando se indican en el argumento opcional <@var="sep">. Si quieres incluir una barra diagonal izquierda literal como carácter separador, debes duplicarla como en “<@lit="\\">”. Ejemplo: 

<code>          
     string s = "c:\fiddle\sticks"
     strings S = strsplit(s, "\\")
</code>

Independientemente del separador, a los elementos del 'array' que se devuelve, se les recorta cualquier espacio en blanco al principio o al final. En consecuencia, si <@var="sep"> contiene caracteres que no son espacios en blanco, entonces se le quita cualquier espacio al principio o al final. 

Cuando indicas un valor entero mayor que cero como tercer argumento, el valor que se devuelve es una única cadena de texto; concretamente, el elemento <@var="i"> (en base 1) del 'array' que se generaría de otro modo sin ese tercer argumento. Cuando <@var="i"> sea menor que 1, se produce un fallo; pero cuando <@var="i"> excede el número de elementos implicados, se devuelve una cadena de texto vacía. 

# strstr strings
Resultado: 	cadena 
Argumentos:	<@var="s1">  (cadena)
		<@var="s2">  (cadena)
		<@var="ign_mayus">  (booleano, opcional)

Busca en <@var="s1"> la cadena <@var="s2">. En caso de encontrar la cadena de texto, devuelve otra cadena con una copia de la parte de <@var="s1"> que comienza con <@var="s2">; en caso contrario, devuelve una cadena de texto vacía. 

Ejemplo: 

<code>          
          string s1 = "GRETL es un programa de Econometría"
          string s2 = strstr(s1, "un")
          print s2
</code>

Si el argumento opcional <@var="ign_mayus"> no es cero, la búsqueda no distinguirá mayúsculas de minúsculas. Por ejemplo: 

<code>          
     strstr("Bilbao", "b")
</code>

devolve “bao”, pero 

<code>          
     strstr("Bilbao", "b", 1)
</code>

devolve “Bilbao”. 

Si únicamente quieres descubrir si <@var="s1"> contiene a <@var="s2"> (prueba booleana), consulta <@ref="instring">. 

# strstrip strings
Resultado: 	cadena 
Argumento: 	<@var="s">  (cadena)

Devuelve una cadena de texto con una copia de <@var="s"> en la que se eliminaron los espacios en blanco del inicio y del final. 

Ejemplo: 

<code>          
          string s1 = "    Mucho espacio en blanco.  "
          string s2 = strstrip(s1)
          print s1 s2
</code>

# strsub strings
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="s">  (cadena, array de cadenas o serie con cadenas de valores)
		<@var="hallada">  (cadena)
		<@var="substit">  (cadena)

Si <@var="s"> es una cadena única, devuelve una cadena de texto con una copia de <@var="s"> en la que se substituyó toda la cadena <@var="hallada"> por <@var="substit">. Si <@var="s"> es un 'array' de cadenas de texto o una serie que contiene cadenas de valores, esta operación se realiza con cada una de las cadenas del 'array' o de la serie. Consulta también <@ref="regsub"> para otras substituciones más complejas mediante expresiones regulares. 

Ejemplo: 

<code>          
          string s1 = "Hola, GRETL!"
          string s2 = strsub(s1, "GRETL", "HANSL")
          print s2
</code>

# strvals strings
Resultado: 	array de cadenas 
Argumentos:	<@var="y">  (serie)
		<@var="submuestra">  (booleano, opcional)

Cuando la serie <@var="y"> se compone de cadenas de texto que expresan valores, esta función devuelve por defecto un 'array' que contiene todos esos valores (con independencia del rango muestral que esté vigente), ordenados numéricamente comenzando por el 1. Si está vigente una submuestra del conjunto de datos, puedes proporcionar un valor no nulo para el segundo argumento (opcional) y obtener así un 'array' que contenga solo las cadenas de texto presentes en la submuestra. 

Cuando <@var="y"> no se compone de cadenas de texto que expresan valores, se devuelve un 'array' de cadenas de texto vacías. Ver también <@ref="stringify">. 

Una alternativa a <@lit="strvals"> que puede ser de utilidad en algunos contextos es la asignación directa de una serie con valores de cadenas de texto a un 'array' de cadenas de texto: esto no solo proporciona los valores que sean diferentes, sino todos los valores de la serie en el rango de la muestra vigente. 

# strvsort strings
Resultado: 	entero 
Argumentos:	<@var="y">  (serie)
		<@var="S">  (array de cadenas, opcional)

Lleva a cabo uno de los dos tipos de reordenación de la serie <@var="y">, que debe constar de valores en cadenas de texto. El valor nominal que se devuelve es 0, cuando se completa con éxito. 

Método 1: Si no indicas el segundo argumento, el efecto que se produce es la ordenación de <@var="y"> en este sentido: los valores de cadena diferentes se alfabetizan y la serie se recodifica entonces de modo que se asigna el 1 a la primera cadena ordenada, el 2 a la segunda, etcétera. Esto puede ser útil, entre otras razones, para asegurar una codificación uniforme para múltiples series que compartan el mismo conjunto de valores en cadenas. 

Método 2: Si indicas el segundo argumento, este debe ser un 'array' que contenga exactamente los diferentes valores de <@var="y"> en cadenas (y que pueden conseguirse mediante <@ref="strvals">), pero colocados en un orden preferido. El efecto entonces consiste en una recodificación de la serie de modo que el valor 1 se asigna a la primera cadena de texto de <@var="S">, el valor 2 a la segunda, etcétera. Esto puede ser útil para asegurar que los códigos numéricos “tengan sentido” cuando los valores en cadenas de texto puedan concebirse como naturalmente ordenados. 

El principal uso de estos métodos es en el manejo de series con valores en cadenas de texto importadas de fuentes de terceros, como son los archivos con información separada por comas. Para esos datos, GRETL asigna códigos numéricos basados simplemente en el orden en que están las cadenas de texto a lo largo de las filas del archivo. Así, en una serie con valores <@lit="bajo">, <@lit="medio"> y <@lit="alto">, se va a asignar a <@lit="alto"> el código 1 si sucede primero, en lugar de 3 (lo que sería claramente más “natural”). Esto se puede fijar utilizando el Método 2. Además, si dos o más series comparten los mismos valores en cadenas de texto, se van a codificar de modo distinto excepto que suceda que sus valores diferentes aparezcan en el mismo orden en el archivo de datos. Esto se puede fijar mediante cualquiera de los métodos. 

Ver también <@ref="stringify">, <@ref="strvals">. 

# substr strings
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="s">  (cadena, array de cadenas o serie con cadenas de valores)
		<@var="inicio">  (entero)
		<@var="fin">  (entero)

Si <@var="s"> es una cadena única, devuelve la subcadena del argumento <@var="s">, empezando en el carácter indicado por el entero positivo de <@var="inicio">, y finalizando en el indicado por el de <@var="fin">, ambos incluidos; o desde <@var="inicio"> hasta el término de <@var="s"> si <@var="fin"> es igual a –1. Si el argumento es un 'array' de cadenas de texto o una serie que contiene cadenas de valores, esta operación se realiza con cada una de las cadenas del 'array' o de la serie. 

Por ejemplo, el código de abajo 

<code>          
          string s1 = "Hola, GRETL!"
          string s2 = substr(s1, 7, 11)
          print s2
</code>

proporciona: 

<code>          
    ? print s2
    GRETL
</code>

Debes darte cuenta de que, en algunos casos, podrías estar deseando intercambiar claridad por concisión, y utilizar operadores de reducción e incremento, como en 

<code>          
          string s1 = "Hola, GRETL!"
          string s2 = s1[7:11]
          string s3 = s1 + 6
          print s2
          print s3
</code>

lo que te proporcionaría 

<code>          
    ? print s2
    GRETL
    ? print s3
    GRETL!
</code>

# sum stats
Resultado: 	escalar o serie 
Argumentos:	<@var="x">  (serie, matriz o lista)
		<@var="parcial">  (booleano, opcional)

Cuando <@var="x"> es una serie, devuelve un escalar con el resultado de sumar las observaciones no ausentes del argumento <@var="x">. Consulta también <@ref="sumall">. 

Cuando <@var="x"> es una matriz, devuelve un escalar con el resultado de sumar los elementos de la matriz. 

Cuando <@var="x"> es una lista de variables, la función devuelve una serie <@mth="y">, en la que cada valor <@mth="y"><@sub="t"> indica la suma de los valores de las variables de la lista en la observación <@mth="t">. Por defecto, la suma se registra como <@lit="NA">, si hay algún valor ausente en <@mth="t">; pero si le das un valor no nulo a <@var="parcial">, cualquier valor no ausente se usará para crear la suma. 

# sumall stats
Resultado: 	escalar 
Argumento: 	<@var="x">  (serie)

Devuelve un escalar con el resultado de sumar las observaciones de la serie <@var="x"> en la muestra seleccionada, o <@lit="NA"> si existe algún valor ausente. Utiliza <@ref="sum"> si quieres obtener la suma descartando los valores ausentes. 

# sumc stats
Resultado: 	vector fila 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector fila con las sumas de cada columna de la matriz <@var="X">. Si indicas un valor no nulo para el segundo argumento (opcional), se ignoran los valores ausentes; de otro modo, el resultado es <@lit="NA"> para cada columna que contenga valores ausentes. Ver también <@ref="meanc">, <@ref="sumr">. 

# sumr stats
Resultado: 	vector columna 
Argumentos:	<@var="X">  (matriz)
		<@var="obviar_na">  (booleano, opcional)

Devuelve un vector columna con las sumas de cada fila de la matriz <@var="X">. Si indicas un valor no nulo para el segundo argumento (opcional), se ignoran los valores ausentes; de otro modo, el resultado es <@lit="NA"> para cada fila que contenga valores ausentes. Ver también <@ref="meanr">, <@ref="sumc">. 

# svd linalg
Resultado: 	vector fila 
Argumentos:	<@var="X">  (matriz)
		<@var="&U">  (referencia a matriz, o <@lit="null">)
		<@var="&V">  (referencia a matriz, o <@lit="null">)

Devuelve un vector fila con el resultado de descomponer la matriz <@var="X"> en valores singulares. 

Los valores singulares se devuelven en un vector fila. Puedes obtener el vector singular izquierdo <@mth="U"> y/o el derecho <@mth="V"> indicando valores no nulos en los argumentos 2 y 3, respectivamente. Para cualquier matriz <@lit="A">, el código... 

<code>          
     s = svd(A, &U, &V)
     B = (U .* s) * V
</code>

... debiera de proporcionar una matriz <@lit="B"> idéntica a <@lit="A"> (excepto pequeñas diferencias debida a la precisión de cálculo). 

Ver también <@ref="eigengen">, <@ref="eigensym">, <@ref="qrdecomp">. 

# svm nonparam
Resultado: 	serie 
Argumentos:	<@var="L">  (lista)
		<@var="bparms">  (bundle)
		<@var="bmod">  (referencia a bundle, opcional)
		<@var="bprob">  (referencia a bundle, opcional)

Esta función te permite el entrenamiento (y la predicción basada en ella) de una MSV (Máquina de Soporte Vectorial o SVM), utilizando la librería LIBSVM como soporte. El argumento de tipo lista <@var="L"> deberá incluir la variable dependiente seguida de las variables independientes; y el 'bundle' <@var="bparms"> se utiliza para pasarle opciones al mecanismo de la MSV. El valor que se devuelve es una serie que contiene las predicciones de la MSV. Puedes utilizar los dos argumentos opcionales puntero-bundle para recuperar información adicional después del entrenamiento y/o predicción. 

Para obtener más detalles, consulta la documentación PDF para <@mnu="gretlSVM">. 

# tan math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con la tangente de <@var="x">. Ver también <@ref="atan">, <@ref="cos">, <@ref="sin">. 

# tanh math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) con la tangente hiperbólica de <@var="x">. 

Ver también <@ref="atanh">, <@ref="cosh">, <@ref="sinh">. 

# tdisagg transforms
Resultado: 	matriz 
Argumentos:	<@var="Y">  (serie o matriz)
		<@var="X">  (serie, lista o matriz, opcional)
		<@var="s">  (escalar)
		<@var="opciones">  (bundle, opcional)
		<@var="resultados">  (bundle, opcional)

Realiza la desagregación temporal (conversión a una frecuencia mayor) de los datos de tipo serie temporal que haya en <@var="Y">. El argumento <@var="s"> proporciona el factor de expansión (por ejemplo, 3 para pasar de trimestrales a mensuales). El argumento <@var="X"> puede contener una o más covariantes (que tengan la frecuencia mayor) para ayudar en el proceso de desagregación. Puedes asumir diversas opciones en el argumento <@var="opciones">, y puedes recoger los detalles de la desagregación por medio de <@var=" resultados">. 

Consulta <@pdf="El manual de gretl#chap:tdisagg"> (Capítulo 9) para obtener más detalles. 

# toepsolv linalg
Resultado: 	vector columna 
Argumentos:	<@var="c">  (vector)
		<@var="r">  (vector)
		<@var="b">  (vector)
		<@var="&det">  (referencia a escalar, opcional)

Devuelve un vector columna con la solución de un sistema Toeplitz de ecuaciones lineales, es decir <@mth="Tx = b"> donde <@mth="T"> es una matriz cuadrada cuyo elemento <@mth="T"><@sub="i,j"> es igual a <@mth="c"><@sub="i-j"> cuando <@mth="i>=j">, e igual a <@mth="r"><@sub="j-i"> cuando <@mth="i<=j">. Ten en cuenta que los primeros elementos de los dos vectores <@mth="c"> y <@mth="r"> deben ser iguales, pues en caso contrario se devuelve un fallo. Cuando se completa con éxito, la ejecución de esta función permite obtener el vector <@mth="x">. 

El algoritmo que se utiliza aquí aprovecha la especial estructura de la matriz <@mth="T">, lo que lo hace mucho más eficiente que otros algoritmos no especializados, particularmente para problemas muy largos. Advertencia: En algunos casos, la función podría sugerir falsamente un fallo en la singularidad de la matriz <@mth="T"> cuando realmente no es singular; de cualquier modo, este problema no podrá surgir cuando la matriz <@mth="T"> sea definida positiva. 

Cuando se indica el argumento opcional <@var="det"> (en forma de puntero), al finalizar, este va a contener el determinante de <@mth="T">. Por ejemplo, el código: 

<code>          
     A = unvech({3;2;1;3;2;3})    # Configura una matriz 3x3 de Toeplitz
     x = ones(3,1)                # y un vector 3x1
     print A x
     eval A\x                     # Soluciona mediante la inversión general
     eval det(A)                  # Presenta el determinante
     a = A[1,]
     d = 0
     eval toepsolv(a, a, x, &d)   # Utiliza la función específica
     print d
</code>

produce 

<code>          
A (3 x 3)

  3   2   1
  2   3   2
  1   2   3

x (3 x 1)

  1 
  1 
  1 

     0,25000
 -3,3307e-17
     0,25000

8
     0,25000
  2,7756e-17
     0,25000


d =  8,0000000
</code>

# tolower strings
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="s">  (cadena, array de cadenas o serie con cadenas de valores)

Si <@var="s"> es una cadena única, devuelve una copia de <@var="s">, en la que todas las letras en mayúsculas se convirtieron en minúsculas. Si <@var="s"> es un 'array' de cadenas de texto o una serie que contiene cadenas de valores, esta operación se realiza con cada una de las cadenas del 'array' o de la serie. 

Ejemplo: 

<code>          
          string s1 = "Hola, GRETL!"
          string s2 = tolower(s1)
          print s2
</code>

# toupper strings
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="s">  (cadena, array de cadenas o serie con cadenas de valores)

Si <@var="s"> es una cadena única, devuelve una copia de <@var="s">, en la que todas las letras en minúsculas se convirtieron en mayúsculas. Si <@var="s"> es un 'array' de cadenas de texto o una serie que contiene cadenas de valores, esta operación se realiza con cada una de las cadenas del 'array' o de la serie. 

Ejemplo: 

<code>          
          string s1 = "Hola, GRETL!"
          string s2 = toupper(s1)
          print s2
</code>

# tr linalg
Resultado: 	escalar 
Argumento: 	<@var="A">  (matriz cuadradax)

Devuelve un escalar con la traza de la matriz cuadrada <@var="A">, es decir, la suma de los elementos de su diagonal. Ver también <@ref="diag">. 

# transp linalg
Resultado: 	matriz 
Argumento: 	<@var="X">  (matriz)

Devuelve una matriz que es la traspuesta de <@var="X">. Aviso: Esta función se utiliza raramente. Para trasponer una matriz, en general puedes usar simplemente el operador para trasposición: <@lit="X'">. 

# trigamma math
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado del mismo tipo que el argumento con la función trigamma de <@var="x">; es decir, la segunda derivada del logaritmo de la función Gamma. 

Ver también <@ref="lngamma">, <@ref="digamma">. 

# trimr matrix
Resultado: 	matriz 
Argumentos:	<@var="X">  (matriz)
		<@var="tsup">  (entero)
		<@var="tinf">  (entero)

Devuelve una matriz que es una copia de la matriz <@var="X"> en la que se eliminaron las <@var="tsup"> filas superiores y las <@var="tinf"> filas inferiores. Los dos últimos argumentos no deben ser negativos, y su suma debe ser menor que el total de filas de <@var="X">. 

Ver también <@ref="selifr">. 

# typename data-utils
Resultado: 	cadena 
Argumento: 	<@var="expr">  (cadena)

Una función de conveniencia que combina las funciones <@ref="typeof"> y <@ref="typestr">, con un pequeño valor añadido. Básicamente, los dos siguientes enunciados serían equivalentes ... 

<code>          
     eval typestr(typeof(x))
     eval typename(x)
</code>

... excepto en que cuando <@var="expr"> denomina un 'array', <@lit="typename"> devuelve el tipo específico de 'array', como en 

<code>          
     strings S = defarray("foo", "bar", "baz")
     eval typestr(typeof(S))  # presenta "array"
     eval typename(S)         # presenta "strings"
</code>

# typeof data-utils
Resultado: 	entero 
Argumento: 	<@var="expr">  (cadena)

Devuelve un código numérico indicando el tipo de <@var="expr"> cuando denomina una variable vigente ya definida, especifica un objeto secundario como un elemento de un 'bundle' o un elemento de un 'array', o es una expresión correcta que se pueda colocar en el lado derecho de una operación de asignación. Los códigos son: 1 para un escalar, 2 para una serie, 3 para una matriz, 4 para una cadena de texto, 5 para un 'bundle', 6 para un 'array' y 7 para una lista. Al devolverse un valor igual a 0, se indica que <@var="expr"> denomina un objeto que no existe o, más generalmente, que fallaría una asignación con <@var="expr"> en el lado derecho. 

A continuación, algunos ejemplos: 

<code>          
     strings S = defarray("foo", "bar")
     eval typeof(S)            # presenta 6 (array)
     eval typeof(S[1])         # presenta 4 (cadena de texto)
     eval typeof(S[7])         # presenta 0 (fuera de límites)
     eval typeof(S[x])         # presenta 0 (índice incorrecto)
     eval typeof(1+1)          # presenta 1 (escalar)
     eval typeof(sqrt("foo"))  # presenta 0 (incorrecto)
</code>

Se puede utilizar la función <@ref="typestr"> para conseguir la cadena de texto que se corresponde con el valor que se devuelve con <@lit="typeof">; aunque si únicamente quieres el resultado de la cadena, <@ref="typename"> puede ser una alternativa más conveniente. 

# typestr data-utils
Resultado: 	cadena 
Argumento: 	<@var="codigotipo">  (entero)

Dado un código de tipo del GRETL (por ejemplo, obtenido mediante <@ref="typeof"> o <@ref="inbundle">), devuelve una cadena de texto que indica el nombre de ese tipo. La asignación de códigos a cadenas de texto es: 1 = “scalar” (escalar), 2 = “series” (serie), 3 = “matrix” (matriz), 4 = “string” (cadena de texto), 5 = “bundle”, 6 = “array”, 7 = “list” (lista), y 0 = “null” (nulo). 

Consulta también <@ref="typename"> como alternativa. 

# uniform probdist
Resultado: 	serie 
Argumentos:	<@var="a">  (escalar)
		<@var="b">  (escalar)

Devuelve una serie que se genera con una variable pseudoaleatoria Uniforme que toma valores dentro del intervalo (<@var=" a">, <@var="b">) o, si no indicas esos argumentos, en el intervalo (0,1). El algoritmo que se utiliza por defecto es el “SIMD-oriented Fast Mersenne Twister” desarrollado por <@bib="Saito y Matsumoto (2008);saito_matsumoto08">. 

Ver también <@ref="randgen">, <@ref="normal">, <@ref="mnormal">, <@ref="muniform">. 

# uniq stats
Resultado: 	vector columna 
Argumento: 	<@var="x">  (serie o vector)

Devuelve un vector que contiene los distintos elementos no ausentes del argumento <@var="x"> sin ningún orden especial, sino en el que están en <@var="x">. Consulta <@ref="values"> para la variante de esta función que devuelve los valores ordenados. 

# unvech matrix
Resultado: 	matriz cuadradax 
Argumentos:	<@var="v">  (vector)
		<@var="d">  (escalar, opcional)

Si omites el segundo argumento, devuelve la matriz simétrica de orden <@itl="n">×<@itl="n"> que se obtiene reordenando los elementos del vector <@mth="v"> en forma de matriz triangular inferior, y copiando los de las posiciones simétricas. El número de elementos de <@mth="v"> debe ser un entero triangular, o sea, un número <@mth="k"> tal que exista un entero <@mth="n"> que cumpla la siguiente propiedad: <@mth="k = n(n+1)/2">. Esta función es la inversa de <@ref="vech">. 

Si indicas el argumento <@var="d">, la función devuelve una matriz <@itl="(n+1)">×<@itl="(n+1)">, con las posiciones fuera de la diagonal principal ocupadas con los elementos de <@mth="v">, como en el caso anterior. Por el contrario, todos los elementos de la diagonal principal se establece que sean iguales a <@var="d">. 

Ejemplo: 

<code>          
        v = {1;2;3}
        matrix uno = unvech(v)
        matrix dos = unvech(v, 99)
        print uno dos
</code>

devuelve 

<code>          
      uno (2 x 2)

      1   2
      2   3

      dos (3 x 3)

      99     1     2
       1    99     3
       2     3    99
</code>

Ver también <@ref="mshape">, <@ref="vech">. 

# upper matrix
Resultado: 	matriz cuadradax 
Argumento: 	<@var="A">  (matriz cuadradax)

Devuelve una matriz triangular superior de orden <@itl="n">×<@itl="n">. Los elementos de la diagonal y los de arriba de esta, son iguales a los elementos que se corresponden en <@var="A">; los demás son iguales a cero. 

Ver también <@ref="lower">. 

# urcpval probdist
Resultado: 	escalar 
Argumentos:	<@var="tau">  (escalar)
		<@var="n">  (entero)
		<@var="niv">  (entero)
		<@var="itv">  (entero)

Devuelve un escalar con la probabilidad asociada (<@mth="P">) al valor del estadístico para hacer el contraste de raíces unitarias de Dickey-Fuller o el contraste de cointegración de Engle–Granger, de acuerdo con <@bib="James MacKinnon (1996);mackinnon96">. 

Los argumentos se expresan de este modo: <@var="tau"> indica el valor del estadístico de contraste que corresponda; <@var="n"> señala el número de observaciones (o 0 si lo que quieres es un resultado asintótico);<@var="niv"> denota el número de variables potencialmente cointegradas, si compruebas la cointegración (o 1 si haces un contraste univariante de raíces unitarias); e <@var="itv"> es un código que especifica el tipo modelo (1 = sin constante, 2 = con constante, 3 = con constante más tendencia lineal, 4 = con constante más tendencia cuadrada). 

Ten en cuenta que debes darle un valor de 0 a <@var="n"> para obtener un resultado asintótico, si la regresión auxiliar para el contraste es “aumentada” con retardos de la variable dependiente. 

Ver también <@ref="pvalue">, <@ref="qlrpval">. 

# values stats
Resultado: 	vector columna 
Argumento: 	<@var="x">  (serie o vector)

Devuelve un vector que contiene los distintos elementos del argumento <@var="x"> ordenados de forma ascendente, ignorando cualquiera de los valores ausente. Si quieres descartar la parte decimal antes de aplicar esta función, utiliza la expresión <@lit="values(int(x))">. 

Ver también <@ref="uniq">, <@ref="dsort">, <@ref="sort">. 

# var stats
Resultado: 	escalar o serie 
Argumentos:	<@var="x">  (serie o lista)
		<@var="parcial">  (booleano, opcional)

Cuando <@var="x"> es una serie, devuelve un escalar con su varianza muestral, descartando cualquier observación ausente. 

Cuando <@var="x"> es una lista, devuelve una serie <@mth="y"> en la que cada valor <@mth="y"><@sub="t"> indica la varianza muestral de los valores de las variables de la lista en la observación <@mth="t">. Por defecto, la varianza se registra como <@lit="NA">, si hay algún valor ausente en <@mth="t">; pero si le das un valor no nulo a <@var="parcial">, cualquier valor no ausente se usará para crear el estadístico. 

En cada uno de esos casos, la suma de los cuadrados de las desviaciones con respecto a la media se divide por (<@mth="n"> – 1) cuando <@mth="n"> > 1. En otro caso, se indica que la varianza es igual a cero si <@mth="n"> = 1, o es igual a <@lit="NA"> si <@mth="n"> = 0. 

Ver también <@ref="sd">. 

# varname strings
Resultado: 	cadena 
Argumento: 	<@var="v">  (entero o lista)

Cuando se indica un número entero como argumento, la función devuelve una cadena de texto con el nombre de la variable que tiene un número ID igual a <@var="v">, o genera un fallo si esa variable no existe. 

Cuando se indica una lista como argumento, devuelve una cadena de texto que contiene los nombres de las variables de la lista, separados por comas. Si indicas una lista que está vacía, se devuelve una cadena de texto vacía. En su lugar, puedes utilizar <@ref="varnames"> para obtener un 'array' de cadenas de texto . 

Ejemplo: 

<code>          
        open broiler.gdt
        string s = varname(7)
        print s
</code>

# varnames strings
Resultado: 	array de cadenas 
Argumento: 	<@var="L">  (lista)

Devuelve un 'array' de cadenas de texto que contiene los nombres de las variables de la lista <@var="L">. Si la lista que indicas está vacía, se devuelve un 'array' vacío. 

Ejemplo: 

<code>          
        open keane.gdt
        list L = year wage status
        strings S = varnames(L)
        eval S[1]
        eval S[2]
        eval S[3]
</code>

# varnum data-utils
Resultado: 	entero 
Argumento: 	<@var="nombrevar">  (cadena)

Devuelve un número entero con el código ID de la variable que tiene el nombre del argumento <@var="nombrevar">, o NA si esa variable no existe. 

# varsimul timeseries
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz)
		<@var="U">  (matriz)
		<@var="y0">  (matriz)

Devuelve una matriz al simular un VAR de orden <@mth="p"> y <@mth="n"> variables, es decir <@mth="y(t) = A1 y(t-1) + ... + Ap y(t-p) + u(t)."> La matriz <@var="A"> de coeficientes se forma agrupando horizontalmente las matrices <@mth="A"><@sub="i">; y es de orden <@itl="n">×<@itl="np">, con una fila por cada ecuación. Esta se corresponde con las primeras <@mth="n"> filas de la matriz <@lit="$compan"> que proporcionan las instrucciones <@lit="var"> y <@lit="vecm">. 

Los vectores <@mth="u_t"> están incluidos (como filas) en la matriz <@var="U"> (<@itl="T">×<@itl="n">). Los valores iniciales están en <@var="y0"> (<@itl="p">×<@itl="n">). 

Cuando el VAR contiene algún término determinista y/o regresores exógenos, puedes manejarlos incorporándolos a la matriz <@var="U">: en este caso cada fila de <@var="U"> pasa a ser entonces <@mth="u(t) = B'x(t) + e(t)."> 

La matriz que resulta tiene <@mth="T"> + <@mth="p"> filas y <@mth="n"> columnas; contiene los <@mth="p"> valores iniciales de las variables endógenas, además de <@mth="T"> valores simulados. 

Ver también <@ref="$compan">, <@xrf="var">, <@xrf="vecm">. 

# vec matrix
Resultado: 	vector columna 
Argumento: 	<@var="X">  (matriz)

Devuelve un vector columna, apilando las columnas de <@var="X">. Ver también <@ref="mshape">, <@ref="unvech">, <@ref="vech">. 

# vech matrix
Resultado: 	vector columna 
Argumentos:	<@var="A">  (matriz cuadradax)
		<@var="omitir-diag">  (booleano, opcional)

Esta función vuelve a ordenar en un vector columna, los elementos de la matriz <@var="A"> que están en la diagonal principal y por encima de ella, excepto que le asignes un valor no nulo a la opción <@var="omitir-diag">, en cuyo caso solo se tienen en cuenta las posiciones por encima. 

Normalmente esta función se utiliza con matrices simétricas, en cuyo caso, esa operación puede revertirse mediante la función <@ref="unvech">. Si la matriz de entrada no es simétrica y su triángulo inferior contiene los valores “correctos”, puedes obtener el resultado deseado por medio de <@lit="vech(A')"> (aunque sus elementos puede que necesiten volver a ordenarse de nuevo). Ver también <@ref="vec">. 

# vma timeseries
Resultado: 	matriz 
Argumentos:	<@var="A">  (matriz)
		<@var="K">  (matriz, opcional)
		<@var="horizonte">  (entero, opcional)

Esta función genera una matriz con la representación VMA de un sistema VAR. Si <@mth="u"><@sub="t"> son los residuos de las predicciones adelantadas un paso y <@mth="y(t) = A1 y(t-1) + ... + Ap y(t-p) + u(t)">, la correspondiente representación VMA es <@mth="y(t) = C0 e(t) + C1 e(t-1) + ...">. La relación entre <@mth="u"><@sub="t"> (residuos de predicciones) y <@mth="e"><@sub="t"> (impactos estructurales) será <@mth="u(t) = K e(t)">. (Observa que <@mth="C"><@sub="0"> = <@mth="K">.) 

La matriz <@var="A"> de coeficientes del primer argumento, se forma apilando las matrices <@mth="A"><@sub="i"> de forma horizontal; tendrá rango <@itl="n">×<@itl="np">, con una fila por cada ecuación. Esto se corresponde con las primeras <@mth="n"> filas de la matriz <@lit="$compan"> que proporcionan las instrucciones <@lit="var"> y <@lit="vecm"> de GRETL. La matriz <@var="K"> es opcional, indicando por defecto la matriz identidad. 

La matriz que devuelve esta función tiene un número de filas igual a <@var="horizonte">, y <@mth="n"><@sup="2"> columnas: cada <@mth="i">-ésima fila contiene <@mth="C"><@sub="i-1"> en formato vectorial. El valor de <@var="horizonte"> se establece por defecto igual a 24, cuando no se indique. 

Ver también <@ref="irf">. 

# weekday calendar
Resultado: 	mismo tipo que introducido 
Argumentos:	<@var="año">  (escalar o serie)
		<@var="mes">  (escalar o serie)
		<@var="día">  (escalar o serie)

Devuelve el día de la semana (de Domingo = 0 hasta Sábado=6) de la fecha especificada por los tres argumentos, o <@lit="NA"> si la fecha no es correcta. Ten en cuenta que los tres argumentos deben ser del mismo tipo; o sea, deben ser todos de tipo escalar (entero) o todos de tipo serie. 

También se admite una solicitud alternativa: cuando se indica un único argumento, se considera que es una fecha (o una serie de fechas) en formato numérico “básico” ISO 8601, <@lit="YYYYMMDD">. De este modo, las siguientes dos solicitudes generan el mismo resultado, concretamente 2 (martes). 

<code>          
     eval weekday(1990, 5, 1)
     eval weekday(19900501)
</code>

Una alternativa habitual de numeración de los días de la semana va desde Lunes = 1 hasta Domingo = 7. Si tienes una serie denominada <@lit="wd"> obtenida mediante la función <@lit="weekday">, y quieres convertirla a la alternativa, puedes hacer 

<code>          
     altwd = wd == 0 ? 7 : wd
</code>

Ten en cuenta que si únicamente añades 1 a <@lit="wd">, obtienes una numeración que es válida pero no estándar; concretamente de Domingo = 1 a Sábado = 7. 

# wmean transforms
Resultado: 	serie 
Argumentos:	<@var="Y">  (lista)
		<@var="W">  (lista)
		<@var="parcial">  (booleano, opcional)

Devuelve una serie <@mth="y"> calculada de forma que cada <@mth="y"><@sub="t"> indica la media ponderada de los valores (en la observación <@mth="t">) de las variables presentes en la lista <@var="Y">, con las respectivas ponderaciones señaladas por los valores de las variables que forman la lista <@var="W"> en cada <@mth="t">. Las ponderaciones pueden así variar con el tiempo. Las listas <@var="Y"> y <@var="W"> de variables deben tener el mismo tamaño, y las ponderaciones deben ser no negativas. 

Por defecto, el resultado es <@lit="NA">, si hay algún valor ausente en la observación <@mth="t">; pero si le das un valor no nulo a <@var="parcial">, se utilizará cualquier valor no ausente. 

Ver también <@ref="wsd">, <@ref="wvar">. 

# wsd transforms
Resultado: 	serie 
Argumentos:	<@var="Y">  (lista)
		<@var="W">  (lista)
		<@var="parcial">  (booleano, opcional)

Devuelve una serie <@mth="y"> calculada de forma que cada <@mth="y"><@sub="t"> indica la desviación típica ponderada muestral, de los valores (en la observación <@mth="t">) de las variables presentes en la lista <@var="Y">, con las respectivas ponderaciones señaladas por los valores de las variables de la lista <@var="W"> en cada <@mth="t">. Las ponderaciones pueden así variar con el tiempo. Las listas <@var="Y"> y <@var="W"> de variables deben tener el mismo tamaño, y las ponderaciones deben ser no negativas. 

Por defecto, el resultado es <@lit="NA">, si hay algún valor ausente en la observación <@mth="t">; pero si le das un valor no nulo a <@var="parcial">, se utilizará cualquier valor no ausente. 

Ver también <@ref="wmean">, <@ref="wvar">. 

# wvar transforms
Resultado: 	serie 
Argumentos:	<@var="X">  (lista)
		<@var="W">  (lista)
		<@var="parcial">  (booleano, opcional)

Devuelve una serie <@mth="y"> calculada de forma que cada <@mth="y"><@sub="t"> indica la varianza ponderada muestral, de los valores (en la observación <@mth="t">) de las variables presentes en la lista <@var="Y">, con las respectivas ponderaciones señaladas por los valores de las variables que forman la lista <@var="W"> en cada <@mth="t">. Las ponderaciones pueden así variar con el tiempo. Las listas <@var="Y"> y <@var="W"> de variables deben tener el mismo tamaño, y las ponderaciones deben ser no negativas. 

Por defecto, el resultado es <@lit="NA">, si hay algún valor ausente en la observación <@mth="t">; pero si le das un valor no nulo a <@var="parcial">, se utilizará cualquier valor no ausente. 

Ver también <@ref="wmean">, <@ref="wsd">. 

# xmlget data-utils
Resultado: 	cadena 
Argumentos:	<@var="buf">  (cadena)
		<@var="ruta">  (cadena o array de cadenas)
		<@var="&coincidencias">  (referencia a escalar, opcional)

El argumento <@var="buf"> debe ser un buffer XML, tal como puede recuperarse de un lugar web adecuado mediante la función <@ref="curl"> (o leerse de un archivo mediante la función <@ref="readfile">); y el argumento <@var="ruta"> debe ser una especificación XPath sencilla o un 'array' de ellas. 

Esta función devuelve una cadena de texto que representa los datos encontrados en el buffer XML en la ruta especificada. Si hay múltiples nodos que coincidan con la expresión de la ruta, las unidades de datos se presentan una por cada línea de la cadena que se devuelve. Cuando indicas un 'array' de rutas como segundo argumento, la cadena que se devuelve tiene la forma de un buffer separado con comas, cuya columna <@mth="i"> contiene las coincidencias de la ruta <@mth="i">. En este caso, si una cadena obtenida del buffer XML contiene algún espacio o coma, se entrecomilla. 

Por defecto, se muestra un fallo si <@var="ruta"> no coincide en el buffer XML; pero este comportamiento se modifica si indicas el tercer argumento (opcional) pues, en este caso, el argumento recupera un recuento de las coincidencias, devolviéndose una cadena vacía si no hay ninguna. Llamada de ejemplo: 

<code>          
     ngot = 0
     ret = xmlget(xbuf, "//some/thing", &ngot)
</code>

Ahora bien, todavía se va a mostrar un fallo en caso de hacer una solicitud mal configurada. 

Puedes encontrar una buena introducción al uso y a la sintaxis de XPath en <@url="https://www.w3schools.com/xml/xml_xpath.asp">. El programa de soporte (back-end) para <@lit="xmlget"> lo proporciona el módulo xpath de libxml2, que admite XPath 1.0 pero no XPath 2.0. 

Ver también <@ref="jsonget">, <@ref="readfile">. 

# zeromiss transforms
Resultado: 	mismo tipo que introducido 
Argumento: 	<@var="x">  (escalar, serie o matriz)

Devuelve un resultado (del tipo del argumento) cambiando los ceros por <@lit="NA">s. Si <@var="x"> es una serie o una matriz, la conversión se hace elemento a elemento. Ver también <@ref="missing">, <@ref="misszero">, <@ref="ok">. 

# zeros matrix
Resultado: 	matriz 
Argumentos:	<@var="r">  (entero)
		<@var="c">  (entero, opcional)

Devuelve una matriz nula con <@mth="r"> filas y <@mth="c"> columnas. Si lo omites, el número de columnas se establece en 1 (vector columna), por defecto. Ver también <@ref="ones">, <@ref="seq">. 

